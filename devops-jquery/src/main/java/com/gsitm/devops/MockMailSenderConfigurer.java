package com.gsitm.devops;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Profile("smtp-mock")
@Configuration
public class MockMailSenderConfigurer {

	@Bean(name = "mailSender")
	public JavaMailSender configureJavaMailSender() {
		MockMailSenderImpl sender = new MockMailSenderImpl();
		return sender;
	}

	public class MockMailSenderImpl extends JavaMailSenderImpl {
		@Override
		public void send(SimpleMailMessage simpleMessage) throws MailException {
			System.out.println("From    : " + simpleMessage.getFrom());
			System.out.println("To      : " + simpleMessage.getTo());
			System.out.println("Subject : " + simpleMessage.getSubject());
			System.out.println("Text    : " + simpleMessage.getText());
		}
	}
}
