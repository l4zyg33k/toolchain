package com.gsitm.devops;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Configuration
public class ObjectMapperConfigurer {

	@Bean
	public Module configureHibernate4Module() {
		return new Hibernate4Module();
	}

	@Bean
	public Module configureJodaModule() {
		return new JodaModule();
	}
}
