package com.gsitm.devops;

import org.apache.catalina.Context;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import de.javakaffee.web.msm.MemcachedBackupSessionManager;

@Profile("mem-cached")
@Configuration
public class MemCachedConfigurer {

	@Value("${memcached.nodes}")
	private String memcachedNodes;

	@Bean(name = "embeddedServletContainerFactory")
	public EmbeddedServletContainerFactory configureServletContainer() {
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		tomcat.addContextCustomizers(new TomcatContextCustomizer() {
			@Override
			public void customize(Context context) {
				MemcachedBackupSessionManager manager = new MemcachedBackupSessionManager();
				manager.setMemcachedNodes(memcachedNodes);
				manager.setRequestUriIgnorePattern(".*\\.(png|gif|jpg|css|js|ico)$");
				manager.setSessionBackupAsync(false);
				manager.setSessionBackupTimeout(100);
				context.setManager(manager);
			}
		});
		return tomcat;
	}
}
