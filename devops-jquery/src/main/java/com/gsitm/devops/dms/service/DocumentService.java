package com.gsitm.devops.dms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.dms.model.Document;

public interface DocumentService extends SharedService<Document, Long> {
}
