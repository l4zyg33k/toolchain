package com.gsitm.devops.dms.model.vo;

import java.util.List;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocMngGrdVO {
	private Long id;
	private String code;
	private String name;
	private String divisionName;
	private String systemName;
	private String categoryName;
	private Float version;
	private String statusName;
	private String sysop;
	private String approvator;
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate approvedDate;
	private List<DocumentRevision> revisions;

	public DocMngGrdVO() {
		super();
	}
	
	@QueryProjection
	public DocMngGrdVO(Long id, String code, String name, String divisionName, String systemName, String categoryName, List<DocumentRevision> revisions) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.divisionName = divisionName;
		this.systemName = systemName;
		this.categoryName = categoryName;
		
		DocumentRevision repRevision = new DocumentRevision();
		for (DocumentRevision revision : revisions) {
			if (!revision.getStatus().getValue().equals("RE") || revisions.size() == 1) {
				repRevision = revision;
				break;
			}
		}
		
		this.version = repRevision.getVersion();
		this.statusName = (repRevision.getStatus() != null) ? repRevision.getStatus().getName() : "N/A";
		this.sysop = (repRevision.getSysop() != null) ? repRevision.getSysop().getName() : "N/A";
		this.approvator = (repRevision.getApprobator() != null) ? repRevision.getApprobator().getName() : "N/A";
		this.approvedDate = repRevision.getApprovedDate();
		
		this.revisions = revisions;
	}
}
