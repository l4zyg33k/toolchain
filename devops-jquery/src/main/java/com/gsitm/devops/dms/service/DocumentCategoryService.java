package com.gsitm.devops.dms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public interface DocumentCategoryService extends
		SharedService<DocumentCategory, Long> {
	
	public Iterable<DocumentCategory> findAll(Predicate predicate,
			OrderSpecifier<?> orderSpecifier);
}
