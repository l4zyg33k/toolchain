package com.gsitm.devops.dms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.dms.model.DocumentRevision;

public interface DocumentRevisionService extends
		SharedService<DocumentRevision, Long> {
}
