package com.gsitm.devops.dms.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.vo.CatMngBatchVO;
import com.gsitm.devops.dms.model.vo.CatMngFrmVO;
import com.gsitm.devops.dms.service.biz.CatMngBiz;

@Controller
public class CatMngController {

	private static final String MENU_CODE = "CatMng";
	private static final String FORM_VO = "catMngFrmVO";
	private static final String BATCH_VO = "catMngBatchVO";

	private static final String INDEX_URL = "/dms/catmng";
	private static final String ACTION_URL = "/dms/catmng/{id}";
	private static final String FORM_URL = "/dms/catmng/form";
	private static final String BATCH_URL = "/dms/catmng/batch";
	private static final String FORM_URL_BY_ID = "/dms/catmng/{id}/form";
	private static final String REDIRECT_URL = "redirect:/dms/catmng?page={page}&rowNum={rowNum}&rowId={rowId}";
	private static final String REDIRECT_URL2 = "redirect:/dms/catmng";

	private static final String INDEX_PAGE = "dms/catmng/index";
	private static final String FORM_PAGE = "dms/catmng/form";
	private static final String BATCH_PAGE = "dms/catmng/batch";

	@Autowired
	CatMngBiz biz;
	
	@Autowired
	private ObjectMapper objectMapper;


	@ModelAttribute("menuCode")
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute("divisions")
	public Iterable<Code> getDivisions(@AuthenticationPrincipal UserDetails user) {
		return biz.getDivisions(user);
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}
	
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String newbie(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum) {

		model.addAttribute(FORM_VO, new CatMngFrmVO(page, rowNum, /*rowId*/0));

		return FORM_PAGE;
	}

	@RequestMapping(value = BATCH_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum) throws JsonProcessingException {

		List<DocumentCategory> rowdata = new ArrayList<>();
		model.addAttribute("rowdata", objectMapper.writeValueAsString(rowdata));
		model.addAttribute(BATCH_VO, new CatMngBatchVO());

		return BATCH_PAGE;
	}

	@RequestMapping(value = BATCH_URL, method = RequestMethod.POST)
	public String create(
			Model model,
			@ModelAttribute(BATCH_VO) CatMngBatchVO vo,
			BindingResult result)
			throws IOException {
		
		List<Map<String, String>> rows = new ArrayList<>();
		String rowdata = vo.getRowdata();

		if (result.hasErrors() || vo.getFromSystem().getId() == vo.getToSystem().getId()) {
			rows = objectMapper.readValue(rowdata,
					new TypeReference<List<Map<String, String>>>() {
					});

			model.addAttribute("rowdata", objectMapper.writeValueAsString(rows));

			return BATCH_PAGE;
		}

		/*
		 * local 데이터소스인 경우 id 값에 jqg\\d+ 패턴으로 행 번호가 입력된다. 객체화를 위해 제거해 준다.
		 */
		rowdata = rowdata.replaceAll("jqg\\d+", "");

		if (rowdata != null) {
			rows = objectMapper.readValue(rowdata,
					new TypeReference<List<Map<String, String>>>() {
					});
		}

		if (rows.isEmpty() == false) {
			List<DocumentCategory> documentCategories = new ArrayList<>();

			for (Map<String, String> row : rows) {
				DocumentCategory documentCategory = new DocumentCategory();

				documentCategory.setSystem(vo.getToSystem());
				documentCategory.setCode(row.get("code"));
				documentCategory.setName(row.get("name"));
				documentCategories.add(documentCategory);
			}

			biz.save(documentCategories);

		}

		return REDIRECT_URL2;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(
			Model model,
			@ModelAttribute(FORM_VO) @Valid CatMngFrmVO vo,
			BindingResult result, RedirectAttributes attr) {

		if (result.hasErrors()) {
			return FORM_PAGE;
		}

		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());

		return REDIRECT_URL;
	}


	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET)
	public String form(Model model,
			@PathVariable("id") Long id,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId) {

		CatMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		
		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(
			Model model, @PathVariable("id") Long id,
			@ModelAttribute(FORM_VO) @Valid CatMngFrmVO vo,
			BindingResult result, RedirectAttributes attr) {

		if (result.hasErrors()) {
			return FORM_PAGE;
		}

		vo.setId(id);
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		
		return String.format(REDIRECT_URL, vo.getId());
	}
}
