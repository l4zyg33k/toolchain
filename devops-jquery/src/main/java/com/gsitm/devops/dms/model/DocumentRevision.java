/**
 * 운영문서Revision 엔티티
 * DocumentRevisions.java
 * @author Administrator
 */
package com.gsitm.devops.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.mysema.query.annotations.QueryInit;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class DocumentRevision extends AbstractAuditable<Account, Long> {

	/**
	 * 운영 문서
	 */
	@ManyToOne
	@QueryInit({"category.system.division","category.system.approbators","category.system.sysops","category.system.auditors"})
	private Document document;

	/**
	 * 변경 사유
	 */
	@Column(length = 32)
	private String occasion;

	/**
	 * 변경 내용
	 */
	@Column(length = 2304)
	private String contents;

	/**
	 * 파일 명칭
	 */
	@Column(length = 128)
	private String fileName;

	/**
	 * 파일 경로
	 */
	@Column(length = 128)
	private String filePath;

	/**
	 * 업로드 파일
	 */
	@Transient
	private CommonsMultipartFile file;

	/**
	 * 파일 설명
	 */
	@Column(length = 1024)
	private String fileDescription;

	/**
	 * 버전
	 */
	@Column(precision = 4, scale = 2)
	private Float version;

	/**
	 * 승인자
	 */
	@ManyToOne
	private Account approbator;

	/**
	 * 운영자
	 */
	@ManyToOne
	private Account sysop;

	/**
	 * 승인 일자
	 */
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate approvedDate;

	/**
	 * 처리 상태 (코드)
	 */
	@ManyToOne
	private Code status;

	/**
	 * 검토 의견
	 */
	@Column(length = 1024)
	private String comment;
	
	@Override
	public String toString() {
		return this.getId().toString();
	}
}