package com.gsitm.devops.dms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.dms.repository.SystemRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class SystemServiceImpl implements SystemService {

	@Autowired
	private SystemRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public com.gsitm.devops.dms.model.System findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public com.gsitm.devops.dms.model.System findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<com.gsitm.devops.dms.model.System> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<com.gsitm.devops.dms.model.System> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<com.gsitm.devops.dms.model.System> findAll(
			Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<com.gsitm.devops.dms.model.System> findAll(
			Predicate predicate, OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<com.gsitm.devops.dms.model.System> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<com.gsitm.devops.dms.model.System> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public com.gsitm.devops.dms.model.System save(
			com.gsitm.devops.dms.model.System system) {
		return repository.save(system);
	}

	@Transactional
	@Override
	public <S extends com.gsitm.devops.dms.model.System> List<S> save(
			Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(com.gsitm.devops.dms.model.System system) {
		repository.delete(system);
	}

	@Transactional
	@Override
	public void delete(
			Iterable<? extends com.gsitm.devops.dms.model.System> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Override
	public Iterable<com.gsitm.devops.dms.model.System> findAll(
			Predicate predicate, OrderSpecifier<?> orderSpecifier) {
		return repository.findAll(predicate, orderSpecifier);
	}
	
}
