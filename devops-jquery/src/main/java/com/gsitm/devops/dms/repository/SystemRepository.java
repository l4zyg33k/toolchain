package com.gsitm.devops.dms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;

@Repository
public interface SystemRepository extends
		SharedRepository<com.gsitm.devops.dms.model.System, Long> {
}
