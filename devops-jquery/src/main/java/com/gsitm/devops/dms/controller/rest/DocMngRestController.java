package com.gsitm.devops.dms.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.JqFilter;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.vo.DocMngGrdVO;
import com.gsitm.devops.dms.model.vo.DocMngSchVO;
import com.gsitm.devops.dms.service.biz.DocMngBiz;

@RestController
public class DocMngRestController {

	@Autowired
	DocMngBiz biz;

	// 목록(그리드)
	@RequestMapping(value = "/rest/dms/docmng", method = RequestMethod.POST)
	public Page<DocMngGrdVO> findAll(@AuthenticationPrincipal UserDetails user,
			Searchable searchable,
			JqFilter filter,
			Pageable pageable,
			@RequestParam(value = "divisionId", required = false) Code division,
			@RequestParam(value = "systemId", required = false) com.gsitm.devops.dms.model.System system,
			@RequestParam(value = "categoryId", required = false) DocumentCategory category) {
		
		return biz.findAll(user, searchable, filter, pageable, division, system, category);
	}

	// 문서명 검색
	@RequestMapping(value = "/rest/dms/docmng/search", method = RequestMethod.POST)
	public List<DocMngSchVO> search(@AuthenticationPrincipal UserDetails user,
			Searchable searchable,
			@RequestParam(value = "divisionId", required = false) Code division,
			@RequestParam(value = "systemId", required = false) com.gsitm.devops.dms.model.System system,
			@RequestParam(value = "categoryId", required = false) DocumentCategory category) {

		return biz.findAll(user, searchable, division, system, category);
	}


	// 시스템
	@RequestMapping(value = "/rest/dms/docmng/systems", method = RequestMethod.POST)
	public Iterable<System> system(@AuthenticationPrincipal UserDetails user,
			Searchable searchable) {

		return biz.systemFindAll(user, searchable);
	}

	// 문서종류
	@RequestMapping(value = "/rest/dms/docmng/documentcategories", method = RequestMethod.POST)
	public Iterable<DocumentCategory> documentCategory(@AuthenticationPrincipal UserDetails user, Searchable searchable) {

		return biz.documentCategoryFindAll(user, searchable);
	}
	
	// 문서일련번호재동채번
	@RequestMapping(value = "/rest/dms/docmng/getmaxseq/{id}", method = RequestMethod.POST)
	public Long getMaxSeq(
			@PathVariable("id") DocumentCategory category) {
		return biz.getMaxSeq(category);
	}
}
