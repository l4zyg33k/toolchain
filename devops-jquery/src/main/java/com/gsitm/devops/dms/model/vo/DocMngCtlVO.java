package com.gsitm.devops.dms.model.vo;

import java.util.HashMap;
import java.util.Map;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.System;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocMngCtlVO {

	private Iterable<Code> divisions;
	private Iterable<System> systems;
	private Iterable<DocumentCategory> categories;
	private Long ddlDivisionId;
	private Long ddlSystemId;
	private Long ddlCategoryId;
	// Grid 화면의 문서상태 checkbox 저장
	Map<String, Boolean> schStatus = new HashMap<String, Boolean>();
	private String keyword;

	public DocMngCtlVO() {
		super();
	}

	public DocMngCtlVO(Iterable<Code> divisions, Iterable<System> systems,
			Iterable<DocumentCategory> categories, long ddlDivisionId,
			long ddlSystemId, long ddlCategoryId,
			Map<String, Boolean> schStatus, String keyword) {
		super();
		this.divisions = divisions;
		this.ddlDivisionId = ddlDivisionId;
		this.systems = systems;
		this.ddlSystemId = ddlSystemId;
		this.categories = categories;
		this.ddlCategoryId = ddlCategoryId;
		this.schStatus = schStatus;
		this.keyword = keyword;
	}
}
