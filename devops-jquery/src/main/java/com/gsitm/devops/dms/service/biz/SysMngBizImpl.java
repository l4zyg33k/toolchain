package com.gsitm.devops.dms.service.biz;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.dms.model.QDocumentCategory;
import com.gsitm.devops.dms.model.QSystem;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.vo.QSysMngGrdVO;
import com.gsitm.devops.dms.model.vo.SysMngFrmVO;
import com.gsitm.devops.dms.model.vo.SysMngGrdVO;
import com.gsitm.devops.dms.service.DocumentCategoryService;
import com.gsitm.devops.dms.service.SystemService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
@Getter
public class SysMngBizImpl implements SysMngBiz {

	private MessageSource messageSource;
	
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	SystemService systemService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	CodeService codeService;
	
	@Autowired
	DocumentCategoryService documentCategoryService;

	@Override
	public Iterable<System> findAll() {
		return systemService.findAll();
	}

	@Override
	public Iterable<System> findAll(Predicate predicate) {
		return systemService.findAll(predicate);
	}

	@Override
	public Iterable<Code> getDivisions() {
		QCode qCode = QCode.code;
		return codeService.findAll(qCode.parent.value.eq("D1"));
	}

	@Override
	public String create(SysMngFrmVO vo) {

		// Unique Check
		QSystem qSystem = QSystem.system;
		Predicate predicate = qSystem.division.id.eq(vo.getDivision().getId()).and(qSystem.code.eq(vo.getCode()));
		long cnt = systemService.count(predicate);
		if (cnt > 0) {
			return messageSource.getMessage("save.fail", new Object[] {},
					Locale.getDefault());
		}
		
		System system = new System();
		BeanUtils.copyProperties(vo, system);
		systemService.save(system);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String delete(Long id) {

		// 해당 시스템에 속하는 문서종류가 있을 경우 삭제 할 수 없다.
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;
		Predicate predicate = qDocumentCategory.system.id.eq(id);
		long cnt = documentCategoryService.count(predicate);
		
		if (cnt > 0) {
			return messageSource.getMessage("remove.fail", new Object[] {},
					Locale.getDefault());
		}
		
		systemService.delete(id);
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Page<SysMngGrdVO> findAll(Searchable searchable, Pageable pageable, Long term) {
		
		BooleanBuilder builider;
		QSystem qSystem = QSystem.system;

		if (term == null) {
			builider = new BooleanBuilder();
		} else {
			builider = new BooleanBuilder(qSystem.division.id.eq(term));
		}
		
		PathBuilder<System> path = new PathBuilder<System>(System.class, "system");
		JqGrid jqGrid = new JqGrid(path);

		Predicate predicate = builider.and(jqGrid.applyPredicate(searchable));
		
		JPQLQuery query = new JPAQuery(em).from(qSystem)
				.where(predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<SysMngGrdVO> search = query.listResults(new QSysMngGrdVO(qSystem.id, qSystem.code, qSystem.name, qSystem.division.value, qSystem.division.name, qSystem.enabled, qSystem.lastModifiedBy.username, qSystem.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}


	@Override
	public SysMngFrmVO findOne(Long id) {
		SysMngFrmVO vo = new SysMngFrmVO();
		BeanUtils.copyProperties(systemService.findOne(id),vo);
		return vo;
	}

	@Override
	public String update(SysMngFrmVO sysMngFrmVO) {
		
		System system = systemService.findOne(sysMngFrmVO.getId());
		BeanUtils.copyProperties(sysMngFrmVO, system);
		systemService.save(system);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("del", false);
		}
		return navGrid;
	}

}
