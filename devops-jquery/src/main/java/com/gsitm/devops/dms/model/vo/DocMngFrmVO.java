package com.gsitm.devops.dms.model.vo;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class DocMngFrmVO extends DocMngWebVO {

	// Document
	private Long document;
	private String code;
	private String name;
	private Long seq;
	// Document Category
	private Long documentCategory;
	private String documentCategoryCode;
	private String documentCategoryName;
	// System
	private Long system;
	private String systemCode;
	private String systemName;
	// Division(Code)
	private Long division;
	private String divisionName;
	// Document Revision
	private Long documentRevision;
	private String occasion;
	private String contents;
	private String fileName;
	private String filePath;
	private String fileDescription;
	private MultipartFile file;
	private Float version;
	private String approbator;
	private String approbatorName;
	private String sysop;
	private String sysopName;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate approvedDate;
	private Long status;
	private String statusValue;
	private String statusName;
	private String comment;

	public DocMngFrmVO(int page, int rowNum, long rowId, long ddlDivisionId, long ddlSystemId, long ddlCategoryId, String keyword) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
		this.ddlDivisionId = ddlDivisionId;
		this.ddlSystemId = ddlSystemId;
		this.ddlCategoryId = ddlCategoryId;
		this.keyword = keyword;
	}

	@QueryProjection
	public DocMngFrmVO(Long document, String code, String name, Long seq,
			Long documentCategory, String documentCategoryCode,
			String documentCategoryName, Long system, String systemCode,
			String systemName, Long division, String divisionValue,
			String divisionName, Long documentRevision, String occasion,
			String contents, String fileName, String filePath,
			String fileDescription, Float version,
			String approbator, String approbatorName,
			String sysop, String sysopName, LocalDate approvedDate,
			Long status, String statusValue, String statusName, String comment) {
		this.document = document;
		this.code = code;
		this.name = name;
		this.seq = seq;
		this.documentCategory = documentCategory;
		this.documentCategoryCode = documentCategoryCode;
		this.documentCategoryName = documentCategoryName;
		this.system = system;
		this.systemCode = systemCode;
		this.systemName = systemName;
		this.division = division;
		this.divisionName = divisionName;
		this.documentRevision = documentRevision;
		this.occasion = occasion;
		this.contents = contents;
		this.fileName = fileName;
		this.filePath = filePath;
		this.fileDescription = fileDescription;
		this.version = version;
		this.approbator = approbator;
		this.approbatorName = approbatorName;
		this.sysop = sysop;
		this.sysopName = sysopName;
		this.approvedDate = approvedDate;
		this.status = status;
		this.statusValue = statusValue;
		this.statusName = statusName;
		this.comment = comment;
	}

	public DocMngFrmVO() {
		super();
	}
	
	public void setDocumentAll(Document document) {
		this.id = document.getId();
		this.document = document.getId();
		this.code = document.getCode();
		this.name = document.getName();
		this.seq = document.getSeq();
		
		DocumentCategory category = document.getCategory();
		
		this.documentCategory = (category == null) ? null: category.getId();
		this.documentCategoryCode = (category == null) ? null: category.getCode();
		this.documentCategoryName = (category == null) ? null: category.getName();
		this.system = (category == null) ? null : (category.getSystem() == null) ? null : category.getSystem().getId();
		this.systemCode = (category == null) ? null : (category.getSystem() == null) ? null : category.getSystem().getCode();
		this.systemName = (category == null) ? null : (category.getSystem() == null) ? null : category.getSystem().getName();
		this.division = (category == null) ? null : (category.getSystem() == null) ? null : category.getSystem().getDivision().getId();
		this.divisionName = (category == null) ? null : (category.getSystem() == null) ? null : category.getSystem().getDivision().getName();
	}
	
	public void setDocumentRevisionAll(DocumentRevision revision) {
		
		this.documentRevision = revision.getId();
		this.occasion = revision.getOccasion();
		this.contents = revision.getContents();
		this.fileName = revision.getFileName();
		this.filePath = revision.getFilePath();
		this.fileDescription = revision.getFileDescription();
		this.version = revision.getVersion();
		this.sysop = (revision.getSysop() == null) ? null: revision.getSysop().getUsername();
		this.sysopName = (revision.getSysop() == null) ? null: revision.getSysop().getName();
		this.approbator = (revision.getApprobator() == null) ? null: revision.getApprobator().getUsername();
		this.approbatorName = (revision.getApprobator() == null) ? null: revision.getApprobator().getName();
		this.approvedDate = revision.getApprovedDate();
		this.status = (revision.getStatus() == null) ? null: revision.getStatus().getId();
		this.statusName = (revision.getStatus() == null) ? null: revision.getStatus().getName();
		this.statusValue = (revision.getStatus() == null) ? null: revision.getStatus().getValue();
		this.comment = revision.getComment();
	}
}
