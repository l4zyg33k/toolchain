package com.gsitm.devops.dms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class DocMngWebVO extends SharedWebVO<Long> {

	// DropDownList
	protected Long ddlDivisionId;
	protected Long ddlSystemId;
	protected Long ddlCategoryId;
	protected String keyword;

	public String getMethod() {
		return "POST"; // PUT으로 MutiparFile을 받을수없음. isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/dms/docmng" : String.format("/dms/docmng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String
				.format("/dms/docmng?page=%d&rowNum=%d&rowId=%d&ddlDivisionId=%d&ddlSystemId=%d&ddlCategoryId=%d&keyword=%s",
						getPage(), getRowNum(), getRowId(), getDdlDivisionId(),
						getDdlSystemId(), getDdlCategoryId(), getKeyword());
	}

	public String getPageUrl() {
		return String
				.format("page=%d&rowNum=%d&rowId=%d&ddlDivisionId=%d&ddlSystemId=%d&ddlCategoryId=%d&keyword=%s",
						getPage(), getRowNum(), getRowId(), getDdlDivisionId(),
						getDdlSystemId(), getDdlCategoryId(), getKeyword());
	}
}
