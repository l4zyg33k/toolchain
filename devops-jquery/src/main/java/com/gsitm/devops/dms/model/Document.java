/**
 * 운영문서 엔티티
 * Document.java
 * @author Administrator
 */
package com.gsitm.devops.dms.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.mysema.query.annotations.QueryInit;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Document extends AbstractAuditable<Account, Long> {

	/**
	 * 문서 코드
	 */
	private String code;

	/**
	 * 문서 명칭
	 */
	@NotEmpty
	private String name;

	/**
	 * 문서 일련 번호
	 */
	@NotNull
	private Long seq;

	/**
	 * 카테고리
	 */
	@ManyToOne
	@QueryInit("system.division")
	private DocumentCategory category;

	/**
	 * 리비전
	 */
	@OneToMany(mappedBy = "document",fetch = FetchType.EAGER)
	@JsonIgnoreProperties("document")
	@OrderBy("id desc")
	private List<DocumentRevision> revisions;

	/**
	 * 운영 시스템
	 */
	@Transient
	private System system;

	/**
	 * 사업부(코드)
	 */
	@Transient
	private Code division;

	@Override
	public String toString() {
		return this.getId() == null ? "" : this.getId().toString();
	}
}
