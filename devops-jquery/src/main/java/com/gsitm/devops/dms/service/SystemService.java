package com.gsitm.devops.dms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public interface SystemService extends SharedService<com.gsitm.devops.dms.model.System, Long> {

	public Iterable<com.gsitm.devops.dms.model.System> findAll(Predicate predicate,
			OrderSpecifier<?> orderSpecifier);
}
