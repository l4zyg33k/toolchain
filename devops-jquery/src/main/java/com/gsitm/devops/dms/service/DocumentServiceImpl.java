package com.gsitm.devops.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.repository.DocumentRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private DocumentRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Document findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Document findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Document> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Document> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Document> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Document> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Document> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Document> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Document save(Document document) {
		return repository.save(document);
	}

	@Transactional
	@Override
	public <S extends Document> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Document document) {
		repository.delete(document);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Document> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
