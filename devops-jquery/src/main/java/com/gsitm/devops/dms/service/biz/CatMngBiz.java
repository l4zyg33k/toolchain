package com.gsitm.devops.dms.service.biz;

import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.vo.CatMngFrmVO;
import com.gsitm.devops.dms.model.vo.CatMngGrdVO;
import com.mysema.query.types.Predicate;

public interface CatMngBiz extends MessageSourceAware{

	public Iterable<DocumentCategory> findAll();
	public Iterable<DocumentCategory> findAll(Predicate predicate);
	public Page<CatMngGrdVO> findAll(Searchable searchable, Pageable pageable, Long divisionId, Long systemId);
	public Iterable<Code> getDivisions(UserDetails user);
	public String create(CatMngFrmVO vo);
	public String delete(Long id);
	public CatMngFrmVO findOne(Long id);
	public String update(CatMngFrmVO sysMngFrmVO);
	public Map<String, Boolean> getNavGrid(UserDetails user);
	public String save(List<DocumentCategory> documentCategories);
	
}
