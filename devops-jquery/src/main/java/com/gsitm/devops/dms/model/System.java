/**
 * 시스템 엔티티
 * System.java
 * @author Administrator
 */
package com.gsitm.devops.dms.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "code",
		"division_id" }))
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class System extends AbstractAuditable<Account, Long> {

	/**
	 * 시스템 코드
	 */
	@Column(length = 2)
	private String code;

	/**
	 * 시스템 명칭
	 */
	@Column(length = 32)
	private String name;

	/**
	 * 사업부 (코드)
	 */
	@ManyToOne
	private Code division;

	/**
	 * 문서 종류
	 */
	@OneToMany(mappedBy = "system")
	private List<DocumentCategory> documentCategory;
	
	/**
	 * 사용여부
	 */
	private Boolean enabled;

	/**
	 * 승인자
	 */
	@ManyToMany
	private List<Account> approbators;

	/**
	 * 운영자
	 */
	@ManyToMany
	private List<Account> sysops;

	/**
	 * 감사자
	 */
	@ManyToMany
	private List<Account> auditors;

	@Override
	public String toString() {
		return this.getId() == null ? "" : this.getId().toString();
	}
}
