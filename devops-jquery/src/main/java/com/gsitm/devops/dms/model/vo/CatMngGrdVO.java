package com.gsitm.devops.dms.model.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;


// '사업부코드', ' 사업부명', '시스템코드', '시스템명', '문서저장경로','문서종류코드','문서종류명'
@Getter
@Setter
public class CatMngGrdVO {
	private Long id;
	private String code;
	private String name;
	private String divisionValue;
	private String divisionName;
	private String systemCode;
	private String systemName;
	private String repository;
	
	@JsonProperty("lastModifiedBy.name")
	private String lastModifiedByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date lastModifiedDate;
	
	public CatMngGrdVO() {
		super();
	}
	
	@QueryProjection
	public CatMngGrdVO(Long id, String code, String name, String divisionValue, String divisionName, String systemCode, String systemName, String repository, String lastModifiedByName, Date lastModifiedDate) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.divisionValue = divisionValue;
		this.divisionName = divisionName;
		this.systemCode = systemCode;
		this.systemName = systemName;
		this.repository = repository;
		this.lastModifiedByName = lastModifiedByName;
		this.lastModifiedDate = lastModifiedDate;
	}
}
