package com.gsitm.devops.dms.service.biz;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.JqFilter;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.vo.DocMngCmdVO;
import com.gsitm.devops.dms.model.vo.DocMngCtlVO;
import com.gsitm.devops.dms.model.vo.DocMngGrdVO;
import com.gsitm.devops.dms.model.vo.DocMngSchVO;

public interface DocMngBiz extends MessageSourceAware {

	public Page<DocMngGrdVO> findAll(UserDetails user, Searchable searchable, JqFilter filter, Pageable pageable, Code division, System system,
			DocumentCategory category);

	public List<DocMngSchVO> findAll(UserDetails user, Searchable searchable,
			Code division, System system, DocumentCategory category);

	public Iterable<System> systemFindAll(UserDetails user,
			Searchable searchable);

	public Iterable<DocumentCategory> documentCategoryFindAll(UserDetails user,
			Searchable searchable);

	public void mailSend(DocumentRevision revision, List<Account> recipients,
			String keyMessage);

	public void mailSend(DocumentRevision revision, Account recipient, String keyMessage);

	public Map<String, Boolean> getButtons(UserDetails user);

	public Account getAccountByUser(UserDetails user);

	public Map<String, Object> create(UserDetails user, DocMngCmdVO vo)
			throws IllegalStateException, IOException;

	public Map<String, Object> edit(UserDetails user, Document document);

	public Map<String, Object> view(UserDetails user,
			DocumentRevision documentRevision);

	public Map<String, Object> checkOut(UserDetails user, Document document);

	public Map<String, Object> revert(UserDetails user,
			DocumentRevision documentRevision);

	public Map<String, Object> save(UserDetails user, DocMngCmdVO docMngCmdVO)
			throws IllegalStateException, IOException;

	public Map<String, Object> checkin(UserDetails user, DocMngCmdVO docMngCmdVO)
			throws IllegalStateException, IOException;

	public Map<String, Object> approve(UserDetails user, DocMngCmdVO docMngCmdVO);

	public Map<String, Object> delete(UserDetails user,
			DocumentRevision documentRevision);

	public Map<String, Object> reject(UserDetails user, DocMngCmdVO docMngCmdVO);

	public Map<String, Object> deleteRequest(UserDetails user,
			DocMngCmdVO docMngCmdVO);

	public Iterable<Code> getDivisions();

	public Iterable<System> getSystems();

	public Iterable<DocumentCategory> getCategories();
/*
	public DocMngCtlVO getDocMngCtlVO(UserDetails user);

	public DocMngCtlVO getDocMngCtlVO(UserDetails user, Code division);

	public DocMngCtlVO getDocMngCtlVO(UserDetails user, Code division, System system);
*/
	public DocMngCtlVO getDocMngCtlVO(UserDetails user, Code division, System system, DocumentCategory category, Map<String, Boolean> schStatus, String keyword);

	Iterable<Code> getDivisions(UserDetails user);

	Iterable<System> getSystems(UserDetails user, Code division);
	
	public Long getMaxSeq(DocumentCategory category);
}
