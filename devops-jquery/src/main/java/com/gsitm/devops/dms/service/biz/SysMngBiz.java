package com.gsitm.devops.dms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.vo.SysMngFrmVO;
import com.gsitm.devops.dms.model.vo.SysMngGrdVO;
import com.mysema.query.types.Predicate;

public interface SysMngBiz extends MessageSourceAware{

	public Iterable<System> findAll();
	public Iterable<System> findAll(Predicate predicate);
	public Page<SysMngGrdVO> findAll(Searchable searchable, Pageable pageable, Long term);
	public Iterable<Code> getDivisions();
	public String create(SysMngFrmVO vo);
	public String delete(Long id);
	public SysMngFrmVO findOne(Long id);
	public String update(SysMngFrmVO sysMngFrmVO);
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
}
