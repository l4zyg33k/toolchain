package com.gsitm.devops.dms.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.PassportService;
import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.vo.DocMngCmdVO;
import com.gsitm.devops.dms.model.vo.DocMngCtlVO;
import com.gsitm.devops.dms.model.vo.DocMngFrmVO;
import com.gsitm.devops.dms.service.biz.DocMngBiz;

@Controller
public class DocMngController {

	private static final String MENU_CODE = "DocMng";
	private static final String FORM_VO = "docMngFrmVO";
	private static final String CTL_VO = "docMngCtlVO";

	private static final String INDEX_URL = "/dms/docmng";
	private static final String ACTION_URL = "/dms/docmng/{id}";
	private static final String FORM_URL = "/dms/docmng/form";
	private static final String FORM_URL_BY_ID = "/dms/docmng/{id}/view";

	private static final String INDEX_PAGE = "dms/docmng/index";
	private static final String FORM_PAGE = "dms/docmng/form";

	@Autowired
	private DocMngBiz biz;
	
	@Autowired
	private PassportService ps;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(CTL_VO)
	public DocMngCtlVO getDocMngCtlVO(
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "ddlDivisionId", required = false) Code division,
			@RequestParam(value = "ddlSystemId", required = false) System system,
			@RequestParam(value = "ddlCategoryId", required = false) DocumentCategory category,
			@RequestParam(value = "schStatus", required = false) Map<String, Boolean> schStatus,
			@RequestParam(value = "keyword", required = false) String keyword) {

		return biz.getDocMngCtlVO(user, division, system, category, schStatus, keyword);
	}

	@ModelAttribute("buttons")
	public Map<String, Boolean> buttons(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getButtons(user);
	}

	// 목록(뷰)
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	// 작성
	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID})
	public String newbie(
			Model model,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(value = "ddlDivisionId", required = false) Code division,
			@RequestParam(value = "ddlSystemId", required = false) System system,
			@RequestParam(value = "ddlCategoryId", required = false) DocumentCategory category,
			@RequestParam(value = "keyword", required = false) String keyword) {

		Long ddlDivisionId = new Long(division != null ? division.getId() : 0);
		Long ddlSystemId = new Long(system != null ? system.getId() : 0);
		Long ddlCategoryId = new Long(category != null ? category.getId() : 0);

		DocMngFrmVO docMngFrmVO = new DocMngFrmVO(page, rowNum, 0,
				ddlDivisionId, ddlSystemId, ddlCategoryId, keyword);

		if (division != null) {
			docMngFrmVO.setDivision(division.getId());
			docMngFrmVO.setDivisionName(division.getName());
		}
		if (system != null) {
			docMngFrmVO.setSystem(system.getId());
			docMngFrmVO.setSystemCode(system.getCode());
			docMngFrmVO.setSystemName(system.getName());
		}
		if (category != null) {
			docMngFrmVO.setDocumentCategory(category.getId());
			docMngFrmVO.setDocumentCategoryCode(category.getCode());
			docMngFrmVO.setDocumentCategoryName(category.getName());
			docMngFrmVO.setSeq(biz.getMaxSeq(category));
		}

		model.addAttribute(FORM_VO, docMngFrmVO);

		return FORM_PAGE;
	}

	// 등록
	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(Model model,
			@ModelAttribute(FORM_VO) @Valid DocMngCmdVO vo,
			BindingResult result, @AuthenticationPrincipal UserDetails user,
			RedirectAttributes attr) throws IOException {

		if (vo.getFile() == null || vo.getFile().isEmpty()) {
			result.addError(new FieldError("docMngFrmVO", "file", "", false, null, null, "문서파일은 반드시 널(null)이 아니어야 합니다."));
		}
		
		if (result.hasErrors()) {
			return FORM_PAGE;
		}

		Map<String, Object> message = biz.create(user, vo);

		if ((Boolean) message.get("isRedirect")) {
			attr.addFlashAttribute("message", message.get("message"));
		} else {
			model.addAttribute("message", message.get("message"));
		}

		return (String) message.get("return");
	}

	// 최신버전보기
	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET)
	public String edit(
			Model model,
			@PathVariable("id") Document document,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@RequestParam(value = "ddlDivisionId", required = false, defaultValue = "0") long ddlDivisionId,
			@RequestParam(value = "ddlSystemId", required = false, defaultValue = "0") long ddlSystemId,
			@RequestParam(value = "ddlCategoryId", required = false, defaultValue = "0") long ddlCategoryId,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = biz.edit(user, document);

		DocMngFrmVO vo = (DocMngFrmVO) message.get(FORM_VO);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		vo.setDdlDivisionId(ddlDivisionId);
		vo.setDdlSystemId(ddlSystemId);
		vo.setDdlCategoryId(ddlCategoryId);
		vo.setKeyword(keyword);

		if ((Boolean) message.get("isRedirect")) {
			//attr.addAttribute("buttons", message.get("buttons"));
			//attr.addAttribute(FORM_VO, vo);
			//attr.addAttribute("categories", message.get("categories"));
			if (message.get("rejectedRevisionId") != null) {
				attr.addFlashAttribute("rejectedRevisionId",
						message.get("rejectedRevisionId"));
			}
			if (message.get("message") != null) {
				attr.addFlashAttribute("message", message.get("message"));
			}
		} else {
			model.addAttribute("buttons", message.get("buttons"));
			model.addAttribute(FORM_VO, vo);
			model.addAttribute("categories", message.get("categories"));
			if (message.get("rejectedRevisionId") != null) {
				model.addAttribute("rejectedRevisionId",
						message.get("rejectedRevisionId"));
			}
			if (message.get("message") != null) {
				model.addAttribute("message", message.get("message"));
			}
		}

		return (String) message.get("return");
	}

	// 특정 리비전내용보기
	@RequestMapping(value = INDEX_URL + "/revisions/{id}/view", method = {
			RequestMethod.GET, RequestMethod.POST })
	public String view(
			Model model,
			@PathVariable("id") DocumentRevision revision,
			@RequestParam(value = PageParams.PAGE, required = false, defaultValue = "1") int page,
			@RequestParam(value = PageParams.ROW_NUM, required = false, defaultValue = "20") int rowNum,
			@RequestParam(value = PageParams.ROW_ID, required = false, defaultValue = "0") long rowId,
			@RequestParam(value = "ddlDivisionId", required = false, defaultValue = "0") long ddlDivisionId,
			@RequestParam(value = "ddlSystemId", required = false, defaultValue = "0") long ddlSystemId,
			@RequestParam(value = "ddlCategoryId", required = false, defaultValue = "0") long ddlCategoryId,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = biz.view(user, revision);

		DocMngFrmVO vo = (DocMngFrmVO) message.get(FORM_VO);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		vo.setDdlDivisionId(ddlDivisionId);
		vo.setDdlSystemId(ddlSystemId);
		vo.setDdlCategoryId(ddlCategoryId);
		vo.setKeyword(keyword);

		if ((Boolean) message.get("isRedirect")) {
			//attr.addAttribute("buttons", message.get("buttons"));
			//attr.addAttribute(FORM_VO, vo);
			if (message.get("message") != null) {
				attr.addFlashAttribute("message", message.get("message"));
			}
		} else {
			model.addAttribute("buttons", message.get("buttons"));
			model.addAttribute("message", message.get("message"));
			model.addAttribute(FORM_VO, vo);
		}

		return (String) message.get("return");
	}

	// 체크아웃(체크인->체크아웃)
	@RequestMapping(value = ACTION_URL + "/checkout", method = RequestMethod.GET)
	public String checkOut(
			Model model,
			@PathVariable("id") Document document,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@RequestParam(value = "ddlDivisionId", required = false, defaultValue = "0") long ddlDivisionId,
			@RequestParam(value = "ddlSystemId", required = false, defaultValue = "0") long ddlSystemId,
			@RequestParam(value = "ddlCategoryId", required = false, defaultValue = "0") long ddlCategoryId,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = biz.checkOut(user, document);
		attr.addAttribute("page", page);
		attr.addAttribute("rowNum", rowNum);
		attr.addAttribute("rowId", rowId);
		attr.addAttribute("ddlDivisionId", ddlDivisionId);
		attr.addAttribute("ddlSystemId", ddlSystemId);
		attr.addAttribute("ddlCategoryId", ddlCategoryId);
		attr.addAttribute("keyword", keyword);
		attr.addFlashAttribute("message", message.get("message"));

		return (String) message.get("return");
	}

	// 체크아웃 취소(리비전삭제)
	@RequestMapping(value = INDEX_URL + "/revisions/{id}/revert", method = RequestMethod.POST)
	public String revert(
			@PathVariable("id") DocumentRevision revision,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@RequestParam(value = "ddlDivisionId", required = false, defaultValue = "0") long ddlDivisionId,
			@RequestParam(value = "ddlSystemId", required = false, defaultValue = "0") long ddlSystemId,
			@RequestParam(value = "ddlCategoryId", required = false, defaultValue = "0") long ddlCategoryId,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = biz.revert(user, revision);
		attr.addAttribute("page", page);
		attr.addAttribute("rowNum", rowNum);
		attr.addAttribute("rowId", rowId);
		attr.addAttribute("ddlDivisionId", ddlDivisionId);
		attr.addAttribute("ddlSystemId", ddlSystemId);
		attr.addAttribute("ddlCategoryId", ddlCategoryId);
		attr.addAttribute("keyword", keyword);
		attr.addFlashAttribute("message", message.get("message"));

		return ((String) message.get("return"));
	}

	// 임시저장
	@RequestMapping(value = INDEX_URL + "/save", method = RequestMethod.POST)
	public String save(
			@ModelAttribute(FORM_VO) DocMngCmdVO vo,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) throws IllegalStateException,
			IOException {

		Map<String, Object> message = new HashMap<>();

		message = biz.save(user, vo);
		//attr.addAttribute("buttons", message.get("buttons"));
		attr.addFlashAttribute("message", message.get("message"));
		return (String) message.get("return");

	}

	// 검토요청(체크아웃->검토중)
	@RequestMapping(value = INDEX_URL + "/checkin", method = RequestMethod.POST)
	public String checkin(
			Model model,
			@ModelAttribute(FORM_VO) @Valid DocMngCmdVO vo,
			BindingResult result,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) throws IllegalStateException,
			IOException {

		if (vo.getFile() == null || vo.getFile().isEmpty()) {
			result.addError(new FieldError("docMngFrmVO", "file", "", false, null, null, "문서파일은 반드시 널(null)이 아니어야 합니다."));
		}
		
		if (result.hasErrors()) {
			Map<String, Boolean> buttons = new HashMap<>();
			buttons = ps.settingByAccountAndStatus("CO", vo.getSystem(), biz.getAccountByUser(user), biz.getAccountByUser(user));
			//model.addAttribute("docMngFrmVO", vo);
			model.addAttribute("buttons", buttons);
			return FORM_PAGE;
		}
		
		Map<String, Object> message = new HashMap<>();

		message = biz.checkin(user, vo);
		//attr.addAttribute("buttons", message.get("buttons"));
		attr.addFlashAttribute("message", message.get("message"));
		return (String) message.get("return");
	}

	// 승인(검토중->체크인)
	@RequestMapping(value = INDEX_URL + "/approve", method = RequestMethod.POST)
	public String approve(
			@ModelAttribute(FORM_VO) DocMngCmdVO vo,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = new HashMap<>();

		message = biz.approve(user, vo);
		//attr.addAttribute("buttons", message.get("buttons"));
		attr.addFlashAttribute("message", message.get("message"));
		return (String) message.get("return");
	}

	// 삭제요청승인(삭제요청->삭제처리)
	@RequestMapping(value = "/dms/docmng/revisions/{id}/delete", method = RequestMethod.POST)
	public String delete(
			@PathVariable("id") DocumentRevision revision,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@RequestParam(value = "ddlDivisionId", required = false, defaultValue = "0") long ddlDivisionId,
			@RequestParam(value = "ddlSystemId", required = false, defaultValue = "0") long ddlSystemId,
			@RequestParam(value = "ddlCategoryId", required = false, defaultValue = "0") long ddlCategoryId,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = new HashMap<>();

		message = biz.delete(user, revision);
		attr.addAttribute("page", page);
		attr.addAttribute("rowNum", rowNum);
		attr.addAttribute("rowId", rowId);
		attr.addAttribute("ddlDivisionId", ddlDivisionId);
		attr.addAttribute("ddlSystemId", ddlSystemId);
		attr.addAttribute("ddlCategoryId", ddlCategoryId);
		attr.addAttribute("keyword", keyword);
		attr.addFlashAttribute("message", message.get("message"));

		return (String) message.get("return");
	}

	// 반송(검토중->반송)
	@RequestMapping(value = INDEX_URL + "/reject", method = RequestMethod.POST)
	public String reject(
			@ModelAttribute(FORM_VO) DocMngCmdVO vo,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = new HashMap<>();

		message = biz.reject(user, vo);
		//attr.addAttribute("buttons", message.get("buttons"));
		attr.addFlashAttribute("message", message.get("message"));

		return (String) message.get("return");
	}

	// 삭제요청
	@RequestMapping(value = INDEX_URL + "/deleteRequest", method = RequestMethod.POST)
	public String deleteRequest(
			@ModelAttribute(FORM_VO) DocMngCmdVO vo,
			@AuthenticationPrincipal UserDetails user,
			final RedirectAttributes attr) {

		Map<String, Object> message = new HashMap<>();

		message = biz.deleteRequest(user, vo);
		//attr.addAttribute("buttons", message.get("buttons"));
		attr.addFlashAttribute("message", message.get("message"));

		return (String) message.get("return");
	}

}
