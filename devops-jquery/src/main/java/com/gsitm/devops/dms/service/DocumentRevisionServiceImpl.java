package com.gsitm.devops.dms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.dms.model.DocumentRevision;
import com.gsitm.devops.dms.repository.DocumentRevisionRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class DocumentRevisionServiceImpl implements DocumentRevisionService {

	@Autowired
	private DocumentRevisionRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public DocumentRevision findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public DocumentRevision findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<DocumentRevision> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<DocumentRevision> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<DocumentRevision> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<DocumentRevision> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<DocumentRevision> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<DocumentRevision> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public DocumentRevision save(DocumentRevision documentRevision) {
		return repository.save(documentRevision);
	}

	@Transactional
	@Override
	public <S extends DocumentRevision> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(DocumentRevision documentRevision) {
		repository.delete(documentRevision);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends DocumentRevision> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
