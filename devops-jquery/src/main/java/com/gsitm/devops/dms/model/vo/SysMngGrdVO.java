package com.gsitm.devops.dms.model.vo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SysMngGrdVO {

	private Long id;
	private String code;
	private String name;
	private String divisionValue;
	private String divisionName;
	private Boolean enabled;

	@JsonProperty("lastModifiedBy.name")
	private String lastModifiedByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date lastModifiedDate;
	
	public SysMngGrdVO() {
		super();
	}

	@QueryProjection
	public SysMngGrdVO(Long id, String code, String name, String divisionValue, String divisionName, Boolean enabled, String lastModifiedByName, Date lastModifiedDate) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.divisionValue = divisionValue;
		this.divisionName = divisionName;
		this.enabled = enabled;
		this.lastModifiedByName = lastModifiedByName;
		this.lastModifiedDate = lastModifiedDate;
	}
}
