package com.gsitm.devops.dms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.dms.model.DocumentRevision;

@Repository
public interface DocumentRevisionRepository extends
		SharedRepository<DocumentRevision, Long> {
}
