package com.gsitm.devops.dms.model.vo;

import com.gsitm.devops.dms.model.System;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CatMngBatchVO {

	//private Long id;
	private String code;
	private String name;
	private System fromSystem;
	private System toSystem;
	private String rowdata;
	
	public CatMngBatchVO() {
		super();
	}
}
