package com.gsitm.devops.dms.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocMngSchVO {
	private String code;
	private String name;

	public DocMngSchVO() {
		super();
	}
	
	@QueryProjection
	public DocMngSchVO(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
}
