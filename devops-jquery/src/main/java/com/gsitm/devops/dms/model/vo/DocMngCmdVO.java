package com.gsitm.devops.dms.model.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class DocMngCmdVO extends DocMngWebVO {

	// Document
	private Document document;
	private String code;
	@NotBlank
	private String name;
	@NotNull
	private Long seq;
	// Document Category
	@NotNull
	private DocumentCategory documentCategory;
	private String documentCategoryCode;
	private String documentCategoryName;
	// System
	@NotNull
	private com.gsitm.devops.dms.model.System system;
	private String systemCode;
	private String systemName;
	// Division(Code)
	@NotNull
	private Code division;
	private String divisionName;
	// Document Revision
	private DocumentRevision documentRevision;
	@NotBlank
	private String occasion;
	private String contents;
	private String fileName;
	private String filePath;
	@NotBlank
	private String fileDescription;
	private MultipartFile file;
	private Float version;
	private Account approbator;
	private String approbatorName;
	private Account sysop;
	private String sysopName;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate approvedDate;
	private Code status;
	private String statusValue;
	private String statusName;
	private String comment;

	public DocMngCmdVO(int page, int rowNum, long rowId, long ddlDivisionId, long ddlSystemId, long ddlCategoryId, String statusValue) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
		this.ddlDivisionId = ddlDivisionId;
		this.ddlSystemId = ddlSystemId;
		this.ddlCategoryId = ddlCategoryId;
		this.statusValue = statusValue;
	}

	@QueryProjection
	public DocMngCmdVO(Document document, String code, String name, Long seq,
			DocumentCategory documentCategory, String documentCategoryCode,
			String documentCategoryName,
			com.gsitm.devops.dms.model.System system, String systemCode,
			String systemName, Code division, String divisionValue,
			String divisionName, DocumentRevision documentRevision,
			String occasion, String contents, String fileName, String filePath,
			String fileDescription, Float version, Account approbator,
			String approbatorName, Account sysop, String sysopName,
			LocalDate approvedDate, Code status, String statusValue, String statusName,
			String comment) {
		this.document = document;
		this.code = code;
		this.name = name;
		this.seq = seq;
		this.documentCategory = documentCategory;
		this.documentCategoryCode = documentCategoryCode;
		this.documentCategoryName = documentCategoryName;
		this.system = system;
		this.systemCode = systemCode;
		this.systemName = systemName;
		this.division = division;
		this.divisionName = divisionName;
		this.documentRevision = documentRevision;
		this.occasion = occasion;
		this.contents = contents;
		this.fileName = fileName;
		this.filePath = filePath;
		this.fileDescription = fileDescription;
		this.version = version;
		this.approbator = approbator;
		this.approbatorName = approbatorName;
		this.sysop = sysop;
		this.sysopName = sysopName;
		this.approvedDate = approvedDate;
		this.status = status;
		this.statusValue = statusValue;
		this.statusName = statusName;
		this.comment = comment;
	}

	public DocMngCmdVO() {
		super();
	}
}
