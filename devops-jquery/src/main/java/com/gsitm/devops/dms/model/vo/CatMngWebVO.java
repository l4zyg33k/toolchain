package com.gsitm.devops.dms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class CatMngWebVO extends SharedWebVO<Long> {

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/dms/catmng" : String.format("/dms/catmng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/dms/catmng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
}
