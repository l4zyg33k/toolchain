package com.gsitm.devops.dms.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.vo.CatMngGrdVO;
import com.gsitm.devops.dms.service.biz.CatMngBiz;

@RestController
public class CatMngRestController {

	@Autowired
	CatMngBiz biz;

	@RequestMapping(value = "/rest/dms/catmng", method = RequestMethod.POST)
	public Page<CatMngGrdVO> findAll(
			Searchable searchable,
			Pageable pageable,
			@RequestParam(value = "divisionId", required = false) Long divisionId,
			@RequestParam(value = "systemId", required = false) Long systemId) {

		return biz.findAll(searchable, pageable, divisionId, systemId);
	}
	
	@RequestMapping(value = "/rest/dms/catmng/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			@RequestParam("id") long id/*, BindingResult result*/) {
		switch (oper) {
		case "del":
			biz.delete(id);
			break;
		}
	}
	
	/*
	@RequestMapping(value = "/rest/dms/catmng/codes", method = RequestMethod.POST)
	public Page<Code> findByCategories(Searchable searchable, Pageable pageable) {

		Predicate predicate = new BooleanBuilder(predicateFactory
				.getCodePredicate().byParentCode("D3")).and(
				predicateFactory.getCodePredicate().toPredicate(searchable))
				.getValue();

		return serviceFactory.getCodeService().findAll(predicate, pageable);
	}
	*/

}
