package com.gsitm.devops.dms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.dms.model.Document;

@Repository
public interface DocumentRepository extends
		SharedRepository<Document, Long> {
}
