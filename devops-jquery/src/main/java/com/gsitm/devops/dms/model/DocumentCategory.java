/**
 * 문서종류 엔티티
 * DocumentCategory.java
 * @author Administrator
 */
package com.gsitm.devops.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "code",
"system_id" }))
@Getter
@Setter
@SuppressWarnings("serial")
public class DocumentCategory extends AbstractAuditable<Account, Long> {

	/**
	 * 문서 종류 코드
	 */
	@Column(length = 2)
	private String code;

	/**
	 * 문서 종류 명칭
	 */
	@Column(length = 32)
	private String name;

	/**
	 * 저장소
	 */
	@Column(length = 64)
	private String repository;

	/**
	 * 운영 시스템
	 */
	@ManyToOne
	private System system;
	
	@Override
	public String toString() {
		return this.getId() == null ? "" : this.getId().toString();
	}
}
