package com.gsitm.devops.dms.service.biz;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.JqFilter;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.service.MailService;
import com.gsitm.devops.cmm.service.PassportService;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.DocumentRevision;
import com.gsitm.devops.dms.model.QDocument;
import com.gsitm.devops.dms.model.QDocumentCategory;
import com.gsitm.devops.dms.model.QDocumentRevision;
import com.gsitm.devops.dms.model.System;
import com.gsitm.devops.dms.model.QSystem;
import com.gsitm.devops.dms.model.vo.DocMngCmdVO;
import com.gsitm.devops.dms.model.vo.DocMngCtlVO;
import com.gsitm.devops.dms.model.vo.DocMngFrmVO;
import com.gsitm.devops.dms.model.vo.DocMngGrdVO;
import com.gsitm.devops.dms.model.vo.DocMngSchVO;
import com.gsitm.devops.dms.model.vo.QDocMngGrdVO;
import com.gsitm.devops.dms.model.vo.QDocMngSchVO;
import com.gsitm.devops.dms.service.DocumentCategoryService;
import com.gsitm.devops.dms.service.DocumentRevisionService;
import com.gsitm.devops.dms.service.DocumentService;
import com.gsitm.devops.dms.service.SystemService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

import static com.mysema.query.group.GroupBy.groupBy;
import static com.mysema.query.group.GroupBy.list;

@Service
@Setter
@Getter
@Slf4j
public class DocMngBizImpl implements DocMngBiz {

	@Autowired
	MailService mailService;

	@Autowired
	CodeService codeService;

	@Autowired
	AccountService accountService;

	@Autowired
	RoleService authorityService;

	@Autowired
	SystemService systemService;

	@Autowired
	DocumentCategoryService documentCategoryService;

	@Autowired
	PassportService passportService;

	@PersistenceContext
	EntityManager em;

	private MessageSource messageSource;

	@Autowired
	DocumentService documentService;

	@Autowired
	DocumentRevisionService documentRevisionService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// 메일발송
	@Override
	public void mailSend(DocumentRevision revision, List<Account> recipients, String keyMessage) {

		// 메일발송은 Optional
		try {

			ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = sra.getRequest();

			for (Account recipient : recipients) {

				String baseUrl = String.format("%s://%s:%d%s/sso/%s/", request.getScheme(), request.getServerName(),
						request.getServerPort(), request.getContextPath(),
						Base64.getEncoder().encodeToString(recipient.getUsername().getBytes()));
				baseUrl += Base64.getEncoder().encodeToString(
						("/dms/docmng/revisions/" + String.valueOf(revision.getId()) + "/view").getBytes());
				String body = "운영문서 " + revision.getDocument().getCode() + ":" + revision.getDocument().getName() + "가 "
						+ keyMessage + " 되었습니다.<br>" + "문서 확인하기: <a href=\"" + baseUrl + "\">" + baseUrl + "</a>";

				mailService.sendMail("qmsadmin@gsretail.com", recipient.getEmail(),
						"[DevOps] 운영문서 " + keyMessage + " - " + revision.getDocument().getName(), body);
			}
		} catch (Exception e) {
			log.info(String.format("[메일발송 에러] - %s", e.getMessage()));
		}
	}

	// 메일발송
	@Override
	public void mailSend(DocumentRevision revision, Account recipient, String keyMessage) {

		// 메일발송은 Optional
		try {

			ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = sra.getRequest();
			String baseUrl = String.format("%s://%s:%d%s/sso/%s/", request.getScheme(), request.getServerName(),
					request.getServerPort(), request.getContextPath(),
					Base64.getEncoder().encodeToString(recipient.getUsername().getBytes()));
			baseUrl += Base64.getEncoder()
					.encodeToString(("/dms/docmng/revisions/" + String.valueOf(revision.getId()) + "/view").getBytes());

			String body = "운영문서 " + revision.getDocument().getCode() + ":" + revision.getDocument().getName() + "가 "
					+ keyMessage + " 되었습니다.<br>" + "문서 확인하기: <a href=\"" + baseUrl + "\">" + baseUrl + "</a>";

			mailService.sendMail("qmsadmin@gsretail.com", recipient.getEmail(),
					"[DevOps] 운영문서 " + keyMessage + " - " + revision.getDocument().getName(), body);

		} catch (Exception e) {
			log.info(String.format("[메일발송 에러] - %s", e.getMessage()));
		}
	}

	@Override
	public Map<String, Boolean> getButtons(UserDetails user) {

		Account account = this.getAccountByUser(user);

		return passportService.settingByAccountAndStatus("", new System(), account, new Account());
	}

	@Override
	public Map<String, Object> create(UserDetails user, DocMngCmdVO vo) throws IllegalStateException, IOException {

		Map<String, Object> message = new HashMap<>();

		Account account = accountService.findOne(user.getUsername());

		// 해당시스템에 대한 쓰기 권한 여부 체크
		if (!passportService.passport("W", vo.getSystem(), account, new Account())) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			notify.put("return", "redirect:/dms/docmng");
			message.put("message", notify);
			message.put("isRedirect", true);

			return message;
		}

		// 문서코드 생성
		DecimalFormat f4 = new DecimalFormat("#0000");
		String documentCode = vo.getDivision().getValue() + vo.getSystem().getCode()
				+ vo.getDocumentCategory().getCode() + f4.format(vo.getSeq());

		// 문서코드 중복확인
		QDocument qDocument = QDocument.document;
		Document existsDocument = documentService.findOne(qDocument.code.eq(documentCode));

		if (existsDocument != null) {
			log.info(String.format("[중복된 문서] - %s", vo.toString()));
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", "문서코드 '" + documentCode + "'는 중복된 문서입니다.저장할 수 없습니다.");
			message.put("return", "dms/docmng/form");
			message.put("message", notify);
			message.put("isRedirect", false);
			return message;
		}

		// Document 생성
		Document document = new Document();
		document.setCode(documentCode);
		document.setName(vo.getName());
		document.setSeq(vo.getSeq());
		document.setDivision(vo.getDivision());
		document.setSystem(vo.getSystem());
		document.setCategory(vo.getDocumentCategory());

		// Document Revision 생성
		DocumentRevision revision = new DocumentRevision();
		revision.setVersion(1.0f);
		revision.setOccasion(vo.getOccasion());
		revision.setFileDescription(vo.getFileDescription());
		QCode qCode = QCode.code;
		// 검토중 상태로 지정
		revision.setStatus(codeService.findOne(qCode.value.eq("EX").and(qCode.parent.value.eq("D2"))));
		revision.setApprobator(null);
		revision.setSysop(account);

		// 첨부파일 저장
		MultipartFile file = vo.getFile();

		if (file == null) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("save.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", (Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view");
			message.put("isRedirect", true);
			return message;
		}

		if (file != null && !file.isEmpty()) {

			String[] tokens = file.getOriginalFilename().split("\\.(?=[^\\.]+$)");
			String fileName = documentCode + "-" + document.getName() + "(" + String.valueOf(revision.getVersion())
					+ ")." + tokens[1];
			// 산출물 경로
			String outputsRoot = codeService.findOne(qCode.value.eq("U1").and(qCode.parent.value.eq("U1"))).getName();
			String dirPath = "/" + document.getDivision().getValue() + "/" + document.getSystem().getCode() + "/";

			File dir = new File(outputsRoot + dirPath);

			if (!dir.isDirectory()) {
				if (!dir.mkdirs()) {
					log.info(String.format("[디렉터리 생성 에러] - %s", outputsRoot + dirPath));
				}
			}

			String filePath = outputsRoot + dirPath + fileName;

			File destFile = new File(filePath);

			// 파일이 있으면 지우고 저장
			if (destFile.exists()) {
				destFile.delete();
				file.transferTo(destFile);
			} else {
				file.transferTo(destFile);
			}

			revision.setFilePath(dirPath);
			revision.setFileName(fileName);
		}

		// 문서 저장
		documentService.save(document);
		revision.setDocument(document);

		try {
			revision = documentRevisionService.save(revision);
		} catch (Exception e) {
			documentService.delete(document);

			log.info(String.format("[문서이력저장 오류] - %s", revision.toString()));
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("save.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "dms/docmng/form");
			message.put("isRedirect", false);

			return message;
		}

		// 승인자에게 메일전송
		this.mailSend(revision, document.getCategory().getSystem().getApprobators(), "검토요청");

		Map<String, String> notify = new HashMap<>();
		notify.put("information", messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault()));
		message.put("message", notify);
		message.put("return",
				(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view?" + vo.getPageUrl());
		message.put("isRedirect", true);

		return message;
	}

	@Override
	public Account getAccountByUser(UserDetails user) {
		return accountService.findOne(user.getUsername());
	}

	@Override
	public Map<String, Object> edit(UserDetails user, Document document) {

		Map<String, Object> message = new HashMap<>();
		Account account = this.getAccountByUser(user);
		if (!passportService.passport("R", document.getCategory().getSystem(), account, new Account())) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("return", "redirect:/dms/docmng");
			message.put("isRedirect", true);
			return message;
		}

		if (document.getRevisions() == null || document.getRevisions().size() == 0) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));
			message.put("return", "redirect:/dms/docmng");
			message.put("isRedirect", true);
			return message;
		}

		DocMngFrmVO docMngFrmVO = new DocMngFrmVO();
		docMngFrmVO.setDocumentAll(document);

		Map<String, Boolean> buttons = new HashMap<>();
		QCode qCode = QCode.code;
		Code rejectCode = codeService.findOne(qCode.value.eq("RE").and(qCode.parent.value.eq("D2")));

		// 최종 리비전이 반송된 상태이면, 반송되지 않은 리비전 중 최신버전을 보여주고 반송문서 보기를 활성화한다.
		if (document.getRevisions().size() > 1 && document.getRevisions().get(0).getStatus().equals(rejectCode)) {
			for (DocumentRevision revision : document.getRevisions()) {
				if (!revision.getStatus().equals(rejectCode)) {
					docMngFrmVO.setDocumentRevisionAll(revision);
					buttons = passportService.settingByAccountAndStatus(revision.getStatus().getValue(),
							document.getCategory().getSystem(), account, revision.getSysop());
					break;
				}
			}
			buttons.put("btn_rejected", true);
			Map<String, String> notify = new HashMap<>();
			notify.put("information",
					messageSource.getMessage("rejectedDocument.success", new Object[] {}, Locale.getDefault()));
			message.put("rejectedRevisionId", document.getRevisions().get(0).getId());
		} else {
			DocumentRevision revision = document.getRevisions().get(0);
			docMngFrmVO.setDocumentRevisionAll(revision);
			buttons = passportService.settingByAccountAndStatus(revision.getStatus().getValue(),
					document.getCategory().getSystem(), account, revision.getSysop());
		}

		if (docMngFrmVO.getOccasion() != null && docMngFrmVO.getOccasion().equals(" ")) {
			docMngFrmVO.setOccasion("");
		}

		message.put("buttons", buttons);
		message.put("docMngFrmVO", docMngFrmVO);
		message.put("return", "dms/docmng/form");
		message.put("isRedirect", false);

		return message;
	}

	@Override
	public Map<String, Object> view(UserDetails user, DocumentRevision revision) {

		DocMngFrmVO docMngFrmVO = new DocMngFrmVO();
		Map<String, Object> message = new HashMap<>();
		
		if (revision == null || revision.getDocument() == null) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("documentStatus.empty", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("docMngFrmVO", docMngFrmVO);
			message.put("return", "redirect:/dms/docmng");
			message.put("isRedirect", true);
			return message;
		}
		
		Document document = revision.getDocument();
		Account account = this.getAccountByUser(user);

		if (!passportService.passport("R", document.getCategory().getSystem(), account, revision.getSysop())) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("docMngFrmVO", docMngFrmVO);
			message.put("return", "redirect:/dms/docmng");
			message.put("isRedirect", true);
			return message;
		}

		if (document.getRevisions() == null || document.getRevisions().size() == 0) {
			Map<String, String> notify = new HashMap<>();
			notify.put("alert", messageSource.getMessage("documentStatus.empty", new Object[] {}, Locale.getDefault()));
			message.put("docMngFrmVO", docMngFrmVO);
			message.put("return", "redirect:/dms/docmng");
			message.put("isRedirect", true);
			return message;
		}

		docMngFrmVO.setDocumentAll(document);
		docMngFrmVO.setDocumentRevisionAll(revision);

		Map<String, Boolean> buttons = passportService.settingByAccountAndStatus(docMngFrmVO.getStatusValue(),
				document.getCategory().getSystem(), account, revision.getSysop());

		buttons.put("btn_checkined", true);
		message.put("buttons", buttons);

		message.put("docMngFrmVO", docMngFrmVO);
		message.put("return", "dms/docmng/form");
		message.put("isRedirect", false);

		return message;
	}

	@Override
	public Map<String, Object> checkOut(UserDetails user, Document document) {

		Account account = this.getAccountByUser(user);
		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("W", document.getCategory().getSystem(), account, new Account())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng");
			return message;
		}

		QCode qCode = QCode.code;
		QDocumentRevision qDocumentRevision = QDocumentRevision.documentRevision;

		// 체크인, 체크아웃 코드
		Code checkinCode = codeService.findOne(qCode.value.eq("CI").and(qCode.parent.value.eq("D2")));
		Code checkoutCode = codeService.findOne(qCode.value.eq("CO").and(qCode.parent.value.eq("D2")));
		Code deCode = codeService.findOne(qCode.value.eq("DE").and(qCode.parent.value.eq("D2")));

		// 결재중(체크아웃 또는 삭제요청)인 문서는 체크아웃할 수 없음.
		if (documentRevisionService.count(qDocumentRevision.document.eq(document)
				.and(qDocumentRevision.status.eq(checkoutCode).or(qDocumentRevision.status.eq(deCode)))) > 0) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", (Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view");
			return message;
		}

		// 새로운 DocumentRevision 생성
		DocumentRevision revision = new DocumentRevision();
		revision.setDocument(document);
		revision.setSysop(account);
		revision.setStatus(checkoutCode);

		float version = 1.0f;

		for (DocumentRevision rev : document.getRevisions()) {
			if (rev.getStatus().equals(checkinCode)) {
				version = rev.getVersion() + (0.1f);
				break;
			}
		}

		revision.setVersion(version);
		revision.setOccasion(" ");

		// 생성된 DocumentRevision Persist
		documentRevisionService.save(revision);

		notify.put("information", messageSource.getMessage("checkout.success", new Object[] {}, Locale.getDefault()));
		message.put("message", notify);
		message.put("return", "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view");

		return message;
	}

	@Override
	public Map<String, Object> revert(UserDetails user, DocumentRevision revision) {

		Document document = revision.getDocument();

		Account account = this.getAccountByUser(user);
		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("M", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng/");

			return message;
		}

		QCode qCode = QCode.code;

		// 체크아웃 코드
		Code checkoutCode = codeService.findOne(qCode.value.eq("CO").and(qCode.parent.value.eq("D2")));

		// 체크아웃상태에서만 체크아웃 취소 할 수 있음.
		if (!revision.getStatus().equals(checkoutCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng");
			return message;
		}

		// 첨부파일 삭제
		if (revision.getFileName() != null && !revision.getFileName().isEmpty()) {
			File file = new File(revision.getFilePath() + revision.getFileName());
			file.delete();
		}
		// 마지막 리비전 삭제
		documentRevisionService.delete(revision);

		notify.put("information",
				messageSource.getMessage("checkoutCancel.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return", "redirect:/dms/docmng/" + document.getId() + "/view");

		return message;
	}

	@Override
	public Map<String, Object> save(UserDetails user, DocMngCmdVO vo) throws IllegalStateException, IOException {

		DocumentRevision revisionVO = vo.getDocumentRevision();
		DocumentRevision revision = documentRevisionService.findOne(revisionVO.getId());

		Document document = vo.getDocument();

		Account account = this.getAccountByUser(user);
		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("M", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng/?" + vo.getPageUrl());

			return message;
		}

		QCode qCode = QCode.code;

		// 체크아웃 코드
		Code checkoutCode = codeService.findOne(qCode.value.eq("CO").and(qCode.parent.value.eq("D2")));

		// 체크아웃상태에서만 임시저장 할 수 있음.
		if (!revision.getStatus().equals(checkoutCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return",
					(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view/?" + vo.getPageUrl());
			message.put("message", notify);
			return message;
		}

		MultipartFile file = vo.getFile();

		if (file != null && !file.isEmpty()) {
			String[] tokens = file.getOriginalFilename().split("\\.(?=[^\\.]+$)");

			String fileName = document.getCode() + "-" + document.getName() + "("
					+ String.valueOf(revision.getVersion()) + ")." + tokens[1];

			String outputsRoot = codeService.findOne(qCode.value.eq("U1").and(qCode.parent.value.eq("U1"))).getName();

			String dirPath = "/" + document.getCategory().getSystem().getDivision() + "/"
					+ document.getCategory().getSystem().getCode() + "/";

			File dir = new File(outputsRoot + dirPath);

			if (!dir.isDirectory()) {
				if (!dir.mkdirs()) {
					logger.info(String.format("[디렉터리 생성 에러] - %s", outputsRoot + dirPath));
				}
			}

			String filePath = outputsRoot + dirPath + fileName;

			File destFile = new File(filePath);

			if (destFile.exists()) {
				destFile.delete();
				file.transferTo(destFile);
			} else {
				file.transferTo(destFile);
			}

			revision.setFilePath(dirPath);
			revision.setFileName(fileName);
		}

		revision.setOccasion(vo.getOccasion());
		revision.setContents(vo.getContents());
		revision.setSysop(account);
		revision.setFileDescription(vo.getFileDescription());

		documentRevisionService.save(revision);

		notify.put("information", messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return",
				(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view/?" + vo.getPageUrl());

		return message;
	}

	@Override
	public Map<String, Object> checkin(UserDetails user, DocMngCmdVO vo) throws IllegalStateException, IOException {

		DocumentRevision revisionVO = vo.getDocumentRevision();
		DocumentRevision revision = documentRevisionService.findOne(revisionVO.getId());
		Document document = vo.getDocument();

		Account account = this.getAccountByUser(user);
		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("M", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng/?" + vo.getPageUrl());

			return message;
		}

		QCode qCode = QCode.code;

		// 문서상태 코드
		Code checkoutCode = codeService.findOne(qCode.value.eq("CO").and(qCode.parent.value.eq("D2")));
		Code exCode = codeService.findOne(qCode.value.eq("EX").and(qCode.parent.value.eq("D2")));

		// 체크아웃상태에서만 검토요청 할 수 있음.
		if (!revision.getStatus().equals(checkoutCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return",
					(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view/?" + vo.getPageUrl());
			message.put("message", notify);

			return message;
		}

		MultipartFile file = vo.getFile();

		if (file != null && !file.isEmpty()) {

			String[] tokens = file.getOriginalFilename().split("\\.(?=[^\\.]+$)");

			String fileName = document.getCode() + "-" + document.getName() + "("
					+ String.valueOf(revision.getVersion()) + ")." + tokens[1];

			String outputsRoot = codeService.findOne(qCode.value.eq("U1").and(qCode.parent.value.eq("U1"))).getName();

			String dirPath = "/" + document.getCategory().getSystem().getDivision() + "/"
					+ document.getCategory().getSystem().getCode() + "/";

			File dir = new File(outputsRoot + dirPath);

			if (!dir.isDirectory()) {
				if (!dir.mkdirs()) {
					logger.info(String.format("[디렉터리 생성 에러] - %s", outputsRoot + dirPath));
				}
			}

			String filePath = outputsRoot + dirPath + fileName;

			File destFile = new File(filePath);

			file.transferTo(destFile);

			revision.setFilePath(dirPath);
			revision.setFileName(fileName);
		}

		revision.setOccasion(vo.getOccasion());
		revision.setContents(vo.getContents());
		revision.setSysop(account);
		revision.setStatus(exCode);
		documentRevisionService.save(revision);

		// 승인자에게 메일전송
		this.mailSend(revision, document.getCategory().getSystem().getApprobators(), "검토요청");

		notify.put("information", messageSource.getMessage("checkin.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return",
				(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view/?" + vo.getPageUrl());

		return message;
	}

	@Override
	public Map<String, Object> approve(UserDetails user, DocMngCmdVO vo) {

		DocumentRevision revisionVO = vo.getDocumentRevision();
		DocumentRevision revision = documentRevisionService.findOne(revisionVO.getId());
		Document document = vo.getDocument();

		Account account = this.getAccountByUser(user);
		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("X", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng?" + vo.getPageUrl());

			return message;
		}

		QCode qCode = QCode.code;

		// 문서상태 코드
		Code checkinCode = codeService.findOne(qCode.value.eq("CI").and(qCode.parent.value.eq("D2")));
		Code exCode = codeService.findOne(qCode.value.eq("EX").and(qCode.parent.value.eq("D2")));

		// 검토중상태에서만 승인 할 수 있음.
		if (!revision.getStatus().equals(exCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return",
					(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view?" + vo.getPageUrl());
			message.put("message", notify);

			return message;
		}

		revision.setStatus(checkinCode);
		revision.setApprobator(account);
		revision.setComment(vo.getComment());
		revision.setApprovedDate(new LocalDate());
		documentRevisionService.save(revision);

		// 작성자에게 메일전송
		this.mailSend(revision, revision.getSysop(), "승인완료");

		notify.put("information", messageSource.getMessage("approve.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return",
				(Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view?" + vo.getPageUrl());

		return message;
	}

	@Override
	public Map<String, Object> delete(UserDetails user, DocumentRevision revision) {

		Document document = revision.getDocument();
		Account account = this.getAccountByUser(user);

		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("X", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng");

			return message;
		}

		QCode qCode = QCode.code;
		QDocumentRevision qDocumentRevision = QDocumentRevision.documentRevision;

		// 문서상태 코드
		Code deCode = codeService.findOne(qCode.value.eq("DE").and(qCode.parent.value.eq("D2")));

		// 삭제요청상태에서만 승인 할 수 있음.
		if (!revision.getStatus().equals(deCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return", (Object) "redirect:/dms/docmng/" + String.valueOf(document.getId()) + "/view");
			message.put("message", notify);
			return message;
		}

		// 삭제 후 메일전송을 위한 작성자 계정 저장
		Account recipient = revision.getSysop();

		// 남아있는 리비전의 수
		Long revisionCount = documentRevisionService.count(qDocumentRevision.document.eq(document));

		// 첨부파일삭제
		if (revision.getFileName() != null && !revision.getFileName().isEmpty()) {
			File file = new File(revision.getFilePath() + revision.getFileName());
			file.delete();
		}

		documentRevisionService.delete(revision);

		// 남아있는 리비전이 없으면, 모든 리비전 및 첨부파일 삭제
		if (revisionCount < 2) {
			for (DocumentRevision item : documentRevisionService.findAll((qDocumentRevision.document.eq(document)))) {
				if (item.getFileName() != null && !item.getFileName().isEmpty()) {
					File remainingFile = new File(item.getFilePath() + item.getFileName());
					remainingFile.delete();
				}
			}
			documentRevisionService.delete(documentRevisionService.findAll((qDocumentRevision.document.eq(document))));
			documentService.delete(document.getId());
		}

		// 작성자에게 메일전송
		this.mailSend(revision, recipient, "삭제");

		notify.put("information", messageSource.getMessage("remove.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return", "redirect:/dms/docmng");
		return message;
	}

	@Override
	public Map<String, Object> reject(UserDetails user, DocMngCmdVO vo) {
		Document document = vo.getDocument();

		DocumentRevision revision = documentRevisionService.findOne(vo.getDocumentRevision().getId());

		Account account = this.getAccountByUser(user);

		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("X", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng?" + vo.getPageUrl());

			return message;
		}

		QCode qCode = QCode.code;

		// 문서상태 코드
		Code exCode = codeService.findOne(qCode.value.eq("EX").and(qCode.parent.value.eq("D2")));
		Code deCode = codeService.findOne(qCode.value.eq("DE").and(qCode.parent.value.eq("D2")));
		Code rejectCode = codeService.findOne(qCode.value.eq("RE").and(qCode.parent.value.eq("D2")));
		Code checkinCode = codeService.findOne(qCode.value.eq("CI").and(qCode.parent.value.eq("D2")));

		// 결재중 문서(검토중 또는 삭제요청) 상태에서만 반송 할 수 있음.
		if (!revision.getStatus().equals(exCode) && !revision.getStatus().equals(deCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return", (Object) "redirect:/dms/docmng/revisions/" + String.valueOf(revision.getId())
					+ "/view?" + vo.getPageUrl());
			message.put("message", notify);
			return message;
		}

		revision.setApprobator(account);
		revision.setStatus(revision.getStatus().equals(exCode) ? rejectCode : checkinCode);
		revision.setComment(vo.getComment());
		revision.setApprovedDate(new LocalDate());
		documentRevisionService.save(revision);

		// 작성자에게 메일전송
		this.mailSend(revision, revision.getSysop(), "반송");

		notify.put("information", messageSource.getMessage("reject.success", new Object[] {}, Locale.getDefault()));

		message.put("message", notify);
		message.put("return", (Object) "redirect:/dms/docmng/revisions/" + String.valueOf(revision.getId()) + "/view?"
				+ vo.getPageUrl());
		return message;
	}

	@Override
	public Map<String, Object> deleteRequest(UserDetails user, DocMngCmdVO vo) {

		DocumentRevision revisionVO = vo.getDocumentRevision();
		DocumentRevision revision = documentRevisionService.findOne(revisionVO.getId());
		Document document = vo.getDocument();

		Account account = this.getAccountByUser(user);

		Map<String, Object> message = new HashMap<>();
		Map<String, String> notify = new HashMap<>();

		if (!passportService.passport("W", document.getCategory().getSystem(), account, revision.getSysop())) {
			notify.put("alert", messageSource.getMessage("permission.fail", new Object[] {}, Locale.getDefault()));
			message.put("message", notify);
			message.put("return", "redirect:/dms/docmng?" + vo.getPageUrl());

			return message;
		}

		QCode qCode = QCode.code;

		// 체크인, 삭제요청 코드
		Code deCode = codeService.findOne(qCode.value.eq("DE").and(qCode.parent.value.eq("D2")));
		Code checkinCode = codeService.findOne(qCode.value.eq("CI").and(qCode.parent.value.eq("D2")));

		// 체크인상태에서만 삭제요청할수있음.
		if (!revision.getStatus().equals(checkinCode)) {
			notify.put("alert", messageSource.getMessage("documentStatus.fail", new Object[] {}, Locale.getDefault()));

			message.put("return", (Object) "redirect:/dms/docmng/revisions/" + String.valueOf(revision.getId())
					+ "/view?" + vo.getPageUrl());
			message.put("message", notify);
			return message;
		}

		// 승인자에게 메일전송
		this.mailSend(revision, vo.getSystem().getApprobators(), "삭제요청");

		revision.setContents(vo.getContents());
		revision.setStatus(deCode);
		documentRevisionService.save(revision);

		message.put("message", notify);
		message.put("return", (Object) "redirect:/dms/docmng/revisions/" + String.valueOf(revision.getId()) + "/view?"
				+ vo.getPageUrl());
		return message;
	}

	@Override
	public List<DocMngSchVO> findAll(UserDetails user, Searchable searchable, Code division, System system,
			DocumentCategory category) {
		BooleanBuilder builder;
		QDocument qDocument = QDocument.document;
		Account account = accountService.findOne(user.getUsername());

		if (category != null) {
			builder = new BooleanBuilder(qDocument.category.eq(category));
		} else {
			if (system != null) {
				builder = new BooleanBuilder(qDocument.category.system.eq(system));
			} else {
				if (division != null) {
					builder = new BooleanBuilder(qDocument.category.system.division.eq(division));
				} else {
					builder = new BooleanBuilder(qDocument.category.system.division.isNull());
				}
			}
		}

		if (!account.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			builder = builder
					.and(qDocument.category.system.approbators.contains(account).or(qDocument.category.system.sysops
							.contains(account).or(qDocument.category.system.auditors.contains(account))));
		}

		// system.enabled 가 false인것은 제외
		builder = builder.and(qDocument.category.system.enabled.eq(true));

		PathBuilder<Document> path = new PathBuilder<Document>(Document.class, "document");
		JqGrid jqGrid = new JqGrid(path);

		Predicate predicate = builder.and(jqGrid.applyPredicate(searchable));

		List<DocMngSchVO> result = new JPAQuery(em).from(qDocument).where(predicate).limit(10)
				.list(new QDocMngSchVO(qDocument.code, qDocument.name));

		return result;
	}

	@Override
	public Page<DocMngGrdVO> findAll(UserDetails user, Searchable searchable, JqFilter filter, Pageable pageable,
			Code division, System system, DocumentCategory category) {

		BooleanBuilder builder;
		QDocument qDocument = QDocument.document;
		QSystem qSystem = QSystem.system;
		QDocumentCategory qCategory = new QDocumentCategory("qCategory");
		QCode qDivision = new QCode("qDivision");
		QDocumentRevision qRevision = new QDocumentRevision("qRevision");

		if (category != null) {
			builder = new BooleanBuilder(qRevision.document.category.eq(category));
		} else {
			if (system != null) {
				builder = new BooleanBuilder(qRevision.document.category.system.eq(system));
			} else {
				if (division != null) {
					builder = new BooleanBuilder(qRevision.document.category.system.division.eq(division));
				} else {
					builder = new BooleanBuilder(qRevision.document.category.system.division.isNull());
				}
			}
		}

		Account account = accountService.findOne(user.getUsername());

		if (!account.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			builder = builder.and(qRevision.document.category.system.approbators.contains(account)
					.or(qRevision.document.category.system.sysops.contains(account)
							.or(qRevision.document.category.system.auditors.contains(account))));
		}

		// system.enabled 가 false인것은 제외
		builder = builder.and(qRevision.document.category.system.enabled.eq(true));
		
		if (searchable.getSearchString() != null && searchable.getSearchString() != "") {
		// 옴니서치
		builder = builder.and(qRevision.document.name.contains(searchable.getSearchString())
				.or(qRevision.document.revisions.any().occasion.contains(searchable.getSearchString()))
				.or(qRevision.document.revisions.any().sysop.name.contains(searchable.getSearchString()))
				.or(qRevision.document.revisions.any().approbator.name.contains(searchable.getSearchString())));
		}
		
		PathBuilder<DocumentRevision> path = new PathBuilder<DocumentRevision>(DocumentRevision.class, "qRevision");
		JqGrid jqGrid = new JqGrid(path);
		Predicate predicate = builder.and(jqGrid.applyPredicate(filter));

		JPQLQuery totalQuery = new JPAQuery(em).from(qRevision).innerJoin(qRevision.document, qDocument)
				.innerJoin(qRevision.document.category, qCategory)
				.innerJoin(qRevision.document.category.system, qSystem)
				.innerJoin(qRevision.document.category.system.division, qDivision).where(predicate);

		// 운영문서 개수
		int total = totalQuery.groupBy(qDocument).distinct().list(qDocument.id).size();

		JPQLQuery query = new JPAQuery(em).from(qRevision).innerJoin(qRevision.document, qDocument)
				.innerJoin(qRevision.document.category, qCategory)
				.innerJoin(qRevision.document.category.system, qSystem)
				.innerJoin(qRevision.document.category.system.division, qDivision).where(predicate);

		// 리비전을 포함한 실제 개수를 구하고, 최종쿼리의 offset과 limit를 구한다.
		// offset 구하기, 그룹바이 운영문서에 pageable의 offset을 limit로 하고 리비전 총 개수를 구하여 최종
		// offset으로 한다.
		Long offset = new Long(0);

		if (pageable.getOffset() != 0) {
			List<Long> offsetTuple = query.groupBy(qDocument.id).limit(pageable.getOffset() + 1).list(qRevision.count());

			// count 된 각 Row를 더하되, count가 0일 땐(리비전이 없는 경우) 1을 더해준다.
			for (Long l : offsetTuple) {
				offset += (l == 0l) ? 1l : l;
			}
		} else {
			offset = 0l;
		}

		// limit 구하기, 그룹바이 운영문서에 pageable의 offset과 limit를 적용한 후에 리지전 총 개수를 구하여
		// 최종 limit로 한다.
		List<Long> limitTuple = query.groupBy(qDocument.id).offset(pageable.getOffset() + 1).limit(pageable.getPageSize()+1)
				.list(qRevision.count());

		// count 된 각 Row를 더하되, count가 0일 땐(리비전이 없는 경우) 1을 더해준다.
		Long limit = new Long(0);
		for (Long l : limitTuple) {
			limit += (l == 0l) ? 1l : l;
		}

		if (limit == 0l) {
			limit = (long) pageable.getPageSize();
		}

		// offset과 limit를 적용
		query.offset(offset).limit(limit).groupBy(qRevision).orderBy(qDocument.id.asc(), qRevision.id.desc());

		Map<Long, DocMngGrdVO> results = query.transform(groupBy(qDocument.id).as(new QDocMngGrdVO(qDocument.id,
				qDocument.code, qDocument.name, qDivision.name, qSystem.name, qCategory.name, list(qRevision))));
		List<DocMngGrdVO> DocMngGrdVOs = new ArrayList<DocMngGrdVO>(results.values());

		return new PageImpl<>(DocMngGrdVOs, pageable, total);
	}

	@Override
	public Iterable<System> systemFindAll(UserDetails user, Searchable searchable) {
		QSystem qSystem = QSystem.system;
		BooleanBuilder builder = getSystem(user, qSystem);
		PathBuilder<System> path = new PathBuilder<System>(System.class, "system");
		JqGrid jqGrid = new JqGrid(path);
		Predicate predicate = builder.and(jqGrid.applyPredicate(searchable, "Long"));

		OrderSpecifier<String> sort = qSystem.name.asc();

		return systemService.findAll(predicate, sort);
	}

	@Override
	public Iterable<DocumentCategory> documentCategoryFindAll(UserDetails user, Searchable searchable) {
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;

		BooleanBuilder builder = getSystem(user, qDocumentCategory.system);
		PathBuilder<DocumentCategory> path = new PathBuilder<DocumentCategory>(DocumentCategory.class,
				"documentCategory");
		JqGrid jqGrid = new JqGrid(path);
		Predicate predicate = builder.and(jqGrid.applyPredicate(searchable, "Long"));

		OrderSpecifier<String> sort = qDocumentCategory.name.asc();

		return documentCategoryService.findAll(predicate, sort);
	}

	@Override
	public Iterable<Code> getDivisions() {
		QCode qCode = QCode.code;
		return codeService.findAll(qCode.parent.value.eq("D1").and(qCode.enabled.eq(true)), qCode.name.asc());
	}

	@Override
	public Iterable<System> getSystems() {
		QSystem qSystem = QSystem.system;
		return systemService.findAll(
				qSystem.division.id.eq(this.getDivisions().iterator().next().getId()).and(qSystem.enabled.eq(true)),
				qSystem.name.asc());
	}

	private BooleanBuilder getSystem(UserDetails user, QSystem qSystem) {
		Account account = accountService.findOne(user.getUsername());
		BooleanBuilder builder = new BooleanBuilder();
		if (!account.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			builder = builder.and(qSystem.enabled.eq(true).and(qSystem.approbators.contains(account)
					.or(qSystem.sysops.contains(account).or(qSystem.auditors.contains(account)))));
		}
		return builder;
	}

	@Override
	public Iterable<DocumentCategory> getCategories() {
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;
		List<DocumentCategory> categories = (List<DocumentCategory>) documentCategoryService.findAll(
				qDocumentCategory.system.id.eq(getSystems().iterator().next().getId()), qDocumentCategory.name.asc());
		DocumentCategory category = new DocumentCategory();
		category.setName("전체");
		categories.add(0, category);

		return categories;
	}

	@Override
	public DocMngCtlVO getDocMngCtlVO(UserDetails user, Code division, System system, DocumentCategory category,
			Map<String, Boolean> schStatus, String keyword) {
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;
		Iterable<Code> divisions = getDivisions(user);
		List<System> systems = new ArrayList<System>();
		List<DocumentCategory> categories = new ArrayList<DocumentCategory>();

		if (system != null) {
			systems = (List<System>) getSystems(user, system.getDivision());
			categories = (List<DocumentCategory>) documentCategoryService
					.findAll(qDocumentCategory.system.id.eq(system.getId()), qDocumentCategory.name.asc());
		} else {
			if (division != null) {
				systems = (List<System>) getSystems(user, division);
			} else {
				systems = (List<System>) getSystems(user, divisions.iterator().next());
			}
		}
		System systemAll = new System();
		systemAll.setName("전체");
		systems.add(0, systemAll);
		DocumentCategory categoryAll = new DocumentCategory();
		categoryAll.setName("전체");
		categories.add(0, categoryAll);

		Long ddlDivisionId = new Long(division != null ? division.getId() : 0);
		Long ddlSystemId = new Long(system != null ? system.getId() : 0);
		Long ddlCategoryId = new Long(category != null ? category.getId() : 0);

		DocMngCtlVO docMngCtlVO = new DocMngCtlVO(divisions, systems, categories, ddlDivisionId, ddlSystemId,
				ddlCategoryId, schStatus, keyword);

		return docMngCtlVO;
	}

	@Override
	public Iterable<Code> getDivisions(UserDetails user) {
		Account account = accountService.findOne(user.getUsername());

		if (account.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			QCode qCode = QCode.code;
			return codeService.findAll(qCode.parent.value.eq("D1").and(qCode.enabled.eq(true)), qCode.name.asc());
		} else {
			QSystem qSystem = QSystem.system;
			JPQLQuery query = new JPAQuery(em);
			return query.distinct().from(qSystem)
					.where(qSystem.enabled.eq(true)
							.and(qSystem.approbators.contains(account)
									.or(qSystem.sysops.contains(account).or(qSystem.auditors.contains(account))))
					.and(qSystem.division.enabled.eq(true))).orderBy(qSystem.division.name.asc())
					.list(qSystem.division);
		}
	}

	@Override
	public Iterable<System> getSystems(UserDetails user, Code division) {
		Account account = accountService.findOne(user.getUsername());

		if (account.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			QSystem qSystem = QSystem.system;
			return systemService.findAll(qSystem.division.id.eq(division.getId()).and(qSystem.enabled.eq(true)),
					qSystem.name.asc());
		} else {
			QSystem qSystem = QSystem.system;
			JPQLQuery query = new JPAQuery(em);
			return query.from(qSystem)
					.where(qSystem.division.id.eq(division.getId())
							.and(qSystem.approbators.contains(account)
									.or(qSystem.sysops.contains(account).or(qSystem.auditors.contains(account))))
					.and(qSystem.enabled.eq(true))).orderBy(qSystem.name.asc()).list(qSystem);
		}
	}

	@Override
	public Long getMaxSeq(DocumentCategory category) {
		QDocument qDocument = QDocument.document;
		JPQLQuery query = new JPAQuery(em);
		return query.from(qDocument).where(qDocument.category.eq(category))
				.singleResult(qDocument.seq.max().coalesce(0l)) + 1l;
	}

}
