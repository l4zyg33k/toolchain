package com.gsitm.devops.dms.service.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.dms.model.QDocument;
import com.gsitm.devops.dms.model.QDocumentCategory;
import com.gsitm.devops.dms.model.QSystem;
import com.gsitm.devops.dms.model.DocumentCategory;
import com.gsitm.devops.dms.model.vo.QCatMngGrdVO;
import com.gsitm.devops.dms.model.vo.CatMngFrmVO;
import com.gsitm.devops.dms.model.vo.CatMngGrdVO;
import com.gsitm.devops.dms.service.DocumentCategoryService;
import com.gsitm.devops.dms.service.DocumentService;
import com.gsitm.devops.dms.service.SystemService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
@Getter
public class CatMngBizImpl implements CatMngBiz {

	private MessageSource messageSource;
	
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	DocumentCategoryService documentCategoryService;
	
	@Autowired
	DocumentService documentService;
	
	@Autowired
	CodeService codeService;
	
	@Autowired
	SystemService systemService;
	
	@Autowired
	AccountService accountService;

	@Override
	public Iterable<DocumentCategory> findAll() {
		return documentCategoryService.findAll();
	}

	@Override
	public Iterable<DocumentCategory> findAll(Predicate predicate) {
		return documentCategoryService.findAll(predicate);
	}

	@Override
	public Iterable<Code> getDivisions(UserDetails user) {

		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		
		if (authorities.contains(Roles.ROLE_ADMIN) || authorities.contains(Roles.ROLE_IL)) {
			QCode qCode = QCode.code;
            return codeService.findAll(qCode.parent.value.eq("D1"));
		} else {

			QSystem qSystem = QSystem.system;
	    	JPQLQuery query = new JPAQuery(em);
	    	
	    	Account account = accountService.findOne(user.getUsername());
	    	
	        return query.distinct()
	                .from(qSystem)
	                .where(qSystem.approbators.contains(account)
	                        .or(qSystem.sysops.contains(account))
	                        .or(qSystem.auditors.contains(account)))
	                .list(qSystem.division);
		}
	}

	@Override
	public String create(CatMngFrmVO vo) {

		// Unique Check
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;
		Predicate predicate = qDocumentCategory.system.id.eq(vo.getSystem().getId()).and(qDocumentCategory.code.eq(vo.getCode()));
		long cnt = documentCategoryService.count(predicate);
		if (cnt > 0) {
			return messageSource.getMessage("save.fail", new Object[] {},
					Locale.getDefault());
		}
		
		DocumentCategory documentCategory = new DocumentCategory();
		BeanUtils.copyProperties(vo, documentCategory);
		documentCategoryService.save(documentCategory);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String delete(Long id) {

		// 해당 시스템에 속하는 문서종류가 있을 경우 삭제 할 수 없다.
		QDocument qDocument = QDocument.document;
		Predicate predicate = qDocument.category.id.eq(id);
		long cnt = documentService.count(predicate);
		
		if (cnt > 0) {
			return messageSource.getMessage("remove.fail", new Object[] {},
					Locale.getDefault());
		}
		
		documentCategoryService.delete(id);
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Page<CatMngGrdVO> findAll(Searchable searchable, Pageable pageable, Long divisionId, Long systemId) {
		
		BooleanBuilder builider = new BooleanBuilder();
		QDocumentCategory qDocumentCategory = QDocumentCategory.documentCategory;

		if (divisionId != null) {
			builider = builider.and(qDocumentCategory.system.division.id.eq(divisionId));
		}
		
		if (systemId != null) {
			builider = builider.and(qDocumentCategory.system.id.eq(systemId));
		}
		
		PathBuilder<DocumentCategory> path = new PathBuilder<DocumentCategory>(DocumentCategory.class, "documentCategory");
		JqGrid jqGrid = new JqGrid(path);

		Predicate predicate = builider.and(jqGrid.applyPredicate(searchable));
		
		JPQLQuery query = new JPAQuery(em).from(qDocumentCategory)
				.where(predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<CatMngGrdVO> search = query.listResults(new QCatMngGrdVO(qDocumentCategory.id, qDocumentCategory.code, qDocumentCategory.name, qDocumentCategory.system.division.value, qDocumentCategory.system.division.name, qDocumentCategory.system.code, qDocumentCategory.system.name, qDocumentCategory.repository, qDocumentCategory.lastModifiedBy.username, qDocumentCategory.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public CatMngFrmVO findOne(Long id) {
		CatMngFrmVO vo = new CatMngFrmVO();
		BeanUtils.copyProperties(documentCategoryService.findOne(id),vo);
		return vo;
	}

	@Override
	public String update(CatMngFrmVO vo) {
		DocumentCategory documentCategory = documentCategoryService.findOne(vo.getId());
		vo.setSystem(documentCategory.getSystem());
		BeanUtils.copyProperties(vo, documentCategory);
		documentCategoryService.save(documentCategory);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public String save(List<DocumentCategory> documentCategories) {
		documentCategoryService.save(documentCategories);
		return null;
	}

}
