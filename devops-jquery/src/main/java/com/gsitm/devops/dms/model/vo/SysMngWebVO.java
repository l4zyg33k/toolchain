package com.gsitm.devops.dms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class SysMngWebVO extends SharedWebVO<Long> {

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/dms/sysmng" : String.format("/dms/sysmng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/dms/sysmng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
}
