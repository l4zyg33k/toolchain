package com.gsitm.devops.dms.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.dms.model.vo.SysMngFrmVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.dms.service.biz.SysMngBiz;

@Controller
public class SysMngController {

	private static final String MENU_CODE = "SysMng";
	private static final String FORM_VO = "sysMngFrmVO";

	private static final String INDEX_URL = "/dms/sysmng";
	private static final String ACTION_URL = "/dms/sysmng/{id}";
	private static final String FORM_URL = "/dms/sysmng/form";
	private static final String FORM_URL_BY_ID = "/dms/sysmng/{id}/form";
	private static final String REDIRECT_URL = "redirect:/dms/sysmng?page={page}&rowNum={rowNum}&rowId={rowId}";
	private static final String REDIRECT_FORM_URL = "redirect:/dms/sysmng/{id}/form?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "dms/sysmng/index";
	private static final String FORM_PAGE = "dms/sysmng/form";

	@Autowired
	SysMngBiz biz;

	@ModelAttribute("menuCode")
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute("divisions")
	public Iterable<Code> getDivisions() {
		return biz.getDivisions();
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String newbie(Model model, @RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum)
			throws JsonProcessingException {

		model.addAttribute(FORM_VO, new SysMngFrmVO(page, rowNum, /*rowId*/0));

		return FORM_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(Model model,
			@ModelAttribute(FORM_VO) @Valid SysMngFrmVO vo,
			BindingResult result, RedirectAttributes attr) throws IOException {

		if (result.hasErrors()) {
			return FORM_PAGE;
		}

		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());

		return REDIRECT_URL;
	}

	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(Model model, @PathVariable("id") Long id,
			@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId) {

		SysMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);

		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@PathVariable("id") Long id,
			@ModelAttribute(FORM_VO) @Valid SysMngFrmVO vo,
			BindingResult result, RedirectAttributes attr) throws IOException {

		if (result.hasErrors()) {
			return FORM_PAGE;
		}

		vo.setId(id);
		String returnMsg = biz.update(vo);
		Map<String, String> message = new HashMap<String, String>();
		message.put("information", returnMsg);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		attr.addFlashAttribute("message", message);
		
		return String.format(REDIRECT_FORM_URL, vo.getId());
	}
}
