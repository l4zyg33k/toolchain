package com.gsitm.devops.dms.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.dms.model.vo.SysMngGrdVO;
import com.gsitm.devops.dms.service.biz.SysMngBiz;

@RestController
public class SysMngRestController {

	@Autowired
	SysMngBiz biz;
	
	@RequestMapping(value = "/rest/dms/sysmng", method = RequestMethod.POST)
	public Page<SysMngGrdVO> findAll(
			Searchable searchable, Pageable pageable,
			@RequestParam(value = "query", required = false) Long term) {

		return biz.findAll(searchable, pageable, term);
	}
	
	@RequestMapping(value = "/rest/dms/sysmng/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			@RequestParam("id") long id/*, BindingResult result*/) {
		switch (oper) {
		case "del":
			biz.delete(id);
			break;
		}
	}
}
