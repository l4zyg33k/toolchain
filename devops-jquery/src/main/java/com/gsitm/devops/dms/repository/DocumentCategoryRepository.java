package com.gsitm.devops.dms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.dms.model.DocumentCategory;

@Repository
public interface DocumentCategoryRepository extends
		SharedRepository<DocumentCategory, Long> {
}
