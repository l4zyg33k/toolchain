package com.gsitm.devops.dms.model.vo;

import javax.validation.Valid;

import lombok.Data;

import com.gsitm.devops.dms.model.Document;
import com.gsitm.devops.dms.model.DocumentRevision;

@Data
public class DocumentAndDocumentRevision {

	@Valid
	private Document document;
	@Valid
	private DocumentRevision documentRevision;
}
