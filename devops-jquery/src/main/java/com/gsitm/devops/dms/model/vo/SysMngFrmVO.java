package com.gsitm.devops.dms.model.vo;

import java.util.List;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class SysMngFrmVO extends SysMngWebVO {

	//private Long id;
	private String code;
	private String name;
	private Code division;
	private Boolean enabled;
	private List<Account> approbators;
	private List<Account> sysops;
	private List<Account> auditors;
	
	public SysMngFrmVO(int page, int rowNum, long rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}
	
	public SysMngFrmVO() {
		super();
	}
}
