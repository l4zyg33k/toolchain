package com.gsitm.devops.dms.model.vo;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.dms.model.System;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class CatMngFrmVO extends CatMngWebVO {

	//private Long id;
	private String code;
	private String name;
	private System system;
	private String repository;
	
	public CatMngFrmVO(int page, int rowNum, long rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
		this.system = new System();
		this.system.setDivision(new Code());
	}
	
	public CatMngFrmVO() {
		super();
	}
}
