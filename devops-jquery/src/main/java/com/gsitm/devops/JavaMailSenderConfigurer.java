package com.gsitm.devops;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Profile("smtp-prod")
@Configuration
public class JavaMailSenderConfigurer {

	@Bean(name = "mailSender")
	public JavaMailSender configureJavaMailSender() {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost("localhost");
		return sender;
	}
}
