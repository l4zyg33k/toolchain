package com.gsitm.devops.slm.service.biz;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.slm.model.AnnualComponentWeightId;
import com.gsitm.devops.slm.model.vo.CompWeigMngCmdVO;
import com.gsitm.devops.slm.model.vo.CompWeigMngGrdVO;

public interface AnnuCompWeigBiz {

	void createAnnuCompWeig(AnnualComponentWeightId id, CompWeigMngCmdVO vo);

	void updateAnnuCompWeig(AnnualComponentWeightId id, CompWeigMngCmdVO vo);

	void removeAnnuCompWeig(AnnualComponentWeightId id);

	Page<CompWeigMngGrdVO> findAllAnnuCompWeig(Integer year, Pageable pageable);
}
