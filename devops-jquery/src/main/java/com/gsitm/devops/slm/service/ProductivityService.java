package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.Productivity;

public interface ProductivityService extends
		SharedService<Productivity, Long> {
}
