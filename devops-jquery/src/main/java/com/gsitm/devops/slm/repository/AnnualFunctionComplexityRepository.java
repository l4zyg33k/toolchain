package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.AnnualFunctionComplexity;
import com.gsitm.devops.slm.model.AnnualFunctionComplexityId;

@Repository
public interface AnnualFunctionComplexityRepository
		extends SharedRepository<AnnualFunctionComplexity, AnnualFunctionComplexityId> {

}
