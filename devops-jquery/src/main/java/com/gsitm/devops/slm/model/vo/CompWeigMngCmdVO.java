package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import javax.validation.constraints.Digits;

import lombok.Data;

@Data
public class CompWeigMngCmdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Digits(integer = 2, fraction = 2)
	private Float ei;

	@Digits(integer = 2, fraction = 2)
	private Float eo;

	@Digits(integer = 2, fraction = 2)
	private Float eq;

	@Digits(integer = 2, fraction = 2)
	private Float ilf;

	@Digits(integer = 2, fraction = 2)
	private Float eif;
}
