package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.ArgMng;

@Repository
public interface ArgMngRepository extends
		SharedRepository<ArgMng, Long> {
}
