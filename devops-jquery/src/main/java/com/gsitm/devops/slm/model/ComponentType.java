package com.gsitm.devops.slm.model;

public enum ComponentType {
	EI, EO, EQ, ILF, EIF
}
