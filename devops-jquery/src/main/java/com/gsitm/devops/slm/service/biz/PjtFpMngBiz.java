package com.gsitm.devops.slm.service.biz;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;

import com.gsitm.devops.slm.model.vo.PjtFpRstCmdVO;
import com.gsitm.devops.slm.model.vo.PjtFpMngFrmVO;
import com.gsitm.devops.slm.model.vo.PjtFpMngGrdVO;

public interface PjtFpMngBiz {

	public void createPjtFpRst(PjtFpRstCmdVO vo);

	public Page<PjtFpMngGrdVO> findAllPjtFpRst(Pageable pageable);

	public PjtFpMngFrmVO findOne(Long id);

	public Map<String, Boolean> getNavGrid(@AuthenticationPrincipal UserDetails user);

	public String getPartVals();

	public String getSystemVals();

	public String getYearsVals();

	public void removePjtFpRst(Long id);

	public void updatePjtFpRst(Long id, PjtFpRstCmdVO vo);
}
