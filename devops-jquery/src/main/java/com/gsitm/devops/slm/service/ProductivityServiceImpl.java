package com.gsitm.devops.slm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.Productivity;
import com.gsitm.devops.slm.repository.ProductivityRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProductivityServiceImpl implements ProductivityService {

	@Autowired
	private ProductivityRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Autowired
	private CodeService codeService;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Productivity findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Productivity findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Productivity> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Productivity> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Productivity> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Productivity> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Productivity> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Productivity> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Productivity save(Productivity productivity) {
		return repository.save(productivity);
	}

	@Transactional
	@Override
	public <S extends Productivity> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Productivity productivity) {
		repository.delete(productivity);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Productivity> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
