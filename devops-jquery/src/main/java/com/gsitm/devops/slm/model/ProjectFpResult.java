package com.gsitm.devops.slm.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectFpResult extends AbstractAuditable<Account, Long> {

	/**
	 * 기준 년도 (코드)
	 */
	@ManyToOne
	private Code year;

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * CSR 파트 (코드)
	 */
	@ManyToOne
	private Code part;

	/**
	 * 처리자
	 */
	@ManyToOne
	private Account op;

	/**
	 * 시스템 특성 (코드)
	 */
	@ManyToOne
	private Code system;

	/**
	 * 최종 기능 점수
	 */
	private Float fp;

	/**
	 * 예상 투입 MH
	 */
	private Float mh;

	/**
	 * 프로그램 기능 점수
	 */
	@OneToMany(mappedBy = "projectFpResult", orphanRemoval = true)
	private List<ProgramFpResult> programs;
}
