package com.gsitm.devops.slm.controller;

import java.util.List;

import org.apache.commons.lang3.EnumUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.vo.FuncCompWebVO;

@Controller
public class FuncCompController {

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return "FuncCompMng";
	}

	@ModelAttribute("years")
	public List<Integer> getYears() {
		int year = new DateTime().getYear();
		return ContiguousSet.create(Range.open(year - 5, year + 5), DiscreteDomain.integers()).asList();
	}
	
	@ModelAttribute("components")
	public List<ComponentType> getComponentTypes() {
		return EnumUtils.getEnumList(ComponentType.class);
	}
	
	@RequestMapping("/slm/funccompmng")
	public String index(Model model) {
		FuncCompWebVO funcCompWebVO = new FuncCompWebVO();
		funcCompWebVO.setYear(new DateTime().getYear());
		funcCompWebVO.setComponent(ComponentType.EI);
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");		
		model.addAttribute("funcCompWebVO", funcCompWebVO);
		return "slm/funccompmng/index";
	}	
}
