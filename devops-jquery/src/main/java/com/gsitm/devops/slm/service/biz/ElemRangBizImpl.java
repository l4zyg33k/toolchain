package com.gsitm.devops.slm.service.biz;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.slm.model.AnnualElementRange;
import com.gsitm.devops.slm.model.AnnualElementRangeId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.QAnnualElementRange;
import com.gsitm.devops.slm.model.vo.ElemRangGrdVO;
import com.gsitm.devops.slm.model.vo.ElemRangMngCmdVO;
import com.gsitm.devops.slm.model.vo.QElemRangGrdVO;
import com.gsitm.devops.slm.repository.AnnualElementRangeRepository;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
public class ElemRangBizImpl implements ElemRangBiz {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AnnualElementRangeRepository repository;

	@Transactional
	@Override
	public void createElemRang(AnnualElementRangeId id, ElemRangMngCmdVO vo) throws DataAccessException {
		AnnualElementRange entity = new AnnualElementRange();
		entity.setId(id);
		BeanUtils.copyProperties(vo, entity);
		em.persist(entity);
	}

	@Transactional
	@Override
	public void updateElemRang(AnnualElementRangeId id, ElemRangMngCmdVO vo) throws DataAccessException {
		AnnualElementRange entity = repository.findOne(id);
		BeanUtils.copyProperties(vo, entity);
		repository.save(entity);
	}

	@Transactional
	@Override
	public void removeElemRang(AnnualElementRangeId id) throws DataAccessException {
		repository.delete(id);
	}

	@Override
	public Page<ElemRangGrdVO> findAllElemRang(Integer year, ComponentType component, Pageable pageable) {
		QAnnualElementRange aer = QAnnualElementRange.annualElementRange;

		JPAQuery query = new JPAQuery(em).from(aer).where(aer.id.year.eq(year), aer.id.component.eq(component));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<AnnualElementRange>(AnnualElementRange.class, "annualElementRange"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ElemRangGrdVO> search = query.listResults(
				new QElemRangGrdVO(aer.id.year, aer.id.component, aer.id.element, aer.lessThan, aer.greaterThan));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

}
