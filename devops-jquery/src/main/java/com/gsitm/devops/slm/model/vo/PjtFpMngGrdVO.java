package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

@Data
public class PjtFpMngGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Long year;

	private String project;

	private String projectName;

	private Long part;

	private String op;

	private String opName;

	private Long system;

	private Float fp;

	private Float mh;

	@QueryProjection
	public PjtFpMngGrdVO(Long id, Long year, String project, String projectName, Long part, String op, String opName,
			Long system, Float fp, Float mh) {
		super();
		this.id = id;
		this.year = year;
		this.project = project;
		this.projectName = projectName;
		this.part = part;
		this.op = op;
		this.opName = opName;
		this.system = system;
		this.fp = fp;
		this.mh = mh;
	}
}
