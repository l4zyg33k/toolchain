package com.gsitm.devops.slm.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

import lombok.Data;

@Data
@Entity
@EntityListeners({ AuditingEntityListener.class })
public class AnnualComponentWeight implements Auditable<Account, AnnualComponentWeightId> {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AnnualComponentWeightId id;

	private Float ei;

	private Float eo;

	private Float eq;

	private Float ilf;

	private Float eif;

	@ManyToOne
	@CreatedBy
	private Account createdBy;

	@CreatedDate
	private DateTime createdDate;

	@ManyToOne
	@LastModifiedBy
	private Account lastModifiedBy;

	@LastModifiedDate
	private DateTime lastModifiedDate;

	@Override
	public boolean isNew() {
		return null == getId();
	}
}
