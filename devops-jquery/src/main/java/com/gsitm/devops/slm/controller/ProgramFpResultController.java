package com.gsitm.devops.slm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsitm.devops.cmm.util.Searchable;
import com.gsitm.devops.cmm.util.SearchableConverter;
import com.gsitm.devops.slm.model.ProgramFpResult;
import com.gsitm.devops.slm.model.ProjectFpResult;
import com.gsitm.devops.slm.model.QProgramFpResult;
import com.gsitm.devops.slm.service.ProgramFpResultService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;

/**
 * CSR FP 조정인자관리 컨트롤러
 */
@Controller
public class ProgramFpResultController {

	private SearchableConverter<ProgramFpResult> search = new SearchableConverter<ProgramFpResult>(
			ProgramFpResult.class);

	@Autowired
	private ProgramFpResultService pfrService;

	/**
	 * 그리드 조회
	 * 
	 * @param searchable
	 * @param pageable
	 * @param projectFpResult
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/programfpresult/{id}", method = RequestMethod.POST)
	@ResponseBody
	public Page<ProgramFpResult> findProjectQuality(Searchable searchable,
			Pageable pageable,
			@PathVariable("id") ProjectFpResult projectFpResult) {
		QProgramFpResult qProgramFpResult = QProgramFpResult.programFpResult;
		BooleanBuilder builider = new BooleanBuilder();
		builider = builider.and(qProgramFpResult.projectFpResult
				.eq(projectFpResult));

		// 그리드 조회조건 매핑
		Predicate predicate = search.toPredicate(searchable);
		Page<ProgramFpResult> result = pfrService.findAll(predicate, pageable);
		return result;
	}

	/**
	 * 그리드 수정
	 * 
	 * @param projectFpResult
	 * @param id
	 * @param programName
	 * @param oper
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/programfpresult/{projectFpResult}/edit", method = RequestMethod.POST)
	@ResponseBody
	public String edit(
			@PathVariable("projectFpResult") ProjectFpResult projectFpResult,
			@RequestParam("id") String id,
			@RequestParam(value = "programName", required = false) String programName,
			@RequestParam("oper") String oper) {
		ProgramFpResult data;
		switch (oper) {
		case "add":
			data = new ProgramFpResult();
			data.setProjectFpResult(projectFpResult);
			data.setName(programName);
			pfrService.save(data);
			break;

		case "edit":
			data = pfrService.findOne(Long.parseLong(id));
			data.setProjectFpResult(projectFpResult);
			data.setName(programName);
			pfrService.save(data);
			break;

		case "del":
			pfrService.delete(Long.parseLong(id));
			break;
		}
		return "success";
	}

}
