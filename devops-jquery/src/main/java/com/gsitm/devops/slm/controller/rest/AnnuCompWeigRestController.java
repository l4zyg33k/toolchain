package com.gsitm.devops.slm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.slm.model.AnnualComponentWeightId;
import com.gsitm.devops.slm.model.vo.CompWeigMngCmdVO;
import com.gsitm.devops.slm.model.vo.CompWeigMngGrdVO;
import com.gsitm.devops.slm.service.biz.AnnuCompWeigBiz;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class AnnuCompWeigRestController {

	@Autowired
	private AnnuCompWeigBiz biz;

	@RequestMapping(value = "/rest/slm/compweigmng", method = RequestMethod.POST)
	public Page<CompWeigMngGrdVO> findAllCompWeigMng(@RequestParam("year") Integer year, Pageable pageable) {
		return biz.findAllAnnuCompWeig(year, pageable);
	}

	@RequestMapping(value = "/rest/slm/compweigmng/edit", method = RequestMethod.POST)
	public JqResult editCompWeigMng(@RequestParam("oper") String oper, AnnualComponentWeightId id,
			@Valid CompWeigMngCmdVO vo, BindingResult result) {
		log.debug(vo.toString());
		log.debug(vo.toString());
		try {
			switch (oper) {
			case "add":
				biz.createAnnuCompWeig(id, vo);
				break;
			case "edit":
				biz.updateAnnuCompWeig(id, vo);
				break;
			default:
				break;
			}
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/compweigmng/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult removeCompWeigMng(AnnualComponentWeightId id) {
		log.debug(id.toString());
		try {
			biz.removeAnnuCompWeig(id);
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}
}
