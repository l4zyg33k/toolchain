package com.gsitm.devops.slm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.model.ProgramFpResult;
import com.gsitm.devops.slm.model.ProjectFpResult;
import com.gsitm.devops.slm.model.QArgMng;
import com.gsitm.devops.slm.model.QLvlMng;
import com.gsitm.devops.slm.model.QProjectFpResult;
import com.gsitm.devops.slm.service.ArgMngService;
import com.gsitm.devops.slm.service.LvlMngService;
import com.gsitm.devops.slm.service.ProjectFpResultService;
import com.mysema.query.BooleanBuilder;

/**
 * 프로젝트 FP 조정인자관리 컨트롤러
 */
@Controller
@Setter
public class ProjectFpResultController implements MessageSourceAware {

	private MessageSource messageSource;

	@Autowired
	private CodeService codeService;

	@Autowired
	private ProjectFpResultService pfrService;

	@Autowired
	private LvlMngService clService;

	@Autowired
	private ArgMngService caaService;

	/**
	 * 프로젝트 FP 기본정보관리 화면 조회
	 * 
	 * @return
	 */
	@RequestMapping(value = "/slm/projectfp/projectfpresultList", method = RequestMethod.GET)
	public String index(Model model) {

		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));

		return "slm/projectfp/projectfpList";
	}

	/**
	 * 프로젝트 FP index 그리드 조회
	 * 
	 * @param standardYear
	 * @param demandFunction
	 * @param jobDivision
	 * @param functionDivision
	 * @param pageable
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/projectfpresult", method = RequestMethod.POST)
	@ResponseBody
	public Page<ProjectFpResult> findAllGrid(
			@RequestParam(value = "standardYear", required = false) String standardYear,
			Pageable pageable) {
		QProjectFpResult qProjectFpResult = QProjectFpResult.projectFpResult;
		BooleanBuilder builider;
		if (standardYear == null || standardYear == "") {
			builider = new BooleanBuilder();
		} else {
			builider = new BooleanBuilder(
					qProjectFpResult.year.name.eq(standardYear));
		}
		Page<ProjectFpResult> result = pfrService.findAll(builider, pageable);
		return result;
	}

	/**
	 * 신규폼 로드
	 * 
	 * @param model
	 * @param standardYear
	 * @return
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/slm/projectfpresult/{standardYear}/new", method = RequestMethod.GET)
	public String newbie(Model model,
			@PathVariable("standardYear") Code standardYear)
			throws JsonProcessingException {
		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));
		// CSR파트(프로젝트만 선택)
		model.addAttribute(
				"csrSystemCode",
				codeService.findAll(qCode.parent.value.eq("G4").and(
						qCode.value.eq("09"))));
		// 시스템특성
		model.addAttribute("systemTypeCode",
				codeService.findAll(qCode.parent.value.eq("H2")));
		// 요구기능
		model.addAttribute("demandFunctionCode",
				codeService.findAll(qCode.parent.value.eq("SLM10")));
		// 작업유형
		model.addAttribute("jobDivisionCode",
				codeService.findAll(qCode.parent.value.eq("SLM20")));

		// 그리드 콤보용 데이터 가져오기
		model.addAttribute("demandFunctionGrid",
				codeService.getJqGridSelectValue("SLM10"));
		model.addAttribute("jobDivisionGrid",
				codeService.getJqGridSelectValue("SLM20"));
		model.addAttribute("functionDivisionGrid",
				codeService.getJqGridSelectValue("SLM30"));
		model.addAttribute("levelGrid",
				codeService.getJqGridSelectValue("SLM40"));

		// FP계산 기준정보 가져오기
		QLvlMng qCsrLevel = QLvlMng.lvlMng;
		QArgMng qCsrAdjustArgument = QArgMng.argMng;
		Iterable<LvlMng> levelList = clService.findAll(qCsrLevel.year
				.eq(standardYear));
		Iterable<ArgMng> adjustArgumentList = caaService
				.findAll(qCsrAdjustArgument.year.eq(standardYear));
		model.addAttribute("levelList", levelList);
		model.addAttribute("adjustArgumentList", adjustArgumentList);

		ProjectFpResult projectFpResult = new ProjectFpResult();
		projectFpResult.setYear(standardYear);
		model.addAttribute("projectFpResultVO", projectFpResult);

		return "slm/projectfp/projectfpResultForm";
	}

	/**
	 * 수정폼 로드
	 * 
	 * @param model
	 * @param projectFpResult
	 * @return
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/slm/projectfpresult/{id}/edit", method = RequestMethod.GET)
	public String edit(Model model,
			@PathVariable("id") ProjectFpResult projectFpResult)
			throws JsonProcessingException {
		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));
		// CSR파트(프로젝트만 선택)
		model.addAttribute(
				"csrSystemCode",
				codeService.findAll(qCode.parent.value.eq("G4").and(
						qCode.value.eq("09"))));
		// 시스템특성
		model.addAttribute("systemTypeCode",
				codeService.findAll(qCode.parent.value.eq("H2")));
		// 요구기능
		model.addAttribute("demandFunctionCode",
				codeService.findAll(qCode.parent.value.eq("SLM10")));
		// 작업유형
		model.addAttribute("jobDivisionCode",
				codeService.findAll(qCode.parent.value.eq("SLM20")));

		model.addAttribute("demandFunctionGrid",
				codeService.getJqGridSelectValue("SLM10"));
		model.addAttribute("jobDivisionGrid",
				codeService.getJqGridSelectValue("SLM20"));
		model.addAttribute("functionDivisionGrid",
				codeService.getJqGridSelectValue("SLM30"));
		model.addAttribute("levelGrid",
				codeService.getJqGridSelectValue("SLM40"));

		// FP계산 기준정보 가져오기
		QLvlMng qCsrLevel = QLvlMng.lvlMng;
		QArgMng qCsrAdjustArgument = QArgMng.argMng;
		Iterable<LvlMng> levelList = clService.findAll(qCsrLevel.year
				.eq(projectFpResult.getYear()));
		Iterable<ArgMng> adjustArgumentList = caaService
				.findAll(qCsrAdjustArgument.year.eq(projectFpResult.getYear()));
		model.addAttribute("levelList", levelList);
		model.addAttribute("adjustArgumentList", adjustArgumentList);

		model.addAttribute("projectFpResultVO", projectFpResult);

		return "slm/projectfp/projectfpResultForm";
	}

	/**
	 * 저장처리
	 * 
	 * @param model
	 * @param projectFpResultVO
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/projectfpResult/edit", method = RequestMethod.POST)
	public String create(
			Model model,
			@ModelAttribute("projectFpResultVO") @Valid ProjectFpResult projectFpResultVO,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		// 유효성체크
		if (result.hasErrors()) {

			QCode qCode = QCode.code;
			// 기준년도 리스트
			model.addAttribute("standardYearList",
					codeService.findAll(qCode.parent.value.eq("SLM50")));
			// CSR파트(프로젝트만 선택)
			model.addAttribute(
					"csrSystemCode",
					codeService.findAll(qCode.parent.value.eq("G4").and(
							qCode.value.eq("09"))));
			// 시스템특성
			model.addAttribute("systemTypeCode",
					codeService.findAll(qCode.parent.value.eq("H2")));
			// 요구기능
			model.addAttribute("demandFunctionCode",
					codeService.findAll(qCode.parent.value.eq("SLM10")));
			// 작업유형
			model.addAttribute("jobDivisionCode",
					codeService.findAll(qCode.parent.value.eq("SLM20")));

			model.addAttribute("demandFunctionGrid",
					codeService.getJqGridSelectValue("SLM10"));
			model.addAttribute("jobDivisionGrid",
					codeService.getJqGridSelectValue("SLM20"));
			model.addAttribute("functionDivisionGrid",
					codeService.getJqGridSelectValue("SLM30"));
			model.addAttribute("levelGrid",
					codeService.getJqGridSelectValue("SLM40"));

			model.addAttribute(model);
			return "slm/projectfp/projectfpResultForm";
		}

		projectFpResultVO = pfrService.save(projectFpResultVO);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("save.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return String.format("redirect:/slm/projectfpresult/%d/edit",
				projectFpResultVO.getId());
	}

	/**
	 * 저장처리
	 * 
	 * @param model
	 * @param projectFpResultVO
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/projectfpResult/edit", method = RequestMethod.PUT)
	public String save(
			Model model,
			@ModelAttribute("projectFpResultVO") @Valid ProjectFpResult projectFpResultVO,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		// 유효성체크
		if (result.hasErrors()) {

			QCode qCode = QCode.code;
			// 기준년도 리스트
			model.addAttribute("standardYearList",
					codeService.findAll(qCode.parent.value.eq("SLM50")));
			// CSR파트(프로젝트만 선택)
			model.addAttribute(
					"csrSystemCode",
					codeService.findAll(qCode.parent.value.eq("G4").and(
							qCode.value.eq("09"))));
			// 시스템특성
			model.addAttribute("systemTypeCode",
					codeService.findAll(qCode.parent.value.eq("H2")));
			// 요구기능
			model.addAttribute("demandFunctionCode",
					codeService.findAll(qCode.parent.value.eq("SLM10")));
			// 작업유형
			model.addAttribute("jobDivisionCode",
					codeService.findAll(qCode.parent.value.eq("SLM20")));

			model.addAttribute("demandFunctionGrid",
					codeService.getJqGridSelectValue("SLM10"));
			model.addAttribute("jobDivisionGrid",
					codeService.getJqGridSelectValue("SLM20"));
			model.addAttribute("functionDivisionGrid",
					codeService.getJqGridSelectValue("SLM30"));
			model.addAttribute("levelGrid",
					codeService.getJqGridSelectValue("SLM40"));

			model.addAttribute(model);
			return "slm/projectfp/projectfpResultForm";
		}
		List<ProgramFpResult> detail = pfrService.findOne(
				projectFpResultVO.getId()).getPrograms();
		projectFpResultVO.setPrograms(detail);
		projectFpResultVO = pfrService.save(projectFpResultVO);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("save.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return String.format("redirect:/slm/projectfpresult/%d/edit",
				projectFpResultVO.getId());
	}

	/**
	 * 삭제처리
	 * 
	 * @param model
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/projectfpresult/{id}", method = RequestMethod.DELETE)
	public String delete(Model model, @PathVariable("id") Long id,
			final RedirectAttributes redirectAttributes) {

		pfrService.delete(id);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("delete.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return "redirect:/slm/projectfp/projectfpresultList";
	}

	@RequestMapping(value = "/slm/projectfpresult/get/{id:\\d+}", method = RequestMethod.GET)
	@ResponseBody
	public ProjectFpResult findOne(
			@PathVariable("id") ProjectFpResult projectFpResult) {
		return projectFpResult;
	}
}
