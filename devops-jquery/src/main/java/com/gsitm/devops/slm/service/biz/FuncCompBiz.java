package com.gsitm.devops.slm.service.biz;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.slm.model.AnnualFunctionComplexityId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.vo.FuncCompCmdVO;
import com.gsitm.devops.slm.model.vo.FuncCompColVO;
import com.gsitm.devops.slm.model.vo.FuncCompGrdVO;

public interface FuncCompBiz {

	public FuncCompColVO getColModel(Integer year, ComponentType component);

	public Page<FuncCompGrdVO> findAllFuncComp(Integer year, ComponentType component, Pageable pageable);

	public void createFuncComp(AnnualFunctionComplexityId id, FuncCompCmdVO vo) throws DataAccessException;

	public void updateFuncComp(AnnualFunctionComplexityId id, FuncCompCmdVO vo) throws DataAccessException;

	public void removeFuncComp(AnnualFunctionComplexityId id) throws DataAccessException;
}
