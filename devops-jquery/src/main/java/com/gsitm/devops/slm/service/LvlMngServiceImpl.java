package com.gsitm.devops.slm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.repository.LvlMngRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class LvlMngServiceImpl implements LvlMngService {

	@Autowired
	private LvlMngRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public LvlMng findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public LvlMng findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<LvlMng> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<LvlMng> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<LvlMng> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<LvlMng> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<LvlMng> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<LvlMng> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public LvlMng save(LvlMng document) {
		return repository.save(document);
	}

	@Transactional
	@Override
	public <S extends LvlMng> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(LvlMng document) {
		repository.delete(document);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends LvlMng> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	/**
	 * 일괄적으로 조정인자 리스트 생성
	 */
	/* 삭제대상 주석처리 김정인 20141105
	@Override
	@Transactional
	public String createNew(String year) {

		// 기존년도 데이터 삭제
		// QCsrLevel qCsrLevel = QCsrLevel.csrLevel;
		// repository.delete(this.findAll(qCsrLevel.standardYear.eq(year)));

		// 빈값으로 데이터 생성
		QCode qCode = QCode.code;
		Iterable<Code> demandFunctionList = codeService
				.findAll(qCode.parent.value.eq("SLM10")); // 요구기능
		Iterable<Code> jobDivisionList = codeService.findAll(qCode.parent.value
				.eq("SLM20")); // 작업구분
		Iterable<Code> functionDivisionList = codeService
				.findAll(qCode.parent.value.eq("SLM30")); // 기능유형

		List<LvlMng> newData = new ArrayList<LvlMng>();
		for (Code demandFunction : demandFunctionList) {
			for (Code jobDivision : jobDivisionList) {
				for (Code functionDivision : functionDivisionList) {
					//demandFunction의 설명에 있는 코드와
					//demandFunction(data,transaction )와 비교해서 저장 data :
					//관리(ILF), 참조(EIF) transaction : 입력(EI), 수정(EO), 삭제(EQ)

					if (demandFunction.getValue().equals(
							functionDivision.getDescription())) {
						LvlMng newRow = new LvlMng();
						// newRow.setStandardYear(year);
						newRow.setFunction(demandFunction.getValue());
						newRow.setJob(jobDivision.getValue());
						newRow.setType(functionDivision.getValue());
						newData.add(newRow);
					}
				}
			}
		}
		repository.save(newData);

		return "success";

	}
	*/
}
