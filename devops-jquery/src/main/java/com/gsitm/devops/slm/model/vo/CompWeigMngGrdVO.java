package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.slm.model.ComplexityType;
import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

@Data
public class CompWeigMngGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer year;

	private ComplexityType complexity;

	private Float ei;

	private Float eo;

	private Float eq;

	private Float ilf;

	private Float eif;

	@QueryProjection
	public CompWeigMngGrdVO(Integer year, ComplexityType complexity, Float ei, Float eo, Float eq, Float ilf, Float eif) {
		super();
		this.year = year;
		this.complexity = complexity;
		this.ei = ei;
		this.eo = eo;
		this.eq = eq;
		this.ilf = ilf;
		this.eif = eif;
	}
}
