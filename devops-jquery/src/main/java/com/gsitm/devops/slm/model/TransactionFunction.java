package com.gsitm.devops.slm.model;

import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class TransactionFunction extends AbstractPersistable<Long> {

	private static final long serialVersionUID = -153231407179671653L;
	private String name;
	@ElementCollection
	private Set<DataEntity> ftrs;
	@ElementCollection
	private Set<DataElementType> dets;
	@ElementCollection
	private Set<String> etcs;
	private TransactionType type;
}
