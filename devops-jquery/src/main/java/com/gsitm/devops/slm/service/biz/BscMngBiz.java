package com.gsitm.devops.slm.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.model.vo.ArgMngGrdVO;
import com.gsitm.devops.slm.model.vo.LvlMngGrdVO;

public interface BscMngBiz extends MessageSourceAware {

	public void deleteLvl(Iterable<LvlMng> lvlMng);

	public Page<LvlMngGrdVO> findAllLvl(Long year, Long function, Long job,
			Long type, Pageable pageable);
	
	public Boolean isExistLvlMng(Long year, Long function, Long job, Long type);
	
	public Boolean isExistArgMng(Long year, Long function, Long job, Long type);

	public void saveLvl(LvlMng lvlMng);

	public LvlMng findOneLvl(long id);

	public void deleteLvl(LvlMng lvlMng);

	public Page<ArgMngGrdVO> findAllArg(Long year, Long function, Long job,
			Long type, Pageable pageable);

	public ArgMng findOneArg(long id);

	public void saveArg(ArgMng argMng);

	public void deleteArg(ArgMng argMng);

	/**
	 * 기준년도를 가져온다
	 * 
	 * @return 기준년도
	 */
	public Map<Long, String> getYears();
	
	/**
	 * 요구기능을 가져온다
	 * 
	 * @return 요구기능
	 */
	public Map<Long, String> getDemands();
	
	/**
	 * 작업유형을 가져온다
	 * 
	 * @return 작업유형
	 */
	public Map<Long, String> getJobs();
	
	public Map<Long, String> getTypes();
	
	public Map<Long, String> getTypes(Long demand);

	public String getLevels();
	
	public String getMores();
	
	public String getLesses();

	public String getDemandsVals();

	public String getJobsVals();

	public String getTypesVals(Long demand);

	public String getLevelsVals();

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public String getYearsVals();
}
