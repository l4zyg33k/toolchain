package com.gsitm.devops.slm.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProgramFpResultDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 프로그램 기능 점수
	 */
	@ManyToOne
	private ProgramFpResult programFpResult;

	/**
	 * 요구 기능 (코드)
	 */
	@ManyToOne
	private Code function;

	/**
	 * 작업 유형 (코드)
	 */
	@ManyToOne
	private Code job;

	/**
	 * 기능 명칭
	 */
	private String name;

	/**
	 * 기능 유형 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * 변경 FTR 수
	 */
	private Integer chgFtrCnt;

	/**
	 * 변경 DET 수
	 */
	private Integer chgDetCnt;

	/**
	 * 이전 FTR 수
	 */
	private Integer befFtrCnt;

	/**
	 * 이전 DET 수
	 */
	private Integer befDetCnt;

	/**
	 * 이후 FTR 수
	 */
	private Integer aftFtrCnt;

	/**
	 * 이후 DET 수
	 */
	private Integer aftDetCnt;

	/**
	 * 추가 변경 FTR
	 */
	private Integer addChgFtr;

	/**
	 * 수정 변경 FTR
	 */
	private Integer modChgFtr;

	/**
	 * 삭제 변경 FTR
	 */
	private Integer delChgFtr;

	/**
	 * 추가 변경 DET
	 */
	private Integer addChgDet;

	/**
	 * 수정 변경 DET
	 */
	private Integer modChgDet;

	/**
	 * 삭제 변경 DET
	 */
	private Integer delChgDet;

	/**
	 * 기능 점수
	 */
	private Float fp;

	/**
	 * 난이도 (코드)
	 */
	@ManyToOne
	private Code level;

	/**
	 * 조정 인자
	 */
	private Float adjArg;

	/**
	 * 조정 후 기능 점수
	 */
	private Float adjFp;
}
