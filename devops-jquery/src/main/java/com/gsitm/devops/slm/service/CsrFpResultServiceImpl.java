package com.gsitm.devops.slm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.CsrFpResult;
import com.gsitm.devops.slm.model.CsrFpResultDetail;
import com.gsitm.devops.slm.model.Productivity;
import com.gsitm.devops.slm.model.QProductivity;
import com.gsitm.devops.slm.repository.CsrFpResultRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class CsrFpResultServiceImpl implements CsrFpResultService {

	@Autowired
	private CsrFpResultRepository repository;

	@Autowired
	private ProductivityService productivityService;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Autowired
	private CodeService codeService;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public CsrFpResult findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public CsrFpResult findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<CsrFpResult> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<CsrFpResult> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<CsrFpResult> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<CsrFpResult> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<CsrFpResult> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<CsrFpResult> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	/**
	 * CsrFpResultDetail이 변경되면 CsrFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public CsrFpResult save(CsrFpResult csrFpResult) {
		if (csrFpResult.getId() == null) {
			return repository.save(csrFpResult);
		} else {
			return saveResultFp(csrFpResult);
		}
	}

	/**
	 * CsrFpResultDetail이 변경되면 CsrFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public <S extends CsrFpResult> List<S> save(Iterable<S> iterable) {
		for (S s : iterable) {
			if (s.getId() == null) {
				repository.save(s);
			} else {
				saveResultFp(s);
			}

		}
		return null;
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(CsrFpResult csrFpResult) {
		repository.delete(csrFpResult);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends CsrFpResult> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Transactional
	@Override
	public CsrFpResult saveResultFp(CsrFpResult csrFpResult) {
		List<CsrFpResultDetail> details = this.findOne(csrFpResult.getId())
				.getDetails();
		Float sumFp = 0F;
		// fp합계
		for (CsrFpResultDetail detail : details) {
			sumFp += detail.getAdjFp();
		}
		csrFpResult.setFp(sumFp);

		// 예상 MH계산
		QProductivity qProductivity = QProductivity.productivity;
		Productivity productivity = productivityService
				.findOne(qProductivity.year
						.eq(csrFpResult.getYear())
						.and(qProductivity.part.eq(csrFpResult.getPart()))
						.and(qProductivity.system.eq(csrFpResult
								.getSystem())));
		Float result = sumFp / productivity.getPoint();
		Float t = (float) (Math.round(result * 100) / 100F);
		csrFpResult.setMh(t);

		return repository.save(csrFpResult);
	}
}
