package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.AnnualComponentWeightId;
import com.gsitm.devops.slm.model.AnnualComponentWeight;

@Repository
public interface AnnualComponentWeightRepository extends SharedRepository<AnnualComponentWeight, AnnualComponentWeightId> {

}
