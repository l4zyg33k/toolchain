package com.gsitm.devops.slm.service.biz;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.slm.model.AnnualElementRangeId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.vo.ElemRangGrdVO;
import com.gsitm.devops.slm.model.vo.ElemRangMngCmdVO;

public interface ElemRangBiz {

	void createElemRang(AnnualElementRangeId id, ElemRangMngCmdVO vo);

	void updateElemRang(AnnualElementRangeId id, ElemRangMngCmdVO vo);

	void removeElemRang(AnnualElementRangeId id);

	Page<ElemRangGrdVO> findAllElemRang(Integer year, ComponentType component, Pageable pageable);
}
