package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class FuncCompColVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String rangeLabel;

	private String rangeValue;

	private String lessThanLabel;

	private String closedLabel;

	private String greaterThanLabel;
}
