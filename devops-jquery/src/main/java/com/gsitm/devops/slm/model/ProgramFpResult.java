package com.gsitm.devops.slm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProgramFpResult extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트 기능 점수
	 */
	@ManyToOne
	private ProjectFpResult projectFpResult;

	/**
	 * 프로그램명
	 */
	@Column(length = 64)
	private String name;

	/**
	 * 프로그램 기능 점수
	 */
	private Float fp;

	/**
	 * 프로그램 예상 투입 MH
	 */
	private Float mh;

	/**
	 * 프로그램 기능 점수 상세
	 */
	@OneToMany(mappedBy = "programFpResult", orphanRemoval = true)
	private List<ProgramFpResultDetail> details;
}
