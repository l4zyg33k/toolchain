package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.ElementType;
import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

@Data
public class ElemRangGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer year;

	private ComponentType component;

	private ElementType element;

	private Integer lessThan;

	private Integer greaterThan;

	@QueryProjection
	public ElemRangGrdVO(Integer year, ComponentType component, ElementType element, Integer lessThan,
			Integer greaterThan) {
		super();
		this.year = year;
		this.component = component;
		this.element = element;
		this.lessThan = lessThan;
		this.greaterThan = greaterThan;
	}
}
