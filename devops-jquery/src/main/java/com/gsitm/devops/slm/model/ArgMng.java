package com.gsitm.devops.slm.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ArgMng extends AbstractAuditable<Account, Long> {

	/**
	 * 기준 년도 (코드)
	 */
	@ManyToOne
	private Code year;

	/**
	 * 요구 기능 (코드)
	 */
	@ManyToOne
	private Code function;

	/**
	 * 작업 유형 (코드)
	 */
	@ManyToOne
	private Code job;

	/**
	 * 기능 유형 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * FTR 변경률 최소 값
	 */
	private Float ftrMin;

	/**
	 * FTR 변경률 최소 연산자 (코드)
	 */
	@ManyToOne
	private Code ftrMinOp;

	/**
	 * FTR 변경률 최대 값
	 */
	private Float ftrMax;

	/**
	 * FTR 변경률 최대 연산자 (코드)
	 */
	@ManyToOne
	private Code ftrMaxOp;

	/**
	 * DET 변경률 최소 값
	 */
	private Float detMin;

	/**
	 * DET 변경률 최소 연산자 (코드)
	 */
	@ManyToOne
	private Code detMinOp;

	/**
	 * DET 변경률 최대 값
	 */
	private Float detMax;

	/**
	 * DET 변경률 최대 연산자 (코드)
	 */
	@ManyToOne
	private Code detMaxOp;

	/**
	 * 조정 인자
	 */
	private Float adjArg;
}
