package com.gsitm.devops.slm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.slm.model.ProgramFpResult;
import com.gsitm.devops.slm.repository.ProgramFpResultRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProgramFpResultServiceImpl implements ProgramFpResultService {

	@Autowired
	private ProgramFpResultRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public ProgramFpResult findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProgramFpResult findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProgramFpResult> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProgramFpResult> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProgramFpResult> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProgramFpResult> findAll(Predicate predicate, OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProgramFpResult> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProgramFpResult> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProgramFpResult save(ProgramFpResult programFpResult) {
		return repository.save(programFpResult);
	}

	@Transactional
	@Override
	public <S extends ProgramFpResult> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProgramFpResult programFpResult) {
		repository.delete(programFpResult);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProgramFpResult> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
