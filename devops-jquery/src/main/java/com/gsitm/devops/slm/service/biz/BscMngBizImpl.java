package com.gsitm.devops.slm.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.model.QArgMng;
import com.gsitm.devops.slm.model.QLvlMng;
import com.gsitm.devops.slm.model.vo.ArgMngGrdVO;
import com.gsitm.devops.slm.model.vo.LvlMngGrdVO;
import com.gsitm.devops.slm.model.vo.QArgMngGrdVO;
import com.gsitm.devops.slm.model.vo.QLvlMngGrdVO;
import com.gsitm.devops.slm.service.ArgMngService;
import com.gsitm.devops.slm.service.LvlMngService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class BscMngBizImpl implements BscMngBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CodeService codeService;

	@Autowired
	private LvlMngService lvlMngService;
	
	@Autowired
	private ArgMngService argMngService;

	@Override
	public void deleteLvl(Iterable<LvlMng> lvlMng) {
		lvlMngService.delete(lvlMng);
	}

	@Override
	public void saveLvl(LvlMng lvlMng) {
		lvlMngService.save(lvlMng);
	}

	@Override
	public LvlMng findOneLvl(long id) {
		return lvlMngService.findOne(id);
	}

	@Override
	public void deleteLvl(LvlMng lvlMng) {
		lvlMngService.delete(lvlMng);
	}

	@Override
	public ArgMng findOneArg(long id) {
		return argMngService.findOne(id);
	}

	@Override
	public void saveArg(ArgMng argMng) {
		argMngService.save(argMng);
	}

	@Override
	public void deleteArg(ArgMng argMng) {
		argMngService.delete(argMng);
	}

	@Override
	public Page<LvlMngGrdVO> findAllLvl(Long year, Long function, Long job,
			Long type, Pageable pageable) {

		QLvlMng lvlMng = QLvlMng.lvlMng;

		BooleanBuilder builder = new BooleanBuilder(lvlMng.year.id.eq(year)
				.and(lvlMng.function.id.eq(function)));

		if (job > 0) {
			builder.and(lvlMng.job.id.eq(job));
		}

		if (type > 0) {
			builder.and(lvlMng.type.id.eq(type));
		}

		JPQLQuery query = new JPAQuery(em).from(lvlMng).where(builder);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<LvlMng>(
				LvlMng.class, "lvlMng"));
		querydsl.applyPagination(pageable, query);

		SearchResults<LvlMngGrdVO> search = query.listResults(new QLvlMngGrdVO(
				lvlMng.id, lvlMng.year.id, lvlMng.function.id, lvlMng.job.id,
				lvlMng.type.id, lvlMng.ftrMin, lvlMng.ftrMinOp.id,
				lvlMng.ftrMax, lvlMng.ftrMaxOp.id, lvlMng.detMin,
				lvlMng.detMinOp.id, lvlMng.detMax, lvlMng.detMaxOp.id,
				lvlMng.level.id, lvlMng.fp));

		return new PageImpl<LvlMngGrdVO>(search.getResults(), pageable,
				search.getTotal());
	}

	@Override
	public Page<ArgMngGrdVO> findAllArg(Long year, Long function, Long job,
			Long type, Pageable pageable) {
		QArgMng argMng = QArgMng.argMng;
		BooleanBuilder builder = new BooleanBuilder(argMng.year.id.eq(year)
				.and(argMng.function.id.eq(function)));

		if (job > 0) {
			builder.and(argMng.job.id.eq(job));
		}

		if (type > 0) {
			builder.and(argMng.type.id.eq(type));
		}

		JPQLQuery query = new JPAQuery(em).from(argMng).where(builder);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<ArgMng>(
				ArgMng.class, "argMng"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ArgMngGrdVO> search = query.listResults(new QArgMngGrdVO(
				argMng.id, argMng.year.id, argMng.function.id, argMng.job.id,
				argMng.type.id, argMng.ftrMin, argMng.ftrMinOp.id,
				argMng.ftrMax, argMng.ftrMaxOp.id, argMng.detMin,
				argMng.detMinOp.id, argMng.detMax, argMng.detMaxOp.id,
				argMng.adjArg));

		return new PageImpl<ArgMngGrdVO>(search.getResults(), pageable,
				search.getTotal());
	}

	@Override
	public Map<Long, String> getYears() {
		Map<Long, String> years = new HashMap<>();
		years.put(0L, "-- 기준년도 -- ");
		years.putAll(codeService.getOptions("SLM50"));
		return years;
	}

	@Override
	public Map<Long, String> getDemands() {
		Map<Long, String> demands = new HashMap<>();
		demands.put(0L, "-- 요구기능 -- ");
		demands.putAll(codeService.getOptions("SLM10"));
		return demands;
	}

	@Override
	public Map<Long, String> getJobs() {
		Map<Long, String> jobs = new HashMap<>();
		jobs.put(0L, "-- 작업유형 -- ");
		jobs.putAll(codeService.getOptions("SLM20"));
		return jobs;
	}

	@Override
	public Map<Long, String> getTypes(Long demand) {
		Map<Long, String> types = new HashMap<>();
		types.put(0L, "-- 기능유형 -- ");

		QCode code = QCode.code;
		QCode demandCode = new QCode("demandCode");
		JPQLQuery query = new JPAQuery(em)
				.from(code)
				.where(code.parent.value.eq("SLM30"),
						code.enabled.eq(true),
						code.description.eq(new JPASubQuery().from(demandCode)
								.where(demandCode.id.eq(demand))
								.unique(demandCode.value)))
				.orderBy(code.rank.asc(), code.value.asc());
		types.putAll(query.map(code.id, code.name));

		return types;
	}

	@Override
	public String getLevels() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM40"));
	}

	@Override
	public String getMores() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM70"));
	}

	@Override
	public String getLesses() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM80"));
	}

	@Override
	public String getDemandsVals() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM10"));
	}

	@Override
	public String getJobsVals() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM20"));
	}

	@Override
	public Map<Long, String> getTypes() {
		Map<Long, String> types = new HashMap<>();
		types.put(0L, "-- 기능유형 -- ");
		return types;
	}

	@Override
	public String getTypesVals(Long demand) {
		Map<Long, String> types = new HashMap<>();
		QCode code = QCode.code;
		QCode demandCode = new QCode("demandCode");
		JPQLQuery query = new JPAQuery(em)
				.from(code)
				.where(code.parent.value.eq("SLM30"),
						code.enabled.eq(true),
						code.description.eq(new JPASubQuery().from(demandCode)
								.where(demandCode.id.eq(demand))
								.unique(demandCode.value)))
				.orderBy(code.rank.asc(), code.value.asc());
		types.putAll(query.map(code.id, code.name));
		return Joiner.on(";").withKeyValueSeparator(":").join(types);
	}

	@Override
	public String getLevelsVals() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM40"));
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public String getYearsVals() {
		return Joiner.on(";").withKeyValueSeparator(":")
				.join(codeService.getOptions("SLM50"));
	}

	@Override
	public Boolean isExistLvlMng(Long year, Long function, Long job, Long type) {
		QLvlMng lvlMng = QLvlMng.lvlMng;
		JPQLQuery query = new JPAQuery(em).from(lvlMng).where(
				lvlMng.year.id.eq(year), lvlMng.function.id.eq(function),
				lvlMng.job.id.eq(job), lvlMng.type.id.eq(type));
		return query.exists();
	}

	@Override
	public Boolean isExistArgMng(Long year, Long function, Long job, Long type) {
		QArgMng argMng = QArgMng.argMng;
		JPQLQuery query = new JPAQuery(em).from(argMng).where(
				argMng.year.id.eq(year), argMng.function.id.eq(function),
				argMng.job.id.eq(job), argMng.type.id.eq(type));
		return query.exists();
	}
}
