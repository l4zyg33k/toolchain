package com.gsitm.devops.slm.controller.rest;

import java.text.ParseException;
import java.util.Map;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.model.vo.ArgMngGrdVO;
import com.gsitm.devops.slm.model.vo.LvlMngGrdVO;
import com.gsitm.devops.slm.service.biz.BscMngBiz;

/**
 * CSR FP 기본정보 컨트롤러
 */
@RestController
@Setter
public class BscMngRestController {

	@Autowired
	private CodeService codeService;

	@Autowired
	private BscMngBiz biz;

	/**
	 * 연계콤보 설정
	 * 
	 * @param model
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/bsc/chained/types", method = RequestMethod.GET)
	public Map<Long, String> getTypes(@RequestParam("demand") Long demand) {
		return biz.getTypes(demand);
	}

	@RequestMapping(value = "/rest/slm/bsc/chained/types", method = RequestMethod.POST)
	public String getTypesVals(@RequestParam("demand") Long demand) {
		return biz.getTypesVals(demand);
	}

	/**
	 * 난이도 그리드 조회
	 * 
	 * @param standardYear
	 * @param demandFunction
	 * @param jobDivision
	 * @param functionDivision
	 * @param pageable
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/bsc/lvlmng", method = RequestMethod.POST)
	public Page<LvlMngGrdVO> findAllLvlMngGrid(@RequestParam("year") Long year,
			@RequestParam("demand") Long function,
			@RequestParam("job") Long job, @RequestParam("type") Long type,
			Pageable pageable) {

		return biz.findAllLvl(year, function, job, type, pageable);
	}

	/**
	 * 난이도 그리드 수정
	 * 
	 * @param id
	 * @param ftrChangeRateMin
	 * @param ftrChangeRateMax
	 * @param detChangeRateMin
	 * @param detChangeRateMax
	 * @param adjustArgument
	 * @param oper
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/rest/slm/bsc/lvlmng/edit", method = RequestMethod.POST)
	public JqResult editLvlMngGrid(@RequestParam("id") String id,
			@RequestParam("year") Code year,
			@RequestParam("function") Code function,
			@RequestParam("job") Code job, @RequestParam("type") Code type,

			@RequestParam("ftrMin") Integer ftrMin,
			@RequestParam("ftrMax") Integer ftrMax,
			@RequestParam("detMin") Integer detMin,
			@RequestParam("detMax") Integer detMax,

			@RequestParam("ftrMinOp") Code ftrMinOp,
			@RequestParam("ftrMaxOp") Code ftrMaxOp,
			@RequestParam("detMinOp") Code detMinOp,
			@RequestParam("detMaxOp") Code detMaxOp,

			@RequestParam("level") Code level, @RequestParam("fp") Float fp,
			@RequestParam("oper") String oper) {

		LvlMng lvlMng;

		switch (oper) {
		case "add":

			if (biz.isExistLvlMng(year.getId(), function.getId(), job.getId(),
					type.getId())) {
				return new JqFailure("이미 존재하는 데이터 입니다. 새 행을 추가 할 수 없습니다.");
			}

			lvlMng = new LvlMng();

			lvlMng.setYear(year);
			lvlMng.setFunction(function);
			lvlMng.setJob(job);
			lvlMng.setType(type);
			lvlMng.setFtrMin(ftrMin);
			lvlMng.setFtrMax(ftrMax);
			lvlMng.setDetMin(detMin);
			lvlMng.setDetMax(detMax);
			lvlMng.setFtrMinOp(ftrMinOp);
			lvlMng.setFtrMaxOp(ftrMaxOp);
			lvlMng.setDetMinOp(detMinOp);
			lvlMng.setDetMaxOp(detMaxOp);
			lvlMng.setLevel(level);
			lvlMng.setFp(fp);

			biz.saveLvl(lvlMng);

			break;
		case "edit":
			lvlMng = biz.findOneLvl(Long.parseLong(id));

			lvlMng.setYear(year);
			lvlMng.setFunction(function);
			lvlMng.setJob(job);
			lvlMng.setType(type);
			lvlMng.setFtrMin(ftrMin);
			lvlMng.setFtrMax(ftrMax);
			lvlMng.setDetMin(detMin);
			lvlMng.setDetMax(detMax);
			lvlMng.setFtrMinOp(ftrMinOp);
			lvlMng.setFtrMaxOp(ftrMaxOp);
			lvlMng.setDetMinOp(detMinOp);
			lvlMng.setDetMaxOp(detMaxOp);
			lvlMng.setLevel(level);
			lvlMng.setFp(fp);

			biz.saveLvl(lvlMng);
			break;

		default:
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/bsc/lvlmng/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult delLvlMngGrid(@RequestParam("id") Long id) {
		LvlMng lvlMng = biz.findOneLvl(id);
		biz.deleteLvl(lvlMng);
		return new JqSuccess();
	}

	/**
	 * 조정인자 그리드 조회
	 * 
	 * @param standardYear
	 * @param demandFunction
	 * @param jobDivision
	 * @param functionDivision
	 * @param pageable
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/bsc/argmng", method = RequestMethod.POST)
	public Page<ArgMngGrdVO> findAllArgMngGrid(@RequestParam("year") Long year,
			@RequestParam("demand") Long function,
			@RequestParam("job") Long job, @RequestParam("type") Long type,
			Pageable pageable) {

		return biz.findAllArg(year, function, job, type, pageable);
	}

	@RequestMapping(value = "/rest/slm/bsc/argmng/edit", method = RequestMethod.POST)
	public JqResult editArgMngGrid(@RequestParam("id") String id,
			@RequestParam("year") Code year,
			@RequestParam("function") Code function,
			@RequestParam("job") Code job, @RequestParam("type") Code type,

			@RequestParam("ftrMin") Float ftrMin,
			@RequestParam("ftrMax") Float ftrMax,
			@RequestParam("detMin") Float detMin,
			@RequestParam("detMax") Float detMax,

			@RequestParam("ftrMinOp") Code ftrMinOp,
			@RequestParam("ftrMaxOp") Code ftrMaxOp,
			@RequestParam("detMinOp") Code detMinOp,
			@RequestParam("detMaxOp") Code detMaxOp,

			@RequestParam("adjArg") Float adjArg,
			@RequestParam("oper") String oper) {

		ArgMng argMng;

		switch (oper) {
		case "add":

			if (biz.isExistArgMng(year.getId(), function.getId(), job.getId(),
					type.getId())) {
				return new JqFailure("이미 존재하는 데이터 입니다. 새 행을 추가 할 수 없습니다.");
			}

			argMng = new ArgMng();

			argMng.setYear(year);
			argMng.setFunction(function);
			argMng.setJob(job);
			argMng.setType(type);
			argMng.setFtrMin(ftrMin);
			argMng.setFtrMax(ftrMax);
			argMng.setDetMin(detMin);
			argMng.setDetMax(detMax);
			argMng.setFtrMinOp(ftrMinOp);
			argMng.setFtrMaxOp(ftrMaxOp);
			argMng.setDetMinOp(detMinOp);
			argMng.setDetMaxOp(detMaxOp);
			argMng.setAdjArg(adjArg);

			biz.saveArg(argMng);
			break;
		case "edit":
			argMng = biz.findOneArg(Long.parseLong(id));

			argMng.setYear(year);
			argMng.setFunction(function);
			argMng.setJob(job);
			argMng.setType(type);
			argMng.setFtrMin(ftrMin);
			argMng.setFtrMax(ftrMax);
			argMng.setDetMin(detMin);
			argMng.setDetMax(detMax);
			argMng.setFtrMinOp(ftrMinOp);
			argMng.setFtrMaxOp(ftrMaxOp);
			argMng.setDetMinOp(detMinOp);
			argMng.setDetMaxOp(detMaxOp);
			argMng.setAdjArg(adjArg);

			biz.saveArg(argMng);
			break;

		default:
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/bsc/argmng/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult delArgMngGrid(@RequestParam("id") Long id) {
		ArgMng argMng = biz.findOneArg(id);
		biz.deleteArg(argMng);
		return new JqSuccess();
	}
}
