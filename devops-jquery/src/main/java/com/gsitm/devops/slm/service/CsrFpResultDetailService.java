package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.CsrFpResultDetail;

public interface CsrFpResultDetailService extends
		SharedService<CsrFpResultDetail, Long> {
}
