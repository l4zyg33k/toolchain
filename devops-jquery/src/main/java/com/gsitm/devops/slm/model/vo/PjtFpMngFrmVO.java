package com.gsitm.devops.slm.model.vo;

public class PjtFpMngFrmVO extends PjtFpMngWebVO {

	private static final long serialVersionUID = 1L;

	public PjtFpMngFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}
}
