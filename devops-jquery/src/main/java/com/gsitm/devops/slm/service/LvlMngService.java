package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.LvlMng;

public interface LvlMngService extends SharedService<LvlMng, Long> {
}
