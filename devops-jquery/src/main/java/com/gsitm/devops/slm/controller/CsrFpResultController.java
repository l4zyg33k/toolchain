package com.gsitm.devops.slm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.model.CsrFpResult;
import com.gsitm.devops.slm.model.CsrFpResultDetail;
import com.gsitm.devops.slm.model.LvlMng;
import com.gsitm.devops.slm.model.QArgMng;
import com.gsitm.devops.slm.model.QCsrFpResult;
import com.gsitm.devops.slm.model.QLvlMng;
import com.gsitm.devops.slm.service.ArgMngService;
import com.gsitm.devops.slm.service.CsrFpResultService;
import com.gsitm.devops.slm.service.LvlMngService;
import com.mysema.query.BooleanBuilder;

/**
 * CSR FP 조정인자관리 컨트롤러
 */
@Controller
@Setter
public class CsrFpResultController implements MessageSourceAware {

	private static final String MENU_CODE = "CsrFp";
	
	private MessageSource messageSource;

	@Autowired
	private CodeService codeService;

	@Autowired
	private CsrFpResultService cfrService;

	@Autowired
	private LvlMngService clService;

	@Autowired
	private ArgMngService caaService;
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}		

	/**
	 * CSR FP 기본정보관리 화면 조회
	 * 
	 * @return
	 */
	@RequestMapping(value = "/slm/csrfp/csrfpresultList", method = RequestMethod.GET)
	public String index(Model model) {

		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));

		return "slm/csrfpmng/index";
	}

	/**
	 * CSR FP index 그리드 조회
	 * 
	 * @param standardYear
	 * @param demandFunction
	 * @param jobDivision
	 * @param functionDivision
	 * @param pageable
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/csrfpresult", method = RequestMethod.POST)
	@ResponseBody
	public Page<CsrFpResult> findAllGrid(
			@RequestParam(value = "standardYear", required = false) String standardYear,
			Pageable pageable) {
		QCsrFpResult qCsrFpResult = QCsrFpResult.csrFpResult;
		BooleanBuilder builider;
		if (standardYear == null || standardYear == "") {
			builider = new BooleanBuilder();
		} else {
			builider = new BooleanBuilder(qCsrFpResult.year.name.eq(standardYear));
		}
		Page<CsrFpResult> result = cfrService.findAll(builider, pageable);
		return result;
	}

	/**
	 * 신규폼 로드
	 * 
	 * @param model
	 * @param standardYear
	 * @return
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/slm/csrfpresult/{standardYear}/new", method = RequestMethod.GET)
	public String newbie(Model model,
			@PathVariable("standardYear") Code standardYear)
			throws JsonProcessingException {
		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));
		// CSR파트
		model.addAttribute("csrSystemCode",
				codeService.findAll(qCode.parent.value.eq("G4")));
		// 시스템특성
		model.addAttribute("systemTypeCode",
				codeService.findAll(qCode.parent.value.eq("H2")));
		// 요구기능
		model.addAttribute("demandFunctionCode",
				codeService.findAll(qCode.parent.value.eq("SLM10")));
		// 작업유형
		model.addAttribute("jobDivisionCode",
				codeService.findAll(qCode.parent.value.eq("SLM20")));

		// 그리드 콤보용 데이터 가져오기
		model.addAttribute("demandFunctionGrid",
				codeService.getJqGridSelectValue("SLM10"));
		model.addAttribute("jobDivisionGrid",
				codeService.getJqGridSelectValue("SLM20"));
		model.addAttribute("functionDivisionGrid",
				codeService.getJqGridSelectValue("SLM30"));
		model.addAttribute("levelGrid",
				codeService.getJqGridSelectValue("SLM40"));

		// FP계산 기준정보 가져오기
		QLvlMng lvlMng = QLvlMng.lvlMng;
		QArgMng argMng = QArgMng.argMng;
		Iterable<LvlMng> levelList = clService.findAll(lvlMng.year
				.eq(standardYear));
		Iterable<ArgMng> adjustArgumentList = caaService
				.findAll(argMng.year.eq(standardYear));
		model.addAttribute("levelList", levelList);
		model.addAttribute("adjustArgumentList", adjustArgumentList);

		CsrFpResult csrFpResult = new CsrFpResult();
		csrFpResult.setYear(standardYear);
		model.addAttribute("csrFpResultVO", csrFpResult);

		return "slm/csrfpmng/form";
	}

	/**
	 * 수정폼 로드
	 * 
	 * @param model
	 * @param csrFpResult
	 * @return
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/slm/csrfpresult/{id}/edit", method = RequestMethod.GET)
	public String edit(Model model, @PathVariable("id") CsrFpResult csrFpResult)
			throws JsonProcessingException {
		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearList",
				codeService.findAll(qCode.parent.value.eq("SLM50")));
		// CSR파트
		model.addAttribute("csrSystemCode",
				codeService.findAll(qCode.parent.value.eq("G4")));
		// 시스템특성
		model.addAttribute("systemTypeCode",
				codeService.findAll(qCode.parent.value.eq("H2")));
		// 요구기능
		model.addAttribute("demandFunctionCode",
				codeService.findAll(qCode.parent.value.eq("SLM10")));
		// 작업유형
		model.addAttribute("jobDivisionCode",
				codeService.findAll(qCode.parent.value.eq("SLM20")));

		model.addAttribute("demandFunctionGrid",
				codeService.getJqGridSelectValue("SLM10"));
		model.addAttribute("jobDivisionGrid",
				codeService.getJqGridSelectValue("SLM20"));
		model.addAttribute("functionDivisionGrid",
				codeService.getJqGridSelectValue("SLM30"));
		model.addAttribute("levelGrid",
				codeService.getJqGridSelectValue("SLM40"));

		// FP계산 기준정보 가져오기
		QLvlMng qCsrLevel = QLvlMng.lvlMng;
		QArgMng qCsrAdjustArgument = QArgMng.argMng;
		Iterable<LvlMng> levelList = clService.findAll(qCsrLevel.year
				.eq(csrFpResult.getYear()));
		Iterable<ArgMng> adjustArgumentList = caaService
				.findAll(qCsrAdjustArgument.year.eq(csrFpResult.getYear()));
		model.addAttribute("levelList", levelList);
		model.addAttribute("adjustArgumentList", adjustArgumentList);

		model.addAttribute("csrFpResultVO", csrFpResult);

		return "slm/csrfpmng/form";
	}

	/**
	 * 저장처리
	 * 
	 * @param model
	 * @param csrFpResultVO
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/csrfpResult/edit", method = RequestMethod.POST)
	public String create(Model model,
			@ModelAttribute("csrFpResultVO") @Valid CsrFpResult csrFpResultVO,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		System.out.println("start------------------");
		// 유효성체크
		if (result.hasErrors()) {

			QCode qCode = QCode.code;
			// 기준년도 리스트
			model.addAttribute("standardYearList",
					codeService.findAll(qCode.parent.value.eq("SLM50")));
			// CSR파트
			model.addAttribute("csrSystemCode",
					codeService.findAll(qCode.parent.value.eq("G4")));
			// 시스템특성
			model.addAttribute("systemTypeCode",
					codeService.findAll(qCode.parent.value.eq("H2")));
			// 요구기능
			model.addAttribute("demandFunctionCode",
					codeService.findAll(qCode.parent.value.eq("SLM10")));
			// 작업유형
			model.addAttribute("jobDivisionCode",
					codeService.findAll(qCode.parent.value.eq("SLM20")));

			model.addAttribute("demandFunctionGrid",
					codeService.getJqGridSelectValue("SLM10"));
			model.addAttribute("jobDivisionGrid",
					codeService.getJqGridSelectValue("SLM20"));
			model.addAttribute("functionDivisionGrid",
					codeService.getJqGridSelectValue("SLM30"));
			model.addAttribute("levelGrid",
					codeService.getJqGridSelectValue("SLM40"));

			model.addAttribute(model);
			return "slm/csrfpmng/form";
		}

		csrFpResultVO = cfrService.save(csrFpResultVO);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("save.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return String.format("redirect:/slm/csrfpresult/%d/edit",
				csrFpResultVO.getId());
	}

	/**
	 * 저장처리
	 * 
	 * @param model
	 * @param csrFpResultVO
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/csrfpResult/edit", method = RequestMethod.PUT)
	public String save(Model model,
			@ModelAttribute("csrFpResultVO") @Valid CsrFpResult csrFpResultVO,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		// 유효성체크
		if (result.hasErrors()) {

			QCode qCode = QCode.code;
			// 기준년도 리스트
			model.addAttribute("standardYearList",
					codeService.findAll(qCode.parent.value.eq("SLM50")));
			// CSR파트
			model.addAttribute("csrSystemCode",
					codeService.findAll(qCode.parent.value.eq("G4")));
			// 시스템특성
			model.addAttribute("systemTypeCode",
					codeService.findAll(qCode.parent.value.eq("H2")));
			// 요구기능
			model.addAttribute("demandFunctionCode",
					codeService.findAll(qCode.parent.value.eq("SLM10")));
			// 작업유형
			model.addAttribute("jobDivisionCode",
					codeService.findAll(qCode.parent.value.eq("SLM20")));

			model.addAttribute("demandFunctionGrid",
					codeService.getJqGridSelectValue("SLM10"));
			model.addAttribute("jobDivisionGrid",
					codeService.getJqGridSelectValue("SLM20"));
			model.addAttribute("functionDivisionGrid",
					codeService.getJqGridSelectValue("SLM30"));
			model.addAttribute("levelGrid",
					codeService.getJqGridSelectValue("SLM40"));

			model.addAttribute(model);
			return "slm/csrfpmng/form";
		}
		List<CsrFpResultDetail> detail = cfrService.findOne(
				csrFpResultVO.getId()).getDetails();
		csrFpResultVO.setDetails(detail);
		csrFpResultVO = cfrService.save(csrFpResultVO);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("save.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return String.format("redirect:/slm/csrfpresult/%d/edit",
				csrFpResultVO.getId());
	}

	/**
	 * 삭제처리
	 * 
	 * @param model
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "/slm/csrfpresult/{id}", method = RequestMethod.DELETE)
	public String delete(Model model, @PathVariable("id") Long id,
			final RedirectAttributes redirectAttributes) {

		cfrService.delete(id);
		Map<String, String> success = new HashMap<>();
		success.put("message", messageSource.getMessage("delete.success",
				new Object[] {}, Locale.getDefault()));
		redirectAttributes.addFlashAttribute("success", success);

		return "redirect:/slm/csrfp/csrfpresultList";
	}

	@RequestMapping(value = "/slm/csrfpresult/get/{id:\\d+}", method = RequestMethod.GET)
	@ResponseBody
	public CsrFpResult findOne(@PathVariable("id") CsrFpResult csrFpResult) {
		return csrFpResult;
	}
}
