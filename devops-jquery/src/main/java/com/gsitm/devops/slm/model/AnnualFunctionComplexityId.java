package com.gsitm.devops.slm.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.Data;

@Data
@Embeddable
public class AnnualFunctionComplexityId implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer year;

	@Enumerated(EnumType.STRING)
	private ComponentType component;
	
	@Enumerated(EnumType.STRING)
	private RangeType range;	
}
