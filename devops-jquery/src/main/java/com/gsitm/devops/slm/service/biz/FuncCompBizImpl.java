package com.gsitm.devops.slm.service.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Joiner;
import com.gsitm.devops.slm.model.AnnualFunctionComplexity;
import com.gsitm.devops.slm.model.AnnualFunctionComplexityId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.ElementType;
import com.gsitm.devops.slm.model.QAnnualElementRange;
import com.gsitm.devops.slm.model.QAnnualFunctionComplexity;
import com.gsitm.devops.slm.model.RangeType;
import com.gsitm.devops.slm.model.vo.FuncCompCmdVO;
import com.gsitm.devops.slm.model.vo.FuncCompColVO;
import com.gsitm.devops.slm.model.vo.FuncCompGrdVO;
import com.gsitm.devops.slm.model.vo.QFuncCompGrdVO;
import com.gsitm.devops.slm.repository.AnnualFunctionComplexityRepository;
import com.mysema.query.SearchResults;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
public class FuncCompBizImpl implements FuncCompBiz {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AnnualFunctionComplexityRepository repository;

	@Override
	public FuncCompColVO getColModel(Integer year, ComponentType component) {
		FuncCompColVO vo = new FuncCompColVO();
		ElementType el = ElementType.FTR;

		switch (component) {
		case EI:
		case EO:
		case EQ:
			vo.setRangeLabel("FTR 구간");
			el = ElementType.FTR;
			break;
		case ILF:
		case EIF:
			vo.setRangeLabel("RET 구간");
			el = ElementType.RET;
			break;
		default:
			break;
		}

		QAnnualElementRange aer = QAnnualElementRange.annualElementRange;

		JPAQuery queryForFtrRet = new JPAQuery(em).from(aer).where(aer.id.year.eq(year), aer.id.component.eq(component),
				aer.id.element.eq(el));

		Tuple ftrRet = queryForFtrRet.singleResult(aer.lessThan, aer.greaterThan);

		JPAQuery queryForDet = new JPAQuery(em).from(aer).where(aer.id.year.eq(year), aer.id.component.eq(component),
				aer.id.element.eq(ElementType.DET));

		Tuple det = queryForDet.singleResult(aer.lessThan, aer.greaterThan);

		List<RangeType> rangeType = EnumUtils.getEnumList(RangeType.class);
		Map<RangeType, String> rangeMap = new HashMap<>();

		for (RangeType range : rangeType) {
			switch (range) {
			case LESSTHAN:
				rangeMap.put(range, String.format("%s %d개 미만", el, ftrRet.get(aer.lessThan)));
				break;
			case CLOSED:
				rangeMap.put(range,
						String.format("%s %d개 이상, %d개 이하", el, ftrRet.get(aer.lessThan), ftrRet.get(aer.greaterThan)));
				break;
			case GREATERTHAN:
				rangeMap.put(range, String.format("%s %d개 초과", el, ftrRet.get(aer.greaterThan)));
				break;
			default:
				break;
			}
		}

		String rangeValue = Joiner.on(";").withKeyValueSeparator(":").join(rangeMap);

		vo.setRangeValue(rangeValue);
		vo.setLessThanLabel(String.format("DET %d개 미만", det.get(aer.lessThan)));
		vo.setClosedLabel(String.format("DET %d개 이상, %d개 이하", det.get(aer.lessThan), det.get(aer.greaterThan)));
		vo.setGreaterThanLabel(String.format("DET %d개 초과", det.get(aer.greaterThan)));

		return vo;
	}

	@Override
	public Page<FuncCompGrdVO> findAllFuncComp(Integer year, ComponentType component, Pageable pageable) {
		QAnnualFunctionComplexity afc = QAnnualFunctionComplexity.annualFunctionComplexity;

		JPAQuery query = new JPAQuery(em).from(afc).where(afc.id.year.eq(year), afc.id.component.eq(component));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<AnnualFunctionComplexity>(AnnualFunctionComplexity.class, "annualFunctionComplexity"));
		querydsl.applyPagination(pageable, query);

		SearchResults<FuncCompGrdVO> search = query.listResults(new QFuncCompGrdVO(afc.id.year, afc.id.component,
				afc.id.range, afc.lessThan, afc.closed, afc.greaterThan));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Transactional
	@Override
	public void createFuncComp(AnnualFunctionComplexityId id, FuncCompCmdVO vo) throws DataAccessException {
		AnnualFunctionComplexity entity = new AnnualFunctionComplexity();
		entity.setId(id);
		BeanUtils.copyProperties(vo, entity);
		em.persist(entity);
	}

	@Transactional
	@Override
	public void updateFuncComp(AnnualFunctionComplexityId id, FuncCompCmdVO vo) throws DataAccessException {
		AnnualFunctionComplexity entity = repository.findOne(id);
		BeanUtils.copyProperties(vo, entity);
		repository.save(entity);
	}

	@Transactional
	@Override
	public void removeFuncComp(AnnualFunctionComplexityId id) throws DataAccessException {
		repository.delete(id);
	}
}
