package com.gsitm.devops.slm.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.ProjectFpResult;
import com.gsitm.devops.slm.model.QProjectFpResult;
import com.gsitm.devops.slm.model.vo.PjtFpRstCmdVO;
import com.gsitm.devops.slm.model.vo.PjtFpMngFrmVO;
import com.gsitm.devops.slm.model.vo.PjtFpMngGrdVO;
import com.gsitm.devops.slm.model.vo.QPjtFpMngGrdVO;
import com.gsitm.devops.slm.service.ProjectFpResultService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
public class PjtFpMngBizImpl implements PjtFpMngBiz {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CodeService codeService;

	@Autowired
	private ProjectFpResultService pfrService;

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		if (authorities.contains("ROLE_USER") || authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public void createPjtFpRst(PjtFpRstCmdVO vo) {
		ProjectFpResult pfr = new ProjectFpResult();
		BeanUtils.copyProperties(vo, pfr);
		pfrService.save(pfr);
	}

	@Override
	public void updatePjtFpRst(Long id, PjtFpRstCmdVO vo) {
		ProjectFpResult pfr = pfrService.findOne(id);
		BeanUtils.copyProperties(vo, pfr);
		pfrService.save(pfr);
	}

	@Override
	public PjtFpMngFrmVO findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<PjtFpMngGrdVO> findAllPjtFpRst(Pageable pageable) {

		QProjectFpResult pfr = QProjectFpResult.projectFpResult;

		JPAQuery query = new JPAQuery(em).from(pfr);

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectFpResult>(ProjectFpResult.class, "projectFpResult"));
		querydsl.applyPagination(pageable, query);

		SearchResults<PjtFpMngGrdVO> search = query
				.listResults(new QPjtFpMngGrdVO(pfr.id, pfr.year.id, pfr.project.code, pfr.project.name, pfr.part.id,
						pfr.op.username, pfr.op.name, pfr.system.id, pfr.fp, pfr.mh));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public String getYearsVals() {
		return Joiner.on(";").withKeyValueSeparator(":").join(codeService.getOptions("SLM50"));
	}

	@Override
	public String getPartVals() {
		return Joiner.on(";").withKeyValueSeparator(":").join(codeService.getOptions("G4"));
	}

	@Override
	public String getSystemVals() {
		return Joiner.on(";").withKeyValueSeparator(":").join(codeService.getOptions("SLM60"));
	}

	@Override
	public void removePjtFpRst(Long id) {
		pfrService.delete(id);
	}
}
