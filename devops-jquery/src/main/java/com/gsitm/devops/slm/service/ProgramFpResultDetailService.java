package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.ProgramFpResultDetail;

public interface ProgramFpResultDetailService extends
		SharedService<ProgramFpResultDetail, Long> {
}
