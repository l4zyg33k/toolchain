package com.gsitm.devops.slm.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Productivity extends AbstractAuditable<Account, Long> {

	/**
	 * 기준 년도 (코드)
	 */
	@ManyToOne
	private Code year;

	/**
	 * CSR 파트 (코드)
	 */
	@ManyToOne
	private Code part;

	/**
	 * 시스템 특성 (코드)
	 */
	@ManyToOne
	private Code system;

	/**
	 * 생산성
	 */
	private Float point;
}
