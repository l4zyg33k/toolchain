package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.slm.model.ComponentType;

import lombok.Data;

@Data
public class ElemRangWebVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer year;
	
	private ComponentType component;
}
