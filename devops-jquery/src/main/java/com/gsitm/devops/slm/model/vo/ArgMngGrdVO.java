package com.gsitm.devops.slm.model.vo;

import lombok.Data;

import com.mysema.query.annotations.QueryProjection;

@Data
public class ArgMngGrdVO {
	// 기본정보 > 조정인자관리 그리드용 VO
	private Long id;
	private Long year;
	private Long function;
	private Long job;
	private Long type;
	private Float ftrMin;
	private Long ftrMinOp;
	private Float ftrMax;
	private Long ftrMaxOp;
	private Float detMin;
	private Long detMinOp;
	private Float detMax;
	private Long detMaxOp;
	private Float adjArg;

	@QueryProjection
	public ArgMngGrdVO(Long id, Long year, Long function, Long job, Long type,
			Float ftrMin, Long ftrMinOp, Float ftrMax, Long ftrMaxOp,
			Float detMin, Long detMinOp, Float detMax, Long detMaxOp,
			Float adjArg) {
		super();
		this.id = id;
		this.year = year;
		this.function = function;
		this.job = job;
		this.type = type;
		this.ftrMin = ftrMin;
		this.ftrMinOp = ftrMinOp;
		this.ftrMax = ftrMax;
		this.ftrMaxOp = ftrMaxOp;
		this.detMin = detMin;
		this.detMinOp = detMinOp;
		this.detMax = detMax;
		this.detMaxOp = detMaxOp;
		this.adjArg = adjArg;
	}
}
