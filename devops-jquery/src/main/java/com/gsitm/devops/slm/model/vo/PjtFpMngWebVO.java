package com.gsitm.devops.slm.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

public class PjtFpMngWebVO extends SharedWebVO<Long> {

	private static final long serialVersionUID = 1L;

	public PjtFpMngWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}
}
