package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.CsrFpResultDetail;

@Repository
public interface CsrFpResultDetailRepository extends
		SharedRepository<CsrFpResultDetail, Long> {
}
