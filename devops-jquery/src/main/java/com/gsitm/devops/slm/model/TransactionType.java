package com.gsitm.devops.slm.model;

public enum TransactionType {
	EI, EO, EQ
}
