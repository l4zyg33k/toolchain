package com.gsitm.devops.slm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.slm.model.AnnualFunctionComplexityId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.vo.FuncCompCmdVO;
import com.gsitm.devops.slm.model.vo.FuncCompColVO;
import com.gsitm.devops.slm.model.vo.FuncCompGrdVO;
import com.gsitm.devops.slm.service.biz.FuncCompBiz;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class FuncCompRestController {

	@Autowired
	private FuncCompBiz biz;

	@RequestMapping(value = "/rest/slm/funccompmng/colmodel", method = RequestMethod.POST)
	public FuncCompColVO getColModel(@RequestParam("year") Integer year,
			@RequestParam("component") ComponentType component) {
		return biz.getColModel(year, component);
	}
	
	@RequestMapping(value = "/rest/slm/funccompmng", method = RequestMethod.POST)
	public Page<FuncCompGrdVO> findAllfunccompmng(@RequestParam("year") Integer year,
			@RequestParam("component") ComponentType component, Pageable pageable) {
		return biz.findAllFuncComp(year, component, pageable);
	}

	@RequestMapping(value = "/rest/slm/funccompmng/edit", method = RequestMethod.POST)
	public JqResult editFuncCompMng(@RequestParam("oper") String oper, AnnualFunctionComplexityId id,
			@Valid FuncCompCmdVO vo, BindingResult result) {
		log.debug(id.toString());
		log.debug(vo.toString());
		try {
			switch (oper) {
			case "add":
				biz.createFuncComp(id, vo);
				break;
			case "edit":
				biz.updateFuncComp(id, vo);
				break;
			default:
				break;
			}
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/funccompmng/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult removeFuncCompMng(AnnualFunctionComplexityId id) {
		log.debug(id.toString());
		try {
			biz.removeFuncComp(id);
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}	
}
