package com.gsitm.devops.slm.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.slm.service.biz.PjtFpMngBiz;

@Controller
public class PjtFpMngController {

	private static final String MENU_CODE = "PjtFpMng";
	private static final String ID = "id";

	private static final String VIEW_URL_BY_ID = "/{id}/view";

	private static final String INDEX_PAGE = "slm/pjtfpmng/index";
	private static final String VIEW_PAGE = "slm/pjtfpmng/view";

	@Autowired
	private PjtFpMngBiz biz;
	
	@ModelAttribute("yearsVals")
	public String getYearsVals() {
		return biz.getYearsVals();
	}
	
	@ModelAttribute("partVals")
	public String getPartVals() {
		return biz.getPartVals();
	}
	
	@ModelAttribute("systemVals")
	public String getSystemVals() {
		return biz.getSystemVals();
	}			

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@RequestMapping(value = "/slm/pjtfpmng", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = "/slm/pjtfpmng", method = RequestMethod.GET, params = { PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = VIEW_URL_BY_ID, method = RequestMethod.GET, params = { PageParams.PAGE, PageParams.ROW_NUM,
			PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId, @PathVariable(ID) String code, Model model) {
		/*
		PjtFpMngFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		*/
		return VIEW_PAGE;
	}
}
