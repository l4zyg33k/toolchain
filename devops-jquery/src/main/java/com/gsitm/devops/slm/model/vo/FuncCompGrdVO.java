package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.slm.model.ComplexityType;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.RangeType;
import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

@Data
public class FuncCompGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer year;

	private ComponentType component;

	private RangeType range;

	private ComplexityType lessThan;

	private ComplexityType closed;

	private ComplexityType greaterThan;

	@QueryProjection
	public FuncCompGrdVO(Integer year, ComponentType component, RangeType range, ComplexityType lessThan,
			ComplexityType closed, ComplexityType greaterThan) {
		super();
		this.year = year;
		this.component = component;
		this.range = range;
		this.lessThan = lessThan;
		this.closed = closed;
		this.greaterThan = greaterThan;
	}
}
