package com.gsitm.devops.slm.model.vo;

import lombok.Data;

import com.mysema.query.annotations.QueryProjection;

@Data
public class LvlMngGrdVO {

	private Long id;
	private Long year;
	private Long function;
	private Long job;
	private Long type;
	private Integer ftrMin;
	private Long ftrMinOp;
	private Integer ftrMax;
	private Long ftrMaxOp;
	private Integer detMin;
	private Long detMinOp;
	private Integer detMax;
	private Long detMaxOp;
	private Long level;
	private Float fp;

	@QueryProjection
	public LvlMngGrdVO(Long id, Long year, Long function, Long job, Long type,
			Integer ftrMin, Long ftrMinOp, Integer ftrMax, Long ftrMaxOp,
			Integer detMin, Long detMinOp, Integer detMax, Long detMaxOp,
			Long level, Float fp) {
		super();
		this.id = id;
		this.year = year;
		this.function = function;
		this.job = job;
		this.type = type;
		this.ftrMin = ftrMin;
		this.ftrMinOp = ftrMinOp;
		this.ftrMax = ftrMax;
		this.ftrMaxOp = ftrMaxOp;
		this.detMin = detMin;
		this.detMinOp = detMinOp;
		this.detMax = detMax;
		this.detMaxOp = detMaxOp;
		this.level = level;
		this.fp = fp;
	}
}
