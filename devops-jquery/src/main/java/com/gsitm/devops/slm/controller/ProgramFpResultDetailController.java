package com.gsitm.devops.slm.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.slm.model.ProgramFpResult;
import com.gsitm.devops.slm.model.ProgramFpResultDetail;
import com.gsitm.devops.slm.model.QProgramFpResultDetail;
import com.gsitm.devops.slm.service.ProgramFpResultDetailService;
import com.mysema.query.BooleanBuilder;

/**
 * CSR FP 조정인자관리 컨트롤러
 */
@Controller
public class ProgramFpResultDetailController {

	@Autowired
	private ProgramFpResultDetailService pfrdService;

	/**
	 * 그리드 상세조회
	 * 
	 * @param projectinspection
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/programfpResultDetail/{id:\\d+}", method = RequestMethod.POST)
	@ResponseBody
	public Iterable<ProgramFpResultDetail> findAll(
			@PathVariable("id") ProgramFpResult programFpResult,
			@RequestParam(value = "demandFunction", required = false) Code demandFunction,
			@RequestParam(value = "jobDivision", required = false) Code jobDivision,
			Pageable pageable) {

		QProgramFpResultDetail qProgramFpResultDetail = QProgramFpResultDetail.programFpResultDetail;

		BooleanBuilder builider = new BooleanBuilder(
				qProgramFpResultDetail.programFpResult.eq(programFpResult));

		if (demandFunction != null) {
			builider = builider.and(qProgramFpResultDetail.function
					.eq(demandFunction));
		}
		if (jobDivision != null) {
			builider = builider.and(qProgramFpResultDetail.job.eq(jobDivision));
		}

		Iterable<ProgramFpResultDetail> t = pfrdService.findAll(builider,
				pageable);
		return t;
		// return programFpResult.getProgramFpResultDetail();
	}

	/**
	 * 난이도 그리드 수정
	 * 
	 * @param id
	 * @param ftrChangeRateMin
	 * @param ftrChangeRateMax
	 * @param detChangeRateMin
	 * @param detChangeRateMax
	 * @param adjustArgument
	 * @param oper
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/rest/slm/programfpResultDetail/{programFpResultId}/edit", method = RequestMethod.POST)
	@ResponseBody
	public String editGrid(
			@PathVariable("programFpResultId") ProgramFpResult programFpResult,
			@RequestParam("id") String id,
			@RequestParam(value = "demandFunction.name", required = false) Code demandFunction,
			@RequestParam(value = "jobDivision.name", required = false) Code jobDivision,
			@RequestParam(value = "functionDivision.name", required = false) Code functionDivision,
			@RequestParam(value = "functionName", required = false) String functionName,
			@RequestParam(value = "changeFtrCount", defaultValue = "") Integer changeFtrCount,
			@RequestParam(value = "changeDetCount", defaultValue = "") Integer changeDetCount,
			@RequestParam(value = "beforeFtrCount", defaultValue = "") Integer beforeFtrCount,
			@RequestParam(value = "beforeDetCount", defaultValue = "") Integer beforeDetCount,
			@RequestParam(value = "afterFtrCount", defaultValue = "") Integer afterFtrCount,
			@RequestParam(value = "afterDetCount", defaultValue = "") Integer afterDetCount,
			@RequestParam(value = "addChangeFtr", defaultValue = "") Integer addChangeFtr,
			@RequestParam(value = "editChangeFtr", defaultValue = "") Integer editChangeFtr,
			@RequestParam(value = "delChangeFtr", defaultValue = "") Integer delChangeFtr,
			@RequestParam(value = "addChangeDet", defaultValue = "") Integer addChangeDet,
			@RequestParam(value = "editChangeDet", defaultValue = "") Integer editChangeDet,
			@RequestParam(value = "delChangeDet", defaultValue = "") Integer delChangeDet,
			@RequestParam(value = "fpMark", defaultValue = "") Float fpMark,
			@RequestParam(value = "level.name", required = false) Code level,
			@RequestParam(value = "adjustArgument", defaultValue = "") Float adjustArgument,
			@RequestParam(value = "adjustFpMark", defaultValue = "") Float adjustFpMark,
			@RequestParam("oper") String oper) throws ParseException {

		ProgramFpResultDetail programFpResultDetail;

		switch (oper) {
		case "add":
			programFpResultDetail = new ProgramFpResultDetail();

			programFpResultDetail.setProgramFpResult(programFpResult);
			programFpResultDetail.setFunction(demandFunction);
			programFpResultDetail.setJob(jobDivision);
			programFpResultDetail.setType(functionDivision);
			programFpResultDetail.setName(functionName);
			programFpResultDetail.setChgFtrCnt(changeFtrCount);
			programFpResultDetail.setChgDetCnt(changeDetCount);
			programFpResultDetail.setBefFtrCnt(beforeFtrCount);
			programFpResultDetail.setBefDetCnt(beforeDetCount);
			programFpResultDetail.setAftFtrCnt(afterFtrCount);
			programFpResultDetail.setAftDetCnt(afterDetCount);
			programFpResultDetail.setAddChgFtr(addChangeFtr);
			programFpResultDetail.setModChgFtr(editChangeFtr);
			programFpResultDetail.setDelChgFtr(delChangeFtr);
			programFpResultDetail.setAddChgDet(addChangeDet);
			programFpResultDetail.setModChgDet(editChangeDet);
			programFpResultDetail.setDelChgDet(delChangeDet);
			programFpResultDetail.setFp(fpMark);
			programFpResultDetail.setLevel(level);
			programFpResultDetail.setAdjArg(adjustArgument);
			programFpResultDetail.setAdjFp(adjustFpMark);
			pfrdService.save(programFpResultDetail);
			break;
		case "edit":
			programFpResultDetail = pfrdService.findOne(Long.parseLong(id));
			programFpResultDetail.setName(functionName);
			programFpResultDetail.setChgFtrCnt(changeFtrCount);
			programFpResultDetail.setChgDetCnt(changeDetCount);
			programFpResultDetail.setBefFtrCnt(beforeFtrCount);
			programFpResultDetail.setBefDetCnt(beforeDetCount);
			programFpResultDetail.setAftFtrCnt(afterFtrCount);
			programFpResultDetail.setAftDetCnt(afterDetCount);
			programFpResultDetail.setAddChgFtr(addChangeFtr);
			programFpResultDetail.setModChgFtr(editChangeFtr);
			programFpResultDetail.setDelChgFtr(delChangeFtr);
			programFpResultDetail.setAddChgDet(addChangeDet);
			programFpResultDetail.setModChgDet(editChangeDet);
			programFpResultDetail.setDelChgDet(delChangeDet);
			programFpResultDetail.setFp(fpMark);
			programFpResultDetail.setLevel(level);
			programFpResultDetail.setAdjArg(adjustArgument);
			programFpResultDetail.setAdjFp(adjustFpMark);

			pfrdService.save(programFpResultDetail);
			break;

		case "del":

			// ProgramFpResultDetail detail =
			// pfrdService.findOne(Long.parseLong(id));
			// programFpResult.getProgramFpResultDetail().remove(detail);
			// serviceFactory.getProgramFpResultService().save(programFpResult);
			pfrdService.delete(Long.parseLong(id));
			break;
		}

		return null;
	}

}
