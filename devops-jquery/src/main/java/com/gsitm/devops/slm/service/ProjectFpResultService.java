package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.ProjectFpResult;

public interface ProjectFpResultService extends
		SharedService<ProjectFpResult, Long> {
	public ProjectFpResult saveResultFp(ProjectFpResult projectFpResult);
}
