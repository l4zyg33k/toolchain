package com.gsitm.devops.slm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.Productivity;
import com.gsitm.devops.slm.model.ProgramFpResult;
import com.gsitm.devops.slm.model.ProgramFpResultDetail;
import com.gsitm.devops.slm.model.ProjectFpResult;
import com.gsitm.devops.slm.model.QProductivity;
import com.gsitm.devops.slm.repository.ProgramFpResultDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProgramFpResultDetailServiceImpl implements
		ProgramFpResultDetailService {

	@Autowired
	private ProgramFpResultDetailRepository repository;

	@Autowired
	private ProgramFpResultService programFpResultService;

	@Autowired
	private ProjectFpResultService projectFpResultService;

	@Autowired
	private ProductivityService productivityService;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Autowired
	private CodeService codeService;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public ProgramFpResultDetail findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProgramFpResultDetail findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProgramFpResultDetail> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProgramFpResultDetail> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProgramFpResultDetail> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProgramFpResultDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProgramFpResultDetail> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProgramFpResultDetail> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public ProgramFpResultDetail save(
			ProgramFpResultDetail programFpResultDetail) {

		ProgramFpResultDetail result = repository.save(programFpResultDetail);
		// FP, 예상투입MH계산
		setFpResultCalculation(result.getProgramFpResult());
		return result;
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public <S extends ProgramFpResultDetail> List<S> save(Iterable<S> iterable) {
		List<S> result = repository.save(iterable);
		for (S s : iterable) {
			setFpResultCalculation(s.getProgramFpResult());
		}
		return result;
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public void delete(Long id) {
		ProgramFpResultDetail programFpResultDetail = this.findOne(id);
		ProgramFpResult programFpResult = programFpResultDetail
				.getProgramFpResult();
		programFpResult.getDetails().remove(programFpResultDetail);
		ProgramFpResult result = programFpResultService.save(programFpResult);
		setFpResultCalculation(result);

		// repository.delete(id);
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public void delete(ProgramFpResultDetail programFpResultDetail) {
		ProgramFpResult programFpResult = programFpResultDetail
				.getProgramFpResult();
		programFpResult.getDetails().remove(programFpResultDetail);
		ProgramFpResult result = programFpResultService.save(programFpResult);
		setFpResultCalculation(result);
		// repository.delete(programFpResultDetail);

	}

	/**
	 * 사용안함(FP, MH변경되도록 수정 후 사용 가능)
	 */
	@Transactional
	@Override
	public void delete(Iterable<? extends ProgramFpResultDetail> iterable) {
		// repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Transactional
	public void setFpResultCalculation(ProgramFpResult programFpResult) {
		ProjectFpResult projectFpResult = programFpResult.getProjectFpResult();

		QProductivity qProductivity = QProductivity.productivity;
		Productivity productivity = productivityService
				.findOne(qProductivity.year
						.eq(projectFpResult.getYear())
						.and(qProductivity.part.eq(projectFpResult.getPart()))
						.and(qProductivity.system.eq(projectFpResult
								.getSystem())));

		// prgramFP FP계산
		Float programFP = 0F;
		for (ProgramFpResultDetail detail : programFpResult.getDetails()) {
			programFP += detail.getAdjFp();
		}
		// prgramFP FP계산
		Float programMH = (float) (Math.round(programFP
				/ productivity.getPoint() * 100) / 100F);
		programFpResult.setFp(programFP);
		programFpResult.setMh(programMH);
		programFpResultService.save(programFpResult);

		// projectFP FP계산
		Float projectFP = 0F;
		for (ProgramFpResult program : projectFpResult.getPrograms()) {
			for (ProgramFpResultDetail detail : program.getDetails()) {
				projectFP += detail.getAdjFp();
			}
		}
		// projectFP FP계산
		Float projectMH = (float) (Math.round(projectFP
				/ productivity.getPoint() * 100) / 100F);
		projectFpResult.setFp(projectFP);
		projectFpResult.setMh(projectMH);
		projectFpResultService.save(projectFpResult);
	}
}
