package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.ProgramFpResult;

public interface ProgramFpResultService extends
		SharedService<ProgramFpResult, Long> {
}
