package com.gsitm.devops.slm.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.slm.model.CsrFpResult;
import com.gsitm.devops.slm.model.CsrFpResultDetail;
import com.gsitm.devops.slm.model.QCsrFpResultDetail;
import com.gsitm.devops.slm.service.CsrFpResultDetailService;
import com.gsitm.devops.slm.service.CsrFpResultService;
import com.mysema.query.BooleanBuilder;

/**
 * CSR FP 조정인자관리 컨트롤러
 */
@Controller
public class CsrFpResultDetailController {
	
	@Autowired
	private CsrFpResultDetailService cfrdService;
	
	@Autowired
	private CsrFpResultService cfrService;

	/**
	 * 그리드 상세조회
	 * 
	 * @param projectinspection
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/csrfpResultDetail/{id:\\d+}", method = RequestMethod.POST)
	@ResponseBody
	public Iterable<CsrFpResultDetail> findAll(
			@PathVariable("id") CsrFpResult csrFpResult,
			@RequestParam(value = "demandFunction", required = false) Code demandFunction,
			@RequestParam(value = "jobDivision", required = false) Code jobDivision,
			Pageable pageable) {

		QCsrFpResultDetail qCsrFpResultDetail = QCsrFpResultDetail.csrFpResultDetail;

		BooleanBuilder builider = new BooleanBuilder(
				qCsrFpResultDetail.csrFpResult.eq(csrFpResult));

		if (demandFunction != null) {
			builider = builider.and(qCsrFpResultDetail.function
					.eq(demandFunction));
		}
		if (jobDivision != null) {
			builider = builider.and(qCsrFpResultDetail.job.eq(jobDivision));
		}

		Iterable<CsrFpResultDetail> t = cfrdService.findAll(builider, pageable);
		return t;
		// return csrFpResult.getCsrFpResultDetail();
	}

	/**
	 * 난이도 그리드 수정
	 * 
	 * @param id
	 * @param ftrChangeRateMin
	 * @param ftrChangeRateMax
	 * @param detChangeRateMin
	 * @param detChangeRateMax
	 * @param adjustArgument
	 * @param oper
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/rest/slm/csrfpResultDetail/{csrFpResultId}/edit", method = RequestMethod.POST)
	@ResponseBody
	public String editGrid(
			@PathVariable("csrFpResultId") CsrFpResult csrFpResult,
			@RequestParam("id") String id,
			@RequestParam(value = "demandFunction.name", required = false) Code demandFunction,
			@RequestParam(value = "jobDivision.name", required = false) Code jobDivision,
			@RequestParam(value = "functionDivision.name", required = false) Code functionDivision,
			@RequestParam(value = "functionName", required = false) String functionName,
			@RequestParam(value = "changeFtrCount", defaultValue = "") Integer changeFtrCount,
			@RequestParam(value = "changeDetCount", defaultValue = "") Integer changeDetCount,
			@RequestParam(value = "beforeFtrCount", defaultValue = "") Integer beforeFtrCount,
			@RequestParam(value = "beforeDetCount", defaultValue = "") Integer beforeDetCount,
			@RequestParam(value = "afterFtrCount", defaultValue = "") Integer afterFtrCount,
			@RequestParam(value = "afterDetCount", defaultValue = "") Integer afterDetCount,
			@RequestParam(value = "addChangeFtr", defaultValue = "") Integer addChangeFtr,
			@RequestParam(value = "editChangeFtr", defaultValue = "") Integer editChangeFtr,
			@RequestParam(value = "delChangeFtr", defaultValue = "") Integer delChangeFtr,
			@RequestParam(value = "addChangeDet", defaultValue = "") Integer addChangeDet,
			@RequestParam(value = "editChangeDet", defaultValue = "") Integer editChangeDet,
			@RequestParam(value = "delChangeDet", defaultValue = "") Integer delChangeDet,
			@RequestParam(value = "fpMark", defaultValue = "") Float fpMark,
			@RequestParam(value = "level.name", required = false) Code level,
			@RequestParam(value = "adjustArgument", defaultValue = "") Float adjustArgument,
			@RequestParam(value = "adjustFpMark", defaultValue = "") Float adjustFpMark,
			@RequestParam("oper") String oper) throws ParseException {

		CsrFpResultDetail csrFpResultDetail;

		switch (oper) {
		case "add":
			csrFpResultDetail = new CsrFpResultDetail();

			csrFpResultDetail.setCsrFpResult(csrFpResult);
			csrFpResultDetail.setFunction(demandFunction);
			csrFpResultDetail.setJob(jobDivision);
			csrFpResultDetail.setType(functionDivision);
			csrFpResultDetail.setName(functionName);
			csrFpResultDetail.setChgFtrCnt(changeFtrCount);
			csrFpResultDetail.setChgDetCnt(changeDetCount);
			csrFpResultDetail.setBefFtrCnt(beforeFtrCount);
			csrFpResultDetail.setBefDetCnt(beforeDetCount);
			csrFpResultDetail.setAftFtrCnt(afterFtrCount);
			csrFpResultDetail.setAftDetCnt(afterDetCount);
			csrFpResultDetail.setAddChgFtr(addChangeFtr);
			csrFpResultDetail.setModChgFtr(editChangeFtr);
			csrFpResultDetail.setDelChgFtr(delChangeFtr);
			csrFpResultDetail.setAddChgDet(addChangeDet);
			csrFpResultDetail.setModChgDet(editChangeDet);
			csrFpResultDetail.setDelChgDet(delChangeDet);
			csrFpResultDetail.setFp(fpMark);
			csrFpResultDetail.setLevel(level);
			csrFpResultDetail.setAdjArg(adjustArgument);
			csrFpResultDetail.setAdjFp(adjustFpMark);
			cfrdService
					.save(csrFpResultDetail);
			break;
		case "edit":
			csrFpResultDetail = cfrdService
					.findOne(Long.parseLong(id));
			csrFpResultDetail.setName(functionName);
			csrFpResultDetail.setChgFtrCnt(changeFtrCount);
			csrFpResultDetail.setChgDetCnt(changeDetCount);
			csrFpResultDetail.setBefFtrCnt(beforeFtrCount);
			csrFpResultDetail.setBefDetCnt(beforeDetCount);
			csrFpResultDetail.setAftFtrCnt(afterFtrCount);
			csrFpResultDetail.setAftDetCnt(afterDetCount);
			csrFpResultDetail.setAddChgFtr(addChangeFtr);
			csrFpResultDetail.setModChgFtr(editChangeFtr);
			csrFpResultDetail.setDelChgFtr(delChangeFtr);
			csrFpResultDetail.setAddChgDet(addChangeDet);
			csrFpResultDetail.setModChgDet(editChangeDet);
			csrFpResultDetail.setDelChgDet(delChangeDet);
			csrFpResultDetail.setFp(fpMark);
			csrFpResultDetail.setLevel(level);
			csrFpResultDetail.setAdjArg(adjustArgument);
			csrFpResultDetail.setAdjFp(adjustFpMark);

			cfrdService
					.save(csrFpResultDetail);
			break;

		case "del":
			CsrFpResultDetail detail = cfrdService.findOne(Long.parseLong(id));
			csrFpResult.getDetails().remove(detail);
			cfrService.save(csrFpResult);
			// cfrdService.delete(Long.parseLong(id));
			break;
		}

		return null;
	}

}
