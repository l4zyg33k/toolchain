package com.gsitm.devops.slm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.slm.model.ArgMng;
import com.gsitm.devops.slm.repository.ArgMngRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ArgMngServiceImpl implements ArgMngService {

	@Autowired
	private ArgMngRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public ArgMng findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ArgMng findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ArgMng> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ArgMng> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ArgMng> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ArgMng> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ArgMng> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ArgMng> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ArgMng save(ArgMng document) {
		return repository.save(document);
	}

	@Transactional
	@Override
	public <S extends ArgMng> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ArgMng document) {
		repository.delete(document);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ArgMng> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	/**
	 * 일괄적으로 조정인자 리스트 생성
	 */
	/* 삭제대상 주석처리 김정인 20141105
	@Override
	@Transactional
	public String create(String year) {

		QCode qCode = QCode.code;

		Iterable<LvlMng> dataList = repositoryLevel.findAll();

		Code codeM = repositoryCode.findOne(qCode.name.eq("이상"));
		Code codeL = repositoryCode.findOne(qCode.name.eq("이하"));
		Code codeL2 = repositoryCode.findOne(qCode.name.eq("미만"));

		for (LvlMng t : dataList) {
			t.setFtrMaxOp(codeL.getValue());
			t.setFtrMinOp(codeM.getValue());
			t.setDetMaxOp(codeL.getValue());
			t.setDetMinOp(codeM.getValue());

			repositoryLevel.save(t);
		}

		Iterable<ArgMng> dataListAd = repository.findAll();
		
		for (ArgMng t1 : dataListAd) {
			t1.setFtrMinOp(codeM.getValue());
			t1.setFtrMaxOp(codeL2.getValue());
			t1.setDetMinOp(codeM.getValue());
			t1.setDetMaxOp(codeL2.getValue());

			repository.save(t1);
		}

		return "success";

	}
	*/
}
