package com.gsitm.devops.slm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class CsrFpResult extends AbstractAuditable<Account, Long> {

	/**
	 * 기준 년도 (코드)
	 */
	@ManyToOne
	private Code year;

	/**
	 * CSR 번호
	 */
	@Column(length = 24)
	private String no;

	/**
	 * CSR 제목
	 */
	@Column(length = 64)
	private String title;

	/**
	 * CSR 파트 (코드)
	 */
	@ManyToOne
	private Code part;

	/**
	 * CSR 운영 시스템 (코드)
	 */
	@ManyToOne
	private Code system;

	/**
	 * 처리자
	 */
	@ManyToOne
	private Account op;

	/**
	 * 시스템 특성 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * 최종 기능 점수
	 */
	private Float fp;

	/**
	 * 예상 투입 MH
	 */
	private Float mh;

	/**
	 * CSR 기능 점수 상세
	 */
	@OneToMany(mappedBy = "csrFpResult", orphanRemoval = true)
	private List<CsrFpResultDetail> details;
}
