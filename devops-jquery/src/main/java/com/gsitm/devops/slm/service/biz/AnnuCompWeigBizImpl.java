package com.gsitm.devops.slm.service.biz;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.slm.model.AnnualComponentWeightId;
import com.gsitm.devops.slm.model.AnnualComponentWeight;
import com.gsitm.devops.slm.model.QAnnualComponentWeight;
import com.gsitm.devops.slm.model.vo.CompWeigMngCmdVO;
import com.gsitm.devops.slm.model.vo.CompWeigMngGrdVO;
import com.gsitm.devops.slm.model.vo.QCompWeigMngGrdVO;
import com.gsitm.devops.slm.repository.AnnualComponentWeightRepository;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
public class AnnuCompWeigBizImpl implements AnnuCompWeigBiz {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AnnualComponentWeightRepository repository;

	@Transactional
	@Override
	public void createAnnuCompWeig(AnnualComponentWeightId id, CompWeigMngCmdVO vo) throws DataAccessException {
		AnnualComponentWeight entity = new AnnualComponentWeight();
		entity.setId(id);
		BeanUtils.copyProperties(vo, entity);
		em.persist(entity);
	}

	@Transactional
	@Override
	public void updateAnnuCompWeig(AnnualComponentWeightId id, CompWeigMngCmdVO vo) throws DataAccessException {
		AnnualComponentWeight entity = repository.findOne(id);
		BeanUtils.copyProperties(vo, entity);
		repository.save(entity);
	}

	@Transactional
	@Override
	public void removeAnnuCompWeig(AnnualComponentWeightId id) throws DataAccessException {
		repository.delete(id);
	}

	@Override
	public Page<CompWeigMngGrdVO> findAllAnnuCompWeig(Integer year, Pageable pageable) {
		QAnnualComponentWeight acw = QAnnualComponentWeight.annualComponentWeight;

		JPAQuery query = new JPAQuery(em).from(acw).where(acw.id.year.eq(year));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<AnnualComponentWeight>(AnnualComponentWeight.class, "annualComponentWeight"));
		querydsl.applyPagination(pageable, query);

		SearchResults<CompWeigMngGrdVO> search = query.listResults(
				new QCompWeigMngGrdVO(acw.id.year, acw.id.complexity, acw.ei, acw.eo, acw.eq, acw.ilf, acw.eif));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
