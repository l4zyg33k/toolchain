package com.gsitm.devops.slm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.util.Searchable;
import com.gsitm.devops.cmm.util.SearchableConverter;
import com.gsitm.devops.slm.model.Productivity;
import com.gsitm.devops.slm.model.QProductivity;
import com.gsitm.devops.slm.service.ProductivityService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;

/**
 * CSR FP 조정인자관리 컨트롤러
 */
@Controller
public class ProductivityController {

	private static final String MENU_CODE = "PdtMng";
	
	private SearchableConverter<Productivity> search = new SearchableConverter<Productivity>(
			Productivity.class);

	@Autowired
	private CodeService codeService;

	@Autowired
	private ProductivityService prodService;
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}	

	/**
	 * 폼로드
	 * 
	 * @return
	 */
	@RequestMapping(value = "/slm/productivity/index", method = RequestMethod.GET)
	public String index(Model model) {

		QCode qCode = QCode.code;
		// 기준년도 리스트
		model.addAttribute("standardYearCode",
				codeService.findAll(qCode.parent.value.eq("SLM50")));
		// CSR파트
		model.addAttribute("csrPartCode",
				codeService.findAll(qCode.parent.value.eq("G4")));
		// 시스템특성
		model.addAttribute("systemTypeCode",
				codeService.findAll(qCode.parent.value.eq("H2")));

		// 그리드 콤보용 데이터 가져오기
		model.addAttribute("standardYearGrid",
				codeService.getJqGridSelectValue("SLM50"));
		model.addAttribute("csrPartGrid",
				codeService.getJqGridSelectValue("G4"));
		model.addAttribute("systemTypeGrid",
				codeService.getJqGridSelectValue("H2"));

		return "slm/pdtmng/index";
	}

	/**
	 * 그리드조회
	 * 
	 * @param searchable
	 * @param pageable
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/productivity", method = RequestMethod.POST)
	@ResponseBody
	public Page<Productivity> findProjectQuality(
			Searchable searchable,
			Pageable pageable,
			@RequestParam(value = "standardYear", required = false) Code standardYear,
			@RequestParam(value = "csrPart", required = false) Code csrPart,
			@RequestParam(value = "systemType", required = false) Code systemType) {
		BooleanBuilder builider = new BooleanBuilder();
		;

		if (standardYear != null) {
			builider = builider.and(QProductivity.productivity.year
					.eq(standardYear));
		}
		if (csrPart != null) {
			builider = builider.and(QProductivity.productivity.part.eq(csrPart));
		}
		if (systemType != null) {
			builider = builider.and(QProductivity.productivity.system.eq(systemType));
		}

		// 그리드 조회조건 매핑
		Predicate predicate = builider.and(search.toPredicate(searchable));
		Page<Productivity> result = prodService.findAll(predicate, pageable);
		return result;
	}

	/**
	 * 그리드 수정
	 * 
	 * @param projectinspection
	 * @param id
	 * @param defectGrade
	 * @param defectType
	 * @param content
	 * @param oper
	 * @return
	 */
	@RequestMapping(value = "/rest/slm/productivity/edit", method = RequestMethod.POST)
	@ResponseBody
	public String edit(
			@RequestParam("id") String id,
			@RequestParam(value = "standardYear.name", required = false) Code standardYear,
			@RequestParam(value = "csrPart.name", required = false) Code csrPart,
			@RequestParam(value = "systemType.name", required = false) Code systemType,
			@RequestParam(value = "productivity", required = false) Float productivity,
			@RequestParam("oper") String oper) {
		Productivity data;
		switch (oper) {
		case "add":
			data = new Productivity();
			data.setYear(standardYear);
			data.setPart(csrPart);
			data.setSystem(systemType);
			data.setPoint(productivity);
			prodService.save(data);
			break;

		case "edit":
			data = prodService.findOne(Long.parseLong(id));
			data.setYear(standardYear);
			data.setPart(csrPart);
			data.setSystem(systemType);
			data.setPoint(productivity);
			prodService.save(data);
			break;

		case "del":
			prodService.delete(Long.parseLong(id));
			break;
		}
		return "success";
	}

}
