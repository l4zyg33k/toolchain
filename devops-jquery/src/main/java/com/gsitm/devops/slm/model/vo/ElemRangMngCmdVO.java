package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import javax.validation.constraints.Min;

import lombok.Data;

@Data
public class ElemRangMngCmdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Min(1)
	private Integer lessThan;

	private Integer greaterThan;
}
