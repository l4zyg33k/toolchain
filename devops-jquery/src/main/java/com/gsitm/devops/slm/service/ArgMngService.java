package com.gsitm.devops.slm.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.slm.model.ArgMng;

public interface ArgMngService extends
		SharedService<ArgMng, Long> {
}
