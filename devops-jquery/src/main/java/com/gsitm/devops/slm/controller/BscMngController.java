package com.gsitm.devops.slm.controller;

import java.util.Map;

import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.slm.service.biz.BscMngBiz;

/**
 *  기본정보관리 컨트롤러
 */
@Controller
@Setter
public class BscMngController {

	private static final String MENU_CODE = "BscMng";

	@Autowired
	private CodeService codeService;

	@Autowired
	private BscMngBiz biz;

	/**
	 * 메뉴코드
	 * 
	 * @return 메뉴코드
	 */
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	/**
	 * 기준년도
	 * 
	 * @return 기준년도 목록
	 */
	@ModelAttribute("years")
	public Map<Long, String> getYears() {
		return biz.getYears();
	}
	
	@ModelAttribute("year")
	public String getYear() {
		return "";
	}
	
	/**
	 * 요구기능
	 * 
	 * @return 요구기능 목록
	 */
	@ModelAttribute("demands")
	public Map<Long, String> getDemands() {
		return biz.getDemands();
	}
	
	@ModelAttribute("demand")
	public String getDemand() {
		return "";
	}
	
	/**
	 * 작업유형
	 * 
	 * @return 작업유형 목록
	 */
	@ModelAttribute("jobs")
	public Map<Long, String> getJobs() {
		return biz.getJobs();
	}	
	
	@ModelAttribute("job")
	public String getJob() {
		return "";
	}
	
	@ModelAttribute("types")
	public Map<Long, String> getTypes() {
		return biz.getTypes();
	}	
	
	@ModelAttribute("type")
	public String getType() {
		return "";
	}
	
	// CSR 레벨
	@ModelAttribute("levels")
	public String getLevels() {
		return biz.getLevels();
	}	
	
	// 이상
	@ModelAttribute("mores")
	public String getMores() {
		return biz.getMores();
	}
	
	// 이하
	@ModelAttribute("lesses")
	public String getLesses() {
		return biz.getLesses();
	}
	
	@ModelAttribute("yearsVals")
	public String getYearsVals() {
		return biz.getYearsVals();
	}		
	
	@ModelAttribute("demandsVals")
	public String getDemandsVals() {
		return biz.getDemandsVals();
	}	
	
	@ModelAttribute("jobsVals")
	public String getJobsVals() {
		return biz.getJobsVals();
	}
	
	@ModelAttribute("levelsVals")
	public String getLevelsVals() {
		return biz.getLevelsVals();
	}		
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	/**
	 * CSR FP 기본정보관리 화면 조회
	 * 
	 * @return
	 */
	@RequestMapping(value = "/slm/bscmng", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return "slm/bscmng/index";
	}
}
