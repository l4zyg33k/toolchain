package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.slm.model.ComplexityType;

import lombok.Data;

@Data
public class FuncCompCmdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ComplexityType lessThan;

	private ComplexityType closed;

	private ComplexityType greaterThan;
}
