package com.gsitm.devops.slm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.CsrFpResultDetail;
import com.gsitm.devops.slm.repository.CsrFpResultDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class CsrFpResultDetailServiceImpl implements CsrFpResultDetailService {

	@Autowired
	private CsrFpResultDetailRepository repository;

	@Autowired
	private CsrFpResultService csrFpResultService;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Autowired
	private CodeService codeService;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public CsrFpResultDetail findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public CsrFpResultDetail findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<CsrFpResultDetail> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<CsrFpResultDetail> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<CsrFpResultDetail> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<CsrFpResultDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<CsrFpResultDetail> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<CsrFpResultDetail> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	/**
	 * CsrFpResultDetail이 변경되면 CsrFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public CsrFpResultDetail save(CsrFpResultDetail csrFpResultDetail) {
		CsrFpResultDetail result = repository.save(csrFpResultDetail);
		csrFpResultService.saveResultFp(csrFpResultDetail.getCsrFpResult());
		return result;
	}

	/**
	 * CsrFpResultDetail이 변경되면 CsrFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public <S extends CsrFpResultDetail> List<S> save(Iterable<S> iterable) {
		List<S> result = repository.save(iterable);
		for (S s : iterable) {
			csrFpResultService.saveResultFp(s.getCsrFpResult());
		}
		return result;
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(CsrFpResultDetail csrFpResultDetail) {
		repository.delete(csrFpResultDetail);

	}

	@Transactional
	@Override
	public void delete(Iterable<? extends CsrFpResultDetail> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
