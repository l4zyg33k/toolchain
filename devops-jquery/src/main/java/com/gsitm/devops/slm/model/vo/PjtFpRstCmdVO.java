package com.gsitm.devops.slm.model.vo;

import java.io.Serializable;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

import lombok.Data;

@Data
public class PjtFpRstCmdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 기준 년도 (코드)
	 */
	private Code year;

	/**
	 * 프로젝트
	 */
	private Project project;

	/**
	 * CSR 파트 (코드)
	 */
	private Code part;

	/**
	 * 처리자
	 */
	private Account op;

	/**
	 * 시스템 특성 (코드)
	 */
	private Code system;
}
