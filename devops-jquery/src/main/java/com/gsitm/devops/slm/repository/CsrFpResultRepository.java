package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.CsrFpResult;

@Repository
public interface CsrFpResultRepository extends
		SharedRepository<CsrFpResult, Long> {
}
