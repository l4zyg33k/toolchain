package com.gsitm.devops.slm.model;

public enum RangeType {
	LESSTHAN, CLOSED, GREATERTHAN
}
