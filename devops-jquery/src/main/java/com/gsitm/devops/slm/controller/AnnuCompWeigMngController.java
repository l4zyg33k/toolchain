package com.gsitm.devops.slm.controller;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.slm.model.vo.CompWeigMngWebVO;

@Controller
public class AnnuCompWeigMngController {

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return "AnnuCompWeigMng";
	}

	@ModelAttribute("years")
	public List<Integer> getYears() {
		int year = new DateTime().getYear();
		return ContiguousSet.create(Range.open(year - 5, year + 5), DiscreteDomain.integers()).asList();
	}

	@RequestMapping("/slm/compweigmng")
	public String index(Model model) {
		CompWeigMngWebVO compWeigMngWebVO = new CompWeigMngWebVO();
		compWeigMngWebVO.setYear(new DateTime().getYear());
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");		
		model.addAttribute("compWeigMngWebVO", compWeigMngWebVO);
		return "slm/compweigmng/index";
	}
}
