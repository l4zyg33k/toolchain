package com.gsitm.devops.slm.model;

import javax.persistence.Entity;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class DataElementType extends AbstractPersistable<Long> {

	private static final long serialVersionUID = -2142787303185386922L;
	private String name;
	private String description;
}
