package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.AnnualElementRange;
import com.gsitm.devops.slm.model.AnnualElementRangeId;

@Repository
public interface AnnualElementRangeRepository extends SharedRepository<AnnualElementRange, AnnualElementRangeId> {

}
