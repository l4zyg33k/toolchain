package com.gsitm.devops.slm.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class DataEntity extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 7737170947223560127L;
	private String name;
	private String description;
	@ManyToMany
	private Set<DataElementType> dets;
}
