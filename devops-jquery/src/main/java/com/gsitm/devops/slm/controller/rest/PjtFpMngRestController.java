package com.gsitm.devops.slm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.slm.model.vo.PjtFpRstCmdVO;
import com.gsitm.devops.slm.model.vo.PjtFpMngGrdVO;
import com.gsitm.devops.slm.service.biz.PjtFpMngBiz;

@RestController
public class PjtFpMngRestController {

	@Autowired
	private PjtFpMngBiz biz;

	@RequestMapping(value = "/rest/slm/pjtfpmng/pjtfp", method = RequestMethod.POST)
	public Page<PjtFpMngGrdVO> findAllPjtFpRst(Pageable pageable) {
		return biz.findAllPjtFpRst(pageable);
	}

	@RequestMapping(value = "/rest/slm/pjtfpmng/pjtfp/edit", method = RequestMethod.POST)
	public JqResult editPjtFpRst(@RequestParam("id") String id, @RequestParam("oper") String oper,
			@Valid PjtFpRstCmdVO vo, BindingResult result) {
		switch (oper) {
		case "add":
			biz.createPjtFpRst(vo);
			break;
		case "edit":
			biz.updatePjtFpRst(Long.parseLong(id), vo);
			break;
		default:
			break;
		}
		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/pjtfpmng/pjtfp/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult editPjtFpRst(@RequestParam("id") Long id) {
		biz.removePjtFpRst(id);
		return new JqSuccess();
	}
}
