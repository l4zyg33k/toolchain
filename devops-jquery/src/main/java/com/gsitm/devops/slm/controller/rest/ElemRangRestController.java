package com.gsitm.devops.slm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.slm.model.AnnualElementRangeId;
import com.gsitm.devops.slm.model.ComponentType;
import com.gsitm.devops.slm.model.vo.ElemRangGrdVO;
import com.gsitm.devops.slm.model.vo.ElemRangMngCmdVO;
import com.gsitm.devops.slm.service.biz.ElemRangBiz;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ElemRangRestController {

	@Autowired
	private ElemRangBiz biz;

	@RequestMapping(value = "/rest/slm/elemrangmng", method = RequestMethod.POST)
	public Page<ElemRangGrdVO> findAllelemrangmng(@RequestParam("year") Integer year,
			@RequestParam("component") ComponentType component, Pageable pageable) {
		return biz.findAllElemRang(year, component, pageable);
	}

	@RequestMapping(value = "/rest/slm/elemrangmng/edit", method = RequestMethod.POST)
	public JqResult editElemRangMng(@RequestParam("oper") String oper, AnnualElementRangeId id,
			@Valid ElemRangMngCmdVO vo, BindingResult result) {
		log.debug(id.toString());
		log.debug(vo.toString());
		if (result.hasErrors()) {
			return new JqFailure(result.getFieldError("lessThan").getDefaultMessage());
		}
		try {
			switch (oper) {
			case "add":
				biz.createElemRang(id, vo);
				break;
			case "edit":
				biz.updateElemRang(id, vo);
				break;
			default:
				break;
			}
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/slm/elemrangmng/edit", method = RequestMethod.POST, params = { "oper=del" })
	public JqResult removeElemRangMng(AnnualElementRangeId id) {
		log.debug(id.toString());
		try {
			biz.removeElemRang(id);
		} catch (DataAccessException e) {
			return new JqFailure(e.getMessage());
		}
		return new JqSuccess();
	}
}
