package com.gsitm.devops.slm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.slm.model.ProjectFpResult;
import com.gsitm.devops.slm.repository.ProjectFpResultRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProjectFpResultServiceImpl implements ProjectFpResultService {

	@Autowired
	private ProjectFpResultRepository repository;

	@Autowired
	private ProductivityService productivityService;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Autowired
	private CodeService codeService;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public ProjectFpResult findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProjectFpResult findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectFpResult> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProjectFpResult> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectFpResult> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectFpResult> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectFpResult> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectFpResult> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public ProjectFpResult save(ProjectFpResult projectFpResult) {
		if (projectFpResult.getId() == null) {
			return repository.save(projectFpResult);
		} else {
			return saveResultFp(projectFpResult);
		}
	}

	/**
	 * ProjectFpResultDetail이 변경되면 ProjectFpResult의 최종FP, MH가 변경되도록 수정
	 */
	@Transactional
	@Override
	public <S extends ProjectFpResult> List<S> save(Iterable<S> iterable) {
		for (S s : iterable) {
			if (s.getId() == null) {
				repository.save(s);
			} else {
				saveResultFp(s);
			}

		}
		return null;
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectFpResult projectFpResult) {
		repository.delete(projectFpResult);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProjectFpResult> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Transactional
	@Override
	public ProjectFpResult saveResultFp(ProjectFpResult projectFpResult) {

		// List<ProgramFpResult> details =
		// this.findOne(projectFpResult.getId()).getProgramFpResults();
		// Float sumFp = 0F;
		// //fp합계
		// for (ProgramFpResult detail : details ){
		// for (ProgramFpResultDetail t : detail.getProgramFpResultDetail() ){
		// sumFp += t.getAdjustFpMark();
		// }
		// }
		// projectFpResult.setFinalFP(sumFp);
		//
		// //예상 MH계산
		// QProductivity qProductivity = QProductivity.productivity1;
		// Productivity productivity =
		// productivityService.findOne(qProductivity.standardYear.eq(projectFpResult.getStandardYear())
		// .and(qProductivity.csrPart.eq(projectFpResult.getCsrPart()))
		// .and(qProductivity.systemType.eq(projectFpResult.getSystemType())));
		// Float result = sumFp/productivity.getProductivity();
		// Float t = (float) (Math.round(result*100)/100F);
		// projectFpResult.setExpectMH(t);
		//
		// return repository.save(projectFpResult);
		return null;
	}
}
