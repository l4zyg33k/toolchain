package com.gsitm.devops.slm.model;

import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class DataFunction extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 3431993508828777962L;
	private String name;
	@ElementCollection
	private Set<DataEntity> rets;
	@ElementCollection
	private Set<DataElementType> dets;
}
