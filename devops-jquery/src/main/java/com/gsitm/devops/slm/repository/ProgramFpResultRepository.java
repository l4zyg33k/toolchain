package com.gsitm.devops.slm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.slm.model.ProgramFpResult;

@Repository
public interface ProgramFpResultRepository extends
		SharedRepository<ProgramFpResult, Long> {
}
