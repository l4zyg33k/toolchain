package com.gsitm.devops;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.service.AccountService;

public class AuditorAwareImpl implements AuditorAware<Account> {

	@Autowired
	private AccountService service;

	@Override
	public Account getCurrentAuditor() {
		return service.getCurrentAuditor();
	}
}
