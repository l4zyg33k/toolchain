package com.gsitm.devops.pms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjSizeCmdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeFrmVO;
import com.gsitm.devops.pms.model.vo.ProjSizeGrdVO;

public interface ProjSizeBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public Page<ProjSizeGrdVO> findAll(Pageable pageable, UserDetails user, String project);

	public Page<ProjSizeGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, String project);

	public ProjSizeFrmVO findOne(Long id);

	public void create(ProjSizeCmdVO projInspCmdVO, String projectTypeCode);

	public void update(ProjSizeCmdVO projInspCmdVO);

	public void delete(ProjSizeCmdVO projInspCmdVO);

	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable, Long id);

	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable, Searchable searchable, Long id);

	public void create(ProjSizeDetlFrmVO projInspDetlFrmVO, Long id, String projectTypeCode);

	public void update(ProjSizeDetlFrmVO projInspDetlFrmVO, Long id, String projectTypeCode);

	public void delete(ProjSizeDetlFrmVO projInspDetlFrmVO, Long id);
}
