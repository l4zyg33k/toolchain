/**
 * 인스펙션 관리 디테일 엔티티 정보
 * ProjectInspectionDetail.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 프로젝트 인스펙션 상세 엔티티
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectInspectionDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 결함 등급 (코드)
	 */
	@ManyToOne
	private Code defectGrade;

	/**
	 * 결함 유형 (코드)
	 */
	@ManyToOne
	private Code defectType;

	/**
	 * 결함 내용
	 */
	@Column(length = 512)
	private String content;
}
