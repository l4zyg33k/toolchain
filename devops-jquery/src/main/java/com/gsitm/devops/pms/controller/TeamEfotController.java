package com.gsitm.devops.pms.controller;

import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.pms.service.biz.TeamEfotBiz;

@Controller
public class TeamEfotController {

	private static final String MENU_CODE = "TeamEfot";

	private static final String INDEX_URL = "/pms/teamefot";

	private static final String INDEX_PAGE = "pms/teamefot/index";

	@Autowired
	private TeamEfotBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("year")
	public String getYear() {
		return "";
	}

	@ModelAttribute("years")
	public Map<String, String> getYears(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getYears(user);
	}

	@ModelAttribute("fromDate")
	public LocalDate getFromDate() {
		return new LocalDate().minusMonths(1);
	}

	@ModelAttribute("toDate")
	public LocalDate getToDate() {
		return new LocalDate();
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}
}
