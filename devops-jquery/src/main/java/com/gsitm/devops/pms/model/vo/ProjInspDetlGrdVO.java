package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjInspDetlGrdVO {

	private Long id;

	private Long defectGrade;

	private Long defectType;

	private String content;

	public ProjInspDetlGrdVO() {
		super();
	}

	@QueryProjection
	public ProjInspDetlGrdVO(Long id, Long defectGrade, Long defectType,
			String content) {
		super();
		this.id = id;
		this.defectGrade = defectGrade;
		this.defectType = defectType;
		this.content = content;
	}
}
