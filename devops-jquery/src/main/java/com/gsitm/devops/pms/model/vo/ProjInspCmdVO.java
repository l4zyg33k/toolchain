package com.gsitm.devops.pms.model.vo;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjInspCmdVO extends ProjInspWebVO {

	private Project project;

	@NotBlank
	private String projectName;

	@NotBlank
	private String productName;

	@NotNull
	private Code productType;

	@NotNull
	private Code productPhase;

	private Integer productSize;

	private Account producer;

	@NotBlank
	private String producerName;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate inspectionDate;

	private Integer inspectors;

	private Account moderator;

	@NotBlank
	private String moderatorName;

	private Integer inspectionPercent;

	private Integer inspectionMeetingTime;
}
