package com.gsitm.devops.pms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.dto.ProjectProgress;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.dto.ProjectQualityDto;
import com.mysema.query.types.Predicate;

public interface ProjectService extends SharedService<Project, String> {

	public Project setNewCode(Project project);

	public Page<ProjectProgress> findProjectProgressByDivision(
			Predicate predicate, Pageable pageable);

	public Page<ProjectQualityDto> findResultDto(Predicate predicate,
			Pageable pageable);
}
