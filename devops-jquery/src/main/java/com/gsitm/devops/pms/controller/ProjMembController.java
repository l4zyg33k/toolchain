package com.gsitm.devops.pms.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.pms.service.biz.ProjMembBiz;

/**
 * 프로젝트 팀원관리 컨트롤러
 */
@Controller
public class ProjMembController {

	private static final String MENU_CODE = "ProjMemb";

	private static final String INDEX_URL = "/pms/projmemb";

	private static final String INDEX_PAGE = "pms/projmemb/index";

	@Autowired
	private ProjMembBiz biz;

	@Autowired
	private CommonCodeService cmmCodeService;

	// 메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	// 사용자별 jqGrid 권한 처리로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	// 직급
	@ModelAttribute("positions")
	public String getPositions() {
		return cmmCodeService.getPositionString();
	}

	// 팀
	@ModelAttribute("teams")
	public String getTeams() {
		return cmmCodeService.getTeamsString();
	}

	// 역할
	@ModelAttribute("roles")
	public String getRoles() {
		return cmmCodeService.getRolesString();
	}

	@ModelAttribute("project")
	public String getProject() {
		return "";
	}

	@ModelAttribute("projects")
	public Map<String, String> getProjects(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getProjects(user);
	}

	@ModelAttribute("dlgRole")
	public String getDlgRole() {
		return "";
	}

	@ModelAttribute("dlgRoles")
	public Map<Long, String> getDlgRoles() {
		return biz.getRoles();
	}

	/**
	 * @SpecialLogic 페이지 처리
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	/**
	 * @SpecialLogic 프로젝트 팀원 관리 페이지 로딩시
	 * 
	 * @param model
	 * @return string
	 */
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}
}
