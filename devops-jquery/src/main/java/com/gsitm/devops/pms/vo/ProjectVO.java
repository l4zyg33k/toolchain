package com.gsitm.devops.pms.vo;

import javax.validation.constraints.Digits;

import lombok.Data;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Data
public class ProjectVO {

	/**
	 * 코드
	 */
	private String code;
	/**
	 * 이름
	 */
	@NotEmpty
	private String name;
	/**
	 * 프로젝트 유형
	 */
	private Code type;
	/**
	 * 프로젝트 관리자
	 */
	private String pmName;
	/**
	 * 비지니스 리더
	 */
	private String blName;
	/**
	 * 등록 아이디
	 */
	private String createdBy;
	/**
	 * 추진 부서
	 */
	private String blDepartment;
	/**
	 * 정보기술 리더
	 */
	private Account il;
	/**
	 * 운영 담당자
	 */
	private Account op;
	/**
     *
     */
	private Account ou;
	/**
	 * 품질 담당자
	 */
	private Account qa;
	/**
	 * 추진 사업부
	 */
	private Code office;
	/**
	 * 시작일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate startDate;
	/**
	 * 종료일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate finishDate;
	/**
	 * 실제 시작일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate actualStartDate;
	/**
	 * 실제 종료일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate actualFinishDate;
	/**
	 * 진행단계
	 */
	private Code phase;
	/**
	 * 준비 시작일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate panelStartDate;
	/**
	 * 준비 종료일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate panelFinishDate;
	/**
	 * 회의일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate panelActualDate;
	/**
	 * 회의시각
	 */
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime panelActualTime;
	/**
	 * 회의장소
	 */
	private Code location;
	/**
	 * 권고사항
	 */
	private String recommandation;
	/**
	 * 통과여부
	 */
	private Code pass;
	/**
	 * 인수인계 시작일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate followUpStartDate;
	/**
	 * 인수인계 종료일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate followUpFinishDate;
	/**
	 * 실제 인수인계 시작일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate followUpActualStartDate;
	/**
	 * 실제 인수인계 종료일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate followUpActualFinishDate;
	/**
     *
     */
	@Digits(integer = 4, fraction = 3)
	private Float ouEffort = 0F;
	/**
	 * PD팀 실제 공수
	 */
	@Digits(integer = 4, fraction = 3)
	private Float ouActualEffort = 0F;
	/**
	 * 총 M/M
	 */
	@Digits(integer = 4, fraction = 3)
	private Float pdEffort = 0F;
	/**
	 * 실제 총 M/M
	 */
	@Digits(integer = 4, fraction = 3)
	private Float pdActualEffort = 0F;
	/**
	 * 프로세스 평가 점수
	 */
	@Digits(integer = 3, fraction = 3)
	private Float processPoint = 0F;
	/**
	 * 테스트 평가 점수
	 */
	@Digits(integer = 3, fraction = 3)
	private Float testingPoint = 0F;
	/**
	 * 산출물 평가 점수
	 */
	@Digits(integer = 3, fraction = 3)
	private Float productPoint = 0F;
	/**
	 * 고객 만족도 평가 점수
	 */
	@Digits(integer = 3, fraction = 3)
	private Float customerPoint = 0F;
	/**
	 * 1일 근무기준 시간
	 */
	@Digits(integer = 2, fraction = 1)
	private Float workingHour = 9F;
	/**
	 * 1달 근무기준 일수
	 */
	@Digits(integer = 2, fraction = 1)
	private Float workingDay = 22F;
	/**
	 * 총 M/H
	 */
	@Digits(integer = 5, fraction = 2)
	private Float workingEffort = 0F;
	/**
	 * 종료보고 일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate closeReportDate;
	/**
	 * 종료보고 형식
	 */
	private Code closeReportType;
	/**
	 * 오픈점검 일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate openCheckDate;
	/**
	 * 운영보고 일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate openReportDate;
	/**
	 * 현황판 표시
	 */
	private Boolean watching = Boolean.FALSE;

	public float getHourPerMonth() {

		float totalHour;

		if (getWorkingEffort() != null) {
			totalHour = getWorkingHour() * getWorkingDay() * getWorkingEffort();
		} else {
			totalHour = 0;
		}

		return totalHour;
	}

	public String getWatchingCheck() {

		String check = "비표시";

		if (getWatching() == null) {
			check = "비표시";
		} else if (getWatching() == true) {
			check = "표시";
		}

		return check;
	}
}
