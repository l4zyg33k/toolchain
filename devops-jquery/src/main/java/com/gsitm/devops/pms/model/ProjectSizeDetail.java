/**
 * 프로젝트 size등록 엔티티
 * ProjectSize.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectSizeDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private ProjectSize projectSize;

	/**
	 * 보고 일자
	 */
	private LocalDate reportDate;

	/**
	 * 신규(입력)
	 */
	private Integer newInput;

	/**
	 * 신규(조회)
	 */
	private Integer newSearch;

	/**
	 * 신규(배치)
	 */
	private Integer newBatch;

	/**
	 * 신규(보고서)
	 */
	private Integer newReport;

	/**
	 * 수정(입력)
	 */
	private Integer editInput;

	/**
	 * 수정(조회)
	 */
	private Integer editSearch;

	/**
	 * 수정(배치)
	 */
	private Integer editBatch;

	/**
	 * 수정(보고서)
	 */
	private Integer editReport;

	/**
	 * 총계
	 */
	private Integer totSum;

	/**
	 * WW 환산
	 */
	private Integer wwCal;
}
