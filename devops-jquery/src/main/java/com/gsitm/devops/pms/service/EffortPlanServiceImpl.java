package com.gsitm.devops.pms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.EffortPlan;
import com.gsitm.devops.pms.model.QEffortPlan;
import com.gsitm.devops.pms.model.QEffortResult;
import com.gsitm.devops.pms.repository.EffortPlanRepository;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class EffortPlanServiceImpl implements EffortPlanService {

	@Autowired
	private EffortPlanRepository repository;

	@PersistenceContext
	private EntityManager em;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public EffortPlan findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public EffortPlan findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<EffortPlan> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<EffortPlan> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<EffortPlan> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<EffortPlan> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<EffortPlan> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<EffortPlan> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public EffortPlan save(EffortPlan t) {
		return repository.save(t);
	}

	@Transactional
	@Override
	public <S extends EffortPlan> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(EffortPlan t) {
		repository.delete(t);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends EffortPlan> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Transactional
	@Async
	@Override
	public void summerizeEffortResultsById(Long id) {
		float beginning = 0;
		float analysis = 0;
		float design = 0;
		float development = 0;
		float test = 0;
		float execution = 0;
		float education = 0;
		float etc = 0;

		QEffortPlan ep = QEffortPlan.effortPlan;
		QEffortResult er = QEffortResult.effortResult;
		JPQLQuery query = new JPAQuery(em).from(ep).innerJoin(ep.results, er)
				.where(ep.id.eq(id));

		if (query.count() > 0) {
			Tuple sum = query.singleResult(er.beginning.sum(),
					er.analysis.sum(), er.design.sum(), er.development.sum(),
					er.test.sum(), er.execution.sum(), er.education.sum(),
					er.etc.sum());
			beginning = sum.get(0, Float.class);
			analysis = sum.get(1, Float.class);
			design = sum.get(2, Float.class);
			development = sum.get(3, Float.class);
			test = sum.get(4, Float.class);
			execution = sum.get(5, Float.class);
			education = sum.get(6, Float.class);
			etc = sum.get(7, Float.class);
		}

		EffortPlan plan = findOne(id);
		plan.setBeginning(beginning);
		plan.setAnalysis(analysis);
		plan.setDesign(design);
		plan.setDevelopment(development);
		plan.setTest(test);
		plan.setExecution(execution);
		plan.setEducation(education);
		plan.setEtc(etc);
		save(plan);
	}
}
