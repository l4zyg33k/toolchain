package com.gsitm.devops.pms.service.biz;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QAttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.adm.model.vo.QAtmtFileVO;
import com.gsitm.devops.adm.service.AttachmentFileService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.model.vo.ProjItemVO;
import com.gsitm.devops.pms.model.vo.ProjMngCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMngExlVO;
import com.gsitm.devops.pms.model.vo.ProjMngFrmVO;
import com.gsitm.devops.pms.model.vo.ProjMngGrdVO;
import com.gsitm.devops.pms.model.vo.ProjTypeVO;
import com.gsitm.devops.pms.model.vo.QProjItemVO;
import com.gsitm.devops.pms.model.vo.QProjMngExlVO;
import com.gsitm.devops.pms.model.vo.QProjMngFrmVO;
import com.gsitm.devops.pms.model.vo.QProjMngGrdVO;
import com.gsitm.devops.pms.model.vo.QProjTypeVO;
import com.gsitm.devops.pms.service.ProjectService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class ProjMngBizImpl implements ProjMngBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private CodeService codeService;

	@Autowired
	private AttachmentFileService afService;

	@Autowired
	private ObjectMapper mapper;

	@Override
	public Page<ProjMngGrdVO> findAll(Pageable pageable, UserDetails user) {
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QCode office = new QCode("office");
		QAccount createdBy = new QAccount("createdBy");
		QCode status = new QCode("status");
		String username = user.getUsername();

		JPQLQuery query = new JPAQuery(em).from(project).leftJoin(project.il, il).leftJoin(project.office, office)
				.leftJoin(project.status, status).leftJoin(project.createdBy, createdBy);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<Project>(Project.class, "project"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjMngGrdVO(project.code, project.name, office.name, project.pmName,
					il.name, status.name, project.finishDate, project.privacy, project.critical));
		} else {
			BooleanExpression editable = new CaseBuilder().when(il.username.eq(username)).then(true)
					.when(createdBy.username.eq(username)).then(true).otherwise(false);
			search = query.listResults(new QProjMngGrdVO(project.code, project.name, office.name, project.pmName,
					il.name, status.name, project.finishDate, project.privacy, project.critical, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjMngGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user) {
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QCode office = new QCode("office");
		QCode status = new QCode("status");
		QAccount createdBy = new QAccount("createdBy");
		String username = user.getUsername();

		PathBuilder<Project> path = new PathBuilder<Project>(Project.class, "project");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(project).leftJoin(project.il, il).leftJoin(project.office, office)
				.leftJoin(project.status, status).leftJoin(project.createdBy, createdBy).where(predicate);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjMngGrdVO(project.code, project.name, office.name, project.pmName,
					il.name, status.name, project.finishDate, project.privacy, project.critical));
		} else {
			BooleanExpression editable = new CaseBuilder().when(il.username.eq(username)).then(true)
					.when(createdBy.username.eq(username)).then(true).otherwise(false);
			search = query.listResults(new QProjMngGrdVO(project.code, project.name, office.name, project.pmName,
					il.name, status.name, project.finishDate, project.privacy, project.critical, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public List<ProjItemVO> findAll(String term) {
		QProject project = QProject.project;
		return new JPAQuery(em).from(project).where(project.name.contains(term)).orderBy(project.code.asc())
				.list(new QProjItemVO(project.code, project.name));
	}

	@Override
	public ProjMngFrmVO findOne(String code) {
		QProject project = QProject.project;
		QCode type = new QCode("type");
		QAccount il = new QAccount("il");
		QAccount op = new QAccount("op");
		QAccount ou = new QAccount("ou");
		QAccount qa = new QAccount("qa");
		QCode office = new QCode("office");
		QCode phase = new QCode("phase");
		QCode location = new QCode("location");
		QCode pass = new QCode("pass");
		QCode closeReportType = new QCode("closeReportType");
		QCode status = new QCode("status");
		JPQLQuery query = new JPAQuery(em).from(project).leftJoin(project.type, type).leftJoin(project.il, il)
				.leftJoin(project.op, op).leftJoin(project.ou, ou).leftJoin(project.qa, qa)
				.leftJoin(project.office, office).leftJoin(project.phase, phase).leftJoin(project.location, location)
				.leftJoin(project.pass, pass).leftJoin(project.closeReportType, closeReportType)
				.leftJoin(project.status, status).where(project.code.eq(code));
		return query.uniqueResult(new QProjMngFrmVO(project.code, project.name, type.id, type.name, project.pmName,
				project.blName, project.blDepartment, il.username, il.name, op.username, op.name, ou.username,
				qa.username, qa.name, office.id, office.name, project.startDate, project.finishDate,
				project.actualStartDate, project.actualFinishDate, phase.id, phase.name, project.panelStartDate,
				project.panelFinishDate, project.panelActualDate, project.panelActualTime, location.id, location.name,
				project.recommandation, pass.id, pass.name, project.followUpStartDate, project.followUpFinishDate,
				project.followUpActualStartDate, project.followUpActualFinishDate, project.openActualDate,
				project.openActualDate2, project.ouEffort, project.ouActualEffort, project.pdEffort,
				project.pdActualEffort, project.processPoint, project.testingPoint, project.productPoint,
				project.customerPoint, project.workingHour, project.workingDay, project.workingEffort,
				project.closeReportDate, closeReportType.id, closeReportType.name, project.openCheckDate,
				project.openReportDate, project.watching, project.satisfactionExpired, project.privacy,
				project.critical, status.id, status.name));
	}

	@Override
	public Map<Long, String> getOffices() {
		return codeService.getOptions("F2");
	}

	@Override
	public Map<Long, String> getTypes() {
		return codeService.getOptions("F1");
	}

	@Override
	public Map<Long, String> getLocations() {
		return codeService.getOptions("F3");
	}

	@Override
	public Map<Long, String> getPasses() {
		return codeService.getOptions("F4");
	}

	@Override
	public Map<Long, String> getCloseReportTypes() {
		return codeService.getOptions("F5");
	}

	@Override
	public Map<Long, String> getPhases() {
		return codeService.getOptions("F6");
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		if (authorities.contains("ROLE_USER") || authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public String create(ProjMngCmdVO vo) {
		Project project = new Project();
		BeanUtils.copyProperties(vo, project);
		String year = Integer.toString(new DateTime().getYear());
		QProject p = QProject.project;
		int seq = new JPAQuery(em).from(p).where(p.code.startsWith(year))
				.singleResult(p.code.substring(4, 6).max().coalesce("0").asString().castToNum(Integer.class).add(1));
		String code = String.format("%s%02d%s", year, seq, vo.getOffice().getValue());
		project.setCode(code);
		projectService.save(project);
		return messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public String update(ProjMngCmdVO vo) {
		Project project = projectService.findOne(vo.getCode());
		BeanUtils.copyProperties(vo, project);
		projectService.save(project);
		return messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public String delete(ProjMngCmdVO vo) {
		projectService.delete(vo.getId());
		return messageSource.getMessage("remove.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public List<ProjTypeVO> findAllByType(String term) {
		QProject project = QProject.project;
		return new JPAQuery(em).from(project).where(project.name.contains(term)).orderBy(project.code.asc())
				.list(new QProjTypeVO(project.code, project.name, project.type.value, project.type.name));
	}

	@Override
	public Map<Long, String> getStatuses() {
		return codeService.getOptions("F7");
	}

	@Override
	public Page<AtmtFileVO> getAttachments(String code, Pageable pageable) {
		QProject project = QProject.project;
		QAttachmentFile attachment = QAttachmentFile.attachmentFile;
		PathBuilder<Project> builder = new PathBuilder<Project>(Project.class, "project");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(project).innerJoin(project.attachments, attachment)
				.where(project.code.eq(code));
		querydsl.applyPagination(pageable, query);
		SearchResults<AtmtFileVO> search = query.listResults(new QAtmtFileVO(attachment.id, attachment.fileName,
				attachment.fileSize, attachment.createdDate, attachment.createdBy.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public void download(Long id, HttpServletRequest request, HttpServletResponse response) {
		afService.download(id, request, response);
	}

	@Override
	public void removeAttachmentFile(String code, AtmtFileVO atmtFileVO, UserDetails user) throws Exception {
		Project project = projectService.findOne(code);
		AttachmentFile attachment = afService.findOne(atmtFileVO.getId());
		if (attachment.getCreatedBy().getUsername().equals(user.getUsername())) {
			project.getAttachments().remove(attachment);
			projectService.save(project);
			afService.delete(attachment);
		} else {
			throw new Exception("등록자만 삭제 할 수 있습니다.");
		}
	}

	@Override
	public void addAttachmentFile(String code, MultipartHttpServletRequest request) {
		Project project = projectService.findOne(code);
		Iterator<String> fileNames = request.getFileNames();
		while (fileNames.hasNext()) {
			MultipartFile file = request.getFile((String) fileNames.next());
			AttachmentFile attachment = afService.save(getContainer(), file);
			project.getAttachments().add(attachment);
		}
		projectService.save(project);
	}

	private Code getContainer() {
		QCode code = QCode.code;
		return codeService.findOne(code.value.eq("04").and(code.parent.value.eq("A3")));
	}

	@Override
	public Page<ProjMngExlVO> findAllForExcel(Pageable pageable, Searchable searchable) {
		QProject project = QProject.project;
		QCode type = new QCode("type");
		QAccount il = new QAccount("il");
		QAccount op = new QAccount("op");
		QAccount ou = new QAccount("ou");
		QAccount qa = new QAccount("qa");
		QCode office = new QCode("office");
		QCode phase = new QCode("phase");
		QCode location = new QCode("location");
		QCode pass = new QCode("pass");
		QCode closeReportType = new QCode("closeReportType");
		QCode status = new QCode("status");
		PathBuilder<Project> path = new PathBuilder<Project>(Project.class, "project");
		JqGrid jqgrid = new JqGrid(path);

		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(project).leftJoin(project.type, type).leftJoin(project.il, il)
				.leftJoin(project.op, op).leftJoin(project.ou, ou).leftJoin(project.qa, qa)
				.leftJoin(project.office, office).leftJoin(project.phase, phase).leftJoin(project.location, location)
				.leftJoin(project.pass, pass).leftJoin(project.closeReportType, closeReportType)
				.leftJoin(project.status, status).where(predicate);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMngExlVO> search = SearchResults.emptyResults();
		search = query.listResults(new QProjMngExlVO(project.code, project.name, type.id, type.name, project.pmName,
				project.blName, project.blDepartment, il.username, il.name, op.username, op.name, ou.username,
				qa.username, qa.name, office.id, office.name, project.startDate, project.finishDate,
				project.actualStartDate, project.actualFinishDate, phase.id, phase.name, project.panelStartDate,
				project.panelFinishDate, project.panelActualDate, project.panelActualTime, location.id, location.name,
				project.recommandation, pass.id, pass.name, project.followUpStartDate, project.followUpFinishDate,
				project.followUpActualStartDate, project.followUpActualFinishDate, project.openActualDate,
				project.openActualDate2, project.ouEffort, project.ouActualEffort, project.pdEffort,
				project.pdActualEffort, project.processPoint, project.testingPoint, project.productPoint,
				project.customerPoint, project.workingHour, project.workingDay, project.workingEffort,
				project.closeReportDate, closeReportType.id, closeReportType.name, project.openCheckDate,
				project.openReportDate, project.watching, project.satisfactionExpired, project.privacy,
				project.critical, status.id, status.name));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjMngExlVO> findAllForExcel(Pageable pageable) {
		QProject project = QProject.project;
		QCode type = new QCode("type");
		QAccount il = new QAccount("il");
		QAccount op = new QAccount("op");
		QAccount ou = new QAccount("ou");
		QAccount qa = new QAccount("qa");
		QCode office = new QCode("office");
		QCode phase = new QCode("phase");
		QCode location = new QCode("location");
		QCode pass = new QCode("pass");
		QCode closeReportType = new QCode("closeReportType");
		QCode status = new QCode("status");
		PathBuilder<Project> path = new PathBuilder<Project>(Project.class, "project");

		JPQLQuery query = new JPAQuery(em).from(project).leftJoin(project.type, type).leftJoin(project.il, il)
				.leftJoin(project.op, op).leftJoin(project.ou, ou).leftJoin(project.qa, qa)
				.leftJoin(project.office, office).leftJoin(project.phase, phase).leftJoin(project.location, location)
				.leftJoin(project.pass, pass).leftJoin(project.closeReportType, closeReportType)
				.leftJoin(project.status, status);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMngExlVO> search = SearchResults.emptyResults();
		search = query.listResults(new QProjMngExlVO(project.code, project.name, type.id, type.name, project.pmName,
				project.blName, project.blDepartment, il.username, il.name, op.username, op.name, ou.username,
				qa.username, qa.name, office.id, office.name, project.startDate, project.finishDate,
				project.actualStartDate, project.actualFinishDate, phase.id, phase.name, project.panelStartDate,
				project.panelFinishDate, project.panelActualDate, project.panelActualTime, location.id, location.name,
				project.recommandation, pass.id, pass.name, project.followUpStartDate, project.followUpFinishDate,
				project.followUpActualStartDate, project.followUpActualFinishDate, project.openActualDate,
				project.openActualDate2, project.ouEffort, project.ouActualEffort, project.pdEffort,
				project.pdActualEffort, project.processPoint, project.testingPoint, project.productPoint,
				project.customerPoint, project.workingHour, project.workingDay, project.workingEffort,
				project.closeReportDate, closeReportType.id, closeReportType.name, project.openCheckDate,
				project.openReportDate, project.watching, project.satisfactionExpired, project.privacy,
				project.critical, status.id, status.name));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
