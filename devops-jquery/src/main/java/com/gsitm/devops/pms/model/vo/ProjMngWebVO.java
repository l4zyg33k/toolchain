package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjMngWebVO extends SharedWebVO<String> {

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/pms/projmng" : String.format("/pms/projmng/%s", getId());
	}

	public String getPrevUrl() {
		return String.format("/pms/projmng?page=%d&rowNum=%d&rowId=%s", getPage(), getRowNum(), getRowId());
	}

	public String getAttachmentsUrl() {
		return String.format("/rest/pms/projmng/%s/attachments", getId());
	}

	public String getAttachmentsEditUrl() {
		return String.format("/rest/pms/projmng/%s/attachments/edit", getId());
	}

	public String getAttachmentsUploadUrl() {
		return String.format("/rest/pms/projmng/%s/attachments/upload", getId());
	}
}
