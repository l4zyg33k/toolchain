package com.gsitm.devops.pms.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjMembCmdMbsVO;
import com.gsitm.devops.pms.model.vo.ProjMembCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMembGrdVO;
import com.gsitm.devops.pms.service.biz.ProjMembBiz;

@RestController
public class ProjMembRestController {

	@Autowired
	private ProjMembBiz biz;

	/**
	 * @SpecialLogic 프로젝트 팀원관리 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjMembGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projmemb", method = RequestMethod.POST)
	public Page<ProjMembGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "project", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, user, projectCode);
	}

	/**
	 * @SpecialLogic 프로젝트 팀원관리 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param user
	 *            : 사용자 정보
	 * @param project
	 *            : 프로젝트코드
	 * @return Page<ProjMembGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projmemb", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjMembGrdVO> findAll(
			Pageable pageable,
			Searchable searchable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, searchable, user, project);
	}

	/**
	 * @SpecialLogic 프로젝트 팀원관리 삭제, 수정 처리
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjMembGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projmemb/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjMembCmdVO projMembCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (projMembCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projMembCmdVO.getOper()) {
		case "edit":
			biz.update(projMembCmdVO);
			break;
		case "del":
			biz.delete(projMembCmdVO);
			break;
		}

		return new JqSuccess();
	}

	/**
	 * @SpecialLogic 선택된 팀원 일괄등록
	 * 
	 * @param project
	 * @param userNames
	 * @param startDate
	 * @param endDate
	 */
	@RequestMapping(value = "/rest/pms/projmemb/save", method = RequestMethod.POST)
	public JqResult addMembers(@Valid ProjMembCmdMbsVO projMembCmdMbsVO,
			BindingResult result) {

		if (result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		// 등록처리
		biz.create(projMembCmdMbsVO);

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/projmemb/accounts", method = RequestMethod.POST)
	public Page<AcntMngGrdVO> findAll(
			Pageable pageable,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, project);
	}

	@RequestMapping(value = "/rest/adm/acntmng/accounts", method = RequestMethod.POST, params = { "_search=true" })
	public Page<AcntMngGrdVO> findAll(
			Pageable pageable,
			Searchable searchable,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, searchable, project);
	}
}
