package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 프로젝트 팀원관리 엔티티
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectMember extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 팀원
	 */
	@ManyToOne
	private Account member;

	/**
	 * 역활 (코드)
	 */
	@ManyToOne
	private Code role;

	/**
	 * 투입 시작 일자
	 */
	private LocalDate startDate;

	/**
	 * 투입 종료 일자
	 */
	private LocalDate endDate;

	/**
	 * 프로젝트 공수 계획
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<EffortPlan> plans;
}
