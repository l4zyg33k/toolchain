package com.gsitm.devops.pms.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;
import com.gsitm.devops.pms.model.Committee;
import com.mysema.query.annotations.QueryProjection;

public class ProjectProgress {

	// Project
	private ProjectWeeklyReport projectWeeklyReport;

	// Committee
	private List<Committee> committees;

	@QueryProjection
	public ProjectProgress(ProjectWeeklyReport projectWeeklyReport,
			List<Committee> committees) {
		super();
		this.projectWeeklyReport = projectWeeklyReport;
		// this.phase = phase;
		this.committees = committees;
	}

	public ProjectProgress() {
		super();
	}

	public ProjectWeeklyReport getProjectWeeklyReport() {
		return projectWeeklyReport;
	}

	public void setProjectWeeklyReport(ProjectWeeklyReport projectWeeklyReport) {
		this.projectWeeklyReport = projectWeeklyReport;
	}

	@JsonIgnoreProperties({ "project", "actualOpeningDate", "openingTime",
			"location", "title", "agendas", "confirm", "createdBy",
			"createdDate", "lastModifiedBy", "lastModifiedDate" })
	public List<Committee> getCommittees() {
		return committees;
	}

	public void setCommittees(List<Committee> committees) {
		this.committees = committees;
	}

}
