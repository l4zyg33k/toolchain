package com.gsitm.devops.pms.service.biz;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjPanlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjPanlGrdVO;

public interface ProjPanlBiz extends MessageSourceAware {

	public ProjPanlFrmVO findOne(String code);

	public Page<ProjPanlGrdVO> findAll(Pageable pageable);

	public Page<ProjPanlGrdVO> findAll(Pageable pageable, Searchable searchable);
}
