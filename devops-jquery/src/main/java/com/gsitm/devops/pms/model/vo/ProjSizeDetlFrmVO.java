package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.pms.model.ProjectSize;

@Getter
@Setter
public class ProjSizeDetlFrmVO {

	private String oper;

	private Long id;

	// 프로젝트
	private ProjectSize projectSize;

	// 보고 일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate reportDate;

	// 신규(입력)
	private Integer newInput;

	// 신규(조회)
	private Integer newSearch;

	// 신규(배치)
	private Integer newBatch;

	// 신규(보고서)
	private Integer newReport;

	// 수정(입력)
	private Integer editInput;

	// 수정(조회)
	private Integer editSearch;

	// 수정(배치)
	private Integer editBatch;

	// 수정(보고서)
	private Integer editReport;

	// 총계
	private Integer totSum;

	// WW 환산
	private Integer wwCal;

	public boolean isNew() {
		return null == getId();
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
