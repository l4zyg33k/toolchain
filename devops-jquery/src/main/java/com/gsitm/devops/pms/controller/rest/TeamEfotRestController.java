package com.gsitm.devops.pms.controller.rest;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqMap;
import com.gsitm.devops.pms.model.vo.TeamEfotGrdVO;
import com.gsitm.devops.pms.service.biz.TeamEfotBiz;

/**
 * 
 * @author lazygeek
 *
 */
@RestController
public class TeamEfotRestController {

	/**
	 * 
	 */
	@Autowired
	private TeamEfotBiz biz;

	/**
	 * 
	 * @param pageable
	 * @param project
	 * @param member
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping(value = "/rest/pms/teamefot", method = RequestMethod.POST)
	public Page<TeamEfotGrdVO> findAll(
			Pageable pageable,
			@RequestParam("project") String project,
			@RequestParam("member") String member,
			@RequestParam("fromDate") @DateTimeFormat(iso = ISO.DATE) LocalDate fromDate,
			@RequestParam("toDate") @DateTimeFormat(iso = ISO.DATE) LocalDate toDate) {
		return biz.findAll(pageable, project, member, fromDate, toDate);
	}

	/**
	 * 
	 * @param year
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/rest/pms/teamefot/projects", method = RequestMethod.POST)
	public List<JqMap> getProjects(@RequestParam("year") String year,
			@AuthenticationPrincipal UserDetails user) {
		return biz.getProjects(year, user);
	}

	/**
	 * 
	 * @param project
	 * @return
	 */
	@RequestMapping(value = "/rest/pms/teamefot/members", method = RequestMethod.POST)
	public List<JqMap> getMembers(@RequestParam("project") String project) {
		return biz.getMembers(project);
	}
}
