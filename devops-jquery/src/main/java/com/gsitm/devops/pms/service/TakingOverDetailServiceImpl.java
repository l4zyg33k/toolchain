package com.gsitm.devops.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.TakingOverDetail;
import com.gsitm.devops.pms.repository.TakingOverDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public class TakingOverDetailServiceImpl implements TakingOverDetailService {

	@Autowired
	private TakingOverDetailRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public TakingOverDetail findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public TakingOverDetail findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<TakingOverDetail> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<TakingOverDetail> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<TakingOverDetail> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<TakingOverDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<TakingOverDetail> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<TakingOverDetail> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public TakingOverDetail save(TakingOverDetail TakingOverDetail) {
		return repository.save(TakingOverDetail);
	}

	@Transactional
	@Override
	public <S extends TakingOverDetail> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(TakingOverDetail TakingOverDetail) {
		repository.delete(TakingOverDetail);
	}

	@Override
	public void delete(Iterable<? extends TakingOverDetail> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

}
