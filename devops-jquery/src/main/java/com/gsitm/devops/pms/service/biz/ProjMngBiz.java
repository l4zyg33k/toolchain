package com.gsitm.devops.pms.service.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjItemVO;
import com.gsitm.devops.pms.model.vo.ProjMngCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMngExlVO;
import com.gsitm.devops.pms.model.vo.ProjMngFrmVO;
import com.gsitm.devops.pms.model.vo.ProjMngGrdVO;
import com.gsitm.devops.pms.model.vo.ProjTypeVO;

public interface ProjMngBiz extends MessageSourceAware {

	public Page<ProjMngGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<ProjMngGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user);

	public List<ProjItemVO> findAll(String term);

	public ProjMngFrmVO findOne(String code);

	public Map<Long, String> getOffices();

	public Map<Long, String> getTypes();

	public Map<Long, String> getLocations();

	public Map<Long, String> getPasses();

	public Map<Long, String> getCloseReportTypes();

	public Map<Long, String> getPhases();

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public String create(ProjMngCmdVO vo);

	public String update(ProjMngCmdVO vo);

	public String delete(ProjMngCmdVO vo);

	public List<ProjTypeVO> findAllByType(String term);

	public Map<Long, String> getStatuses();

	public Page<AtmtFileVO> getAttachments(String code, Pageable pageable);

	public void download(Long id, HttpServletRequest request, HttpServletResponse response);

	public void removeAttachmentFile(String code, AtmtFileVO atmtFileVO, UserDetails user) throws Exception;

	public void addAttachmentFile(String code, MultipartHttpServletRequest request);

	public Page<ProjMngExlVO> findAllForExcel(Pageable pageable, Searchable searchable);

	public Page<ProjMngExlVO> findAllForExcel(Pageable pageable);
}
