package com.gsitm.devops.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 프로젝트 공수 실적
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class EffortResult extends AbstractAuditable<Account, Long> {

	/**
	 * 작업 일자
	 */
	private LocalDate workDate;

	/**
	 * 작업 유형 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * 착수
	 */
	@Column(precision = 3, scale = 1)
	private Float beginning;

	/**
	 * 분석
	 */
	@Column(precision = 3, scale = 1)
	private Float analysis;

	/**
	 * 설계
	 */
	@Column(precision = 3, scale = 1)
	private Float design;

	/**
	 * 개발
	 */
	@Column(precision = 3, scale = 1)
	private Float development;

	/**
	 * 테스트
	 */
	@Column(precision = 3, scale = 1)
	private Float test;

	/**
	 * 이행
	 */
	@Column(precision = 3, scale = 1)
	private Float execution;

	/**
	 * 교육
	 */
	@Column(precision = 3, scale = 1)
	private Float education;

	/**
	 * 기타
	 */
	@Column(precision = 3, scale = 1)
	private Float etc;
}
