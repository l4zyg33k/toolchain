package com.gsitm.devops.pms.controller.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjItemVO;
import com.gsitm.devops.pms.model.vo.ProjMngCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMngGrdVO;
import com.gsitm.devops.pms.model.vo.ProjTypeVO;
import com.gsitm.devops.pms.service.biz.ProjMngBiz;

@RestController
public class ProjMngRestController {

	@Autowired
	private ProjMngBiz biz;

	@RequestMapping(value = "/rest/pms/projmng", method = RequestMethod.POST)
	public Page<ProjMngGrdVO> findAll(Pageable pageable, @AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, user);
	}

	@RequestMapping(value = "/rest/pms/projmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			@AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, searchable, user);
	}

	@RequestMapping(value = "/rest/pms/projmng", method = RequestMethod.GET, params = { "term" })
	public List<ProjItemVO> findAll(@RequestParam("term") String term) {
		return biz.findAll(term);
	}

	@RequestMapping(value = "/rest/pms/projmng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjMngCmdVO projMngCmdVO, BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (projMngCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next().getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projMngCmdVO.getOper()) {
		case "del":
			biz.delete(projMngCmdVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/projType", method = RequestMethod.GET, params = { "term" })
	public List<ProjTypeVO> findAllByType(@RequestParam("term") String term) {
		return biz.findAllByType(term);
	}

	@RequestMapping(value = "/rest/pms/projmng/{code}/attachments", method = RequestMethod.POST)
	public Page<AtmtFileVO> getAttachments(@PathVariable("code") String code, Pageable pageable) {
		return biz.getAttachments(code, pageable);
	}

	@RequestMapping(value = "/rest/pms/projmng/*/attachments", method = RequestMethod.GET)
	public void download(@RequestParam("id") Long id, HttpServletRequest request, HttpServletResponse response) {
		biz.download(id, request, response);
	}

	@RequestMapping(value = "/rest/pms/projmng/{code}/attachments/edit", method = RequestMethod.POST)
	public JqResult removeAttachment(@PathVariable("code") String code, @Valid AtmtFileVO atmtFileVO,
			BindingResult result, @AuthenticationPrincipal UserDetails user) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (atmtFileVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next().getDefaultMessage();
			return new JqFailure(message);
		}

		switch (atmtFileVO.getOper()) {
		case "del":
			try {
				biz.removeAttachmentFile(code, atmtFileVO, user);
			} catch (Exception e) {
				return new JqFailure(e.getMessage());
			}
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/projmng/{code}/attachments/upload", method = RequestMethod.POST)
	public JqResult addAttachment(@PathVariable("code") String code, MultipartHttpServletRequest request) {
		biz.addAttachmentFile(code, request);
		return new JqSuccess();
	}
}
