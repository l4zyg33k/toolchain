package com.gsitm.devops.pms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.pms.model.ProjectInspection;

@Repository
public interface ProjectInspectionRepository extends
		SharedRepository<ProjectInspection, Long> {
}
