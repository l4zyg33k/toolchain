package com.gsitm.devops.pms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.gsitm.devops.pms.model.ProjectSizeDetail;

@Repository
public interface ProjectSizeDetailRepository extends
		JpaRepository<ProjectSizeDetail, Long>,
		QueryDslPredicateExecutor<ProjectSizeDetail> {

}
