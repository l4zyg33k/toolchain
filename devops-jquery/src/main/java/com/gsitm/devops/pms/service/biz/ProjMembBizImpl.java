package com.gsitm.devops.pms.service.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.adm.model.vo.QAcntMngGrdVO;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.ProjectMember;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.model.QProjectMember;
import com.gsitm.devops.pms.model.vo.ProjMembCmdMbsVO;
import com.gsitm.devops.pms.model.vo.ProjMembCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMembFrmVO;
import com.gsitm.devops.pms.model.vo.ProjMembGrdVO;
import com.gsitm.devops.pms.model.vo.QProjMembFrmVO;
import com.gsitm.devops.pms.model.vo.QProjMembGrdVO;
import com.gsitm.devops.pms.service.EffortPlanService;
import com.gsitm.devops.pms.service.ProjectMemberService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class ProjMembBizImpl implements ProjMembBiz {

	// 메시지 처리
	private MessageSource messageSource;

	@Autowired
	private ProjectMemberService pmService;

	@Autowired
	private EffortPlanService epService;

	@PersistenceContext
	private EntityManager em;

	// 계정
	@Autowired
	private AccountService accountService;

	// 코드
	@Autowired
	private CodeService codeService;

	@Override
	public void create(ProjMembCmdVO vo) {
		ProjectMember projectMember = new ProjectMember();
		BeanUtils.copyProperties(vo, projectMember);
		pmService.save(projectMember);
	}

	@Override
	public void create(ProjMembCmdMbsVO projMembCmdMbsVO) {
		List<ProjectMember> pms = new ArrayList<>();

		for (Account newbie : projMembCmdMbsVO.getMembers()) {
			ProjectMember member = new ProjectMember();
			member.setProject(projMembCmdMbsVO.getProject());
			member.setMember(newbie);
			member.setRole(projMembCmdMbsVO.getRole());
			member.setStartDate(projMembCmdMbsVO.getStartDate());
			member.setEndDate(projMembCmdMbsVO.getEndDate());

			pms.add(member);
		}

		pms = pmService.save(pms);

		for (ProjectMember pm : pms) {
			pmService.createEffortPlans(pm);
		}
	}

	@Override
	public void delete(ProjMembCmdVO vo) {
		pmService.delete(vo.getId());
	}

	@Override
	public Page<ProjMembGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, String projectCode) {
		QProjectMember pm = QProjectMember.projectMember;

		QAccount account = new QAccount("account"); // 계정
		QProject project = new QProject("project"); // 프로젝트
		QCode role = new QCode("role"); // 파트 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		// 조회조건 추가 - 프로젝트코드
		BooleanBuilder conditions = new BooleanBuilder();
		if (projectCode != null) {
			conditions.and(pm.project.code.eq(projectCode));
		}

		PathBuilder<ProjectMember> path = new PathBuilder<ProjectMember>(
				ProjectMember.class, "projectMember");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(pm)
				.leftJoin(pm.member, account).leftJoin(pm.project, project)
				.leftJoin(pm.role, role).leftJoin(pm.createdBy, createdBy)
				.where(predicate, conditions);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMembGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjMembGrdVO(pm.id, project.name,
					project.code, account.username, account.name, role.id,
					pm.startDate, pm.endDate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(project.il.username.eq(user.getUsername()))
					.then(true).when(createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QProjMembGrdVO(pm.id, project.name,
					project.code, account.username, account.name, role.id,
					pm.startDate, pm.endDate, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjMembGrdVO> findAll(Pageable pageable, UserDetails user,
			String projectCode) {
		QProjectMember pm = QProjectMember.projectMember;

		QAccount account = new QAccount("account"); // 계정
		QProject project = new QProject("project"); // 프로젝트
		QCode role = new QCode("role"); // 파트 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		// 조회조건 추가 - 프로젝트코드
		BooleanBuilder conditions = new BooleanBuilder();
		if (projectCode != null) {
			conditions.and(project.code.eq(projectCode));
		}

		JPQLQuery query = new JPAQuery(em).from(pm)
				.leftJoin(pm.member, account).leftJoin(pm.project, project)
				.leftJoin(pm.role, role).leftJoin(pm.createdBy, createdBy)
				.where(conditions);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<ProjectMember>(
				ProjectMember.class, "projectMember"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjMembGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjMembGrdVO(pm.id, project.name,
					project.code, account.username, account.name, role.id,
					pm.startDate, pm.endDate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(project.il.username.eq(user.getUsername()))
					.then(true).when(createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QProjMembGrdVO(pm.id, project.name,
					project.code, account.username, account.name, role.id,
					pm.startDate, pm.endDate, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public ProjMembFrmVO findOne(Long id) {
		QProjectMember pm = QProjectMember.projectMember;

		QAccount account = new QAccount("account"); // 계정
		QProject project = new QProject("project"); // 프로젝트
		QCode role = new QCode("role"); // 파트 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		JPQLQuery query = new JPAQuery(em).from(pm)
				.leftJoin(pm.member, account).leftJoin(pm.project, project)
				.leftJoin(pm.role, role).leftJoin(pm.createdBy, createdBy)
				.where(pm.id.eq(id));

		return query.uniqueResult(new QProjMembFrmVO(pm.id, project.code,
				project.name, account.username, account.name, role.id,
				pm.startDate, pm.endDate));
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public void update(ProjMembCmdVO vo) {
		ProjectMember projectMember = pmService.findOne(vo.getId());
		BeanUtils.copyProperties(vo, projectMember);

		// 저장 처리
		projectMember = pmService.save(projectMember);
		pmService.updateEffortPlans(projectMember);
	}

	@Override
	public Map<Long, String> getRoles() {
		return codeService.getOptions("Q0");
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable, String project) {
		QProjectMember pm = QProjectMember.projectMember;
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		JPQLQuery query = new JPAQuery(em)
				.from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team)
				.where(account.notIn(new JPASubQuery().from(pm)
						.where(pm.project.code.eq(project)).list(pm.member)));
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Account>(
				Account.class, "account"));
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			String project) {
		QProjectMember pm = QProjectMember.projectMember;
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		PathBuilder<Account> path = new PathBuilder<Account>(Account.class,
				"account");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em)
				.from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team)
				.where(predicate,
						account.notIn(new JPASubQuery().from(pm)
								.where(pm.project.code.eq(project))
								.list(pm.member)));
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Map<String, String> getProjects(UserDetails user) {
		QProject project = QProject.project;
		LocalDate today = new LocalDate();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		BooleanBuilder builder = new BooleanBuilder();
		// 관리자가 아니면 로긴한 사용자가 IL을 담당하는 프로젝트만 표시한다.
		if (!authorities.contains("ROLE_ADMIN")) {
			builder.and(project.il.username.eq(user.getUsername()));
		}
		JPQLQuery query = new JPAQuery(em).from(project).where(
				builder.getValue(), project.startDate.before(today),
				project.finishDate.after(today));
		return query.map(project.code, project.code.prepend("[ ").append(" ] ")
				.append(project.name));
	}
}
