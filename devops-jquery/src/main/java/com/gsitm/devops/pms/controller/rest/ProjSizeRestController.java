package com.gsitm.devops.pms.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjSizeCmdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeGrdVO;
import com.gsitm.devops.pms.service.biz.ProjSizeBiz;

@RestController
public class ProjSizeRestController {

	@Autowired
	private ProjSizeBiz biz;

	/**
	 * @SpecialLogic 프로젝트 size 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjSizeGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projsize", method = RequestMethod.POST)
	public Page<ProjSizeGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, user, project);
	}

	/**
	 * @SpecialLogic 프로젝트 size 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjSizeGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projsize", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjSizeGrdVO> findAll(
			Pageable pageable,
			Searchable searchable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, searchable, user, project);
	}

	/**
	 * @SpecialLogic 프로젝트 size별 상세 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param id
	 *            : 프로젝트 size id
	 * @return Page<ProjSizeDetlGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projsize/{id}/details", method = RequestMethod.POST)
	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable,
			@PathVariable("id") Long id) {
		return biz.findAll(pageable, id);
	}

	/**
	 * @SpecialLogic 프로젝트 size별 상세 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param id
	 *            : 프로젝트 size id
	 * @return Page<ProjSizeDetlGrdVO>
	 */
	@RequestMapping(value = "/rest/pms/projsize/{id}/details", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, @PathVariable("id") Long id) {
		return biz.findAll(pageable, searchable, id);
	}

	/**
	 * @SpecialLogic 프로젝트 size 삭제
	 * 
	 * @param oper
	 * @param projSizeCmdVO
	 */
	@RequestMapping(value = "/rest/pms/projsize/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjSizeCmdVO projSizeCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (projSizeCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projSizeCmdVO.getOper()) {
		case "del":
			biz.delete(projSizeCmdVO);
			break;
		}

		return new JqSuccess();
	}

	/**
	 * @SpecialLogic 프로젝트 size별 상세 - 등록, 수정, 삭제 처리
	 * 
	 * @param oper
	 * @param id
	 * @param projSizeDetlFrmVO
	 * @param result
	 */
	@RequestMapping(value = "/rest/pms/projsize/{id}/details/edit", method = RequestMethod.POST)
	public JqResult formEditing(
			@PathVariable("id") Long id,
			@RequestParam(value = "projectTypeCode", defaultValue = "", required = false) String projectTypeCode,
			@Valid ProjSizeDetlFrmVO projSizeDetlFrmVO, BindingResult result) {

		// 추가, 삭제인 경우 유효성 검사를 하지 않는다.
		if (projSizeDetlFrmVO.isNew() == false
				&& projSizeDetlFrmVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projSizeDetlFrmVO.getOper()) {
		case "add":
			biz.create(projSizeDetlFrmVO, id, projectTypeCode);
			break;
		case "edit":
			biz.update(projSizeDetlFrmVO, id, projectTypeCode);
			break;
		case "del":
			biz.delete(projSizeDetlFrmVO, id);
			break;
		}

		return new JqSuccess();
	}
}
