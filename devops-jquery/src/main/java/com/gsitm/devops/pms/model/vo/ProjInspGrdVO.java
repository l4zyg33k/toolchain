package com.gsitm.devops.pms.model.vo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjInspGrdVO {

	private Long id;

	@JsonProperty("project.code")
	private String projectCode;

	@JsonProperty("project.name")
	private String projectName;

	private String productName;

	@JsonProperty("productType.name")
	private String productTypeName;

	@JsonProperty("productPhase.name")
	private String productPhaseName;

	private Integer productSize;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate inspectionDate;

	@JsonProperty("project.il.name")
	private String projectIlName;

	private Boolean editable;

	@QueryProjection
	public ProjInspGrdVO(Long id, String projectCode, String projectName,
			String productName, String productTypeName,
			String productPhaseName, Integer productSize,
			LocalDate inspectionDate, String projectIlName) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.productName = productName;
		this.productTypeName = productTypeName;
		this.productPhaseName = productPhaseName;
		this.productSize = productSize;
		this.inspectionDate = inspectionDate;
		this.projectIlName = projectIlName;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public ProjInspGrdVO(Long id, String projectCode, String projectName,
			String productName, String productTypeName,
			String productPhaseName, Integer productSize,
			LocalDate inspectionDate, String projectIlName, Boolean editable) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.productName = productName;
		this.productTypeName = productTypeName;
		this.productPhaseName = productPhaseName;
		this.productSize = productSize;
		this.inspectionDate = inspectionDate;
		this.projectIlName = projectIlName;
		this.editable = editable;
	}
}
