/**
 * 스티어링커미티 > 회의안건
 * Agenda.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Agenda extends AbstractAuditable<Account, Long> {

	/**
	 * 안건
	 */
	@NotNull
	@Column(length = 512)
	private String issue;

	/**
	 * 회의 결과
	 */
	@Column(length = 512)
	private String outcome;

	/**
	 * 결과 검토
	 */
	@Column(length = 512)
	private String review;
}
