package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.ProjectInspectionDetail;

public interface ProjectInspectionDetailService extends
		SharedService<ProjectInspectionDetail, Long> {

}
