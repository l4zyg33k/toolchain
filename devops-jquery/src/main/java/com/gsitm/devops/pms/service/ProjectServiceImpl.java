package com.gsitm.devops.pms.service;

import static com.mysema.query.group.GroupBy.groupBy;
import static com.mysema.query.group.GroupBy.list;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.QProjectWeeklyReport;
import com.gsitm.devops.pms.dto.ProjectProgress;
import com.gsitm.devops.pms.dto.QProjectProgress;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.QCommittee;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.repository.ProjectRepository;
import com.gsitm.devops.qam.dto.ProjectQualityDto;
import com.gsitm.devops.qam.model.QDocumentEvaluation;
import com.gsitm.devops.qam.model.QProgressEvaluation;
import com.gsitm.devops.qam.model.QSatisfactionSurveySummary;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Transactional(readOnly = true)
public class ProjectServiceImpl implements ProjectService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ProjectRepository repository;

	@Override
	public boolean exists(String id) {
		return repository.exists(id);
	}

	@Override
	public Project findOne(String id) {
		return repository.findOne(id);
	}

	@Override
	public Project findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Project> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Project> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Project> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Project> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Project> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Project> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Project save(Project project) {
		return repository.save(project);
	}

	@Transactional
	@Override
	public <S extends Project> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(String code) {
		repository.delete(code);
	}

	@Transactional
	@Override
	public void delete(Project project) {
		repository.delete(project);
	}

	@Override
	public void delete(Iterable<? extends Project> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Override
	public Project setNewCode(Project project) {

		// if (project.getCode().isEmpty()) {
		if (project.getId() == null) {
			String year = Integer.toString(Calendar.getInstance().get(
					Calendar.YEAR));
			String lastSeq = new JPAQuery(entityManager).from(QProject.project)
					.where(QProject.project.code.startsWith(year))
					.uniqueResult(QProject.project.code.substring(4, 6).max());
			if (lastSeq == null) {
				lastSeq = "00";
			}
			String nextSeq = String.format("%02d",
					Integer.parseInt(lastSeq) + 1);
			project.setCode(year + nextSeq + project.getOffice());
			return project;
		} else {
			return project;
		}
	}

	@Override
	public Page<ProjectProgress> findProjectProgressByDivision(
			Predicate predicate, Pageable pageable) {

		JPQLQuery jpqlQuery = new JPAQuery(entityManager);

		PathBuilder<Project> builder = new PathBuilder<Project>(Project.class,
				"project");
		Querydsl querydsl = new Querydsl(entityManager, builder);
		JPQLQuery query = querydsl.applyPagination(pageable, jpqlQuery);

		QProject qProject = QProject.project;
		QProjectWeeklyReport qProjectWeeklyReport = QProjectWeeklyReport.projectWeeklyReport;
		QCommittee qCommittee = QCommittee.committee;

		Map<List<?>, ProjectProgress> results = query
				.from(qProject)
				.innerJoin(qProject.projectWeeklyReport, qProjectWeeklyReport)
				.leftJoin(qProject.committees, qCommittee)
				.where(predicate,
						qProjectWeeklyReport.id.eq(new JPASubQuery()
								.from(qProjectWeeklyReport)
								.where(qProjectWeeklyReport.project
										.eq(QProject.project))
								.unique(qProjectWeeklyReport.id.max())))
				.offset(0)
				.limit(Long.MAX_VALUE)
				.transform(
						groupBy(QProject.project, qProjectWeeklyReport.phase)
								.as(new QProjectProgress(qProjectWeeklyReport,
										list(qCommittee))));

		List<ProjectProgress> projectProgressVO = new ArrayList<ProjectProgress>(
				results.values());

		int total = projectProgressVO.size();
		int from = pageable.getOffset() <= 0 ? 0 : pageable.getOffset() - 1;
		int to = total <= from + pageable.getPageSize() ? total : from
				+ pageable.getPageSize();

		projectProgressVO = projectProgressVO.subList(from, to);

		return new PageImpl<>(projectProgressVO, pageable, total);
	}

	@Override
	public Page<ProjectQualityDto> findResultDto(Predicate predicate,
			Pageable pageable) {
		JPQLQuery jpqlQuery = new JPAQuery(entityManager);
		QProject qProject = QProject.project;
		QProgressEvaluation qProgressEvaluation = QProgressEvaluation.progressEvaluation;
		// 서브쿼리 사용
		QProgressEvaluation qProgressEvaluationSub = new QProgressEvaluation(
				"sub1");
		QDocumentEvaluation qDocumentEvaluation = new QDocumentEvaluation(
				"sub2");
		QSatisfactionSurveySummary qSatisfactionSurveySummarySub = new QSatisfactionSurveySummary(
				"sub3");
		/*
		 * DateSubQuery<Date> s1 = new
		 * JPASubQuery().from(qProgressEvaluationSub)
		 * .where(qProgressEvaluationSub.projectStep.code.eq("G0"))
		 * .unique(qProgressEvaluationSub.realEvaluationDate.max());
		 */

		// 페이징 처리(그리드sort정보와 path경로가 안맞을경우 그리드의 sort정보 수정 필요)
		pageable.getSort().toString();
		PathBuilder<Project> builder = new PathBuilder<Project>(Project.class,
				"project");
		Querydsl querydsl = new Querydsl(entityManager, builder);
		JPQLQuery query = querydsl.applyPagination(pageable, jpqlQuery);

		// 조회
		// List<ProjectQualityDto> result = null;
		List<ProjectQualityDto> result = query
				.from(qProject)
				.where(predicate)
				.list( // new QProjectQualityDto(
				ConstructorExpression
						.create(ProjectQualityDto.class,
								qProject,
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G0"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.planDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G1"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G2"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G3"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G4"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.step
												.eq("G5"),
												qProgressEvaluationSub.project
														.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualDate
												.max()),
								new JPASubQuery()
										.from(qProgressEvaluationSub)
										.where(qProgressEvaluationSub.project
												.eq(QProject.project))
										.unique(qProgressEvaluationSub.actualRate
												.sum()
												.divide(qProgressEvaluationSub.planRate
														.sum()).multiply(10000)
												.round().divide(100)),
								new JPASubQuery()
										.from(qDocumentEvaluation)
										.where(qDocumentEvaluation.project
												.eq(QProject.project))
										.groupBy(qDocumentEvaluation.project)
										.unique(qDocumentEvaluation.weight
												.add(qDocumentEvaluation.point)
												.sum()
												.divide(qDocumentEvaluation.weight
														.sum())),
								new JPASubQuery()
										.from(qSatisfactionSurveySummarySub)
										.where(qSatisfactionSurveySummarySub.project
												.eq(QProject.project))
										.groupBy(
												qSatisfactionSurveySummarySub.project)
										.unique(qSatisfactionSurveySummarySub.point
												.multiply(
														qSatisfactionSurveySummarySub.answerCnt)
												.sum()
												.divide(qSatisfactionSurveySummarySub.answerCnt
														.sum()))

						));

		// 페이징처리 관련
		JPQLQuery jpqlQueryCount = new JPAQuery(entityManager);
		long total = 0;

		List<Project> resultList = jpqlQueryCount.from(qProject)
				.leftJoin(qProject.progressEvaluations, qProgressEvaluation)
				.where(predicate).groupBy(qProject).list(qProject);
		total = resultList.size();
		// return new PageImpl<>(null);
		return new PageImpl<>(result, pageable, total);
	}
}
