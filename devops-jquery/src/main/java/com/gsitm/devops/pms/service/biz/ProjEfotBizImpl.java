package com.gsitm.devops.pms.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.EffortPlan;
import com.gsitm.devops.pms.model.EffortResult;
import com.gsitm.devops.pms.model.ProjectMember;
import com.gsitm.devops.pms.model.QEffortPlan;
import com.gsitm.devops.pms.model.QEffortResult;
import com.gsitm.devops.pms.model.QProjectMember;
import com.gsitm.devops.pms.model.vo.ProjEfotDetlCmdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotFrmVO;
import com.gsitm.devops.pms.model.vo.ProjEfotGrdVO;
import com.gsitm.devops.pms.model.vo.QProjEfotDetlGrdVO;
import com.gsitm.devops.pms.model.vo.QProjEfotGrdVO;
import com.gsitm.devops.pms.service.EffortPlanService;
import com.gsitm.devops.pms.service.EffortResultService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Expression;
import com.mysema.query.types.path.PathBuilder;

@Getter
@Setter
@Service
public class ProjEfotBizImpl implements ProjEfotBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CodeService cService;

	@Autowired
	private EffortPlanService epService;

	@Autowired
	private EffortResultService erService;

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public Map<String, String> getProjects(UserDetails user) {
		Assert.notNull(user, "로그인 하지 않은 사용자");
		QProjectMember pm = QProjectMember.projectMember;
		LocalDate today = new LocalDate();

		// 프로젝트 시작ᅟ일이 오늘 보다 작거나 같고, 종료일이 오는 보다 크거나 같은 프로젝트 코드와 이름을 반환한다.
		return new JPAQuery(em)
				.from(pm)
				.where(pm.member.username.eq(user.getUsername()),
						pm.project.startDate.loe(today),
						pm.project.finishDate.goe(today))
				.orderBy(pm.project.startDate.desc())
				.map(pm.project.code,
						pm.project.code.prepend("[ ").append(" ] ")
								.append(pm.project.name));
	}

	@Override
	public Page<ProjEfotGrdVO> findAll(Pageable pageable, String project,
			UserDetails user) {

		QProjectMember memb = QProjectMember.projectMember;
		QEffortPlan plan = QEffortPlan.effortPlan;

		PathBuilder<ProjectMember> path = new PathBuilder<ProjectMember>(
				ProjectMember.class, "projectMember");

		JPQLQuery query = new JPAQuery(em)
				.from(memb)
				.leftJoin(memb.plans, plan)
				.where(memb.member.username.eq(user.getUsername()),
						memb.project.code.eq(project));

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		Expression<String> week = plan.monday.stringValue().append(" ~ ")
				.append(plan.sunday.stringValue());

		Expression<Float> total = plan.beginning.add(plan.analysis
				.add(plan.development.add(plan.test.add(plan.execution
						.add(plan.education.add(plan.etc))))));

		SearchResults<ProjEfotGrdVO> search = query
				.listResults(new QProjEfotGrdVO(plan.id, week,
						memb.project.workingHour.multiply(plan.manDays),
						plan.beginning, plan.analysis, plan.design,
						plan.development, plan.test, plan.execution,
						plan.education, plan.etc, total));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public ProjEfotFrmVO findOne(long id) {
		QProjectMember memb1 = new QProjectMember("memb1");
		QProjectMember memb2 = new QProjectMember("memb2");
		QEffortPlan plan1 = new QEffortPlan("plan1");
		QEffortPlan plan2 = new QEffortPlan("plan2");

		JPQLQuery query = new JPAQuery(em)
				.from(memb1)
				.innerJoin(memb1.plans, plan1)
				.where(memb1.eq(new JPASubQuery().from(memb2)
						.innerJoin(memb2.plans, plan2).where(plan2.id.eq(id))
						.unique(memb2)));

		Expression<String> week = plan1.monday.stringValue().append(" ~ ")
				.append(plan1.sunday.stringValue());

		Map<Long, String> weeks = query.map(plan1.id, week);

		return new ProjEfotFrmVO(id, weeks);
	}

	@Override
	public Map<Long, String> getTypes() {
		return cService.getOptions("Q3");
	}

	@Override
	public Page<ProjEfotDetlGrdVO> findAll(Pageable pageable, Long plan,
			Long type) {
		QEffortPlan ep = QEffortPlan.effortPlan;
		QEffortResult er = QEffortResult.effortResult;

		JPQLQuery query = new JPAQuery(em).from(ep).leftJoin(ep.results, er)
				.where(ep.id.eq(plan), er.type.id.eq(type));

		PathBuilder<EffortPlan> path = new PathBuilder<EffortPlan>(
				EffortPlan.class, "effortPlan");

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjEfotDetlGrdVO> search = query
				.listResults(new QProjEfotDetlGrdVO(er.id, er.workDate,
						er.beginning, er.analysis, er.design, er.development,
						er.test, er.execution, er.education, er.etc));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public void create(ProjEfotDetlCmdVO projEfotDetlCmdVO)
			throws IllegalArgumentException {
		EffortPlan plan = epService.findOne(projEfotDetlCmdVO.getPlan());
		EffortResult result = new EffortResult();
		BeanUtils.copyProperties(projEfotDetlCmdVO, result);

		Interval interval = new Interval(plan.getMonday()
				.toDateTimeAtStartOfDay(), plan.getSunday().plusDays(1)
				.toDateTimeAtStartOfDay());

		if (interval.contains(projEfotDetlCmdVO.getWorkDate()
				.toDateTimeAtStartOfDay()) == false) {
			throw new IllegalArgumentException("수행 기간에 해당하는 일자가 아닙니다.");
		}

		result = erService.save(result);
		plan.getResults().add(result);

		epService.save(plan);
		epService.summerizeEffortResultsById(projEfotDetlCmdVO.getPlan());
	}

	@Override
	public void update(ProjEfotDetlCmdVO projEfotDetlCmdVO) {
		EffortResult result = erService.findOne(projEfotDetlCmdVO.getId());
		BeanUtils.copyProperties(projEfotDetlCmdVO, result);

		erService.save(result);
		epService.summerizeEffortResultsById(projEfotDetlCmdVO.getPlan());
	}

	@Override
	public void delete(ProjEfotDetlCmdVO projEfotDetlCmdVO) {
		EffortPlan plan = epService.findOne(projEfotDetlCmdVO.getPlan());
		EffortResult result = erService.findOne(projEfotDetlCmdVO.getId());

		plan.getResults().remove(result);

		epService.save(plan);
		erService.delete(result);
		epService.summerizeEffortResultsById(projEfotDetlCmdVO.getPlan());
	}
}
