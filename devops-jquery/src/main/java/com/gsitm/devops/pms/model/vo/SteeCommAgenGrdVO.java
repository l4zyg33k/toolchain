package com.gsitm.devops.pms.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SteeCommAgenGrdVO {

	private String oper;

	private Long id;

	private String issue;

	private String outcome;

	private String review;

	public SteeCommAgenGrdVO() {
		super();
	}

	@QueryProjection
	public SteeCommAgenGrdVO(Long id, String issue, String outcome,
			String review) {
		super();
		this.id = id;
		this.issue = issue;
		this.outcome = outcome;
		this.review = review;
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
