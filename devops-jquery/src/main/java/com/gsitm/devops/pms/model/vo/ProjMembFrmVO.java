package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjMembFrmVO extends ProjMembWebVO {

	private Long id;

	// 프로젝트
	private String project; // 프로젝트 코드
	private String projectName; // 프로젝트 명

	// 팀원 정보
	private String member; // username
	private String memberName; // name

	// 역할
	private Long role;

	// 투입시작일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	// 투입종료일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate endDate;


	public ProjMembFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	@QueryProjection
	public ProjMembFrmVO(Long id, String project, String projectName,
			String member, String memberName, Long role, LocalDate startDate,
			LocalDate endDate) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.member = member;
		this.memberName = memberName;
		this.role = role;
		this.startDate = startDate;
		this.endDate = endDate;
	}

}
