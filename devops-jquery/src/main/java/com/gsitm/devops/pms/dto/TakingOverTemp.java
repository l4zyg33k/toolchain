package com.gsitm.devops.pms.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.pms.model.Project;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TakingOverTemp {
	private Account writer;
	private String directory;
	private List<SessionMultipartFile> files = new ArrayList<>();
	private System system;
	private Project project;

	public Account getWriter() {
		return writer;
	}

	public void setWriter(Account writer) {
		this.writer = writer;
	}

	public List<SessionMultipartFile> getFiles() {
		return files;
	}

	public void setFile(List<SessionMultipartFile> files) {
		this.files = files;
	}

	public void addFile(SessionMultipartFile file) {
		this.files.add(file);
	}

	public void removeFile(int index) {
		this.files.remove(index);
	}

	public void removeAllFiles() {
		this.files.clear();
	}

	public void removeFiles(List<SessionMultipartFile> files) {
		this.files.removeAll(files);
	}

	public System getSystem() {
		return system;
	}

	public void setSystem(System system) {
		this.system = system;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}
}
