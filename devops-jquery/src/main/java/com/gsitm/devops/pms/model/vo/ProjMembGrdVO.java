package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjMembGrdVO implements Serializable {

	private Long id;

	// 프로젝트
	@JsonProperty("project.name")
	private String projectName;

	// 프로젝트 코드
	@JsonProperty("project.code")
	private String projectCode;

	// 팀원 아이디
	@JsonProperty("member.username")
	private String memberUsername;

	// 팀원 이름
	@JsonProperty("member.name")
	private String memberName;

	// 역활 (코드)
	private Long role;

	// 투입 시작 일자
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate startDate;

	// 투입 종료 일자
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate endDate;

	// 변경 권한
	private Boolean editable;

	@QueryProjection
	public ProjMembGrdVO(Long id, String projectName, String projectCode,
			String memberUsername, String memberName, Long role,
			LocalDate startDate, LocalDate endDate) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.projectCode = projectCode;
		this.memberUsername = memberUsername;
		this.memberName = memberName;
		this.role = role;
		this.startDate = startDate;
		this.endDate = endDate;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public ProjMembGrdVO(Long id, String projectName, String projectCode,
			String memberUsername, String memberName, Long role,
			LocalDate startDate, LocalDate endDate, Boolean editable) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.projectCode = projectCode;
		this.memberUsername = memberUsername;
		this.memberName = memberName;
		this.role = role;
		this.startDate = startDate;
		this.endDate = endDate;
		this.editable = editable;
	}
}
