package com.gsitm.devops.pms.model.vo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SteeCommGrdVO {

	private Long id;

	@JsonProperty("project.name")
	private String projectName;

	private String title;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate openingDate;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate actualOpeningDate;

	@JsonProperty("project.il.name")
	private String projectIlName;

	private Boolean editable;

	@QueryProjection
	public SteeCommGrdVO(Long id, String projectName, String title,
			LocalDate openingDate, LocalDate actualOpeningDate,
			String projectIlName) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.title = title;
		this.openingDate = openingDate;
		this.actualOpeningDate = actualOpeningDate;
		this.projectIlName = projectIlName;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public SteeCommGrdVO(Long id, String projectName, String title,
			LocalDate openingDate, LocalDate actualOpeningDate,
			String projectIlName, Boolean editable) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.title = title;
		this.openingDate = openingDate;
		this.actualOpeningDate = actualOpeningDate;
		this.projectIlName = projectIlName;
		this.editable = editable;
	}
}
