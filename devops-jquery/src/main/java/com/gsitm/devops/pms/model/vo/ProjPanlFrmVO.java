package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.util.StringUtils;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjPanlFrmVO extends ProjPanlWebVO {

	private String code;

	private String name;

	private Long type;

	private String typeName;

	private String pmName;

	private String blName;

	private String blDepartment;

	private String il;

	private String ilName;

	private String op;

	private String opName;

	private String ou;

	private String qa;

	private String qaName;

	private Long office;

	private String officeName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate finishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualFinishDate;

	private Long phase;

	private String phaseName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelFinishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelActualDate;

	@DateTimeFormat(pattern = "a h:mm")
	private LocalTime panelActualTime;

	private Long location;

	private String locationName;

	private String recommandation;

	private Long pass;

	private String passName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpFinishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpActualStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpActualFinishDate;

	private Float ouEffort = 0F;

	private Float ouActualEffort = 0F;

	private Float pdEffort = 0F;

	private Float pdActualEffort = 0F;

	private Float processPoint = 0F;

	private Float testingPoint = 0F;

	private Float productPoint = 0F;

	private Float customerPoint = 0F;

	private Float workingHour = 9F;

	private Float workingDay = 22F;

	private Float workingEffort = 0F;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate closeReportDate;

	private Long closeReportType;

	private String closeReportTypeName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openCheckDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openReportDate;

	private Boolean watching = Boolean.FALSE;

	private Boolean satisfactionExpired = Boolean.FALSE;

	public ProjPanlFrmVO(int page, int rowNum, String rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}

	@QueryProjection
	public ProjPanlFrmVO(String code, String name, Long type, String typeName,
			String pmName, String blName, String blDepartment, String il,
			String ilName, String op, String opName, String ou, String qa,
			String qaName, Long office, String officeName, LocalDate startDate,
			LocalDate finishDate, LocalDate actualStartDate,
			LocalDate actualFinishDate, Long phase, String phaseName,
			LocalDate panelStartDate, LocalDate panelFinishDate,
			LocalDate panelActualDate, LocalTime panelActualTime,
			Long location, String locationName, String recommandation,
			Long pass, String passName, LocalDate followUpStartDate,
			LocalDate followUpFinishDate, LocalDate followUpActualStartDate,
			LocalDate followUpActualFinishDate, Float ouEffort,
			Float ouActualEffort, Float pdEffort, Float pdActualEffort,
			Float processPoint, Float testingPoint, Float productPoint,
			Float customerPoint, Float workingHour, Float workingDay,
			Float workingEffort, LocalDate closeReportDate,
			Long closeReportType, String closeReportTypeName,
			LocalDate openCheckDate, LocalDate openReportDate,
			Boolean watching, Boolean satisfactionExpired) {
		super();
		this.code = code;
		this.name = name;
		this.type = type;
		this.typeName = typeName;
		this.pmName = pmName;
		this.blName = blName;
		this.blDepartment = blDepartment;
		this.il = il;
		this.ilName = ilName;
		this.op = op;
		this.opName = opName;
		this.ou = ou;
		this.qa = qa;
		this.qaName = qaName;
		this.office = office;
		this.officeName = officeName;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.actualStartDate = actualStartDate;
		this.actualFinishDate = actualFinishDate;
		this.phase = phase;
		this.phaseName = phaseName;
		this.panelStartDate = panelStartDate;
		this.panelFinishDate = panelFinishDate;
		this.panelActualDate = panelActualDate;
		this.panelActualTime = panelActualTime;
		this.location = location;
		this.locationName = locationName;
		this.recommandation = recommandation;
		this.pass = pass;
		this.followUpStartDate = followUpStartDate;
		this.followUpFinishDate = followUpFinishDate;
		this.followUpActualStartDate = followUpActualStartDate;
		this.followUpActualFinishDate = followUpActualFinishDate;
		this.ouEffort = ouEffort;
		this.ouActualEffort = ouActualEffort;
		this.pdEffort = pdEffort;
		this.pdActualEffort = pdActualEffort;
		this.processPoint = processPoint;
		this.testingPoint = testingPoint;
		this.productPoint = productPoint;
		this.customerPoint = customerPoint;
		this.workingHour = workingHour;
		this.workingDay = workingDay;
		this.workingEffort = workingEffort;
		this.closeReportDate = closeReportDate;
		this.closeReportType = closeReportType;
		this.closeReportTypeName = closeReportTypeName;
		this.openCheckDate = openCheckDate;
		this.openReportDate = openReportDate;
		this.watching = watching;
		this.satisfactionExpired = satisfactionExpired;
	}

	@Override
	public boolean isNew() {
		return StringUtils.isEmpty(getCode());
	}
}