package com.gsitm.devops.pms.model.vo;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * 프로젝트 공수 폼
 * 
 * @author lazygeek
 *
 */
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjEfotFrmVO extends ProjEfotWebVO {

	/**
	 * 공수 계획의 식별자
	 */
	private Long plan;

	/**
	 * 공수 계획 식별자의 컬렉션
	 */
	private Map<Long, String> plans;

	public ProjEfotFrmVO() {
		super();
	}

	public ProjEfotFrmVO(Long plan, Map<Long, String> plans) {
		this.plan = plan;
		this.plans = plans;
	}
}
