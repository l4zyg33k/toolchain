package com.gsitm.devops.pms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.pms.model.ProjectSize;

@Repository
public interface ProjectSizeRepository extends
		SharedRepository<ProjectSize, Long> {
}
