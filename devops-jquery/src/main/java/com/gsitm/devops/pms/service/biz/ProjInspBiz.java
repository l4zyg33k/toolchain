package com.gsitm.devops.pms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjInspCmdVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjInspFrmVO;
import com.gsitm.devops.pms.model.vo.ProjInspGrdVO;

public interface ProjInspBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public Map<Long, String> getProductTypes();

	public Map<Long, String> getProductPhases();

	public Page<ProjInspGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<ProjInspGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user);

	public ProjInspFrmVO findOne(Long id);

	public void create(ProjInspCmdVO projInspCmdVO);

	public void update(ProjInspCmdVO projInspCmdVO);

	public void delete(ProjInspCmdVO projInspCmdVO);

	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable, Long id);

	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id);

	public void create(ProjInspDetlFrmVO projInspDetlFrmVO, Long id);

	public void update(ProjInspDetlFrmVO projInspDetlFrmVO);

	public void delete(ProjInspDetlFrmVO projInspDetlFrmVO, Long id);

	public String getDefectTypes(Long id);

	public String getDefectGrades();
	
	public Page<ProjInspGrdVO> findAll(Pageable pageable, String projectCode);
}
