package com.gsitm.devops.pms.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.pms.model.ProjectMember;

@Repository
public interface ProjectMemberRepository extends
		SharedRepository<ProjectMember, Long> {
}
