package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjTypeVO {

	private String code;

	private String name;
	
	private String typeCode;
	
	private String typeName;
	
	@QueryProjection
	public ProjTypeVO(String code, String name, String typeCode, String typeName) {
		super();
		this.code = code;
		this.name = name;
		this.typeCode = typeCode;
		this.typeName = typeName;
	}
}
