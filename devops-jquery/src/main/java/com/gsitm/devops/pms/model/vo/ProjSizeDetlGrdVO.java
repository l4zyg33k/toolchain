package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjSizeDetlGrdVO {

	private Long id;

	// 프로젝트
	private Long projectSize;

	//보고 일자
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate reportDate;

	//신규(입력)
	private Integer newInput;

	//신규(조회)
	private Integer newSearch;

	//신규(배치)
	private Integer newBatch;

	//신규(보고서)
	private Integer newReport;

	//수정(입력)
	private Integer editInput;

	//수정(조회)
	private Integer editSearch;

	//수정(배치)
	private Integer editBatch;

	//수정(보고서)
	private Integer editReport;

	//총계
	private Integer totSum;

	//WW 환산
	private Integer wwCal;

	public ProjSizeDetlGrdVO() {
		super();
	}
	
	@QueryProjection
	public ProjSizeDetlGrdVO(Long id, Long projectSize, LocalDate reportDate,
			Integer newInput, Integer newSearch, Integer newBatch,
			Integer newReport, Integer editInput, Integer editSearch,
			Integer editBatch, Integer editReport, Integer totSum, Integer wwCal) {
		super();
		this.id = id;
		this.projectSize = projectSize;
		this.reportDate = reportDate;
		this.newInput = newInput;
		this.newSearch = newSearch;
		this.newBatch = newBatch;
		this.newReport = newReport;
		this.editInput = editInput;
		this.editSearch = editSearch;
		this.editBatch = editBatch;
		this.editReport = editReport;
		this.totSum = totSum;
		this.wwCal = wwCal;
	}
}
