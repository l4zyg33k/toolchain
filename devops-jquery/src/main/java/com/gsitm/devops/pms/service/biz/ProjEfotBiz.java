package com.gsitm.devops.pms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.pms.model.vo.ProjEfotDetlCmdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotFrmVO;
import com.gsitm.devops.pms.model.vo.ProjEfotGrdVO;

/**
 * 프로젝트 공수 비지니스
 * 
 * @author lazygeek
 *
 */
public interface ProjEfotBiz extends MessageSourceAware {

	/**
	 * 네비게이션 그리드의 권한을 반환한다.
	 * 
	 * @param user
	 *            로그인 한 유저 상세
	 * @return add, edit, view, del 권한
	 */
	public Map<String, Boolean> getNavGrid(UserDetails user);

	/**
	 * 참여 중인 프로젝트를 반환한다.
	 * 
	 * @param user
	 *            로그인 한 유저 상세
	 * @return 참여 중인 프로젝트
	 */
	public Map<String, String> getProjects(UserDetails user);

	/**
	 * 해당하는 프로젝트의 기간별 공수를 반환한다.
	 * 
	 * @param pageable
	 *            페이지 정보
	 * @param project
	 *            프로젝트 코드
	 * @param user
	 *            로그인 한 유저 상세
	 * @return 기간별 공수 그리드
	 */
	public Page<ProjEfotGrdVO> findAll(Pageable pageable, String project,
			UserDetails user);

	/**
	 * 식별자와 동일한 프로젝트 공수 폼을 반환한다.
	 * 
	 * @param id
	 *            식별자
	 * @return 프로젝트 공수 폼
	 */
	public ProjEfotFrmVO findOne(long id);

	/**
	 * 공수 유형
	 * 
	 * @return
	 */
	public Map<Long, String> getTypes();

	public Page<ProjEfotDetlGrdVO> findAll(Pageable pageable, Long plan,
			Long type);

	public void create(ProjEfotDetlCmdVO projEfotDetlCmdVO);

	public void update(ProjEfotDetlCmdVO projEfotDetlCmdVO);

	public void delete(ProjEfotDetlCmdVO projEfotDetlCmdVO);
}
