package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.EffortPlan;

public interface EffortPlanService extends
		SharedService<EffortPlan, Long> {
	
	public void summerizeEffortResultsById(Long id);
}
