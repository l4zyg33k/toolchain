package com.gsitm.devops.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.TakingOver;
import com.gsitm.devops.pms.repository.TakingOverRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public class TakingOverServiceImpl implements TakingOverService {

	@Autowired
	private TakingOverRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public TakingOver findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public TakingOver findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<TakingOver> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<TakingOver> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<TakingOver> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<TakingOver> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<TakingOver> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<TakingOver> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public TakingOver save(TakingOver TakingOver) {
		return repository.save(TakingOver);
	}

	@Transactional
	@Override
	public <S extends TakingOver> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(TakingOver TakingOver) {
		repository.delete(TakingOver);
	}

	@Override
	public void delete(Iterable<? extends TakingOver> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

}
