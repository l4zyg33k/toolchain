package com.gsitm.devops.pms.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.pms.model.vo.SteeCommCmdVO;
import com.gsitm.devops.pms.model.vo.SteeCommFrmVO;
import com.gsitm.devops.pms.service.biz.SteeCommBiz;

@Controller
public class SteeCommController {

	private static final String MENU_CODE = "SteeComm";
	private static final String FORM_VO = "steeCommFrmVO";

	private static final String INDEX_URL = "/pms/steecomm";
	private static final String ACTION_URL = "/pms/steecomm/*";
	private static final String FORM_URL = "/pms/steecomm/form";
	private static final String FORM_URL_BY_ID = "/pms/steecomm/{id}/form";
	private static final String VIEW_URL_BY_ID = "/pms/steecomm/{id}/view";
	private static final String REDIRECT_URL = "redirect:/pms/steecomm?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "pms/steecomm/index";
	private static final String FORM_PAGE = "pms/steecomm/form";
	private static final String VIEW_PAGE = "pms/steecomm/view";

	@Autowired
	private SteeCommBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("locations")
	public Map<Long, String> getLocations() {
		return biz.getLocations();
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId, Model model) {
		SteeCommFrmVO vo = new SteeCommFrmVO(page, rowNum, rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(@ModelAttribute(FORM_VO) @Valid SteeCommCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@PathVariable(PageParams.ID) Long id, Model model) {
		SteeCommFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid SteeCommCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = VIEW_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@PathVariable(PageParams.ID) long id, Model model) {
		SteeCommFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return VIEW_PAGE;
	}
}
