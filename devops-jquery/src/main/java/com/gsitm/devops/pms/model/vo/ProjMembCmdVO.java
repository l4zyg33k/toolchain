package com.gsitm.devops.pms.model.vo;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Code;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjMembCmdVO extends ProjMembWebVO {

	/**
	 * 역할
	 */
	private Code role;

	/**
	 * 투입시작일
	 */
	@NotNull(message = "투입일을 입력하세요.")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	/**
	 * 투입종료일
	 */
	@NotNull(message = "철수일을 입력하세요.")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate endDate;

	@AssertFalse(message = "투입일은 종료일 이전이 되어야 합니다.")
	public boolean isAfter() {
		if (this.oper.equals("del")) {
			return false;
		} else {
			try {
				return startDate.isAfter(endDate);
			} catch (IllegalArgumentException e) {
				return true;
			}
		}
	}
}
