package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjInspWebVO extends SharedWebVO<Long> {

	private String rowData;

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/pms/projinsp" : String.format("/pms/projinsp/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/pms/projinsp?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getDetailsUrl() {
		return String.format("/rest/pms/projinsp/%d/details", getId());
	}

	public String getDetailsEditUrl() {
		return String.format("/rest/pms/projinsp/%d/details/edit", getId());
	}
}
