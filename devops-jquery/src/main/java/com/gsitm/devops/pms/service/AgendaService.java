package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.Agenda;

public interface AgendaService extends SharedService<Agenda, Long> {
}
