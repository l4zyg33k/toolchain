package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
public class ProjMembCmdMbsVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Project project;

	private List<Account> members;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate endDate;

	private Code role;

	@AssertFalse(message = "투입일은 종료일 이전이 되어야 합니다.")
	public boolean isAfter() {
		try {
			return startDate.isAfter(endDate);
		} catch (IllegalArgumentException e) {
			return true;
		}
	}
}
