package com.gsitm.devops.pms.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.pms.model.vo.ProjEfotFrmVO;
import com.gsitm.devops.pms.service.biz.ProjEfotBiz;

/**
 * 프로젝트 공수 컨트롤러
 * 
 * @author lazygeek
 *
 */
@Controller
public class ProjEfotController {

	private static final String MENU_CODE = "ProjEfot";
	private static final String FORM_VO = "projEfotFrmVO";

	private static final String INDEX_URL = "/pms/projefot";
	private static final String FORM_URL_BY_ID = "/pms/projefot/{id}/form";

	private static final String INDEX_PAGE = "pms/projefot/index";
	private static final String FORM_PAGE = "pms/projefot/form";

	@Autowired
	private ProjEfotBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("project")
	public String getProject() {
		return "";
	}

	@ModelAttribute("projects")
	public Map<String, String> getProjects(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getProjects(user);
	}

	@ModelAttribute("type")
	public Long getType() {
		return 0L;
	}

	@ModelAttribute("types")
	public Map<Long, String> getTypes() {
		return biz.getTypes();
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@PathVariable(PageParams.ID) long id, Model model) {
		ProjEfotFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}
}
