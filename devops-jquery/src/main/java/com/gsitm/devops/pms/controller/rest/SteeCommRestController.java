package com.gsitm.devops.pms.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.SteeCommAgenGrdVO;
import com.gsitm.devops.pms.model.vo.SteeCommCmdVO;
import com.gsitm.devops.pms.model.vo.SteeCommGrdVO;
import com.gsitm.devops.pms.service.biz.SteeCommBiz;

@RestController
public class SteeCommRestController {

	@Autowired
	private SteeCommBiz biz;

	@RequestMapping(value = "/rest/pms/steecomm", method = RequestMethod.POST)
	public Page<SteeCommGrdVO> findAll(Pageable pageable,
			@AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, user);
	}

	@RequestMapping(value = "/rest/pms/steecomm", method = RequestMethod.POST, params = { "_search=true" })
	public Page<SteeCommGrdVO> findAll(Pageable pageable,
			Searchable searchable, @AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, searchable, user);
	}

	@RequestMapping(value = "/rest/pms/steecomm/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid SteeCommCmdVO steeCommCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (steeCommCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (steeCommCmdVO.getOper()) {
		case "del":
			biz.delete(steeCommCmdVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/steecomm/{id}/agendas", method = RequestMethod.POST)
	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable,
			@PathVariable("id") Long id) {
		return biz.findAll(pageable, id);
	}

	@RequestMapping(value = "/rest/pms/steecomm/{id}/agendas", method = RequestMethod.POST, params = { "_search=true" })
	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable,
			Searchable searchable, @PathVariable("id") Long id) {
		return biz.findAll(pageable, searchable, id);
	}

	@RequestMapping(value = "/rest/pms/steecomm/{id}/agendas/edit", method = RequestMethod.POST)
	public JqResult formEditing(@PathVariable("id") Long id,
			@Valid SteeCommAgenGrdVO steeCommAgenGrdVO, BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (steeCommAgenGrdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (steeCommAgenGrdVO.getOper()) {
		case "add":
			biz.create(steeCommAgenGrdVO, id);
			break;
		case "edit":
			biz.update(steeCommAgenGrdVO);
			break;
		case "del":
			biz.delete(steeCommAgenGrdVO, id);
			break;
		}

		return new JqSuccess();
	}
}
