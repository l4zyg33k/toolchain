package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjSizeCmdVO extends ProjSizeWebVO {
	//프로젝트
	private Project project;
	private String projectTypeName; //프로젝트 유형
	private String projectTypeCode; //프로젝트 유형 코드

	//프로젝트 이름
	@NotBlank
	private String projectName;

	//단계 (코드)
	private Code phase;	

	//Threshold
	private Integer threshold;

	//Contingency
	private Integer contingency;
}
