package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

/**
 * 프로젝트 공수 계획
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class EffortPlan extends AbstractAuditable<Account, Long> {

	/**
	 * 계획 시작 일자
	 */
	@Column(nullable = false)
	private LocalDate monday;

	/**
	 * 계획 종료 일자
	 */
	@Column(nullable = false)
	private LocalDate sunday;

	/**
	 * 주당 공수
	 */
	private Integer manDays;

	/**
	 * 착수
	 */
	@Column(precision = 3, scale = 1)
	private Float beginning;

	/**
	 * 분석
	 */
	@Column(precision = 3, scale = 1)
	private Float analysis;

	/**
	 * 설계
	 */
	@Column(precision = 3, scale = 1)
	private Float design;

	/**
	 * 개발
	 */
	@Column(precision = 3, scale = 1)
	private Float development;

	/**
	 * 테스트
	 */
	@Column(precision = 3, scale = 1)
	private Float test;

	/**
	 * 이행
	 */
	@Column(precision = 3, scale = 1)
	private Float execution;

	/**
	 * 교육
	 */
	@Column(precision = 3, scale = 1)
	private Float education;

	/**
	 * 기타
	 */
	@Column(precision = 3, scale = 1)
	private Float etc;

	/**
	 * 프로젝트 공수 실적
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<EffortResult> results;
}
