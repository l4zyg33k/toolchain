package com.gsitm.devops.pms.dto;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class SessionMultipartFile extends CommonsMultipartFile implements
		Serializable {

	private static final long serialVersionUID = 8411834722286594330L;

	private byte[] fileByte;

	public SessionMultipartFile(FileItem fileItem) {
		super((org.apache.commons.fileupload.FileItem) fileItem);
		this.fileByte = fileItem.get();
	}

	public SessionMultipartFile(CommonsMultipartFile commonsMultipartFile) {
		super(commonsMultipartFile.getFileItem());
	}

	public byte[] getFileByte() {
		return fileByte;
	}

	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}

	public void transferTo(FileOutputStream dest) {
		try {
			dest.write(this.fileByte);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
	}

}
