/**
 * 스티어링커미티 마스터 엔티티 정보
 * Committee.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Committee extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 회의 예정일
	 */
	private LocalDate openingDate;

	/**
	 * 실제 회의일
	 */
	private LocalDate actualOpeningDate;

	/**
	 * 회의 시간
	 */
	private LocalTime openingTime;

	/**
	 * 회의 장소 (코드)
	 */
	@ManyToOne
	private Code location;

	/**
	 * 회의 제목
	 */
	@Column(length = 32)
	private String title;

	/**
	 * 회의 안건
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<Agenda> agendas;

	/**
	 * 검토 여부
	 */
	private Boolean confirmed;
}
