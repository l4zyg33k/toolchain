package com.gsitm.devops.pms.model.vo;

import com.gsitm.devops.adm.model.Code;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjInspDetlFrmVO {

	private String oper;

	private Long id;

	private Code defectGrade;

	private Code defectType;

	private String content;

	public boolean isNew() {
		return null == getId();
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
