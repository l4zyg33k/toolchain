package com.gsitm.devops.pms.service.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.cmm.model.JqMap;
import com.gsitm.devops.cmm.model.QJqMap;
import com.gsitm.devops.pms.model.EffortResult;
import com.gsitm.devops.pms.model.QEffortPlan;
import com.gsitm.devops.pms.model.QEffortResult;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.model.QProjectMember;
import com.gsitm.devops.pms.model.vo.QTeamEfotGrdVO;
import com.gsitm.devops.pms.model.vo.TeamEfotGrdVO;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Getter
@Setter
@Service
public class TeamEfotBizImpl implements TeamEfotBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public List<JqMap> getProjects(String year, UserDetails user) {
		QProject project = QProject.project;

		// 수정 권한
		BooleanBuilder builder = new BooleanBuilder(
				project.code.startsWith(year));
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());

		if (!authorities.contains("ROLE_ADMIN")) {
			// 해당하는 프로젝트만 조회한다.
			builder.and(project.il.username.eq(user.getUsername()));
		}

		JPQLQuery query = new JPAQuery(em).from(project).where(builder)
				.orderBy(project.code.desc());

		return query.list(new QJqMap(project.code, project.code.prepend("[ ")
				.append(" ] ").append(project.name)));
	}

	@Override
	public Map<String, String> getYears(UserDetails user) {
		QProject project = QProject.project;

		// 수정 권한
		BooleanBuilder builder = new BooleanBuilder();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());

		if (!authorities.contains("ROLE_ADMIN")) {
			// 해당하는 프로젝트만 조회한다.
			builder.and(project.il.username.eq(user.getUsername()));
		}

		JPQLQuery query = new JPAQuery(em).from(project).where(builder)
				.orderBy(project.code.desc());

		return query.distinct().map(project.code.substring(0, 4),
				project.code.substring(0, 4).append("년도"));
	}

	@Override
	public Page<TeamEfotGrdVO> findAll(Pageable pageable, String project,
			String member, LocalDate fromDate, LocalDate toDate) {
		QProjectMember memb = QProjectMember.projectMember;
		QEffortPlan plan = QEffortPlan.effortPlan;
		QEffortResult result = QEffortResult.effortResult;

		PathBuilder<EffortResult> path = new PathBuilder<EffortResult>(
				EffortResult.class, "effortResult");

		JPQLQuery query = new JPAQuery(em)
				.from(memb)
				.innerJoin(memb.plans, plan)
				.innerJoin(plan.results, result)
				.where(memb.project.code.eq(project),
						memb.member.username.eq(member),
						result.workDate.between(fromDate, toDate));

		long total = query.uniqueResult(result.workDate.countDistinct());

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		List<TeamEfotGrdVO> results = query.groupBy(result.workDate).list(
				new QTeamEfotGrdVO(result.workDate.stringValue().append(
						new CaseBuilder()
								.when(result.workDate.dayOfWeek().eq(2))
								.then(" (월)")
								.when(result.workDate.dayOfWeek().eq(3))
								.then(" (화)")
								.when(result.workDate.dayOfWeek().eq(4))
								.then(" (수)")
								.when(result.workDate.dayOfWeek().eq(5))
								.then(" (목)")
								.when(result.workDate.dayOfWeek().eq(6))
								.then(" (금)")
								.when(result.workDate.dayOfWeek().eq(7))
								.then(" (토)").otherwise(" (일)")),
						result.beginning.sum(), result.analysis.sum(),
						result.design.sum(), result.development.sum(),
						result.test.sum(), result.execution.sum(),
						result.education.sum(), result.etc.sum()));

		SearchResults<TeamEfotGrdVO> search = new SearchResults<>(results,
				(long) pageable.getPageSize(), (long) pageable.getOffset(),
				total);

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public List<JqMap> getMembers(String project) {
		QProjectMember member = QProjectMember.projectMember;

		JPQLQuery query = new JPAQuery(em).from(member)
				.where(member.project.code.eq(project))
				.orderBy(member.member.name.asc());

		return query
				.list(new QJqMap(member.member.username, member.member.name));
	}
}
