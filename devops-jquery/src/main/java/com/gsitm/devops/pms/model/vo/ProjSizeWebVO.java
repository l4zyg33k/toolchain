package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjSizeWebVO extends SharedWebVO<Long> {
	
	private String rowData;

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/pms/projsize" : String.format("/pms/projsize/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/pms/projsize?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getDetailsUrl() {
		return String.format("/rest/pms/projsize/%d/details", getId());
	}

	public String getDetailsEditUrl() {
		return String.format("/rest/pms/projsize/%d/details/edit", getId());
	}	
}
