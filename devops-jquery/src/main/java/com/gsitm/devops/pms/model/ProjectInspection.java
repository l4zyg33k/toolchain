/**
 * 인스펙션 관리 마스터 엔티티 정보
 * ProjectInspection.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 프로젝트 인스펙션 엔티티
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectInspection extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 산출물 명칭
	 */
	@Column(length = 64)
	private String productName;

	/**
	 * 산출물 유형 (코드)
	 */
	@ManyToOne
	private Code productType;

	/**
	 * 산출물 단계 (코드)
	 */
	@ManyToOne
	private Code productPhase;

	/**
	 * 산출물 크기
	 */
	private Integer productSize;

	/**
	 * 작성자 이름
	 */
	@ManyToOne
	private Account producer;

	/**
	 * 검토 일자
	 */
	private LocalDate inspectionDate;

	/**
	 * 검토 인원 수
	 */
	private Integer inspectors;

	/**
	 * 진행자 이름
	 */
	@ManyToOne
	private Account moderator;

	/**
	 * 검토 분량
	 */
	private Integer inspectionPercent;

	/**
	 * 검토 미팅 시간
	 */
	private Integer inspectionMeetingTime;

	/**
	 * 검토 상세
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<ProjectInspectionDetail> details;
}
