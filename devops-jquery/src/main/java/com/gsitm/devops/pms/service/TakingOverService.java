package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.TakingOver;

public interface TakingOverService extends
		SharedService<TakingOver, Long> {

}
