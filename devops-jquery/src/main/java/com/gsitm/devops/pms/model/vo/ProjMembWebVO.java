package com.gsitm.devops.pms.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

@SuppressWarnings("serial")
public class ProjMembWebVO extends SharedWebVO<Long> {

	public ProjMembWebVO() {
		super();
	}

	public ProjMembWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/pms/projmemb" : String.format("/pms/projmemb/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/pms/projmemb?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
}
