package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

@Data
public class ProjMngExlVO implements Serializable {

	private static final long serialVersionUID = 923780018707472241L;

	private String code;

	private String name;

	private Long type;

	private String typeName;

	private String pmName;

	private String blName;

	private String blDepartment;

	private String il;

	private String ilName;

	private String op;

	private String opName;

	private String ou;

	private String qa;

	private String qaName;

	private Long office;

	private String officeName;

	private Date startDate;

	private Date finishDate;

	private Date actualStartDate;

	private Date actualFinishDate;

	private Long phase;

	private String phaseName;

	private Date panelStartDate;

	private Date panelFinishDate;

	private Date panelActualDate;

	private LocalTime panelActualTime;

	private Long location;

	private String locationName;

	private String recommandation;

	private Long pass;

	private String passName;

	private Date followUpStartDate;

	private Date followUpFinishDate;

	private Date followUpActualStartDate;

	private Date followUpActualFinishDate;

	private Date openActualDate;

	private Date openActualDate2;

	private Float ouEffort;

	private Float ouActualEffort;

	private Float pdEffort = 0F;

	private Float pdActualEffort;

	private Float processPoint;

	private Float testingPoint;

	private Float productPoint;

	private Float customerPoint;

	private Float workingHour = 9F;

	private Float workingDay = 22F;

	private Float workingEffort = 0F;

	private Date closeReportDate;

	private Long closeReportType;

	private String closeReportTypeName;

	private Date openCheckDate;

	private Date openReportDate;

	private Boolean watching = Boolean.FALSE;

	private Boolean satisfactionExpired = Boolean.FALSE;

	private Boolean privacy = Boolean.FALSE;

	private Boolean critical = Boolean.FALSE;

	private Long status;

	private String statusName;

	@QueryProjection
	public ProjMngExlVO(String code, String name, Long type, String typeName, String pmName, String blName,
			String blDepartment, String il, String ilName, String op, String opName, String ou, String qa,
			String qaName, Long office, String officeName, LocalDate startDate, LocalDate finishDate,
			LocalDate actualStartDate, LocalDate actualFinishDate, Long phase, String phaseName,
			LocalDate panelStartDate, LocalDate panelFinishDate, LocalDate panelActualDate, LocalTime panelActualTime,
			Long location, String locationName, String recommandation, Long pass, String passName,
			LocalDate followUpStartDate, LocalDate followUpFinishDate, LocalDate followUpActualStartDate,
			LocalDate followUpActualFinishDate, LocalDate openActualDate, LocalDate openActualDate2, Float ouEffort,
			Float ouActualEffort, Float pdEffort, Float pdActualEffort, Float processPoint, Float testingPoint,
			Float productPoint, Float customerPoint, Float workingHour, Float workingDay, Float workingEffort,
			LocalDate closeReportDate, Long closeReportType, String closeReportTypeName, LocalDate openCheckDate,
			LocalDate openReportDate, Boolean watching, Boolean satisfactionExpired, Boolean privacy, Boolean critical,
			Long status, String statusName) {
		super();
		this.code = code;
		this.name = name;
		this.type = type;
		this.typeName = typeName;
		this.pmName = pmName;
		this.blName = blName;
		this.blDepartment = blDepartment;
		this.il = il;
		this.ilName = ilName;
		this.op = op;
		this.opName = opName;
		this.ou = ou;
		this.qa = qa;
		this.qaName = qaName;
		this.office = office;
		this.officeName = officeName;
		this.startDate = startDate == null ? null : startDate.toDate();
		this.finishDate = finishDate == null ? null : finishDate.toDate();
		this.actualStartDate = actualStartDate == null ? null : actualStartDate.toDate();
		this.actualFinishDate = actualFinishDate == null ? null : actualFinishDate.toDate();
		this.phase = phase;
		this.phaseName = phaseName;
		this.panelStartDate = panelStartDate == null ? null : panelStartDate.toDate();
		this.panelFinishDate = panelFinishDate == null ? null : panelFinishDate.toDate();
		this.panelActualDate = panelActualDate == null ? null : panelActualDate.toDate();
		this.panelActualTime = panelActualTime;
		this.location = location;
		this.locationName = locationName;
		this.recommandation = recommandation;
		this.pass = pass;
		this.followUpStartDate = followUpStartDate == null ? null : followUpStartDate.toDate();
		this.followUpFinishDate = followUpFinishDate == null ? null : followUpFinishDate.toDate();
		this.followUpActualStartDate = followUpActualStartDate == null ? null : followUpActualStartDate.toDate();
		this.followUpActualFinishDate = followUpActualFinishDate == null ? null : followUpActualFinishDate.toDate();
		this.openActualDate = openActualDate == null ? null : openActualDate.toDate();
		this.openActualDate2 = openActualDate2 == null ? null : openActualDate2.toDate();
		this.ouEffort = ouEffort;
		this.ouActualEffort = ouActualEffort;
		this.pdEffort = pdEffort;
		this.pdActualEffort = pdActualEffort;
		this.processPoint = processPoint;
		this.testingPoint = testingPoint;
		this.productPoint = productPoint;
		this.customerPoint = customerPoint;
		this.workingHour = workingHour;
		this.workingDay = workingDay;
		this.workingEffort = workingEffort;
		this.closeReportDate = closeReportDate == null ? null : closeReportDate.toDate();
		this.closeReportType = closeReportType;
		this.closeReportTypeName = closeReportTypeName;
		this.openCheckDate = openCheckDate == null ? null : openCheckDate.toDate();
		this.openReportDate = openReportDate == null ? null : openReportDate.toDate();
		this.watching = watching;
		this.satisfactionExpired = satisfactionExpired;
		this.privacy = privacy;
		this.critical = critical;
		this.status = status;
		this.statusName = statusName;
	}
}
