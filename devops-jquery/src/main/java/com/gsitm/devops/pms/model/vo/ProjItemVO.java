package com.gsitm.devops.pms.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjItemVO {

	private String code;

	private String name;

	@QueryProjection
	public ProjItemVO(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
}
