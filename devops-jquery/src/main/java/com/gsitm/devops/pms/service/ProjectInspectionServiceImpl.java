package com.gsitm.devops.pms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.ProjectInspection;
import com.gsitm.devops.pms.repository.ProjectInspectionRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProjectInspectionServiceImpl implements ProjectInspectionService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ProjectInspectionRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public ProjectInspection findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProjectInspection findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectInspection> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProjectInspection> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectInspection> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectInspection> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectInspection> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectInspection> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProjectInspection save(ProjectInspection Inspection) {
		return repository.save(Inspection);
	}

	@Transactional
	@Override
	public <S extends ProjectInspection> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectInspection Inspection) {
		repository.delete(Inspection);
	}

	@Override
	public void delete(Iterable<? extends ProjectInspection> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	/*
	@Override
	public Page<ProjectInspectionList> findAllResult(Predicate predicate,
			Pageable pageable) {
		QProjectInspection qProjectInspection = QProjectInspection.projectInspection;
		QProjectInspectionDetail qDetail = QProjectInspectionDetail.projectInspectionDetail;

		List<ProjectInspectionList> result = new JPAQuery(entityManager)
				.from(qProjectInspection)
				.leftJoin(qProjectInspection.details, qDetail)
				.where(predicate)
				.groupBy(qProjectInspection.project,
						qProjectInspection.productPhase)
				.setHint("org.hibernate.cacheable", "true")
				.list(ConstructorExpression.create(ProjectInspectionList.class,
						qProjectInspection.project,
						qProjectInspection.productPhase, qDetail.id.count(),
						qDetail.defectGrade.name.eq("MA").count(),
						qDetail.defectGrade.name.eq("MI").count(),
						qProjectInspection.inspectors.sum(),
						qProjectInspection.inspectionMeetingTime.sum(),
						qProjectInspection.inspectionPercent.sum()));

		int total = result.size();

		return new PageImpl<>(result, pageable, total);
	}
	*/
}
