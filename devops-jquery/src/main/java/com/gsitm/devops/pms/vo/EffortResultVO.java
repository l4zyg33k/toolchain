package com.gsitm.devops.pms.vo;

import javax.validation.constraints.Digits;

import lombok.Data;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Data
public class EffortResultVO {

	/**
	 * 식별자
	 */
	private Long id;
	/**
	 * 작업일자
	 */
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate workDate;
	/**
	 * 작업자
	 */
	private Account worker;
	/**
	 * 유형
	 */
	private Code type;
	/**
	 * 착수
	 */
	@Digits(integer = 2, fraction = 1)
	private Float beginning = 0F;
	/**
	 * 분석
	 */
	@Digits(integer = 2, fraction = 1)
	private Float analysis = 0F;
	/**
	 * 설계
	 */
	@Digits(integer = 2, fraction = 1)
	private Float design = 0F;
	/**
	 * 개발
	 */
	@Digits(integer = 2, fraction = 1)
	private Float development = 0F;
	/**
	 * 테스트
	 */
	@Digits(integer = 2, fraction = 1)
	private Float test = 0F;
	/**
	 * 이행
	 */
	@Digits(integer = 2, fraction = 1)
	private Float execution = 0F;
	/**
	 * 교육
	 */
	@Digits(integer = 2, fraction = 1)
	private Float education = 0F;
	/**
	 * 기타
	 */
	@Digits(integer = 2, fraction = 1)
	private Float etc = 0F;
}
