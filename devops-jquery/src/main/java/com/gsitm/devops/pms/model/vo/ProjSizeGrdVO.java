package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjSizeGrdVO {

	private Long id;

	// 프로젝트
	@JsonProperty("project.name")
	private String projectName;

	// 프로젝트 코드
	@JsonProperty("project.code")
	private String project;

	// 단계 (코드)
	@JsonProperty("phase.name")
	private String phaseName;

	// Threshold
	private Integer threshold;

	// Contingency
	private Integer contingency;

	// 변경 권한
	private Boolean editable;

	@QueryProjection
	public ProjSizeGrdVO(Long id, String projectName, String project,
			String phaseName, Integer threshold, Integer contingency) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.project = project;
		this.phaseName = phaseName;
		this.threshold = threshold;
		this.contingency = contingency;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public ProjSizeGrdVO(Long id, String projectName, String project,
			String phaseName, Integer threshold, Integer contingency,
			Boolean editable) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.project = project;
		this.phaseName = phaseName;
		this.threshold = threshold;
		this.contingency = contingency;
		this.editable = editable;
	}
}
