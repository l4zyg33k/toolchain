package com.gsitm.devops.pms.service.biz;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.JqMap;
import com.gsitm.devops.pms.model.vo.TeamEfotGrdVO;

public interface TeamEfotBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public List<JqMap> getProjects(String year, UserDetails user);

	public Map<String, String> getYears(UserDetails user);

	public Page<TeamEfotGrdVO> findAll(Pageable pageable, String project,
			String member, LocalDate fromDate, LocalDate toDate);

	public List<JqMap> getMembers(String project);
}
