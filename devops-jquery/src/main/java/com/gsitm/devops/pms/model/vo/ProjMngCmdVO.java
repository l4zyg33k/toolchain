package com.gsitm.devops.pms.model.vo;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjMngCmdVO extends ProjMngWebVO {

	private String code;

	@NotBlank
	private String name;

	@NotNull
	private Code type;

	@NotBlank
	private String pmName;

	@NotBlank
	private String blName;

	@NotBlank
	private String blDepartment;

	private Account il;

	@NotBlank
	private String ilName;

	private Account op;

	@NotBlank
	private String opName;

	private Account ou;

	private Account qa;

	@NotBlank
	private String qaName;

	@NotNull
	private Code office;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate finishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualFinishDate;

	private Code phase;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelFinishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate panelActualDate;

	@DateTimeFormat(pattern = "a h:mm")
	private LocalTime panelActualTime;

	private Code location;

	private String recommandation;

	private Code pass;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpFinishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpActualStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate followUpActualFinishDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openActualDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openActualDate2;

	private Float ouEffort;

	private Float ouActualEffort;

	@DecimalMin(value = "0.1")
	private Float pdEffort;

	private Float pdActualEffort;

	private Float processPoint;

	private Float testingPoint;

	private Float productPoint;

	private Float customerPoint;

	private Float workingHour;

	private Float workingDay;

	private Float workingEffort;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate closeReportDate;

	private Code closeReportType;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openCheckDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openReportDate;

	private Boolean watching;

	private Boolean satisfactionExpired;

	private Boolean privacy;

	private Boolean critical;

	private Code status;
}
