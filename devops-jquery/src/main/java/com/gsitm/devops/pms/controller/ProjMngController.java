package com.gsitm.devops.pms.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjMngCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMngExlVO;
import com.gsitm.devops.pms.model.vo.ProjMngFrmVO;
import com.gsitm.devops.pms.service.biz.ProjMngBiz;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class ProjMngController {

	private static final String MENU_CODE = "ProjMng";
	private static final String FORM_VO = "projMngFrmVO";
	private static final String CODE = "code";

	private static final String INDEX_URL = "/pms/projmng";
	private static final String ACTION_URL = "/pms/projmng/*";
	private static final String FORM_URL = "/pms/projmng/form";
	private static final String FORM_URL_BY_CODE = "/pms/projmng/{code}/form";
	private static final String VIEW_URL_BY_CODE = "/pms/projmng/{code}/view";
	private static final String REDIRECT_URL = "redirect:/pms/projmng?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "pms/projmng/index";
	private static final String FORM_PAGE = "pms/projmng/form";
	private static final String VIEW_PAGE = "pms/projmng/view";

	@Autowired
	private ProjMngBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute("offices")
	public Map<Long, String> getOffices() {
		return biz.getOffices();
	}

	@ModelAttribute("types")
	public Map<Long, String> getTypes() {
		return biz.getTypes();
	}

	@ModelAttribute("locations")
	public Map<Long, String> getLocations() {
		return biz.getLocations();
	}

	@ModelAttribute("passes")
	public Map<Long, String> getPasses() {
		return biz.getPasses();
	}

	@ModelAttribute("closeReportTypes")
	public Map<Long, String> getCloseReportTypes() {
		return biz.getCloseReportTypes();
	}

	@ModelAttribute("phases")
	public Map<Long, String> getPhases() {
		return biz.getPasses();
	}

	@ModelAttribute("statuses")
	public Map<Long, String> getStatuses() {
		return biz.getStatuses();
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = { PageParams.PAGE, PageParams.ROW_NUM,
			PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = { PageParams.PAGE, PageParams.ROW_NUM,
			PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		ProjMngFrmVO vo = new ProjMngFrmVO(page, rowNum, rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(@ModelAttribute(FORM_VO) @Valid ProjMngCmdVO vo, BindingResult result,
			RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = { PageParams.PAGE,
			PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, @PathVariable(CODE) String code, Model model) {
		ProjMngFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid ProjMngCmdVO vo, BindingResult result,
			RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = VIEW_URL_BY_CODE, method = RequestMethod.GET, params = { PageParams.PAGE,
			PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page, @RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, @PathVariable(CODE) String code, Model model) {
		ProjMngFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return VIEW_PAGE;
	}

	@RequestMapping(value = "/jasper/pms/projmng/export", method = RequestMethod.GET)
	public String report1(Model model, @RequestParam("format") String format, @RequestParam("page") int page,
			@RequestParam("rowNum") int rowNum, @RequestParam("sortname") String sortname,
			@RequestParam("sortorder") String sortorder, @RequestParam("_search") boolean search,
			@RequestParam(value = "field", required = false) String field,
			@RequestParam(value = "oper", required = false) String oper,
			@RequestParam(value = "string", required = false) String string) {

		Pageable pageable = new PageRequest(page - 1, 50, Direction.fromStringOrNull(sortorder), sortname);
		List<ProjMngExlVO> datasource;

		if (search) {
			Searchable searchable = new Searchable(field, oper, string);
			datasource = biz.findAllForExcel(pageable, searchable).getContent();
		} else {
			datasource = biz.findAllForExcel(pageable).getContent();
		}

		model.addAttribute("format", format);
		model.addAttribute("datasource", new JRBeanCollectionDataSource(datasource));
		model.addAttribute(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		return "WEB-INF/jasper/pms/projmng/projlist.jasper";
	}
}
