package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.ProjectSizeDetail;

public interface ProjectSizeDetailService extends
		SharedService<ProjectSizeDetail, Long> {

}
