package com.gsitm.devops.pms.dto;

import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

/**
 *
 */
@Data
public class MemberWeeklyEffortSummary {

	/**
	 * 식별자
	 */
	private Long id;
	/**
	 * 프로젝트 코드
	 */
	private String code;
	/**
	 * 프로젝트 명칭
	 */
	private String name;
	/**
	 * 공수 기간
	 */
	private String week;
	/**
	 * 공수 계획
	 */
	private Float manHours;
	/**
	 * 착수
	 */
	private Float phase1;
	/**
	 * 분석
	 */
	private Float phase2;
	/**
	 * 설계
	 */
	private Float phase3;
	/**
	 * 개발
	 */
	private Float phase4;
	/**
	 * 테스트
	 */
	private Float phase5;
	/**
	 * 이행
	 */
	private Float phase6;
	/**
	 * 교육
	 */
	private Float phase7;
	/**
	 * 기타
	 */
	private Float phase8;
	/**
	 * 합계
	 */
	private Float summary;

	@QueryProjection
	public MemberWeeklyEffortSummary(Long id, String code, String name,
			String week, Float manHours, Float phase1, Float phase2,
			Float phase3, Float phase4, Float phase5, Float phase6,
			Float phase7, Float phase8, Float summary) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.week = week;
		this.manHours = manHours;
		this.phase1 = phase1;
		this.phase2 = phase2;
		this.phase3 = phase3;
		this.phase4 = phase4;
		this.phase5 = phase5;
		this.phase6 = phase6;
		this.phase7 = phase7;
		this.phase8 = phase8;
		this.summary = summary;
	}
}
