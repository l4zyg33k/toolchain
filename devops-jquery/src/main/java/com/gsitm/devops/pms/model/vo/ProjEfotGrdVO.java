package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

/**
 * 기간별 공수 그리드
 * 
 * @author lazygeek
 *
 */
@Getter
@Setter
public class ProjEfotGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 식별자
	 */
	private Long id;

	/**
	 * 공수 기간
	 */
	private String week;

	/**
	 * 공수 계획
	 */
	private Float manHours;

	/**
	 * 착수
	 */
	private Float beginning;

	/**
	 * 분석
	 */
	private Float analysis;

	/**
	 * 설계
	 */
	private Float design;

	/**
	 * 개발
	 */
	private Float development;

	/**
	 * 테스트
	 */
	private Float test;

	/**
	 * 이행
	 */
	private Float execution;

	/**
	 * 교육
	 */
	private Float education;

	/**
	 * 기타
	 */
	private Float etc;

	/**
	 * 합계
	 */
	private Float total;

	@QueryProjection
	public ProjEfotGrdVO(Long id, String week, Float manHours, Float beginning,
			Float analysis, Float design, Float development, Float test,
			Float execution, Float education, Float etc, Float total) {
		super();
		this.id = id;
		this.week = week;
		this.manHours = manHours;
		this.beginning = beginning;
		this.analysis = analysis;
		this.design = design;
		this.development = development;
		this.test = test;
		this.execution = execution;
		this.education = education;
		this.etc = etc;
		this.total = total;
	}
}
