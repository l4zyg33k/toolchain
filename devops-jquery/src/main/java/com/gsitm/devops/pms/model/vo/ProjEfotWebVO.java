package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

/*
 * 프로젝트 공수 웹 VO
 */
@Getter
@Setter
public class ProjEfotWebVO extends SharedWebVO<Long> {

	private static final long serialVersionUID = 1L;

	public ProjEfotWebVO() {
		super();
	}

	public ProjEfotWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getPrevUrl() {
		return String.format("/pms/projefot?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
}
