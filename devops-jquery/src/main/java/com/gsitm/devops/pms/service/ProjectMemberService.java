package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.ProjectMember;

public interface ProjectMemberService extends
		SharedService<ProjectMember, Long> {
	
	public void createEffortPlans(ProjectMember member);
	public void updateEffortPlans(ProjectMember member);	
}
