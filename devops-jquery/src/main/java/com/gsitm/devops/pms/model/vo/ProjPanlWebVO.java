package com.gsitm.devops.pms.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

@SuppressWarnings("serial")
public class ProjPanlWebVO extends SharedWebVO<String> {

	public String getPrevUrl() {
		return String.format("/pms/projpanl?page=%d&rowNum=%d&rowId=%s",
				getPage(), getRowNum(), getRowId());
	}
}
