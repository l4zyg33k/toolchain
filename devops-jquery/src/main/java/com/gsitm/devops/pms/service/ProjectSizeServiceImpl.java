package com.gsitm.devops.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.ProjectSize;
import com.gsitm.devops.pms.repository.ProjectSizeRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProjectSizeServiceImpl implements ProjectSizeService {

	@Autowired
	private ProjectSizeRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public ProjectSize findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProjectSize findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectSize> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProjectSize> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectSize> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectSize> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectSize> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectSize> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProjectSize save(ProjectSize ProjectSize) {
		return repository.save(ProjectSize);
	}

	@Transactional
	@Override
	public <S extends ProjectSize> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectSize ProjectSize) {
		repository.delete(ProjectSize);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProjectSize> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
