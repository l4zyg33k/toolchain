package com.gsitm.devops.pms.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Weeks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.pms.model.EffortPlan;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.ProjectMember;
import com.gsitm.devops.pms.model.QEffortPlan;
import com.gsitm.devops.pms.model.QProjectMember;
import com.gsitm.devops.pms.repository.ProjectMemberRepository;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 프로젝트 Inspection 상세 서비스
 */
@Service
@Transactional(readOnly = true)
public class ProjectMemberServiceImpl implements ProjectMemberService {

	/**
	 * 프로젝트 Inspection 상세 리파지토리
	 */
	@Autowired
	private ProjectMemberRepository repository;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private EffortPlanService epService;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public ProjectMember findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public ProjectMember findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectMember> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<ProjectMember> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectMember> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectMember> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectMember> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectMember> findAll(Predicate predicate, Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProjectMember save(ProjectMember projectMember) {

		return repository.save(projectMember);
	}

	@Transactional
	@Override
	public <S extends ProjectMember> List<S> save(Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectMember projectMember) {

		repository.delete(projectMember);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProjectMember> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

	@Transactional
	@Async
	@Override
	public void createEffortPlans(ProjectMember member) {
		if (member.getProject().getName().contains("주간업무")) {
			return;
		}
		
		List<EffortPlan> result = new ArrayList<>();
		LocalDate startDate = member.getStartDate();
		LocalDate finishDate = member.getEndDate();

		// 프로젝트 시작, 종료의 주기
		Interval planned = new Interval(startDate.toDateTimeAtStartOfDay(),
				finishDate.plusDays(1).toDateTimeAtStartOfDay());

		Assert.notNull(planned, "시작일과 종료일로 기간 계산이 불가합니다.");

		// 프로젝트 시작, 종료의 기간
		Weeks involved = Weeks.weeksBetween(
				startDate.withDayOfWeek(DateTimeConstants.MONDAY),
				finishDate.withDayOfWeek(DateTimeConstants.SUNDAY));

		Assert.notNull(involved, "시작일과 종료일로 기간 계산이 불가합니다.");

		// 기간으로 몇 주인지 계산
		int weeks = involved.getWeeks();

		for (int w = 0; w <= weeks; w++) {
			LocalDate dDay = startDate.plusWeeks(w);
			LocalDate monday = dDay.withDayOfWeek(DateTimeConstants.MONDAY);
			LocalDate friday = dDay.withDayOfWeek(DateTimeConstants.FRIDAY);
			LocalDate sunday = dDay.withDayOfWeek(DateTimeConstants.SUNDAY);

			// 근무일 시작, 종료의 주기
			Interval working = new Interval(monday.toDateTimeAtStartOfDay(),
					friday.plusDays(1).toDateTimeAtStartOfDay());

			// 겹치는 주기를 얻는다
			boolean isOverlapped = planned.overlaps(working);
			Interval overlapped = planned.overlap(working);

			// 기간으로 환산하여 몇 일인지 계산
			int days = isOverlapped ? overlapped.toPeriod().getDays() : 0;

			EffortPlan plan = new EffortPlan();
			plan.setMonday(monday);
			plan.setSunday(sunday);
			plan.setManDays(days);
			plan.setBeginning(0F);
			plan.setAnalysis(0F);
			plan.setDesign(0F);
			plan.setDevelopment(0F);
			plan.setTest(0F);
			plan.setExecution(0F);
			plan.setEducation(0F);
			plan.setEtc(0F);
			plan = epService.save(plan);

			result.add(plan);
		}

		member.setPlans(result);
		save(member);
	}

	@Transactional
	@Async
	@Override
	public void updateEffortPlans(ProjectMember member) {
		if (member.getProject().getName().contains("주간업무")) {
			return;
		}

		List<EffortPlan> plans = member.getPlans();
		Project project = member.getProject();
		Account account = member.getMember();
		LocalDate startDate = member.getStartDate();
		LocalDate finishDate = member.getEndDate();

		// 프로젝트 시작, 종료의 주기
		Interval planned = new Interval(startDate.toDateTimeAtStartOfDay(),
				finishDate.plusDays(1).toDateTimeAtStartOfDay());

		Assert.notNull(planned, "시작일과 종료일로 기간 계산이 불가합니다.");

		// 프로젝트 시작, 종료의 기간
		Weeks involved = Weeks.weeksBetween(
				startDate.withDayOfWeek(DateTimeConstants.MONDAY),
				finishDate.withDayOfWeek(DateTimeConstants.SUNDAY));

		Assert.notNull(involved, "시작일과 종료일로 기간 계산이 불가합니다.");

		// 기간으로 몇 주인지 계산
		int weeks = involved.getWeeks();

		// 기존 공수 계획 재계산
		for (EffortPlan plan : plans) {
			LocalDate monday = plan.getMonday();
			LocalDate friday = monday.withDayOfWeek(DateTimeConstants.FRIDAY);

			// 근무일 시작, 종료의 주기
			Interval working = new Interval(monday.toDateTimeAtStartOfDay(),
					friday.plusDays(1).toDateTimeAtStartOfDay());

			// 겹치는 주기를 얻는다
			boolean isOverlapped = planned.overlaps(working);
			Interval overlapped = planned.overlap(working);

			// 기간으로 환산하여 몇 일인지 계산
			int days = isOverlapped ? overlapped.toPeriod().getDays() : 0;

			plan.setManDays(days);
			epService.save(plan);
		}

		plans = member.getPlans();

		// 주별로 공수 계획이 없는 경우 생성
		for (int w = 0; w <= weeks; w++) {
			LocalDate dDay = startDate.plusWeeks(w);
			LocalDate monday = dDay.withDayOfWeek(DateTimeConstants.MONDAY);
			LocalDate friday = dDay.withDayOfWeek(DateTimeConstants.FRIDAY);
			LocalDate sunday = dDay.withDayOfWeek(DateTimeConstants.SUNDAY);

			// 근무일 시작, 종료의 주기
			Interval working = new Interval(monday.toDateTimeAtStartOfDay(),
					friday.plusDays(1).toDateTimeAtStartOfDay());

			// 겹치는 주기를 얻는다
			boolean isOverlapped = planned.overlaps(working);
			Interval overlapped = planned.overlap(working);

			// 기간으로 환산하여 몇 일인지 계산
			int days = isOverlapped ? overlapped.toPeriod().getDays() : 0;

			QProjectMember pm = QProjectMember.projectMember;
			QEffortPlan ep = QEffortPlan.effortPlan;
			JPQLQuery query = new JPAQuery(em)
					.from(pm)
					.innerJoin(pm.plans, ep)
					.where(pm.project.eq(project), pm.member.eq(account),
							ep.monday.eq(monday), ep.sunday.eq(sunday));
			long cnt = query.count();

			if (cnt == 0) {
				EffortPlan plan = new EffortPlan();
				plan.setMonday(monday);
				plan.setSunday(sunday);
				plan.setManDays(days);
				plan.setBeginning(0F);
				plan.setAnalysis(0F);
				plan.setDesign(0F);
				plan.setDevelopment(0F);
				plan.setTest(0F);
				plan.setExecution(0F);
				plan.setEducation(0F);
				plan.setEtc(0F);
				plan = epService.save(plan);

				plans.add(plan);
				save(member);
			}
		}
	}
}
