package com.gsitm.devops.pms.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.dms.model.DocumentRevision;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class TakingOverDetail extends AbstractAuditable<Account, Long> {

	@ManyToOne
	private TakingOver takingOver;

	@Transient
	private CommonsMultipartFile file;

	@OneToOne
	private DocumentRevision revision;
}
