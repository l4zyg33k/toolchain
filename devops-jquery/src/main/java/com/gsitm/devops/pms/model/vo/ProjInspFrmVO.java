package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjInspFrmVO extends ProjInspWebVO {

	private String project;

	private String projectName;

	private String productName;

	private Long productType;

	private String productTypeName;

	private Long productPhase;

	private String productPhaseName;

	private Integer productSize;

	private String producer;

	private String producerName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate inspectionDate;

	private Integer inspectors;

	private String moderator;

	private String moderatorName;

	private Integer inspectionPercent;

	private Integer inspectionMeetingTime;

	public ProjInspFrmVO(int page, int rowNum, long rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}

	@QueryProjection
	public ProjInspFrmVO(Long id, String project, String projectName,
			String productName, Long productType, String productTypeName,
			Long productPhase, String productPhaseName, Integer productSize,
			String producer, String producerName, LocalDate inspectionDate,
			Integer inspectors, String moderator, String moderatorName,
			Integer inspectionPercent, Integer inspectionMeetingTime) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.productName = productName;
		this.productType = productType;
		this.productTypeName = productTypeName;
		this.productPhase = productPhase;
		this.productPhaseName = productPhaseName;
		this.productSize = productSize;
		this.producer = producer;
		this.producerName = producerName;
		this.inspectionDate = inspectionDate;
		this.inspectors = inspectors;
		this.moderator = moderator;
		this.moderatorName = moderatorName;
		this.inspectionPercent = inspectionPercent;
		this.inspectionMeetingTime = inspectionMeetingTime;
	}
}
