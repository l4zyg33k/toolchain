package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.Committee;

public interface CommitteeService extends SharedService<Committee, Long> {
}
