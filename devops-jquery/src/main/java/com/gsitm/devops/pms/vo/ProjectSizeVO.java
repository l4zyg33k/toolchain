/**
 * 프로젝트 size등록 엔티티 ProjectSize.java
 *
 * @author Administrator
 */
package com.gsitm.devops.pms.vo;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Data;

import com.gsitm.devops.adm.model.Code;

@SuppressWarnings("serial")
@Data
public class ProjectSizeVO implements Serializable {

	/**
	 * 식별자
	 */
	private Long id;
	/**
	 * 프로젝트
	 */
	@Valid
	private ProjectVO project = new ProjectVO();
	/**
	 * 프로젝트 단계
	 */
	@Valid
	private Code phase = new Code();
	/**
	 * Threshold
	 */
	@NotNull
	private Integer threshold;
	/**
	 * Contingency
	 */
	@NotNull
	private Integer contingency;
}
