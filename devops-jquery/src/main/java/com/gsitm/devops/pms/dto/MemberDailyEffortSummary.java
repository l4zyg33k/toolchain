package com.gsitm.devops.pms.dto;

import org.joda.time.LocalDate;

import com.mysema.query.annotations.QueryProjection;

import lombok.Data;

/**
 * 팀원 일별 공수 합계
 */
@Data
public class MemberDailyEffortSummary {

	/**
	 * 작업 일자
	 */
	private LocalDate workDate;
	/**
	 * 착수
	 */
	private Float phase1;
	/**
	 * 분석
	 */
	private Float phase2;
	/**
	 * 설계
	 */
	private Float phase3;
	/**
	 * 개발
	 */
	private Float phase4;
	/**
	 * 테스트
	 */
	private Float phase5;
	/**
	 * 이행
	 */
	private Float phase6;
	/**
	 * 교육
	 */
	private Float phase7;
	/**
	 * 기타
	 */
	private Float phase8;
	/**
	 * 합계
	 */
	private Float summary;

	@QueryProjection
	public MemberDailyEffortSummary(LocalDate workDate, Float phase1,
			Float phase2, Float phase3, Float phase4, Float phase5,
			Float phase6, Float phase7, Float phase8, Float summary) {
		super();
		this.workDate = workDate;
		this.phase1 = phase1;
		this.phase2 = phase2;
		this.phase3 = phase3;
		this.phase4 = phase4;
		this.phase5 = phase5;
		this.phase6 = phase6;
		this.phase7 = phase7;
		this.phase8 = phase8;
		this.summary = summary;
	}
}
