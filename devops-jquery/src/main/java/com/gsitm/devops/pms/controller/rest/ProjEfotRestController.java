package com.gsitm.devops.pms.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.pms.model.vo.ProjEfotDetlCmdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjEfotGrdVO;
import com.gsitm.devops.pms.service.biz.ProjEfotBiz;

/**
 * 프로젝트 공수 레스트 컨트롤러
 * 
 * @author lazygeek
 *
 */
@RestController
public class ProjEfotRestController {

	@Autowired
	private ProjEfotBiz biz;

	@RequestMapping(value = "/rest/pms/projefot", method = RequestMethod.POST)
	public Page<ProjEfotGrdVO> findAll(Pageable pageable,
			@RequestParam("project") String project,
			@AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, project, user);
	}

	@RequestMapping(value = "/rest/pms/projefot/result", method = RequestMethod.POST)
	public Page<ProjEfotDetlGrdVO> findAll(Pageable pageable,
			@RequestParam("plan") Long plan, @RequestParam("type") Long type) {
		return biz.findAll(pageable, plan, type);
	}

	@RequestMapping(value = "/rest/pms/projefot/result/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjEfotDetlCmdVO projEfotDetlCmdVO,
			BindingResult result) {

		// 추가, 삭제인 경우 유효성 검사를 하지 않는다.
		if (projEfotDetlCmdVO.isNew() == false
				&& projEfotDetlCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projEfotDetlCmdVO.getOper()) {
		case "add":
			try {
				biz.create(projEfotDetlCmdVO);
			} catch (IllegalArgumentException e) {
				return new JqFailure(e.getMessage());
			}
			break;
		case "edit":
			biz.update(projEfotDetlCmdVO);
			break;
		case "del":
			biz.delete(projEfotDetlCmdVO);
			break;
		}

		return new JqSuccess();
	}
}
