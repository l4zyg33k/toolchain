package com.gsitm.devops.pms.model.vo;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Code;

/**
 * 프로젝트 공수 커맨드
 * 
 * @author lazygeek
 *
 */
@Getter
@Setter
public class ProjEfotDetlCmdVO extends ProjEfotWebVO {

	private static final long serialVersionUID = 1L;

	@NotNull
	private Long plan;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate workDate;

	@NotNull
	private Code type;

	private Float beginning = 0F;

	private Float analysis = 0F;

	private Float design = 0F;

	private Float development = 0F;

	private Float test = 0F;

	private Float execution = 0F;

	private Float education = 0F;

	private Float etc = 0F;
}
