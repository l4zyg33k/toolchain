package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class TakingOver extends AbstractAuditable<Account, Long> {

	@ManyToOne
	private Project project;

	@ManyToOne
	private Account uploader;

	@Column(length = 8)
	private String division;

	@ManyToOne
	private com.gsitm.devops.dms.model.System system;

	@OneToMany
	private List<TakingOverDetail> takingOverDetails;
}
