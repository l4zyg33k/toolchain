package com.gsitm.devops.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.Committee;
import com.gsitm.devops.pms.repository.CommitteeRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class CommitteeServiceImpl implements CommitteeService {

	@Autowired
	private CommitteeRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Committee findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Committee findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Committee> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Committee> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Committee> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Committee> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Committee> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Committee> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Committee save(Committee committee) {
		return repository.save(committee);
	}

	@Transactional
	@Override
	public <S extends Committee> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Committee committee) {
		repository.delete(committee);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Committee> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

}
