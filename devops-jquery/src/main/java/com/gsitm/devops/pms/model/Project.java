/**
 * 프로젝트 엔티티 정보
 * Project.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Auditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;
import com.gsitm.devops.qam.model.ProgressEvaluation;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@Audited
@SuppressWarnings("serial")
public class Project implements Auditable<Account, String> {

	/**
	 * 코드
	 */
	@Id
	@Column(length = 7)
	private String code;

	/**
	 * 이름
	 */
	@NotBlank
	@Column(length = 48)
	private String name;

	/**
	 * 프로젝트 유형 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * 프로젝트 관리자
	 */
	@Column(length = 16)
	private String pmName;

	/**
	 * 비지니스 리더
	 */
	@Column(length = 16)
	private String blName;

	/**
	 * 추진 부서
	 */
	@Column(length = 16)
	private String blDepartment;

	/**
	 * 정보기술 리더
	 */
	@ManyToOne
	private Account il;

	/**
	 * 운영 담당자
	 */
	@ManyToOne
	private Account op;

	/**
     *
     */
	@ManyToOne
	private Account ou;

	/**
	 * 품질 담당자
	 */
	@ManyToOne
	private Account qa;

	/**
	 * 추진 사업부 (코드)
	 */
	@ManyToOne
	private Code office;

	/**
	 * 시작 일자
	 */
	private LocalDate startDate;

	/**
	 * 종료 일자
	 */
	private LocalDate finishDate;

	/**
	 * 실제 시작 일자
	 */
	private LocalDate actualStartDate;

	/**
	 * 실제 종료 일자
	 */
	private LocalDate actualFinishDate;

	/**
	 * 진행 단계 (코드)
	 */
	@ManyToOne
	private Code phase;

	/**
	 * 준비 시작 일자
	 */
	private LocalDate panelStartDate;

	/**
	 * 준비 종료 일자
	 */
	private LocalDate panelFinishDate;

	/**
	 * 회의 일자
	 */
	private LocalDate panelActualDate;

	/**
	 * 회의 시각
	 */
	private LocalTime panelActualTime;

	/**
	 * 회의 장소 (코드)
	 */
	@ManyToOne
	private Code location;

	/**
	 * 권고 사항
	 */
	@Column(length = 960)
	private String recommandation;

	/**
	 * 통과 여부 (코드)
	 */
	@ManyToOne
	private Code pass;

	/**
	 * 인수인계 시작일자
	 */
	private LocalDate followUpStartDate;

	/**
	 * 인수인계 종료일자
	 */
	private LocalDate followUpFinishDate;

	/**
	 * 실제 인수인계 시작일자
	 */
	private LocalDate followUpActualStartDate;

	/**
	 * 실제 인수인계 종료일자
	 */
	private LocalDate followUpActualFinishDate;
	
	/**
	 * 실제 오픈일자
	 */
	private LocalDate openActualDate;
	
	/**
	 * 실제 오픈일자
	 */
	private LocalDate openActualDate2;

	/**
     *
     */
	@Column(precision = 7, scale = 3)
	private Float ouEffort = 0F;

	/**
     *
     */
	@Column(precision = 7, scale = 3)
	private Float ouActualEffort = 0F;

	/**
	 * 총 M/M
	 */
	@Column(precision = 7, scale = 3)
	private Float pdEffort = 0F;

	/**
	 * 실제 총 M/M
	 */
	@Column(precision = 7, scale = 3)
	private Float pdActualEffort = 0F;

	/**
	 * 프로세스 평가 점수
	 */
	@Column(precision = 6, scale = 3)
	private Float processPoint = 0F;

	/**
	 * 테스트 평가 점수
	 */
	@Column(precision = 6, scale = 3)
	private Float testingPoint = 0F;

	/**
	 * 산출물 평가 점수
	 */
	@Column(precision = 6, scale = 3)
	private Float productPoint = 0F;

	/**
	 * 고객 만족도 평가 점수
	 */
	@Column(precision = 6, scale = 3)
	private Float customerPoint = 0F;

	/**
	 * 1일 근무 기준 시간
	 */
	@Column(precision = 3, scale = 1)
	private Float workingHour = 9F;

	/**
	 * 1달 근무 기준 일수
	 */
	@Column(precision = 3, scale = 1)
	private Float workingDay = 22F;

	/**
	 * 총 M/H
	 */
	@Column(precision = 7, scale = 2)
	private Float workingEffort = 0F;

	/**
	 * 종료 보고 일자
	 */
	private LocalDate closeReportDate;

	/**
	 * 종료 보고 형식 (코드)
	 */
	@ManyToOne
	private Code closeReportType;
	
	/**
	 * 진행 단계 (코드)
	 */
	@ManyToOne
	private Code status;	

	/**
	 * 오픈 점검 일자
	 */
	private LocalDate openCheckDate;

	/**
	 * 운영 보고 일자
	 */
	private LocalDate openReportDate;

	/**
	 * 현황판 표시
	 */
	private Boolean watching = Boolean.FALSE;

	/**
	 * 설문조사 기간 만료 여부
	 */
	private Boolean satisfactionExpired = Boolean.FALSE;

	/**
	 * 스티어링 커미티
	 */
	@NotAudited
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<Committee> committees;

	/**
	 * 프로젝트 주간업무
	 */
	@NotAudited
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<ProjectWeeklyReport> projectWeeklyReport;

	/**
	 * 진척도 평가
	 */
	@NotAudited
	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<ProgressEvaluation> progressEvaluations;
	
	/**
	 * 개인정보 취급
	 */
	private Boolean privacy;
	
	/**
	 * 핵심 업무
	 */
	private Boolean critical;
	
	/**
	 * 첨부 파일
	 */
	@NotAudited
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<AttachmentFile> attachments;	

	/**
	 * 등록 아이디
	 */
	@ManyToOne
	@CreatedBy
	private Account createdBy;

	/**
	 * 등록 일시
	 */
	@CreatedDate
	private DateTime createdDate;

	/**
	 * 수정 아이디
	 */
	@ManyToOne
	@LastModifiedBy
	private Account lastModifiedBy;

	/**
	 * 수정 일시
	 */
	@LastModifiedDate
	private DateTime lastModifiedDate;

	@Override
	public String getId() {
		return getCode();
	}

	@Override
	public boolean isNew() {
		return null == getId();
	}

	@Override
	public String toString() {
		return getCode();
	}
}
