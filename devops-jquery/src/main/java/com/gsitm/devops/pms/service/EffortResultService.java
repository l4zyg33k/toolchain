package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.EffortResult;

public interface EffortResultService extends
		SharedService<EffortResult, Long> {

}
