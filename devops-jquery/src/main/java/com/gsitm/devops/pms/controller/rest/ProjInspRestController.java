package com.gsitm.devops.pms.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjInspCmdVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjInspGrdVO;
import com.gsitm.devops.pms.service.biz.ProjInspBiz;

@RestController
public class ProjInspRestController {

	@Autowired
	private ProjInspBiz biz;

	@RequestMapping(value = "/rest/pms/projinsp", method = RequestMethod.POST)
	public Page<ProjInspGrdVO> findAll(Pageable pageable,
			@AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, user);
	}

	@RequestMapping(value = "/rest/pms/projinsp", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjInspGrdVO> findAll(Pageable pageable,
			Searchable searchable, @AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, searchable, user);
	}

	@RequestMapping(value = "/rest/pms/projinsp/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjInspCmdVO projInspCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (projInspCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projInspCmdVO.getOper()) {
		case "del":
			biz.delete(projInspCmdVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/projinsp/{id}/details", method = RequestMethod.POST)
	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable,
			@PathVariable("id") Long id) {
		return biz.findAll(pageable, id);
	}

	@RequestMapping(value = "/rest/pms/projinsp/{id}/details", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, @PathVariable("id") Long id) {
		return biz.findAll(pageable, searchable, id);
	}

	@RequestMapping(value = "/rest/pms/projinsp/{id}/details/edit", method = RequestMethod.POST)
	public JqResult formEditing(@PathVariable("id") Long id,
			@Valid ProjInspDetlFrmVO projInspDetlFrmVO, BindingResult result) {

		// 추가, 삭제인 경우 유효성 검사를 하지 않는다.
		if (projInspDetlFrmVO.isNew() == false
				&& projInspDetlFrmVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projInspDetlFrmVO.getOper()) {
		case "add":
			biz.create(projInspDetlFrmVO, id);
			break;
		case "edit":
			biz.update(projInspDetlFrmVO);
			break;
		case "del":
			biz.delete(projInspDetlFrmVO, id);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/pms/projinsp/{id}/defecttypes", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String getDefectTypes(@PathVariable("id") Long id) {
		return biz.getDefectTypes(id);
	}

	@RequestMapping(value = "/rest/pms/projinsp/projweekinvt", method = RequestMethod.POST)
	public Page<ProjInspGrdVO> findAll(Pageable pageable,
			@RequestParam("project") String projectCode) {
		return biz.findAll(pageable, projectCode);
	}
}
