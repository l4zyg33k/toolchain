package com.gsitm.devops.pms.service.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Agenda;
import com.gsitm.devops.pms.model.Committee;
import com.gsitm.devops.pms.model.QAgenda;
import com.gsitm.devops.pms.model.QCommittee;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.model.vo.QSteeCommAgenGrdVO;
import com.gsitm.devops.pms.model.vo.QSteeCommFrmVO;
import com.gsitm.devops.pms.model.vo.QSteeCommGrdVO;
import com.gsitm.devops.pms.model.vo.SteeCommAgenGrdVO;
import com.gsitm.devops.pms.model.vo.SteeCommCmdVO;
import com.gsitm.devops.pms.model.vo.SteeCommFrmVO;
import com.gsitm.devops.pms.model.vo.SteeCommGrdVO;
import com.gsitm.devops.pms.service.AgendaService;
import com.gsitm.devops.pms.service.CommitteeService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class SteeCommBizImpl implements SteeCommBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private CommitteeService committeeService;

	@Autowired
	private AgendaService agendaService;

	@Override
	public Map<Long, String> getLocations() {
		return codeService.getOptions("F3");
	}

	@Override
	public Page<SteeCommGrdVO> findAll(Pageable pageable, UserDetails user) {
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Committee>(
				Committee.class, "committee"));
		QCommittee committee = QCommittee.committee;
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QAccount createdBy = new QAccount("createdBy");
		JPQLQuery query = new JPAQuery(em).from(committee)
				.leftJoin(committee.project, project)
				.leftJoin(committee.project.il, il)
				.leftJoin(committee.createdBy, createdBy);
		querydsl.applyPagination(pageable, query);
		SearchResults<SteeCommGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QSteeCommGrdVO(committee.id,
					project.name, committee.title, committee.openingDate,
					committee.actualOpeningDate, il.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(il.username.eq(user.getUsername())).then(true)
					.when(createdBy.username.eq(user.getUsername())).then(true)
					.otherwise(false);
			search = query.listResults(new QSteeCommGrdVO(committee.id,
					project.name, committee.title, committee.openingDate,
					committee.actualOpeningDate, il.name, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<SteeCommGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user) {
		PathBuilder<Committee> builder = new PathBuilder<Committee>(
				Committee.class, "committee");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		QCommittee committee = QCommittee.committee;
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QAccount createdBy = new QAccount("createdBy");
		JPQLQuery query = new JPAQuery(em).from(committee)
				.leftJoin(committee.project, project)
				.leftJoin(committee.project.il, il)
				.leftJoin(committee.createdBy, createdBy).where(predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<SteeCommGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QSteeCommGrdVO(committee.id,
					project.name, committee.title, committee.openingDate,
					committee.actualOpeningDate, il.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(il.username.eq(user.getUsername())).then(true)
					.when(createdBy.username.eq(user.getUsername())).then(true)
					.otherwise(false);
			search = query.listResults(new QSteeCommGrdVO(committee.id,
					project.name, committee.title, committee.openingDate,
					committee.actualOpeningDate, il.name, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable, Long id) {
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Agenda>(
				Agenda.class, "agenda"));
		QCommittee committee = QCommittee.committee;
		QAgenda agenda = QAgenda.agenda;
		JPQLQuery query = new JPAQuery(em).from(committee)
				.innerJoin(committee.agendas, agenda)
				.where(committee.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<SteeCommAgenGrdVO> search = query
				.listResults(new QSteeCommAgenGrdVO(agenda.id, agenda.issue,
						agenda.outcome, agenda.review));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id) {
		PathBuilder<Agenda> builder = new PathBuilder<Agenda>(Agenda.class,
				"agenda");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		QCommittee committee = QCommittee.committee;
		QAgenda agenda = QAgenda.agenda;
		JPQLQuery query = new JPAQuery(em).from(committee)
				.innerJoin(committee.agendas, agenda)
				.where(committee.id.eq(id), predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<SteeCommAgenGrdVO> search = query
				.listResults(new QSteeCommAgenGrdVO(agenda.id, agenda.issue,
						agenda.outcome, agenda.review));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public void create(SteeCommAgenGrdVO steeCommAgenGrdVO, Long id) {
		Agenda agenda = new Agenda();
		BeanUtils.copyProperties(steeCommAgenGrdVO, agenda);
		agenda = agendaService.save(agenda);
		Committee committee = committeeService.findOne(id);
		committee.getAgendas().add(agenda);
		committeeService.save(committee);
	}

	@Override
	public void update(SteeCommAgenGrdVO steeCommAgenGrdVO) {
		Agenda agenda = agendaService.findOne(steeCommAgenGrdVO.getId());
		BeanUtils.copyProperties(steeCommAgenGrdVO, agenda);
		agendaService.save(agenda);
	}

	@Override
	public void delete(SteeCommAgenGrdVO steeCommAgenGrdVO, Long id) {
		Agenda agenda = agendaService.findOne(steeCommAgenGrdVO.getId());
		Committee committee = committeeService.findOne(id);
		committee.getAgendas().remove(agenda);
		committeeService.save(committee);
		agendaService.delete(agenda);
	}

	@Override
	public SteeCommFrmVO findOne(Long id) {
		QCommittee committee = QCommittee.committee;
		return new JPAQuery(em)
				.from(committee)
				.where(committee.id.eq(id))
				.singleResult(
						new QSteeCommFrmVO(committee.id,
								committee.project.code, committee.project.name,
								committee.openingDate,
								committee.actualOpeningDate,
								committee.openingTime, committee.location.id,
								committee.location.name, committee.title,
								committee.confirmed));
	}

	@Override
	public void create(SteeCommCmdVO steeCommCmdVO) {
		Committee committee = new Committee();
		BeanUtils.copyProperties(steeCommCmdVO, committee);
		try {
			List<Agenda> agendas = Arrays.asList(mapper.readValue(
					steeCommCmdVO.getRowData(), Agenda[].class));
			committee.setAgendas(new ArrayList<Agenda>());
			committee.getAgendas().addAll(agendas);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		committeeService.save(committee);
	}

	@Override
	public void update(SteeCommCmdVO steeCommCmdVO) {
		Committee committee = committeeService.findOne(steeCommCmdVO.getId());
		BeanUtils.copyProperties(steeCommCmdVO, committee);
		committeeService.save(committee);
	}

	@Override
	public void delete(SteeCommCmdVO steeCommCmdVO) {
		committeeService.delete(steeCommCmdVO.getId());
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}
}
