package com.gsitm.devops.pms.service.biz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.ProjectSize;
import com.gsitm.devops.pms.model.ProjectSizeDetail;
import com.gsitm.devops.pms.model.QProjectSize;
import com.gsitm.devops.pms.model.QProjectSizeDetail;
import com.gsitm.devops.pms.model.vo.ProjSizeCmdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjSizeDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeFrmVO;
import com.gsitm.devops.pms.model.vo.ProjSizeGrdVO;
import com.gsitm.devops.pms.model.vo.QProjSizeDetlGrdVO;
import com.gsitm.devops.pms.model.vo.QProjSizeFrmVO;
import com.gsitm.devops.pms.model.vo.QProjSizeGrdVO;
import com.gsitm.devops.pms.service.ProjectSizeDetailService;
import com.gsitm.devops.pms.service.ProjectSizeService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class ProjSizeBizImpl implements ProjSizeBiz {

	// 메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private ProjectSizeService psService;

	@Autowired
	private ProjectSizeDetailService psdService;

	/**
	 * @SpecialLogic 사용자별 jqGrid 권한
	 * 
	 * @param user
	 *            : 사용자 정보
	 * @return Map<String, Boolean>
	 */
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	/**
	 * @SpecialLogic 프로젝트 size 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjSizeGrdVO>
	 */
	@Override
	public Page<ProjSizeGrdVO> findAll(Pageable pageable, UserDetails user,
			String project) {
		QProjectSize ps = QProjectSize.projectSize;

		// 조회조건 - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(project)) {
			conditions.and(ps.project.code.contains(project).or(
					ps.project.name.contains(project)));
		}

		// 쿼리
		JPQLQuery query = new JPAQuery(em).from(ps).where(conditions);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<ProjectSize>(
				ProjectSize.class, "projectSize"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjSizeGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjSizeGrdVO(ps.id,
					ps.project.code, ps.project.name, ps.phase.name,
					ps.threshold, ps.contingency));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(ps.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(ps.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);

			search = query.listResults(new QProjSizeGrdVO(ps.id,
					ps.project.code, ps.project.name, ps.phase.name,
					ps.threshold, ps.contingency, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 프로젝트 size 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param user
	 *            : 사용자 정보
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<ProjSizeGrdVO>
	 */
	@Override
	public Page<ProjSizeGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, String project) {
		PathBuilder<ProjectSize> builder = new PathBuilder<ProjectSize>(
				ProjectSize.class, "projectSize");
		QProjectSize ps = QProjectSize.projectSize;

		// 조회조건 - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(project)) {
			conditions.and(ps.project.code.contains(project).or(
					ps.project.name.contains(project)));
		}

		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(ps)
				.where(predicate, conditions);

		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjSizeGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjSizeGrdVO(ps.id,
					ps.project.code, ps.project.name, ps.phase.name,
					ps.threshold, ps.contingency));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(ps.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(ps.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);

			search = query.listResults(new QProjSizeGrdVO(ps.id,
					ps.project.code, ps.project.name, ps.phase.name,
					ps.threshold, ps.contingency, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public ProjSizeFrmVO findOne(Long id) {
		QProjectSize ps = QProjectSize.projectSize;
		JPQLQuery query = new JPAQuery(em).from(ps).where(ps.id.eq(id));

		return query.singleResult(new QProjSizeFrmVO(ps.id, ps.project.code,
				ps.project.name, ps.project.type.name, ps.project.type.value,
				ps.phase.id, ps.threshold, ps.contingency));
	}

	/**
	 * @SpecialLogic 프로젝트 size size정보와 상세목록정보를 저장 - 신규
	 * 
	 * @param projSizeCmdVO
	 * @param projectTypeCode
	 */
	@Override
	public void create(ProjSizeCmdVO projSizeCmdVO, String projectTypeCode) {
		ProjectSize ps = new ProjectSize();
		BeanUtils.copyProperties(projSizeCmdVO, ps);

		try {
			ProjectSize projectSize = psService.save(ps);
			// 신규인 경우 id에 jqg1, jqg2씩으로 생성됨으로..
			projSizeCmdVO.setRowData(projSizeCmdVO.getRowData().replaceAll(
					"jqg", ""));
			List<ProjectSizeDetail> details = new ArrayList<ProjectSizeDetail>();
			List<ProjSizeDetlGrdVO> grdVOs = Arrays.asList(mapper.readValue(
					projSizeCmdVO.getRowData(), ProjSizeDetlGrdVO[].class));

			for (ProjSizeDetlGrdVO frmVO : grdVOs) {
				setDtlVo(frmVO, projectTypeCode);
				ProjectSizeDetail psd = new ProjectSizeDetail();
				psd.setProjectSize(projectSize);
				psd.setReportDate(frmVO.getReportDate()); // 보고 일자
				psd.setNewInput(frmVO.getNewInput()); // 신규(입력)
				psd.setNewSearch(frmVO.getNewSearch()); // 신규(조회)
				psd.setNewBatch(frmVO.getNewBatch()); // 신규(배치)
				psd.setNewReport(frmVO.getNewReport()); // 신규(보고서)
				psd.setEditInput(frmVO.getEditInput()); // 수정(입력)
				psd.setEditSearch(frmVO.getEditSearch()); // 수정(조회)
				psd.setEditBatch(frmVO.getEditBatch()); // 수정(배치)
				psd.setEditReport(frmVO.getEditReport()); // 수정(보고서)

				psd.setTotSum(frmVO.getTotSum()); // 총계
				psd.setWwCal(frmVO.getWwCal()); // WW 환산
				psdService.save(psd);
				details.add(psd);
			}
			ps.setDetails(details);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		psService.save(ps);
	}

	@Override
	public void update(ProjSizeCmdVO projSizeCmdVO) {
		ProjectSize ps = psService.findOne(projSizeCmdVO.getId());
		BeanUtils.copyProperties(projSizeCmdVO, ps);
		psService.save(ps);
	}

	@Override
	public void delete(ProjSizeCmdVO projSizeCmdVO) {
		psService.delete(projSizeCmdVO.getId());
	}

	/**
	 * @SpecialLogic 프로젝트 size별 상세 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param id
	 *            : 프로젝트 size id
	 * @return Page<ProjSizeDetlGrdVO>
	 */
	@Override
	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable, Long id) {
		QProjectSize ps = QProjectSize.projectSize;
		QProjectSizeDetail psd = QProjectSizeDetail.projectSizeDetail;

		JPQLQuery query = new JPAQuery(em).from(ps).innerJoin(ps.details, psd)
				.where(ps.id.eq(id));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectSizeDetail>(ProjectSizeDetail.class,
						"projectSizeDetail"));

		querydsl.applyPagination(pageable, query);

		SearchResults<ProjSizeDetlGrdVO> search = query
				.listResults(new QProjSizeDetlGrdVO(psd.id, psd.projectSize.id,
						psd.reportDate, psd.newInput, psd.newSearch,
						psd.newBatch, psd.newReport, psd.editInput,
						psd.editSearch, psd.editBatch, psd.editReport,
						psd.totSum, psd.wwCal));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 프로젝트 size별 상세 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param id
	 *            : 프로젝트 size id
	 * @return Page<ProjSizeDetlGrdVO>
	 */
	@Override
	public Page<ProjSizeDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id) {
		PathBuilder<ProjectSizeDetail> builder = new PathBuilder<ProjectSizeDetail>(
				ProjectSizeDetail.class, "projectSizeDetail");

		JqGrid jqgrid = new JqGrid(builder);

		QProjectSize ps = QProjectSize.projectSize;
		QProjectSizeDetail psd = QProjectSizeDetail.projectSizeDetail;

		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(ps).innerJoin(ps.details, psd)
				.where(ps.id.eq(id), predicate);

		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjSizeDetlGrdVO> search = query
				.listResults(new QProjSizeDetlGrdVO(psd.id, psd.projectSize.id,
						psd.reportDate, psd.newInput, psd.newSearch,
						psd.newBatch, psd.newReport, psd.editInput,
						psd.editSearch, psd.editBatch, psd.editReport,
						psd.totSum, psd.wwCal));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 프로젝트 size 상세 정보 저장
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param id
	 *            : 프로젝트 size id
	 * @param projectTypeCode
	 */
	@Override
	public void create(ProjSizeDetlFrmVO projSizeDetlFrmVO, Long id,
			String projectTypeCode) {
		ProjectSizeDetail psd = new ProjectSizeDetail();
		setDtlVo(projSizeDetlFrmVO, projectTypeCode);
		BeanUtils.copyProperties(projSizeDetlFrmVO, psd);
		ProjectSize ps = psService.findOne(id);
		psd.setProjectSize(ps);
		psd = psdService.save(psd);
	}

	/**
	 * @SpecialLogic 프로젝트 size 상세 정보 수정
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param id
	 *            : 프로젝트 size id
	 * @param projectTypeCode
	 */
	@Override
	public void update(ProjSizeDetlFrmVO projSizeDetlFrmVO, Long id,
			String projectTypeCode) {
		ProjectSizeDetail psd = psdService.findOne(projSizeDetlFrmVO.getId());
		setDtlVo(projSizeDetlFrmVO, projectTypeCode);
		BeanUtils.copyProperties(projSizeDetlFrmVO, psd);
		ProjectSize ps = psService.findOne(id);
		psd.setProjectSize(ps);
		psdService.save(psd);
	}

	@Override
	public void delete(ProjSizeDetlFrmVO projSizeDetlFrmVO, Long id) {
		ProjectSizeDetail pid = psdService.findOne(projSizeDetlFrmVO.getId());
		ProjectSize pi = psService.findOne(id);
		pi.getDetails().remove(pid);
		psService.save(pi);
		psdService.delete(pid);
	}

	/*
	 * 프로젝트 size 상세 총계, 환산 계산 처리
	 * 
	 * @param ProjSizeDetlFrmVO
	 * 
	 * @param projectTypeCode
	 */
	private void setDtlVo(ProjSizeDetlFrmVO detlVO, String projectTypeCode) {
		int w1 = 0;
		int w2 = 0;
		int w3 = 0;
		int w4 = 0;
		int w5 = 0;
		int w6 = 0;
		int w7 = 0;
		int w8 = 0;

		if ("T1".equals(projectTypeCode)) {
			w1 = 4;
			w2 = 2;
			w3 = 2;
			w4 = 2;
			w5 = 2;
			w6 = 1;
			w7 = 1;
			w8 = 1;
		} else if ("T2".equals(projectTypeCode)) {
			w1 = 8;
			w2 = 4;
			w3 = 4;
			w4 = 4;
			w5 = 4;
			w6 = 2;
			w7 = 2;
			w8 = 2;
		} else {
			w1 = 12;
			w2 = 6;
			w3 = 6;
			w4 = 6;
			w5 = 0;
			w6 = 0;
			w7 = 0;
			w8 = 0;
		}

		int v1 = 0;
		int v2 = 0;
		int v3 = 0;
		int v4 = 0;
		int v5 = 0;
		int v6 = 0;
		int v7 = 0;
		int v8 = 0;
		int vt1 = 0;
		int vt2 = 0;
		int vt3 = 0;
		int vt4 = 0;
		int vt5 = 0;
		int vt6 = 0;
		int vt7 = 0;
		int vt8 = 0;

		if (detlVO.getNewInput() != null && detlVO.getNewInput() > 0) {
			v1 = detlVO.getNewInput() * w1;
			vt1 = detlVO.getNewInput();
		}
		if (detlVO.getNewSearch() != null && detlVO.getNewSearch() > 0) {
			v2 = detlVO.getNewSearch() * w2;
			vt2 = detlVO.getNewSearch();
		}
		if (detlVO.getNewBatch() != null && detlVO.getNewBatch() > 0) {
			v3 = detlVO.getNewBatch() * w3;
			vt3 = detlVO.getNewBatch();
		}
		if (detlVO.getNewReport() != null && detlVO.getNewReport() > 0) {
			v4 = detlVO.getNewReport() * w4;
			vt4 = detlVO.getNewReport();
		}
		if (detlVO.getEditInput() != null && detlVO.getEditInput() > 0) {
			v5 = detlVO.getEditInput() * w5;
			vt5 = detlVO.getEditInput();
		}
		if (detlVO.getEditSearch() != null && detlVO.getEditSearch() > 0) {
			v6 = detlVO.getEditSearch() * w6;
			vt6 = detlVO.getEditSearch();
		}
		if (detlVO.getEditBatch() != null && detlVO.getEditBatch() > 0) {
			v7 = detlVO.getEditBatch() * w7;
			vt7 = detlVO.getEditBatch();
		}
		if (detlVO.getEditReport() != null && detlVO.getEditReport() > 0) {
			v8 = detlVO.getEditReport() * w8;
			vt8 = detlVO.getEditReport();
		}

		int wwCal = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8;
		detlVO.setWwCal(wwCal);

		int totSum = vt1 + vt2 + vt3 + vt4 + vt5 + vt6 + vt7 + vt8;
		detlVO.setTotSum(totSum);
	}

	/*
	 * 프로젝트 size 상세 총계, 환산 계산 처리
	 * 
	 * @param ProjSizeDetlGrdVO
	 * 
	 * @param projectTypeCode
	 */
	private void setDtlVo(ProjSizeDetlGrdVO detlVO, String projectTypeCode) {
		int w1 = 0;
		int w2 = 0;
		int w3 = 0;
		int w4 = 0;
		int w5 = 0;
		int w6 = 0;
		int w7 = 0;
		int w8 = 0;

		if ("T1".equals(projectTypeCode)) {
			w1 = 4;
			w2 = 2;
			w3 = 2;
			w4 = 2;
			w5 = 2;
			w6 = 1;
			w7 = 1;
			w8 = 1;
		} else if ("T2".equals(projectTypeCode)) {
			w1 = 8;
			w2 = 4;
			w3 = 4;
			w4 = 4;
			w5 = 4;
			w6 = 2;
			w7 = 2;
			w8 = 2;
		} else {
			w1 = 12;
			w2 = 6;
			w3 = 6;
			w4 = 6;
			w5 = 0;
			w6 = 0;
			w7 = 0;
			w8 = 0;
		}

		int v1 = 0;
		int v2 = 0;
		int v3 = 0;
		int v4 = 0;
		int v5 = 0;
		int v6 = 0;
		int v7 = 0;
		int v8 = 0;
		int vt1 = 0;
		int vt2 = 0;
		int vt3 = 0;
		int vt4 = 0;
		int vt5 = 0;
		int vt6 = 0;
		int vt7 = 0;
		int vt8 = 0;

		if (detlVO.getNewInput() != null && detlVO.getNewInput() > 0) {
			v1 = detlVO.getNewInput() * w1;
			vt1 = detlVO.getNewInput();
		}
		if (detlVO.getNewSearch() != null && detlVO.getNewSearch() > 0) {
			v2 = detlVO.getNewSearch() * w2;
			vt2 = detlVO.getNewSearch();
		}
		if (detlVO.getNewBatch() != null && detlVO.getNewBatch() > 0) {
			v3 = detlVO.getNewBatch() * w3;
			vt3 = detlVO.getNewBatch();
		}
		if (detlVO.getNewReport() != null && detlVO.getNewReport() > 0) {
			v4 = detlVO.getNewReport() * w4;
			vt4 = detlVO.getNewReport();
		}
		if (detlVO.getEditInput() != null && detlVO.getEditInput() > 0) {
			v5 = detlVO.getEditInput() * w5;
			vt5 = detlVO.getEditInput();
		}
		if (detlVO.getEditSearch() != null && detlVO.getEditSearch() > 0) {
			v6 = detlVO.getEditSearch() * w6;
			vt6 = detlVO.getEditSearch();
		}
		if (detlVO.getEditBatch() != null && detlVO.getEditBatch() > 0) {
			v7 = detlVO.getEditBatch() * w7;
			vt7 = detlVO.getEditBatch();
		}
		if (detlVO.getEditReport() != null && detlVO.getEditReport() > 0) {
			v8 = detlVO.getEditReport() * w8;
			vt8 = detlVO.getEditReport();
		}

		int wwCal = v1 + v2 + v3 + v4 + v5 + v6 + v7 + v8;
		detlVO.setWwCal(wwCal);

		int totSum = vt1 + vt2 + vt3 + vt4 + vt5 + vt6 + vt7 + vt8;
		detlVO.setTotSum(totSum);
	}

}
