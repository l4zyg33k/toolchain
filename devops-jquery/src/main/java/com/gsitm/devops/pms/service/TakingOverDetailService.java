package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.TakingOverDetail;

public interface TakingOverDetailService extends
		SharedService<TakingOverDetail, Long> {

}
