package com.gsitm.devops.pms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjMembCmdMbsVO;
import com.gsitm.devops.pms.model.vo.ProjMembCmdVO;
import com.gsitm.devops.pms.model.vo.ProjMembFrmVO;
import com.gsitm.devops.pms.model.vo.ProjMembGrdVO;

public interface ProjMembBiz extends MessageSourceAware {

	/**
	 * 
	 * @param user
	 * @return
	 */
	public Map<String, Boolean> getNavGrid(UserDetails user);

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ProjMembFrmVO findOne(Long id);

	/**
	 * 
	 * @param pageable
	 * @param user
	 * @param projectCode
	 * @return
	 */
	public Page<ProjMembGrdVO> findAll(Pageable pageable, UserDetails user,
			String projectCode);

	/**
	 * 
	 * @param pageable
	 * @param searchable
	 * @param user
	 * @param projectCode
	 * @return
	 */
	public Page<ProjMembGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, String projectCode);

	/**
	 * 프로젝트 팀원 관리 등록 처리
	 * 
	 * @param vo
	 */
	public void create(ProjMembCmdVO vo);

	/**
	 * 
	 * @param projMembCmdMbsVO
	 */
	public void create(ProjMembCmdMbsVO projMembCmdMbsVO);

	/**
	 * 
	 * @param vo
	 */
	public void update(ProjMembCmdVO vo);

	/**
	 * 
	 * @param vo
	 */
	public void delete(ProjMembCmdVO vo);

	/**
	 * 
	 * @return
	 */
	public Map<Long, String> getRoles();

	/**
	 * 
	 * @param pageable
	 * @param project
	 * @return
	 */
	public Page<AcntMngGrdVO> findAll(Pageable pageable, String project);

	/**
	 * 
	 * @param pageable
	 * @param searchable
	 * @param project
	 * @return
	 */
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			String project);
	
	/**
	 * 
	 * @param user
	 * @return
	 */
	public Map<String, String> getProjects(UserDetails user);
}
