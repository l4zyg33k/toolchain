package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class SteeCommFrmVO extends SteeCommWebVO {

	private String project;

	private String projectName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openingDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualOpeningDate;

	@DateTimeFormat(pattern = "a h:mm")
	private LocalTime openingTime;

	private Long location;

	private String locationName;

	private String title;

	private Boolean confirmed;

	public SteeCommFrmVO(int page, int rowNum, long rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}

	@QueryProjection
	public SteeCommFrmVO(Long id, String project, String projectName,
			LocalDate openingDate, LocalDate actualOpeningDate,
			LocalTime openingTime, Long location, String locationName,
			String title, Boolean confirmed) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.openingDate = openingDate;
		this.actualOpeningDate = actualOpeningDate;
		this.openingTime = openingTime;
		this.location = location;
		this.locationName = locationName;
		this.title = title;
		this.confirmed = confirmed;
	}
}
