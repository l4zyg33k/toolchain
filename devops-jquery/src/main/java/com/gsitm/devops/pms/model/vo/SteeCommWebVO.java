package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class SteeCommWebVO extends SharedWebVO<Long> {

	private String rowData;

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/pms/steecomm" : String.format("/pms/steecomm/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/pms/steecomm?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getAgendasUrl() {
		return String.format("/rest/pms/steecomm/%d/agendas", getId());
	}

	public String getAgendasEditUrl() {
		return String.format("/rest/pms/steecomm/%d/agendas/edit", getId());
	}
}
