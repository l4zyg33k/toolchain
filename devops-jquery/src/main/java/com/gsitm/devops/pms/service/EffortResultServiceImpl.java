package com.gsitm.devops.pms.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.EffortResult;
import com.gsitm.devops.pms.repository.EffortResultRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class EffortResultServiceImpl implements EffortResultService {

	@Autowired
	private EffortResultRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public EffortResult findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public EffortResult findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<EffortResult> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<EffortResult> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<EffortResult> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<EffortResult> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<EffortResult> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<EffortResult> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public EffortResult save(EffortResult t) {
		return repository.save(t);
	}

	@Transactional
	@Override
	public <S extends EffortResult> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(EffortResult t) {
		repository.delete(t);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends EffortResult> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
