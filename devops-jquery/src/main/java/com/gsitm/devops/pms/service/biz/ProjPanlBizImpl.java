package com.gsitm.devops.pms.service.biz;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.model.vo.ProjPanlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjPanlGrdVO;
import com.gsitm.devops.pms.model.vo.QProjPanlFrmVO;
import com.gsitm.devops.pms.model.vo.QProjPanlGrdVO;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class ProjPanlBizImpl implements ProjPanlBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Override
	public ProjPanlFrmVO findOne(String code) {
		QProject project = QProject.project;
		QCode type = new QCode("type");
		QAccount il = new QAccount("il");
		QAccount op = new QAccount("op");
		QAccount ou = new QAccount("ou");
		QAccount qa = new QAccount("qa");
		QCode office = new QCode("office");
		QCode phase = new QCode("phase");
		QCode location = new QCode("location");
		QCode pass = new QCode("pass");
		QCode closeReportType = new QCode("closeReportType");
		JPQLQuery query = new JPAQuery(em).from(project)
				.leftJoin(project.type, type).leftJoin(project.il, il)
				.leftJoin(project.op, op).leftJoin(project.ou, ou)
				.leftJoin(project.qa, qa).leftJoin(project.office, office)
				.leftJoin(project.phase, phase)
				.leftJoin(project.location, location)
				.leftJoin(project.pass, pass)
				.leftJoin(project.closeReportType, closeReportType)
				.where(project.code.eq(code));
		return query.uniqueResult(new QProjPanlFrmVO(project.code,
				project.name, type.id, type.name, project.pmName,
				project.blName, project.blDepartment, il.username, il.name,
				op.username, op.name, ou.username, qa.username, qa.name,
				office.id, office.name, project.startDate, project.finishDate,
				project.actualStartDate, project.actualFinishDate, phase.id,
				phase.name, project.panelStartDate, project.panelFinishDate,
				project.panelActualDate, project.panelActualTime, location.id,
				location.name, project.recommandation, pass.id, pass.name,
				project.followUpStartDate, project.followUpFinishDate,
				project.followUpActualStartDate,
				project.followUpActualFinishDate, project.ouEffort,
				project.ouActualEffort, project.pdEffort,
				project.pdActualEffort, project.processPoint,
				project.testingPoint, project.productPoint,
				project.customerPoint, project.workingHour, project.workingDay,
				project.workingEffort, project.closeReportDate,
				closeReportType.id, closeReportType.name,
				project.openCheckDate, project.openReportDate,
				project.watching, project.satisfactionExpired));
	}

	@Override
	public Page<ProjPanlGrdVO> findAll(Pageable pageable) {
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QCode office = new QCode("office");
		QCode location = new QCode("location");
		JPQLQuery query = new JPAQuery(em).from(project)
				.leftJoin(project.il, il).leftJoin(project.office, office)
				.leftJoin(project.location, location);
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Project>(
				Project.class, "project"));
		querydsl.applyPagination(pageable, query);
		SearchResults<ProjPanlGrdVO> search = query
				.listResults(new QProjPanlGrdVO(office.name, project.code,
						project.name, project.pmName, il.name,
						project.panelStartDate, project.panelFinishDate,
						project.panelActualDate, project.panelActualTime,
						location.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjPanlGrdVO> findAll(Pageable pageable, Searchable searchable) {
		PathBuilder<Project> builder = new PathBuilder<Project>(Project.class,
				"project");
		QProject project = QProject.project;
		QAccount il = new QAccount("il");
		QCode office = new QCode("office");
		QCode location = new QCode("location");
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(project)
				.leftJoin(project.il, il).leftJoin(project.office, office)
				.leftJoin(project.location, location).where(predicate);
		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);
		SearchResults<ProjPanlGrdVO> search = query
				.listResults(new QProjPanlGrdVO(office.name, project.code,
						project.name, project.pmName, il.name,
						project.panelStartDate, project.panelFinishDate,
						project.panelActualDate, project.panelActualTime,
						location.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
