package com.gsitm.devops.pms.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjSizeFrmVO extends ProjSizeWebVO {

	private Long id;

	// 프로젝트
	private String project; // 프로젝트 코드
	private String projectName; // 프로젝트 명
	private String projectTypeName; //프로젝트 유형
	private String projectTypeCode; //프로젝트 유형 코드
	
	//단계 (코드)
	private Long phase;	

	//Threshold
	private Integer threshold;

	//Contingency
	private Integer contingency;
	
	
	public ProjSizeFrmVO(int page, int rowNum, long rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}

	@QueryProjection
	public ProjSizeFrmVO(Long id, String project, String projectName,
			String projectTypeName, String projectTypeCode, Long phase,
			Integer threshold, Integer contingency) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.projectTypeName = projectTypeName;
		this.projectTypeCode = projectTypeCode;
		this.phase = phase;
		this.threshold = threshold;
		this.contingency = contingency;
	}
}
