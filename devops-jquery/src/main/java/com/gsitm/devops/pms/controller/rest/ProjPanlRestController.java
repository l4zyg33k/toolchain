package com.gsitm.devops.pms.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.ProjPanlGrdVO;
import com.gsitm.devops.pms.service.biz.ProjPanlBiz;

@RestController
public class ProjPanlRestController {

	@Autowired
	private ProjPanlBiz biz;

	@RequestMapping(value = "/rest/pms/projpanl", method = RequestMethod.POST)
	public Page<ProjPanlGrdVO> findAll(Pageable pageable) {
		return biz.findAll(pageable);
	}

	@RequestMapping(value = "/rest/pms/projpanl", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjPanlGrdVO> findAll(Pageable pageable, Searchable searchable) {
		return biz.findAll(pageable, searchable);
	}
}
