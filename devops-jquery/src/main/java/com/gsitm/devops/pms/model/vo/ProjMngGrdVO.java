package com.gsitm.devops.pms.model.vo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjMngGrdVO {

	private String code;

	private String name;

	@JsonProperty("office.name")
	private String officeName;

	private String pmName;

	@JsonProperty("il.name")
	private String ilName;

	@JsonProperty("status.name")
	private String statusName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate finishDate;

	private Boolean privacy;

	private Boolean critical;

	private Boolean editable;

	public ProjMngGrdVO() {
		super();
	}

	@QueryProjection
	public ProjMngGrdVO(String code, String name, String officeName, String pmName, String ilName, String statusName,
			LocalDate finishDate, Boolean privacy, Boolean critical) {
		super();
		this.code = code;
		this.name = name;
		this.officeName = officeName;
		this.pmName = pmName;
		this.ilName = ilName;
		this.statusName = statusName;
		this.finishDate = finishDate;
		this.privacy = privacy;
		this.critical = critical;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public ProjMngGrdVO(String code, String name, String officeName, String pmName, String ilName, String statusName,
			LocalDate finishDate, Boolean privacy, Boolean critical, Boolean editable) {
		super();
		this.code = code;
		this.name = name;
		this.officeName = officeName;
		this.pmName = pmName;
		this.ilName = ilName;
		this.statusName = statusName;
		this.finishDate = finishDate;
		this.privacy = privacy;
		this.critical = critical;
		this.editable = editable;
	}
}
