package com.gsitm.devops.pms.model.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class SteeCommCmdVO extends SteeCommWebVO {

	private Project project;

	@NotBlank
	private String projectName;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate openingDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualOpeningDate;

	@DateTimeFormat(pattern = "a h:mm")
	private LocalTime openingTime;

	@NotNull
	private Code location;

	@NotBlank
	private String title;

	private Boolean confirmed;
}
