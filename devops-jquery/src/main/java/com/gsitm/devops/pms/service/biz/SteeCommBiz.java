package com.gsitm.devops.pms.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.vo.SteeCommAgenGrdVO;
import com.gsitm.devops.pms.model.vo.SteeCommCmdVO;
import com.gsitm.devops.pms.model.vo.SteeCommFrmVO;
import com.gsitm.devops.pms.model.vo.SteeCommGrdVO;

public interface SteeCommBiz extends MessageSourceAware {

	public Map<Long, String> getLocations();

	public Page<SteeCommGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<SteeCommGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user);

	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable, Long id);

	public Page<SteeCommAgenGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id);

	public void create(SteeCommAgenGrdVO steeCommAgenGrdVO, Long id);

	public void update(SteeCommAgenGrdVO steeCommAgenGrdVO);

	public void delete(SteeCommAgenGrdVO steeCommAgenGrdVO, Long id);

	public SteeCommFrmVO findOne(Long id);

	public void create(SteeCommCmdVO steeCommCmdVO);

	public void update(SteeCommCmdVO steeCommCmdVO);

	public void delete(SteeCommCmdVO steeCommCmdVO);

	public Map<String, Boolean> getNavGrid(UserDetails user);
}
