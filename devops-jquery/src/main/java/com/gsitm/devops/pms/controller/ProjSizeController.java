package com.gsitm.devops.pms.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.pms.model.vo.ProjSizeCmdVO;
import com.gsitm.devops.pms.model.vo.ProjSizeFrmVO;
import com.gsitm.devops.pms.service.biz.ProjSizeBiz;

@Controller
public class ProjSizeController {

	@Autowired
	private ProjSizeBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;	
	
	//프로젝트 진행단계 (코드)
	@ModelAttribute("phases")
	public Map<Long, String> getProjectPhases() {
		return cmmCodeService.getProjectPhases();
	}	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute("menuCode")
	public String menuCode() {
		return "ProjSize";
	}	
	
	//사용자별 jqGrid 권한 처리로 반드시 선언되어야 한다. 
	@ModelAttribute("navGrid")
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}	
	
	/**
	 * @SpecialLogic 프로젝트 사이즈 페이지 처리 
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = "/pms/projsize", method = RequestMethod.GET, params = {
			"page", "rowNum", "rowId" })
	public String index(@RequestParam("page") int page,
			@RequestParam("rowNum") int rowNum,
			@RequestParam("rowId") String rowId, Model model) {
		model.addAttribute("page", page);
		model.addAttribute("rowNum", rowNum);
		model.addAttribute("rowId", rowId);
		return "pms/projsize/index";
	}

	/**
	 * @SpecialLogic 프로젝트 사이즈  페이지 로딩시 
	 * 
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = "/pms/projsize", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("page", 1);
		model.addAttribute("rowNum", 20);
		model.addAttribute("rowId", "");
		return "pms/projsize/index";
	}

	/**
	 * @SpecialLogic 프로젝트 사이즈  등록 화면으로 이동
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */	    
	@RequestMapping(value = "/pms/projsize/form", method = RequestMethod.GET, params = {
			"page", "rowNum", "rowId" })
	public String form(@RequestParam("page") int page,
			@RequestParam("rowNum") int rowNum,
			@RequestParam("rowId") long rowId, Model model) {
		ProjSizeFrmVO projSizeFrmVO = new ProjSizeFrmVO(page, rowNum, rowId);
		model.addAttribute("projSizeFrmVO", projSizeFrmVO);
		return "pms/projsize/form";
	}
	
	/**
	 * @SpecialLogic 프로젝트 사이즈  등록 처리 - 신규(size정보와 상세목록정보를 저장)
	 * 
	 * @param projSizeCmdVO
	 * @param result
	 * @param redirectAttributes
	 * @return string
	 */		
	@RequestMapping(value = "/pms/projsize", method = RequestMethod.POST)
	public String create(
			@ModelAttribute("projSizeFrmVO") @Valid ProjSizeCmdVO projSizeCmdVO,
			BindingResult result, RedirectAttributes redirectAttributes,
			@RequestParam(value = "projectTypeCode", defaultValue = "", required = false) String projectTypeCode) {
		if (result.hasErrors()) {
			return "pms/projsize/form";
		}
		biz.create(projSizeCmdVO, projectTypeCode);		
		redirectAttributes.addAttribute("page", projSizeCmdVO.getPage());
		redirectAttributes.addAttribute("rowNum", projSizeCmdVO.getRowNum());
		redirectAttributes.addAttribute("rowId", projSizeCmdVO.getRowId());
		return "redirect:/pms/projsize?page={page}&rowNum={rowNum}&rowId={rowId}";
	}
		
	/**
	 * @SpecialLogic 프로젝트 사이즈  수정처리
	 * 
	 * @param projSizeCmdVO
	 * @param result
	 * @param redirectAttributes
	 * @return string
	 */		
	@RequestMapping(value = "/pms/projsize/*", method = RequestMethod.PUT)
	public String update(@ModelAttribute("projSizeFrmVO") @Valid ProjSizeCmdVO projSizeCmdVO,
			BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "pms/projsize/form";
		}
		biz.update(projSizeCmdVO);
		redirectAttributes.addAttribute("page", projSizeCmdVO.getPage());
		redirectAttributes.addAttribute("rowNum", projSizeCmdVO.getRowNum());
		redirectAttributes.addAttribute("rowId", projSizeCmdVO.getRowId());
		return "redirect:/pms/projsize?page={page}&rowNum={rowNum}&rowId={rowId}";
	}	

	/**
	 * @SpecialLogic 프로젝트 사이즈  등록, 수정 화면으로 이동
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param id
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = "/pms/projsize/{id}/form", method = RequestMethod.GET, params = {
			"page", "rowNum", "rowId" })
	public String form(@RequestParam("page") int page,
			@RequestParam("rowNum") int rowNum,
			@RequestParam("rowId") long rowId, @PathVariable("id") Long id,
			Model model) {
		ProjSizeFrmVO projSizeFrmVO = biz.findOne(id);
		projSizeFrmVO.setPage(page);
		projSizeFrmVO.setRowNum(rowNum);
		projSizeFrmVO.setRowId(rowId);
		model.addAttribute("projSizeFrmVO", projSizeFrmVO);
		return "pms/projsize/form";
	}	

}
