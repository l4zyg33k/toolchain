package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjEfotDetlGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate workDate;

	private Float beginning;

	private Float analysis;

	private Float design;

	private Float development;

	private Float test;

	private Float execution;

	private Float education;

	private Float etc;

	@QueryProjection
	public ProjEfotDetlGrdVO(Long id, LocalDate workDate, Float beginning,
			Float analysis, Float design, Float development, Float test,
			Float execution, Float education, Float etc) {
		super();
		this.id = id;
		this.workDate = workDate;
		this.beginning = beginning;
		this.analysis = analysis;
		this.design = design;
		this.development = development;
		this.test = test;
		this.execution = execution;
		this.education = education;
		this.etc = etc;
	}
}
