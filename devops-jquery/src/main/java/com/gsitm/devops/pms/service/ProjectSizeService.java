package com.gsitm.devops.pms.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.ProjectSize;

public interface ProjectSizeService extends
		SharedService<ProjectSize, Long> {

}
