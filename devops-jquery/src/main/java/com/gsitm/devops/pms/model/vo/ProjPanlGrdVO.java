package com.gsitm.devops.pms.model.vo;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjPanlGrdVO {

	@JsonProperty("office.name")
	private String officeName;

	private String code;

	private String name;

	private String pmName;

	@JsonProperty("il.name")
	private String ilName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate panelStartDate;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate panelFinishDate;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate panelActualDate;

	@JsonFormat(shape = Shape.STRING, pattern = "a h:mm", timezone = "GMT+9")
	private LocalTime panelActualTime;

	@JsonProperty("location.name")
	private String locationName;

	@QueryProjection
	public ProjPanlGrdVO(String officeName, String code, String name,
			String pmName, String ilName, LocalDate panelStartDate,
			LocalDate panelFinishDate, LocalDate panelActualDate,
			LocalTime panelActualTime, String locationName) {
		super();
		this.officeName = officeName;
		this.code = code;
		this.name = name;
		this.pmName = pmName;
		this.ilName = ilName;
		this.panelStartDate = panelStartDate;
		this.panelFinishDate = panelFinishDate;
		this.panelActualDate = panelActualDate;
		this.panelActualTime = panelActualTime;
		this.locationName = locationName;
	}
}
