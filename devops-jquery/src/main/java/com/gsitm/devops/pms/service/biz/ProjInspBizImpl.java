package com.gsitm.devops.pms.service.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.ProjectInspection;
import com.gsitm.devops.pms.model.ProjectInspectionDetail;
import com.gsitm.devops.pms.model.QProjectInspection;
import com.gsitm.devops.pms.model.QProjectInspectionDetail;
import com.gsitm.devops.pms.model.vo.ProjInspCmdVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlFrmVO;
import com.gsitm.devops.pms.model.vo.ProjInspDetlGrdVO;
import com.gsitm.devops.pms.model.vo.ProjInspFrmVO;
import com.gsitm.devops.pms.model.vo.ProjInspGrdVO;
import com.gsitm.devops.pms.model.vo.QProjInspDetlGrdVO;
import com.gsitm.devops.pms.model.vo.QProjInspFrmVO;
import com.gsitm.devops.pms.model.vo.QProjInspGrdVO;
import com.gsitm.devops.pms.service.ProjectInspectionDetailService;
import com.gsitm.devops.pms.service.ProjectInspectionService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class ProjInspBizImpl implements ProjInspBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private ProjectInspectionService piService;

	@Autowired
	private ProjectInspectionDetailService pidService;

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public Map<Long, String> getProductTypes() {
		return codeService.getOptions("G5");
	}

	@Override
	public Map<Long, String> getProductPhases() {
		QCode code = QCode.code;
		JPQLQuery query = new JPAQuery(em)
				.from(code)
				.where(code.parent.value.eq("G6"), code.enabled.eq(true),
						code.value.in(new String[] { "G1", "G2", "G3" }))
				.orderBy(code.rank.asc(), code.value.asc());
		return query.map(code.id, code.name);
	}

	@Override
	public Page<ProjInspGrdVO> findAll(Pageable pageable, UserDetails user) {
		QProjectInspection pi = QProjectInspection.projectInspection;

		JPQLQuery query = new JPAQuery(em).from(pi);

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectInspection>(ProjectInspection.class,
						"projectInspection"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjInspGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjInspGrdVO(pi.id,
					pi.project.code, pi.project.name, pi.productName,
					pi.productType.name, pi.productPhase.name, pi.productSize,
					pi.inspectionDate, pi.project.il.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(pi.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(pi.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);

			search = query.listResults(new QProjInspGrdVO(pi.id,
					pi.project.code, pi.project.name, pi.productName,
					pi.productType.name, pi.productPhase.name, pi.productSize,
					pi.inspectionDate, pi.project.il.name, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjInspGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user) {
		PathBuilder<ProjectInspection> builder = new PathBuilder<ProjectInspection>(
				ProjectInspection.class, "projectInspection");
		QProjectInspection pi = QProjectInspection.projectInspection;

		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(pi).where(predicate);

		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjInspGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjInspGrdVO(pi.id,
					pi.project.code, pi.project.name, pi.productName,
					pi.productType.name, pi.productPhase.name, pi.productSize,
					pi.inspectionDate, pi.project.il.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(pi.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(pi.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);

			search = query.listResults(new QProjInspGrdVO(pi.id,
					pi.project.code, pi.project.name, pi.productName,
					pi.productType.name, pi.productPhase.name, pi.productSize,
					pi.inspectionDate, pi.project.il.name, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public ProjInspFrmVO findOne(Long id) {
		QProjectInspection pi = QProjectInspection.projectInspection;
		JPQLQuery query = new JPAQuery(em).from(pi).where(pi.id.eq(id));
		return query.singleResult(new QProjInspFrmVO(pi.id, pi.project.code,
				pi.project.name, pi.productName, pi.productType.id,
				pi.productType.name, pi.productPhase.id, pi.productPhase.name,
				pi.productSize, pi.producer.username, pi.producer.name,
				pi.inspectionDate, pi.inspectors, pi.moderator.username,
				pi.moderator.name, pi.inspectionPercent,
				pi.inspectionMeetingTime));
	}

	@Override
	public void create(ProjInspCmdVO projInspCmdVO) {
		ProjectInspection pi = new ProjectInspection();
		BeanUtils.copyProperties(projInspCmdVO, pi);
		try {
			List<ProjectInspectionDetail> details = new ArrayList<ProjectInspectionDetail>();
			List<ProjInspDetlGrdVO> grdVOs = Arrays.asList(mapper.readValue(
					projInspCmdVO.getRowData(), ProjInspDetlGrdVO[].class));
			for (ProjInspDetlGrdVO frmVO : grdVOs) {
				ProjectInspectionDetail pid = new ProjectInspectionDetail();
				pid.setDefectGrade(codeService.findOne(frmVO.getDefectGrade()));
				pid.setDefectType(codeService.findOne(frmVO.getDefectType()));
				pid.setContent(frmVO.getContent());
				details.add(pid);
			}
			pi.setDetails(details);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		piService.save(pi);
	}

	@Override
	public void update(ProjInspCmdVO projInspCmdVO) {
		ProjectInspection pi = piService.findOne(projInspCmdVO.getId());
		BeanUtils.copyProperties(projInspCmdVO, pi);
		piService.save(pi);
	}

	@Override
	public void delete(ProjInspCmdVO projInspCmdVO) {
		piService.delete(projInspCmdVO.getId());
	}

	@Override
	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable, Long id) {
		QProjectInspection pi = QProjectInspection.projectInspection;
		QProjectInspectionDetail pid = QProjectInspectionDetail.projectInspectionDetail;

		JPQLQuery query = new JPAQuery(em).from(pi).innerJoin(pi.details, pid)
				.where(pi.id.eq(id));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectInspectionDetail>(
						ProjectInspectionDetail.class,
						"projectInspectionDetail"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjInspDetlGrdVO> search = query
				.listResults(new QProjInspDetlGrdVO(pid.id, pid.defectGrade.id,
						pid.defectType.id, pid.content));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjInspDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id) {
		PathBuilder<ProjectInspectionDetail> builder = new PathBuilder<ProjectInspectionDetail>(
				ProjectInspectionDetail.class, "projectInspectionDetail");
		JqGrid jqgrid = new JqGrid(builder);

		QProjectInspection pi = QProjectInspection.projectInspection;
		QProjectInspectionDetail pid = QProjectInspectionDetail.projectInspectionDetail;
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(pi).innerJoin(pi.details, pid)
				.where(pi.id.eq(id), predicate);

		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjInspDetlGrdVO> search = query
				.listResults(new QProjInspDetlGrdVO(pid.id, pid.defectGrade.id,
						pid.defectType.id, pid.content));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public void create(ProjInspDetlFrmVO projInspDetlFrmVO, Long id) {
		ProjectInspectionDetail pid = new ProjectInspectionDetail();
		BeanUtils.copyProperties(projInspDetlFrmVO, pid);
		pid = pidService.save(pid);
		ProjectInspection pi = piService.findOne(id);
		pi.getDetails().add(pid);
		piService.save(pi);
	}

	@Override
	public void update(ProjInspDetlFrmVO projInspDetlFrmVO) {
		ProjectInspectionDetail pid = pidService.findOne(projInspDetlFrmVO
				.getId());
		BeanUtils.copyProperties(projInspDetlFrmVO, pid);
		pidService.save(pid);
	}

	@Override
	public void delete(ProjInspDetlFrmVO projInspDetlFrmVO, Long id) {
		ProjectInspectionDetail pid = pidService.findOne(projInspDetlFrmVO
				.getId());
		ProjectInspection pi = piService.findOne(id);
		pi.getDetails().remove(pid);
		piService.save(pi);
		pidService.delete(pid);
	}

	@Override
	public String getDefectTypes(Long id) {
		QCode code = QCode.code;
		QCode child = new QCode("child");
		QCode parent = new QCode("parent");

		JPQLQuery query = new JPAQuery(em)
				.from(parent)
				.innerJoin(parent.children, child)
				.where(parent.parent.isNull(),
						parent.value.eq(new JPASubQuery().from(code)
								.where(code.id.eq(id)).unique(code.value)));

		Map<Long, String> results = query.map(child.id, child.name);

		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}

	@Override
	public String getDefectGrades() {
		Map<Long, String> results = codeService.getOptions("G9");
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}

	@Override
	public Page<ProjInspGrdVO> findAll(Pageable pageable, String projectCode) {
		QProjectInspection pi = QProjectInspection.projectInspection;

		// 조회조건 - 프로젝트 코드
		BooleanBuilder conditions = new BooleanBuilder();
		if (projectCode != null) {
			conditions.and(pi.project.code.eq(projectCode));
		}

		JPQLQuery query = new JPAQuery(em).from(pi).where(conditions);

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectInspection>(ProjectInspection.class,
						"projectInspection"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjInspGrdVO> search = query
				.listResults(new QProjInspGrdVO(pi.id, pi.project.code,
						pi.project.name, pi.productName, pi.productType.name,
						pi.productPhase.name, pi.productSize,
						pi.inspectionDate, pi.project.il.name,
						new CaseBuilder().when(pi.isNotNull()).then(true)
								.otherwise(false)));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
