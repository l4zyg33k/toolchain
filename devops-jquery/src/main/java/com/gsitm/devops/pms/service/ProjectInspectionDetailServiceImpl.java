package com.gsitm.devops.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.pms.model.ProjectInspectionDetail;
import com.gsitm.devops.pms.repository.ProjectInspectionDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 프로젝트 Inspection 상세 서비스
 */
@Service
@Transactional(readOnly = true)
public class ProjectInspectionDetailServiceImpl implements
		ProjectInspectionDetailService {

	/**
	 * 프로젝트 Inspection 상세 리파지토리
	 */
	@Autowired
	private ProjectInspectionDetailRepository repository;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public ProjectInspectionDetail findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public ProjectInspectionDetail findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectInspectionDetail> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<ProjectInspectionDetail> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectInspectionDetail> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectInspectionDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectInspectionDetail> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectInspectionDetail> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProjectInspectionDetail save(
			ProjectInspectionDetail projectInspectionDetail) {

		return repository.save(projectInspectionDetail);
	}

	@Transactional
	@Override
	public <S extends ProjectInspectionDetail> List<S> save(Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectInspectionDetail projectInspectionDetail) {

		repository.delete(projectInspectionDetail);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProjectInspectionDetail> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

}
