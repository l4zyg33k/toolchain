/**
 * 프로젝트 size등록 엔티티
 * ProjectSize.java
 * @author Administrator
 */
package com.gsitm.devops.pms.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectSize extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 프로젝트 단계 (코드)
	 */
	@ManyToOne
	private Code phase;

	/**
	 * Threshold
	 */
	private Integer threshold;

	/**
	 * Contingency
	 */
	private Integer contingency;

	/**
	 * 프로젝트 규모 상세
	 */
	@OneToMany(mappedBy = "projectSize", orphanRemoval = true)
	private List<ProjectSizeDetail> details;
}
