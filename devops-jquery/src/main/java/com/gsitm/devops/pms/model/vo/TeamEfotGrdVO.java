package com.gsitm.devops.pms.model.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class TeamEfotGrdVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String workDate;

	private Float beginning;

	private Float analysis;

	private Float design;

	private Float development;

	private Float test;

	private Float execution;

	private Float education;

	private Float etc;

	@QueryProjection
	public TeamEfotGrdVO(String workDate, Float beginning, Float analysis,
			Float design, Float development, Float test, Float execution,
			Float education, Float etc) {
		super();
		this.workDate = workDate;
		this.beginning = beginning;
		this.analysis = analysis;
		this.design = design;
		this.development = development;
		this.test = test;
		this.execution = execution;
		this.education = education;
		this.etc = etc;
	}
}
