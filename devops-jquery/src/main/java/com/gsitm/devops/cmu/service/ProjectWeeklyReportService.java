package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;

public interface ProjectWeeklyReportService extends
		SharedService<ProjectWeeklyReport, Long> {

}
