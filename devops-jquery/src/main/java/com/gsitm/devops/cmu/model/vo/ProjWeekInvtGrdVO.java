package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjWeekInvtGrdVO {

	private Long id;
	
	//프로젝트코드
	@JsonProperty("project.code")
	private String project;
	
	//프로젝트 명
	@JsonProperty("project.name")
	private String projectName;	
	
	//보고서 제목
	private String title;
	
	//보고일자
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate reportDate;
	
	//작성자
	private String reportName;
	
	@QueryProjection
	public ProjWeekInvtGrdVO(Long id, String project, String projectName,
			String title, LocalDate reportDate, String reportName) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.title = title;
		this.reportDate = reportDate;
		this.reportName = reportName;
	}
}
