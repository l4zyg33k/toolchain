package com.gsitm.devops.cmu.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.cmu.model.Issue;

@Repository
public interface IssueRepository extends SharedRepository<Issue, Long> {
}
