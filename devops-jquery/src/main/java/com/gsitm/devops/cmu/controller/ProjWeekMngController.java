package com.gsitm.devops.cmu.controller;

import java.util.Map;

import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngCmdVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngFrmVO;
import com.gsitm.devops.cmu.service.biz.ProjWeekMngBiz;

@Controller
public class ProjWeekMngController {

	private static final String MENU_CODE = "ProjWeekMng";
	private static final String FORM_VO = "projWeekMngFrmVO";

	private static final String INDEX_URL = "/cmu/projweekmng";
	private static final String ACTION_URL = "/cmu/projweekmng/*";
	private static final String FORM_URL = "/cmu/projweekmng/form";
	private static final String FORM_URL_BY_ID = "/cmu/projweekmng/{id}/form";
	private static final String REDIRECT_URL = "redirect:/cmu/projweekmng?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "cmu/projweekmng/index";
	private static final String FORM_PAGE = "cmu/projweekmng/form";

	@Autowired
	private ProjWeekMngBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("phases")
	public Map<Long, String> getProductPhases() {
		return biz.getPhases();
	}

	@ModelAttribute("projects")
	public Map<String, String> getProjects(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getProjects(user);
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, 0);
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId, Model model,
			@AuthenticationPrincipal UserDetails user) {
		ProjWeekMngFrmVO vo = new ProjWeekMngFrmVO(page, rowNum, rowId);
		StringBuilder sb = new StringBuilder(
				"<p>1. 주요 논리/요청 사항(이슈/리스크는 QMS 등록)</p>");
		sb.append("<p>&nbsp;</p>");
		sb.append("<p>2. 프로젝트 진행실적</p>");
		sb.append("<ol>");
		sb.append("<li>전체 진척율 : [계획] xx.x% / [실적] xx.x%</li>");
		sb.append("<li>개발 진척율 : [계획] xx.x% / [실적] xx.x%</li>");
		sb.append("<li>프로젝트 진행 현황<br />");
		sb.append("&nbsp;</li>");
		sb.append("</ol>");
		sb.append("<p>3. 기타</p>");
		sb.append("&nbsp;</li>");
		sb.append("<p>4. 주요 마일스톤 일정</p>");
		sb.append("<ol>");
		sb.append("<li>착수단계 ( MM/DD )");
		sb.append("<ol>");
		sb.append("<li>Kick-Off 일자 : MM/DD</li>");
		sb.append("</ol></li>");
		sb.append("<li>분석단계 ( MM/DD )</li>");
		sb.append("<li>설계단계 ( MM/DD )</li>");
		sb.append("<li>개발(구현)단계 ( MM/DD )</li>");
		sb.append("<li>테스트단계 ( MM/DD )</li>");
		sb.append("<li>이행(전개)단계 ( MM/DD )");
		sb.append("<ol>");
		sb.append("<li> 프로젝트 Open 예정일 : MM/DD</li>");
		sb.append("</ol></li>");
		sb.append("</ol>");
		sb.append("<p>5. 협력업체 보안관리는 잘 되고 있나요?</p>");
		sb.append("<p>&nbsp;</p>");
		vo.setThisWeek(sb.toString());
		vo.setNextWeek("<p>차주 계획 보고 가능하도록 반드시 입력</p>");
		vo.setReportDate(new LocalDate());
		AcntItemVO acntVO = biz.getReporter(user);
		vo.setReporter(acntVO.getUsername());
		vo.setReporterName(acntVO.getName());
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(@ModelAttribute(FORM_VO) @Valid ProjWeekMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@PathVariable(PageParams.ID) long id, Model model) {
		ProjWeekMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid ProjWeekMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}
}
