package com.gsitm.devops.cmu.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.cmu.model.vo.CoWorkMngCmdVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngFrmVO;
import com.gsitm.devops.cmu.service.biz.CoWorkMngBiz;

@Controller
public class CoWorkMngController {

	private static final String MENU_CODE = "CoWorkMng";
	private static final String FORM_VO = "coWorkMngFrmVO";

	private static final String INDEX_URL = "/cmu/coworkmng";
	private static final String ACTION_URL = "/cmu/coworkmng/*";
	private static final String FORM_URL = "/cmu/coworkmng/form";
	private static final String FORM_URL_BY_ID = "/cmu/coworkmng/{id}/form";
	private static final String VIEW_URL_BY_ID = "/cmu/coworkmng/{id}/view";
	private static final String REDIRECT_URL = "redirect:/cmu/coworkmng?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "cmu/coworkmng/index";
	private static final String FORM_PAGE = "cmu/coworkmng/form";
	private static final String VIEW_PAGE = "cmu/coworkmng/view";

	@Autowired
	private CoWorkMngBiz biz;

	@Autowired
	private CommonCodeService cmmCodeService;

	// 메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	// 사용자별 jqGrid 권한 처리로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	// 공통코드 처리 하는 부분 --- start ---
	// 조직 (코드)
	@ModelAttribute("organization")
	public Map<Long, String> getOrganization() {
		return cmmCodeService.getOrganization();
	}

	// 파트 (코드)
	@ModelAttribute("part")
	public Map<Long, String> getPart() {
		return cmmCodeService.getPart();
	}

	// 직급 (코드)
	@ModelAttribute("position")
	public Map<Long, String> getPosition() {
		return cmmCodeService.getPosition();
	}

	// 상태
	@ModelAttribute("status")
	public Map<Long, String> getStatus() {
		return cmmCodeService.getStatus();
	}

	// 상주여부
	@ModelAttribute("indoor")
	public Map<Long, String> getIndoor() {
		return cmmCodeService.getIndoor();
	}

	// PC제공
	@ModelAttribute("computer")
	public Map<Long, String> getComputer() {
		return cmmCodeService.getComputer();
	}

	// LG CNS 보안각서 적용여부
	@ModelAttribute("memorandum")
	public Map<Long, String> getMemorandum() {
		return cmmCodeService.getMemorandum();
	}

	// 공통코드 처리 하는 부분 --- end ---

	/**
	 * @SpecialLogic 협력업체 인원관리 등록
	 * 
	 * @param vo
	 * @param result
	 * @param attr
	 * @return
	 */
	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(@ModelAttribute(FORM_VO) @Valid CoWorkMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		// 필수값 체크
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		// 등록처리
		biz.create(vo);
		// 페이지 Redirect
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 수정
	 * 
	 * @param coWorkMngCmdVO
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId,
			@PathVariable(PageParams.ID) Long id, Model model) {
		CoWorkMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId, Model model) {
		CoWorkMngFrmVO vo = new CoWorkMngFrmVO(page, rowNum, rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid CoWorkMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = VIEW_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId,
			@PathVariable(PageParams.ID) Long id, Model model) {
		CoWorkMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return VIEW_PAGE;
	}
}
