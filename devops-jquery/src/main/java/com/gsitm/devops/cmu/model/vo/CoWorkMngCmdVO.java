package com.gsitm.devops.cmu.model.vo;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class CoWorkMngCmdVO extends CoWorkMngWebVO {

	/**
	 * 조직
	 */
	private Code organization;

	/**
	 * 이름
	 */
	@NotBlank
	private String name;

	/**
	 * 협력직원
	 */
	private Account account;
	private String accountName;
	private String accountEmail;

	/**
	 * 파트
	 */
	private Code part;

	/**
	 * 프로젝트
	 */
	private Project project;
	private String projectName;

	/**
	 * 직급
	 */
	private Code position;

	/**
	 * 이메일
	 */
	private String email;

	/**
	 * 업체명
	 */
	private String companyName;

	/**
	 * 투입시작일
	 */
	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	/**
	 * 투입종료일
	 */
	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate endDate;

	/**
	 * 상태
	 */
	@NotNull
	private Code status;

	/**
	 * 투입목적
	 */
	private String purpose;

	/**
	 * 전화번호
	 */
	private String phone;

	/**
	 * 핸드폰번호
	 */
	private String mobile;

	/**
	 * 주소
	 */
	private String address;

	/**
	 * 상주여부
	 */
	@NotNull
	private Code indoor;

	/**
	 * PC제공여부
	 */
	@NotNull
	private Code computer;

	/**
	 * IP
	 */
	private String ip;

	/**
	 * 보안각서징구일자(GS리테일) 투입시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate clientMemorandumStartDate;

	/**
	 * 보안각서징구일자(GS리테일) 철수시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate clientMemorandumEndDate;

	/**
	 * 보안체크리스트징구일자(공통) 투입시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate checklistStartDate;

	/**
	 * 보안체크리스트징구일자(공통) 철수시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate checklistEndDate;

	/**
	 * 보안각서징구일자(LG CNS) 대상여부
	 */
	private Code memorandum;

	/**
	 * 보안각서징구일자(LG CNS) 투입시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate memorandumStartDate;

	/**
	 * 보안각서징구일자(LG CNS) 철수시
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate memorandumEndDate;

	/**
	 * 출입카드 제공일자
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate givedDate;

	/**
	 * 출입카드 회수일자
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate callbackDate;

	/**
	 * 출입카드 총무팀반환일자
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate returnedDate;

	/**
	 * 보안교육일자
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate orientationDate;

	/**
	 * 보안Tool설치일자
	 */
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate installedDate;

	/**
	 * 비고
	 */
	private String notes;
}
