package com.gsitm.devops.cmu.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;

@Repository
public interface ProjectWeeklyReportRepository extends
		SharedRepository<ProjectWeeklyReport, Long> {
}
