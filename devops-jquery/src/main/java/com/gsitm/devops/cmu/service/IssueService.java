package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.Issue;

public interface IssueService extends SharedService<Issue, Long> {
}
