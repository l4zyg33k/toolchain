package com.gsitm.devops.cmu.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmu.model.vo.IsueMngCmdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngFrmVO;
import com.gsitm.devops.cmu.service.biz.IsueMngBiz;

@Controller
public class IsueMngController {

	private static final String MENU_CODE = "IsueMng";
	private static final String FORM_VO = "isueMngFrmVO";

	private static final String INDEX_URL = "/cmu/isuemng";
	private static final String ACTION_URL = "/cmu/isuemng/*";
	private static final String FORM_URL = "/cmu/isuemng/form";
	private static final String FORM_URL_BY_ID = "/cmu/isuemng/{id}/form";
	private static final String VIEW_URL_BY_ID = "/cmu/isuemng/{id}/view";
	private static final String REDIRECT_URL = "redirect:/cmu/isuemng?page={page}&rowNum={rowNum}&rowId={rowId}";

	private static final String INDEX_PAGE = "cmu/isuemng/index";
	private static final String FORM_PAGE = "cmu/isuemng/form";
	private static final String VIEW_PAGE = "cmu/isuemng/view";

	@Autowired
	private IsueMngBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("categories")
	public Map<Long, String> getCategories() {
		return biz.getCategories();
	}

	@ModelAttribute("statuses")
	public Map<Long, String> getStatuses() {
		return biz.getStatuses();
	}

	@ModelAttribute("importances")
	public Map<Long, String> getImportances() {
		return biz.getImportances();
	}

	@ModelAttribute("urgencies")
	public Map<Long, String> getUrgencies() {
		return biz.getUrgencies();
	}

	@ModelAttribute("status")
	public String getStatus() {
		return biz.getStatus();
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return "cmu/isuemng/index";
	}

	@RequestMapping(value = FORM_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId, Model model) {
		IsueMngFrmVO isueMngFrmVO = new IsueMngFrmVO(page, rowNum, rowId);
		model.addAttribute("isueMngFrmVO", isueMngFrmVO);
		return "cmu/isuemng/form";
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.POST)
	public String create(@ModelAttribute(FORM_VO) @Valid IsueMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.create(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = FORM_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId,
			@PathVariable(PageParams.ID) Long id, Model model) {
		IsueMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}

	@RequestMapping(value = ACTION_URL, method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid IsueMngCmdVO vo,
			BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(vo);
		attr.addAttribute(PageParams.PAGE, vo.getPage());
		attr.addAttribute(PageParams.ROW_NUM, vo.getRowNum());
		attr.addAttribute(PageParams.ROW_ID, vo.getRowId());
		return REDIRECT_URL;
	}

	@RequestMapping(value = VIEW_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId,
			@PathVariable(PageParams.ID) long id, Model model) {
		IsueMngFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return VIEW_PAGE;
	}
}
