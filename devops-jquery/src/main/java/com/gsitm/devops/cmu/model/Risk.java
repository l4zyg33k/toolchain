/**
 * 리스크 마스터 정보
 * Risk.java
 * @author Administrator
 */
package com.gsitm.devops.cmu.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Risk extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 리스크 제목
	 */
	@Column(length = 64)
	private String title;

	/**
	 * 리스크 발생일
	 */
	@Column
	private LocalDate occurDate;

	/**
	 * 리스크 범주 (코드)
	 */
	@ManyToOne
	private Code category;

	/**
	 * 리스크 상태 (코드)
	 */
	@ManyToOne
	private Code status;

	/**
	 * 리스크 영향도 (코드)
	 */
	@ManyToOne
	private Code inspect;

	/**
	 * 접근 방안 (코드)
	 */
	@ManyToOne
	private Code approchPlan;

	/**
	 * 리스크 발생 확률 (0~100%)
	 */
	private Integer occurRate;

	/**
	 * 내용
	 */
	@Column(length = 2048)
	private String content;

	/**
	 * 영향
	 */
	@Column(length = 2048)
	private String impact;

	/**
	 * 완화 계획
	 */
	@Column(length = 2048)
	private String relaxPlan;

	/**
	 * 비상 대처 방안
	 */
	@Column(length = 2048)
	private String emergencyPlan;

	/**
	 * 리스크 상세
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<RiskDetail> details;

	/**
	 * 첨부 파일
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<AttachmentFile> attachments;
}
