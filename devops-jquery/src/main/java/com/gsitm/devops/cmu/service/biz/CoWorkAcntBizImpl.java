package com.gsitm.devops.cmu.service.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Setter;

import org.jadira.usertype.spi.utils.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.MessageSource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.QRole;
import com.gsitm.devops.adm.model.Role;
import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.adm.model.vo.QAcntMngGrdVO;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
public class CoWorkAcntBizImpl implements CoWorkAcntBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RoleService roleService;

	@Autowired
	private CodeService codeService;

	@Autowired
	private AccountService accountService;

	@Override
	@CacheEvict(value = "userCache", key = "#vo.username")
	public String delete(AcntMngFrmVO vo) {
		accountService.delete(vo.getId());
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String create(AcntMngFrmVO vo) {
		Account found = accountService.findOne(vo.getUsername());
		if (found != null) {
			throw new DuplicateKeyException("아이디가 존재 합니다.");
		}
		Account account = new Account();
		if (StringUtils.isNotEmpty(vo.getNewPassword())) {
			account.setPassword(vo.getNewPassword());
		}
		BeanUtils.copyProperties(vo, account);
		account.setTeam(getTeam());
		account.setAuthorities(getAuthorities());
		accountService.save(account);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	@CacheEvict(value = "userCache", key = "#vo.username")
	public String update(AcntMngFrmVO vo) {
		Account account = accountService.findOne(vo.getUsername());
		if (StringUtils.isNotEmpty(vo.getNewPassword())) {
			account.setPassword(vo.getNewPassword());
		}
		BeanUtils.copyProperties(vo, account, "team", "authorities");
		accountService.save(account);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable) {
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		QRole role = new QRole("role");
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team)
				.leftJoin(account.authorities, role)
				.where(team.value.eq("IG"), role.authority.eq("ROLE_DEV"));
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Account>(
				Account.class, "account"));
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		QRole role = new QRole("role");
		PathBuilder<Account> path = new PathBuilder<Account>(Account.class,
				"account");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em)
				.from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team)
				.leftJoin(account.authorities, role)
				.where(team.value.eq("IG"), role.authority.eq("ROLE_DEV"),
						predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public String getPositions() {
		Map<Long, String> results = codeService.getOptions("A1");
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	private List<Role> getAuthorities() {
		QRole role = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(role).where(
				role.authority.in("ROLE_ANONYMOUS", "ROLE_USER", "ROLE_DEV"));
		return query.list(role);
	}

	private Code getTeam() {
		return codeService.getCode("IG", "A2");
	}
}
