package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.IssueDetail;

public interface IssueDetailService extends
		SharedService<IssueDetail, Long> {
}
