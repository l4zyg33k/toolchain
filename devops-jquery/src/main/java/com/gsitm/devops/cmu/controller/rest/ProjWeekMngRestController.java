package com.gsitm.devops.cmu.controller.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngCmdVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngGrdVO;
import com.gsitm.devops.cmu.service.biz.ProjWeekMngBiz;

@RestController
public class ProjWeekMngRestController {

	@Autowired
	private ProjWeekMngBiz biz;

	@RequestMapping(value = "/rest/cmu/projweekmng", method = RequestMethod.POST)
	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable,
			@AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, user);
	}

	@RequestMapping(value = "/rest/cmu/projweekmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable,
			Searchable searchable, @AuthenticationPrincipal UserDetails user) {
		return biz.findAll(pageable, searchable, user);
	}

	@RequestMapping(value = "/rest/cmu/projweekmng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid ProjWeekMngCmdVO projWeekMngCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (projWeekMngCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (projWeekMngCmdVO.getOper()) {
		case "del":
			biz.delete(projWeekMngCmdVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/cmu/projweekmng/{id}/attachments", method = RequestMethod.POST)
	public Page<AtmtFileVO> getAttachments(@PathVariable("id") Long id,
			Pageable pageable) {
		return biz.getAttachments(id, pageable);
	}

	@RequestMapping(value = "/rest/cmu/projweekmng/*/attachments", method = RequestMethod.GET)
	public void download(@RequestParam("id") Long id,
			HttpServletRequest request, HttpServletResponse response) {
		biz.download(id, request, response);
	}

	@RequestMapping(value = "/rest/cmu/projweekmng/{id}/attachments/edit", method = RequestMethod.POST)
	public JqResult removeAttachment(@PathVariable("id") Long id,
			@Valid AtmtFileVO atmtFileVO, BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (atmtFileVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (atmtFileVO.getOper()) {
		case "del":
			biz.removeAttachmentFile(id, atmtFileVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/cmu/projweekmng/{id}/attachments/upload", method = RequestMethod.POST)
	public JqResult addAttachment(@PathVariable("id") Long id,
			MultipartHttpServletRequest request) {
		biz.addAttachmentFile(id, request);
		return new JqSuccess();
	}
}
