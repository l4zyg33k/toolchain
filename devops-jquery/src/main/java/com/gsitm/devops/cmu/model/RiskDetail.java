/**
 * 리스크 디테일 정보
 * RiskDetail.java
 * @author Administrator
 */
package com.gsitm.devops.cmu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class RiskDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 변경 일자
	 */
	private LocalDate changedDate;

	/**
	 * 상태
	 */
	@ManyToOne
	private Code status;

	/**
	 * 리스크 점검 및 변경내용
	 */
	@Column(length = 2048)
	private String content;
}
