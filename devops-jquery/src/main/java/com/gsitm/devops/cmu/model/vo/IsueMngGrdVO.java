package com.gsitm.devops.cmu.model.vo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IsueMngGrdVO {

	private Long id;

	@JsonProperty("project.code")
	private String projectCode;

	@JsonProperty("project.name")
	private String projectName;

	private String title;

	@JsonProperty("project.il.name")
	private String projectIlName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate occurrenceDate;

	@JsonProperty("category.name")
	private String categoryName;

	@JsonProperty("status.name")
	private String statusName;

	@JsonProperty("importance.name")
	private String importanceName;

	@JsonProperty("urgency.name")
	private String urgencyName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate planResolveDate;

	private Boolean editable;

	@QueryProjection
	public IsueMngGrdVO(Long id, String projectCode, String projectName,
			String title, String projectIlName, LocalDate occurrenceDate,
			String categoryName, String statusName, String importanceName,
			String urgencyName, LocalDate planResolveDate) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.title = title;
		this.projectIlName = projectIlName;
		this.occurrenceDate = occurrenceDate;
		this.categoryName = categoryName;
		this.statusName = statusName;
		this.importanceName = importanceName;
		this.urgencyName = urgencyName;
		this.planResolveDate = planResolveDate;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public IsueMngGrdVO(Long id, String projectCode, String projectName,
			String title, String projectIlName, LocalDate occurrenceDate,
			String categoryName, String statusName, String importanceName,
			String urgencyName, LocalDate planResolveDate, Boolean editable) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.title = title;
		this.projectIlName = projectIlName;
		this.occurrenceDate = occurrenceDate;
		this.categoryName = categoryName;
		this.statusName = statusName;
		this.importanceName = importanceName;
		this.urgencyName = urgencyName;
		this.planResolveDate = planResolveDate;
		this.editable = editable;
	}
}
