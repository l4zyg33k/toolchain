package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtFrmVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtGrdVO;

public interface ProjWeekInvtBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public ProjWeekInvtFrmVO findOne(Long id);
	
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable, UserDetails user, Long teamCode, LocalDate fromReportDate, LocalDate toReportDate);
	
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, Long teamCode, LocalDate fromReportDate, LocalDate toReportDate);
	
	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable);
}
