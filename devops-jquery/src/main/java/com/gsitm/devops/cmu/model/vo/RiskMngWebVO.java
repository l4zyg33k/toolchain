package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class RiskMngWebVO extends SharedWebVO<Long> {

	private String rowData;

	public RiskMngWebVO() {
		super();
	}

	public RiskMngWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/cmu/riskmng" : String.format("/cmu/riskmng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/cmu/riskmng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getDetailsUrl() {
		return String.format("/rest/cmu/riskmng/%d/details", getId());
	}

	public String getDetailsEditUrl() {
		return String.format("/rest/cmu/riskmng/%d/details/edit", getId());
	}

	public String getAttachmentsUrl() {
		return String.format("/rest/cmu/riskmng/%d/attachments", getId());
	}

	public String getAttachmentsEditUrl() {
		return String.format("/rest/cmu/riskmng/%d/attachments/edit", getId());
	}

	public String getAttachmentsUploadUrl() {
		return String
				.format("/rest/cmu/riskmng/%d/attachments/upload", getId());
	}
}
