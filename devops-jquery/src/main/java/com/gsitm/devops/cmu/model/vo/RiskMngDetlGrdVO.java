package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class RiskMngDetlGrdVO {

	private Long id;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate changedDate;

	private Long status;

	private String content;

	public RiskMngDetlGrdVO() {
		super();
	}

	@QueryProjection
	public RiskMngDetlGrdVO(Long id, LocalDate changedDate, Long status,
			String content) {
		super();
		this.id = id;
		this.changedDate = changedDate;
		this.status = status;
		this.content = content;
	}
}
