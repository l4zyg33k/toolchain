package com.gsitm.devops.cmu.model.vo;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class IsueMngCmdVO extends IsueMngWebVO {

	private Project project;

	@NotBlank
	private String projectName;

	@NotBlank
	private String title;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate occurrenceDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate planResolveDate;

	@NotNull
	private Code category;

	@NotNull
	private Code status;

	@NotNull
	private Code importance;

	@NotNull
	private Code urgency;

	private String content;

	private String impact;

	private String actionPlan;

	private String projectIlName;

	private List<MultipartFile> files;
}
