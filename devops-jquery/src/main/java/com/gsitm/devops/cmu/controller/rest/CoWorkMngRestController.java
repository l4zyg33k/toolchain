package com.gsitm.devops.cmu.controller.rest;

import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.CoWorkMngCmdVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngGrdVO;
import com.gsitm.devops.cmu.service.biz.CoWorkMngBiz;

@RestController
public class CoWorkMngRestController {

	@Autowired
	private CoWorkMngBiz biz;

	/**
	 * @SpecialLogic 협력업체 인원관리 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param startDate
	 *            : 투입일자
	 * @param endDate
	 *            : 철수 일자
	 * @return Page<CoWorkMngGrdVO>
	 */
	@RequestMapping(value = "/rest/cmu/coworkmng", method = RequestMethod.POST)
	public Page<CoWorkMngGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
		return biz.findAll(pageable, user, startDate, endDate);
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param user
	 *            : 사용자 정보
	 * @param startDate
	 *            : 투입일자
	 * @param endDate
	 *            : 철수 일자
	 * @return Page<CoWorkMngGrdVO>
	 */
	@RequestMapping(value = "/rest/cmu/coworkmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<CoWorkMngGrdVO> findAll(
			Pageable pageable,
			Searchable searchable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
		return biz.findAll(pageable, searchable, user, startDate, endDate);
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 삭제
	 * 
	 * @param oper
	 * @param coWorkMngCmdVO
	 * @param result
	 */
	@RequestMapping(value = "/rest/cmu/coworkmng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid CoWorkMngCmdVO coWorkMngCmdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (coWorkMngCmdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (coWorkMngCmdVO.getOper()) {
		case "del":
			biz.delete(coWorkMngCmdVO);
			break;
		}

		return new JqSuccess();
	}
}
