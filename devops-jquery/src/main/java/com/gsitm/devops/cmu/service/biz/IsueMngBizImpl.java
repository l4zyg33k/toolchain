package com.gsitm.devops.cmu.service.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QAttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.adm.model.vo.QAtmtFileVO;
import com.gsitm.devops.adm.service.AttachmentFileService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.cmu.model.Issue;
import com.gsitm.devops.cmu.model.IssueDetail;
import com.gsitm.devops.cmu.model.QIssue;
import com.gsitm.devops.cmu.model.QIssueDetail;
import com.gsitm.devops.cmu.model.vo.IsueMngCmdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngDetlCmdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngFrmVO;
import com.gsitm.devops.cmu.model.vo.IsueMngGrdVO;
import com.gsitm.devops.cmu.model.vo.QIsueMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.QIsueMngFrmVO;
import com.gsitm.devops.cmu.model.vo.QIsueMngGrdVO;
import com.gsitm.devops.cmu.service.IssueDetailService;
import com.gsitm.devops.cmu.service.IssueService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class IsueMngBizImpl implements IsueMngBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private IssueService iService;

	@Autowired
	private IssueDetailService idService;

	@Autowired
	private AttachmentFileService afService;

	@Override
	public void addAttachmentFile(Long id, MultipartHttpServletRequest request) {
		Issue issue = iService.findOne(id);
		Iterator<String> fileNames = request.getFileNames();
		while (fileNames.hasNext()) {
			MultipartFile file = request.getFile((String) fileNames.next());
			AttachmentFile attachment = afService.save(getContainer(), file);
			issue.getAttachments().add(attachment);
		}
		iService.save(issue);
	}

	@Override
	public void create(IsueMngDetlCmdVO isueMngDetlFrmVO, Long id) {
		IssueDetail detail = new IssueDetail();
		BeanUtils.copyProperties(isueMngDetlFrmVO, detail);
		detail = idService.save(detail);
		Issue issue = iService.findOne(id);
		issue.getDetails().add(detail);
		iService.save(issue);
	}

	@Override
	public void create(IsueMngCmdVO isueMngCmdVO) {
		Issue issue = new Issue();
		BeanUtils.copyProperties(isueMngCmdVO, issue);
		List<IssueDetail> details = getDetails(isueMngCmdVO);
		List<AttachmentFile> attachments = getAttachments(isueMngCmdVO);
		issue.setDetails(details);
		issue.setAttachments(attachments);
		iService.save(issue);
	}

	@Override
	public void delete(IsueMngDetlCmdVO isueMngDetlFrmVO, Long id) {
		IssueDetail detail = idService.findOne(isueMngDetlFrmVO.getId());
		Issue issue = iService.findOne(id);
		issue.getDetails().remove(detail);
		iService.save(issue);
		idService.delete(detail);
	}

	@Override
	public void delete(IsueMngCmdVO isueMngCmdVO) {
		iService.delete(isueMngCmdVO.getId());
	}

	@Override
	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response) {
		afService.download(id, request, response);
	}

	@Override
	public Page<IsueMngDetlGrdVO> findAll(Pageable pageable, Long id) {
		QIssue issue = QIssue.issue;
		QIssueDetail issueDetail = QIssueDetail.issueDetail;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(issue)
				.innerJoin(issue.details, issueDetail).where(issue.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<IsueMngDetlGrdVO> search = query
				.listResults(new QIsueMngDetlGrdVO(issueDetail.id,
						issueDetail.changedDate, issueDetail.status.id,
						issueDetail.content));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<IsueMngDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id) {
		QIssue issue = QIssue.issue;
		QIssueDetail issueDetail = QIssueDetail.issueDetail;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(issue)
				.innerJoin(issue.details, issueDetail)
				.where(issue.id.eq(id), predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<IsueMngDetlGrdVO> search = query
				.listResults(new QIsueMngDetlGrdVO(issueDetail.id,
						issueDetail.changedDate, issueDetail.status.id,
						issueDetail.content));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<IsueMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			UserDetails user) {
		QIssue issue = QIssue.issue;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(issue).where(predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<IsueMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QIsueMngGrdVO(issue.id,
					issue.project.code, issue.project.name, issue.title,
					issue.project.il.name, issue.occurrenceDate,
					issue.category.name, issue.status.name,
					issue.importance.name, issue.urgency.name,
					issue.planResolveDate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(issue.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(issue.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QIsueMngGrdVO(issue.id,
					issue.project.code, issue.project.name, issue.title,
					issue.project.il.name, issue.occurrenceDate,
					issue.category.name, issue.status.name,
					issue.importance.name, issue.urgency.name,
					issue.planResolveDate, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<IsueMngGrdVO> findAll(Pageable pageable, UserDetails user) {
		QIssue issue = QIssue.issue;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(issue);
		querydsl.applyPagination(pageable, query);
		SearchResults<IsueMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QIsueMngGrdVO(issue.id,
					issue.project.code, issue.project.name, issue.title,
					issue.project.il.name, issue.occurrenceDate,
					issue.category.name, issue.status.name,
					issue.importance.name, issue.urgency.name,
					issue.planResolveDate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(issue.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(issue.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QIsueMngGrdVO(issue.id,
					issue.project.code, issue.project.name, issue.title,
					issue.project.il.name, issue.occurrenceDate,
					issue.category.name, issue.status.name,
					issue.importance.name, issue.urgency.name,
					issue.planResolveDate, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public IsueMngFrmVO findOne(Long id) {
		QIssue issue = QIssue.issue;
		QAccount il = QAccount.account;
		JPQLQuery query = new JPAQuery(em).from(issue)
				.innerJoin(issue.project.il, il).where(issue.id.eq(id));
		return query.uniqueResult(new QIsueMngFrmVO(issue.id,
				issue.project.code, issue.project.name, issue.title,
				issue.occurrenceDate, issue.planResolveDate, issue.category.id,
				issue.category.name, issue.status.id, issue.status.name,
				issue.importance.id, issue.importance.name, issue.urgency.id,
				issue.urgency.name, issue.content, issue.impact,
				issue.actionPlan, il.name));
	}

	private List<AttachmentFile> getAttachments(IsueMngCmdVO isueMngCmdVO) {
		List<AttachmentFile> attachments = new ArrayList<>();
		List<MultipartFile> files = isueMngCmdVO.getFiles();
		if (CollectionUtils.isEmpty(files) == false) {
			attachments = afService.save(getContainer(), files);
		}
		return attachments;
	}

	@Override
	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable) {
		QIssue issue = QIssue.issue;
		QAttachmentFile attachment = QAttachmentFile.attachmentFile;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(issue)
				.innerJoin(issue.attachments, attachment)
				.where(issue.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<AtmtFileVO> search = query.listResults(new QAtmtFileVO(
				attachment.id, attachment.fileName, attachment.fileSize,
				attachment.createdDate, attachment.createdBy.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Map<Long, String> getCategories() {
		return codeService.getOptions("A0");
	}

	private Code getContainer() {
		QCode code = QCode.code;
		return codeService.findOne(code.value.eq("01").and(
				code.parent.value.eq("A3")));
	}

	private List<IssueDetail> getDetails(IsueMngCmdVO isueMngCmdVO) {
		List<IssueDetail> details = new ArrayList<>();
		List<IsueMngDetlGrdVO> vos;
		try {
			vos = Arrays.asList(mapper.readValue(isueMngCmdVO.getRowData(),
					IsueMngDetlGrdVO[].class));
			for (IsueMngDetlGrdVO vo : vos) {
				IssueDetail detail = new IssueDetail();
				detail.setChangedDate(vo.getChangedDate());
				detail.setStatus(codeService.findOne(vo.getStatus()));
				detail.setContent(vo.getContent());
				details.add(detail);
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return details;
	}

	@Override
	public Map<Long, String> getImportances() {
		return codeService.getOptions("C0");
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public String getStatus() {
		return Joiner.on(";").withKeyValueSeparator(":").join(getStatuses());
	}

	@Override
	public Map<Long, String> getStatuses() {
		return codeService.getOptions("B0");
	}

	@Override
	public Map<Long, String> getUrgencies() {
		return codeService.getOptions("D0");
	}

	@Override
	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO) {
		Issue issue = iService.findOne(id);
		AttachmentFile attachment = afService.findOne(atmtFileVO.getId());
		issue.getAttachments().remove(attachment);
		iService.save(issue);
		afService.delete(attachment);
	}

	@Override
	public void update(IsueMngDetlCmdVO isueMngDetlFrmVO) {
		IssueDetail detail = idService.findOne(isueMngDetlFrmVO.getId());
		BeanUtils.copyProperties(isueMngDetlFrmVO, detail);
		idService.save(detail);
	}

	@Override
	public void update(IsueMngCmdVO isueMngCmdVO) {
		Issue issue = iService.findOne(isueMngCmdVO.getId());
		BeanUtils.copyProperties(isueMngCmdVO, issue);
		iService.save(issue);
	}

	@Override
	public Page<IsueMngGrdVO> findAll(Pageable pageable, String projectCode) {
		QIssue issue = QIssue.issue;
		PathBuilder<Issue> builder = new PathBuilder<Issue>(Issue.class,
				"issue");
		Querydsl querydsl = new Querydsl(em, builder);

		// 조회조건 - 프로젝트 코드
		BooleanBuilder conditions = new BooleanBuilder();
		if (projectCode != null) {
			conditions.and(issue.project.code.eq(projectCode));
		}

		JPQLQuery query = new JPAQuery(em).from(issue).where(conditions);

		querydsl.applyPagination(pageable, query);
		SearchResults<IsueMngGrdVO> search = query
				.listResults(new QIsueMngGrdVO(issue.id, issue.project.code,
						issue.project.name, issue.title, issue.project.il.name,
						issue.occurrenceDate, issue.category.name,
						issue.status.name, issue.importance.name,
						issue.urgency.name, issue.planResolveDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
