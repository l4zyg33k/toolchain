package com.gsitm.devops.cmu.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.cmu.model.CoWorker;

@Repository
public interface CoWorkerRepository extends
		SharedRepository<CoWorker, Long> {
}
