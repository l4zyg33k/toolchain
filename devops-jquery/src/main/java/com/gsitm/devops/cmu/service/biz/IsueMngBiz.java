package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.IsueMngCmdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngDetlCmdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.IsueMngFrmVO;
import com.gsitm.devops.cmu.model.vo.IsueMngGrdVO;

public interface IsueMngBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public IsueMngFrmVO findOne(Long id);

	public Page<IsueMngGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<IsueMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			UserDetails user);

	public void create(IsueMngCmdVO isueMngCmdVO);

	public void update(IsueMngCmdVO isueMngCmdVO);

	public void delete(IsueMngCmdVO isueMngCmdVO);

	public Page<IsueMngDetlGrdVO> findAll(Pageable pageable, Long id);

	public Page<IsueMngDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id);

	public void create(IsueMngDetlCmdVO isueMngDetlFrmVO, Long id);

	public void update(IsueMngDetlCmdVO isueMngDetlFrmVO);

	public void delete(IsueMngDetlCmdVO isueMngDetlFrmVO, Long id);

	public Map<Long, String> getCategories();

	public Map<Long, String> getStatuses();

	public Map<Long, String> getImportances();

	public Map<Long, String> getUrgencies();

	public String getStatus();

	public void addAttachmentFile(Long id, MultipartHttpServletRequest request);

	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO);

	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable);

	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response);
	
	public Page<IsueMngGrdVO> findAll(Pageable pageable, String projectCode);
}
