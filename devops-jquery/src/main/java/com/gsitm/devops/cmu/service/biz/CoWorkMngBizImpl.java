package com.gsitm.devops.cmu.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.cmu.model.CoWorker;
import com.gsitm.devops.cmu.model.QCoWorker;
import com.gsitm.devops.cmu.model.vo.CoWorkMngCmdVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngFrmVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngGrdVO;
import com.gsitm.devops.cmu.model.vo.QCoWorkMngFrmVO;
import com.gsitm.devops.cmu.model.vo.QCoWorkMngGrdVO;
import com.gsitm.devops.cmu.service.CoWorkerService;
import com.gsitm.devops.pms.model.QProject;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class CoWorkMngBizImpl implements CoWorkMngBiz {

	// 메시지 처리
	private MessageSource messageSource;

	@Autowired
	private CoWorkerService coWorkerService;

	@PersistenceContext
	private EntityManager em;

	/**
	 * @SpecialLogic 협력업체 인원관리 등록 처리
	 * 
	 * @param CoWorkMngCmdVO
	 */
	@Override
	public void create(CoWorkMngCmdVO coWorkMngCmdVO) {
		CoWorker coWorker = new CoWorker();
		BeanUtils.copyProperties(coWorkMngCmdVO, coWorker);

		// 저장 처리
		coWorkerService.save(coWorker);

		// 처리결과 메시지 전송
		// return messageSource.getMessage("save.success", new Object[] {},
		// Locale.getDefault());
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 삭제 처리
	 * 
	 * @param CoWorkMngCmdVO
	 */
	@Override
	public void delete(CoWorkMngCmdVO coWorkMngCmdVO) {
		// 삭제처리
		coWorkerService.delete(coWorkMngCmdVO.getId());

		// 처리결과 메시지 전송
		// return messageSource.getMessage("remove.success", new Object[] {},
		// Locale.getDefault());
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param user
	 *            : 사용자 정보
	 * @param startDate
	 *            : 투입일자
	 * @param endDate
	 *            : 철수 일자
	 * @return Page<CoWorkMngGrdVO>
	 */
	@Override
	public Page<CoWorkMngGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, LocalDate startDate,
			LocalDate endDate) {
		QCoWorker cw = QCoWorker.coWorker;

		QCode organization = new QCode("organization"); // 조직
		QAccount account = new QAccount("account"); // 계정
		QCode part = new QCode("part"); // 파트 (코드)
		QProject project = new QProject("project"); // 프로젝트
		QCode position = new QCode("position"); // 직급 (코드)
		QCode status = new QCode("status"); // 상태 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		// 조회조건 추가 - 투입일, 철수일
		BooleanBuilder conditions = new BooleanBuilder();
		if (startDate != null) {
			conditions.and(cw.startDate.goe(startDate));
		}
		if (endDate != null) {
			conditions.and(cw.endDate.loe(endDate));
		}

		// 변경권한에서 사용하기 위해서..
		String username = user.getUsername();

		PathBuilder<CoWorker> path = new PathBuilder<CoWorker>(CoWorker.class,
				"coWorker");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(cw)
				.leftJoin(cw.organization, organization)
				.leftJoin(cw.account, account).leftJoin(cw.part, part)
				.leftJoin(cw.project, project).leftJoin(cw.position, position)
				.leftJoin(cw.status, status).leftJoin(cw.createdBy, createdBy)
				.where(predicate, conditions);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<CoWorkMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QCoWorkMngGrdVO(cw.id,
					organization.name, account.name, position.name,
					cw.companyName, part.name, project.name, cw.startDate,
					cw.endDate, status.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(account.username.eq(username)).then(true)
					.when(createdBy.username.eq(username)).then(true)
					.otherwise(false);

			search = query.listResults(new QCoWorkMngGrdVO(cw.id,
					organization.name, account.name, position.name,
					cw.companyName, part.name, project.name, cw.startDate,
					cw.endDate, status.name, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param startDate
	 *            : 투입일자
	 * @param endDate
	 *            : 철수 일자
	 * @return Page<CoWorkMngGrdVO>
	 */
	@Override
	public Page<CoWorkMngGrdVO> findAll(Pageable pageable, UserDetails user,
			LocalDate startDate, LocalDate endDate) {
		QCoWorker cw = QCoWorker.coWorker;

		QCode organization = new QCode("organization"); // 조직
		QAccount account = new QAccount("account"); // 계정
		QCode part = new QCode("part"); // 파트 (코드)
		QProject project = new QProject("project"); // 프로젝트
		QCode position = new QCode("position"); // 직급 (코드)
		QCode status = new QCode("status"); // 상태 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		// 조회조건 추가 - 투입일, 철수일
		BooleanBuilder conditions = new BooleanBuilder();
		if (startDate != null) {
			conditions.and(cw.startDate.goe(startDate));
		}
		if (endDate != null) {
			conditions.and(cw.endDate.loe(endDate));
		}

		// 변경권한에서 사용하기 위해서..
		String username = user.getUsername();

		JPQLQuery query = new JPAQuery(em).from(cw)
				.leftJoin(cw.organization, organization)
				.leftJoin(cw.account, account).leftJoin(cw.part, part)
				.leftJoin(cw.project, project).leftJoin(cw.position, position)
				.leftJoin(cw.status, status).leftJoin(cw.createdBy, createdBy)
				.where(conditions);

		Querydsl querydsl = new Querydsl(em, new PathBuilder<CoWorker>(
				CoWorker.class, "coWorker"));
		querydsl.applyPagination(pageable, query);

		SearchResults<CoWorkMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QCoWorkMngGrdVO(cw.id,
					organization.name, account.name, position.name,
					cw.companyName, part.name, project.name, cw.startDate,
					cw.endDate, status.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(account.username.eq(username)).then(true)
					.when(createdBy.username.eq(username)).then(true)
					.otherwise(false);

			search = query.listResults(new QCoWorkMngGrdVO(cw.id,
					organization.name, account.name, position.name,
					cw.companyName, part.name, project.name, cw.startDate,
					cw.endDate, status.name, editable));
		}

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 단건 조회로 수정이나 보기화면 데이터 생성
	 * 
	 * @param id
	 * @return CoWorkMngFrmVO
	 */
	@Override
	public CoWorkMngFrmVO findOne(Long id) {
		QCoWorker cw = QCoWorker.coWorker;

		QCode organization = new QCode("organization"); // 조직
		QAccount account = new QAccount("account"); // 계정
		QCode part = new QCode("part"); // 파트 (코드)
		QProject project = new QProject("project"); // 프로젝트
		QCode position = new QCode("position"); // 직급 (코드)
		QCode status = new QCode("status"); // 상태 (코드)
		QAccount createdBy = new QAccount("createdBy"); // 생성자

		JPQLQuery query = new JPAQuery(em).from(cw)
				.leftJoin(cw.organization, organization)
				.leftJoin(cw.account, account).leftJoin(cw.part, part)
				.leftJoin(cw.project, project).leftJoin(cw.position, position)
				.leftJoin(cw.status, status).leftJoin(cw.createdBy, createdBy)
				.where(cw.id.eq(id));

		return query
				.uniqueResult(new QCoWorkMngFrmVO(cw.id, organization.id,
						cw.name, account.username, account.name, account.email,
						part.id, project.code, project.name, position.id,
						cw.email, cw.companyName, cw.startDate, cw.endDate,
						status.id, cw.purpose, cw.phone, cw.mobile, cw.address,
						cw.indoor.id, cw.computer.id, cw.ip,
						cw.clientMemorandumStartDate,
						cw.clientMemorandumEndDate, cw.checklistStartDate,
						cw.checklistEndDate, cw.memorandum.id,
						cw.memorandumStartDate, cw.memorandumEndDate,
						cw.givedDate, cw.callbackDate, cw.returnedDate,
						cw.orientationDate, cw.installedDate, cw.notes));
	}

	/**
	 * @SpecialLogic 사용자별 jqGrid 권한
	 * 
	 * @param user
	 *            : 사용자 정보
	 * @return Map<String, Boolean>
	 */
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	/**
	 * @SpecialLogic 협력업체 인원관리 수정 처리
	 * 
	 * @param CoWorkMngCmdVO
	 */
	@Override
	public void update(CoWorkMngCmdVO coWorkMngCmdVO) {
		CoWorker coWorker = coWorkerService.findOne(coWorkMngCmdVO.getId());
		BeanUtils.copyProperties(coWorkMngCmdVO, coWorker);

		// 저장 처리
		coWorkerService.save(coWorker);

		// 처리결과 메시지 전송
		// return messageSource.getMessage("save.success", new Object[] {},
		// Locale.getDefault());
	}
}
