package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.RiskMngCmdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngDetlCmdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngFrmVO;
import com.gsitm.devops.cmu.model.vo.RiskMngGrdVO;

public interface RiskMngBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public RiskMngFrmVO findOne(Long id);

	public Page<RiskMngGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<RiskMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			UserDetails user);

	public void create(RiskMngCmdVO riskMngCmdVO);

	public void update(RiskMngCmdVO riskMngCmdVO);

	public void delete(RiskMngCmdVO riskMngCmdVO);

	public Page<RiskMngDetlGrdVO> findAll(Pageable pageable, Long id);

	public Page<RiskMngDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id);

	public void create(RiskMngDetlCmdVO riskMngDetlFrmVO, Long id);

	public void update(RiskMngDetlCmdVO riskMngDetlFrmVO);

	public void delete(RiskMngDetlCmdVO riskMngDetlFrmVO, Long id);

	public Map<Long, String> getCategories();

	public Map<Long, String> getStatuses();

	public Map<Long, String> getInspectes();

	public Map<Long, String> getApprochPlans();

	public String getStatus();

	public void addAttachmentFile(Long id, MultipartHttpServletRequest request);

	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO);

	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable);

	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response);
	
	public Page<RiskMngGrdVO> findAll(Pageable pageable, String projectCode);
}
