package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjWeekMngGrdVO {

	private Long id;

	@JsonProperty("project.code")
	private String projectCode;

	private String title;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate reportDate;

	@JsonProperty("reporter.name")
	private String reporterName;

	private Boolean editable;

	@QueryProjection
	public ProjWeekMngGrdVO(Long id, String projectCode, String title,
			LocalDate reportDate, String reporterName) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.title = title;
		this.reportDate = reportDate;
		this.reporterName = reporterName;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public ProjWeekMngGrdVO(Long id, String projectCode, String title,
			LocalDate reportDate, String reporterName, Boolean editable) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.title = title;
		this.reportDate = reportDate;
		this.reporterName = reporterName;
		this.editable = editable;
	}
}
