package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjWeekInvtFrmVO extends ProjWeekInvtWebVO {
	
	private String project;

	private String projectName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate projectStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate projectFinishDate;

	private String title;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate reportDate;

	private String reporter;

	private String reporterName;

	private Long phase;

	private String thisWeek;

	private String nextWeek;

	private Integer planRate;

	private Integer acctualRate;

	public ProjWeekInvtFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}
	
	@QueryProjection
	public ProjWeekInvtFrmVO(Long id, String project, String projectName,
			LocalDate projectStartDate, LocalDate projectFinishDate,
			String title, LocalDate reportDate, String reporter,
			String reporterName, Long phase, String thisWeek, String nextWeek,
			Integer planRate, Integer acctualRate) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.projectStartDate = projectStartDate;
		this.projectFinishDate = projectFinishDate;
		this.title = title;
		this.reportDate = reportDate;
		this.reporter = reporter;
		this.reporterName = reporterName;
		this.phase = phase;
		this.thisWeek = thisWeek;
		this.nextWeek = nextWeek;
		this.planRate = planRate;
		this.acctualRate = acctualRate;
	}
}
