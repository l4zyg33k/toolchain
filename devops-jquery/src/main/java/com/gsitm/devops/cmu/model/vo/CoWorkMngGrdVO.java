package com.gsitm.devops.cmu.model.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class CoWorkMngGrdVO implements Serializable {

	private Long id;

	@JsonProperty("organization.name")
	private String organizationName;

	private String name;

	@JsonProperty("position.name")
	private String positionName;

	private String companyName;

	@JsonProperty("part.name")
	private String partName;

	@JsonProperty("project.name")
	private String projectName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate startDate;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate endDate;

	@JsonProperty("status.name")
	private String statusName;

	private Boolean editable;

	public CoWorkMngGrdVO() {
		super();
	}

	@QueryProjection
	public CoWorkMngGrdVO(Long id, String organizationName, String name,
			String positionName, String companyName, String partName,
			String projectName, LocalDate startDate, LocalDate endDate,
			String statusName) {
		super();
		this.id = id;
		this.organizationName = organizationName;
		this.name = name;
		this.positionName = positionName;
		this.companyName = companyName;
		this.partName = partName;
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.statusName = statusName;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public CoWorkMngGrdVO(Long id, String organizationName, String name,
			String positionName, String companyName, String partName,
			String projectName, LocalDate startDate, LocalDate endDate,
			String statusName, Boolean editable) {
		super();
		this.id = id;
		this.organizationName = organizationName;
		this.name = name;
		this.positionName = positionName;
		this.companyName = companyName;
		this.partName = partName;
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.statusName = statusName;
		this.editable = editable;
	}
}
