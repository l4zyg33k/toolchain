package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class IsueMngWebVO extends SharedWebVO<Long> {

	private String rowData;

	public IsueMngWebVO() {
		super();
	}

	public IsueMngWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/cmu/isuemng" : String.format("/cmu/isuemng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/cmu/isuemng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getDetailsUrl() {
		return String.format("/rest/cmu/isuemng/%d/details", getId());
	}

	public String getDetailsEditUrl() {
		return String.format("/rest/cmu/isuemng/%d/details/edit", getId());
	}

	public String getAttachmentsUrl() {
		return String.format("/rest/cmu/isuemng/%d/attachments", getId());
	}

	public String getAttachmentsEditUrl() {
		return String.format("/rest/cmu/isuemng/%d/attachments/edit", getId());
	}

	public String getAttachmentsUploadUrl() {
		return String
				.format("/rest/cmu/isuemng/%d/attachments/upload", getId());
	}
}
