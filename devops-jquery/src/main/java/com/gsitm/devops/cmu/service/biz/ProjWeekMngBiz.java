package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngCmdVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngFrmVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngGrdVO;

public interface ProjWeekMngBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public ProjWeekMngFrmVO findOne(long id);

	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable, UserDetails user);

	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user);

	public void create(ProjWeekMngCmdVO projWeekMngCmdVO);

	public void update(ProjWeekMngCmdVO projWeekMngCmdVO);

	public void delete(ProjWeekMngCmdVO projWeekMngCmdVO);

	public void addAttachmentFile(Long id, MultipartHttpServletRequest request);

	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO);

	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable);

	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response);

	public Map<Long, String> getPhases();
	
	public Map<String, String> getProjects(UserDetails user);
	
	public AcntItemVO getReporter(UserDetails user);
}
