package com.gsitm.devops.cmu.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmu.service.biz.CoWorkAcntBiz;

@Controller
public class CoWorkAcntMngController {

	private static final String MENU_CODE = "CoWorkAcntMng";

	@Autowired
	private CoWorkAcntBiz biz;

	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute("positions")
	public String getPositions() {
		return biz.getPositions();
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@RequestMapping(value = "/cmu/acntmng", method = RequestMethod.GET)
	public String index() {
		return "cmu/acntmng/index";
	}
}
