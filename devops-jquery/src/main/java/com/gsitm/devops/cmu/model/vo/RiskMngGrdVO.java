package com.gsitm.devops.cmu.model.vo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RiskMngGrdVO {

	private Long id;

	@JsonProperty("project.code")
	private String projectCode;

	@JsonProperty("project.name")
	private String projectName;

	private String title;

	@JsonProperty("project.il.name")
	private String projectIlName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate occurDate;

	@JsonProperty("category.name")
	private String categoryName;

	@JsonProperty("status.name")
	private String statusName;

	@JsonProperty("inspect.name")
	private String inspectName;

	private Integer occurRate;

	private Boolean editable;

	@QueryProjection
	public RiskMngGrdVO(Long id, String projectCode, String projectName,
			String title, String projectIlName, LocalDate occurDate,
			String categoryName, String statusName, String inspectName,
			Integer occurRate) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.title = title;
		this.projectIlName = projectIlName;
		this.occurDate = occurDate;
		this.categoryName = categoryName;
		this.statusName = statusName;
		this.inspectName = inspectName;
		this.occurRate = occurRate;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public RiskMngGrdVO(Long id, String projectCode, String projectName,
			String title, String projectIlName, LocalDate occurDate,
			String categoryName, String statusName, String inspectName,
			Integer occurRate, Boolean editable) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.title = title;
		this.projectIlName = projectIlName;
		this.occurDate = occurDate;
		this.categoryName = categoryName;
		this.statusName = statusName;
		this.inspectName = inspectName;
		this.occurRate = occurRate;
		this.editable = editable;
	}
}
