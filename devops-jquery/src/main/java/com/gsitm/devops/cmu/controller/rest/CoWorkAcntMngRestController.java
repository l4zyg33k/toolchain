package com.gsitm.devops.cmu.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.service.biz.CoWorkAcntBiz;

@RestController
public class CoWorkAcntMngRestController {

	@Autowired
	private CoWorkAcntBiz biz;

	@RequestMapping(value = "/rest/cmu/acntmng", method = RequestMethod.POST)
	public Page<AcntMngGrdVO> findAll(Pageable pageable) {
		return biz.findAll(pageable);
	}

	@RequestMapping(value = "/rest/cmu/acntmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		return biz.findAll(pageable, searchable);
	}

	@RequestMapping(value = "/rest/cmu/acntmng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid AcntMngFrmVO acntMngFrmVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (acntMngFrmVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (acntMngFrmVO.getOper()) {
		case "add":
			try {
				biz.create(acntMngFrmVO);
			} catch (DuplicateKeyException de) {
				return new JqFailure(de.getMessage());
			}
			break;
		case "edit":
			try {
				biz.update(acntMngFrmVO);
			} catch (ObjectRetrievalFailureException oe) {
				return new JqFailure(oe.getMessage());
			}
			break;
		case "del":
			try {
				biz.delete(acntMngFrmVO);
			} catch (ObjectRetrievalFailureException oe) {
				return new JqFailure(oe.getMessage());
			}
			break;
		}

		return new JqSuccess();
	}
}
