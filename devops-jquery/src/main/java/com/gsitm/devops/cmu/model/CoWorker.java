package com.gsitm.devops.cmu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.envers.Audited;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@Audited
@SuppressWarnings("serial")
public class CoWorker extends AbstractAuditable<Account, Long> {

	/**
	 * 조직 (코드)
	 */
	@ManyToOne
	private Code organization;
	
	/**
	 * 이름
	 */
	private String name;

	/**
	 * 계정
	 */
	@ManyToOne
	private Account account;

	/**
	 * 파트 (코드)
	 */
	@ManyToOne
	private Code part;

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 직급 (코드)
	 */
	@ManyToOne
	private Code position;
	
	/**
	 * 이메일
	 */
	private String email;

	/**
	 * 업체 명칭
	 */
	@Column(length = 32)
	private String companyName;

	/**
	 * 투입 시작일
	 */
	private LocalDate startDate;

	/**
	 * 투입 종료일
	 */
	private LocalDate endDate;

	/**
	 * 상태 (코드)
	 */
	@ManyToOne
	private Code status;

	/**
	 * 투입 목적
	 */
	private String purpose;

	/**
	 * 전화번호
	 */
	private String phone;

	/**
	 * 핸드폰 번호
	 */
	private String mobile;

	/**
	 * 주소
	 */
	private String address;

	/**
	 * 상주 여부 (코드)
	 */
	@ManyToOne
	private Code indoor;

	/**
	 * PC 제공 여부 (코드)
	 */
	@ManyToOne
	private Code computer;

	/**
	 * IP
	 */
	@Column(length = 16)
	private String ip;

	/**
	 * 보안 각서 징구 일자(고객사) 투입 시
	 */
	private LocalDate clientMemorandumStartDate;

	/**
	 * 보안 각서 징구 일자(고객사) 철수 시
	 */
	private LocalDate clientMemorandumEndDate;

	/**
	 * 보안 체크 리스트징구 일자(공통) 투입 시
	 */
	private LocalDate checklistStartDate;

	/**
	 * 보안 체크 리스트 징구 일자(공통) 철수 시
	 */
	private LocalDate checklistEndDate;

	/**
	 * 보안 각서 징구 일자(LG CNS) 대상 여부 (코드)
	 */
	@ManyToOne
	private Code memorandum;

	/**
	 * 보안 각서 징구 일자 투입 시
	 */
	private LocalDate memorandumStartDate;

	/**
	 * 보안 각서 징구 일자 철수 시
	 */
	private LocalDate memorandumEndDate;

	/**
	 * 출입 카드 제공 일자
	 */
	private LocalDate givedDate;

	/**
	 * 출입 카드 회수 일자
	 */
	private LocalDate callbackDate;

	/**
	 * 출입 카드 반환 일자
	 */
	private LocalDate returnedDate;

	/**
	 * 보안 교육 일자
	 */
	private LocalDate orientationDate;

	/**
	 * 보안 툴 설치 일자
	 */
	private LocalDate installedDate;

	/**
	 * 비고
	 */
	private String notes;
	
}
