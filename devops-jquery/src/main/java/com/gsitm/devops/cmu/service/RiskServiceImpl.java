package com.gsitm.devops.cmu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.Risk;
import com.gsitm.devops.cmu.repository.RiskRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class RiskServiceImpl implements RiskService {

	@Autowired
	private RiskRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Risk findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Risk findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Risk> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Risk> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Risk> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Risk> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Risk> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Risk> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Risk save(Risk risk) {
		return repository.save(risk);
	}

	@Transactional
	@Override
	public <S extends Risk> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Risk risk) {
		repository.delete(risk);
	}

	@Override
	public void delete(Iterable<? extends Risk> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
