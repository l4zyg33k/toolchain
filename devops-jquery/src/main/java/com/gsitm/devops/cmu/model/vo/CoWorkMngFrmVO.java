package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class CoWorkMngFrmVO extends CoWorkMngWebVO {

	private Long id;

	// 조직
	private Long organization;

	// 이름
	private String name;

	// 협력업체 직원정보
	private String account; // username
	private String accountName; // name
	private String accountEmail; // email

	// 파트
	private Long part;

	// 프로젝트
	private String project; // 프로젝트 코드
	private String projectName; // 프로젝트 명

	// 직급
	private Long position;

	private String email;

	// 업체명
	private String companyName;

	// 투입시작일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;

	// 투입종료일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate endDate;

	// 상태
	private Long status;

	// 투입목적
	private String purpose;

	// 전화번호
	private String phone;

	// 핸드폰번호
	private String mobile;

	// 주소
	private String address;

	// 상주여부
	private Long indoor;

	// PC제공여부
	private Long computer;

	// IP - 없음
	private String ip;

	// 보안각서징구일자(GS리테일) 투입시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate clientMemorandumStartDate;

	// 보안각서징구일자(GS리테일) 철수시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate clientMemorandumEndDate;

	// 보안체크리스트징구일자(공통) 투입시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate checklistStartDate;

	// 보안체크리스트징구일자(공통) 철수시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate checklistEndDate;

	// 보안각서징구일자(LG CNS) 대상여부
	private Long memorandum;

	// 보안각서징구일자(LG CNS) 투입시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate memorandumStartDate;

	// 보안각서징구일자(LG CNS) 철수시
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate memorandumEndDate;

	// 출입카드 제공일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate givedDate;

	// 출입카드 회수일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate callbackDate;

	// 출입카드 총무팀반환일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate returnedDate;

	// 보안교육일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate orientationDate;

	// 보안Tool설치일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate installedDate;

	// 비고
	private String notes;

	public CoWorkMngFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	@QueryProjection
	public CoWorkMngFrmVO(Long id, Long organization, String name,
			String account, String accountName, String accountEmail, Long part,
			String project, String projectName, Long position, String email,
			String companyName, LocalDate startDate, LocalDate endDate,
			Long status, String purpose, String phone, String mobile,
			String address, Long indoor, Long computer, String ip,
			LocalDate clientMemorandumStartDate,
			LocalDate clientMemorandumEndDate, LocalDate checklistStartDate,
			LocalDate checklistEndDate, Long memorandum,
			LocalDate memorandumStartDate, LocalDate memorandumEndDate,
			LocalDate givedDate, LocalDate callbackDate,
			LocalDate returnedDate, LocalDate orientationDate,
			LocalDate installedDate, String notes) {
		super();
		this.id = id;
		this.organization = organization;
		this.name = name;
		this.account = account;
		this.accountName = accountName;
		this.accountEmail = accountEmail;
		this.email = email;
		this.part = part;
		this.project = project;
		this.projectName = projectName;
		this.position = position;
		this.companyName = companyName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.purpose = purpose;
		this.phone = phone;
		this.mobile = mobile;
		this.address = address;
		this.indoor = indoor;
		this.computer = computer;
		this.ip = ip;
		this.clientMemorandumStartDate = clientMemorandumStartDate;
		this.clientMemorandumEndDate = clientMemorandumEndDate;
		this.checklistStartDate = checklistStartDate;
		this.checklistEndDate = checklistEndDate;
		this.memorandum = memorandum;
		this.memorandumStartDate = memorandumStartDate;
		this.memorandumEndDate = memorandumEndDate;
		this.givedDate = givedDate;
		this.callbackDate = callbackDate;
		this.returnedDate = returnedDate;
		this.orientationDate = orientationDate;
		this.installedDate = installedDate;
		this.notes = notes;
	}
}
