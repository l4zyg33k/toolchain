package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.CoWorker;

public interface CoWorkerService extends
		SharedService<CoWorker, Long> {

}
