package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class RiskMngFrmVO extends RiskMngWebVO {

	private String project;

	private String projectName;

	private String title;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate occurDate;

	private Long category;

	private String categoryName;

	private Long status;

	private String statusName;

	private Long inspect;

	private String inspectName;

	private Long approchPlan;

	private String approchPlanName;

	private Integer occurRate;

	private String content;

	private String impact;

	private String relaxPlan;

	private String emergencyPlan;

	private String projectIlName;

	public RiskMngFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	@QueryProjection
	public RiskMngFrmVO(Long id, String project, String projectName,
			String title, LocalDate occurDate, Long category,
			String categoryName, Long status, String statusName, Long inspect,
			String inspectName, Long approchPlan, String approchPlanName,
			Integer occurRate, String content, String impact, String relaxPlan,
			String emergencyPlan, String projectIlName) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.title = title;
		this.occurDate = occurDate;
		this.category = category;
		this.categoryName = categoryName;
		this.status = status;
		this.statusName = statusName;
		this.inspect = inspect;
		this.inspectName = inspectName;
		this.approchPlan = approchPlan;
		this.approchPlanName = approchPlanName;
		this.occurRate = occurRate;
		this.content = content;
		this.impact = impact;
		this.relaxPlan = relaxPlan;
		this.emergencyPlan = emergencyPlan;
		this.projectIlName = projectIlName;
	}
}
