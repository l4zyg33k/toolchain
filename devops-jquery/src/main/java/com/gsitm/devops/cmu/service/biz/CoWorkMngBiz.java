package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.CoWorkMngCmdVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngFrmVO;
import com.gsitm.devops.cmu.model.vo.CoWorkMngGrdVO;

public interface CoWorkMngBiz extends MessageSourceAware {

	public Map<String, Boolean> getNavGrid(UserDetails user);

	public void create(CoWorkMngCmdVO coWorkMngCmdVO);

	public void update(CoWorkMngCmdVO coWorkMngCmdVO);

	public void delete(CoWorkMngCmdVO coWorkMngCmdVO);

	public CoWorkMngFrmVO findOne(Long id);
	
	//목록
	public Page<CoWorkMngGrdVO> findAll(Pageable pageable, UserDetails user, LocalDate startDate, LocalDate endDate);
	
	//목록(하단)
	public Page<CoWorkMngGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, LocalDate startDate, LocalDate endDate);
}
