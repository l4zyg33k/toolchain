package com.gsitm.devops.cmu.service.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QAttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.adm.model.vo.QAtmtFileVO;
import com.gsitm.devops.adm.service.AttachmentFileService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.cmu.model.QRisk;
import com.gsitm.devops.cmu.model.QRiskDetail;
import com.gsitm.devops.cmu.model.Risk;
import com.gsitm.devops.cmu.model.RiskDetail;
import com.gsitm.devops.cmu.model.vo.QRiskMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.QRiskMngFrmVO;
import com.gsitm.devops.cmu.model.vo.QRiskMngGrdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngCmdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngDetlCmdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngDetlGrdVO;
import com.gsitm.devops.cmu.model.vo.RiskMngFrmVO;
import com.gsitm.devops.cmu.model.vo.RiskMngGrdVO;
import com.gsitm.devops.cmu.service.RiskDetailService;
import com.gsitm.devops.cmu.service.RiskService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class RiskMngBizImpl implements RiskMngBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private RiskService rService;

	@Autowired
	private RiskDetailService rdService;

	@Autowired
	private AttachmentFileService afService;

	@Override
	public void addAttachmentFile(Long id, MultipartHttpServletRequest request) {
		Risk risk = rService.findOne(id);
		Iterator<String> fileNames = request.getFileNames();
		while (fileNames.hasNext()) {
			MultipartFile file = request.getFile((String) fileNames.next());
			AttachmentFile attachment = afService.save(getContainer(), file);
			risk.getAttachments().add(attachment);
		}
		rService.save(risk);
	}

	@Override
	public void create(RiskMngDetlCmdVO riskMngDetlFrmVO, Long id) {
		RiskDetail detail = new RiskDetail();
		BeanUtils.copyProperties(riskMngDetlFrmVO, detail);
		detail = rdService.save(detail);
		Risk risk = rService.findOne(id);
		risk.getDetails().add(detail);
		rService.save(risk);
	}

	@Override
	public void create(RiskMngCmdVO riskMngCmdVO) {
		Risk risk = new Risk();
		BeanUtils.copyProperties(riskMngCmdVO, risk);
		List<RiskDetail> details = getDetails(riskMngCmdVO);
		List<AttachmentFile> attachments = getAttachments(riskMngCmdVO);
		risk.setDetails(details);
		risk.setAttachments(attachments);
		rService.save(risk);
	}

	@Override
	public void delete(RiskMngDetlCmdVO riskMngDetlFrmVO, Long id) {
		RiskDetail detail = rdService.findOne(riskMngDetlFrmVO.getId());
		Risk risk = rService.findOne(id);
		risk.getDetails().remove(detail);
		rService.save(risk);
		rdService.delete(detail);
	}

	@Override
	public void delete(RiskMngCmdVO riskMngCmdVO) {
		rService.delete(riskMngCmdVO.getId());
	}

	@Override
	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response) {
		afService.download(id, request, response);
	}

	@Override
	public Page<RiskMngDetlGrdVO> findAll(Pageable pageable, Long id) {
		QRisk risk = QRisk.risk;
		QRiskDetail detail = QRiskDetail.riskDetail;
		PathBuilder<Risk> builder = new PathBuilder<>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(risk)
				.innerJoin(risk.details, detail).where(risk.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<RiskMngDetlGrdVO> search = query
				.listResults(new QRiskMngDetlGrdVO(detail.id,
						detail.changedDate, detail.status.id, detail.content));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<RiskMngDetlGrdVO> findAll(Pageable pageable,
			Searchable searchable, Long id) {
		QRisk risk = QRisk.risk;
		QRiskDetail detail = QRiskDetail.riskDetail;
		PathBuilder<Risk> builder = new PathBuilder<>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(risk)
				.innerJoin(risk.details, detail)
				.where(risk.id.eq(id), predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<RiskMngDetlGrdVO> search = query
				.listResults(new QRiskMngDetlGrdVO(detail.id,
						detail.changedDate, detail.status.id, detail.content));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<RiskMngGrdVO> findAll(Pageable pageable, Searchable searchable,
			UserDetails user) {
		QRisk risk = QRisk.risk;
		PathBuilder<Risk> builder = new PathBuilder<>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(risk).where(predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<RiskMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QRiskMngGrdVO(risk.id,
					risk.project.code, risk.project.name, risk.title,
					risk.project.il.name, risk.occurDate, risk.category.name,
					risk.status.name, risk.inspect.name, risk.occurRate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(risk.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(risk.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QRiskMngGrdVO(risk.id,
					risk.project.code, risk.project.name, risk.title,
					risk.project.il.name, risk.occurDate, risk.category.name,
					risk.status.name, risk.inspect.name, risk.occurRate,
					editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<RiskMngGrdVO> findAll(Pageable pageable, UserDetails user) {
		QRisk risk = QRisk.risk;
		PathBuilder<Risk> builder = new PathBuilder<>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(risk);
		querydsl.applyPagination(pageable, query);
		SearchResults<RiskMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QRiskMngGrdVO(risk.id,
					risk.project.code, risk.project.name, risk.title,
					risk.project.il.name, risk.occurDate, risk.category.name,
					risk.status.name, risk.inspect.name, risk.occurRate));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(risk.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(risk.createdBy.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QRiskMngGrdVO(risk.id,
					risk.project.code, risk.project.name, risk.title,
					risk.project.il.name, risk.occurDate, risk.category.name,
					risk.status.name, risk.inspect.name, risk.occurRate,
					editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public RiskMngFrmVO findOne(Long id) {
		QRisk risk = QRisk.risk;
		QAccount il = QAccount.account;
		JPQLQuery query = new JPAQuery(em).from(risk)
				.innerJoin(risk.project.il, il).where(risk.id.eq(id));
		return query.uniqueResult(new QRiskMngFrmVO(risk.id, risk.project.code,
				risk.project.name, risk.title, risk.occurDate,
				risk.category.id, risk.category.name, risk.status.id,
				risk.status.name, risk.inspect.id, risk.inspect.name,
				risk.approchPlan.id, risk.approchPlan.name, risk.occurRate,
				risk.content, risk.impact, risk.relaxPlan, risk.emergencyPlan,
				il.name));
	}

	@Override
	public Map<Long, String> getApprochPlans() {
		return codeService.getOptions("E0");
	}

	@Override
	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable) {
		QRisk risk = QRisk.risk;
		QAttachmentFile attachment = QAttachmentFile.attachmentFile;
		PathBuilder<Risk> builder = new PathBuilder<Risk>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(risk)
				.innerJoin(risk.attachments, attachment).where(risk.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<AtmtFileVO> search = query.listResults(new QAtmtFileVO(
				attachment.id, attachment.fileName, attachment.fileSize,
				attachment.createdDate, attachment.createdBy.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	private List<AttachmentFile> getAttachments(RiskMngCmdVO riskMngCmdVO) {
		List<AttachmentFile> attachments = new ArrayList<>();
		List<MultipartFile> files = riskMngCmdVO.getFiles();
		if (CollectionUtils.isEmpty(files) == false) {
			attachments = afService.save(getContainer(), files);
		}
		return attachments;
	}

	@Override
	public Map<Long, String> getCategories() {
		return codeService.getOptions("A0");
	}

	private Code getContainer() {
		QCode code = QCode.code;
		return codeService.findOne(code.value.eq("02").and(
				code.parent.value.eq("A3")));
	}

	private List<RiskDetail> getDetails(RiskMngCmdVO riskMngCmdVO) {
		List<RiskDetail> details = new ArrayList<>();
		List<RiskMngDetlGrdVO> vos;
		try {
			vos = Arrays.asList(mapper.readValue(riskMngCmdVO.getRowData(),
					RiskMngDetlGrdVO[].class));
			for (RiskMngDetlGrdVO vo : vos) {
				RiskDetail detail = new RiskDetail();
				detail.setChangedDate(vo.getChangedDate());
				detail.setStatus(codeService.findOne(vo.getStatus()));
				detail.setContent(vo.getContent());
				details.add(detail);
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return details;
	}

	@Override
	public Map<Long, String> getInspectes() {
		return codeService.getOptions("C0");
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public String getStatus() {
		return Joiner.on(";").withKeyValueSeparator(":").join(getStatuses());
	}

	@Override
	public Map<Long, String> getStatuses() {
		return codeService.getOptions("B0");
	}

	@Override
	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO) {
		Risk risk = rService.findOne(id);
		AttachmentFile attachment = afService.findOne(atmtFileVO.getId());
		risk.getAttachments().remove(attachment);
		rService.save(risk);
		afService.delete(attachment);
	}

	@Override
	public void update(RiskMngDetlCmdVO riskMngDetlFrmVO) {
		RiskDetail detail = rdService.findOne(riskMngDetlFrmVO.getId());
		BeanUtils.copyProperties(riskMngDetlFrmVO, detail);
		rdService.save(detail);
	}

	@Override
	public void update(RiskMngCmdVO riskMngCmdVO) {
		Risk risk = rService.findOne(riskMngCmdVO.getId());
		BeanUtils.copyProperties(riskMngCmdVO, risk);
		rService.save(risk);
	}

	@Override
	public Page<RiskMngGrdVO> findAll(Pageable pageable, String projectCode) {
		QRisk risk = QRisk.risk;
		PathBuilder<Risk> builder = new PathBuilder<>(Risk.class, "risk");
		Querydsl querydsl = new Querydsl(em, builder);

		// 조회조건 - 프로젝트 코드
		BooleanBuilder conditions = new BooleanBuilder();
		if (projectCode != null) {
			conditions.and(risk.project.code.eq(projectCode));
		}

		JPQLQuery query = new JPAQuery(em).from(risk).where(conditions);
		querydsl.applyPagination(pageable, query);

		SearchResults<RiskMngGrdVO> search = query
				.listResults(new QRiskMngGrdVO(risk.id, risk.project.code,
						risk.project.name, risk.title, risk.project.il.name,
						risk.occurDate, risk.category.name, risk.status.name,
						risk.inspect.name, risk.occurRate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
