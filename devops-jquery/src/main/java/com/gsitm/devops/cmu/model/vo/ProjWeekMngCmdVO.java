package com.gsitm.devops.cmu.model.vo;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProjWeekMngCmdVO extends ProjWeekMngWebVO {

	@NotNull
	private Project project;

	private String projectName;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate projectStartDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate projectFinishDate;

	@NotBlank
	private String title;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate reportDate;

	private Account reporter;

	@NotBlank
	private String reporterName;

	private Code phase;

	private String thisWeek;

	private String nextWeek;

	private Integer planRate;

	private Integer acctualRate;

	private List<MultipartFile> files;
}
