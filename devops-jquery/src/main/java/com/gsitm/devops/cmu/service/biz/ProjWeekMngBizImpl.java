package com.gsitm.devops.cmu.service.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QAttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.adm.model.vo.QAcntItemVO;
import com.gsitm.devops.adm.model.vo.QAtmtFileVO;
import com.gsitm.devops.adm.service.AttachmentFileService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;
import com.gsitm.devops.cmu.model.QProjectWeeklyReport;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngCmdVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngFrmVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekMngGrdVO;
import com.gsitm.devops.cmu.model.vo.QProjWeekMngFrmVO;
import com.gsitm.devops.cmu.model.vo.QProjWeekMngGrdVO;
import com.gsitm.devops.cmu.service.ProjectWeeklyReportService;
import com.gsitm.devops.pms.model.QProjectMember;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class ProjWeekMngBizImpl implements ProjWeekMngBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ProjectWeeklyReportService pwrService;

	@Autowired
	private CodeService codeService;

	@Autowired
	private AttachmentFileService afService;

	@Override
	public void addAttachmentFile(Long id, MultipartHttpServletRequest request) {
		ProjectWeeklyReport pwr = pwrService.findOne(id);
		Iterator<String> fileNames = request.getFileNames();
		while (fileNames.hasNext()) {
			MultipartFile file = request.getFile((String) fileNames.next());
			AttachmentFile attachment = afService.save(getContainer(), file);
			pwr.getAttachments().add(attachment);
		}
		pwrService.save(pwr);
	}

	@Override
	public void create(ProjWeekMngCmdVO projWeekMngCmdVO) {
		ProjectWeeklyReport pwr = new ProjectWeeklyReport();
		BeanUtils.copyProperties(projWeekMngCmdVO, pwr);
		List<AttachmentFile> attachments = getAttachments(projWeekMngCmdVO);
		pwr.setAttachments(attachments);
		pwrService.save(pwr);
	}

	@Override
	public void delete(ProjWeekMngCmdVO projWeekMngCmdVO) {
		pwrService.delete(projWeekMngCmdVO.getId());
	}

	@Override
	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response) {
		afService.download(id, request, response);
	}

	@Override
	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;
		PathBuilder<ProjectWeeklyReport> builder = new PathBuilder<ProjectWeeklyReport>(
				ProjectWeeklyReport.class, "projectWeeklyReport");
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(pwr).where(
				pwr.reporter.username.eq(user.getUsername()), predicate);
		querydsl.applyPagination(pageable, query);
		SearchResults<ProjWeekMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjWeekMngGrdVO(pwr.id,
					pwr.project.code, pwr.title, pwr.reportDate,
					pwr.reporter.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(pwr.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(pwr.createdBy.username.eq(user.getUsername()))
					.then(true)
					.when(pwr.reporter.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QProjWeekMngGrdVO(pwr.id,
					pwr.project.code, pwr.title, pwr.reportDate,
					pwr.reporter.name, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<ProjWeekMngGrdVO> findAll(Pageable pageable, UserDetails user) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;
		PathBuilder<ProjectWeeklyReport> builder = new PathBuilder<ProjectWeeklyReport>(
				ProjectWeeklyReport.class, "projectWeeklyReport");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(pwr).where(
				pwr.reporter.username.eq(user.getUsername()));
		querydsl.applyPagination(pageable, query);
		SearchResults<ProjWeekMngGrdVO> search = SearchResults.emptyResults();
		if (AuthorityUtils.authorityListToSet(user.getAuthorities()).contains(
				Roles.ROLE_ADMIN)) {
			search = query.listResults(new QProjWeekMngGrdVO(pwr.id,
					pwr.project.code, pwr.title, pwr.reportDate,
					pwr.reporter.name));
		} else {
			BooleanExpression editable = new CaseBuilder()
					.when(pwr.project.il.username.eq(user.getUsername()))
					.then(true)
					.when(pwr.createdBy.username.eq(user.getUsername()))
					.then(true)
					.when(pwr.reporter.username.eq(user.getUsername()))
					.then(true).otherwise(false);
			search = query.listResults(new QProjWeekMngGrdVO(pwr.id,
					pwr.project.code, pwr.title, pwr.reportDate,
					pwr.reporter.name, editable));
		}
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public ProjWeekMngFrmVO findOne(long id) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;
		JPQLQuery query = new JPAQuery(em).from(pwr).where(pwr.id.eq(id));
		return query.uniqueResult(new QProjWeekMngFrmVO(pwr.id,
				pwr.project.code, pwr.project.name, pwr.project.startDate,
				pwr.project.finishDate, pwr.title, pwr.reportDate,
				pwr.reporter.username, pwr.reporter.name, pwr.phase.id,
				pwr.thisWeek, pwr.nextWeek, pwr.planRate, pwr.acctualRate));
	}

	@Override
	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;
		QAttachmentFile attachment = QAttachmentFile.attachmentFile;
		PathBuilder<ProjectWeeklyReport> builder = new PathBuilder<ProjectWeeklyReport>(
				ProjectWeeklyReport.class, "projectWeeklyReport");
		Querydsl querydsl = new Querydsl(em, builder);
		JPQLQuery query = new JPAQuery(em).from(pwr)
				.innerJoin(pwr.attachments, attachment).where(pwr.id.eq(id));
		querydsl.applyPagination(pageable, query);
		SearchResults<AtmtFileVO> search = query.listResults(new QAtmtFileVO(
				attachment.id, attachment.fileName, attachment.fileSize,
				attachment.createdDate, attachment.createdBy.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	private List<AttachmentFile> getAttachments(
			ProjWeekMngCmdVO projWeekMngCmdVO) {
		List<AttachmentFile> attachments = new ArrayList<>();
		List<MultipartFile> files = projWeekMngCmdVO.getFiles();
		if (CollectionUtils.isEmpty(files) == false) {
			attachments = afService.save(getContainer(), files);
		}
		return attachments;
	}

	private Code getContainer() {
		QCode code = QCode.code;
		return codeService.findOne(code.value.eq("03").and(
				code.parent.value.eq("A3")));
	}

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	@Override
	public Map<Long, String> getPhases() {
		return codeService.getOptions("Q1");
	}

	@Override
	public void removeAttachmentFile(Long id, AtmtFileVO atmtFileVO) {
		ProjectWeeklyReport pwr = pwrService.findOne(id);
		AttachmentFile attachment = afService.findOne(atmtFileVO.getId());
		pwr.getAttachments().remove(attachment);
		pwrService.save(pwr);
		afService.delete(attachment);
	}

	@Override
	public void update(ProjWeekMngCmdVO projWeekMngCmdVO) {
		ProjectWeeklyReport pwr = pwrService.findOne(projWeekMngCmdVO.getId());
		BeanUtils.copyProperties(projWeekMngCmdVO, pwr);
		pwrService.save(pwr);
	}

	@Override
	public Map<String, String> getProjects(UserDetails user) {
		QProjectMember mbr = QProjectMember.projectMember;
		LocalDate today = new LocalDate();
		JPQLQuery query = new JPAQuery(em).from(mbr).where(
				mbr.member.username.eq(user.getUsername()),
				mbr.project.startDate.before(today),
				mbr.project.finishDate.after(today));
		return query.map(mbr.project.code, mbr.project.code.prepend("[ ")
				.append(" ] ").append(mbr.project.name));
	}

	@Override
	public AcntItemVO getReporter(UserDetails user) {
		QAccount account = QAccount.account;
		QCode team = new QCode("team");
		QCode position = new QCode("position");
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.team, team)
				.leftJoin(account.position, position)
				.where(account.username.eq(user.getUsername()));
		return query.uniqueResult(new QAcntItemVO(account.username,
				account.name, team.name, position.name, account.email));
	}
}
