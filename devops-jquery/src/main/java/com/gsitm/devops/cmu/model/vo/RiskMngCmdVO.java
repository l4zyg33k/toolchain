package com.gsitm.devops.cmu.model.vo;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class RiskMngCmdVO extends RiskMngWebVO {

	private Project project;

	@NotBlank
	private String projectName;

	@NotBlank
	private String title;

	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate occurDate;

	@NotNull
	private Code category;

	@NotNull
	private Code status;

	@NotNull
	private Code inspect;

	@NotNull
	private Code approchPlan;

	private Integer occurRate;

	private String content;

	private String impact;

	private String relaxPlan;

	private String emergencyPlan;

	private String projectIlName;

	private List<MultipartFile> files;
}
