/**
 * 이슈 디테일 정보
 * IssueDetail.java
 * @author Administrator
 */
package com.gsitm.devops.cmu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 이슈 상세 엔티티
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class IssueDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 상태 변경 일자
	 */
	private LocalDate changedDate;

	/**
	 * 상태 (코드)
	 */
	@ManyToOne
	private Code status;

	/**
	 * 이슈 내용
	 */
	@Column(length = 2048)
	private String content;
}
