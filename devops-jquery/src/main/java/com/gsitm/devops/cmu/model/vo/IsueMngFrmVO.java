package com.gsitm.devops.cmu.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class IsueMngFrmVO extends IsueMngWebVO {

	private String project;

	private String projectName;

	private String title;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate occurrenceDate;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate planResolveDate;

	private Long category;

	private String categoryName;

	private Long status;

	private String statusName;

	private Long importance;

	private String importanceName;

	private Long urgency;

	private String urgencyName;

	private String content;

	private String impact;

	private String actionPlan;

	private String projectIlName;

	public IsueMngFrmVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	@QueryProjection
	public IsueMngFrmVO(Long id, String project, String projectName,
			String title, LocalDate occurrenceDate, LocalDate planResolveDate,
			Long category, String categoryName, Long status, String statusName,
			Long importance, String importanceName, Long urgency,
			String urgencyName, String content, String impact,
			String actionPlan, String projectIlName) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.title = title;
		this.occurrenceDate = occurrenceDate;
		this.planResolveDate = planResolveDate;
		this.category = category;
		this.categoryName = categoryName;
		this.status = status;
		this.statusName = statusName;
		this.importance = importance;
		this.importanceName = importanceName;
		this.urgency = urgency;
		this.urgencyName = urgencyName;
		this.content = content;
		this.impact = impact;
		this.actionPlan = actionPlan;
		this.projectIlName = projectIlName;
	}
}
