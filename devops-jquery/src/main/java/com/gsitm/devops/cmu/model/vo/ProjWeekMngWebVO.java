package com.gsitm.devops.cmu.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

@SuppressWarnings("serial")
public class ProjWeekMngWebVO extends SharedWebVO<Long> {

	/*
	 * 상위 생성자 두 개를 꼭 생성 해야 한다
	 */
	public ProjWeekMngWebVO() {
		super();
	}

	public ProjWeekMngWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/cmu/projweekmng" : String.format(
				"/cmu/projweekmng/%d", getId());
	}

	public String getPrevUrl() {
		return String.format("/cmu/projweekmng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}

	public String getAttachmentsUrl() {
		return String.format("/rest/cmu/projweekmng/%d/attachments", getId());
	}

	public String getAttachmentsEditUrl() {
		return String.format("/rest/cmu/projweekmng/%d/attachments/edit",
				getId());
	}

	public String getAttachmentsUploadUrl() {
		return String.format("/rest/cmu/projweekmng/%d/attachments/upload",
				getId());
	}
}
