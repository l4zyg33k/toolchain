package com.gsitm.devops.cmu.model.vo;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Code;

@Getter
@Setter
public class RiskMngDetlCmdVO {

	private String oper;

	private Long id;

	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate changedDate;

	@NotNull
	private Code status;

	private String content;

	public boolean isNew() {
		return null == getId();
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
