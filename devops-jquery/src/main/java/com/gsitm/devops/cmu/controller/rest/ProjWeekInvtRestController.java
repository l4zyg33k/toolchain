package com.gsitm.devops.cmu.controller.rest;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtGrdVO;
import com.gsitm.devops.cmu.service.biz.ProjWeekInvtBiz;

@RestController
public class ProjWeekInvtRestController {
	@Autowired
	private ProjWeekInvtBiz biz;

	/**
	 * @SpecialLogic 주간보고 현황 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보 
	 * @param teamCode : 팁코드
	 * @param fromReportDate : 보고서 시작일자
	 * @param toReportDate : 보고서 종료일자
	 * @return  Page<ProjWeekInvtGrdVO>
	 */
	@RequestMapping(value = "/rest/cmu/projweekinvt", method = RequestMethod.POST)
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("team") Long teamCode, 
			@RequestParam(value = "fromReportDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromReportDate,
			@RequestParam(value = "toReportDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toReportDate) {

		return biz.findAll(pageable, user, teamCode, fromReportDate, toReportDate);
	}
	
	/**
	 * @SpecialLogic 주간보고 현황 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param searchable : jqGrid 조회조건
	 * @param user : 사용자 정보 
	 * @param teamCode : 팁코드
	 * @param fromReportDate : 보고서 시작일자
	 * @param toReportDate : 보고서 종료일자
	 * @return  Page<ProjWeekInvtGrdVO>
	 */
	@RequestMapping(value = "/rest/cmu/projweekinvt", method = RequestMethod.POST, params = { "_search=true" })
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable,
			Searchable searchable, @AuthenticationPrincipal UserDetails user,
			@RequestParam("team") Long teamCode,
			@RequestParam(value = "fromReportDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromReportDate,
			@RequestParam(value = "toReportDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toReportDate) {
		return biz.findAll(pageable, searchable, user, teamCode, fromReportDate, toReportDate);
	}
	
	/**
	 * @SpecialLogic 주간보고서 첨부파일
	 * 
	 * @param id : 주간보고서 id
	 * @return  Page<AtmtFileVO>
	 */	
	@RequestMapping(value = "/rest/cmu/projweekinvt/{id}/attachments", method = RequestMethod.POST)
	public Page<AtmtFileVO> getAttachments(@PathVariable("id") Long id,
			Pageable pageable) {
		return biz.getAttachments(id, pageable);
	}	
}
