package com.gsitm.devops.cmu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.CoWorker;
import com.gsitm.devops.cmu.repository.CoWorkerRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class CoWorkerServiceImpl implements CoWorkerService {

	@Autowired
	private CoWorkerRepository repository;

	@Override
	public boolean exists(Long seq) {
		return repository.exists(seq);
	}

	@Override
	public CoWorker findOne(Long seq) {
		return repository.findOne(seq);
	}

	@Override
	public CoWorker findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<CoWorker> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<CoWorker> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<CoWorker> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<CoWorker> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<CoWorker> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<CoWorker> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public CoWorker save(CoWorker coWorker) {
		return repository.save(coWorker);
	}

	@Transactional
	@Override
	public <S extends CoWorker> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(CoWorker coWorker) {
		repository.delete(coWorker);
	}

	@Override
	public void delete(Iterable<? extends CoWorker> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
