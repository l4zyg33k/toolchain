package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.Risk;

public interface RiskService extends SharedService<Risk, Long> {
}
