/**
 * 주간업무 엔티티 ProjectWeeklyReport.java
 *
 * @author Administrator
 */
package com.gsitm.devops.cmu.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProjectWeeklyReport extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 보고서 제목
	 */
	private String title;

	/**
	 * 보고 일자
	 */
	private LocalDate reportDate;

	/**
	 * 계정
	 */
	@ManyToOne
	private Account reporter;

	/**
	 * 진행 단계 (코드)
	 */
	@ManyToOne
	private Code phase;

	/**
	 * 금주 실적
	 */
	@Column(length = 2560)
	private String thisWeek;

	/**
	 * 차주 계획
	 */
	@Column(length = 2560)
	private String nextWeek;

	/**
	 * 계획 진척율
	 */
	private Integer planRate;

	/**
	 * 실제 진척율
	 */
	private Integer acctualRate;

	/**
	 * 첨부 파일
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<AttachmentFile> attachments;
}
