package com.gsitm.devops.cmu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.RiskDetail;
import com.gsitm.devops.cmu.repository.RiskDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class RiskDetailServiceImpl implements RiskDetailService {

	@Autowired
	private RiskDetailRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public RiskDetail findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public RiskDetail findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<RiskDetail> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<RiskDetail> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<RiskDetail> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<RiskDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<RiskDetail> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<RiskDetail> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public RiskDetail save(RiskDetail riskDetail) {
		return repository.save(riskDetail);
	}

	@Transactional
	@Override
	public <S extends RiskDetail> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(RiskDetail riskDetail) {
		repository.delete(riskDetail);
	}

	@Override
	public void delete(Iterable<? extends RiskDetail> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
