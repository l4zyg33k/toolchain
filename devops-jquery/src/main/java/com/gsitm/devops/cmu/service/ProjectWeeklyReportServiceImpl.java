package com.gsitm.devops.cmu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.ProjectWeeklyReport;
import com.gsitm.devops.cmu.repository.ProjectWeeklyReportRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class ProjectWeeklyReportServiceImpl implements
		ProjectWeeklyReportService {

	@Autowired
	private ProjectWeeklyReportRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public ProjectWeeklyReport findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public ProjectWeeklyReport findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProjectWeeklyReport> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<ProjectWeeklyReport> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProjectWeeklyReport> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProjectWeeklyReport> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProjectWeeklyReport> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<ProjectWeeklyReport> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProjectWeeklyReport save(ProjectWeeklyReport projectWeeklyReport) {
		return repository.save(projectWeeklyReport);
	}

	@Transactional
	@Override
	public <S extends ProjectWeeklyReport> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProjectWeeklyReport projectWeeklyReport) {
		repository.delete(projectWeeklyReport);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProjectWeeklyReport> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
