package com.gsitm.devops.cmu.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QAttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.AtmtFileVO;
import com.gsitm.devops.adm.model.vo.QAtmtFileVO;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.cmu.model.ProjectWeeklyReport;
import com.gsitm.devops.cmu.model.QProjectWeeklyReport;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtFrmVO;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtGrdVO;
import com.gsitm.devops.cmu.model.vo.QProjWeekInvtFrmVO;
import com.gsitm.devops.cmu.model.vo.QProjWeekInvtGrdVO;
import com.gsitm.devops.cmu.service.ProjectWeeklyReportService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class ProjWeekInvtBizImpl implements ProjWeekInvtBiz {

	// 메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ProjectWeeklyReportService pwrService;

	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	/**
	 * @SpecialLogic 주간보고 현황 단건 상세 조회
	 * 
	 * @param id
	 * @return ProjWeekInvtFrmVO
	 */
	@Override
	public ProjWeekInvtFrmVO findOne(Long id) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;

		JPQLQuery query = new JPAQuery(em).from(pwr).where(pwr.id.eq(id));

		return query.uniqueResult(new QProjWeekInvtFrmVO(pwr.id,
				pwr.project.code, pwr.project.name, pwr.project.startDate,
				pwr.project.finishDate, pwr.title, pwr.reportDate,
				pwr.reporter.username, pwr.reporter.name, pwr.phase.id,
				pwr.thisWeek, pwr.nextWeek, pwr.planRate, pwr.acctualRate));
	}

	/**
	 * @SpecialLogic 주간보고 현황 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param teamCode
	 *            : 팁코드
	 * @param fromReportDate
	 *            : 보고서 시작일자
	 * @param toReportDate
	 *            : 보고서 종료일자
	 * @return Page<ProjWeekInvtGrdVO>
	 */
	@Override
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable, UserDetails user,
			Long teamCode, LocalDate fromReportDate, LocalDate toReportDate) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;

		QAccount account = new QAccount("account"); // 계정-작성자
		QCode team = new QCode("team"); // 팀

		// 조회조건 추가 - 투입일, 철수일
		BooleanBuilder conditions = new BooleanBuilder();
		if (fromReportDate != null) {
			conditions.and(pwr.reportDate.goe(fromReportDate));
		}
		if (toReportDate != null) {
			conditions.and(pwr.reportDate.loe(toReportDate));
		}
		if (teamCode > 0) {
			conditions.and(team.id.eq(teamCode));
		}

		JPQLQuery query = new JPAQuery(em).from(pwr)
				.leftJoin(pwr.reporter, account).leftJoin(account.team, team)
				.where(conditions);

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<ProjectWeeklyReport>(ProjectWeeklyReport.class,
						"projectWeeklyReport"));
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjWeekInvtGrdVO> search = query
				.listResults(new QProjWeekInvtGrdVO(pwr.id, pwr.project.code,
						pwr.project.name, pwr.title, pwr.reportDate,
						pwr.reporter.name));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 주간보고 현황 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : jqGrid 조회조건
	 * @param user
	 *            : 사용자 정보
	 * @param teamCode
	 *            : 팁코드
	 * @param fromReportDate
	 *            : 보고서 시작일자
	 * @param toReportDate
	 *            : 보고서 종료일자
	 * @return Page<ProjWeekInvtGrdVO>
	 */
	@Override
	public Page<ProjWeekInvtGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, Long teamCode,
			LocalDate fromReportDate, LocalDate toReportDate) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;

		QAccount account = new QAccount("account"); // 계정-작성자
		QCode team = new QCode("team"); // 팀

		// 조회조건 추가 - 투입일, 철수일
		BooleanBuilder conditions = new BooleanBuilder();
		if (fromReportDate != null) {
			conditions.and(pwr.reportDate.goe(fromReportDate));
		}
		if (toReportDate != null) {
			conditions.and(pwr.reportDate.loe(toReportDate));
		}
		if (teamCode > 0) {
			conditions.and(team.id.eq(teamCode));
		}

		PathBuilder<ProjectWeeklyReport> path = new PathBuilder<ProjectWeeklyReport>(
				ProjectWeeklyReport.class, "projectWeeklyReport");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(pwr)
				.leftJoin(pwr.reporter, account).leftJoin(account.team, team)
				.where(predicate, conditions);

		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);

		SearchResults<ProjWeekInvtGrdVO> search = query
				.listResults(new QProjWeekInvtGrdVO(pwr.id, pwr.project.code,
						pwr.project.name, pwr.title, pwr.reportDate,
						pwr.reporter.name));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 주간보고 - 첨부파일
	 * 
	 * @param id
	 * @param pageable
	 *            : 페이지 처리
	 * @return Page<AtmtFileVO>
	 */
	@Override
	public Page<AtmtFileVO> getAttachments(Long id, Pageable pageable) {
		QProjectWeeklyReport pwr = QProjectWeeklyReport.projectWeeklyReport;

		QAttachmentFile attachment = QAttachmentFile.attachmentFile;
		PathBuilder<ProjectWeeklyReport> builder = new PathBuilder<ProjectWeeklyReport>(
				ProjectWeeklyReport.class, "projectWeeklyReport");

		Querydsl querydsl = new Querydsl(em, builder);

		JPQLQuery query = new JPAQuery(em).from(pwr)
				.innerJoin(pwr.attachments, attachment).where(pwr.id.eq(id));

		querydsl.applyPagination(pageable, query);

		SearchResults<AtmtFileVO> search = query.listResults(new QAtmtFileVO(
				attachment.id, attachment.fileName, attachment.fileSize,
				attachment.createdDate, attachment.createdBy.name));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
}
