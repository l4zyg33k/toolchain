package com.gsitm.devops.cmu.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;

public interface CoWorkAcntBiz extends MessageSourceAware {

	public String delete(AcntMngFrmVO vo);

	public String create(AcntMngFrmVO vo);

	public String update(AcntMngFrmVO vo);

	public Page<AcntMngGrdVO> findAll(Pageable pageable);

	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable);

	public String getPositions();

	public Map<String, Boolean> getNavGrid(UserDetails user);
}
