package com.gsitm.devops.cmu.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

@SuppressWarnings("serial")
public class ProjWeekInvtWebVO extends SharedWebVO<Long> {

	public ProjWeekInvtWebVO() {
		super();
	}

	public ProjWeekInvtWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}	

	public String getActionUrl() {
		return isNew() ? "/cmu/projweekinvt" : String.format("/cmu/projweekinvt/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/cmu/projweekinvt?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}	
	
	public String getAttachmentsUrl() {
		return String.format("/rest/cmu/projweekmng/%d/attachments", getId());
	}	

}
