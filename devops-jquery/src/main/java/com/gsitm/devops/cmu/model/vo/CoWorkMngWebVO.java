package com.gsitm.devops.cmu.model.vo;

import com.gsitm.devops.cmm.model.SharedWebVO;

@SuppressWarnings("serial")
public class CoWorkMngWebVO extends SharedWebVO<Long> {

	public CoWorkMngWebVO() {
		super();
	}

	public CoWorkMngWebVO(int page, int rowNum, Long rowId) {
		super(page, rowNum, rowId);
	}

	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/cmu/coworkmng" : String.format("/cmu/coworkmng/%d",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/cmu/coworkmng?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
}
