/**
 * 이슈 마스터 정보
 * Issue.java
 * @author Administrator
 */
package com.gsitm.devops.cmu.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Issue extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 이슈 제목
	 */
	private String title;

	/**
	 * 발생 일자
	 */
	private LocalDate occurrenceDate;

	/**
	 * 해결 예정 일자
	 */
	private LocalDate planResolveDate;

	/**
	 * 범주 (코드)
	 */
	@ManyToOne
	private Code category;

	/**
	 * 상태 (코드)
	 */
	@ManyToOne
	private Code status;

	/**
	 * 중요도 (코드)
	 */
	@ManyToOne
	private Code importance;

	/**
	 * 긴급성 (코드)
	 */
	@ManyToOne
	private Code urgency;

	/**
	 * 이슈 내용
	 */
	@Column(length = 1024)
	private String content;

	/**
	 * 이슈 영향
	 */
	@Column(length = 1024)
	private String impact;

	/**
	 * 이슈 처리 내용
	 */
	@Column(length = 1024)
	private String actionPlan;

	/**
	 * 이슈 상세
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<IssueDetail> details;

	/**
	 * 첨부 파일
	 */
	@OneToMany(orphanRemoval = true, cascade = { CascadeType.PERSIST })
	private List<AttachmentFile> attachments;
}
