package com.gsitm.devops.cmu.controller;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.cmu.model.vo.ProjWeekInvtFrmVO;
import com.gsitm.devops.cmu.service.biz.ProjWeekInvtBiz;

@Controller
public class ProjWeekInvtController {

	private static final String MENU_CODE = "ProjWeekInvt";
	private static final String FORM_VO = "projWeekInvtFrmVO";
	private static final String FROM_DATE = "fromReportDate";
	private static final String TO_DATE = "toReportDate";

	private static final String INDEX_URL = "/cmu/projweekinvt";
	private static final String VIEW_URL_BY_ID = "/cmu/projweekinvt/{id}/view";

	private static final String INDEX_PAGE = "cmu/projweekinvt/index";
	private static final String VIEW_PAGE = "cmu/projweekinvt/view";

	@Autowired
	private ProjWeekInvtBiz biz;

	@Autowired
	private CommonCodeService cmmCodeService;

	// 메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	// 사용자별 jqGrid 권한 처리로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	// 소속, 팀
	@ModelAttribute("team")
	public String getTeam() {
		return "";
	}

	// 소속, 팀
	@ModelAttribute("teams")
	public Map<Long, String> getTeams() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "전체");
		result.putAll(cmmCodeService.getTeams());
		return result;
	}

	// 시작 일자
	@ModelAttribute("fromReportDate")
	public LocalDate getFromReportDate() {
		return new LocalDate().minusDays(7);
	}

	// 종료 일자
	@ModelAttribute("toReportDate")
	public LocalDate getToReportDate() {
		return new LocalDate();
	}

	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	@RequestMapping(value = VIEW_URL_BY_ID, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId,
			@PathVariable(PageParams.ID) Long id, Model model,
			@RequestParam(FROM_DATE) String fromReportDate, 
			@RequestParam(TO_DATE) String toReportDate) {
		ProjWeekInvtFrmVO vo = biz.findOne(id);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		model.addAttribute(FROM_DATE, fromReportDate);
		model.addAttribute(TO_DATE, toReportDate);
		return VIEW_PAGE;
	}
	
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) Long rowId,
			@RequestParam(FROM_DATE) String fromReportDate, 
			@RequestParam(TO_DATE) String toReportDate, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		model.addAttribute(FROM_DATE, fromReportDate);
		model.addAttribute(TO_DATE, toReportDate);
		return INDEX_PAGE;
	}	
}
