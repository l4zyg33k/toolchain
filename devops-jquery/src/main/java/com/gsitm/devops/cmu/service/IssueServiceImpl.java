package com.gsitm.devops.cmu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.cmu.model.Issue;
import com.gsitm.devops.cmu.repository.IssueRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class IssueServiceImpl implements IssueService {

	@Autowired
	private IssueRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Issue findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Issue findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Issue> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Issue> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Issue> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Issue> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Issue> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Issue> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Issue save(Issue issue) {
		return repository.save(issue);
	}

	@Transactional
	@Override
	public <S extends Issue> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Issue issue) {
		repository.delete(issue);
	}

	@Override
	public void delete(Iterable<? extends Issue> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
