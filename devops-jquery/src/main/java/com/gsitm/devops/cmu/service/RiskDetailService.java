package com.gsitm.devops.cmu.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.cmu.model.RiskDetail;

public interface RiskDetailService extends
		SharedService<RiskDetail, Long> {
}
