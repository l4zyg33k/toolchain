package com.gsitm.devops;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class AuditingConfigurer {

	@Bean(name = "auditorAware")
	public AuditorAwareImpl configureAuditorAware() {
		return new AuditorAwareImpl();
	}
}
