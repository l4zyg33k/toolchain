package com.gsitm.devops;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.cache.EhCacheBasedUserCache;

@Configuration
@EnableCaching
public class EhcacheConfigurer {
	
	@Bean(name = "ehCacheManagerFactoryBean")
	public EhCacheManagerFactoryBean configureEhCacheManager() {
		EhCacheManagerFactoryBean factoryBean = new EhCacheManagerFactoryBean();
		factoryBean.setShared(true);
		factoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
		return factoryBean;
	}

	@Bean(name = "cacheManager")
	@Autowired
	public CacheManager configureCacheManager(
			net.sf.ehcache.CacheManager ehCacheManager) {
		EhCacheCacheManager cacheManager = new EhCacheCacheManager();
		cacheManager.setCacheManager(ehCacheManager);
		return cacheManager;
	}

	@Bean(name = "userCache")
	@Autowired
	public UserCache configureUserCache(
			net.sf.ehcache.CacheManager ehCacheManager) {
		EhCacheBasedUserCache userCache = new EhCacheBasedUserCache();
		userCache.setCache(ehCacheManager.getEhcache("userCache"));
		return userCache;
	}
}