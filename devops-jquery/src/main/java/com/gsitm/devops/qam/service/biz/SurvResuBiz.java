package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.SurvResuMultiGrdVO;
import com.gsitm.devops.qam.model.vo.SurvResuShortGrdVO;

public interface SurvResuBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public Page<SurvResuMultiGrdVO> findMultiAll(Pageable pageable, UserDetails user, String userType, String projectCode);
	public Page<SurvResuMultiGrdVO> findMultiAll(Pageable pageable, UserDetails user, String userType, String projectCode, Searchable searchable);
	
	public Page<SurvResuShortGrdVO> findShortAll(Pageable pageable, UserDetails user, String userType, String projectCode);
	public Page<SurvResuShortGrdVO> findShortAll(Pageable pageable, UserDetails user, String userType, String projectCode, Searchable searchable);
	
	public void update(String projectCode);
}
