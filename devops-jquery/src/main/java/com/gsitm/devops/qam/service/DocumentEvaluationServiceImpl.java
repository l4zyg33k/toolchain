package com.gsitm.devops.qam.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.DocumentEvaluation;
import com.gsitm.devops.qam.repository.DocumentEvaluationRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 산출물 측정
 */
@Service
@Transactional(readOnly = true)
public class DocumentEvaluationServiceImpl implements DocumentEvaluationService {

	@Autowired
	private DocumentEvaluationRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public DocumentEvaluation findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public DocumentEvaluation findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<DocumentEvaluation> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<DocumentEvaluation> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<DocumentEvaluation> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<DocumentEvaluation> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<DocumentEvaluation> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<DocumentEvaluation> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public DocumentEvaluation save(DocumentEvaluation DocumentEvaluation) {

		return repository.save(DocumentEvaluation);
	}

	@Transactional
	@Override
	public <S extends DocumentEvaluation> List<S> save(Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(DocumentEvaluation DocumentEvaluation) {

		repository.delete(DocumentEvaluation);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends DocumentEvaluation> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

}
