package com.gsitm.devops.qam.service;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.dto.QSatisfactionSurveyResultDto;
import com.gsitm.devops.qam.dto.SatisfactionSurveyResultDto;
import com.gsitm.devops.qam.model.QSatisfactionSurveyResult;
import com.gsitm.devops.qam.model.SatisfactionSurveyResult;
import com.gsitm.devops.qam.repository.SatisfactionSurveyResultRepository;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.expr.Wildcard;

/**
 * 산출물 측정
 */
@Service
@Transactional(readOnly = true)
public class SatisfactionSurveyResultServiceImpl implements
		SatisfactionSurveyResultService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private SatisfactionSurveyResultRepository repository;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public SatisfactionSurveyResult findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public SatisfactionSurveyResult findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyResult> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<SatisfactionSurveyResult> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<SatisfactionSurveyResult> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyResult> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<SatisfactionSurveyResult> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<SatisfactionSurveyResult> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public SatisfactionSurveyResult save(
			SatisfactionSurveyResult SatisfactionSurveyResult) {

		return repository.save(SatisfactionSurveyResult);
	}

	@Transactional
	@Override
	public <S extends SatisfactionSurveyResult> List<S> save(
			Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(SatisfactionSurveyResult SatisfactionSurveyResult) {

		repository.delete(SatisfactionSurveyResult);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends SatisfactionSurveyResult> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

	@Override
	public Page<SatisfactionSurveyResultDto> findResultDto(Predicate predicate,
			Pageable pageable) {
		// 응답자유형은 predicate에 포함
		QSatisfactionSurveyResult qSatisfactionSurveyResult = QSatisfactionSurveyResult.satisfactionSurveyResult;
		List<SatisfactionSurveyResultDto> result = new JPAQuery(entityManager)
				.from(qSatisfactionSurveyResult)
				.where(predicate)
				.groupBy(qSatisfactionSurveyResult.username,
						qSatisfactionSurveyResult.name,
						qSatisfactionSurveyResult.position,
						qSatisfactionSurveyResult.team,
						qSatisfactionSurveyResult.project,
						qSatisfactionSurveyResult.userType)
				.limit(pageable.getOffset() + pageable.getPageSize()) // 페이징처리관련
				.offset(pageable.getOffset()) // 페이징처리관련
				.setHint("org.hibernate.cacheable", "true")
				.list(new QSatisfactionSurveyResultDto(
						qSatisfactionSurveyResult.username,
						qSatisfactionSurveyResult.name,
						qSatisfactionSurveyResult.position,
						qSatisfactionSurveyResult.team,
						qSatisfactionSurveyResult.project,
						qSatisfactionSurveyResult.userType,

						// NumberExpression<Integer>타입변환을 위해 CaseBuilder()사용
						// 사업부담당자
						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("응답속도"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("요구사항"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("출력양식"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("자료정확"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("보안체계"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("일정준수"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("개발시스템.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("교육자료"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("메뉴얼"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("UI"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("호환성"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("시스템활용지원.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						// 현업사용자
						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("응답속도"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("요구사항"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("자료정확"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.안정"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("사용편의"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.개선"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("개발시스템.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						// 시스템운영자
						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("문서완성"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("문서이해"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("문서일치"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("문서실질"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("개발문서.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.구성"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.확장"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("보안체계"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.성능"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("오류빈도"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("시스템구조및성능.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("운영편의"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("수정용이"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("표준준수"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("Sys.연동"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(),

						new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("User관리"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("배치편리"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max(), new CaseBuilder()
								.when(qSatisfactionSurveyResult.keyword
										.eq("시스템운영편리성.종합"))
								.then(qSatisfactionSurveyResult.point)
								.otherwise(0).max()));

		// 페이징처리 관련
		long total = 0;

		// count = 0 일경우 null 처리
		try {
			total = new JPAQuery(entityManager)
					.from(qSatisfactionSurveyResult)
					.where(predicate)
					.groupBy(qSatisfactionSurveyResult.username,
							qSatisfactionSurveyResult.name,
							qSatisfactionSurveyResult.position,
							qSatisfactionSurveyResult.team,
							qSatisfactionSurveyResult.project,
							qSatisfactionSurveyResult.userType)
					.setHint("org.hibernate.cacheable", "true")
					.uniqueResult(Wildcard.count);
		} catch (Exception e) {
			result = Collections.<SatisfactionSurveyResultDto> emptyList();
		}

		return new PageImpl<>(result, pageable, total);
	}

}
