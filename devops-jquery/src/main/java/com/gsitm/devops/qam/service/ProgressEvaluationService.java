package com.gsitm.devops.qam.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.ProgressEvaluation;
import com.mysema.query.types.Predicate;

public interface ProgressEvaluationService extends
		SharedService<ProgressEvaluation, Long> {
	public double getProgressPerPlan(String code);

	public Page<ProgressEvaluation> findAllJoinProjectStep(Predicate predicate,
			Pageable pageable);
}
