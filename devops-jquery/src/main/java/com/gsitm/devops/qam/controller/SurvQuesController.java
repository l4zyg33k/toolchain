package com.gsitm.devops.qam.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.qam.service.biz.SurvQuesBiz;

@Controller
public class SurvQuesController {

	private static final String MENU_CODE = "SurvQues";
	
	private static final String INDEX_URL = "/qam/survques";
	
	private static final String INDEX_PAGE = "qam/survques/index";
	
	@Autowired
	private SurvQuesBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	//사업부 (T1)
	@ModelAttribute("userType")
	public Map<Long, String> getRecipients() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "--- 선택 ---");
		result.putAll(cmmCodeService.getRecipient());
		return result;
	}
	
	@ModelAttribute("surveyType")
	public Map<Long, String> getSurveyType() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "--- 선택 ---");
		return result;
	}	
	
	//사업부 문항유형
	@ModelAttribute("divisions")
	public Map<Long, String> getDivisions() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "--- 선택 ---");
		result.putAll(cmmCodeService.getOperationDivisions());
		return result;
	}	
	
	//협업사용자
	@ModelAttribute("actualWork")
	public Map<Long, String> getActualWork() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "--- 선택 ---");
		result.putAll(cmmCodeService.getActualWork());
		return result;
	}		
	
	//운영자
	@ModelAttribute("operator")
	public Map<Long, String> getOperator() {
		Map<Long, String> result = new HashMap<>();
		result.put(0L, "--- 선택 ---");
		result.putAll(cmmCodeService.getOperator());
		return result;
	}		

	/**
	 * @SpecialLogic 만족도 설문 문항 관리  페이지 로딩시 
	 * 
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}
	

}
