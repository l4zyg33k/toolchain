package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurvResuExcelFrmVO {	
	
	private String data;
	private String filename;
	
	public SurvResuExcelFrmVO(String data, String filename) {
		super();
		this.data = data;
		this.filename = filename;
	}
}
