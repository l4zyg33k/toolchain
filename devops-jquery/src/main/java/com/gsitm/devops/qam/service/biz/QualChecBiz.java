package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.qam.model.QualityCheckResult;
import com.gsitm.devops.qam.model.vo.QualChecCmdVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlFrmVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlGrdVO;
import com.gsitm.devops.qam.model.vo.QualChecFrmVO;
import com.gsitm.devops.qam.model.vo.QualChecGrdVO;

public interface QualChecBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public Page<QualChecGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode);
	
	public QualChecFrmVO findOne(Long id);
	
	public void create(QualChecCmdVO qualChecCmdVO, String rowdata);
	
	public void update(QualChecCmdVO qualChecCmdVO);
	
	public Page<QualChecDetlGrdVO> findDtlAll(Pageable pageable, QualityCheckResult qualityCheckResult);
	
	public Page<QualChecGrdVO> findAll(Pageable pageable, String projectCode);
	
	public void create(QualChecDetlFrmVO qualChecDetlFrmVO, QualityCheckResult qualityCheckResult);
	
	public void update(QualChecDetlFrmVO qualChecDetlFrmVO, QualityCheckResult qualityCheckResult);
	
	public void delete(QualChecDetlFrmVO qualChecDetlFrmVO);
	
	public void delete(QualChecCmdVO qualChecCmdVO);
}
