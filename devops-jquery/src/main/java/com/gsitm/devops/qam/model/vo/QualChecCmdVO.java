package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class QualChecCmdVO extends QualChecWebVO {	
	//프로젝트
	private Project project;
	private Account il; //정보 기술 리더
	private String projectName;
	private String iLname;
	
	//품질 점검 제목
	private String title;	

	//품질 점검 일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate inspectedDate;

	//품질 점검 유형 (코드)
	private Code type;
	
	//품질 점검 단계 (코드)
	private Code stage;	

	//검토자
	private Account inspector;
	
	@NotBlank
	private String inspectorName; 

	//품질 검토 소요시간(MH)
	private int hours;
	
	private int requiredCount;
	private int opinionCount;
	private int mainPage;
	private int mainRowNum;	
}
