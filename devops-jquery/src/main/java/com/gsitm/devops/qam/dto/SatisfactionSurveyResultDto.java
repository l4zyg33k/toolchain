package com.gsitm.devops.qam.dto;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.model.SatisfactionSurveyResult;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SatisfactionSurveyResultDto {

	private SatisfactionSurveyResult result;
	private String username;
	private String name;
	private Code position;
	private Code team;
	private Project project;
	private Code userType;

	// 사업부
	private Integer T301; // 응답속도
	private Integer T302; // 요구사항
	private Integer T303; // 출력양식
	private Integer T304; // 자료정확
	private Integer T305; // 보안체계
	private Integer T306; // 일정준수
	private Integer T307; // 개발시스템.종합

	private Integer T308; // 교육자료
	private Integer T309; // 메뉴얼
	private Integer T310; // UI
	private Integer T311; // 호환성
	private Integer T312; // 시스템활용지원.종합

	// 현업사용자
	private Integer T401; // 응답속도
	private Integer T402; // 요구사항
	private Integer T403; // 자료정확
	private Integer T404; // Sys.안정
	private Integer T405; // 사용편의
	private Integer T406; // Sys.개선
	private Integer T407; // 개발시스템.종합

	// 시스템운영자
	private Integer T501; // 문서완성
	private Integer T502; // 문서이해
	private Integer T503; // 문서일치
	private Integer T504; // 문서실질
	private Integer T505; // 개발문서.종합

	private Integer T506; // Sys.구성
	private Integer T507; // Sys.확장
	private Integer T508; // 보안체계
	private Integer T509; // Sys.성능
	private Integer T510; // 오류빈도
	private Integer T511; // 시스템구조및성능.종합

	private Integer T512; // 운영편의
	private Integer T513; // 수정용이
	private Integer T514; // 표준준수
	private Integer T515; // Sys.연동
	private Integer T516; // User관리
	private Integer T517; // 배치편리
	private Integer T518; // 시스템운영편리성.종합

	@QueryProjection
	public SatisfactionSurveyResultDto(String username, String name,
			Code position, Code team, Project project, Code userType,
			Integer T301, Integer T302, Integer T303, Integer T304,
			Integer T305, Integer T306, Integer T307, Integer T308,
			Integer T309, Integer T310, Integer T311, Integer T312,
			Integer T401, Integer T402, Integer T403, Integer T404,
			Integer T405, Integer T406, Integer T407, Integer T501,
			Integer T502, Integer T503, Integer T504, Integer T505,
			Integer T506, Integer T507, Integer T508, Integer T509,
			Integer T510, Integer T511, Integer T512, Integer T513,
			Integer T514, Integer T515, Integer T516, Integer T517, Integer T518) {

		this.username = username;
		this.name = name;
		this.position = position;
		this.team = team;
		this.project = project;
		this.userType = userType;

		this.T301 = T301; // 응답속도
		this.T302 = T302; // 요구사항
		this.T303 = T303; // 출력양식
		this.T304 = T304; // 자료정확
		this.T305 = T305; // 보안체계
		this.T306 = T306; // 일정준수
		this.T307 = T307; // 개발시스템.종합

		this.T308 = T308; // 교육자료
		this.T309 = T309; // 메뉴얼
		this.T310 = T310; // UI
		this.T311 = T311; // 호환성
		this.T312 = T312; // 시스템활용지원.종합

		this.T401 = T401; // 응답속도
		this.T402 = T402; // 요구사항
		this.T403 = T403; // 자료정확
		this.T404 = T404; // Sys.안정
		this.T405 = T405; // 사용편의
		this.T406 = T406; // Sys.개선
		this.T407 = T407; // 개발시스템.종합

		this.T501 = T501; // 문서완성
		this.T502 = T502; // 문서이해
		this.T503 = T503; // 문서일치
		this.T504 = T504; // 문서실질
		this.T505 = T505; // 개발문서.종합

		this.T506 = T506; // Sys.구성
		this.T507 = T507; // Sys.확장
		this.T508 = T508; // 보안체계
		this.T509 = T509; // Sys.성능
		this.T510 = T510; // 오류빈도
		this.T511 = T511; // 시스템구조및성능.종합

		this.T512 = T512; // 운영편의
		this.T513 = T513; // 수정용이
		this.T514 = T514; // 표준준수
		this.T515 = T515; // Sys.연동
		this.T516 = T516; // User관리
		this.T517 = T517; // 배치편리
		this.T518 = T518; // 시스템운영편리성.종합
	}
}
