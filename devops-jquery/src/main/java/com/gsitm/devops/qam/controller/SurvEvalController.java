package com.gsitm.devops.qam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.qam.model.vo.SurvEvalFrmVO;
import com.gsitm.devops.qam.service.biz.SurvEvalBiz;

@Controller
public class SurvEvalController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "survEvalFrmVO";
	private static final String CODE = "code";
	
	private static final String FORM_URL_BY_CODE = "/qam/surveval/{code}/form";
	private static final String GET_URL_BY_CODE = "/qam/surveval/{code}/get";
	
	private static final String FORM_PAGE = "qam/surveval/form";
	
	@Autowired
	private SurvEvalBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;	
	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	//설문대상자(T3.사업부, T4.현업사용자, T5.운영자)
	@ModelAttribute("recipients")
	public String getRecipients() {
		return cmmCodeService.getRecipientsString();
	}	
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	/**
	 * @SpecialLogic 만족도 평가등록  조회
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
 	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) String code,
			Model model) {
		SurvEvalFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}
	
	/**
	 * @SpecialLogic 설문결과 가져오기
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
 	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = GET_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String get(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) String code,
			Model model) {
		//설문결과 가져오기
		biz.getSurveySummary(code);
		//만족도 평가 페이지로
		SurvEvalFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}		
}
