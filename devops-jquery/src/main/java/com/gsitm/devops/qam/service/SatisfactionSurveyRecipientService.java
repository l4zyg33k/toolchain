package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.SatisfactionSurveyRecipient;

public interface SatisfactionSurveyRecipientService extends
		SharedService<SatisfactionSurveyRecipient, Long> {

}
