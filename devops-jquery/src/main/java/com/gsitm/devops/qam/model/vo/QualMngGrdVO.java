package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class QualMngGrdVO {
	//프로젝트 ---- start --------------
	//프로젝트 코드
	private String projectCode;
	
	//프로젝트명
	private String projectName;
	
	//프로젝트 시작일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate projectStartDate;	

	//프로젝트 F/U종료일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate projectFollowUpFinishDate;
	//프로젝트 ---- end --------------
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG0; // 진척도.착수
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG1; // 진척도.분석
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG2; // 진척도.설계
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG3; // 진척도.코드
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG4; // 진척도.테스트
	
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate progressG5; // 진척도.이행

	private Double progressRate; // 진척도
	private Number productPoint; // 산출물
	private Number satisfactionPoint; // 만족도
	
	//변경 권한
	//private Boolean editable;
	
	public QualMngGrdVO() {
		super();
	}
	
	@QueryProjection
	public QualMngGrdVO(String projectCode, String projectName,
			LocalDate projectStartDate, LocalDate projectFollowUpFinishDate,
			LocalDate progressG0, LocalDate progressG1, LocalDate progressG2,
			LocalDate progressG3, LocalDate progressG4, LocalDate progressG5,
			Double progressRate, Number productPoint, Number satisfactionPoint) {
		super();
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.projectStartDate = projectStartDate;
		this.projectFollowUpFinishDate = projectFollowUpFinishDate;
		this.progressG0 = progressG0;
		this.progressG1 = progressG1;
		this.progressG2 = progressG2;
		this.progressG3 = progressG3;
		this.progressG4 = progressG4;
		this.progressG5 = progressG5;
		this.progressRate = progressRate;
		this.productPoint = productPoint;
		this.satisfactionPoint = satisfactionPoint;
	}
}
