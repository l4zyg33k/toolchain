package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.SatisfactionSurveyQuestion;

public interface SatisfactionSurveyQuestionService extends
		SharedService<SatisfactionSurveyQuestion, Long> {

}
