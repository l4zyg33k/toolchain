package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.SurvQuesCmdVO;
import com.gsitm.devops.qam.model.vo.SurvQuesGrdVO;

public interface SurvQuesBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public Page<SurvQuesGrdVO> findAll(Pageable pageable, UserDetails user, String code, Long userType, Long surveyType);
	
	public Page<SurvQuesGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, String code, Long userType, Long surveyType);
	
	public void update(SurvQuesCmdVO survQuesCmdVO);
	
	public void delete(SurvQuesCmdVO survQuesCmdVO);
	
	public void create(SurvQuesCmdVO survQuesCmdVO);
}
