package com.gsitm.devops.qam.controller.rest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.SurvResuMultiGrdVO;
import com.gsitm.devops.qam.model.vo.SurvResuShortGrdVO;
import com.gsitm.devops.qam.service.biz.SurvResuBiz;

@RestController
public class SurvResuRestController {
	
	private static final String EXPIRE_ERROR_MSG = "프로젝트 조회 후 설문종료 해주세요.";
	
	@Autowired
	private SurvResuBiz biz; 
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 객관식 설문
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return Page<SurvResuMultiGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/survresu/multipleChoice", method = RequestMethod.POST)
	public Page<SurvResuMultiGrdVO> findMultiAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("userType") String userType,
			@RequestParam("projectCode") String projectCode) {
		return biz.findMultiAll(pageable, user, userType, projectCode);
	}	
	
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 객관식 설문 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @param searchable : 조회조건
	 * @return Page<SurvResuMultiGrdVO>
	 */		
	@RequestMapping(value = "/rest/qam/survresu/multipleChoice", method = RequestMethod.POST, params = { "_search=true" })
	public Page<SurvResuMultiGrdVO> findMultiAll(
			Pageable pageable, Searchable searchable, 
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("userType") String userType,
			@RequestParam("projectCode") String projectCode) {
		return biz.findMultiAll(pageable, user, userType, projectCode, searchable);
	}		
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 주관식 설문
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return Page<SurvResuMultiGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/survresu/shortAnswer", method = RequestMethod.POST)
	public Page<SurvResuShortGrdVO> findShortAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("userType") String userType,
			@RequestParam("projectCode") String projectCode) {
		return biz.findShortAll(pageable, user, userType, projectCode);
	}
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 주관식 설문 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @param searchable : 조회조건
	 * @return Page<SurvResuMultiGrdVO>
	 */	
	@RequestMapping(value = "/rest/qam/survresu/shortAnswer", method = RequestMethod.POST, params = { "_search=true" })
	public Page<SurvResuShortGrdVO> findShortAll(
			Pageable pageable, Searchable searchable, 
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("userType") String userType,
			@RequestParam("projectCode") String projectCode) {
		return biz.findShortAll(pageable, user, userType, projectCode, searchable);
	}	

	/**
	 * @SpecialLogic  프로젝트별 설문 종료
	 * 
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return JqResult
	 */	
	@RequestMapping(value = "/rest/qam/survresu/expire", method = RequestMethod.POST)
	public JqResult formEditing(
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("projectCode") String projectCode) {
		
		// 프로젝트코드가 없는 경우
		if (StringUtils.isBlank(projectCode)) {
			return new JqFailure(EXPIRE_ERROR_MSG);
		}
				
		biz.update(projectCode);
		return new JqSuccess();
	}			
	
	
}
