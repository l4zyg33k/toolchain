package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class CustEvalCmdVO extends CustEvalWebVO {	
	
	//프로젝트
	private Project project;
	
	//수신자
	private Account recipient;
	private String recipientName;
	private String recipientEmail;
	private String recipientTeamName;
	private String recipientPositionName;

	//사용자 유형 (코드)
	private Code userType;

	//메일 발송 날짜
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate sentDate;	
}
