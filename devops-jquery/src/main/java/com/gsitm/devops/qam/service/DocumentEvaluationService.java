package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.DocumentEvaluation;

public interface DocumentEvaluationService extends
		SharedService<DocumentEvaluation, Long> {

}
