/**
 * 품질점검 결과 마스터 엔티티
 * QualityCheckResult.java
 */
package com.gsitm.devops.qam.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class QualityCheckResult extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 품질 점검 일자
	 */
	private LocalDate inspectedDate;

	/**
	 * 품질 점검 제목
	 */
	@Column(length = 64)
	private String title;

	/**
	 * 품질 점검 단계 (코드)
	 */
	@ManyToOne
	private Code stage;

	/**
	 * 품질 점검 유형 (코드)
	 */
	@ManyToOne
	private Code type;

	/**
	 * 품질 점검자
	 */
	@ManyToOne
	private Account inspector;

	/**
	 * 정보 기술 리더
	 */
	@ManyToOne
	private Account il;

	/**
	 * 품질 검토 소요시간(MH)
	 */
	private int hours;

	/**
	 * 회의 안건
	 */
	@OneToMany(orphanRemoval = true)
	private List<QualityCheckResultDetail> details;
}
