package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.qam.model.QDocumentEvaluation;
import com.gsitm.devops.qam.model.QProgressEvaluation;
import com.gsitm.devops.qam.model.QSatisfactionSurveySummary;
import com.gsitm.devops.qam.model.vo.QQualMngFrmVO;
import com.gsitm.devops.qam.model.vo.QQualMngGrdVO;
import com.gsitm.devops.qam.model.vo.QualMngFrmVO;
import com.gsitm.devops.qam.model.vo.QualMngGrdVO;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class QualMngBizImpl implements QualMngBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;


	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectName : 프로젝트명
	 * @return Page<QualMngGrdVO>
	 */	
	@Override
	public Page<QualMngGrdVO> findAll(Pageable pageable, UserDetails user, String projectName) {		
		
		QProject pj = QProject.project;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectName)) {
			//conditions.and(pj.name.contains(projectName)); 
			conditions.and(pj.code.contains(projectName).or(pj.name.contains(projectName))); 
		}
		
		QProgressEvaluation pe = new QProgressEvaluation("sub1"); 
		QDocumentEvaluation de = new QDocumentEvaluation("sub2");
		QSatisfactionSurveySummary ss = new QSatisfactionSurveySummary("sub3");
		
		//쿼리
		JPQLQuery query = new JPAQuery(em).from(pj)
				.where(conditions); 
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Project>(Project.class, "project"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<QualMngGrdVO> search = query
				.listResults(new QQualMngGrdVO(pj.code, pj.name, pj.startDate, pj.followUpStartDate, 
					    new JPASubQuery().from(pe).where(pe.step.eq("G0"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G0-착수
					    new JPASubQuery().from(pe).where(pe.step.eq("G1"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G1-분석
					    new JPASubQuery().from(pe).where(pe.step.eq("G2"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G2-설계
					    new JPASubQuery().from(pe).where(pe.step.eq("G3"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G3-코드
					    new JPASubQuery().from(pe).where(pe.step.eq("G4"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G4-테스트
					    new JPASubQuery().from(pe).where(pe.step.eq("G5"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G5-이행
					    new JPASubQuery().from(pe).where(pe.project.eq(pj.project)).unique(pe.actualRate.sum().divide(pe.planRate
								.sum()).multiply(10000).round().divide(100)),
						new JPASubQuery().from(de).where(de.project.eq(pj.project)).groupBy(de.project).unique(de.weight
								.add(de.point).sum().divide(de.weight.sum())),
						new JPASubQuery().from(ss).where(ss.project.eq(pj.project)).groupBy(ss.project)
								.unique(ss.point.multiply(ss.answerCnt).sum().divide(ss.answerCnt.sum()))
						));
						
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param searchable : jqGrid 조회조건
	 * @param user : 사용자 정보 
	 * @param projectName : 프로젝트명
	 * @return  Page<QualMngGrdVO>
	 */	
	@Override
	public Page<QualMngGrdVO> findAll(Pageable pageable,
			Searchable searchable, UserDetails user, String projectName) {
		PathBuilder<Project> builder = new PathBuilder<Project>(Project.class, "project");
		QProject pj = QProject.project;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectName)) {
			conditions.and(pj.code.contains(projectName).or(pj.name.contains(projectName))); 
		}
		
		QProgressEvaluation pe = new QProgressEvaluation("sub1"); 
		QDocumentEvaluation de = new QDocumentEvaluation("sub2");
		QSatisfactionSurveySummary ss = new QSatisfactionSurveySummary("sub3");
		
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(pj).where(predicate, conditions);
		
		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);
		
		SearchResults<QualMngGrdVO> search = query
				.listResults(new QQualMngGrdVO(pj.code, pj.name, pj.startDate, pj.followUpStartDate, 
					    new JPASubQuery().from(pe).where(pe.step.eq("G0"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G0-착수
					    new JPASubQuery().from(pe).where(pe.step.eq("G1"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G1-분석
					    new JPASubQuery().from(pe).where(pe.step.eq("G2"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G2-설계
					    new JPASubQuery().from(pe).where(pe.step.eq("G3"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G3-코드
					    new JPASubQuery().from(pe).where(pe.step.eq("G4"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G4-테스트
					    new JPASubQuery().from(pe).where(pe.step.eq("G5"), pe.project.eq(pj.project)).unique(pe.actualDate.max()), //G5-이행
					    new JPASubQuery().from(pe).where(pe.project.eq(pj.project)).unique(pe.actualRate.sum().divide(pe.planRate
								.sum()).multiply(10000).round().divide(100)),
						new JPASubQuery().from(de).where(de.project.eq(pj.project)).groupBy(de.project).unique(de.weight
								.add(de.point).sum().divide(de.weight.sum())),
						new JPASubQuery().from(ss).where(ss.project.eq(pj.project)).groupBy(ss.project)
								.unique(ss.point.multiply(ss.answerCnt).sum().divide(ss.answerCnt.sum()))
						));
						
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 상세조회
	 * 
	 * @param code
	 * @return  QualMngFrmVO
	 */		
	@Override
	public QualMngFrmVO findOne(String code) {
		QProject project = QProject.project;
		
		QCode type = new QCode("type");
		QAccount il = new QAccount("il");
		QAccount op = new QAccount("op");
		QAccount ou = new QAccount("ou");
		QAccount qa = new QAccount("qa");
		QCode office = new QCode("office");
		QCode phase = new QCode("phase");
		QCode location = new QCode("location");
		QCode pass = new QCode("pass");
		QCode closeReportType = new QCode("closeReportType");
		
		JPQLQuery query = new JPAQuery(em).from(project)
				.leftJoin(project.type, type).leftJoin(project.il, il)
				.leftJoin(project.op, op).leftJoin(project.ou, ou)
				.leftJoin(project.qa, qa).leftJoin(project.office, office)
				.leftJoin(project.phase, phase)
				.leftJoin(project.location, location)
				.leftJoin(project.pass, pass)
				.leftJoin(project.closeReportType, closeReportType)
				.where(project.code.eq(code));
		
		return query.uniqueResult(new QQualMngFrmVO(project.code, project.name,
				type.id, type.name, project.pmName, project.blName,
				project.blDepartment, il.username, il.name, op.username,
				op.name, ou.username, qa.username, qa.name, office.id,
				office.name, project.startDate, project.finishDate,
				project.actualStartDate, project.actualFinishDate, phase.id,
				phase.name, project.panelStartDate, project.panelFinishDate,
				project.panelActualDate, project.panelActualTime, location.id,
				location.name, project.recommandation, pass.id, pass.name,
				project.followUpStartDate, project.followUpFinishDate,
				project.followUpActualStartDate,
				project.followUpActualFinishDate, project.ouEffort,
				project.ouActualEffort, project.pdEffort,
				project.pdActualEffort, project.processPoint,
				project.testingPoint, project.productPoint,
				project.customerPoint, project.workingHour, project.workingDay,
				project.workingEffort, project.closeReportDate,
				closeReportType.id, closeReportType.name,
				project.openCheckDate, project.openReportDate,
				project.watching, project.satisfactionExpired));
	}	
	
}
