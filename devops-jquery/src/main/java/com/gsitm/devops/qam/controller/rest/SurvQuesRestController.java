package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.SurvQuesCmdVO;
import com.gsitm.devops.qam.model.vo.SurvQuesGrdVO;
import com.gsitm.devops.qam.service.biz.SurvQuesBiz;

@RestController
public class SurvQuesRestController {

	@Autowired
	private SurvQuesBiz biz; 
	
	/**
	 * @SpecialLogic 만족도설문문항관리 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param surveyType : 설문유형
	 * @return Page<SurvQuesGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/survques", method = RequestMethod.POST)
	public Page<SurvQuesGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("code") String code,
			@RequestParam("userType") Long userType,
			@RequestParam("surveyType") Long surveyType) {
		return biz.findAll(pageable, user, code, userType, surveyType);
	}
	
	/**
	 * @SpecialLogic 만족도설문문항관리  조회  (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param searchable : 조회조건
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param surveyType : 설문유형
	 * @return Page<SurvQuesGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/survques", method = RequestMethod.POST, params = { "_search=true" })
	public Page<SurvQuesGrdVO> findAll(
			Pageable pageable, Searchable searchable, 
			@AuthenticationPrincipal UserDetails user,
			@RequestParam("code") String code,
			@RequestParam("userType") Long userType,
			@RequestParam("surveyType") Long surveyType) {
		return biz.findAll(pageable, searchable, user, code, userType, surveyType);
	}	
	
	/**
	 * @SpecialLogic 만족도설문문항관리 - 등록, 수정, 삭제 처리
	 * 
	 * @param oper
	 * @param user
	 * @param survQuesCmdVO
	 * @param result
	 */		
	@RequestMapping(value = "/rest/qam/survques/edit", method = RequestMethod.POST)
	public JqResult formEditing(@RequestParam("oper") String oper,
			@AuthenticationPrincipal UserDetails user,
			SurvQuesCmdVO survQuesCmdVO, BindingResult result) {
		switch (oper) {
		case "add":
			biz.create(survQuesCmdVO);
			break;
		case "edit":
			biz.update(survQuesCmdVO);
			break;
		case "del":
			biz.delete(survQuesCmdVO);
			break;
		}
		
		return new JqSuccess();
	}	
			
}
