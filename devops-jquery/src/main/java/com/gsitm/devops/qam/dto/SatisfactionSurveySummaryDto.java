package com.gsitm.devops.qam.dto;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SatisfactionSurveySummaryDto {
	private Project project;
	private Code userType;
	private double pointAvg; // 평균
	private LocalDate minSurveyDate; // 설문조사시작날짜
	private Long targetCount; // 대상자수
	private Long answerCount; // 응답자수

	@QueryProjection
	public SatisfactionSurveySummaryDto(Project project, Code userType,
			double marksAvg, Long answerCount, LocalDate minSurveyDate,
			Long targetCount) {
		this.project = project;
		this.userType = userType;
		this.pointAvg = marksAvg;
		this.answerCount = answerCount;
		this.minSurveyDate = minSurveyDate;
		this.targetCount = targetCount;
	}

	public float getMarksAvg() {
		return 0;
	}
}
