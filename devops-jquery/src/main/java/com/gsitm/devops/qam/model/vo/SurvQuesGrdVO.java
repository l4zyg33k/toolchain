package com.gsitm.devops.qam.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurvQuesGrdVO {
	
	private Long id;
	
	//사용자 유형 (코드)
	private Long userType;

	//설문 유형 (코드)
	private Long surveyType;

	//문항 유형 (코드)
	private Long questionType;

	//문항 번호
	private Integer no;

	//키워드
	private String keyword;

	//설문 문항
	private String question;	

	
	public SurvQuesGrdVO() {
		super();
	}

	@QueryProjection
	public SurvQuesGrdVO(Long id, Long userType, Long surveyType,
			Long questionType, Integer no, String keyword, String question) {
		super();
		this.id = id;
		this.userType = userType;
		this.surveyType = surveyType;
		this.questionType = questionType;
		this.no = no;
		this.keyword = keyword;
		this.question = question;
	}
}
