package com.gsitm.devops.qam.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SatisfactionSurveySummaryListDto {
	// 식별자
	private Long id;

	// 프로젝트
	private Project project;

	// 응답자유형

	private Code userType;

	// 조사예정일
	private Date surveyStartDate;

	// 실제조사일
	private Date surveyRealDate;

	// 대상자수
	private Long targetCount;

	// 응답수
	private Long answerCount;

	// 만족도
	private float satisfaction;

	// 만족도
	private String comment;

	@QueryProjection
	public SatisfactionSurveySummaryListDto(Project project, Code userType,
			Date surveyStartDate, Date surveyRealDate, Long targetCount,
			Long answerCount, float satisfaction, String comment) {
		this.project = project;
		this.userType = userType;
		this.surveyStartDate = surveyStartDate;
		this.surveyRealDate = surveyRealDate;
		this.targetCount = targetCount;
		this.answerCount = answerCount;
		this.satisfaction = satisfaction;
		this.comment = comment;
	}

}
