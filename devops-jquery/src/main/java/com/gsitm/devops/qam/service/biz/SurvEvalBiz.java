package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.qam.model.vo.SurvEvalCmdVO;
import com.gsitm.devops.qam.model.vo.SurvEvalFrmVO;
import com.gsitm.devops.qam.model.vo.SurvEvalGrdVO;

public interface SurvEvalBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public SurvEvalFrmVO findOne(String code);	
	
	public Page<SurvEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode);	
	
	public void update(SurvEvalCmdVO survEvalCmdVO);
	
	public void getSurveySummary(String code);	
}
