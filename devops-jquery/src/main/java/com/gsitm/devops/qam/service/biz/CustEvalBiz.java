package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.model.vo.CustEvalCmdVO;
import com.gsitm.devops.qam.model.vo.CustEvalGrdVO;

public interface CustEvalBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public void update(CustEvalCmdVO docuEvalCmdVO);
	
	public void create(CustEvalCmdVO docuEvalCmdVO);

	public void delete(CustEvalCmdVO docuEvalCmdVO);
	
	public Page<CustEvalGrdVO> findAll(Pageable pageable, String projectCode);
	
	public Page<CustEvalGrdVO> findAll(Pageable pageable, Searchable searchable, String projectCode);
	
	public void sendMail(Project project, String sendList);
	
}
