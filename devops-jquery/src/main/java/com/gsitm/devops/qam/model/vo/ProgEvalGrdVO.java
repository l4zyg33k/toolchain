package com.gsitm.devops.qam.model.vo;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProgEvalGrdVO {
	
	private Long id;
	
	//프로젝트 코드
	private String projectCode;
	
	//프로젝트명
	private String projectName;

	//진척도 단계 (단계)
	private String step;

	//계획 측정일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate planDate;

	//실제 측정일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate actualDate;

	//계획 진척률
	private Double planRate;

	//실제 진척률
	private Double actualRate;

	//진척도 평가 사유
	@Column(length = 100)
	private String reason;
	
	public ProgEvalGrdVO() {
		super();
	}
	
	@QueryProjection
	public ProgEvalGrdVO(Long id, String projectCode, String projectName,
			String step, LocalDate planDate, LocalDate actualDate,
			Double planRate, Double actualRate, String reason) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.step = step;
		this.planDate = planDate;
		this.actualDate = actualDate;
		this.planRate = planRate;
		this.actualRate = actualRate;
		this.reason = reason;
	}
}
