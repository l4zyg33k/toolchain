package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
@SuppressWarnings("serial")
public class SurvEvalCmdVO extends SurvEvalWebVO {	
	//조사예정일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate planDate;

	//실제조사일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualDate;

	//대상자
	private Long recipientCnt;

	//응답수
	private Long answerCnt;

	//만족도
	private Float point;
	
	//의견
	private String comment;
}