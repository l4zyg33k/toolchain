package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.QualMngGrdVO;
import com.gsitm.devops.qam.service.biz.QualMngBiz;

@RestController
public class QualMngRestController {

	@Autowired
	private QualMngBiz biz; 
	
	/**
	 * @SpecialLogic 프로젝트 품질관리 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectName : 프로젝트명
	 * @return Page<QualMngGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/qualmng", method = RequestMethod.POST)
	public Page<QualMngGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectName", defaultValue = "", required = false) String projectName) {
		return biz.findAll(pageable, user, projectName);
	}
	
	/**
	 * @SpecialLogic 프로젝트 품질관리 다건 조회  (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectName : 프로젝트명
	 * @return Page<QualMngGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/qualmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<QualMngGrdVO> findAll(
			Pageable pageable, Searchable searchable, 
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectName", defaultValue = "", required = false) String projectName) {
		return biz.findAll(pageable, searchable, user, projectName);
	}	

}
