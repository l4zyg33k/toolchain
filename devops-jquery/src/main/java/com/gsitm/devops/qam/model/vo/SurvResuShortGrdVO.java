package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SurvResuShortGrdVO {
	
	private Long id;
	
	//프로젝트
	private Project project;

	//대상자 정보
	private String username;
	private String name;
	private Code position;
	private Code team;

	//문항 번호
	private Integer no;

	//사용자 유형 (코드)
	private Code userType;

	//설문 유형 (코드)
	private Code surveyType;

	//키워드
	private String keyword;

	//설문 문항
	private String question;

	//결과 점수
	private Integer point;

	//결과 내용
	private String content;

	
	public SurvResuShortGrdVO() {
		super();
	}

	@QueryProjection
	public SurvResuShortGrdVO(Long id, Project project, String username,
			String name, Code position, Code team, Integer no, Code userType,
			Code surveyType, String keyword, String question, Integer point,
			String content) {
		super();
		this.id = id;
		this.project = project;
		this.username = username;
		this.name = name;
		this.position = position;
		this.team = team;
		this.no = no;
		this.userType = userType;
		this.surveyType = surveyType;
		this.keyword = keyword;
		this.question = question;
		this.point = point;
		this.content = content;
	}
}
