package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.QualityCheckResultDetail;

public interface QualityCheckResultDetailService extends
		SharedService<QualityCheckResultDetail, Long> {

}
