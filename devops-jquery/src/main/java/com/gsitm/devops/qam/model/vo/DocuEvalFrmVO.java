package com.gsitm.devops.qam.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class DocuEvalFrmVO extends QualMngWebVO {
	
	//프로젝트 코드
	private String code;
	
	//프로젝트 명
	private String name;
	
	//종합평점
	private String summaryPoint;

	public DocuEvalFrmVO(int page, int rowNum, String rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}
	
	@QueryProjection
	public DocuEvalFrmVO(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
}