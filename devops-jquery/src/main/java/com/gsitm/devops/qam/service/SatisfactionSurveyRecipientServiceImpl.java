package com.gsitm.devops.qam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.SatisfactionSurveyRecipient;
import com.gsitm.devops.qam.repository.SatisfactionSurveyRecipientRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 산출물 측정
 */
@Service
@Transactional(readOnly = true)
public class SatisfactionSurveyRecipientServiceImpl implements
		SatisfactionSurveyRecipientService {

	@Autowired
	private SatisfactionSurveyRecipientRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public SatisfactionSurveyRecipient findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public SatisfactionSurveyRecipient findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyRecipient> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<SatisfactionSurveyRecipient> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<SatisfactionSurveyRecipient> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyRecipient> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<SatisfactionSurveyRecipient> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<SatisfactionSurveyRecipient> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public SatisfactionSurveyRecipient save(
			SatisfactionSurveyRecipient SatisfactionSurveyMember) {

		return repository.save(SatisfactionSurveyMember);
	}

	@Transactional
	@Override
	public <S extends SatisfactionSurveyRecipient> List<S> save(
			Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(SatisfactionSurveyRecipient SatisfactionSurveyMember) {

		repository.delete(SatisfactionSurveyMember);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends SatisfactionSurveyRecipient> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

}
