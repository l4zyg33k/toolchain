package com.gsitm.devops.qam.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.qam.model.SatisfactionSurveyRecipient;

@Repository
public interface SatisfactionSurveyRecipientRepository extends
		SharedRepository<SatisfactionSurveyRecipient, Long> {
}