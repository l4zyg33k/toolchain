package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.qam.model.vo.SurvEvalCmdVO;
import com.gsitm.devops.qam.model.vo.SurvEvalGrdVO;
import com.gsitm.devops.qam.service.biz.SurvEvalBiz;

@RestController
public class SurvEvalRestController {

	@Autowired
	private SurvEvalBiz biz; 
	
	/**
	 * @SpecialLogic 만족도 평가등록 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<SurvEvalGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/surveval", method = RequestMethod.POST)
	public Page<SurvEvalGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, user, projectCode);
	}
	
	/**
	 * @SpecialLogic 만족도 평가등록 수정
	 * 
	 * @param oper
	 * @param survEvalCmdVO
	 */	
	@RequestMapping(value = "/rest/qam/surveval/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			SurvEvalCmdVO survEvalCmdVO) {
		switch (oper) {
		case "edit":
			biz.update(survEvalCmdVO);
			break;
		}
	}	
			
}
