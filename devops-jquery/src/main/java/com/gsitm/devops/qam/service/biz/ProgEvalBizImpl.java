package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.qam.model.ProgressEvaluation;
import com.gsitm.devops.qam.model.QProgressEvaluation;
import com.gsitm.devops.qam.model.vo.ProgEvalCmdVO;
import com.gsitm.devops.qam.model.vo.ProgEvalFrmVO;
import com.gsitm.devops.qam.model.vo.ProgEvalGrdVO;
import com.gsitm.devops.qam.model.vo.QProgEvalFrmVO;
import com.gsitm.devops.qam.model.vo.QProgEvalGrdVO;
import com.gsitm.devops.qam.service.ProgressEvaluationService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class ProgEvalBizImpl implements ProgEvalBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private ProgressEvaluationService progressEvaluationService;
	
	@Autowired
	private CodeService codeService;
	
	@Autowired
	private ProjectService projectService;	


	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}


	/**
	 * @SpecialLogic 진척도 평가 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<ProgEvalGrdVO>
	 */	
	@Override
	public Page<ProgEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode) {	
		QProgressEvaluation pe = QProgressEvaluation.progressEvaluation;
		
		JPQLQuery query = new JPAQuery(em).from(pe)
				.where(pe.project.code.eq(projectCode));
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<ProgressEvaluation>(ProgressEvaluation.class, "progressEvaluation"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<ProgEvalGrdVO> search = query
				.listResults(new QProgEvalGrdVO(pe.id, pe.project.code, pe.project.name,
						pe.step, pe.planDate, pe.actualDate, pe.planRate, 
						pe.actualRate, pe.reason));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	/**
	 * @SpecialLogic 진척도 평가 결과 상세조회
	 * 
	 * @param code
	 * @return  ProgEvalFrmVO
	 */		
	@Override
	public ProgEvalFrmVO findOne(String code) {
		QProgressEvaluation pe = QProgressEvaluation.progressEvaluation;
		
		//진철도 평가 건수를 확인 하고 없으면 저장 처리 한다.
		if(progressEvaluationService.count(pe.project.code.eq(code)) <= 0){
			QCode qCode = QCode.code;
			Project project = projectService.findOne(code);
			Iterable<Code> codeList = codeService.findAll(qCode.parent.value.eq("G6"));
			for (Code codeVO : codeList) {
				ProgressEvaluation newData = new ProgressEvaluation();
				newData.setProject(project);
				newData.setStep(codeVO.getValue());
				progressEvaluationService.save(newData);
			}
		}

		JPQLQuery query = new JPAQuery(em).from(pe)
				.where(pe.project.code.eq(code))
				.distinct();
		
		return query.uniqueResult(new QProgEvalFrmVO(pe.project.code, pe.project.name));
	}
	
	/**
	 * @SpecialLogic 계획대비 진철률  계산
	 * 
	 * @param code
	 * @return  double
	 */		
	@Override
	public String getProgressPerPlan(String code) {
		QProgressEvaluation pe = QProgressEvaluation.progressEvaluation;
		
		try{
			JPAQuery query = new JPAQuery(em);
			double result1 = query
					.from(pe)
					.where(pe.project.code.eq(code))
					.setHint("org.hibernate.cacheable", "true")
					.uniqueResult(pe.actualRate.sum().coalesce((double) 0));
			
			JPAQuery query2 = new JPAQuery(em);
			double result2 = query2
					.from(pe)
					.where(pe.project.code.eq(code))
					.setHint("org.hibernate.cacheable", "true")
					.uniqueResult(pe.planRate.sum().coalesce((double) 0));
			
			if (result1 == 0 || result2 == 0) {
				return "0 %";
			}
			
			double result = Math.round(10000 * result1 / result2);
			return (result/100) + " %";
		}catch(Exception e){
			return "0 %";
		}
	}
	
	/**
	 * @SpecialLogic 진척도 평가 결과 수정
	 * 
	 * @param   progEvalCmdVO
	 */		
	@Override
	public void update(ProgEvalCmdVO progEvalCmdVO) {
		ProgressEvaluation pe = progressEvaluationService.findOne(progEvalCmdVO.getId());
		BeanUtils.copyProperties(progEvalCmdVO, pe);
		progressEvaluationService.save(pe);
	}	
	
}
