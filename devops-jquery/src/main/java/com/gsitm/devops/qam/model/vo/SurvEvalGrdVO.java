package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SurvEvalGrdVO {
	
	private Long id;
	
	//조사 대상(코드)
	private String userType;
	
	//조사 예정일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate planDate;
	
	//실제 조사일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate actualDate;
	
	//대상자수
	private Long recipientCnt;
	
	//응답수
	private Long answerCnt;
	
	//만족도
	private Float point;
	
	//의견
	private String comment;
	
	public SurvEvalGrdVO() {
		super();
	}
	
	@QueryProjection
	public SurvEvalGrdVO(Long id, String userType, LocalDate planDate,
			LocalDate actualDate, Long recipientCnt, Long answerCnt,
			Float point, String comment) {
		super();
		this.id = id;
		this.userType = userType;
		this.planDate = planDate;
		this.actualDate = actualDate;
		this.recipientCnt = recipientCnt;
		this.answerCnt = answerCnt;
		this.point = point;
		this.comment = comment;
	}
}
