package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class QualChecDetlGrdVO {		
	//id
	private Long id;
	
	//결과유형(코드)
	private Long result;
	
	//부적합내용
	private String content;
	
	//시정조치계획/결과
	private String action;	

	//권고일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate recommendedDate;

	//합의일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate appointedDate;
	
	//완료일
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate completedDate;	
	
	public QualChecDetlGrdVO() {
		super();
	}
	
	@QueryProjection
	public QualChecDetlGrdVO(Long id, Long result, String content,
			String action, LocalDate recommendedDate, LocalDate appointedDate,
			LocalDate completedDate) {
		super();
		this.id = id;
		this.result = result;
		this.content = content;
		this.action = action;
		this.recommendedDate = recommendedDate;
		this.appointedDate = appointedDate;
		this.completedDate = completedDate;
	}
}
