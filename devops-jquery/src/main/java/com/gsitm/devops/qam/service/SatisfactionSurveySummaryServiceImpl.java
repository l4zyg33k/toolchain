package com.gsitm.devops.qam.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.dto.QSatisfactionSurveySummaryDto;
import com.gsitm.devops.qam.dto.SatisfactionSurveySummaryDto;
import com.gsitm.devops.qam.dto.SatisfactionSurveySummaryFormDto;
import com.gsitm.devops.qam.model.QSatisfactionSurveyRecipient;
import com.gsitm.devops.qam.model.QSatisfactionSurveyResult;
import com.gsitm.devops.qam.model.QSatisfactionSurveySummary;
import com.gsitm.devops.qam.model.SatisfactionSurveyResult;
import com.gsitm.devops.qam.model.SatisfactionSurveySummary;
import com.gsitm.devops.qam.repository.SatisfactionSurveySummaryRepository;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.ConstructorExpression;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 산출물 측정
 */
@Service
@Transactional(readOnly = true)
public class SatisfactionSurveySummaryServiceImpl implements
		SatisfactionSurveySummaryService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private SatisfactionSurveySummaryRepository repository;

	@Autowired
	private CodeService codeService;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public SatisfactionSurveySummary findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public SatisfactionSurveySummary findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveySummary> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<SatisfactionSurveySummary> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<SatisfactionSurveySummary> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveySummary> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<SatisfactionSurveySummary> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<SatisfactionSurveySummary> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public SatisfactionSurveySummary save(
			SatisfactionSurveySummary SatisfactionSurveySummary) {

		return repository.save(SatisfactionSurveySummary);
	}

	@Transactional
	@Override
	public <S extends SatisfactionSurveySummary> List<S> save(
			Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(SatisfactionSurveySummary SatisfactionSurveySummary) {

		repository.delete(SatisfactionSurveySummary);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends SatisfactionSurveySummary> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

	@Override
	@Transactional
	public long saveSummary(Project project) {
		// 삭제
		QSatisfactionSurveySummary qSatisfactionSurveySummary = QSatisfactionSurveySummary.satisfactionSurveySummary;
		Predicate Predicate = qSatisfactionSurveySummary.project.eq(project);
		this.delete(this.findAll(Predicate));

		QSatisfactionSurveyResult qSatisfactionSurveyResult = QSatisfactionSurveyResult.satisfactionSurveyResult;
		JPQLQuery query = new JPAQuery(entityManager);
		// 서브쿼리 사용
		QSatisfactionSurveyRecipient memberSub = new QSatisfactionSurveyRecipient(
				"sub1");

		// 평균계산
		List<SatisfactionSurveySummaryDto> result = query
				.from(qSatisfactionSurveyResult)
				.where(qSatisfactionSurveyResult.project.eq(project))
				.groupBy(qSatisfactionSurveyResult.project,
						qSatisfactionSurveyResult.userType)
				.list(new QSatisfactionSurveySummaryDto(
						qSatisfactionSurveyResult.project,
						qSatisfactionSurveyResult.userType,
						qSatisfactionSurveyResult.point.avg(),
						qSatisfactionSurveyResult.count(),
						new JPASubQuery()
								.from(memberSub)
								.where(memberSub.project
										.eq(qSatisfactionSurveyResult.project),
										memberSub.userType
												.eq(qSatisfactionSurveyResult.userType))
								.groupBy(memberSub.project, memberSub.userType)
								.unique(memberSub.sentDate.min()),
						new JPASubQuery()
								.from(memberSub)
								.where(memberSub.project
										.eq(qSatisfactionSurveyResult.project),
										memberSub.userType
												.eq(qSatisfactionSurveyResult.userType))
								.groupBy(memberSub.project, memberSub.userType)
								.unique(memberSub.recipient.count())));
		for (SatisfactionSurveySummaryDto dtoData : result) {
			// 신규값 설정
			SatisfactionSurveySummary newData = new SatisfactionSurveySummary();
			newData.setProject(dtoData.getProject());
			newData.setUserType(dtoData.getUserType());
			// 프로젝트 종료일 + 1week
			if (dtoData.getProject().getFinishDate() != null) {
				newData.setPlanDate(dtoData.getProject().getFinishDate()
						.plusWeeks(1));

			}

			// 설문시작일
			if (dtoData.getMinSurveyDate() != null) {
				newData.setPlanDate(dtoData.getMinSurveyDate());
			}
			// 대상자, 응답자
			newData.setRecipientCnt(dtoData.getTargetCount());
			newData.setAnswerCnt(dtoData.getAnswerCount());
			// 평균점수
			newData.setPoint(dtoData.getMarksAvg());

			// 주관식결과
			JPQLQuery commentQuery = new JPAQuery(entityManager);
			List<SatisfactionSurveyResult> commentList = commentQuery
					.from(qSatisfactionSurveyResult)
					.where(qSatisfactionSurveyResult.project.eq(dtoData
							.getProject()),
							qSatisfactionSurveyResult.userType.eq(dtoData
									.getUserType()),
							qSatisfactionSurveyResult.content
									.isNotEmpty())
					.list(qSatisfactionSurveyResult);
			List<String> comment = new ArrayList<String>();
			for (SatisfactionSurveyResult commentData : commentList) {
				comment.add(commentData.getContent());
			}
			newData.setComment(Joiner.on("<br>").join(comment));
			// 저장

			this.save(newData);
		}

		return 1;
	}

	@SuppressWarnings("static-access")
	@Override
	public Iterable<SatisfactionSurveySummaryFormDto> findAll2(
			Predicate predicate) {
		JPQLQuery query = new JPAQuery(entityManager);
		QSatisfactionSurveySummary qSatisfactionSurveySummary = QSatisfactionSurveySummary.satisfactionSurveySummary;

		QCode qCode = QCode.code;
		Iterable<SatisfactionSurveySummaryFormDto> result = null;

		result = query
				.from(qCode)
				.where(qCode.parent.value.eq("T1"))
				.list(ConstructorExpression.create(
						SatisfactionSurveySummaryFormDto.class,
						new JPASubQuery()
								.from(qSatisfactionSurveySummary)
								.where(predicate,
										qSatisfactionSurveySummary.userType
												.eq(QCode.code))
								.unique(qSatisfactionSurveySummary.id),
						QCode.code,
						new JPASubQuery()
								.from(qSatisfactionSurveySummary)
								.where(predicate,
										qSatisfactionSurveySummary.userType
												.eq(QCode.code))
								.unique(qSatisfactionSurveySummary)));
		return result;
	}

}
