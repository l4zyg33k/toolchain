package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.model.QualityCheckResult;

public interface QualityCheckResultService extends
		SharedService<QualityCheckResult, Long> {

}
