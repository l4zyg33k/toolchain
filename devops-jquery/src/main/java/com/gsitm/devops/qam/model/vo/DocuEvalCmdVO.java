package com.gsitm.devops.qam.model.vo;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class DocuEvalCmdVO extends DocuEvalWebVO {	
	
	//프로젝트
	private Project projectCode;

	//문서 명칭
	@Column(length = 100)
	private String name;

	//가중치
	private Float weight;

	//평점
	private Float point;
}
