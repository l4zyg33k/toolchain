package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class QualChecFrmVO extends QualChecWebVO {	
	
	//id
	private Long id;
	
	//프로젝트
	private String project;
	private String projectName;
	private String iLname;
	private String il; //정보 기술 리더
	
	//품질 점검 제목
	private String title;	

	//품질 점검 일자
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate inspectedDate;

	//품질 점검 유형
	private Long type;
	
	//품질 점검 단계
	private Long stage;	

	//품질 점검자
	private String inspector; // username
	private String inspectorName; // name	
	
	//품질 검토 소요시간(MH)
	private int hours;
	
	private int requiredCount;
	private int opinionCount;
	private int mainPage;
	private int mainRowNum;
	
	public QualChecFrmVO(String project, String projectName, String il, String iLname, int page, int rowNum, 
			long rowId, int mainPage, int mainRowNum) {
		super();
		this.project = project;
		this.projectName = projectName;
		this.il = il;
		this.iLname = iLname;
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
		this.mainPage = mainPage;
		this.mainRowNum = mainRowNum;
	}
	
	@QueryProjection
	public QualChecFrmVO(Long id, String project, String projectName,
			String iLname, String il, String title, LocalDate inspectedDate,
			Long type, Long stage, String inspector, String inspectorName,
			int hours) {
		super();
		this.id = id;
		this.project = project;
		this.projectName = projectName;
		this.iLname = iLname;
		this.il = il;
		this.title = title;
		this.inspectedDate = inspectedDate;
		this.type = type;
		this.stage = stage;
		this.inspector = inspector;
		this.inspectorName = inspectorName;
		this.hours = hours;
	}
}
