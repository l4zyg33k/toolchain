package com.gsitm.devops.qam.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class SurvEvalFrmVO extends QualMngWebVO {
	
	//프로젝트 코드
	private String code;
	
	//프로젝트 명
	private String name;
	
	//종합만족도
	private String summaryPoint;

	public SurvEvalFrmVO(int page, int rowNum, String rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}
	
	@QueryProjection
	public SurvEvalFrmVO(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
}