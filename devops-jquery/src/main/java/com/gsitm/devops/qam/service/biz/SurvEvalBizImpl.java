package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.qam.model.QSatisfactionSurveySummary;
import com.gsitm.devops.qam.model.SatisfactionSurveySummary;
import com.gsitm.devops.qam.model.vo.QSurvEvalFrmVO;
import com.gsitm.devops.qam.model.vo.QSurvEvalGrdVO;
import com.gsitm.devops.qam.model.vo.SurvEvalCmdVO;
import com.gsitm.devops.qam.model.vo.SurvEvalFrmVO;
import com.gsitm.devops.qam.model.vo.SurvEvalGrdVO;
import com.gsitm.devops.qam.service.SatisfactionSurveySummaryService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class SurvEvalBizImpl implements SurvEvalBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private SatisfactionSurveySummaryService summaryService;
	
	@Autowired
	private CodeService codeService;
	
	@Autowired
	private ProjectService projectService;		


	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}
	
	/**
	 * @SpecialLogic 만족도 평가등록  조회
	 * 
	 * @param code
	 * @return  SurvEvalFrmVO
	 */		
	@Override
	public SurvEvalFrmVO findOne(String code) {
		QSatisfactionSurveySummary ss = QSatisfactionSurveySummary.satisfactionSurveySummary;
		
		// 만족도집계 데이터 사용자 유형에 따라 자동 생성
		if(summaryService.count(ss.project.code.eq(code)) <= 0){
			QCode qCode = QCode.code;
			Project project = projectService.findOne(code);
			Iterable<Code> codeList = codeService.findAll(qCode.parent.value.eq("T1"));
			for (Code codeVO : codeList) {
				SatisfactionSurveySummary newData = new SatisfactionSurveySummary();
				newData.setProject(project);
				newData.setUserType(codeVO);
				summaryService.save(newData);
			}
		}

		JPQLQuery query = new JPAQuery(em).from(ss)
				.where(ss.project.code.eq(code))
				.distinct();
		
		return query.uniqueResult(new QSurvEvalFrmVO(ss.project.code, ss.project.name));
	}
	
	/**
	 * @SpecialLogic 만족도 평가등록 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<ProgEvalGrdVO>
	 */	
	@Override
	public Page<SurvEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode) {	
		QSatisfactionSurveySummary ss = QSatisfactionSurveySummary.satisfactionSurveySummary;
		
		JPQLQuery query = new JPAQuery(em).from(ss)
				.where(ss.project.code.eq(projectCode));
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<SatisfactionSurveySummary>(SatisfactionSurveySummary.class, "satisfactionSurveySummary"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<SurvEvalGrdVO> search = query
				.listResults(new QSurvEvalGrdVO(ss.id, ss.userType.value, ss.planDate,
						ss.actualDate, ss.recipientCnt, ss.answerCnt, ss.point.coalesce((float)0), ss.comment));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}	
	
	/**
	 * @SpecialLogic 만족도 평가등록  수정
	 * 
	 * @param   survEvalCmdVO
	 */		
	@Override
	public void update(SurvEvalCmdVO survEvalCmdVO) {
		SatisfactionSurveySummary ss = summaryService.findOne(survEvalCmdVO.getId());
		BeanUtils.copyProperties(survEvalCmdVO, ss);
		summaryService.save(ss);
	}	
	
	@Override
	public void getSurveySummary(String code){
		summaryService.saveSummary(projectService.findOne(code));
	}
	
}
