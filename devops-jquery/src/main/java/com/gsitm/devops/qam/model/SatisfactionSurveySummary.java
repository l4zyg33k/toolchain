package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

/**
 * 설문결과 종합
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class SatisfactionSurveySummary extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 응답자 유형 (코드)
	 */
	@ManyToOne
	private Code userType;

	/**
	 * 조사 예정 일자
	 */
	private LocalDate planDate;

	/**
	 * 실제 조사 일자
	 */
	private LocalDate actualDate;

	/**
	 * 대상자 수
	 */
	private Long recipientCnt;

	/**
	 * 응답 수
	 */
	private Long answerCnt;

	/**
	 * 만족도
	 */
	private Float point;

	/**
	 * 만족도 의견
	 */
	@Column(length = 100)
	private String comment;
}
