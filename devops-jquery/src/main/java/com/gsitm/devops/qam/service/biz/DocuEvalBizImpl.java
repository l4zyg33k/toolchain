package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.qam.model.DocumentEvaluation;
import com.gsitm.devops.qam.model.QDocumentEvaluation;
import com.gsitm.devops.qam.model.vo.DocuEvalCmdVO;
import com.gsitm.devops.qam.model.vo.DocuEvalFrmVO;
import com.gsitm.devops.qam.model.vo.DocuEvalGrdVO;
import com.gsitm.devops.qam.model.vo.QDocuEvalFrmVO;
import com.gsitm.devops.qam.model.vo.QDocuEvalGrdVO;
import com.gsitm.devops.qam.service.DocumentEvaluationService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class DocuEvalBizImpl implements DocuEvalBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private DocumentEvaluationService documentEvaluationService;
	
	@Autowired
	private CodeService codeService;
	
	@Autowired
	private ProjectService projectService;	


	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}
	
	/**
	 * @SpecialLogic 산출물 측정결과 등록 상세조회
	 * 
	 * @param code
	 * @return  DocuEvalFrmVO
	 */		
	@Override
	public DocuEvalFrmVO findOne(String code) {
		QProject pj = QProject.project;

		JPQLQuery query = new JPAQuery(em).from(pj)
				.where(pj.code.eq(code))
				.distinct();
		
		return query.uniqueResult(new QDocuEvalFrmVO(pj.code, pj.name));
	}
	
	/**
	 * @SpecialLogic 산출물 측정결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<DocuEvalGrdVO>
	 */	
	@Override
	public Page<DocuEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode) {	
		QDocumentEvaluation de = QDocumentEvaluation.documentEvaluation;
		
		JPQLQuery query = new JPAQuery(em).from(de)
				.where(de.project.code.eq(projectCode));
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<DocumentEvaluation>(DocumentEvaluation.class, "documentEvaluation"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<DocuEvalGrdVO> search = query
				.listResults(new QDocuEvalGrdVO(de.id, de.project.code, de.project.name,
						de.name, de.weight, de.point));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	/**
	 * @SpecialLogic 산출물 측정결과 수정
	 * 
	 * @param   docuEvalCmdVO
	 */		
	@Override
	public void update(DocuEvalCmdVO docuEvalCmdVO) {
		DocumentEvaluation de = documentEvaluationService.findOne(docuEvalCmdVO.getId());
		BeanUtils.copyProperties(docuEvalCmdVO, de);
		documentEvaluationService.save(de);
	}

	/**
	 * @SpecialLogic 산출물 측정결과 삭제
	 * 
	 * @param docuEvalCmdVO
	 */		
	@Override
	public void delete(DocuEvalCmdVO docuEvalCmdVO) {
		documentEvaluationService.delete(docuEvalCmdVO.getId());
	}
	
	/**
	 * @SpecialLogic 산출물 측정결과 저장
	 * 
	 * @param docuEvalCmdVO
	 * @param projectCode
	 */		
	@Override
	public void create(DocuEvalCmdVO docuEvalCmdVO, String projectCode) {
		DocumentEvaluation de = new DocumentEvaluation();
		BeanUtils.copyProperties(docuEvalCmdVO, de);
		de.setProject(projectService.findOne(projectCode));
		documentEvaluationService.save(de);
	}	
	
}
