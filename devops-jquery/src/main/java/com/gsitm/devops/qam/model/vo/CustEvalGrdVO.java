package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class CustEvalGrdVO {
	
	private Long id;
	
	//프로젝트
	private String project;
	
	//수신자
	private String recipient;
	private String recipientName;
	private String recipientEmail;
	private String recipientTeamName;
	private String recipientPositionName;

	//사용자 유형 (코드)
	private Long userType;
	

	//메일 발송 날짜
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate sentDate;	

	
	public CustEvalGrdVO() {
		super();
	}

	@QueryProjection
	public CustEvalGrdVO(Long id, String project, String recipient,
			String recipientName, String recipientEmail,
			String recipientTeamName, String recipientPositionName,
			Long userType, LocalDate sentDate) {
		super();
		this.id = id;
		this.project = project;
		this.recipient = recipient;
		this.recipientName = recipientName;
		this.recipientEmail = recipientEmail;
		this.recipientTeamName = recipientTeamName;
		this.recipientPositionName = recipientPositionName;
		this.userType = userType;
		this.sentDate = sentDate;
	}
}
