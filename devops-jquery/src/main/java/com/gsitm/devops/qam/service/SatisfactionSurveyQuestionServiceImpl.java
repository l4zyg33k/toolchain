package com.gsitm.devops.qam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.SatisfactionSurveyQuestion;
import com.gsitm.devops.qam.repository.SatisfactionSurveyQuestionRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 산출물 측정
 */
@Service
@Transactional(readOnly = true)
public class SatisfactionSurveyQuestionServiceImpl implements
		SatisfactionSurveyQuestionService {

	@Autowired
	private SatisfactionSurveyQuestionRepository repository;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public SatisfactionSurveyQuestion findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public SatisfactionSurveyQuestion findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyQuestion> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<SatisfactionSurveyQuestion> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<SatisfactionSurveyQuestion> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<SatisfactionSurveyQuestion> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<SatisfactionSurveyQuestion> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<SatisfactionSurveyQuestion> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public SatisfactionSurveyQuestion save(
			SatisfactionSurveyQuestion SatisfactionSurveyQuestion) {

		return repository.save(SatisfactionSurveyQuestion);
	}

	@Transactional
	@Override
	public <S extends SatisfactionSurveyQuestion> List<S> save(
			Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(SatisfactionSurveyQuestion SatisfactionSurveyQuestion) {

		repository.delete(SatisfactionSurveyQuestion);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends SatisfactionSurveyQuestion> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

}
