package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.qam.model.vo.ProgEvalCmdVO;
import com.gsitm.devops.qam.model.vo.ProgEvalFrmVO;
import com.gsitm.devops.qam.model.vo.ProgEvalGrdVO;

public interface ProgEvalBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public ProgEvalFrmVO findOne(String code);
	
	public String getProgressPerPlan(String code);
	
	public Page<ProgEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode);

	public void update(ProgEvalCmdVO progEvalCmdVO);
}
