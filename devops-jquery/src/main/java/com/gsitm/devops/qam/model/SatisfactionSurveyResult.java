package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

/**
 * 프로젝트 설문결과
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class SatisfactionSurveyResult extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 대상자 정보
	 */
	private String username;
	private String name;
	private Code position;
	private Code team;

	/**
	 * 문항 번호
	 */
	private Integer no;

	/**
	 * 사용자 유형 (코드)
	 */
	@ManyToOne
	private Code userType;

	/**
	 * 설문 유형 (코드)
	 */
	@ManyToOne
	private Code surveyType;

	/**
	 * 키워드
	 */
	@Column(length = 64)
	private String keyword;

	/**
	 * 설문 문항
	 */
	@Column(length = 100)
	private String question;

	/**
	 * 결과 점수
	 */
	private Integer point;

	/**
	 * 결과 내용
	 */
	@Column(length = 100)
	private String content;
}
