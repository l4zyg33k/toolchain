package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.pms.model.Project;

@Getter
@Setter
@SuppressWarnings("serial")
public class CustEvalFrmVO extends QualMngWebVO {
	
	//프로젝트
	private Project project;

	public CustEvalFrmVO(Project project, int page, int rowNum, String rowId) {
		super();
		this.project = project;
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}
}