package com.gsitm.devops.qam.controller;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.qam.model.vo.QualMngFrmVO;
import com.gsitm.devops.qam.service.biz.QualMngBiz;

@Controller
public class QualMngController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "qualMngFrmVO";
	private static final String CODE = "code";

	private static final String INDEX_URL = "/qam/qualmng";
	private static final String VIEW_URL_BY_CODE = "/qam/qualmng/{code}/view";

	private static final String INDEX_PAGE = "qam/qualmng/index";
	private static final String VIEW_PAGE = "qam/qualmng/view";

	@Autowired
	private QualMngBiz biz;

	// 메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}

	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}

	@ModelAttribute("icon")
	public String getIcon(@AuthenticationPrincipal UserDetails user) {
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains(Roles.ROLE_ADMIN)
				|| authorities.contains(Roles.ROLE_QA)) {
			return "ui-icon-pencil";
		} else {
			return "ui-icon-document";
		}
	}

	@ModelAttribute("label")
	public String getLabel(@AuthenticationPrincipal UserDetails user) {
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());
		if (authorities.contains(Roles.ROLE_ADMIN)
				|| authorities.contains(Roles.ROLE_QA)) {
			return "등록";
		} else {
			return "조회";
		}
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 페이지 처리
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model) {
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		return INDEX_PAGE;
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 페이지 로딩시
	 * 
	 * @param model
	 * @return string
	 */
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}

	/**
	 * @SpecialLogic 프로젝트 품질관리 상세 페이지로
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */
	@RequestMapping(value = VIEW_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String view(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) String code, Model model) {
		QualMngFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return VIEW_PAGE;
	}

}
