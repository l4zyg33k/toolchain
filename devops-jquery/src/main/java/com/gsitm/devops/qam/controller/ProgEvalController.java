package com.gsitm.devops.qam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.qam.model.vo.ProgEvalFrmVO;
import com.gsitm.devops.qam.service.biz.ProgEvalBiz;

@Controller
public class ProgEvalController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "progEvalFrmVO";
	private static final String CODE = "code";
	
	private static final String FORM_URL_BY_CODE = "/qam/progeval/{code}/form";
	
	private static final String FORM_PAGE = "qam/progeval/form";
	
	@Autowired
	private ProgEvalBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;	
	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	//프로젝트 단계 (코드)
	@ModelAttribute("steps")
	public String getProjectSteps() {
		return cmmCodeService.getProjectStepsString();
	}	
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	/**
	 * @SpecialLogic 진척도 평가 결과 조회
	 * 
	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) String code,
			Model model) {
		ProgEvalFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}	
}
