package com.gsitm.devops.qam.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;

/**
 * 설문조사 대상자
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class SatisfactionSurveyRecipient extends
		AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 수신자
	 */
	@ManyToOne
	private Account recipient;

	/**
	 * 사용자 유형 (코드)
	 */
	@ManyToOne
	private Code userType;

	/**
	 * 메일 발송 날짜
	 */
	private LocalDate sentDate;
}
