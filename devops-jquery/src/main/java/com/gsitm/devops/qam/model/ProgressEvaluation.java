/**
 * 진척도 평가 엔티티
 * ProgressEvaluation.java
 */
package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class ProgressEvaluation extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 진척도 단계 (단계)
	 */
	@Column(length = 8)
	private String step;

	/**
	 * 계획 측정일
	 */
	private LocalDate planDate;

	/**
	 * 실제 측정일
	 */
	private LocalDate actualDate;

	/**
	 * 계획 진척률
	 */
	private Double planRate;

	/**
	 * 실제 진척률
	 */
	private Double actualRate;

	/**
	 * 진척도 평가 사유
	 */
	@Column(length = 100)
	private String reason;
}
