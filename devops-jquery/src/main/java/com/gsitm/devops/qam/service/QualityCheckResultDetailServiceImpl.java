package com.gsitm.devops.qam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.QualityCheckResultDetail;
import com.gsitm.devops.qam.repository.QualityCheckResultDetailRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class QualityCheckResultDetailServiceImpl implements
		QualityCheckResultDetailService {

	@Autowired
	private QualityCheckResultDetailRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public QualityCheckResultDetail findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public QualityCheckResultDetail findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<QualityCheckResultDetail> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<QualityCheckResultDetail> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<QualityCheckResultDetail> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<QualityCheckResultDetail> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<QualityCheckResultDetail> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<QualityCheckResultDetail> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public QualityCheckResultDetail save(
			QualityCheckResultDetail QualityCheckResultDetail) {
		return repository.save(QualityCheckResultDetail);
	}

	@Transactional
	@Override
	public <S extends QualityCheckResultDetail> List<S> save(
			Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(QualityCheckResultDetail QualityCheckResultDetail) {
		repository.delete(QualityCheckResultDetail);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends QualityCheckResultDetail> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
