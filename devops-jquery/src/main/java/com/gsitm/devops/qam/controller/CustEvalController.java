package com.gsitm.devops.qam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.model.vo.CustEvalFrmVO;
import com.gsitm.devops.qam.service.biz.CustEvalBiz;

@Controller
public class CustEvalController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "custEvalFrmVO";
	private static final String CODE = "code";
	
	private static final String FORM_URL_BY_CODE = "/qam/custeval/{code}";
	
	private static final String FORM_PAGE = "qam/custeval/form";
	
	@Autowired
	private CustEvalBiz biz; 	
	
	@Autowired
	private CommonCodeService cmmCodeService;		
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	//설문대상자(T3.사업부, T4.현업사용자, T5.운영자)
	@ModelAttribute("recipients")
	public String getRecipients() {
		return cmmCodeService.getRecipientsLong();
	}
	
	
	/**
	 * @SpecialLogic 고객만족도 대상자 관리 페이지 로딩
	 * 
	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET)
	public String form(@PathVariable(CODE) Project project,
			Model model) {
		CustEvalFrmVO vo = new CustEvalFrmVO(project, 1, 20, "");
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}
	
	/**
	 * @SpecialLogic 고객만족도 대상자 관리 페이지 처리 
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) Project project,
			Model model) {
		CustEvalFrmVO vo = new CustEvalFrmVO(project, page, rowNum, rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}			
}
