package com.gsitm.devops.qam.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.qam.model.DocumentEvaluation;

@Repository
public interface DocumentEvaluationRepository extends
		SharedRepository<DocumentEvaluation, Long> {
}