package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.qam.model.vo.DocuEvalCmdVO;
import com.gsitm.devops.qam.model.vo.DocuEvalGrdVO;
import com.gsitm.devops.qam.service.biz.DocuEvalBiz;

@RestController
public class DocuEvalRestController {

	@Autowired
	private DocuEvalBiz biz; 
	
	/**
	 * @SpecialLogic 산출물 측정결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<DocuEvalGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/docueval", method = RequestMethod.POST)
	public Page<DocuEvalGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, user, projectCode);
	}
	
	/**
	 * @SpecialLogic 산출물 측정결과 등록, 수정, 삭제
	 * 
	 * @param oper
	 * @param progEvalCmdVO
	 */	
	@RequestMapping(value = "/rest/qam/docueval/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			DocuEvalCmdVO docuEvalCmdVO, BindingResult result, 
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		switch (oper) {
		case "add":
			biz.create(docuEvalCmdVO, projectCode);
			break;
		case "edit":
			biz.update(docuEvalCmdVO);
			break;
		case "del":
			biz.delete(docuEvalCmdVO);
			break;
		}
	}	
}
