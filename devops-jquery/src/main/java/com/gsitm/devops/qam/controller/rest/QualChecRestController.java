package com.gsitm.devops.qam.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.qam.model.QualityCheckResult;
import com.gsitm.devops.qam.model.vo.QualChecCmdVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlFrmVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlGrdVO;
import com.gsitm.devops.qam.model.vo.QualChecGrdVO;
import com.gsitm.devops.qam.service.biz.QualChecBiz;

@RestController
public class QualChecRestController {

	@Autowired
	private QualChecBiz biz; 
	
	/**
	 * @SpecialLogic 품질점검 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<QualChecGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/qualchec", method = RequestMethod.POST)
	public Page<QualChecGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, user, projectCode);
	}
	
	/**
	 * @SpecialLogic 품질점검 결과 삭제
	 * 
	 * @param oper
	 * @param qualChecCmdVO
	 */	
	@RequestMapping(value = "/rest/qam/qualchec/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			QualChecCmdVO qualChecCmdVO) {
		switch (oper) {
		case "del":
			biz.delete(qualChecCmdVO);
			break;
		}
	}	
	
	/**
	 * @SpecialLogic 품질점검 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<QualChecGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/qualchec/{id}/details", method = RequestMethod.POST)
	public Page<QualChecDetlGrdVO> findAll(Pageable pageable,
			@PathVariable("id") QualityCheckResult qualityCheckResult) {
		return biz.findDtlAll(pageable, qualityCheckResult);
	}
	

	/**
	 * @SpecialLogic 품질점검 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param projectCode : 프로젝트 코드
	 * @return Page<QualChecGrdVO>
	 */	
	@RequestMapping(value = "/rest/qam/qualchec/detailsList", method = RequestMethod.POST)
	public Page<QualChecGrdVO> findAllWeek(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, projectCode);
	}	
	
	/**
	 * @SpecialLogic 품질점검 결과 상세 - 등록, 수정, 삭제 처리
	 * 
	 * @param oper
	 * @param id
	 * @param qualChecDetlFrmVO
	 * @param result
	 */		
	@RequestMapping(value = "/rest/qam/qualchec/{id}/details/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			@PathVariable("id") QualityCheckResult qualityCheckResult,
			@Valid QualChecDetlFrmVO qualChecDetlFrmVO, BindingResult result) {
		switch (oper) {
		case "add":
			biz.create(qualChecDetlFrmVO, qualityCheckResult);
			break;
		case "edit":
			biz.update(qualChecDetlFrmVO, qualityCheckResult);
			break;
		case "del":
			biz.delete(qualChecDetlFrmVO);
			break;
		}
	}
		
}
