package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.pms.model.Project;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class SurvResuMultiGrdVO {
	
	private String username;
	private String name;
	private Code position;
	private Code team;
	private Project project;
	private Code userType;

	// 사업부
	private Integer T301; // 응답속도
	private Integer T302; // 요구사항
	private Integer T303; // 출력양식
	private Integer T304; // 자료정확
	private Integer T305; // 보안체계
	private Integer T306; // 일정준수
	private Integer T307; // 개발시스템.종합

	private Integer T308; // 교육자료
	private Integer T309; // 메뉴얼
	private Integer T310; // UI
	private Integer T311; // 호환성
	private Integer T312; // 시스템활용지원.종합

	// 현업사용자
	private Integer T401; // 응답속도
	private Integer T402; // 요구사항
	private Integer T403; // 자료정확
	private Integer T404; // Sys.안정
	private Integer T405; // 사용편의
	private Integer T406; // Sys.개선
	private Integer T407; // 개발시스템.종합

	// 시스템운영자
	private Integer T501; // 문서완성
	private Integer T502; // 문서이해
	private Integer T503; // 문서일치
	private Integer T504; // 문서실질
	private Integer T505; // 개발문서.종합

	private Integer T506; // Sys.구성
	private Integer T507; // Sys.확장
	private Integer T508; // 보안체계
	private Integer T509; // Sys.성능
	private Integer T510; // 오류빈도
	private Integer T511; // 시스템구조및성능.종합

	private Integer T512; // 운영편의
	private Integer T513; // 수정용이
	private Integer T514; // 표준준수
	private Integer T515; // Sys.연동
	private Integer T516; // User관리
	private Integer T517; // 배치편리
	private Integer T518; // 시스템운영편리성.종합	

	
	public SurvResuMultiGrdVO() {
		super();
	}

	@QueryProjection
	public SurvResuMultiGrdVO(String username, String name, Code position,
			Code team, Project project, Code userType, Integer t301,
			Integer t302, Integer t303, Integer t304, Integer t305,
			Integer t306, Integer t307, Integer t308, Integer t309,
			Integer t310, Integer t311, Integer t312, Integer t401,
			Integer t402, Integer t403, Integer t404, Integer t405,
			Integer t406, Integer t407, Integer t501, Integer t502,
			Integer t503, Integer t504, Integer t505, Integer t506,
			Integer t507, Integer t508, Integer t509, Integer t510,
			Integer t511, Integer t512, Integer t513, Integer t514,
			Integer t515, Integer t516, Integer t517, Integer t518) {
		super();
		this.username = username;
		this.name = name;
		this.position = position;
		this.team = team;
		this.project = project;
		this.userType = userType;
		T301 = t301;
		T302 = t302;
		T303 = t303;
		T304 = t304;
		T305 = t305;
		T306 = t306;
		T307 = t307;
		T308 = t308;
		T309 = t309;
		T310 = t310;
		T311 = t311;
		T312 = t312;
		T401 = t401;
		T402 = t402;
		T403 = t403;
		T404 = t404;
		T405 = t405;
		T406 = t406;
		T407 = t407;
		T501 = t501;
		T502 = t502;
		T503 = t503;
		T504 = t504;
		T505 = t505;
		T506 = t506;
		T507 = t507;
		T508 = t508;
		T509 = t509;
		T510 = t510;
		T511 = t511;
		T512 = t512;
		T513 = t513;
		T514 = t514;
		T515 = t515;
		T516 = t516;
		T517 = t517;
		T518 = t518;
	}
}
