package com.gsitm.devops.qam.dto;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.gsitm.devops.pms.model.Project;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class ProjectQualityDto {
	private Project project;
	private LocalDate progressG0; // 진척도.착수
	private LocalDate progressG1; // 진척도.분석
	private LocalDate progressG2; // 진척도.설계
	private LocalDate progressG3; // 진척도.코드
	private LocalDate progressG4; // 진척도.테스트
	private LocalDate progressG5; // 진척도.이행

	private Double progressRate; // 진척도.이행
	private Number productPoint; // 진척도.이행
	private Number satisfactionPoint; // 진척도.이행

	@QueryProjection
	public ProjectQualityDto(Project project, LocalDate progressG0,
			LocalDate progressG1, LocalDate progressG2, LocalDate progressG3,
			LocalDate progressG4, LocalDate progressG5, Double progressRate,
			Number productPoint, Number satisfactionPoint) {

		this.project = project;
		this.progressG0 = progressG0;
		this.progressG1 = progressG1;
		this.progressG2 = progressG2;
		this.progressG3 = progressG3;
		this.progressG4 = progressG4;
		this.progressG5 = progressG5;

		this.progressRate = progressRate;
		this.productPoint = productPoint;
		this.satisfactionPoint = satisfactionPoint;

	}

}
