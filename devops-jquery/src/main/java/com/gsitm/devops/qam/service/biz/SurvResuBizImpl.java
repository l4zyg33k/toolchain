package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.qam.model.QSatisfactionSurveyResult;
import com.gsitm.devops.qam.model.SatisfactionSurveyResult;
import com.gsitm.devops.qam.model.vo.QSurvResuMultiGrdVO;
import com.gsitm.devops.qam.model.vo.QSurvResuShortGrdVO;
import com.gsitm.devops.qam.model.vo.SurvResuMultiGrdVO;
import com.gsitm.devops.qam.model.vo.SurvResuShortGrdVO;
import com.gsitm.devops.qam.service.SatisfactionSurveyQuestionService;
import com.gsitm.devops.qam.service.SatisfactionSurveyRecipientService;
import com.gsitm.devops.qam.service.SatisfactionSurveyResultService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.CaseBuilder;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class SurvResuBizImpl implements SurvResuBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private SatisfactionSurveyResultService ssrService;

	@Autowired
	private SatisfactionSurveyQuestionService ssqService;

	@Autowired
	private SatisfactionSurveyRecipientService ssrcService;	
	
	@Autowired
	private ProjectService projectService;		

	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		
		return navGrid;
	}	
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 객관식 설문
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return Page<SurvResuMultiGrdVO>
	 */	
	@Override
	public Page<SurvResuMultiGrdVO> findMultiAll(Pageable pageable, UserDetails user, String userType, String projectCode){
		QSatisfactionSurveyResult ssr = QSatisfactionSurveyResult.satisfactionSurveyResult;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectCode)) {
			conditions.and(ssr.project.code.eq(projectCode)); 
		}
		conditions.and(ssr.userType.value.eq(userType));
		
		//쿼리
		JPQLQuery query = new JPAQuery(em).from(ssr).where(conditions);
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<SatisfactionSurveyResult>(SatisfactionSurveyResult.class, "satisfactionSurveyResult"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<SurvResuMultiGrdVO> search = query
				.listResults(new QSurvResuMultiGrdVO(ssr.username, ssr.name, ssr.position, ssr.team, ssr.project, ssr.userType,
						// 사업부담당자
						new CaseBuilder().when(ssr.keyword.eq("응답속도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("요구사항")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("출력양식")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("자료정확")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("보안체계")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("일정준수")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발시스템.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("교육자료")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("메뉴얼")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("UI")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("호환성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템활용지원.종합")).then(ssr.point).otherwise(0).max(),
						// 현업사용자
						new CaseBuilder().when(ssr.keyword.eq("응답속도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("요구사항")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("자료정확")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.안정")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("사용편의")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.개선")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발시스템.종합")).then(ssr.point).otherwise(0).max(),
						// 시스템운영자
						new CaseBuilder().when(ssr.keyword.eq("문서완성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서이해")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서일치")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서실질")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발문서.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("Sys.구성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.확장")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("보안체계")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.성능")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("오류빈도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템구조및성능.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("운영편의")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("수정용이")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("표준준수")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.연동")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("User관리")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("배치편리")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템운영편리성.종합")).then(ssr.point).otherwise(0).max()
				));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 객관식 설문 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @param searchable : 조회조건
	 * @return Page<SurvResuMultiGrdVO>
	 */		
	@Override
	public Page<SurvResuMultiGrdVO> findMultiAll(Pageable pageable, UserDetails user, String userType, String projectCode, Searchable searchable){
		PathBuilder<SatisfactionSurveyResult> builder = new PathBuilder<SatisfactionSurveyResult>(SatisfactionSurveyResult.class, "satisfactionSurveyResult");
		QSatisfactionSurveyResult ssr = QSatisfactionSurveyResult.satisfactionSurveyResult;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectCode)) {
			conditions.and(ssr.project.code.eq(projectCode)); 
		}
		conditions.and(ssr.userType.value.eq(userType));
		
		//쿼리
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(ssr).where(predicate, conditions);
		
		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);
		
		SearchResults<SurvResuMultiGrdVO> search = query
				.listResults(new QSurvResuMultiGrdVO(ssr.username, ssr.name, ssr.position, ssr.team, ssr.project, ssr.userType,
						// 사업부담당자
						new CaseBuilder().when(ssr.keyword.eq("응답속도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("요구사항")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("출력양식")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("자료정확")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("보안체계")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("일정준수")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발시스템.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("교육자료")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("메뉴얼")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("UI")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("호환성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템활용지원.종합")).then(ssr.point).otherwise(0).max(),
						// 현업사용자
						new CaseBuilder().when(ssr.keyword.eq("응답속도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("요구사항")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("자료정확")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.안정")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("사용편의")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.개선")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발시스템.종합")).then(ssr.point).otherwise(0).max(),
						// 시스템운영자
						new CaseBuilder().when(ssr.keyword.eq("문서완성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서이해")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서일치")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("문서실질")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("개발문서.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("Sys.구성")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.확장")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("보안체계")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.성능")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("오류빈도")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템구조및성능.종합")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("운영편의")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("수정용이")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("표준준수")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("Sys.연동")).then(ssr.point).otherwise(0).max(),
						new CaseBuilder().when(ssr.keyword.eq("User관리")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("배치편리")).then(ssr.point).otherwise(0).max(), 
						new CaseBuilder().when(ssr.keyword.eq("시스템운영편리성.종합")).then(ssr.point).otherwise(0).max()
				));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());		
	}

	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 주관식 설문 
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return Page<SurvResuMultiGrdVO>
	 */	
	@Override
	public Page<SurvResuShortGrdVO> findShortAll(Pageable pageable, UserDetails user, String userType, String projectCode){
		QSatisfactionSurveyResult ssr = QSatisfactionSurveyResult.satisfactionSurveyResult;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectCode)) {
			conditions.and(ssr.project.code.eq(projectCode)); 
		}
		conditions.and(ssr.userType.value.eq(userType));
		
		//쿼리
		JPQLQuery query = new JPAQuery(em).from(ssr).where(conditions);
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<SatisfactionSurveyResult>(SatisfactionSurveyResult.class, "satisfactionSurveyResult"));
		querydsl.applyPagination(pageable, query);

		SearchResults<SurvResuShortGrdVO> search = query
				.listResults(new QSurvResuShortGrdVO(ssr.id, ssr.project, ssr.username, ssr.name, ssr.position, ssr.team,
						ssr.no, ssr.userType, ssr.surveyType, ssr.keyword, ssr.question, ssr.point, ssr.content));	
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	/**
	 * @SpecialLogic  프로젝트 설문결과 조회 - 주관식 설문 (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @param searchable : 조회조건
	 * @return Page<SurvResuMultiGrdVO>
	 */	
	@Override
	public Page<SurvResuShortGrdVO> findShortAll(Pageable pageable, UserDetails user, String userType, String projectCode, Searchable searchable){
		PathBuilder<SatisfactionSurveyResult> builder = new PathBuilder<SatisfactionSurveyResult>(SatisfactionSurveyResult.class, "satisfactionSurveyResult");
		QSatisfactionSurveyResult ssr = QSatisfactionSurveyResult.satisfactionSurveyResult;
		
		//조회조건  - 프로젝트코드와 프로젝트명
		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectCode)) {
			conditions.and(ssr.project.code.eq(projectCode)); 
		}
		conditions.and(ssr.userType.value.eq(userType));
		
		//쿼리
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(ssr).where(predicate, conditions);
		
		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<SurvResuShortGrdVO> search = query
				.listResults(new QSurvResuShortGrdVO(ssr.id, ssr.project, ssr.username, ssr.name, ssr.position, ssr.team,
						ssr.no, ssr.userType, ssr.surveyType, ssr.keyword, ssr.question, ssr.point, ssr.content));	
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	/**
	 * @SpecialLogic  프로젝트별 설문 종료
	 * 
	 * @param userType : 사용자유형
	 * @param projectCode : 프로젝트코드
	 * @return JqResult
	 */		
	@Override
	public void update(String projectCode){
		Project project = projectService.findOne(projectCode);
		project.setSatisfactionExpired(Boolean.TRUE);
		projectService.save(project);
	}
	
	
}
