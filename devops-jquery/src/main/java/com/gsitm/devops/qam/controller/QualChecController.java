package com.gsitm.devops.qam.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.model.vo.QualChecCmdVO;
import com.gsitm.devops.qam.model.vo.QualChecFrmVO;
import com.gsitm.devops.qam.service.biz.QualChecBiz;

@Controller
public class QualChecController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "qualChecFrmVO";
	private static final String CODE = "code";
	private static final String MAIN_PAGE = "mainPage";
	private static final String MAIN_ROW_NUM = "mainRowNum";
	private static final String ROWDATA = "rowdata";
	private static final String PREVIEW_MAIN_URL = "previewMainUrl";

	private static final String INDEX_URL = "/qam/qualchec/{code}/list";
	private static final String EDIT_URL_BY_CODE = "/qam/qualchec/{code}/edit";
	private static final String FORM_URL_BY_CODE = "/qam/qualchec/{code}/form";
	private static final String INDEX_URL_BY_CODE = "/qam/qualchec/{code}/index";
	private static final String REDIRECT_MAIN_URL = "/qam/qualmng?page=";
	private static final String SAVE_URL = "/qam/qualchec";
	private static final String REDIRECT_URL = "redirect:/qam/qualchec/{code}/list?page={page}&rowNum={rowNum}&rowId={rowId}&mainPage={mainPage}&mainRowNum={mainRowNum}";

	private static final String INDEX_PAGE = "qam/qualchec/index";
	private static final String FORM_PAGE = "qam/qualchec/form";
	
	@Autowired
	private QualChecBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;		
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}	
	
	//품질검토종류(SQA검사, 단계검토)
	@ModelAttribute("kinds")
	public Map<Long, String> getExamineKinds() {
		return cmmCodeService.getExamineKinds();
	}		

	//프로젝트상태
	@ModelAttribute("status")
	public Map<Long, String> getProjectStatus() {
		return cmmCodeService.getProjectStatus();
	}
	
	//결과유형(RE 필수, OP 권고)
	@ModelAttribute("resultTypes")
	public String getResultTypes() {
		return cmmCodeService.getResultTypesString();
	}	
	
	/**
	 * @SpecialLogic 프로젝트 품질관리 페이지 처리 
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */	
	@RequestMapping(value = INDEX_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID  })
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model,
			@PathVariable(CODE) String code) {
		String previewMainUrl = REDIRECT_MAIN_URL+page+"&rowNum="+rowNum+"&rowId="+rowId;
		model.addAttribute(PREVIEW_MAIN_URL, previewMainUrl);
		model.addAttribute(CODE, code);
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		model.addAttribute(MAIN_PAGE, page);
		model.addAttribute(MAIN_ROW_NUM, rowNum);
		return INDEX_PAGE;
	}
	
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId, Model model,
			@PathVariable(CODE) String code,
			@RequestParam(MAIN_PAGE) int mainPage,
			@RequestParam(MAIN_ROW_NUM) int mainRowNum) {
		String previewMainUrl = REDIRECT_MAIN_URL+mainPage+"&rowNum="+mainRowNum+"&rowId="+code;
		model.addAttribute(PREVIEW_MAIN_URL, previewMainUrl);
		model.addAttribute(CODE, code);
		model.addAttribute(PageParams.PAGE, page);
		model.addAttribute(PageParams.ROW_NUM, rowNum);
		model.addAttribute(PageParams.ROW_ID, rowId);
		model.addAttribute(MAIN_PAGE, mainPage);
		model.addAttribute(MAIN_ROW_NUM, mainRowNum);
		model.addAttribute(CODE, code);
		return INDEX_PAGE;
	}	
	
	/**
	 * @SpecialLogic 프로젝트 품질관리  등록 화면으로 이동
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */	    
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID})
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId, Model model,
			@PathVariable(CODE) Project project,
			@RequestParam(MAIN_PAGE) int mainPage,
			@RequestParam(MAIN_ROW_NUM) int mainRowNum) {
		QualChecFrmVO qualChecFrmVO = new QualChecFrmVO(project.getCode(), project.getName(), 
									  project.getIl().getUsername(), project.getIl().getName(),
				                      page, rowNum, rowId, mainPage, mainRowNum);
		model.addAttribute(FORM_VO, qualChecFrmVO);
		
		return FORM_PAGE;
	}	
	
	/**
	 * @SpecialLogic 프로젝트 품질관리  수정 화면으로 이동
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param model
	 * @return string
	 */	    
	@RequestMapping(value = EDIT_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID})
	public String edit(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) long rowId, Model model,
			@RequestParam(MAIN_PAGE) int mainPage,
			@RequestParam(MAIN_ROW_NUM) int mainRowNum) {
		QualChecFrmVO vo = biz.findOne(rowId);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		vo.setMainPage(mainPage);
		vo.setMainRowNum(mainRowNum);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}
	
	@RequestMapping(value = SAVE_URL, method = RequestMethod.POST)
	public String create(
			@ModelAttribute(FORM_VO) @Valid QualChecCmdVO qualChecCmdVO,
			BindingResult result, RedirectAttributes redirectAttributes,
			@RequestParam(value = ROWDATA, defaultValue = "", required = false) String rowdata) {
		
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.create(qualChecCmdVO, rowdata);		
		redirectAttributes.addAttribute(PageParams.PAGE, qualChecCmdVO.getPage());
		redirectAttributes.addAttribute(PageParams.ROW_NUM, qualChecCmdVO.getRowNum());
		redirectAttributes.addAttribute(PageParams.ROW_ID, qualChecCmdVO.getRowId());
		redirectAttributes.addAttribute(MAIN_PAGE, qualChecCmdVO.getMainPage());
		redirectAttributes.addAttribute(MAIN_ROW_NUM, qualChecCmdVO.getMainRowNum());
		redirectAttributes.addAttribute(CODE, qualChecCmdVO.getProject().getCode());
		return REDIRECT_URL;
	}
	
	/**
	 * @SpecialLogic 프로젝트 사이즈  수정처리
	 * 
	 * @param projSizeCmdVO
	 * @param result
	 * @param redirectAttributes
	 * @return string
	 */		
	@RequestMapping(value = "/qam/qualchec/*", method = RequestMethod.PUT)
	public String update(@ModelAttribute(FORM_VO) @Valid QualChecCmdVO qualChecCmdVO,
			BindingResult result, RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			return FORM_PAGE;
		}
		biz.update(qualChecCmdVO);
		redirectAttributes.addAttribute(PageParams.PAGE, qualChecCmdVO.getPage());
		redirectAttributes.addAttribute(PageParams.ROW_NUM, qualChecCmdVO.getRowNum());
		redirectAttributes.addAttribute(PageParams.ROW_ID, qualChecCmdVO.getRowId());
		redirectAttributes.addAttribute(MAIN_PAGE, qualChecCmdVO.getMainPage());
		redirectAttributes.addAttribute(MAIN_ROW_NUM, qualChecCmdVO.getMainRowNum());
		redirectAttributes.addAttribute(CODE, qualChecCmdVO.getProject().getCode());
		return REDIRECT_URL;
	}
}
