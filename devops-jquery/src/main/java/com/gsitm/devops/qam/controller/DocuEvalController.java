package com.gsitm.devops.qam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.qam.model.vo.DocuEvalFrmVO;
import com.gsitm.devops.qam.service.biz.DocuEvalBiz;

@Controller
public class DocuEvalController {

	private static final String MENU_CODE = "QualMng";
	private static final String FORM_VO = "docuEvalFrmVO";
	private static final String CODE = "code";
	
	private static final String FORM_URL_BY_CODE = "/qam/docueval/{code}/form";
	
	private static final String FORM_PAGE = "qam/docueval/form";
	
	@Autowired
	private DocuEvalBiz biz; 	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}
	
	/**
	 * @SpecialLogic 산출물 평가등록 조회
	 * 
	 * @param code
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = FORM_URL_BY_CODE, method = RequestMethod.GET, params = {
			PageParams.PAGE, PageParams.ROW_NUM, PageParams.ROW_ID })
	public String form(@RequestParam(PageParams.PAGE) int page,
			@RequestParam(PageParams.ROW_NUM) int rowNum,
			@RequestParam(PageParams.ROW_ID) String rowId,
			@PathVariable(CODE) String code,
			Model model) {
		DocuEvalFrmVO vo = biz.findOne(code);
		vo.setPage(page);
		vo.setRowNum(rowNum);
		vo.setRowId(rowId);
		model.addAttribute(FORM_VO, vo);
		return FORM_PAGE;
	}	
}
