package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class QualChecGrdVO {		
	//id
	private Long id;
	
	//프로젝트 코드
	private String projectCode;
	
	//프로젝트명
	private String projectName;
	
	//품질 점검 제목
	private String title;	

	//품질 점검 일자
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private LocalDate inspectedDate;

	//품질 점검 유형
	private String type;
	
	//품질 점검 단계
	private String stage;	

	//품질 점검자
	private String inspector;
	
	//IL 이름
	private String iLname;
	
	private String requiredCount;
	private String opinionCount;

	
	public QualChecGrdVO() {
		super();
	}
	
	@QueryProjection
	public QualChecGrdVO(Long id, String projectCode, String projectName,
			String title, LocalDate inspectedDate, String type, String stage,
			String inspector, String iLname) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.title = title;
		this.inspectedDate = inspectedDate;
		this.type = type;
		this.stage = stage;
		this.inspector = inspector;
		this.iLname = iLname;
	}
	
}
