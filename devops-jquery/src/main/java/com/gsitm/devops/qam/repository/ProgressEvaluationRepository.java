package com.gsitm.devops.qam.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.cmm.repository.SharedRepository;
import com.gsitm.devops.qam.model.ProgressEvaluation;

@Repository
public interface ProgressEvaluationRepository extends
		SharedRepository<ProgressEvaluation, Long> {
}
