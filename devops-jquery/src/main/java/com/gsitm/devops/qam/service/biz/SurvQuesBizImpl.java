package com.gsitm.devops.qam.service.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.qam.model.QSatisfactionSurveyQuestion;
import com.gsitm.devops.qam.model.SatisfactionSurveyQuestion;
import com.gsitm.devops.qam.model.vo.QSurvQuesGrdVO;
import com.gsitm.devops.qam.model.vo.SurvQuesCmdVO;
import com.gsitm.devops.qam.model.vo.SurvQuesGrdVO;
import com.gsitm.devops.qam.service.SatisfactionSurveyQuestionService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class SurvQuesBizImpl implements SurvQuesBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private CodeService codeService;
	
	@Autowired
	private SatisfactionSurveyQuestionService questionService;	
	

	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		
		return navGrid;
	}
	
	/**
	 * @SpecialLogic 만족도설문문항관리 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param surveyType : 설문유형
	 * @return Page<SurvQuesGrdVO>
	 */	
	@Override
	public Page<SurvQuesGrdVO> findAll(Pageable pageable, UserDetails user, String questionType, Long userType, Long surveyType){
		QSatisfactionSurveyQuestion ssq = QSatisfactionSurveyQuestion.satisfactionSurveyQuestion;
		
		//조회조건추가
		BooleanBuilder conditions = new BooleanBuilder();
		conditions.and(ssq.userType.id.eq(userType)); 
		conditions.and(ssq.surveyType.id.eq(surveyType)); 
		conditions.and(ssq.questionType.value.eq(questionType)); 
		
		//쿼리
		JPQLQuery query = new JPAQuery(em).from(ssq).where(conditions);
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<SatisfactionSurveyQuestion>(SatisfactionSurveyQuestion.class, "satisfactionSurveyQuestion"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<SurvQuesGrdVO> search = query
				.listResults(new QSurvQuesGrdVO(ssq.id, ssq.userType.id, ssq.surveyType.id,
						ssq.questionType.id, ssq.no, ssq.keyword, ssq.question));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 만족도설문문항관리  조회  (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param searchable : 조회조건
	 * @param code : 설문코드
	 * @param userType : 사용자유형
	 * @param surveyType : 설문유형
	 * @return Page<SurvQuesGrdVO>
	 */	
	public Page<SurvQuesGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, String questionType, Long userType, Long surveyType){
		
		PathBuilder<SatisfactionSurveyQuestion> builder = new PathBuilder<SatisfactionSurveyQuestion>(SatisfactionSurveyQuestion.class, "satisfactionSurveyQuestion");
		QSatisfactionSurveyQuestion ssq = QSatisfactionSurveyQuestion.satisfactionSurveyQuestion;
		
		//조회조건추가
		BooleanBuilder conditions = new BooleanBuilder();
		conditions.and(ssq.userType.id.eq(userType)); 
		conditions.and(ssq.surveyType.id.eq(surveyType)); 
		conditions.and(ssq.questionType.value.eq(questionType)); 
		
		//쿼리
		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(ssq).where(predicate, conditions);
		
		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);
		
		SearchResults<SurvQuesGrdVO> search = query
				.listResults(new QSurvQuesGrdVO(ssq.id, ssq.userType.id, ssq.surveyType.id,
						ssq.questionType.id, ssq.no, ssq.keyword, ssq.question));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());		
	}
	
	/**
	 * @SpecialLogic 만족도설문문항관리 수정
	 * 
	 * @param   survQuesCmdVO
	 */		
	@Override
	public void update(SurvQuesCmdVO survQuesCmdVO) {
		SatisfactionSurveyQuestion ssq = questionService.findOne(survQuesCmdVO.getId());
		BeanUtils.copyProperties(survQuesCmdVO, ssq);
		ssq.setQuestionType(getCode(survQuesCmdVO.getQuestionType()));
		questionService.save(ssq);
	}

	/**
	 * @SpecialLogic 만족도설문문항관리 삭제
	 * 
	 * @param survQuesCmdVO
	 */		
	@Override
	public void delete(SurvQuesCmdVO survQuesCmdVO) {
		questionService.delete(survQuesCmdVO.getId());
	}
	
	/**
	 * @SpecialLogic 만족도설문문항관리 저장
	 * 
	 * @param survQuesCmdVO
	 */		
	@Override
	public void create(SurvQuesCmdVO survQuesCmdVO) {
		SatisfactionSurveyQuestion ssq = new SatisfactionSurveyQuestion();
		BeanUtils.copyProperties(survQuesCmdVO, ssq);
		ssq.setQuestionType(getCode(survQuesCmdVO.getQuestionType()));
		questionService.save(ssq);
	}	
	
	private Code getCode(String codeValue) {
		QCode code = QCode.code;
		return codeService.findOne(code.value.eq(codeValue).and(code.parent.value.eq("T2")));
	}
	
	
}
