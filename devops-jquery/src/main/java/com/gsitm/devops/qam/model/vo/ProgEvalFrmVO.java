package com.gsitm.devops.qam.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProgEvalFrmVO extends QualMngWebVO {
	
	//프로젝트 코드
	private String code;
	
	//프로젝트 명
	private String name;
	
	//계획대비 진척률
	private String progressPerPlan;

	public ProgEvalFrmVO(int page, int rowNum, String rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}
	
	@QueryProjection
	public ProgEvalFrmVO(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
}