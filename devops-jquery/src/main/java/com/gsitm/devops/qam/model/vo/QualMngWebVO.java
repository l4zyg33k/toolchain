package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class QualMngWebVO extends SharedWebVO<String> {
	
	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/qam/qualmng" : String.format("/qam/qualmng/%s",
				getId());
	}

	public String getPrevUrl() {
		return String.format("/qam/qualmng?page=%d&rowNum=%d&rowId=%s",
				getPage(), getRowNum(), getRowId());
	}	
}
