package com.gsitm.devops.qam.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.qam.dto.SatisfactionSurveyResultDto;
import com.gsitm.devops.qam.model.SatisfactionSurveyResult;
import com.mysema.query.types.Predicate;

public interface SatisfactionSurveyResultService extends
		SharedService<SatisfactionSurveyResult, Long> {
	public Page<SatisfactionSurveyResultDto> findResultDto(Predicate predicate,
			Pageable pageable);

}
