package com.gsitm.devops.qam.service;

import com.gsitm.devops.cmm.service.SharedService;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.dto.SatisfactionSurveySummaryFormDto;
import com.gsitm.devops.qam.model.SatisfactionSurveySummary;
import com.mysema.query.types.Predicate;

public interface SatisfactionSurveySummaryService extends
		SharedService<SatisfactionSurveySummary, Long> {
	public long saveSummary(Project project);

	public Iterable<SatisfactionSurveySummaryFormDto> findAll2(
			Predicate predicate);
}
