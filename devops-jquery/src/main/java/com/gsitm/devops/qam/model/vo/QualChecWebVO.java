package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class QualChecWebVO extends SharedWebVO<Long> {
	
	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/qam/qualchec" : String.format("/qam/qualchec/%d",
				getId());
	}
	
	public String getPrevUrl() {
		return String.format("/qam/qualchec?page=%d&rowNum=%d&rowId=%d",
				getPage(), getRowNum(), getRowId());
	}
	
	public String getDetailsUrl() {
		return String.format("/rest/qam/qualchec/%d/details", getId());
	}

	public String getDetailsEditUrl() {
		return String.format("/rest/qam/qualchec/%d/details/edit", getId());
	}	
}
