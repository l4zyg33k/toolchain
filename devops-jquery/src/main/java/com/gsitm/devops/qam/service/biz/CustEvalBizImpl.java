package com.gsitm.devops.qam.service.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.service.MailService;
import com.gsitm.devops.cmm.util.JqGrid;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.qam.model.QSatisfactionSurveyRecipient;
import com.gsitm.devops.qam.model.SatisfactionSurveyRecipient;
import com.gsitm.devops.qam.model.vo.CustEvalCmdVO;
import com.gsitm.devops.qam.model.vo.CustEvalGrdVO;
import com.gsitm.devops.qam.model.vo.QCustEvalGrdVO;
import com.gsitm.devops.qam.service.SatisfactionSurveyRecipientService;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
public class CustEvalBizImpl implements CustEvalBiz {

	// 메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private SatisfactionSurveyRecipientService memberService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MailService mailService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * @SpecialLogic 사용자별 jqGrid 권한
	 * 
	 * @param user
	 *            : 사용자 정보
	 * @return Map<String, Boolean>
	 */
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user
				.getAuthorities());

		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}

	/**
	 * @SpecialLogic 고객만족도 대상자 관리 수정
	 * 
	 * @param custEvalCmdVO
	 */
	@Override
	public void update(CustEvalCmdVO custEvalCmdVO) {
		SatisfactionSurveyRecipient de = memberService.findOne(custEvalCmdVO
				.getId());
		BeanUtils.copyProperties(custEvalCmdVO, de);
		memberService.save(de);
	}

	/**
	 * @SpecialLogic 고객만족도 대상자 관리 삭제
	 * 
	 * @param custEvalCmdVO
	 */
	@Override
	public void delete(CustEvalCmdVO custEvalCmdVO) {
		memberService.delete(custEvalCmdVO.getId());
	}

	/**
	 * @SpecialLogic 고객만족도 대상자 관리 저장
	 * 
	 * @param custEvalCmdVO
	 * @param projectCode
	 */
	@Override
	public void create(CustEvalCmdVO custEvalCmdVO) {
		SatisfactionSurveyRecipient ssr = new SatisfactionSurveyRecipient();
		BeanUtils.copyProperties(custEvalCmdVO, ssr);
		memberService.save(ssr);
	}

	/**
	 * @SpecialLogic 고객만족도 대상자 다건 조회
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param user
	 *            : 사용자 정보
	 * @param project
	 *            : 프로젝트
	 * @return Page<CustEvalGrdVO>
	 */
	@Override
	public Page<CustEvalGrdVO> findAll(Pageable pageable, String projectCode) {
		QSatisfactionSurveyRecipient ssr = QSatisfactionSurveyRecipient.satisfactionSurveyRecipient;

		QAccount recipient = new QAccount("recipient");

		JPQLQuery query = new JPAQuery(em).from(ssr)
				.leftJoin(ssr.recipient, recipient)
				.where(ssr.project.code.eq(projectCode));

		Querydsl querydsl = new Querydsl(em,
				new PathBuilder<SatisfactionSurveyRecipient>(
						SatisfactionSurveyRecipient.class,
						"satisfactionSurveyRecipient"));
		querydsl.applyPagination(pageable, query);

		SearchResults<CustEvalGrdVO> search = query
				.listResults(new QCustEvalGrdVO(ssr.id, ssr.project.code,
						recipient.username, recipient.name, recipient.email,
						recipient.team.name, recipient.position.name,
						ssr.userType.id, ssr.sentDate));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 고객만족도 대상자 다건 조회 (jqGrid 조회)
	 * 
	 * @param pageable
	 *            : 페이지 처리
	 * @param searchable
	 *            : 조회조건
	 * @param projectCode
	 *            : 프로젝트코드
	 * @return Page<CustEvalGrdVO>
	 */
	@Override
	public Page<CustEvalGrdVO> findAll(Pageable pageable,
			Searchable searchable, String projectCode) {
		PathBuilder<SatisfactionSurveyRecipient> builder = new PathBuilder<SatisfactionSurveyRecipient>(
				SatisfactionSurveyRecipient.class,
				"satisfactionSurveyRecipient");
		QSatisfactionSurveyRecipient ssr = QSatisfactionSurveyRecipient.satisfactionSurveyRecipient;

		QAccount recipient = new QAccount("recipient");

		BooleanBuilder conditions = new BooleanBuilder();
		if (StringUtils.isNotBlank(projectCode)) {
			conditions.and(ssr.project.code.eq(projectCode));
		}

		// searchable 변환 처리
		String field = searchable.getSearchField();
		switch (field) {
		case "recipientEmail":
			field = "recipient.email";
			break;
		case "recipientTeamName":
			field = "recipient.team.name";
			break;
		case "recipientName":
			field = "recipient.name";
			break;
		case "recipient":
			field = "recipient.username";
			break;
		case "recipientPositionName":
			field = "recipient.position.name";
			break;
		case "userType":
			field = "userType.name";
			break;
		}
		searchable.setSearchField(field);

		JqGrid jqgrid = new JqGrid(builder);
		Predicate predicate = jqgrid.applyPredicate(searchable);

		JPQLQuery query = new JPAQuery(em).from(ssr)
				.leftJoin(ssr.recipient, recipient)
				.where(predicate, conditions);

		Querydsl querydsl = new Querydsl(em, builder);
		querydsl.applyPagination(pageable, query);

		SearchResults<CustEvalGrdVO> search = query
				.listResults(new QCustEvalGrdVO(ssr.id, ssr.project.code,
						recipient.username, recipient.name, recipient.email,
						recipient.team.name, recipient.position.name,
						ssr.userType.id, ssr.sentDate));

		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 메일발송처리
	 * 
	 * @param project
	 *            : 프로젝트
	 * @param sendList
	 *            : 메일발송몰록
	 */
	@Override
	public void sendMail(Project project, String sendList) {
		List<String> rows = new ArrayList<>();

		try {
			rows = objectMapper.readValue(sendList,
					new TypeReference<List<String>>() {
					});

			// logger.info(String.format("[메일발송 사이즈] - %d", rows.size()));

			for (String uuid : rows) {
				SatisfactionSurveyRecipient memberInfo = memberService
						.findOne(Long.parseLong(uuid));

				// 메일발송은 Optional
				try {
					ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder
							.currentRequestAttributes();
					HttpServletRequest request = sra.getRequest();
					String baseUrl = String.format("%s://%s:%d%s",
							request.getScheme(), request.getServerName(),
							request.getServerPort(), request.getContextPath());
					baseUrl += "/mail/satisfaction/survey/" + uuid;

					// logger.info(String.format("[baseUrl] - %s", baseUrl));
					String body = "만족도 설문조사: <a href='" + baseUrl
							+ "'>설문조사</a>";

					mailService.sendMail("qmsadmin@gsretail.com", memberInfo
							.getRecipient().getEmail(),
							"[QMS] 만족도 설문조사에 참여해 주시기 바랍니다.", body);

					// 최초 메일발송일자 저장
					if (memberInfo.getSentDate() == null) {
						LocalDate date = new LocalDate();
						memberInfo.setSentDate(date);
						memberService.save(memberInfo);
					}

				} catch (Exception e) {
					logger.info(String.format("[메일발송 에러] - %s", e.getMessage()));
				}
			}
		} catch (Exception e) {
			logger.info(String.format("[메일발송 에러] - %s", e.getMessage()));
		}
	}

}
