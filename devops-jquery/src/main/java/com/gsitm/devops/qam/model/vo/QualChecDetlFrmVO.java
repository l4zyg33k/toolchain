package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.qam.model.QualityCheckResult;

@Getter
@Setter
public class QualChecDetlFrmVO {		
	//id
	private Long id;
	
	//QualityCheckResult
	private QualityCheckResult qualityCheckResult;
	
	//결과유형(코드)
	private Code result;
	
	//부적합내용
	private String content;
	
	//시정조치계획/결과
	private String action;	

	//권고일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate recommendedDate;

	//합의일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate appointedDate;
	
	//완료일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate completedDate;	
	
}
