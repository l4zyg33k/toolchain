package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class DocuEvalGrdVO {
	
	private Long id;
	
	//프로젝트 코드
	private String projectCode;
	
	//프로젝트명
	private String projectName;

	//문서 명칭
	private String name;

	//가중치
	private Float weight;

	//평점
	private Float point;

	
	public DocuEvalGrdVO() {
		super();
	}

	@QueryProjection
	public DocuEvalGrdVO(Long id, String projectCode, String projectName,
			String name, Float weight, Float point) {
		super();
		this.id = id;
		this.projectCode = projectCode;
		this.projectName = projectName;
		this.name = name;
		this.weight = weight;
		this.point = point;
	}
}
