package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.cmm.model.SharedWebVO;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProgEvalWebVO extends SharedWebVO<Long> {
	
	public String getMethod() {
		return isNew() ? "POST" : "PUT";
	}

	public String getActionUrl() {
		return isNew() ? "/qam/progeval" : String.format("/qam/progeval/%d",
				getId());
	}
}
