package com.gsitm.devops.qam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.cmm.model.PageParams;
import com.gsitm.devops.cmm.service.CommonCodeService;
import com.gsitm.devops.qam.service.biz.SurvResuBiz;

@Controller
public class SurvResuController {

	private static final String MENU_CODE = "SurvResu";
	
	private static final String INDEX_URL = "/qam/survresu";
	
	private static final String INDEX_PAGE = "qam/survresu/index";
	
	@Autowired
	private SurvResuBiz biz; 
	
	@Autowired
	private CommonCodeService cmmCodeService;	
	
	//메뉴코드로 반드시 선언되어야 한다.
	@ModelAttribute(PageParams.MENU_CODE)
	public String menuCode() {
		return MENU_CODE;
	}
	
	@ModelAttribute(PageParams.NAV_GRID)
	public Map<String, Boolean> getNavGrid(
			@AuthenticationPrincipal UserDetails user) {
		return biz.getNavGrid(user);
	}		

	/**
	 * @SpecialLogic 프로젝트 설문결과 조회  페이지 로딩시 
	 * 
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = INDEX_URL, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute(PageParams.PAGE, 1);
		model.addAttribute(PageParams.ROW_NUM, 20);
		model.addAttribute(PageParams.ROW_ID, "");
		return INDEX_PAGE;
	}
	

}
