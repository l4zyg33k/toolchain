/**
 * 품질점검 결과 디테일 엔티티
 * QualityCheckResultDetail.java
 */
package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class QualityCheckResultDetail extends AbstractAuditable<Account, Long> {

	/**
	 * 품질 점검 결과
	 */
	@ManyToOne
	private QualityCheckResult qualityCheckResult;

	/**
	 * 결과 유형 (코드)
	 */
	@ManyToOne
	private Code result;

	/**
	 * 부적합 내용
	 */
	@Column(length = 100)
	private String content;

	/**
	 * 시정 조치 계획/결과
	 */
	@Column(length = 256)
	private String action;

	/**
	 * 권고 일자
	 */
	private LocalDate recommendedDate;

	/**
	 * 합의 일자
	 */
	private LocalDate appointedDate;

	/**
	 * 완료 일자
	 */
	private LocalDate completedDate;
}
