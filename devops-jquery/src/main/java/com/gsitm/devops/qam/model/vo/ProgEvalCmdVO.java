package com.gsitm.devops.qam.model.vo;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
@SuppressWarnings("serial")
public class ProgEvalCmdVO extends ProgEvalWebVO {	
	//계획 측정일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate planDate;

	//실제 측정일
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate actualDate;

	//계획 진척률
	private Double planRate;

	//실제 진척률
	private Double actualRate;

	//진척도 평가 사유
	@Column(length = 100)
	private String reason;
}
