package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.qam.model.vo.CustEvalCmdVO;
import com.gsitm.devops.qam.model.vo.CustEvalGrdVO;
import com.gsitm.devops.qam.service.biz.CustEvalBiz;

@RestController
public class CustEvalRestController {

	@Autowired
	private CustEvalBiz biz; 
	
	/**
	 * @SpecialLogic 고객만족도 대상자 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<CustEvalGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/custeval", method = RequestMethod.POST)
	public Page<CustEvalGrdVO> findAll(
			Pageable pageable,
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, project);
	}
	
	/**
	 * @SpecialLogic 고객만족도 대상자  다건 조회  (jqGrid 조회)
	 * 
	 * @param pageable : 페이지 처리
	 * @param searchable : 조회조건
	 * @param projectCode : 프로젝트코드
	 * @return Page<CustEvalGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/custeval", method = RequestMethod.POST, params = { "_search=true" })
	public Page<CustEvalGrdVO> findAll(
			Pageable pageable, Searchable searchable, 
			@RequestParam(value = "project", defaultValue = "", required = false) String project) {
		return biz.findAll(pageable, searchable, project);
	}	
	
	/**
	 * @SpecialLogic 고객만족도 대상자 관리 등록, 수정, 삭제
	 * 
	 * @param oper
	 * @param progEvalCmdVO
	 */	
	@RequestMapping(value = "/rest/qam/custeval/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			CustEvalCmdVO custEvalCmdVO, BindingResult result) {
		switch (oper) {
		case "add":
			biz.create(custEvalCmdVO);
			break;
		case "edit":
			biz.update(custEvalCmdVO);
			break;
		case "del":
			biz.delete(custEvalCmdVO);
			break;
		}
	}
	
	/**
	 * @SpecialLogic 메일발송처리
	 * 
	 * @param page
	 * @param rowNum
	 * @param rowId
	 * @param code
	 * @param sendList
	 * @param model
	 * @return string
	 */		
	@RequestMapping(value = "rest/qam/custeval/send/{code}", method = RequestMethod.POST)
	public void sendMail(@PathVariable("code") Project project,
			@RequestParam("mailList") String sendList,
			Model model) {
		biz.sendMail(project, sendList);
	}	
}
