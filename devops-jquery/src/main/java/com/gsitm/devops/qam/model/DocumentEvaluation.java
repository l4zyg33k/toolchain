/**
 * 산출물 평가 엔티티
 * DocumentEvaluation.java
 */
package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.pms.model.Project;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class DocumentEvaluation extends AbstractAuditable<Account, Long> {

	/**
	 * 프로젝트
	 */
	@ManyToOne
	private Project project;

	/**
	 * 평가자
	 */
	private Account qa;

	/**
	 * 문서 명칭
	 */
	@Column(length = 100)
	private String name;

	/**
	 * 가중치
	 */
	@Column(precision = 8)
	private Float weight;

	/**
	 * 평점
	 */
	@Column(precision = 8)
	private Float point;
}
