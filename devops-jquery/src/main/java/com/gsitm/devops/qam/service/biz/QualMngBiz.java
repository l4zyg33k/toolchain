package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.qam.model.vo.QualMngFrmVO;
import com.gsitm.devops.qam.model.vo.QualMngGrdVO;

public interface QualMngBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public Page<QualMngGrdVO> findAll(Pageable pageable, UserDetails user, String project);

	public Page<QualMngGrdVO> findAll(Pageable pageable, Searchable searchable, UserDetails user, String project);
	
	public QualMngFrmVO findOne(String code);
}
