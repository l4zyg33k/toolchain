package com.gsitm.devops.qam.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.ProgressEvaluation;
import com.gsitm.devops.qam.model.QProgressEvaluation;
import com.gsitm.devops.qam.repository.ProgressEvaluationRepository;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * 프로젝트 Inspection 상세 서비스
 */
@Service
@Transactional(readOnly = true)
public class ProgressEvaluationServiceImpl implements ProgressEvaluationService {

	/**
	 * 프로젝트 Inspection 상세 리파지토리
	 */
	@Autowired
	private ProgressEvaluationRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	private final QProgressEvaluation qProgressEvaluation = QProgressEvaluation.progressEvaluation;

	@Override
	public boolean exists(Long id) {

		return repository.exists(id);
	}

	@Override
	public ProgressEvaluation findOne(Long id) {

		return repository.findOne(id);
	}

	@Override
	public ProgressEvaluation findOne(Predicate predicate) {

		return repository.findOne(predicate);
	}

	@Override
	public Iterable<ProgressEvaluation> findAll() {

		return repository.findAll();
	}

	@Override
	public Iterable<ProgressEvaluation> findAll(Sort sort) {

		return repository.findAll(sort);
	}

	@Override
	public Iterable<ProgressEvaluation> findAll(Predicate predicate) {

		return repository.findAll(predicate);
	}

	@Override
	public Iterable<ProgressEvaluation> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {

		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<ProgressEvaluation> findAll(Pageable pageable) {

		return repository.findAll(pageable);
	}

	@Override
	public Page<ProgressEvaluation> findAll(Predicate predicate,
			Pageable pageable) {

		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public ProgressEvaluation save(ProgressEvaluation progressEvaluation) {

		return repository.save(progressEvaluation);
	}

	@Transactional
	@Override
	public <S extends ProgressEvaluation> List<S> save(Iterable<S> iterable) {

		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {

		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(ProgressEvaluation progressEvaluation) {

		repository.delete(progressEvaluation);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends ProgressEvaluation> iterable) {

		repository.delete(iterable);
	}

	@Override
	public long count() {

		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {

		return repository.count(predicate);
	}

	@Override
	public Page<ProgressEvaluation> findAllJoinProjectStep(Predicate predicate,
			Pageable pageable) {

		Page<ProgressEvaluation> result = null;
		return result;
	};

	/**
	 * 계획대비진철률 계산
	 */
	@Override
	public double getProgressPerPlan(String code) {
		JPAQuery query = new JPAQuery(entityManager);
		double result1 = query
				.from(qProgressEvaluation)
				.where(qProgressEvaluation.project.code.eq(code))
				.setHint("org.hibernate.cacheable", "true")
				.uniqueResult(
						qProgressEvaluation.actualRate.sum().coalesce(
								(double) 0));
		JPAQuery query2 = new JPAQuery(entityManager);
		double result2 = query2
				.from(qProgressEvaluation)
				.where(qProgressEvaluation.project.code.eq(code))
				.setHint("org.hibernate.cacheable", "true")
				.uniqueResult(
						qProgressEvaluation.planRate.sum().coalesce((double) 0));
		if (result1 == 0 || result2 == 0) {
			return 0;
		}
		double result = Math.round(10000 * result1 / result2);
		return result / 100;
	}

}
