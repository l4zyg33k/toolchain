package com.gsitm.devops.qam.service.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.qam.model.QQualityCheckResult;
import com.gsitm.devops.qam.model.QQualityCheckResultDetail;
import com.gsitm.devops.qam.model.QualityCheckResult;
import com.gsitm.devops.qam.model.QualityCheckResultDetail;
import com.gsitm.devops.qam.model.vo.QQualChecDetlGrdVO;
import com.gsitm.devops.qam.model.vo.QQualChecFrmVO;
import com.gsitm.devops.qam.model.vo.QQualChecGrdVO;
import com.gsitm.devops.qam.model.vo.QualChecCmdVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlFrmVO;
import com.gsitm.devops.qam.model.vo.QualChecDetlGrdVO;
import com.gsitm.devops.qam.model.vo.QualChecFrmVO;
import com.gsitm.devops.qam.model.vo.QualChecGrdVO;
import com.gsitm.devops.qam.service.QualityCheckResultDetailService;
import com.gsitm.devops.qam.service.QualityCheckResultService;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;

@Service
@Getter
@Setter
@Slf4j
public class QualChecBizImpl implements QualChecBiz {

	//메시지 처리
	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private QualityCheckResultService resultService;
	
	@Autowired
	private QualityCheckResultDetailService resultDetailService;	
	
	@Autowired
	private ObjectMapper objectMapper;	
	
	@Autowired
	private CodeService codeService;	


	/**
	 * @SpecialLogic 사용자별 jqGrid 권한 
	 * 
	 * @param user : 사용자 정보 
	 * @return  Map<String, Boolean>
	 */		
	@Override
	public Map<String, Boolean> getNavGrid(UserDetails user) {
		Map<String, Boolean> navGrid = new HashMap<>();
		Set<String> authorities = AuthorityUtils.authorityListToSet(user.getAuthorities());
		
		if (authorities.contains("ROLE_USER")
				|| authorities.contains("ROLE_ADMIN")) {
			navGrid.put("add", true);
			navGrid.put("edit", true);
			navGrid.put("view", true);
			navGrid.put("del", true);
		} else {
			navGrid.put("add", false);
			navGrid.put("edit", false);
			navGrid.put("view", true);
			navGrid.put("del", false);
		}
		return navGrid;
	}
	
	/**
	 * @SpecialLogic 품질점검 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<QualChecGrdVO>
	 */	
	@Override
	public Page<QualChecGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode) {	
		QQualityCheckResult qc = QQualityCheckResult.qualityCheckResult;
		
		QProject project = new QProject("project");
		QAccount inspector = new QAccount("inspector");
		QAccount il = new QAccount("il");
		
		JPQLQuery query = new JPAQuery(em).from(qc)
				.leftJoin(qc.project, project)
				.leftJoin(qc.inspector, inspector)
				.leftJoin(qc.il, il)
				.where(qc.project.code.eq(projectCode));
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<QualityCheckResult>(QualityCheckResult.class, "qualityCheckResult"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<QualChecGrdVO> search = query
				.listResults(new QQualChecGrdVO(qc.id, project.code, project.name, 
						qc.title, qc.inspectedDate, qc.type.name, qc.stage.name, inspector.name, il.name));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}
	
	
	/**
	 * @SpecialLogic 품질점검 결과 상세조회
	 * 
	 * @param id
	 * @return  QualChecFrmVO
	 */		
	@Override
	public QualChecFrmVO findOne(Long id) {
		QQualityCheckResult qc = QQualityCheckResult.qualityCheckResult;
		
		QAccount inspector = new QAccount("inspector");
		QAccount il = new QAccount("il");
		QProject project = new QProject("project");
		
		JPQLQuery query = new JPAQuery(em).from(qc)
				.leftJoin(qc.inspector, inspector)
				.leftJoin(qc.project, project)
				.leftJoin(qc.il, il)
				.where(qc.id.eq(id)); 

		return query.uniqueResult(new QQualChecFrmVO(qc.id, project.code, project.name, il.name, il.username, 
				qc.title, qc.inspectedDate, qc.type.id, qc.stage.id, inspector.username, inspector.name,
				qc.hours));
	}	
	
	
	/**
	 * @SpecialLogic 품질점검 결과 저장 - 신규
	 * 
	 * @param qualChecCmdVO
	 * @param rowdata
	 */		
	@Override
	public void create(QualChecCmdVO qualChecCmdVO, String rowdata) {
		List<Map<String, String>> rows = new ArrayList<>();
		QualityCheckResult qc = new QualityCheckResult();
		BeanUtils.copyProperties(qualChecCmdVO, qc);
		
		//local 데이터소스인 경우 id 값에 jqg\\d+ 패턴으로 행 번호가 입력된다.
		rowdata = rowdata.replaceAll("jqg\\d+", "");
		System.out.println("rowdata :" + rowdata);
		
		try {
			if (rowdata != null) {
				rows = objectMapper.readValue(rowdata, new TypeReference<List<Map<String, String>>>() {});
			}
			
			if (rows.isEmpty() == false) {
				QualityCheckResult qualityCheckResult = resultService.save(qc);
				List<QualityCheckResultDetail> details = new ArrayList<>();
	
				for (Map<String, String> row : rows) {
					QualityCheckResultDetail qualityCheckResultDetail = new QualityCheckResultDetail();
					
					qualityCheckResultDetail.setResult(codeService.findOne(Long.parseLong(row.get("result"))));
					qualityCheckResultDetail.setContent(row.get("content"));
					qualityCheckResultDetail.setAction(row.get("action"));
					qualityCheckResultDetail.setRecommendedDate(LocalDate.parse(row.get("recommendedDate")));
					if(row.get("appointedDate") != null && !"".equals(row.get("appointedDate"))){
						qualityCheckResultDetail.setAppointedDate(LocalDate.parse(row.get("appointedDate")));
					}
					if(row.get("completedDate") != null && !"".equals(row.get("completedDate"))){
						qualityCheckResultDetail.setCompletedDate(LocalDate.parse(row.get("completedDate")));
					}
					qualityCheckResultDetail.setQualityCheckResult(qualityCheckResult);
					details.add(qualityCheckResultDetail);
				}
				details = resultDetailService.save(details);
				qc.setDetails(details);
			}	
			resultService.save(qc);
		} catch (Exception e) {
			log.error(e.getMessage());
		}		
	}
	
	/**
	 * @SpecialLogic 품질점검 결과 수정
	 * 
	 * @param qualChecCmdVO
	 * @param rowdata
	 */		
	@Override
	public void update(QualChecCmdVO qualChecCmdVO) {
		QualityCheckResult qc = resultService.findOne(qualChecCmdVO.getId());
		BeanUtils.copyProperties(qualChecCmdVO, qc);
		resultService.save(qc);
	}	

	/**
	 * @SpecialLogic 품질점검 결과 상세 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param qualityCheckResult : 품질점검 결과
	 * @return Page<QualChecDetlGrdVO>
	 */		
	@Override
	public Page<QualChecDetlGrdVO> findDtlAll(Pageable pageable, QualityCheckResult qualityCheckResult){
		QQualityCheckResultDetail qcd = QQualityCheckResultDetail.qualityCheckResultDetail;
		
		JPQLQuery query = new JPAQuery(em).from(qcd)
				.where(qcd.qualityCheckResult.eq(qualityCheckResult));
		
		Querydsl querydsl = new Querydsl(em, new PathBuilder<QualityCheckResultDetail>(QualityCheckResultDetail.class, "qualityCheckResultDetail"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<QualChecDetlGrdVO> search = query
				.listResults(new QQualChecDetlGrdVO(qcd.id, qcd.result.id, qcd.content,
						qcd.action, qcd.recommendedDate, qcd.appointedDate, qcd.completedDate));
		
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	/**
	 * @SpecialLogic 품질점검 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param projectCode : 프로젝트 코드
	 * @return Page<QualChecGrdVO>
	 */		
	@Override
	public Page<QualChecGrdVO> findAll(Pageable pageable, String projectCode) {
		QQualityCheckResult qc = QQualityCheckResult.qualityCheckResult;
		QQualityCheckResultDetail qcd = QQualityCheckResultDetail.qualityCheckResultDetail;
		
		QProject project = new QProject("project");
		QAccount inspector = new QAccount("inspector");
		QAccount il = new QAccount("il");
		
		JPQLQuery query = new JPAQuery(em).from(qc)
				.leftJoin(qc.project, project)
				.leftJoin(qc.inspector, inspector)
				.leftJoin(qc.il, il)
				.where(qc.project.code.eq(projectCode));
		 
		Querydsl querydsl = new Querydsl(em, new PathBuilder<QualityCheckResult>(QualityCheckResult.class, "qualityCheckResult"));
		querydsl.applyPagination(pageable, query);
		
		SearchResults<QualChecGrdVO> search = query
				.listResults(new QQualChecGrdVO(qc.id, project.code, project.name, 
						qc.title, qc.inspectedDate, qc.type.name, qc.stage.name, inspector.name, il.name));
		 
		List<QualChecGrdVO>  gualChecGrdVOList =  search.getResults();
		 
		int opinionCount = 0;
		int requiredCount = 0;
		for(QualChecGrdVO qualChecGrdVO : gualChecGrdVOList){
			 JPQLQuery dtlQuery = new JPAQuery(em).from(qcd).where(qcd.qualityCheckResult.id.eq(qualChecGrdVO.getId()));
			 
			 opinionCount = 0;
			 requiredCount = 0;
			 List<QualChecDetlGrdVO>  qualChecDetlGrdVOList =  dtlQuery.list(new QQualChecDetlGrdVO(qcd.id, qcd.result.id, qcd.content, qcd.action, qcd.recommendedDate, qcd.appointedDate, qcd.completedDate));
			 for(QualChecDetlGrdVO qualChecDetlGrdVO : qualChecDetlGrdVOList){
				 if(qualChecDetlGrdVO.getResult() == 93){
					 requiredCount++;
				 }else{
					 opinionCount++;
				 }
			 }
			 qualChecGrdVO.setOpinionCount(""+opinionCount);
			 qualChecGrdVO.setRequiredCount(""+requiredCount);
		}
		 
		return new PageImpl<>(gualChecGrdVOList, pageable, search.getTotal());
	}	
	
	/**
	 * @SpecialLogic 품질점검 결과 상세 정보 저장
	 * 
	 * @param qualChecDetlFrmVO : 품질점검 결과 상세 내역
	 * @param qualityCheckResult : 품질점검 결과
	 */		
	@Override
	public void create(QualChecDetlFrmVO qualChecDetlFrmVO, QualityCheckResult qualityCheckResult) {
		QualityCheckResultDetail qcd = new QualityCheckResultDetail();
		BeanUtils.copyProperties(qualChecDetlFrmVO, qcd);
		qcd.setQualityCheckResult(qualityCheckResult);
		resultDetailService.save(qcd);
	}	

	/**
	 * @SpecialLogic 품질점검 결과 상세 정보 수정
	 * 
	 * @param qualChecDetlFrmVO : 품질점검 결과 상세 내역
	 * @param qualityCheckResult : 품질점검 결과
	 */		
	@Override
	public void update(QualChecDetlFrmVO qualChecDetlFrmVO, QualityCheckResult qualityCheckResult) {
		QualityCheckResultDetail qcd = resultDetailService.findOne(qualChecDetlFrmVO.getId());
		BeanUtils.copyProperties(qualChecDetlFrmVO, qcd);
		qcd.setQualityCheckResult(qualityCheckResult);
		resultDetailService.save(qcd);
	}		

	/**
	 * @SpecialLogic 품질점검 결과 상세 정보 삭제
	 * 
	 * @param qualChecDetlFrmVO : 품질점검 결과 상세 내역
	 */		
	@Override
	public void delete(QualChecDetlFrmVO qualChecDetlFrmVO) {
		resultDetailService.delete(qualChecDetlFrmVO.getId());
	}

	/**
	 * @SpecialLogic 품질점검 결과 정보 삭제
	 * 
	 * @param qualChecDetlFrmVO : 품질점검 결과 상세 내역
	 */		
	@Override
	public void delete(QualChecCmdVO qualChecCmdVO) {
		resultService.delete(qualChecCmdVO.getId());
	}	
	
}
