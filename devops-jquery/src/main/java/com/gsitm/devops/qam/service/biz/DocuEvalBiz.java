package com.gsitm.devops.qam.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import com.gsitm.devops.qam.model.vo.DocuEvalCmdVO;
import com.gsitm.devops.qam.model.vo.DocuEvalFrmVO;
import com.gsitm.devops.qam.model.vo.DocuEvalGrdVO;

public interface DocuEvalBiz extends MessageSourceAware {
	public Map<String, Boolean> getNavGrid(UserDetails user);
	
	public DocuEvalFrmVO findOne(String code);
	
	public Page<DocuEvalGrdVO> findAll(Pageable pageable, UserDetails user, String projectCode);
	
	public void update(DocuEvalCmdVO docuEvalCmdVO);
	
	public void create(DocuEvalCmdVO docuEvalCmdVO, String projectCode);

	public void delete(DocuEvalCmdVO docuEvalCmdVO);		
}
