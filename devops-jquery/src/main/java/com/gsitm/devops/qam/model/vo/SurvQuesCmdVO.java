package com.gsitm.devops.qam.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.gsitm.devops.adm.model.Code;

@Getter
@Setter
@SuppressWarnings("serial")
public class SurvQuesCmdVO extends SurvQuesWebVO {	
	
	//사용자 유형 (코드)
	private Code userType;

	//설문 유형 (코드)
	private Code surveyType;

	//문항 유형 (코드)
	private String questionType;

	//문항 번호
	private Integer no;

	//키워드
	private String keyword;

	//설문 문항
	private String question;	
}
