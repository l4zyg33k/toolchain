package com.gsitm.devops.qam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.Code;

/**
 * 설문 문항
 */
@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class SatisfactionSurveyQuestion extends
		AbstractAuditable<Account, Long> {

	/**
	 * 사용자 유형 (코드)
	 */
	@ManyToOne
	private Code userType;

	/**
	 * 설문 유형 (코드)
	 */
	@ManyToOne
	private Code surveyType;

	/**
	 * 문항 유형 (코드)
	 */
	@ManyToOne
	private Code questionType;

	/**
	 * 문항 번호
	 */
	private Integer no;

	/**
	 * 키워드
	 */
	@Column(length = 64)
	private String keyword;

	/**
	 * 설문 문항
	 */
	@Column(length = 100)
	private String question;
}
