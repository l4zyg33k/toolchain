package com.gsitm.devops.qam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.qam.model.vo.ProgEvalCmdVO;
import com.gsitm.devops.qam.model.vo.ProgEvalGrdVO;
import com.gsitm.devops.qam.service.biz.ProgEvalBiz;

@RestController
public class ProgEvalRestController {

	@Autowired
	private ProgEvalBiz biz; 
	
	/**
	 * @SpecialLogic 진척도 평가 결과 다건 조회
	 * 
	 * @param pageable : 페이지 처리
	 * @param user : 사용자 정보
	 * @param projectCode : 프로젝트 코드
	 * @return Page<ProgEvalGrdVO>
	 */
	@RequestMapping(value = "/rest/qam/progeval", method = RequestMethod.POST)
	public Page<ProgEvalGrdVO> findAll(
			Pageable pageable,
			@AuthenticationPrincipal UserDetails user,
			@RequestParam(value = "projectCode", defaultValue = "", required = false) String projectCode) {
		return biz.findAll(pageable, user, projectCode);
	}
	
	/**
	 * @SpecialLogic 진척도 평가 결과 수정
	 * 
	 * @param oper
	 * @param progEvalCmdVO
	 */	
	@RequestMapping(value = "/rest/qam/progeval/edit", method = RequestMethod.POST)
	public void formEditing(@RequestParam("oper") String oper,
			ProgEvalCmdVO progEvalCmdVO) {
		switch (oper) {
		case "edit":
			biz.update(progEvalCmdVO);
			break;
		}
	}
	
	/**
	 * @SpecialLogic 계획대비 진척률
	 * 
	 * @param searchable
	 */
	@RequestMapping(value = "/rest/qam/progeval/plan/{code}", method = RequestMethod.GET)
	@ResponseBody
	public String getProgressPerPlan(@PathVariable("code") String code) {
		return biz.getProgressPerPlan(code);
	}		
}
