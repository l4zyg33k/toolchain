package com.gsitm.devops.qam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.qam.model.QualityCheckResult;
import com.gsitm.devops.qam.repository.QualityCheckResultRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class QualityCheckResultServiceImpl implements QualityCheckResultService {

	@Autowired
	private QualityCheckResultRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public QualityCheckResult findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public QualityCheckResult findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<QualityCheckResult> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<QualityCheckResult> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<QualityCheckResult> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<QualityCheckResult> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<QualityCheckResult> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<QualityCheckResult> findAll(Predicate predicate,
			Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public QualityCheckResult save(QualityCheckResult QualityCheckResult) {
		return repository.save(QualityCheckResult);
	}

	@Transactional
	@Override
	public <S extends QualityCheckResult> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(QualityCheckResult QualityCheckResult) {
		repository.delete(QualityCheckResult);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends QualityCheckResult> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
