package com.gsitm.devops.adm.model.vo;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AcntItemVO {

	private String username;
	private String name;
	private String team;
	private String position;
	private String email;

	@QueryProjection
	public AcntItemVO(String username, String name, String team) {
		super();
		this.username = username;
		this.name = name;
		this.team = team;
	}
	
	@QueryProjection
	public AcntItemVO(String username, String name, String team,
			String position, String email) {
		super();
		this.username = username;
		this.name = name;
		this.team = team;
		this.position = position;
		this.email = email;
	}
}
