package com.gsitm.devops.adm.model.vo;

import lombok.Getter;
import lombok.Setter;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class AcntMngGrdVO {

	private String username;

	private String name;

	private Long position;

	private Long team;

	private String email;

	private Boolean editable;

	public AcntMngGrdVO() {
		super();
	}

	@QueryProjection
	public AcntMngGrdVO(String username, String name, Long position, Long team,
			String email) {
		super();
		this.username = username;
		this.name = name;
		this.position = position;
		this.team = team;
		this.email = email;
		this.editable = Boolean.TRUE;
	}

	@QueryProjection
	public AcntMngGrdVO(String username, String name, Long position, Long team,
			String email, Boolean editable) {
		super();
		this.username = username;
		this.name = name;
		this.position = position;
		this.team = team;
		this.email = email;
		this.editable = editable;
	}
}
