package com.gsitm.devops.adm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.model.Menu;
import com.gsitm.devops.adm.repository.MenuRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class MenuServiceImpl implements MenuService {

	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private MenuRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Menu findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Menu findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Menu> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Menu> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Menu> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Menu> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Menu> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Menu> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Menu save(Menu menu) {
		return repository.save(menu);
	}

	@Transactional
	@Override
	public <S extends Menu> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Menu menu) {
		repository.delete(menu);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Menu> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}
}
