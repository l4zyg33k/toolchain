package com.gsitm.devops.adm.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.adm.model.vo.RoleMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;

public interface RoleBiz extends MessageSourceAware {

	public Page<RoleMngGrdVO> findAll(Pageable pageable);

	public Page<RoleMngGrdVO> findAll(Pageable pageable, Searchable searchable);

	public Map<String, String> getAuthorities();

	public String create(RoleMngGrdVO roleMngGrdVO);

	public String update(RoleMngGrdVO roleMngGrdVO);

	public String delete(RoleMngGrdVO roleMngGrdVO);
}
