/**
 * 공통코드 정보
 * Code.java
 * @author Administrator
 */
package com.gsitm.devops.adm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "value",
		"parent_id" }))
@Getter
@Setter
@Audited
@SuppressWarnings("serial")
public class Code extends AbstractAuditable<Account, Long> {

	/**
	 * 코드 값
	 */
	@Column(length = 8)
	private String value;

	/**
	 * 코드 명칭
	 */
	@Column(length = 32)
	private String name;

	/**
	 * 표시 순서
	 */
	private Integer rank = 0;

	/**
	 * 사용 안함
	 */
	private Boolean enabled = true;

	/**
	 * 코드 설명
	 */
	@Column(length = 64)
	private String description;

	/**
	 * 상위 코드
	 */
	@ManyToOne
	private Code parent;

	/**
	 * 하위 코드
	 */
	@OneToMany(mappedBy = "parent", orphanRemoval = true)
	private List<Code> children;

	@Override
	public String toString() {
		return this.getId().toString();
	}
}
