package com.gsitm.devops.adm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.cmm.repository.SharedRepository;

@Repository
public interface AttachmentFileRepository extends
		SharedRepository<AttachmentFile, Long> {
}