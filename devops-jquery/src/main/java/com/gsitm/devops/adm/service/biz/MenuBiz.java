package com.gsitm.devops.adm.service.biz;

import java.util.List;
import java.util.Set;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.adm.model.vo.MenuInfoVO;
import com.gsitm.devops.adm.model.vo.MenuItemVO;
import com.gsitm.devops.adm.model.vo.MenuMngFrmVO;
import com.gsitm.devops.adm.model.vo.MenuMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;

public interface MenuBiz extends MessageSourceAware {

	public String create(MenuMngFrmVO vo);

	public String create(MenuMngFrmVO vo, Long id);

	public String update(MenuMngFrmVO vo);

	public String delete(MenuMngFrmVO vo);

	public Page<MenuMngGrdVO> getParents(Pageable pageable);

	public Page<MenuMngGrdVO> getParents(Pageable pageable,
			Searchable searchable);

	public Page<MenuMngGrdVO> getChildren(Pageable pageable, Long id);

	public Page<MenuMngGrdVO> getChildren(Pageable pageable,
			Searchable searchable, Long id);

	public List<MenuInfoVO> getMenusByAuthorities(Set<String> authorities);
	
	public List<MenuItemVO> getMenuItems(String menuCode, List<MenuInfoVO> menus);

	public List<String> getMenuRoles(Long id);

	public String getAuthorities();
}
