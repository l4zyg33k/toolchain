package com.gsitm.devops.adm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.adm.model.Role;
import com.gsitm.devops.cmm.repository.SharedRepository;

@Repository
public interface RoleRepository extends
		SharedRepository<Role, String> {
}
