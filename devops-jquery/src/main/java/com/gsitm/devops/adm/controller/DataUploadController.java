package com.gsitm.devops.adm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.adm.service.RoleService;

@Controller
public class DataUploadController {

	@Autowired
	AccountService accountService;

	@Autowired
	CodeService codeService;

	@Autowired
	RoleService authorityService;

	@RequestMapping(value = "/adm/dataupload", method = RequestMethod.GET)
	public String index() {

		return "adm/dataupload/index";
	}

	// 등록
	@RequestMapping(value = "/adm/dataupload", method = RequestMethod.POST)
	public String create(Model model,
			@RequestParam(value = "entity") String entity,
			@RequestParam(value = "file") CommonsMultipartFile file)
			throws IllegalStateException, IOException, InvalidFormatException {

		if (!file.isEmpty()) {

			// String fileExt =
			// file.getOriginalFilename().split("\\.(?=[^\\.]+$)")[1];
			// file.getContentType()

			// if (fileExt.equals("xls") || fileExt.equals("xlsx")) {

			// }

			Workbook workbook = WorkbookFactory.create(file.getInputStream());

			Sheet sheet = workbook.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows();

			switch (entity) {
			case "Account":
				/*
				 * username,password,email,name,position,team,authorities,enabled
				 */
				List<Account> accounts = new ArrayList<>();

				try {
					for (int i = 1; i < rows; i++) {
						Row row = sheet.getRow(i);
						int cells = row.getPhysicalNumberOfCells();
						for (int j = 0; j < cells; j++)
							row.getCell(j).setCellType(Cell.CELL_TYPE_STRING);

						Account account = new Account();

						account.setUsername(row.getCell(0).getStringCellValue());
						account.setPassword(row.getCell(1).getStringCellValue());
						account.setEmail(row.getCell(2).getStringCellValue());
						account.setName(row.getCell(3).getStringCellValue());
						Long position = Long.valueOf(0);
						try {
							position = Long.valueOf(row.getCell(4)
									.getStringCellValue());
						} catch (Exception e) {
							model.addAttribute("errorMsg",
									"("
											+ String.valueOf(i)
											+ ",4) \""
											+ row.getCell(4)
													.getStringCellValue()
											+ "\" 직위코드는 정수여야합니다.");
							return "adm/dataupload/index";
						}
						if (codeService.exists(position)) {
							//account.setPosition(codeService.findOne(position));
						} else {
							model.addAttribute("errorMsg",
									"("
											+ String.valueOf(i)
											+ ",4) \""
											+ row.getCell(4)
													.getStringCellValue()
											+ "\" 존재하지 않는 직위코드입니다.");
							return "adm/dataupload/index";
						}
						Long team = Long.valueOf(0);
						try {
							team = Long.valueOf(row.getCell(5)
									.getStringCellValue());
						} catch (Exception e) {
							model.addAttribute("errorMsg",
									"("
											+ String.valueOf(i)
											+ ",5) \""
											+ row.getCell(5)
													.getStringCellValue()
											+ "\" 조직코드는 정수여야합니다.");
							return "adm/dataupload/index";
						}
						if (codeService.exists(team)) {
							//account.setTeam(codeService.findOne(team));
						} else {
							model.addAttribute("errorMsg",
									"("
											+ String.valueOf(i)
											+ ",5) \""
											+ row.getCell(5)
													.getStringCellValue()
											+ "\" 존재하지 않는 조직코드입니다.");
							return "adm/dataupload/index";
						}

						if (authorityService.exists(row.getCell(6)
								.getStringCellValue())) {
						} else {
							model.addAttribute("errorMsg",
									"("
											+ String.valueOf(i)
											+ ",6) \""
											+ row.getCell(6)
													.getStringCellValue()
											+ "\" 존재하지 않는 권한코드입니다.");
							return "adm/dataupload/index";
						}

						account.setEnabled(row.getCell(7).getStringCellValue()
								.equals("Y") ? true : false);

						accounts.add(account);
					}

					accountService.save(accounts);
				} catch (Exception e) {
					model.addAttribute("errorMsg", e.getMessage());
				}

				break;
			case "LeaderWorkshop": // 리더워크샵 및 리스크 관리

				break;
			case "BaseITSM":

				break;

			default:
				break;
			}
		}

		return "adm/dataupload/index";
	}
}
