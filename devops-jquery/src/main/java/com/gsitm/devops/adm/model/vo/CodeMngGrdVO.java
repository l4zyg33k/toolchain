package com.gsitm.devops.adm.model.vo;

import java.util.Date;

import lombok.Getter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
public class CodeMngGrdVO {

	private Long id;

	private String value;

	private String name;

	private Integer rank;

	private Boolean enabled;

	private String description;

	@JsonProperty("createdBy.name")
	private String createdByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date createdDate;

	@JsonProperty("lastModifiedBy.name")
	private String lastModifiedByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date lastModifiedDate;

	@QueryProjection
	public CodeMngGrdVO(Long id, String value, String name, Integer rank,
			Boolean enabled, String description, String createdByName,
			Date createdDate, String lastModifiedByName, Date lastModifiedDate) {
		super();
		this.id = id;
		this.value = value;
		this.name = name;
		this.rank = rank;
		this.enabled = enabled;
		this.description = description;
		this.createdByName = createdByName;
		this.createdDate = createdDate;
		this.lastModifiedByName = lastModifiedByName;
		this.lastModifiedDate = lastModifiedDate;
	}
}
