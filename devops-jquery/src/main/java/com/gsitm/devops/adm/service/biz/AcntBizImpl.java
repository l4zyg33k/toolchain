package com.gsitm.devops.adm.service.biz;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Setter;

import org.jadira.usertype.spi.utils.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.QRole;
import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.adm.model.vo.QAcntItemVO;
import com.gsitm.devops.adm.model.vo.QAcntMngGrdVO;
import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
public class AcntBizImpl implements AcntBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RoleService roleService;

	@Autowired
	private CodeService codeService;

	@Autowired
	private AccountService accountService;

	@Override
	@CacheEvict(value = "userCache", key = "#vo.username")
	public String delete(AcntMngFrmVO vo) {
		accountService.delete(vo.getId());
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String create(AcntMngFrmVO vo) {
		Account account = new Account();
		if (StringUtils.isNotEmpty(vo.getNewPassword())) {
			account.setPassword(vo.getNewPassword());
		}
		BeanUtils.copyProperties(vo, account);
		accountService.save(account);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	@CacheEvict(value = "userCache", key = "#vo.username")
	public String update(AcntMngFrmVO vo) {
		Account account = accountService.findOne(vo.getUsername());
		account.getAuthorities().clear();
		account.getAuthorities().addAll(vo.getAuthorities());
		if (StringUtils.isNotEmpty(vo.getNewPassword())) {
			account.setPassword(vo.getNewPassword());
		}
		BeanUtils.copyProperties(vo, account, "authorities");
		accountService.save(account);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable) {
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team);
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Account>(
				Account.class, "account"));
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		QAccount account = QAccount.account;
		QCode position = new QCode("position");
		QCode team = new QCode("team");
		PathBuilder<Account> path = new PathBuilder<Account>(Account.class,
				"account");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.position, position)
				.leftJoin(account.team, team).where(predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<AcntMngGrdVO> search = query
				.listResults(new QAcntMngGrdVO(account.username, account.name,
						position.id, team.id, account.email));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public List<AcntItemVO> findAll(String term) {
		QAccount account = QAccount.account;
		QCode team = new QCode("team");
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.team, team)
				.where(account.name.contains(term));
		return query.list(new QAcntItemVO(account.username, account.name,
				team.name));
	}

	@Override
	public List<String> getUserRoles(String username) {
		QAccount account = QAccount.account;
		QRole role = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.authorities, role)
				.where(account.username.eq(username));
		return query.list(role.authority);
	}

	@Override
	public String getPositions() {
		Map<Long, String> results = codeService.getOptions("A1");
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}

	@Override
	public String getTeams() {
		Map<Long, String> results = codeService.getOptions("A2");
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}

	@Override
	public String getAuthorities() {
		Map<String, String> results = roleService.getAuthorities();
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}
	
	@Override
	public List<AcntItemVO> findAllData(String term) {
		QAccount account = QAccount.account;
		QCode team = new QCode("team");
		QCode position = new QCode("position");
		JPQLQuery query = new JPAQuery(em).from(account)
				.leftJoin(account.team, team)
				.leftJoin(account.position, position)
				.where(account.name.contains(term));
		return query.list(new QAcntItemVO(account.username, account.name,
				team.name, position.name, account.email));
	}	
}
