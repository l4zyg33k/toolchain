package com.gsitm.devops.adm.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.adm.service.biz.AcntBiz;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;

@RestController
public class AcntMngRestController {

	@Autowired
	private AcntBiz biz;

	@RequestMapping(value = "/rest/adm/acntmng", method = RequestMethod.POST)
	public Page<AcntMngGrdVO> findAll(Pageable pageable) {
		return biz.findAll(pageable);
	}

	@RequestMapping(value = "/rest/adm/acntmng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		return biz.findAll(pageable, searchable);
	}

	@RequestMapping(value = "/rest/adm/acntmng", method = RequestMethod.GET, params = { "term" })
	public List<AcntItemVO> findAll(@RequestParam("term") String term) {
		return biz.findAll(term);
	}

	@RequestMapping(value = "/rest/adm/acntmng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid AcntMngFrmVO acntMngFrmVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (acntMngFrmVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (acntMngFrmVO.getOper()) {
		case "add":
			biz.create(acntMngFrmVO);
			break;
		case "edit":
			biz.update(acntMngFrmVO);
			break;
		case "del":
			biz.delete(acntMngFrmVO);
			break;
		}
		
		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/adm/acntmng/{username}/authorities", method = RequestMethod.POST)
	public List<String> getUserRoles(@PathVariable String username) {
		return biz.getUserRoles(username);
	}

	@RequestMapping(value = "/rest/adm/acntmng/all", method = RequestMethod.GET, params = { "term" })
	public List<AcntItemVO> findAllData(@RequestParam("term") String term) {
		return biz.findAllData(term);
	}
}
