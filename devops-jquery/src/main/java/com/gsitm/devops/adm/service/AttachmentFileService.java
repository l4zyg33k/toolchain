package com.gsitm.devops.adm.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.service.SharedService;

public interface AttachmentFileService extends
		SharedService<AttachmentFile, Long> {

	public AttachmentFile save(Code container, MultipartFile file);

	public List<AttachmentFile> save(Code container, List<MultipartFile> files);

	public void download(Long id, HttpServletRequest request, HttpServletResponse response);
}
