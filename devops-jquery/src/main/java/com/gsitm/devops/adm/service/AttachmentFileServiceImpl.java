package com.gsitm.devops.adm.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.repository.AttachmentFileRepository;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
@Slf4j
public class AttachmentFileServiceImpl implements AttachmentFileService {

	@Autowired
	private AttachmentFileRepository repository;

	@Value("${attachment.filepath}")
	private String filepath;

	private FileSystem fileSystem = FileSystems.getDefault();

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public AttachmentFile findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public AttachmentFile findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<AttachmentFile> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<AttachmentFile> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<AttachmentFile> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<AttachmentFile> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<AttachmentFile> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<AttachmentFile> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public AttachmentFile save(AttachmentFile attachmentFile) {
		return repository.save(attachmentFile);
	}

	@Transactional
	@Override
	public <S extends AttachmentFile> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(AttachmentFile attachmentFile) {
		repository.delete(attachmentFile);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends AttachmentFile> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Transactional
	@Override
	public AttachmentFile save(Code container, MultipartFile file) {
		AttachmentFile attachment = new AttachmentFile();
		if (file.isEmpty() == false) {
			try {
				String fileName = Paths.get(file.getOriginalFilename())
						.getFileName().toString();
				String contentType = file.getContentType();
				String digest = DigestUtils.md5Hex(file.getBytes());
				String diskDirectory = fileSystem.getPath(filepath,
						RandomStringUtils.randomAlphabetic(8)).toString();
				String diskFileName = UUID.randomUUID().toString();
				String pathname = fileSystem.getPath(diskDirectory,
						diskFileName).toString();
				FileUtils.forceMkdir(new File(filepath));
				FileUtils.forceMkdir(new File(diskDirectory));
				long fileSize = file.getSize();
				file.transferTo(new File(pathname));
				attachment.setContainer(container);
				attachment.setFileName(fileName);
				attachment.setContentType(contentType);
				attachment.setDigest(digest);
				attachment.setDiskDirectory(diskDirectory);
				attachment.setDiskFileName(diskFileName);
				attachment.setFileSize(fileSize);
				attachment = save(attachment);
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
		return attachment;
	}

	@Transactional
	@Override
	public List<AttachmentFile> save(Code container, List<MultipartFile> files) {
		List<AttachmentFile> attachments = new ArrayList<>();
		for (MultipartFile file : files) {
			if (file.isEmpty()) {
				continue;
			}
			try {
				String fileName = Paths.get(file.getOriginalFilename())
						.getFileName().toString();
				String contentType = file.getContentType();
				String digest = DigestUtils.md5Hex(file.getBytes());
				String diskDirectory = fileSystem.getPath(filepath,
						RandomStringUtils.randomAlphabetic(8)).toString();
				String diskFileName = UUID.randomUUID().toString();
				String pathname = fileSystem.getPath(diskDirectory,
						diskFileName).toString();
				FileUtils.forceMkdir(new File(filepath));
				FileUtils.forceMkdir(new File(diskDirectory));
				long fileSize = file.getSize();
				file.transferTo(new File(pathname));
				AttachmentFile attachment = new AttachmentFile();
				attachment.setContainer(container);
				attachment.setFileName(fileName);
				attachment.setContentType(contentType);
				attachment.setDigest(digest);
				attachment.setDiskDirectory(diskDirectory);
				attachment.setDiskFileName(diskFileName);
				attachment.setFileSize(fileSize);
				attachment = save(attachment);
				attachments.add(attachment);
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
		return attachments;
	}

	@Override
	public void download(Long id, HttpServletRequest request,
			HttpServletResponse response) {
		AttachmentFile file = repository.findOne(id);
		String fileName = file.getFileName();
		String userAgent = request.getHeader("User-Agent");
		try {
			if (userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("rv") > -1) {
				fileName = URLEncoder.encode(file.getFileName(), "UTF-8");
			} else {
				fileName = new String(file.getFileName().getBytes("UTF-8"),
						"8859_1");
			}
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
		}
		response.setContentType(file.getContentType());
		response.setContentLength(file.getFileSize().intValue());
		response.setHeader("Content-Disposition",
				String.format("attachment; filename=\"%s\"", fileName));
		try {
			String pathname = fileSystem.getPath(file.getDiskDirectory(),
					file.getDiskFileName()).toString();
			FileCopyUtils.copy(new FileInputStream(pathname),
					response.getOutputStream());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
}
