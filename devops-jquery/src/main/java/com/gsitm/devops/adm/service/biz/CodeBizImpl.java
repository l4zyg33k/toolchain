package com.gsitm.devops.adm.service.biz;

import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Setter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.model.vo.CodeMngFrmVO;
import com.gsitm.devops.adm.model.vo.CodeMngGrdVO;
import com.gsitm.devops.adm.model.vo.QCodeMngGrdVO;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
public class CodeBizImpl implements CodeBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CodeService codeService;

	@Override
	public String create(CodeMngFrmVO vo) {
		Code code = new Code();
		BeanUtils.copyProperties(vo, code);
		codeService.save(code);
		return messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public String create(CodeMngFrmVO vo, Long id) {
		Code code = new Code();
		BeanUtils.copyProperties(vo, code);
		Code parent = codeService.findOne(id);
		code.setParent(parent);
		codeService.save(code);
		return messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public String update(CodeMngFrmVO vo) {
		Code code = codeService.findOne(new Long(vo.getId()));
		BeanUtils.copyProperties(vo, code);
		codeService.save(code);
		return messageSource.getMessage("save.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public String delete(CodeMngFrmVO vo) {
		codeService.delete(new Long(vo.getId()));
		return messageSource.getMessage("remove.success", new Object[] {}, Locale.getDefault());
	}

	@Override
	public Page<CodeMngGrdVO> getParents(Pageable pageable) {
		QCode code = QCode.code;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		JPQLQuery query = new JPAQuery(em).from(code).leftJoin(code.createdBy, createdBy)
				.leftJoin(code.lastModifiedBy, lastModifiedBy).where(code.parent.isNull());
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Code>(Code.class, "code"));
		querydsl.applyPagination(pageable, query);
		SearchResults<CodeMngGrdVO> search = query.listResults(
				new QCodeMngGrdVO(code.id, code.value, code.name, code.rank, code.enabled, code.description,
						createdBy.name, code.createdDate, lastModifiedBy.name, code.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<CodeMngGrdVO> getParents(Pageable pageable, Searchable searchable) {
		QCode code = QCode.code;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		PathBuilder<Code> path = new PathBuilder<Code>(Code.class, "code");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(code).leftJoin(code.createdBy, createdBy)
				.leftJoin(code.lastModifiedBy, lastModifiedBy).where(code.parent.isNull(), predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<CodeMngGrdVO> search = query.listResults(
				new QCodeMngGrdVO(code.id, code.value, code.name, code.rank, code.enabled, code.description,
						createdBy.name, code.createdDate, lastModifiedBy.name, code.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<CodeMngGrdVO> getChildren(Pageable pageable, Long id) {
		QCode code = QCode.code;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		JPQLQuery query = new JPAQuery(em).from(code).leftJoin(code.createdBy, createdBy)
				.leftJoin(code.lastModifiedBy, lastModifiedBy).where(code.parent.id.eq(id));
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Code>(Code.class, "code"));
		querydsl.applyPagination(pageable, query);
		SearchResults<CodeMngGrdVO> search = query.listResults(
				new QCodeMngGrdVO(code.id, code.value, code.name, code.rank, code.enabled, code.description,
						createdBy.name, code.createdDate, lastModifiedBy.name, code.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<CodeMngGrdVO> getChildren(Pageable pageable, Searchable searchable, Long id) {
		QCode code = QCode.code;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		PathBuilder<Code> path = new PathBuilder<Code>(Code.class, "code");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(code).leftJoin(code.createdBy, createdBy)
				.leftJoin(code.lastModifiedBy, lastModifiedBy).where(code.parent.id.eq(id), predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<CodeMngGrdVO> search = query.listResults(
				new QCodeMngGrdVO(code.id, code.value, code.name, code.rank, code.enabled, code.description,
						createdBy.name, code.createdDate, lastModifiedBy.name, code.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Map<Long, String> getOptions(String value) {
		return codeService.getOptions(value);
	}
}
