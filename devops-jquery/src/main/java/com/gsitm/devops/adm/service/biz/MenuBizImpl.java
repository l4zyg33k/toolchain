package com.gsitm.devops.adm.service.biz;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Setter;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.gsitm.devops.adm.model.Menu;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.model.QMenu;
import com.gsitm.devops.adm.model.QRole;
import com.gsitm.devops.adm.model.vo.MenuInfoVO;
import com.gsitm.devops.adm.model.vo.MenuItemVO;
import com.gsitm.devops.adm.model.vo.MenuMngFrmVO;
import com.gsitm.devops.adm.model.vo.MenuMngGrdVO;
import com.gsitm.devops.adm.model.vo.QMenuInfoVO;
import com.gsitm.devops.adm.model.vo.QMenuMngGrdVO;
import com.gsitm.devops.adm.service.MenuService;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
public class MenuBizImpl implements MenuBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private MenuService menuService;

	@Autowired
	private RoleService roleService;

	@Override
	@CacheEvict(value = "menuCache", allEntries = true)
	public String create(MenuMngFrmVO vo) {
		Menu menu = new Menu();
		BeanUtils.copyProperties(vo, menu);
		menuService.save(menu);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	@CacheEvict(value = "menuCache", allEntries = true)
	public String create(MenuMngFrmVO vo, Long id) {
		Menu menu = new Menu();
		BeanUtils.copyProperties(vo, menu);
		Menu parent = menuService.findOne(id);
		menu.setParent(parent);
		menuService.save(menu);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	@CacheEvict(value = "menuCache", allEntries = true)
	public String update(MenuMngFrmVO vo) {
		Menu menu = menuService.findOne(vo.getId());
		BeanUtils.copyProperties(vo, menu);
		menuService.save(menu);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	@CacheEvict(value = "menuCache", allEntries = true)
	public String delete(MenuMngFrmVO vo) {
		menuService.delete(vo.getId());
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public Page<MenuMngGrdVO> getParents(Pageable pageable) {
		QMenu menu = QMenu.menu;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		JPQLQuery query = new JPAQuery(em).from(menu)
				.leftJoin(menu.createdBy, createdBy)
				.leftJoin(menu.lastModifiedBy, lastModifiedBy)
				.where(menu.parent.isNull());
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Menu>(Menu.class,
				"menu"));
		querydsl.applyPagination(pageable, query);
		SearchResults<MenuMngGrdVO> search = query
				.listResults(new QMenuMngGrdVO(menu.id, menu.code, menu.name,
						menu.href, menu.rank, menu.enabled, createdBy.name,
						menu.createdDate, lastModifiedBy.name,
						menu.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<MenuMngGrdVO> getParents(Pageable pageable,
			Searchable searchable) {
		QMenu menu = QMenu.menu;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		PathBuilder<Menu> path = new PathBuilder<Menu>(Menu.class, "menu");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(menu)
				.leftJoin(menu.createdBy, createdBy)
				.leftJoin(menu.lastModifiedBy, lastModifiedBy)
				.where(menu.parent.isNull(), predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<MenuMngGrdVO> search = query
				.listResults(new QMenuMngGrdVO(menu.id, menu.code, menu.name,
						menu.href, menu.rank, menu.enabled, createdBy.name,
						menu.createdDate, lastModifiedBy.name,
						menu.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<MenuMngGrdVO> getChildren(Pageable pageable, Long id) {
		QMenu menu = QMenu.menu;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		JPQLQuery query = new JPAQuery(em).from(menu)
				.leftJoin(menu.createdBy, createdBy)
				.leftJoin(menu.lastModifiedBy, lastModifiedBy)
				.where(menu.parent.id.eq(id));
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Menu>(Menu.class,
				"menu"));
		querydsl.applyPagination(pageable, query);
		SearchResults<MenuMngGrdVO> search = query
				.listResults(new QMenuMngGrdVO(menu.id, menu.code, menu.name,
						menu.href, menu.rank, menu.enabled, createdBy.name,
						menu.createdDate, lastModifiedBy.name,
						menu.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<MenuMngGrdVO> getChildren(Pageable pageable,
			Searchable searchable, Long id) {
		QMenu menu = QMenu.menu;
		QAccount createdBy = new QAccount("createdBy");
		QAccount lastModifiedBy = new QAccount("lastModifiedBy");
		PathBuilder<Menu> path = new PathBuilder<Menu>(Menu.class, "menu");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(menu)
				.leftJoin(menu.createdBy, createdBy)
				.leftJoin(menu.lastModifiedBy, lastModifiedBy)
				.where(menu.parent.id.eq(id), predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<MenuMngGrdVO> search = query
				.listResults(new QMenuMngGrdVO(menu.id, menu.code, menu.name,
						menu.href, menu.rank, menu.enabled, createdBy.name,
						menu.createdDate, lastModifiedBy.name,
						menu.lastModifiedDate));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Cacheable(value = "menuCache", key = "#authorities.hashCode()")
	@Override
	public List<MenuInfoVO> getMenusByAuthorities(Set<String> authorities) {
		QMenu parent = new QMenu("parent");
		QMenu child = new QMenu("child");
		QRole parentRole = new QRole("parentRole");
		QRole childRole = new QRole("childRole");
		return new JPAQuery(em)
				.from(parent)
				.innerJoin(parent.authorities, parentRole)
				.innerJoin(parent.children, child)
				.innerJoin(child.authorities, childRole)
				.where(parent.parent.isNull(),
						parentRole.authority.in(authorities),
						childRole.authority.in(authorities),
						parent.enabled.isTrue(), child.enabled.isTrue())
				.orderBy(parent.rank.asc(), child.rank.asc())
				.distinct()
				.list(new QMenuInfoVO(parent.name, child.code, child.name,
						child.href));
	}

	@Override
	public List<MenuItemVO> getMenuItems(String menuCode, List<MenuInfoVO> menus) {

		ListMultimap<String, MenuItemVO> menuItems = LinkedListMultimap
				.create();
		for (MenuInfoVO menu : menus) {
			String key = menu.getParentName();
			MenuItemVO value = new MenuItemVO(menu.getChildName(),
					menu.getChildHref(), menu.getChildCode().equals(menuCode));
			menuItems.put(key, value);
		}
		List<MenuItemVO> result = new ArrayList<>();
		for (String key : menuItems.keySet()) {
			Collection<MenuItemVO> values = menuItems.get(key);
			if (values.size() > 0) {
				MenuItemVO parent = new MenuItemVO(key);
				parent.setMenus(Lists.newArrayList(values.iterator()));
				parent.setHref(parent.getMenus().get(0).getHref());
				int selected = CollectionUtils.countMatches(values,
						new org.apache.commons.collections.Predicate() {
							@Override
							public boolean evaluate(Object obj) {
								MenuItemVO vo = (MenuItemVO) obj;
								return vo.getSelected().equals(Boolean.TRUE);
							}
						});
				if (selected > 0) {
					parent.setSelected(Boolean.TRUE);
				}
				result.add(parent);
			}
		}
		return result;
	}

	@Override
	public List<String> getMenuRoles(Long id) {
		QMenu menu = QMenu.menu;
		QRole role = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(menu)
				.leftJoin(menu.authorities, role).where(menu.id.eq(id));
		return query.list(role.authority);
	}

	@Override
	public String getAuthorities() {
		Map<String, String> results = roleService.getAuthorities();
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}
}
