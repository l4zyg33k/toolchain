package com.gsitm.devops.adm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.adm.model.Menu;
import com.gsitm.devops.cmm.repository.SharedRepository;

@Repository
public interface MenuRepository extends SharedRepository<Menu, Long> {
}
