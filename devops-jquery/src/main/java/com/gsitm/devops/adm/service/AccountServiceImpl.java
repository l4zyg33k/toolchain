package com.gsitm.devops.adm.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.model.QAccount;
import com.gsitm.devops.adm.repository.AccountRepository;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository repository;

	@PersistenceContext
	private EntityManager em;

	@Override
	public boolean exists(String username) {
		return repository.exists(username);
	}

	@Override
	public Account findOne(String username) {
		return repository.findOne(username);
	}

	@Override
	public Account findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Account> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Account> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Account> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Account> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Account> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Account> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Account save(Account account) {
		return repository.save(account);
	}

	@Transactional
	@Override
	public <S extends Account> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(String username) {
		repository.delete(username);
	}

	@Transactional
	@Override
	public void delete(Account account) {
		repository.delete(account);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Account> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Override
	public Account getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		// @Async의 thread에 의한 작업은 보안 컨텍스트가 없다. 때문에 null을 검사한다.
		if (authentication == null) {
			return null;
		} else {
			return repository.findOne(authentication.getName());
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		QAccount principal = QAccount.account;
		Account account = new JPAQuery(em).from(principal)
				.leftJoin(principal.authorities).fetch()
				.where(principal.username.eq(username)).singleResult(principal);
		if (account == null) {
			throw new UsernameNotFoundException(username);
		}
		return account;
	}
}
