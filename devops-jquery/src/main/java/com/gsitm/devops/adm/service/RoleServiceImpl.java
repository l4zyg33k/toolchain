package com.gsitm.devops.adm.service;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.model.QRole;
import com.gsitm.devops.adm.model.Role;
import com.gsitm.devops.adm.repository.RoleRepository;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class RoleServiceImpl implements RoleService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RoleRepository repository;

	@Override
	public boolean exists(String role) {
		return repository.exists(role);
	}

	@Override
	public Role findOne(String role) {
		return repository.findOne(role);
	}

	@Override
	public Role findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Role> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Role> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Role> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Role> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Role> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Role> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Role save(Role role) {
		return repository.save(role);
	}

	@Transactional
	@Override
	public <S extends Role> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(String role) {
		repository.delete(role);
	}

	@Transactional
	@Override
	public void delete(Role role) {
		repository.delete(role);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Role> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Override
	public Map<String, String> getAuthorities() {
		QRole qRole = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(qRole);
		return query.map(qRole.authority, qRole.name);
	}

	@Override
	public List<Role> findAuthorityIn(List<String> roles) {
		QRole qRole = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(qRole).where(
				qRole.authority.in(roles));
		return query.list(qRole);
	}
}
