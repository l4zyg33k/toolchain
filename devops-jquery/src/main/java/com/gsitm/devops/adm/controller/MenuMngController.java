package com.gsitm.devops.adm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.adm.service.biz.MenuBiz;

@Controller
public class MenuMngController {

	@Autowired
	private MenuBiz biz;

	@ModelAttribute("menuCode")
	public String menuCode() {
		return "MenuMng";
	}

	@ModelAttribute("authorities")
	public String getAuthorities() {
		return biz.getAuthorities();
	}

	@RequestMapping(value = "/adm/menumng", method = RequestMethod.GET)
	public String index() {
		return "adm/menumng/index";
	}
}
