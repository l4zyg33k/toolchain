/**
 * 첨부 파일 엔티티
 * AttachmentFile.java
 */
package com.gsitm.devops.adm.model;

import java.io.File;
import java.nio.file.FileSystems;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.PostRemove;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.io.FileUtils;
import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class AttachmentFile extends AbstractAuditable<Account, Long> {

	/**
	 * 컨테이너 (코드)
	 */
	@ManyToOne
	private Code container;

	/**
	 * 파일 이름
	 */
	@Column(length = 256)
	private String fileName;

	/**
	 * 디스크 상의 파일 이름
	 */
	@Column(length = 64)
	private String diskFileName;

	/**
	 * 디스크 상의 폴더 이름
	 */
	@Column(length = 64)
	private String diskDirectory;

	/**
	 * 파일 크기
	 */
	private Long fileSize;

	/**
	 * 파일 타입
	 */
	@Column(length = 128)
	private String contentType;

	/**
	 * 해시 코드
	 */
	@Column(length = 64)
	private String digest;

	/**
	 * 파일 설명
	 */
	@Column(length = 256)
	private String description;

	/**
	 * 파일 업로드
	 */
	@Transient
	private CommonsMultipartFile file;

	@PostRemove
	private void onPostRemove() {
		boolean isEmpty = StringUtils.isEmpty(diskDirectory)
				&& StringUtils.isEmpty(diskFileName);
		if (!isEmpty) {
			String pathname = FileSystems.getDefault()
					.getPath(diskDirectory, diskFileName).toString();
			FileUtils.deleteQuietly(new File(pathname));
		}
	}
}
