package com.gsitm.devops.adm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.adm.service.biz.AcntBiz;

@Controller
public class AcntMngController {

	@Autowired
	private AcntBiz biz;

	@ModelAttribute("menuCode")
	public String menuCode() {
		return "AcntMng";
	}

	@ModelAttribute("positions")
	public String getPositions() {
		return biz.getPositions();
	}

	@ModelAttribute("teams")
	public String getTeams() {
		return biz.getTeams();
	}

	@ModelAttribute("authorities")
	public String getAuthorities() {
		return biz.getAuthorities();
	}

	@RequestMapping(value = "/adm/acntmng", method = RequestMethod.GET)
	public String index() {
		return "adm/acntmng/index";
	}
}
