package com.gsitm.devops.adm.model.vo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
@SuppressWarnings("serial")
public class AtmtFileVO implements Persistable<Long> {

	private String oper;

	private Long id;

	private String fileName;

	private Long fileSize;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date createdDate;

	@JsonProperty("createdBy.name")
	private String createdByName;

	public AtmtFileVO() {
		super();
	}

	@QueryProjection
	public AtmtFileVO(Long id, String fileName, Long fileSize,
			Date createdDate, String createdByName) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.createdDate = createdDate;
		this.createdByName = createdByName;
	}

	@Override
	public boolean isNew() {
		return null == getId();
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
