package com.gsitm.devops.adm.model.vo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class MenuMngGrdVO {

	private Long id;

	private String code;

	private String name;

	private String href;

	private Integer rank;

	private Boolean enabled;

	@JsonProperty("createdBy.name")
	private String createdByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date createdDate;

	@JsonProperty("lastModifiedBy.name")
	private String lastModifiedByName;

	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+9")
	private Date lastModifiedDate;

	public MenuMngGrdVO() {
		super();
	}

	@QueryProjection
	public MenuMngGrdVO(Long id, String code, String name, String href,
			Integer rank, Boolean enabled, String createdByName,
			Date createdDate, String lastModifiedByName, Date lastModifiedDate) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.href = href;
		this.rank = rank;
		this.enabled = enabled;
		this.createdByName = createdByName;
		this.createdDate = createdDate;
		this.lastModifiedByName = lastModifiedByName;
		this.lastModifiedDate = lastModifiedDate;
	}
}
