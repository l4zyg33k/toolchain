package com.gsitm.devops.adm.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.adm.service.biz.RoleBiz;

@Controller
public class RoleMngController {

	@Autowired
	private RoleBiz biz;
	
	@ModelAttribute("menuCode")
	public String menuCode() {
		return "RoleMng";
	}	

	@RequestMapping(value = "/adm/rolemng", method = RequestMethod.GET)
	public String index() {
		return "adm/rolemng/index";
	}

	@RequestMapping(value = "/ajax/select/authorities", method = RequestMethod.GET)
	public String ajaxSelect(Model model) {
		Map<String, String> options = biz.getAuthorities();
		model.addAttribute("options", options);
		return "ajax/select";
	}
}
