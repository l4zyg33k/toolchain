package com.gsitm.devops.adm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.RoleMngGrdVO;
import com.gsitm.devops.adm.service.biz.RoleBiz;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;

@RestController
public class RoleMngRestController {

	@Autowired
	private RoleBiz biz;

	@RequestMapping(value = "/rest/adm/rolemng", method = RequestMethod.POST)
	public Page<RoleMngGrdVO> findAll(Pageable pageable) {
		return biz.findAll(pageable);
	}

	@RequestMapping(value = "/rest/adm/rolemng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<RoleMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		return biz.findAll(pageable, searchable);
	}

	@RequestMapping(value = "/rest/adm/rolemng/edit", method = RequestMethod.POST)
	public JqResult formEditing(@Valid RoleMngGrdVO roleMngGrdVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (roleMngGrdVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (roleMngGrdVO.getOper()) {
		case "add":
			biz.create(roleMngGrdVO);
			break;
		case "edit":
			biz.update(roleMngGrdVO);
			break;
		case "del":
			biz.delete(roleMngGrdVO);
			break;
		}

		return new JqSuccess();
	}
}
