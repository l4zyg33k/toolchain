package com.gsitm.devops.adm.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.cmm.service.SharedService;

public interface AccountService extends SharedService<Account, String> {

	public Account getCurrentAuditor();
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
