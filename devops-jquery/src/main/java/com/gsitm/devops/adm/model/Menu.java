/**
 * 메뉴관리 엔티티
 */
package com.gsitm.devops.adm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.jpa.domain.AbstractAuditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@SuppressWarnings("serial")
public class Menu extends AbstractAuditable<Account, Long> {

	/**
	 * 메뉴 코드
	 */
	@Column(length = 16, unique = true)
	private String code;

	/**
	 * 메뉴 이름
	 */
	@Column(length = 24)
	private String name;

	/**
	 * 뷰 이름
	 */
	@Column(length = 48)
	private String href;

	/**
	 * 사용 권한
	 */
	@ManyToMany
	private List<Role> authorities;

	/**
	 * 표시 순서
	 */
	private Integer rank = 0;

	/**
	 * 사용 안함
	 */
	private Boolean enabled = true;

	/**
	 * 상위 메뉴
	 */
	@ManyToOne
	private Menu parent;

	/**
	 * 하위 메뉴
	 */
	@OneToMany(mappedBy = "parent", orphanRemoval = true)
	@OrderBy("rank asc")
	private List<Menu> children;
}
