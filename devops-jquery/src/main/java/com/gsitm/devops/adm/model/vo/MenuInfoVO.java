package com.gsitm.devops.adm.model.vo;

import java.io.Serializable;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class MenuInfoVO implements Serializable {

	private String parentName;

	private String childCode;

	private String childName;

	private String childHref;

	@QueryProjection
	public MenuInfoVO(String parentName, String childCode, String childName,
			String childHref) {
		super();
		this.parentName = parentName;
		this.childCode = childCode;
		this.childName = childName;
		this.childHref = childHref;
	}
}
