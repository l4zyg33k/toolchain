/**
 * 사용자 정보
 * Account.java
 * @author Administrator
 */
package com.gsitm.devops.adm.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.envers.Audited;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@Audited
@SuppressWarnings("serial")
public class Account implements UserDetails, Persistable<String> {

	/**
	 * 아이디
	 */
	@Id
	@Column(length = 16)
	private String username;

	/**
	 * 비밀번호
	 */
	@Column(length = 64)
	private String password;

	/**
	 * 이메일
	 */
	@Column(length = 48)
	private String email;

	/**
	 * 이름
	 */
	@Column(length = 16)
	private String name;

	/**
	 * 직급 (코드)
	 */
	@ManyToOne
	private Code position;

	/**
	 * 소속 팀 (코드)
	 */
	@ManyToOne
	@JsonIgnoreProperties({"createdBy","createdDate","lastModifiedBy","lastModifiedDate"})
	private Code team;

	/**
	 * 권한
	 */
	@ManyToMany
	private List<Role> authorities;

	/**
	 * 계정 미만료
	 */
	private Boolean accountNonExpired = true;

	/**
	 * 계정 잠김 해제
	 */
	private Boolean accountNonLocked = true;

	/**
	 * 비밀번호 미만료
	 */
	private Boolean credentialsNonExpired = true;

	/**
	 * 사용 여부
	 */
	private Boolean enabled = true;

	@Override
	public String toString() {
		return getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public String getId() {
		return username;
	}

	@Override
	public boolean isNew() {
		return null == getId();
	}
}
