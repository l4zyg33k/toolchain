package com.gsitm.devops.adm.controller.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.MenuMngFrmVO;
import com.gsitm.devops.adm.model.vo.MenuMngGrdVO;
import com.gsitm.devops.adm.service.biz.MenuBiz;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;

@RestController
public class MenuMngRestController {

	@Autowired
	private MenuBiz biz;

	@RequestMapping(value = "/rest/adm/menumng", method = RequestMethod.POST)
	public Page<MenuMngGrdVO> getParents(Pageable pageable) {
		return biz.getParents(pageable);
	}

	@RequestMapping(value = "/rest/adm/menumng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<MenuMngGrdVO> getParents(Pageable pageable,
			Searchable searchable) {
		return biz.getParents(pageable, searchable);
	}

	@RequestMapping(value = "/rest/adm/menumng/{id}/children", method = RequestMethod.POST)
	public Page<MenuMngGrdVO> getChildren(@PathVariable("id") Long id,
			Pageable pageable) {
		return biz.getChildren(pageable, id);
	}

	@RequestMapping(value = "/rest/adm/menumng/{id}/children", method = RequestMethod.POST, params = { "_search=true" })
	public Page<MenuMngGrdVO> getChildren(@PathVariable("id") Long id,
			Pageable pageable, Searchable searchable) {
		return biz.getChildren(pageable, searchable, id);
	}

	@RequestMapping(value = "/rest/adm/menumng/edit", method = RequestMethod.POST)
	public JqResult formEditingParent(@Valid MenuMngFrmVO menuMngFrmVO,
			BindingResult result) {

		// 변경인 경우 유효성 검사를 한다.
		if (menuMngFrmVO.getOper().equals("edit") && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (menuMngFrmVO.getOper()) {
		case "add":
			biz.create(menuMngFrmVO);
			break;
		case "edit":
			biz.update(menuMngFrmVO);
			break;
		case "del":
			biz.delete(menuMngFrmVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/adm/menumng/{parent}/children/edit", method = RequestMethod.POST)
	public JqResult formEditingChild(@PathVariable("parent") Long parent,
			@Valid MenuMngFrmVO menuMngFrmVO, BindingResult result) {

		// 변경인 경우 유효성 검사를 한다.
		if (menuMngFrmVO.getOper().equals("edit") && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (menuMngFrmVO.getOper()) {
		case "add":
			biz.create(menuMngFrmVO, parent);
			break;
		case "edit":
			biz.update(menuMngFrmVO);
			break;
		case "del":
			biz.delete(menuMngFrmVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/adm/menumng/{id}/authorities", method = RequestMethod.POST)
	public List<String> getMenurRoles(@PathVariable Long id) {
		return biz.getMenuRoles(id);
	}
}
