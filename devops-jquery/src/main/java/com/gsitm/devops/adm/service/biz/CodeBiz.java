package com.gsitm.devops.adm.service.biz;

import java.util.Map;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.adm.model.vo.CodeMngFrmVO;
import com.gsitm.devops.adm.model.vo.CodeMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;

public interface CodeBiz extends MessageSourceAware {

	public String create(CodeMngFrmVO vo);

	public String create(CodeMngFrmVO vo, Long id);

	public String update(CodeMngFrmVO vo);

	public String delete(CodeMngFrmVO vo);

	public Page<CodeMngGrdVO> getParents(Pageable pageable);

	public Page<CodeMngGrdVO> getParents(Pageable pageable,
			Searchable searchable);

	public Page<CodeMngGrdVO> getChildren(Pageable pageable, Long id);

	public Page<CodeMngGrdVO> getChildren(Pageable pageable,
			Searchable searchable, Long id);

	public Map<Long, String> getOptions(String value);
}
