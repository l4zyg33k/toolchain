package com.gsitm.devops.adm.repository;

import org.springframework.stereotype.Repository;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.repository.SharedRepository;

@Repository
public interface CodeRepository extends SharedRepository<Code, Long> {
}
