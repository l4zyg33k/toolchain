package com.gsitm.devops.adm.model.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class MenuItemVO implements Serializable {

	private String name;

	private String href;

	private Boolean selected;

	private List<MenuItemVO> menus;

	public MenuItemVO() {
		super();
	}

	public MenuItemVO(String name) {
		super();
		this.name = name;
	}

	public MenuItemVO(String name, String href, Boolean selected) {
		super();
		this.name = name;
		this.href = href;
		this.selected = selected;
	}
}
