package com.gsitm.devops.adm.service.biz;

import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.Setter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.QRole;
import com.gsitm.devops.adm.model.Role;
import com.gsitm.devops.adm.model.vo.QRoleMngGrdVO;
import com.gsitm.devops.adm.model.vo.RoleMngGrdVO;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.Searchable;
import com.gsitm.devops.cmm.util.JqGrid;
import com.mysema.query.SearchResults;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Service
@Setter
public class RoleBizImpl implements RoleBiz {

	private MessageSource messageSource;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RoleService roleService;

	@Override
	public Page<RoleMngGrdVO> findAll(Pageable pageable) {
		QRole role = QRole.role;
		JPQLQuery query = new JPAQuery(em).from(role);
		Querydsl querydsl = new Querydsl(em, new PathBuilder<Role>(Role.class,
				"role"));
		querydsl.applyPagination(pageable, query);
		SearchResults<RoleMngGrdVO> search = query
				.listResults(new QRoleMngGrdVO(role.authority, role.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Page<RoleMngGrdVO> findAll(Pageable pageable, Searchable searchable) {
		QRole role = QRole.role;
		PathBuilder<Role> path = new PathBuilder<Role>(Role.class, "role");
		JqGrid jqgrid = new JqGrid(path);
		Predicate predicate = jqgrid.applyPredicate(searchable);
		JPQLQuery query = new JPAQuery(em).from(role).where(predicate);
		Querydsl querydsl = new Querydsl(em, path);
		querydsl.applyPagination(pageable, query);
		SearchResults<RoleMngGrdVO> search = query
				.listResults(new QRoleMngGrdVO(role.authority, role.name));
		return new PageImpl<>(search.getResults(), pageable, search.getTotal());
	}

	@Override
	public Map<String, String> getAuthorities() {
		return roleService.getAuthorities();
	}

	@Override
	public String create(RoleMngGrdVO roleMngGrdVO) {
		Role role = new Role();
		BeanUtils.copyProperties(roleMngGrdVO, role);
		roleService.save(role);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String update(RoleMngGrdVO roleMngGrdVO) {
		Role role = roleService.findOne(roleMngGrdVO.getAuthority());
		BeanUtils.copyProperties(roleMngGrdVO, role);
		roleService.save(role);
		return messageSource.getMessage("save.success", new Object[] {},
				Locale.getDefault());
	}

	@Override
	public String delete(RoleMngGrdVO roleMngGrdVO) {
		roleService.delete(roleMngGrdVO.getId());
		return messageSource.getMessage("remove.success", new Object[] {},
				Locale.getDefault());
	}
}
