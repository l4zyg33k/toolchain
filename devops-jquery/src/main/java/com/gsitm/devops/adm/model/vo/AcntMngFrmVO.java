package com.gsitm.devops.adm.model.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.Role;

@Getter
@Setter
public class AcntMngFrmVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String oper;

	private String id;

	@NotEmpty
	private String username;

	private String newPassword;

	@Email
	private String email;

	@NotEmpty
	private String name;

	private Code position;

	private Code team;

	private List<Role> authorities;

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
