package com.gsitm.devops.adm.service;

import com.gsitm.devops.adm.model.Menu;
import com.gsitm.devops.cmm.service.SharedService;

public interface MenuService extends SharedService<Menu, Long> {

}
