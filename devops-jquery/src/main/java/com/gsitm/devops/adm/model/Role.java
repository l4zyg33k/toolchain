/**
 * 권한관리 엔티티
 * Authority.java
 */
package com.gsitm.devops.adm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.envers.Audited;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;

@Entity
@EntityListeners({ AuditingEntityListener.class })
@Getter
@Setter
@Audited
@SuppressWarnings("serial")
public class Role implements GrantedAuthority, Persistable<String> {

	/**
	 * 권한 코드
	 */
	@Id
	@Column(length = 16)
	private String authority;

	/**
	 * 권한 명칭
	 */
	@Column(length = 16)
	private String name;

	@Override
	public String getId() {
		return authority;
	}

	@Override
	public boolean isNew() {
		return null == getId();
	}
}
