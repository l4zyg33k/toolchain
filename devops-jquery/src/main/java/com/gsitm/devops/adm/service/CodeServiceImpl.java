package com.gsitm.devops.adm.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.repository.CodeRepository;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Service
@Transactional(readOnly = true)
public class CodeServiceImpl implements CodeService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CodeRepository repository;

	@Override
	public boolean exists(Long id) {
		return repository.exists(id);
	}

	@Override
	public Code findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public Code findOne(Predicate predicate) {
		return repository.findOne(predicate);
	}

	@Override
	public Iterable<Code> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<Code> findAll(Sort sort) {
		return repository.findAll(sort);
	}

	@Override
	public Iterable<Code> findAll(Predicate predicate) {
		return repository.findAll(predicate);
	}

	@Override
	public Iterable<Code> findAll(Predicate predicate,
			OrderSpecifier<?>[] orderSpecifiers) {
		return repository.findAll(predicate, orderSpecifiers);
	}

	@Override
	public Page<Code> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Page<Code> findAll(Predicate predicate, Pageable pageable) {
		return repository.findAll(predicate, pageable);
	}

	@Transactional
	@Override
	public Code save(Code code) {
		return repository.save(code);
	}

	@Transactional
	@Override
	public <S extends Code> List<S> save(Iterable<S> iterable) {
		return repository.save(iterable);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Code code) {
		repository.delete(code);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends Code> iterable) {
		repository.delete(iterable);
	}

	@Override
	public long count() {
		return repository.count();
	}

	@Override
	public long count(Predicate predicate) {
		return repository.count(predicate);
	}

	@Deprecated
	@Override
	public String getJqGridSelectValue(String code) {

		Iterator<Code> children = findAll(QCode.code.parent.value.eq(code))
				.iterator();
		StringBuilder builder = new StringBuilder();

		while (children.hasNext()) {
			Code child = children.next();
			builder.append(String.format("%d:%s", child.getId().intValue(),
					child.getName()));

			if (children.hasNext()) {
				builder.append(";");
			}
		}

		return builder.toString();
	}

	@Deprecated
	@Override
	public String getJqGridSelectValue(Predicate predicate) {

		Iterator<Code> children = findAll(predicate).iterator();
		StringBuilder builder = new StringBuilder();

		while (children.hasNext()) {
			Code child = children.next();
			builder.append(String.format("%d:%s", child.getId().intValue(),
					child.getName()));

			if (children.hasNext()) {
				builder.append(";");
			}
		}

		return builder.toString();
	}

	@Cacheable("codeCache")
	@Override
	public Map<Long, String> getOptions(String value) {
		QCode code = QCode.code;
		JPQLQuery query = new JPAQuery(em).from(code)
				.where(code.parent.value.eq(value), code.enabled.eq(true))
				.orderBy(code.rank.asc(), code.value.asc());
		return query.map(code.id, code.name);
	}

	@Override
	public Code getCode(String value, String parent) {
		QCode code = QCode.code;
		return repository.findOne(code.value.eq(value).and(
				code.parent.value.eq(parent)));
	}
	
	@Override
	public Map<String, String> getOptionValue(String value) {
		QCode code = QCode.code;
		JPQLQuery query = new JPAQuery(em).from(code)
				.where(code.parent.value.eq(value), code.enabled.eq(true))
				.orderBy(code.rank.asc(), code.value.asc());
		return query.map(code.value, code.name);
	}

	@Override
	public Iterable<Code> findAll(Predicate predicate,
			OrderSpecifier<?> orderSpecifier) {
		return repository.findAll(predicate, orderSpecifier);
	}
}
