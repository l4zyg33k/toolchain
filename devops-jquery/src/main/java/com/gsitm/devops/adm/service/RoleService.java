package com.gsitm.devops.adm.service;

import java.util.List;
import java.util.Map;

import com.gsitm.devops.adm.model.Role;
import com.gsitm.devops.cmm.service.SharedService;

public interface RoleService extends SharedService<Role, String> {

	public Map<String, String> getAuthorities();

	public List<Role> findAuthorityIn(List<String> roles);
}
