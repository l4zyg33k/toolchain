package com.gsitm.devops.adm.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gsitm.devops.adm.service.biz.CodeBiz;

@Controller
public class CodeMngController {

	@Autowired
	private CodeBiz biz;
	
	@ModelAttribute("menuCode")
	public String menuCode() {
		return "CodeMng";
	}		

	@RequestMapping(value = "/adm/codemng", method = RequestMethod.GET)
	public String index() {
		return "adm/codemng/index";
	}

	@RequestMapping(value = "/ajax/select/code/{code}", method = RequestMethod.GET)
	public String ajaxSelect(Model model, @PathVariable String code) {
		Map<Long, String> options = biz.getOptions(code);
		model.addAttribute("options", options);
		return "ajax/select";
	}
}
