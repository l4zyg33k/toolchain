package com.gsitm.devops.adm.service;

import java.util.Map;

import com.gsitm.devops.adm.model.Code;
import com.gsitm.devops.cmm.service.SharedService;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public interface CodeService extends SharedService<Code, Long> {

	public String getJqGridSelectValue(String code);

	public String getJqGridSelectValue(Predicate predicate);

	public Map<Long, String> getOptions(String value);
	
	public Code getCode(String value, String parent);
	
	public Map<String, String> getOptionValue(String value);	

	public Iterable<Code> findAll(Predicate predicate,
			OrderSpecifier<?> orderSpecifier);
}
