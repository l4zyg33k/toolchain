package com.gsitm.devops.adm.service.biz;

import java.util.List;

import org.springframework.context.MessageSourceAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gsitm.devops.adm.model.vo.AcntItemVO;
import com.gsitm.devops.adm.model.vo.AcntMngFrmVO;
import com.gsitm.devops.adm.model.vo.AcntMngGrdVO;
import com.gsitm.devops.cmm.model.Searchable;

public interface AcntBiz extends MessageSourceAware {

	public String delete(AcntMngFrmVO vo);

	public String create(AcntMngFrmVO vo);

	public String update(AcntMngFrmVO vo);

	public Page<AcntMngGrdVO> findAll(Pageable pageable);

	public Page<AcntMngGrdVO> findAll(Pageable pageable, Searchable searchable);

	public List<AcntItemVO> findAll(String term);

	public List<String> getUserRoles(String username);

	public String getPositions();

	public String getTeams();

	public String getAuthorities();
	
	public List<AcntItemVO> findAllData(String term);
}
