package com.gsitm.devops.adm.model.vo;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

import com.mysema.query.annotations.QueryProjection;

@Getter
@Setter
public class RoleMngGrdVO {

	private String oper;

	private String id;

	@NotEmpty
	private String authority;

	@NotEmpty
	private String name;

	public RoleMngGrdVO() {
		super();
	}

	@QueryProjection
	public RoleMngGrdVO(String authority, String name) {
		super();
		this.authority = authority;
		this.name = name;
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
