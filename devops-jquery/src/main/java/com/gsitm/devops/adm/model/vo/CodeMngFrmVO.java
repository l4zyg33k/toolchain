package com.gsitm.devops.adm.model.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class CodeMngFrmVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String oper;

	private String id;

	@NotEmpty
	private String value;

	@NotEmpty
	private String name;

	private Integer rank = 0;

	private Boolean enabled = true;

	private String description;

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
