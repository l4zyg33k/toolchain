package com.gsitm.devops.adm.controller.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.vo.CodeMngFrmVO;
import com.gsitm.devops.adm.model.vo.CodeMngGrdVO;
import com.gsitm.devops.adm.service.biz.CodeBiz;
import com.gsitm.devops.cmm.model.JqFailure;
import com.gsitm.devops.cmm.model.JqResult;
import com.gsitm.devops.cmm.model.JqSuccess;
import com.gsitm.devops.cmm.model.Searchable;

@RestController
public class CodeMngRestController {

	@Autowired
	private CodeBiz biz;

	@RequestMapping(value = "/rest/adm/codemng", method = RequestMethod.POST)
	public Page<CodeMngGrdVO> getParents(Pageable pageable) {
		return biz.getParents(pageable);
	}

	@RequestMapping(value = "/rest/adm/codemng", method = RequestMethod.POST, params = { "_search=true" })
	public Page<CodeMngGrdVO> getParents(Pageable pageable,
			Searchable searchable) {
		return biz.getParents(pageable, searchable);
	}

	@RequestMapping(value = "/rest/adm/codemng/{id}/children", method = RequestMethod.POST)
	public Page<CodeMngGrdVO> getChildren(@PathVariable("id") Long id,
			Pageable pageable) {
		return biz.getChildren(pageable, id);
	}

	@RequestMapping(value = "/rest/adm/codemng/{id}/children", method = RequestMethod.POST, params = { "_search=true" })
	public Page<CodeMngGrdVO> getChildren(@PathVariable("id") Long id,
			Pageable pageable, Searchable searchable) {
		return biz.getChildren(pageable, searchable, id);
	}

	@RequestMapping(value = "/rest/adm/codemng/edit", method = RequestMethod.POST)
	public JqResult formEditingParent(@Valid CodeMngFrmVO codeMngFrmVO,
			BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (codeMngFrmVO.isDelete() == false && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (codeMngFrmVO.getOper()) {
		case "add":
			biz.create(codeMngFrmVO);
			break;
		case "edit":
			biz.update(codeMngFrmVO);
			break;
		case "del":
			biz.delete(codeMngFrmVO);
			break;
		}

		return new JqSuccess();
	}

	@RequestMapping(value = "/rest/adm/codemng/{parent}/children/edit", method = RequestMethod.POST)
	public JqResult formEditingChild(@PathVariable("parent") Long parent,
			@Valid CodeMngFrmVO codeMngFrmVO, BindingResult result) {

		// 삭제인 경우 유효성 검사를 하지 않는다.
		if (codeMngFrmVO.isDelete() == false
				&& !codeMngFrmVO.getOper().equals("add") && result.hasErrors()) {
			String message = result.getAllErrors().iterator().next()
					.getDefaultMessage();
			return new JqFailure(message);
		}

		switch (codeMngFrmVO.getOper()) {
		case "add":
			biz.create(codeMngFrmVO, parent);
			break;
		case "edit":
			biz.update(codeMngFrmVO);
			break;
		case "del":
			biz.delete(codeMngFrmVO);
			break;
		}

		return new JqSuccess();
	}
}
