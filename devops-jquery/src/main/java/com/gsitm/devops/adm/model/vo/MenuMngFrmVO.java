package com.gsitm.devops.adm.model.vo;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.gsitm.devops.adm.model.Role;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuMngFrmVO {

	private String oper;

	private Long id;

	@NotBlank
	private String code;

	@NotBlank
	private String name;

	private String href;

	private Integer rank;

	private Boolean enabled;

	private List<Role> authorities;

	public MenuMngFrmVO() {
		super();
	}
}
