package com.gsitm.devops.cmm.util;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.gsitm.devops.cmm.model.Searchable;

public class JqGridSearchHandlerMethodArgumentResolver implements
		HandlerMethodArgumentResolver {

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		String searchField = webRequest.getParameter("searchField");
		String searchOper = webRequest.getParameter("searchOper");
		String searchString = webRequest.getParameter("searchString");
		return new Searchable(searchField, searchOper, searchString);
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return Searchable.class.equals(parameter.getParameterType());
	}
}
