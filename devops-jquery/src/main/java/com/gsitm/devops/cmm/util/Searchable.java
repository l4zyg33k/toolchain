package com.gsitm.devops.cmm.util;

@Deprecated
public interface Searchable {

	public Boolean getSearch();
	public String getField();
	public String getValue();
	public String getOper();
}
