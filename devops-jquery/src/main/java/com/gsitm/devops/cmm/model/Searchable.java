package com.gsitm.devops.cmm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Searchable {

	private String searchField;

	private String searchOper;

	private String searchString;

	public Searchable(String searchField, String searchOper,
			String searchString) {
		super();
		this.searchField = searchField;
		this.searchOper = searchOper;
		this.searchString = searchString;
	}
}