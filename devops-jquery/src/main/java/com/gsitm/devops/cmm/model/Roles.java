package com.gsitm.devops.cmm.model;

public final class Roles {
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	public static final String ROLE_DEV = "ROLE_DEV";
	public static final String ROLE_IL = "ROLE_IL";
	public static final String ROLE_MANAGER = "ROLE_MANAGER";
	public static final String ROLE_OP = "ROLE_OP";
	public static final String ROLE_QA = "ROLE_QA";
	public static final String ROLE_USER = "ROLE_USER";
}
