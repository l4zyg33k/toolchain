package com.gsitm.devops.cmm.service;

import com.gsitm.devops.adm.service.AccountService;
import com.gsitm.devops.adm.service.AttachmentFileService;
import com.gsitm.devops.adm.service.CodeService;
import com.gsitm.devops.adm.service.MenuService;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmu.service.CoWorkerService;
import com.gsitm.devops.cmu.service.IssueDetailService;
import com.gsitm.devops.cmu.service.IssueService;
import com.gsitm.devops.cmu.service.ProjectWeeklyReportService;
import com.gsitm.devops.cmu.service.RiskDetailService;
import com.gsitm.devops.cmu.service.RiskService;
import com.gsitm.devops.dms.service.DocumentCategoryService;
import com.gsitm.devops.dms.service.DocumentRevisionService;
import com.gsitm.devops.dms.service.DocumentService;
import com.gsitm.devops.dms.service.SystemService;
import com.gsitm.devops.pms.dto.TakingOverTemp;
import com.gsitm.devops.pms.service.AgendaService;
import com.gsitm.devops.pms.service.CommitteeService;
import com.gsitm.devops.pms.service.EffortPlanService;
import com.gsitm.devops.pms.service.EffortResultService;
import com.gsitm.devops.pms.service.ProjectInspectionDetailService;
import com.gsitm.devops.pms.service.ProjectInspectionService;
import com.gsitm.devops.pms.service.ProjectMemberService;
import com.gsitm.devops.pms.service.ProjectService;
import com.gsitm.devops.pms.service.ProjectSizeDetailService;
import com.gsitm.devops.pms.service.ProjectSizeService;
import com.gsitm.devops.pms.service.TakingOverDetailService;
import com.gsitm.devops.pms.service.TakingOverService;
import com.gsitm.devops.qam.service.DocumentEvaluationService;
import com.gsitm.devops.qam.service.ProgressEvaluationService;
import com.gsitm.devops.qam.service.QualityCheckResultDetailService;
import com.gsitm.devops.qam.service.QualityCheckResultService;
import com.gsitm.devops.qam.service.SatisfactionSurveyRecipientService;
import com.gsitm.devops.qam.service.SatisfactionSurveyQuestionService;
import com.gsitm.devops.qam.service.SatisfactionSurveyResultService;
import com.gsitm.devops.qam.service.SatisfactionSurveySummaryService;
import com.gsitm.devops.slm.service.ArgMngService;
import com.gsitm.devops.slm.service.CsrFpResultDetailService;
import com.gsitm.devops.slm.service.CsrFpResultService;
import com.gsitm.devops.slm.service.LvlMngService;
import com.gsitm.devops.slm.service.ProductivityService;
import com.gsitm.devops.slm.service.ProgramFpResultDetailService;
import com.gsitm.devops.slm.service.ProgramFpResultService;
import com.gsitm.devops.slm.service.ProjectFpResultService;

/**
 * 서비스 로케이터
 */
@Deprecated
public interface JpaServiceFactory {

	public AccountService getAccountService();

	public AttachmentFileService getAttachmentFileService();

	public RoleService getAuthorityService();

	public CodeService getCodeService();

	public MenuService getMenuService();

	public MailService getMailService();

	public DocumentCategoryService getDocumentCategoryService();

	public DocumentRevisionService getDocumentRevisionService();

	public DocumentService getDocumentService();

	public SystemService getSystemService();

	public AgendaService getAgendaService();

	public CommitteeService getCommitteeService();

	public EffortPlanService getEffortPlanService();

	public EffortResultService getEffortResultService();

	public IssueDetailService getIssueDetailService();

	public IssueService getIssueService();

	public ProgressEvaluationService getProgressEvaluationService();

	public ProjectInspectionDetailService getProjectInspectionDetailService();

	public ProjectInspectionService getProjectInspectionService();

	public ProjectMemberService getProjectMemberService();

	public ProjectService getProjectService();

	public ProjectSizeService getProjectSizeService();

	public ProjectWeeklyReportService getWeeklyReportService();

	public QualityCheckResultDetailService getQualityCheckResultDetailService();

	public QualityCheckResultService getQualityCheckResultService();

	public DocumentEvaluationService getDocumentEvaluationService();

	public SatisfactionSurveyQuestionService getSatisfactionSurveyQuestionService();

	public SatisfactionSurveyRecipientService getSatisfactionSurveyMemberService();

	public SatisfactionSurveyResultService getSatisfactionSurveyResultService();

	public RiskDetailService getRiskDetailService();

	public RiskService getRiskService();

	public SatisfactionSurveySummaryService getSatisfactionSurveySummaryService();

	public TakingOverService getTakingOverService();

	public TakingOverDetailService getTakingOverDetailService();

	public PassportService getPassportService();

	public TakingOverTemp getTakingOverTemp();

	public CoWorkerService getCooperationCompanyPersonService();

	public ArgMngService getCsrAdjustArgumentService();

	public LvlMngService getCsrLevelService();

	public CsrFpResultService getCsrFpResultService();

	public CsrFpResultDetailService getCsrFpResultDetailService();

	public ProductivityService getProductivityService();

	public ProjectFpResultService getProjectFpResultService();

	public ProgramFpResultDetailService getProgramFpResultDetailService();

	public ProgramFpResultService getProgramFpResultService();

	public ProjectSizeDetailService getProjectSizeDetailService();
}
