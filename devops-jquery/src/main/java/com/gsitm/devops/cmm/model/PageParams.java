package com.gsitm.devops.cmm.model;

public final class PageParams {

	public static final String MENU_CODE = "menuCode";
	public static final String NAV_GRID = "navGrid";
	public static final String ID = "id";
	public static final String PAGE = "page";
	public static final String ROW_NUM = "rowNum";
	public static final String ROW_ID = "rowId";
}
