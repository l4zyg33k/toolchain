package com.gsitm.devops.cmm.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MultiExcelDownloadView extends AbstractPOIExcelView {

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		@SuppressWarnings("unchecked")
		List<Map<String, String>> data = (List<Map<String, String>>) model
				.get("data");

		for (Map<String, String> dataSheet : data) {

			List<Map<String, String>> grid = new ArrayList<>();
			grid = objectMapper.readValue(dataSheet.get("data"),
					new TypeReference<List<Map<String, String>>>() {
					});

			// 데이터가 없는 경우 시트 만들지 않음
			if (grid.size() > 0) {
				Sheet sheet = workbook.createSheet(dataSheet.get("name"));

				Row HeaderRow = sheet.createRow(0);
				Font f = workbook.createFont();
				f.setBoldweight(Font.BOLDWEIGHT_BOLD);
				CellStyle cs = workbook.createCellStyle();
				cs.setFont(f);

				int cellNum = 0;
				for (String key : grid.get(0).keySet()) {
					Cell cell = HeaderRow.createCell(cellNum++);
					cell.setCellValue(key);
					cell.setCellStyle(cs);
				}
				int rowNum = 1;

				for (Map<String, String> map : grid) {
					Row detailRow = sheet.createRow(rowNum++);
					cellNum = 0;
					for (String value : map.values()) {
						Cell cell = detailRow.createCell(cellNum++);
						cell.setCellValue(value);
					}
				}
			}

		}
		System.out.println("end11");
		String userAgent = request.getHeader("User-Agent");

		boolean ie = userAgent.indexOf("MSIE") > -1;

		String modelFileName = (String) model.get("filename");
		String fileName = null;

		if (ie) {

			fileName = URLEncoder.encode(modelFileName, "UTF-8");

		} else {

			fileName = new String(modelFileName.getBytes("UTF-8"), "ISO-8859-1");

		}// end if;
		System.out.println("end12");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ fileName + "\";");
	}

	@Override
	protected Workbook createWorkbook() {
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		return workbook;
	}

}
