package com.gsitm.devops.cmm.util;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;


public class ExcelDownloadView extends AbstractPOIExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		@SuppressWarnings("unchecked")
		List<Map<String, String>> data = (List<Map<String, String>>) model
				.get("data");
		// Code version = (Code) model.get("version");

		Sheet sheet = workbook.createSheet("data");

		Row HeaderRow = sheet.createRow(0);
		Font f = workbook.createFont();
		f.setBoldweight(Font.BOLDWEIGHT_BOLD);
		CellStyle cs = workbook.createCellStyle();
		cs.setFont(f);

		int cellNum = 0;

		for (String key : data.get(0).keySet()) {
			Cell cell = HeaderRow.createCell(cellNum++);
			cell.setCellValue(key);
			cell.setCellStyle(cs);
		}

		int rowNum = 1;

		for (Map<String, String> map : data) {
			Row detailRow = sheet.createRow(rowNum++);
			cellNum = 0;
			for (String value : map.values()) {
				Cell cell = detailRow.createCell(cellNum++);
				cell.setCellValue(value);
			}
		}

		String userAgent = request.getHeader("User-Agent");

		boolean ie = userAgent.indexOf("MSIE") > -1;

		String modelFileName = (String) model.get("filename");
		String fileName = null;

		if (ie) {

			fileName = URLEncoder.encode(modelFileName, "UTF-8");

		} else {

			fileName = new String(modelFileName.getBytes("UTF-8"), "ISO-8859-1");

		}// end if;

		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ fileName + "\";");
	}

	@Override
	protected Workbook createWorkbook() {
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		return workbook;
	}

}
