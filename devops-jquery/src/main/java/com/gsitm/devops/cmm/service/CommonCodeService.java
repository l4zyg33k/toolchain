package com.gsitm.devops.cmm.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.gsitm.devops.adm.service.CodeService;

@Service
public class CommonCodeService {

	//공통코드 처리
	@Autowired
	private CodeService codeService;	

	//조직
	public Map<Long, String> getOrganization() {
		return codeService.getOptions("S0");
	}
	
	//파트
	public Map<Long, String> getPart() {
		return codeService.getOptions("G4");
	}	
	
	//직급
	public Map<Long, String> getPosition() {
		return codeService.getOptions("A1");
	}
	public String getPositionString() {
		return getValueLong(codeService.getOptions("A1"));
	}	
	
	//상태
	public Map<Long, String> getStatus() {
		return codeService.getOptions("S5");
	}
	
	//상주여부
	public Map<Long, String> getIndoor() {
		return codeService.getOptions("PMS10");
	}
	
	//PC제공
	public Map<Long, String> getComputer() {
		return codeService.getOptions("PMS10");
	}
	
	//LG CNS 보안각서 적용여부
	public Map<Long, String> getMemorandum() {
		return codeService.getOptions("PMS10");
	}
	
	//팀
	public Map<Long, String> getTeams() {
		return codeService.getOptions("A2");
	}	
	public String getTeamsString() {
		return getValueLong(codeService.getOptions("A2"));
	}
	
	//역할 - 프로젝트 
	public Map<Long, String> getRoles() {
		return codeService.getOptions("Q0");
	}	
	public String getRolesString() {
		return getValueLong(codeService.getOptions("Q0"));
	}	
	
	//진행단계
	public Map<Long, String> getPhases() {
		return codeService.getOptions("Q1");
	}	
	public String getPhasesString() {
		return getValueLong(codeService.getOptions("Q1"));
	}	
	
	//프로젝트 진행단계
	public Map<Long, String> getProjectPhases() {
		return codeService.getOptions("PMS20");
	}	
	public String getProjectPhasesString() {
		return getValueLong(codeService.getOptions("PMS20"));
	}
	
	//프로젝트 단계
	public Map<String, String> getProjectSteps() {
		return codeService.getOptionValue("G6");
	}	
	public String getProjectStepsString() {
		return getValueString(codeService.getOptionValue("G6"));
	}	
	
	//설문대상자(T3.사업부, T4.현업사용자, T5.운영자)
	public Map<String, String> getRecipients() {
		return codeService.getOptionValue("T1");
	}	
	public String getRecipientsString() {
		return getValueString(codeService.getOptionValue("T1"));
	}
	public Map<Long, String> getRecipient() {
		return codeService.getOptions("T1");
	}	
	public String getRecipientsLong() {
		return getValueLong(codeService.getOptions("T1"));
	}
	
	//품질검토종류(SQA검사, 단계검토)
	public Map<Long, String> getExamineKinds() {
		return codeService.getOptions("G8");
	}	
	public String getExamineKindsString() {
		return getValueLong(codeService.getOptions("G8"));
	}	
	
	//프로젝트상태
	public Map<Long, String> getProjectStatus() {
		return codeService.getOptions("F6");
	}	
	public String getProjectStatusString() {
		return getValueLong(codeService.getOptions("F6"));
	}
	
	//결과유형(RE 필수, OP 권고)
	public Map<Long, String> getResultTypes() {
		return codeService.getOptions("G7");
	}	
	public String getResultTypesString() {
		return getValueLong(codeService.getOptions("G7"));
	}	
	
	//사업부 문항유형
	public Map<Long, String> getOperationDivisions() {
		return codeService.getOptions("T3");
	}	
	public String getOperationDivisionsLong() {
		return getValueLong(codeService.getOptions("T3"));
	}
	
	//협업사용자
	public Map<Long, String> getActualWork() {
		return codeService.getOptions("T4");
	}	
	public String getActualWorkLong() {
		return getValueLong(codeService.getOptions("T4"));
	}	
	
	//운영자
	public Map<Long, String> getOperator() {
		return codeService.getOptions("T5");
	}	
	public String getOperatorLong() {
		return getValueLong(codeService.getOptions("T5"));
	}	
	
	//String으로 변환 처리
	private String getValueLong(Map<Long, String> results) {
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}
	private String getValueString(Map<String, String> results) {
		return Joiner.on(";").withKeyValueSeparator(":").join(results);
	}
}
