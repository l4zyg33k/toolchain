package com.gsitm.devops.cmm.util;

import javax.servlet.ServletRequest;

import org.springframework.beans.PropertyValues;
import org.springframework.core.MethodParameter;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestParameterPropertyValues;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

@Deprecated
public class SearchableArgumentResolver implements WebArgumentResolver {

	@Override
	public Object resolveArgument(MethodParameter methodParameter,
			NativeWebRequest nativeWebRequest) throws Exception {

		if (methodParameter.getParameterType().equals(Searchable.class)) {
			Searchable request = new SearchRequest();
			ServletRequest servletRequest = (ServletRequest) nativeWebRequest
					.getNativeRequest();
			PropertyValues propertyValues = new ServletRequestParameterPropertyValues(
					servletRequest, "search", ".");
			DataBinder binder = new ServletRequestDataBinder(request);
			binder.initDirectFieldAccess();
			binder.bind(propertyValues);
			return request;
		}

		return UNRESOLVED;
	}
}
