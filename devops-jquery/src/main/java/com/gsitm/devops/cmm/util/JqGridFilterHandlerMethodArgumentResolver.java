package com.gsitm.devops.cmm.util;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.cmm.model.JqFilter;

public class JqGridFilterHandlerMethodArgumentResolver implements
		HandlerMethodArgumentResolver {

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		String filters = webRequest.getParameter("filters");
		if (filters != null) {
			ObjectMapper mapper = new ObjectMapper();

			try {
				return mapper.readValue(filters, JqFilter.class);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		return new JqFilter();
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return JqFilter.class.equals(parameter.getParameterType());
	}
}
