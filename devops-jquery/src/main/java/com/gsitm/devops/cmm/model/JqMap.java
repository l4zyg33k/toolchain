package com.gsitm.devops.cmm.model;

import java.io.Serializable;

import com.mysema.query.annotations.QueryProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JqMap implements Serializable {

	private static final long serialVersionUID = 1L;

	private String key;

	private String value;

	@QueryProjection
	public JqMap(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
}
