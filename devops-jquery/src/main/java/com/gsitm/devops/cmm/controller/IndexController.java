package com.gsitm.devops.cmm.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.AttachmentFile;
import com.gsitm.devops.adm.model.QCode;
import com.gsitm.devops.adm.service.CodeService;

@Controller
public class IndexController {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CodeService codeService;

	@Autowired
	private UserDetailsService udService;

	@Autowired
	private DaoAuthenticationProvider daoAuthenticationProvider;

	@ModelAttribute("menuCode")
	public String menuCode() {
		return "Home";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {

		return "home";
	}

	@RequestMapping(value = "/sso/{username}", method = RequestMethod.GET)
	public String sso(@PathVariable String username, HttpServletRequest request) {
		try {
			UserDetails details = udService.loadUserByUsername(username);
			Authentication authentication = new UsernamePasswordAuthenticationToken(
					details.getUsername(), details.getPassword(),
					details.getAuthorities());
			authentication = daoAuthenticationProvider
					.authenticate(authentication);
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (UsernameNotFoundException e) {
			SecurityContextHolder.getContext().setAuthentication(null);
		}

		request.getSession()
				.setAttribute(
						HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
						SecurityContextHolder.getContext());

		return "redirect:/";
	}

	@RequestMapping(value = "/sso/{username}/{forUrl}", method = RequestMethod.GET)
	public String sso(@PathVariable("username") String username,
			@PathVariable("forUrl") String forUrl, HttpServletRequest request) {
		try {
			UserDetails details = udService.loadUserByUsername(new String(Base64.getDecoder().decode(username),"UTF-8"));
			Authentication authentication = new UsernamePasswordAuthenticationToken(
					details.getUsername(), details.getPassword(),
					details.getAuthorities());
			authentication = daoAuthenticationProvider
					.authenticate(authentication);
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (UsernameNotFoundException e) {
			SecurityContextHolder.getContext().setAuthentication(null);
		} catch (UnsupportedEncodingException e) {
			SecurityContextHolder.getContext().setAuthentication(null);
		}

		request.getSession()
				.setAttribute(
						HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
						SecurityContextHolder.getContext());

		String decodedUrl;
		try {
			decodedUrl = new String(Base64.getDecoder().decode(forUrl),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			decodedUrl = "/";
		}
		
		return "redirect:" + decodedUrl;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET, params = "error")
	public String failed(Model model, HttpSession session) {

		AuthenticationException ae = (AuthenticationException) session
				.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");

		Map<String, String> message = new HashMap<>();
		message.put("alert", ae.getLocalizedMessage());

		model.addAttribute("message", message);

		return "login";
	}

	@RequestMapping(value = "/error/accessDenied", method = RequestMethod.GET)
	public String accessDenied(Model model) {

		throw new AccessDeniedException("해당 페이지의 접근 권한이 없습니다.");
	}

	@RequestMapping(value = "/rest/dummy", method = RequestMethod.POST, params = { "oper" })
	@ResponseBody
	public String dummy() {

		return "";
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public String download(Model model, @RequestParam("path") String path,
			@RequestParam("fileName") String fileName)
			throws UnsupportedEncodingException {
		String rootPath = codeService
				.findOne(
						QCode.code.value.eq("U1").and(
								QCode.code.parent.value.eq("U1"))).getName();
		File file = Paths.get(rootPath, path,
				new String(fileName.getBytes(), "UTF-8")).toFile();

		model.addAttribute("downloadFile", file);

		return "download";
	}

	@RequestMapping(value = "/attachmentFile", method = RequestMethod.GET)
	public String download(Model model,
			@RequestParam("id") AttachmentFile attachmentFile) {

		File file = new File(attachmentFile.getDiskDirectory()
				+ attachmentFile.getFileName());

		model.addAttribute("downloadFile", file);

		return "download";
	}

	@RequestMapping(value = "/excelDownload", method = RequestMethod.POST)
	public String excelDownload(Model model, @RequestParam("data") String data,
			@RequestParam("filename") String filename) throws IOException {

		List<Map<String, String>> rows = new ArrayList<>();

		rows = objectMapper.readValue(data,
				new TypeReference<List<Map<String, String>>>() {
				});

		filename = getFileNameDateTime(filename);

		model.addAttribute("data", rows);
		model.addAttribute("filename", filename);

		return "excelDownload";
	}

	@RequestMapping(value = "/multiExcelDownload", method = RequestMethod.POST)
	public String multiExcelDownload(Model model,
			@RequestParam("data") String data,
			@RequestParam("filename") String filename) throws IOException {

		List<Map<String, String>> tabs = new ArrayList<>();

		tabs = objectMapper.readValue(data,
				new TypeReference<List<Map<String, String>>>() {
				});

		filename = getFileNameDateTime(filename);

		model.addAttribute("data", tabs);
		model.addAttribute("filename", filename);

		return "multiExcelDownload";
	}

	private String getFileNameDateTime(String filename) {
		// 파일명에 년월일 시분초 붙이기 (GMT기준)
		int index = filename.lastIndexOf(".");
		if (index != -1) {
			String name = filename.substring(0, index);
			String ext = filename.substring(index);

			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
			df.setTimeZone(tz);

			filename = name + "_" + df.format(date) + ext;
		}
		return filename;
	}
}
