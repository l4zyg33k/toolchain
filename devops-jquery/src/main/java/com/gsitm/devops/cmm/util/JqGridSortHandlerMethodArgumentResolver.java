package com.gsitm.devops.cmm.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.google.common.collect.Lists;

public class JqGridSortHandlerMethodArgumentResolver extends
		SortHandlerMethodArgumentResolver {

	@Override
	public Sort resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) {
		String sidx = webRequest.getParameter("sidx");
		String sord = webRequest.getParameter("sord");
		sidx = StringUtils.trimTrailingWhitespace(sidx);
		String params = sidx.concat(" " + sord);
		return parseParameterIntoSort(params);
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return Sort.class.equals(parameter.getParameterType());
	}

	protected Sort parseParameterIntoSort(String params) {
		List<Order> allOrders = new ArrayList<Sort.Order>();
		for (String order : StringUtils.commaDelimitedListToSet(params)) {
			order = StringUtils.trimLeadingWhitespace(order);
			String[] sort = order.split(" ");
			if (sort.length == 2) {
				allOrders.add(new Order(Direction.fromStringOrNull(sort[1]),
						sort[0]));
			}
		}
		return allOrders.isEmpty() ? null : new Sort(Lists.reverse(allOrders));
	}
}
