package com.gsitm.devops.cmm.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gsitm.devops.adm.model.Account;
import com.gsitm.devops.adm.service.RoleService;
import com.gsitm.devops.cmm.model.Roles;
import com.gsitm.devops.dms.model.System;

@Service
public class PassportService {

	@Autowired
	RoleService authorityService;

	/*
	 * String intent R: Read W: Create, Checkout M: Checkin, Delete Reqest (관리자
	 * 또는 개정자 본인에게만 권한이 있음) X: Approve, Delete, Reject
	 */
	public Boolean passport(String intent, System system, Account requestor, Account owner) {

		if (requestor.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			return true;
		}

		switch (intent) {
		case "R":
			if (system.getSysops().contains(requestor) || system.getApprobators().contains(requestor)
					|| system.getAuditors().contains(requestor)
					|| requestor.getAuthorities().contains(authorityService.findOne(Roles.ROLE_IL))) {
				return true;
			} else {
				return false;
			}
		case "W":
			if (system.getSysops().contains(requestor) || system.getApprobators().contains(requestor)) {
				return true;
			} else {
				return false;
			}
		case "M":
			if ((system.getSysops().contains(requestor) || system.getApprobators().contains(requestor))
					&& owner.equals(requestor)) {
				return true;
			} else {
				return false;
			}
		case "X":
			if (system.getApprobators().contains(requestor)) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	// 사용자역할과 문서상태에 따른 버튼과 항목표시속성을 위한 메서드
	public Map<String, Boolean> settingByAccountAndStatus(String state, System system, Account requestor, Account owner) {
		/*
		 * 권한체크(ROLE) 관리자(ROLE_ADMIN): 모든권한 정보운영팀(ROLE_OP): (자신이 운영하는 시스템에
		 * 한하여)등록·체크아웃·체크인(검토요청)·승인·삭제요청·임시저장 품질파트, 정보기술리더, 팀장: 열람가능? 권한에 따른 플래그
		 * A: 관리자, B: 정보운영팀, C: 품질파트, 정보기술리더, 팀장, D: 그 외(열람권한 없음)
		 */

		Map<String, Boolean> buttons = new HashMap<>();

		buttons.put("btn_create", false);
		buttons.put("btn_save", false);
		buttons.put("btn_checkout", false);
		buttons.put("btn_cancel", false);
		buttons.put("btn_checkin", false);
		buttons.put("btn_reject", false);
		buttons.put("btn_approve", false);
		buttons.put("btn_deleterequest", false);
		buttons.put("btn_delete", false);

		if (requestor.getAuthorities().contains(authorityService.findOne(Roles.ROLE_ADMIN))) {
			switch (state) {
			case "":
				buttons.put("btn_create", true);
				break;
			case "CI":
				buttons.put("btn_checkout", true);
				buttons.put("btn_deleterequest", true);
				break;
			case "CO":
				buttons.put("btn_save", true);
				buttons.put("btn_cancel", true);
				buttons.put("btn_checkin", true);
				break;
			case "EX":
				buttons.put("btn_reject", true);
				buttons.put("btn_approve", true);
				break;
			case "RE":
				buttons.put("btn_checkout", true);
				break;
			case "DE":
				buttons.put("btn_reject", true);
				buttons.put("btn_delete", true);
				break;
			}

			return buttons;
		} else if (system != null && system.getId() != null && system.getSysops().contains(requestor)
				&& !system.getApprobators().contains(requestor)) {
			switch (state) {
			case "":
				buttons.put("btn_create", true);
				break;
			case "CI":
				buttons.put("btn_checkout", true);
				buttons.put("btn_deleterequest", true);
				break;
			case "CO":
				// 본인이 소유한(체크아웃한) 문서만 임시저장/체크아웃취소/체크인요청 이 가능함.
				if (requestor.equals(owner)) {
					buttons.put("btn_save", true);
					buttons.put("btn_cancel", true);
					buttons.put("btn_checkin", true);
				}
				break;
			case "EX":
				break;
			case "RE":
				buttons.put("btn_checkout", true);
				break;
			case "DE":
				break;
			}
		} else if (system != null && system.getId() != null && !system.getSysops().contains(requestor)
				&& system.getApprobators().contains(requestor)) {
			switch (state) {
			case "":
				buttons.put("btn_create", true);
				break;
			case "CI":
				buttons.put("btn_checkout", true);
				break;
			case "CO":
				// 본인이 소유한(체크아웃한) 문서만 임시저장/체크아웃취소/체크인요청 이 가능함.
				if (requestor.equals(owner)) {
					buttons.put("btn_save", true);
					buttons.put("btn_cancel", true);
					buttons.put("btn_checkin", true);
				}
				break;
			case "EX":
				buttons.put("btn_reject", true);
				buttons.put("btn_approve", true);
				break;
			case "RE":
				buttons.put("btn_checkout", true);
				break;
			case "DE":
				buttons.put("btn_reject", true);
				buttons.put("btn_delete", true);
				break;
			}
		} else if (system != null && system.getId() != null && system.getSysops().contains(requestor)
				&& system.getApprobators().contains(requestor)) {
			switch (state) {
			case "":
				buttons.put("btn_create", true);
				break;
			case "CI":
				buttons.put("btn_checkout", true);
				buttons.put("btn_deleterequest", true);
				break;
			case "CO":
				// 본인이 소유한(체크아웃한) 문서만 임시저장/체크아웃취소/체크인요청 이 가능함.
				if (requestor.equals(owner)) {
					buttons.put("btn_save", true);
					buttons.put("btn_cancel", true);
					buttons.put("btn_checkin", true);
				}
				break;
			case "EX":
				buttons.put("btn_reject", true);
				buttons.put("btn_approve", true);
				break;
			case "RE":
				buttons.put("btn_checkout", true);
				break;
			case "DE":
				buttons.put("btn_reject", true);
				buttons.put("btn_delete", true);
				break;
			}
		} else {
			buttons.put("btn_create", true);
		}

		return buttons;
	}

}
