package com.gsitm.devops.cmm.util;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.PathBuilderFactory;

@Deprecated
public class SearchableConverter<T> {

	private PathBuilder<T> pathBuilder;

	public SearchableConverter(Class<T> clazz) {
		this.pathBuilder = new PathBuilderFactory().create(clazz);
	}

	public Predicate toPredicate(Searchable searchable) {

		BooleanBuilder booleanBuilder = new BooleanBuilder();

		if (searchable.getSearch() == true) {
			String field = searchable.getField();
			String value = searchable.getValue();
			String oper = searchable.getOper();

			switch (oper) {
			case "eq":
				booleanBuilder = booleanBuilder.or(pathBuilder.getString(field)
						.eq(value));
				break;
			case "cn":
				booleanBuilder = booleanBuilder.or(pathBuilder.getString(field)
						.contains(value));
				break;
			}
		}

		return booleanBuilder.getValue();
	}
}
