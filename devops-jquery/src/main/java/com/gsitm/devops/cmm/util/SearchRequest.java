package com.gsitm.devops.cmm.util;

import java.io.Serializable;

@Deprecated
@SuppressWarnings("serial")
public class SearchRequest implements Searchable, Serializable {

	private Boolean search;
	private String field;
	private String value;
	private String oper;

	@Override
	public Boolean getSearch() {
		return search;
	}

	public void setSearch(Boolean search) {
		this.search = search;
	}

	@Override
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}
}
