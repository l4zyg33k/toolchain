package com.gsitm.devops.cmm.controller;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.devops.adm.model.QCode;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;

@RestController
public class CommonRestController {

	@PersistenceContext
	private EntityManager em;

	/**
	 * 요청한 코드를 JSP select 태그의 option 항목 반환
	 * 
	 * @param model
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/rest/cmm/selectoptions", method = RequestMethod.POST)
	public Map<String, String> getOptions(@RequestParam("value") String value) {
		QCode code = QCode.code;
		JPQLQuery query = new JPAQuery(em).from(code)
				.where(code.parent.value.eq(value), code.enabled.eq(true))
				.orderBy(code.rank.asc(), code.value.asc());
		return query.map(code.value, code.name);
	}
}
