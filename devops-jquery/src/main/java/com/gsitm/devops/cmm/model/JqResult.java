package com.gsitm.devops.cmm.model;

public interface JqResult {

	public Boolean getSuccess();

	public String getMessage();
}
