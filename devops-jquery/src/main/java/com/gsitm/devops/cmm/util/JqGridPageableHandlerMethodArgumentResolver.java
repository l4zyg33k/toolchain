package com.gsitm.devops.cmm.util;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

public class JqGridPageableHandlerMethodArgumentResolver extends
		PageableHandlerMethodArgumentResolver {

	public JqGridPageableHandlerMethodArgumentResolver(
			SortHandlerMethodArgumentResolver configureSortResolver) {
		super(configureSortResolver);
	}

	@Override
	public Pageable resolveArgument(MethodParameter methodParameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) {
		Pageable pageable = super.resolveArgument(methodParameter,
				mavContainer, webRequest, binderFactory);
		return new PageRequest(pageable.getPageNumber() - 1,
				pageable.getPageSize(), pageable.getSort());
	}
}
