package com.gsitm.devops.cmm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gsitm.devops.pms.model.Project;
import com.gsitm.devops.pms.model.QProject;
import com.gsitm.devops.pms.service.ProjectService;

@Controller
public class ProjectDashboardController {

	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "/dashboard/projects/{code}", method = RequestMethod.GET)
	public String index(Model model, @PathVariable(value = "code") String code) {

		model.addAttribute("code", code);

		return "dashboard/projects/index";
	}

	@RequestMapping(value = "/dashboard/projects/{code}", method = RequestMethod.POST)
	@ResponseBody
	public Page<Project> findAll(@PathVariable(value = "code") String code,
			Pageable pageable) {
		Page<Project> result;
		QProject proj = QProject.project;
		if (code.equalsIgnoreCase("P")) {
			result = projectService.findAll(proj.il.team.value
					.equalsIgnoreCase("P").and(proj.watching.isTrue()),
					pageable);
		} else {
			result = projectService.findAll(proj.il.team.value
					.notEqualsIgnoreCase("P").and(proj.watching.isTrue()),
					pageable);
		}
		return result;
	}
}
