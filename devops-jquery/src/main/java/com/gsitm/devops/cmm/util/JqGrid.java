package com.gsitm.devops.cmm.util;

import java.beans.PropertyDescriptor;

import lombok.Getter;
import lombok.Setter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;

import com.gsitm.devops.cmm.model.JqFilter;
import com.gsitm.devops.cmm.model.JqRule;
import com.gsitm.devops.cmm.model.Searchable;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.PathBuilder;

@Getter
@Setter
public class JqGrid {

	private PathBuilder<?> builder;

	public JqGrid(PathBuilder<?> builder) {
		this.builder = builder;
	}
	
	public Predicate applyPredicate(JqFilter filter) {
		BooleanBuilder predicate = new BooleanBuilder();
		
		if (filter == null || filter.getRules() == null || filter.getRules().size() < 1) {
			return predicate;
		}
		
		for (JqRule rule : filter.getRules()) {
			Searchable searchable = new Searchable(rule.getField(), rule.getOp(), rule.getData());
			
			switch (filter.getGroupOp()) {
			case "AND":
				predicate = predicate.and(this.applyPredicate(searchable));
				break;
			case "OR":
				predicate = predicate.or(this.applyPredicate(searchable));
				break;

			default:
				predicate = predicate.and(this.applyPredicate(searchable));
				break;
			}
			
		}
		return predicate;
	}

	public Predicate applyPredicate(Searchable searchable) {
		String searchField = searchable.getSearchField();
		String searchOper = searchable.getSearchOper();
		String searchString = searchable.getSearchString();
		
		if (searchOper == null)	{
			searchOper = "";
		}
		
		PropertyDescriptor property = BeanUtils.getPropertyDescriptor(
				builder.getType(), searchField);
		try {
			if (property == null) {
				return parseString(searchField, searchOper, searchString);
			} else {
				switch (property.getPropertyType().getName()) {
				case "java.lang.Integer":
					return parseInteger(searchField, searchOper, searchString);
				case "java.lang.Long":
					return parseLong(searchField, searchOper, searchString);
				case "org.joda.time.DateTime":
					return parseDateTime(searchField, searchOper, searchString);
				case "org.joda.time.LocalDate":
					return parseLocalDate(searchField, searchOper, searchString);
				default:
					return parseString(searchField, searchOper, searchString);
				}
			}
		} catch (IllegalArgumentException exception) {
			return new BooleanBuilder().getValue();
		}
	}

	public Predicate applyPredicate(Searchable searchable, String type) {
		String searchField = searchable.getSearchField();
		String searchOper = searchable.getSearchOper();
		String searchString = searchable.getSearchString();
		
		if (searchOper == null)	{
			searchOper = "";
		}
		
		try {
			switch (type) {
			case "Integer":
				return parseInteger(searchField, searchOper, searchString);
			case "Long":
				return parseLong(searchField, searchOper, searchString);
			case "DateTime":
				return parseDateTime(searchField, searchOper, searchString);
			case "LocalDate":
				return parseLocalDate(searchField, searchOper, searchString);
			default:
				return parseString(searchField, searchOper, searchString);
			}
		} catch (IllegalArgumentException exception) {
			return new BooleanBuilder().getValue();
		}
	}
	
	private Predicate parseInteger(String searchField, String searchOper,
			String searchString) {
		
		switch (searchOper) {
		case "eq":
			return builder.getNumber(searchField, Integer.class).eq(
					Integer.parseInt(searchString));
		case "ne":
			return builder.getNumber(searchField, Integer.class).ne(
					Integer.parseInt(searchString));
		case "lt":
			return builder.getNumber(searchField, Integer.class).lt(
					Integer.parseInt(searchString));
		case "le":
			return builder.getNumber(searchField, Integer.class).loe(
					Integer.parseInt(searchString));
		case "gt":
			return builder.getNumber(searchField, Integer.class).gt(
					Integer.parseInt(searchString));
		case "ge":
			return builder.getNumber(searchField, Integer.class).goe(
					Integer.parseInt(searchString));
		case "in":
			return builder.getNumber(searchField, Integer.class).in(
					Integer.parseInt(searchString));
		case "ni":
			return builder.getNumber(searchField, Integer.class).notIn(
					Integer.parseInt(searchString));
		case "nu":
			return builder.getNumber(searchField, Integer.class).isNull();
		case "nn":
			return builder.getNumber(searchField, Integer.class).isNotNull();
		default:
			return new BooleanBuilder().getValue();
		}
	}

	private Predicate parseLong(String searchField, String searchOper,
			String searchString) {
		switch (searchOper) {
		case "eq":
			return builder.getNumber(searchField, Long.class).eq(
					Long.parseLong(searchString));
		case "ne":
			return builder.getNumber(searchField, Long.class).ne(
					Long.parseLong(searchString));
		case "lt":
			return builder.getNumber(searchField, Long.class).lt(
					Long.parseLong(searchString));
		case "le":
			return builder.getNumber(searchField, Long.class).loe(
					Long.parseLong(searchString));
		case "gt":
			return builder.getNumber(searchField, Long.class).gt(
					Long.parseLong(searchString));
		case "ge":
			return builder.getNumber(searchField, Long.class).goe(
					Long.parseLong(searchString));
		case "in":
			return builder.getNumber(searchField, Long.class).in(
					Long.parseLong(searchString));
		case "ni":
			return builder.getNumber(searchField, Long.class).notIn(
					Long.parseLong(searchString));
		case "nu":
			return builder.getNumber(searchField, Long.class).isNull();
		case "nn":
			return builder.getNumber(searchField, Long.class).isNotNull();
		default:
			return new BooleanBuilder().getValue();
		}
	}

	private Predicate parseString(String searchField, String searchOper,
			String searchString) {
		switch (searchOper) {
		case "eq":
			return builder.getString(searchField).eq(searchString);
		case "ne":
			return builder.getString(searchField).ne(searchString);
		case "lt":
			return builder.getString(searchField).lt(searchString);
		case "le":
			return builder.getString(searchField).loe(searchString);
		case "gt":
			return builder.getString(searchField).gt(searchString);
		case "ge":
			return builder.getString(searchField).goe(searchString);
		case "bw":
			return builder.getString(searchField).startsWith(searchString);
		case "bn":
			return builder.getString(searchField).notLike(searchString + "%");
		case "in":
			return builder.getString(searchField).in(searchString);
		case "ni":
			return builder.getString(searchField).notIn(searchString);
		case "ew":
			return builder.getString(searchField).endsWith(searchString);
		case "en":
			return builder.getString(searchField).notLike("%" + searchString);
		case "cn":
			return builder.getString(searchField).contains(searchString);
		case "nc":
			return builder.getString(searchField).notLike(
					"%" + searchString + "%");
		case "nu":
			return builder.getString(searchField).isNull();
		case "nn":
			return builder.getString(searchField).isNotNull();
		default:
			return new BooleanBuilder().getValue();
		}
	}

	private Predicate parseDateTime(String searchField, String searchOper,
			String searchString) {
		switch (searchOper) {
		case "eq":
			return builder.getDateTime(searchField, DateTime.class).eq(
					new DateTime(searchString));
		case "ne":
			return builder.getDateTime(searchField, DateTime.class).ne(
					new DateTime(searchString));
		case "lt":
			return builder.getDateTime(searchField, DateTime.class).lt(
					new DateTime(searchString));
		case "le":
			return builder.getDateTime(searchField, DateTime.class).loe(
					new DateTime(searchString));
		case "gt":
			return builder.getDateTime(searchField, DateTime.class).gt(
					new DateTime(searchString));
		case "ge":
			return builder.getDateTime(searchField, DateTime.class).goe(
					new DateTime(searchString));
		case "in":
			return builder.getDateTime(searchField, DateTime.class).in(
					new DateTime(searchString));
		case "ni":
			return builder.getDateTime(searchField, DateTime.class).notIn(
					new DateTime(searchString));
		case "nu":
			return builder.getDateTime(searchField, DateTime.class).isNull();
		case "nn":
			return builder.getDateTime(searchField, DateTime.class).isNotNull();
		default:
			return new BooleanBuilder().getValue();
		}
	};

	private Predicate parseLocalDate(String searchField, String searchOper,
			String searchString) {
		switch (searchOper) {
		case "eq":
			return builder.getDate(searchField, LocalDate.class).eq(
					new LocalDate(searchString));
		case "ne":
			return builder.getDate(searchField, LocalDate.class).ne(
					new LocalDate(searchString));
		case "lt":
			return builder.getDate(searchField, LocalDate.class).lt(
					new LocalDate(searchString));
		case "le":
			return builder.getDate(searchField, LocalDate.class).loe(
					new LocalDate(searchString));
		case "gt":
			return builder.getDate(searchField, LocalDate.class).gt(
					new LocalDate(searchString));
		case "ge":
			return builder.getDate(searchField, LocalDate.class).goe(
					new LocalDate(searchString));
		case "in":
			return builder.getDate(searchField, LocalDate.class).in(
					new LocalDate(searchString));
		case "ni":
			return builder.getDate(searchField, LocalDate.class).notIn(
					new LocalDate(searchString));
		case "nu":
			return builder.getDate(searchField, LocalDate.class).isNull();
		case "nn":
			return builder.getDate(searchField, LocalDate.class).isNotNull();
		default:
			return new BooleanBuilder().getValue();
		}
	};
}
