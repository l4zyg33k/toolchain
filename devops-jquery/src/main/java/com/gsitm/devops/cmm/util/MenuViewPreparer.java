package com.gsitm.devops.cmm.util;

import java.util.List;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsitm.devops.adm.model.vo.MenuInfoVO;
import com.gsitm.devops.adm.model.vo.MenuItemVO;
import com.gsitm.devops.adm.service.biz.MenuBiz;

@Slf4j
public class MenuViewPreparer implements ViewPreparer {

	@Autowired
	private MenuBiz biz;

	@Autowired
	private ObjectMapper mapper;

	@Override
	public void execute(Request request, AttributeContext attributeContext)
			throws PreparerException {
		if (request.getContext("request").get("menuCode") == null) {
			request.getContext("request").put("menus", "");
			return;
		}
		String menuCode = request.getContext("request").get("menuCode")
				.toString();
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		if (authentication == null) {
			return;
		}
		Set<String> authorities = AuthorityUtils
				.authorityListToSet(authentication.getAuthorities());
		List<MenuInfoVO> info = biz.getMenusByAuthorities(authorities);
		List<MenuItemVO> item = biz.getMenuItems(menuCode, info);
		try {
			request.getContext("request").put("menus",
					mapper.writeValueAsString(item));
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
		}
	}
}
