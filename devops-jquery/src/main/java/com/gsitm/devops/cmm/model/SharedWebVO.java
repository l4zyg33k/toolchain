package com.gsitm.devops.cmm.model;

import java.io.Serializable;

import org.springframework.data.domain.Persistable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class SharedWebVO<T extends Serializable> implements Persistable<T> {

	protected String oper;

	protected int page;

	protected int rowNum;

	protected T rowId;

	protected T id;

	public SharedWebVO() {
		super();
	}

	public SharedWebVO(int page, int rowNum, T rowId) {
		super();
		this.page = page;
		this.rowNum = rowNum;
		this.rowId = rowId;
	}

	@Override
	public T getId() {
		return this.id;
	}

	@Override
	public boolean isNew() {
		return null == getId();
	}

	public boolean isDelete() {
		return this.oper != null && this.oper.equals("del");
	}
}
