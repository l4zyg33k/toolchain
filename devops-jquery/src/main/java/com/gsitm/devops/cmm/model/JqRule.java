package com.gsitm.devops.cmm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JqRule {
	private String field;
	private String op;
	private String data;
	
	public JqRule() {
		super();
	}
	
	public JqRule(String field, String op, String data) {
		super();
		this.field = field;
		this.op = op;
		this.data = data;
	}
}