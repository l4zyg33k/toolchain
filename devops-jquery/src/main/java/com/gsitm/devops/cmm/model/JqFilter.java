package com.gsitm.devops.cmm.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JqFilter {
	private String groupOp;
	private List<JqRule> rules;
	
	public JqFilter() {
		super();
	}
	
	public JqFilter(String groupOp, List<JqRule> rules) {
		super();
		this.groupOp = groupOp;
		this.rules = rules;
	}
}