package com.gsitm.devops.cmm.model;

public class JqFailure implements JqResult {

	private String message;

	public JqFailure(String message) {
		this.message = message;
	}

	@Override
	public Boolean getSuccess() {
		return Boolean.FALSE;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
