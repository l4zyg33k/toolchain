package com.gsitm.devops;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsViewResolver;
import org.springframework.web.servlet.view.tiles3.SpringBeanPreparerFactory;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.gsitm.devops.cmm.util.ExcelDownloadView;
import com.gsitm.devops.cmm.util.JqGridFilterHandlerMethodArgumentResolver;
import com.gsitm.devops.cmm.util.JqGridPageableHandlerMethodArgumentResolver;
import com.gsitm.devops.cmm.util.JqGridSearchHandlerMethodArgumentResolver;
import com.gsitm.devops.cmm.util.JqGridSortHandlerMethodArgumentResolver;
import com.gsitm.devops.cmm.util.MenuViewPreparer;
import com.gsitm.devops.cmm.util.MultiExcelDownloadView;

@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

	@Autowired
	private ApplicationContext context;

	@PersistenceContext
	private EntityManager em;

	@Override
	public void addFormatters(FormatterRegistry registry) {
		if (!(registry instanceof FormattingConversionService)) {
			return;
		}
		registerDomainClassConverterFor((FormattingConversionService) registry);
	}

	private void registerDomainClassConverterFor(
			FormattingConversionService conversionService) {
		DomainClassConverter<FormattingConversionService> converter = new DomainClassConverter<FormattingConversionService>(
				conversionService);
		converter.setApplicationContext(context);
	}

	@Override
	public void addArgumentResolvers(
			List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(configureSortResolver());
		argumentResolvers.add(configurePageableResolver());
		argumentResolvers.add(configureFilterResolver());
		argumentResolvers.add(configureSearchResolver());
	}

	@Bean(name = "pageableHandlerMethodArgumentResolver")
	public JqGridPageableHandlerMethodArgumentResolver configurePageableResolver() {
		JqGridPageableHandlerMethodArgumentResolver resolver = new JqGridPageableHandlerMethodArgumentResolver(
				configureSortResolver());
		resolver.setSizeParameterName("rows");
		return resolver;
	}

	@Bean(name = "sortHandlerMethodArgumentResolver")
	public SortHandlerMethodArgumentResolver configureSortResolver() {
		return new JqGridSortHandlerMethodArgumentResolver();
	}

	@Bean
	public JqGridSearchHandlerMethodArgumentResolver configureSearchResolver() {
		return new JqGridSearchHandlerMethodArgumentResolver();
	}

	@Bean(name = "filterHandlerMethodArgumentResolver")
	public JqGridFilterHandlerMethodArgumentResolver configureFilterResolver() {
		return new JqGridFilterHandlerMethodArgumentResolver();
	}
	

	@Bean(name = "tilesConfigurer")
	public TilesConfigurer configureTilesConfigurer() {
		TilesConfigurer configurer = new TilesConfigurer();
		configurer.setDefinitions(new String[] { "/WEB-INF/tiles/tiles.xml",
				"/WEB-INF/views/**/views.xml" });
		configurer.setPreparerFactoryClass(SpringBeanPreparerFactory.class);
		return configurer;
	}

	@Bean(name = "tilesViewResolver")
	public TilesViewResolver configureTilesViewResolver() {
		TilesViewResolver resolver = new TilesViewResolver();
		resolver.setViewClass(TilesView.class);
		return resolver;
	}

	@Bean(name = "menuViewPreparer")
	public MenuViewPreparer configureMenuViewPreparer() {
		return new MenuViewPreparer();
	}

	@Bean(name = "viewResolver")
	public JasperReportsViewResolver configureJasperReportsViewResolver() {
		JasperReportsViewResolver resolver = new JasperReportsViewResolver();
		resolver.setViewClass(JasperReportsMultiFormatView.class);
		resolver.setReportDataKey("datasource");
		return resolver;
	}

	@Bean(name = "multiExcelDownload")
	public MultiExcelDownloadView multiExcelDownloadView() {
		return new MultiExcelDownloadView();
	}

	@Bean(name = "excelDownload")
	public ExcelDownloadView excelDownloadView() {
		return new ExcelDownloadView();
	}
}
