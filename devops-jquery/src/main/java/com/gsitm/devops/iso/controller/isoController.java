package com.gsitm.devops.iso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class isoController {

	/***
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/iso/iso", method = RequestMethod.GET)
	public String process(Model model) {

		return "iso/iso/index";
	}

}
