// Spring Data JPA 와 연동을 위한 jqGrid 설정
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$.extend($.jgrid.defaults, {
	datatype : 'json',
	mtype : 'POST',
	gridview : true,
	viewrecords : true,
	autowidth : true,
	autoencode : true,
	multiSort : true,
	height : 'auto',
	jsonReader : {
		root : 'content',
		page : 'number' + 1,
		total : 'totalPages',
		records : 'numberOfElements',
		repeatitems : false
	},
	ajaxGridOptions : {
		beforeSend : function(jqXHR) {
			jqXHR.setRequestHeader(header, token);
		}
	}
}, $.jgrid.add, {
	ajaxEditOptions : {
		afterSubmit : function(response, postData) {
			var data = $.parseJSON(response.responseText);
			if (data && data.success) {
				return [ true ];
			} else {
				return [ false, data.message ];
			}
		}
	}
}, $.jgrid.edit, {
	ajaxEditOptions : {
		afterSubmit : function(response, postData) {
			var data = $.parseJSON(response.responseText);
			if (data && data.success) {
				return [ true ];
			} else {
				return [ false, data.message ];
			}
		}
	}
}, $.jgrid.del, {
	ajaxEditOptions : {
		afterSubmit : function(response, postData) {
			var data = $.parseJSON(response.responseText);
			if (data && data.success) {
				return [ true ];
			} else {
				return [ false, data.message ];
			}
		}
	}
});