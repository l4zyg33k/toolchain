$(function() {
	$.ajaxSetup({
		beforeSend : function(jqXHR) {
			jqXHR.setRequestHeader(header, token);
		}
	});

	$('.qms-notify').delay(3000).fadeTo(2000, 0.0);
	$('.qms-dialog').hide();
	$('#qms-infomation').hide();

	$.each(menus, function(key, top) {
		$('.qms-header-menu ul').append(
				'<li><a href="' + parseUrl(top.href) + '">' + top.name
						+ '</a></li>');
		if (top.selected) {
			$('.qms-sidebar-menu .pure-menu-heading').text(top.name);
			$('.qms-header-menu ul li:last a').css('font-weight', 'bold');
			$('.qms-header-menu ul li:last').addClass('pure-menu-selected');
			$.each(top.menus, function(key, side) {
				$('.qms-sidebar-menu ul').append(
						'<li><a href="' + parseUrl(side.href) + '">'
								+ side.name + '</a></li>');
				if (side.selected) {
					$('.qms-sidebar-menu ul li:last a').css('font-weight',
							'bold');
					$('.qms-sidebar-menu ul li:last').addClass(
							'pure-menu-selected');
				}
			});
		}
	});

	$('#btn-login-page').button().click(function(event) {
		$(location).attr('href', "login");
	});

	$('#btn-logout').button().click(function(event) {
		$('#logout').submit();
	});

	$('[title]').tooltip({
	    content: function() {
	        return $(this).attr('title');
	    },
	    open : function (event, ui) {setTimeout(function () {$(ui.tooltip).hide('fade')},3000)}
	});

	$('input, textarea').placeholder();
});

function roundFormat2(cellvalue, options, rowObject) {
	if (cellvalue === null) {
		return "";
	} else {
		return Math.round(cellvalue * 100) / 100;
	}
};

function notifyDone(msg) {
	$(".qms-notify").remove();
	$(".qms-pane-msg")
			.append(
					"<div class=\"qms-notify ui-widget\"><div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em; font-size: .8em;\"><p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>"
							+ msg + "</p></div></div>");
	$('.qms-notify').delay(3000).fadeTo(2000, 0.0);
};

function notifyFail(msg) {
	$(".qms-notify").remove();
	$(".qms-pane-msg")
			.append(
					"<div class=\"qms-notify ui-widget\"><div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em; font-size: .8em;\"><p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>"
							+ msg + "</p></div></div>");
	$('.qms-notify').delay(3000).fadeTo(2000, 0.0);
};

function popUp(url) {
	popupWindow = window
			.open(
					url,
					'',
					'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
};

function applyAccountAutoComplete(selector) {
	return $(selector + 'Name').autocomplete(({
		autoFocus : true,
		source : parseUrl('/rest/adm/acntmng'),
		select : function(event, ui) {
			$(selector).val(ui.item.username);
			$(this).val(ui.item.name);
			return false;
		},
		change : function(event, ui) {
			if (ui.item == null) {
				$(selector).val(null);
				$(this).val(null);
			}
		}
	})).data('ui-autocomplete')._renderItem = function(ul, item) {
		return $('<li>').append('<a>' + item.name + ' (' + item.team + ')</a>')
				.appendTo(ul);
	};
}

function applyProjectAutoComplete(selector) {
	return $(selector + 'Name').autocomplete(({
		autoFocus : true,
		source : parseUrl('/rest/pms/projmng'),
		select : function(event, ui) {
			$(selector).val(ui.item.code);
			$(this).val(ui.item.name);
			return false;
		},
		change : function(event, ui) {
			if (ui.item == null) {
				$(selector).val(null);
				$(this).val(null);
			}
		}
	})).data('ui-autocomplete')._renderItem = function(ul, item) {
		return $('<li>').append(
				'<a><b>' + item.code + '</b> ' + item.name + '</a>').appendTo(
				ul);
	};
}

function applyAccountTagit(selector) {
	$.isSelected = false;
	return $(selector).tagit(
			{
				fieldName : selector.replace(/^#/g, ''),
				autocomplete : {
					delay : 0,
					minLength : 2,
					autoFocus : true,
					source : parseUrl('/rest/adm/acntmng'),
					select : function(event, ui) {
						event.target.value = ui.item.name + "("
								+ ui.item.username + ")/" + ui.item.team;
						$.isSelected = true;
						return false;
					},
					change : function(event, ui) {
						if (ui.item == null) {
							event.target.value = "";
						}
					},
					create : function() {
						$(this).data('ui-autocomplete')._renderItem = function(
								ul, item) {
							$.dataSource.push(item.username);
							return $('<li>').append(
									'<a>' + item.name + ', ' + item.team
											+ '</a>').appendTo(ul);
						}
					},
					close : function() {
						if ($.isSelected) {
							this.blur();
							this.focus();
							$.isSelected = false;
						}
					}
				},
				beforeTagAdded : function(event, ui) {
					if (ui.tag.id == undefined) {
						ui.tag.id = function() {
							var a = ui.tagLabel.split("/")[0]
									.match(/\([^\)]+\)/g);
							return a != null ? a.join(", ").replace(/[\(\)]/g,
									"") : "";
						}();
					}
					if ($.isLoaded) {
						if ($.dataSource.indexOf(ui.tag.id) == -1) {
							$(".ui-autocomplete-input").val("");
							return false;
						}
						if (ui.tagLabel == "not found") {
							$(".ui-autocomplete-input").val("");
							return false;
						}
					}
					$.dataSource = [];
				},
				afterTagAdded : function(event, ui) {
					$('<span class="tagit-label">' + ui.tagLabel + '</span>')
							.attr("id", ui.tag.id);
				}

			});
}

function setSelectionJqGrid(rowId, scope) {
	if ($(scope).jqGrid('getGridParam', 'records') > 0) {
		if (!rowId) {
			rowId = $(scope).jqGrid('getDataIDs')[0];
		}
		$(scope).jqGrid('setSelection', rowId);
	}
}

function setButton(selector, icon, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-' + icon
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setButtonFn(selector, icon, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-' + icon
		},
		disabled : disabled
	}).click(callback);
}

function setSearchButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-search'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setSearchButtonFn(selector, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-search'
		},
		disabled : disabled
	}).click(callback);
}

function setAddButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-plus'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setEditButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-pencil'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setViewButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-document'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setDelButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-trash'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setSaveButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-disk'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setSaveButtonFn(selector, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-disk'
		},
		disabled : disabled
	}).click(callback);
}

function setCancelButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-cancel'
		},
		disabled : disabled
	}).click(function() {
		$(el).click();
	});
}

function setCancelButtonFn(selector, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-cancel'
		},
		disabled : disabled
	}).click(callback);
}

function setSaveFormButton(selector, disabled, el) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-disk'
		},
		disabled : disabled
	}).click(function() {
		$(el).submit();
	});
}

function setSaveFormButtonFn(selector, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-disk'
		},
		disabled : disabled
	}).click(callback);
}

function setCancleFormButton(selector, disabled, url) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-cancel'
		},
		disabled : disabled
	}).click(function() {
		$(location).attr('href', url);
	});
}

function setExportButtonFn(selector, disabled, callback) {
	$(selector).button({
		icons : {
			primary : 'ui-icon-extlink'
		},
		disabled : disabled
	}).click(callback);
}

function applyProjectTypeAutoComplete(selector) {
	return $(selector + 'Name').autocomplete(({
		autoFocus : true,
		source : parseUrl('/rest/pms/projType'),
		select : function(event, ui) {
			$(selector).val(ui.item.code);
			$(selector + 'Name').val(ui.item.name);
			$(selector + 'TypeCode').val(ui.item.typeCode);
			$(selector + 'TypeName').val(ui.item.typeName);
			return false;
		},
		change : function(event, ui) {
			if (ui.item == null) {
				$(selector).val(null);
				$(selector + 'Name').val(null);
				$(selector + 'TypeCode').val(null);
				$(selector + 'TypeName').val(null);
			}
		}
	})).data('ui-autocomplete')._renderItem = function(ul, item) {
		return $('<li>').append(
				'<a><b>' + item.code + '</b> ' + item.name + '</a>').appendTo(
				ul);
	};
}

function jqResult(response, postData) {
	var data = $.parseJSON(response.responseText);
	if (data && data.success) {
		return [ true ];
	} else {
		return [ false, data.message ];
	}
}