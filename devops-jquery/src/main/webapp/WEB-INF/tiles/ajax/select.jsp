<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<select>
	<c:forEach items="${options}" var="option">
		<option value="${option.key}">${option.value}</option>
	</c:forEach>
</select>