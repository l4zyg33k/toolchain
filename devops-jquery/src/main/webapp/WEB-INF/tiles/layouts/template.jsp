<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>GS 리테일 품질관리 시스템</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--[if lt IE 9]>
	<script src="<c:url value="/webjars/html5shiv/3.7.2/html5shiv.min.js" />"></script>
	<script src="<c:url value="/webjars/respond/1.4.2/dest/respond.min.js" />"></script>
<![endif]-->
<link rel="shortcut icon" href="<c:url value="/img/favicon.ico" />"
	type="image/x-icon">
<link rel="icon" href="<c:url value="/img/favicon.ico" />"
	type="image/x-icon">
<link rel="stylesheet" href="<c:url value="/css/app.css" />" />
<link rel="stylesheet"
	href="<c:url value="/webjars/jquery-ui-themes/1.11.4/redmond/jquery-ui.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/webjars/yui-pure/0.5.0/pure-min.css" />" />
<link rel="stylesheet"
	href="<c:url value="/webjars/font-awesome/4.3.0/css/font-awesome.min.css" />">
<!-- 3rd party 스타일시트 시작 -->
<tilesx:useAttribute id="styles" name="styles"
	classname="java.util.List" ignore="true" />
<c:forEach var="css" items="${styles}">
	<link rel="stylesheet" href="<c:url value="${css}" />" />
</c:forEach>
<link rel="stylesheet"
	href="<c:url value="/css/pure.customization.css" />" />
<!-- 3rd party 스타일시트 종료 -->
<script type="text/javascript"
	src="<c:url value="/webjars/jquery/1.11.2/jquery.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/webjars/jquery-ui/1.11.4/jquery-ui.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/webjars/jquery-placeholder/2.0.7/jquery.placeholder.min.js" />"></script>
<!-- 3rd party 자바스크립트 시작 -->
<tilesx:useAttribute id="scripts" name="scripts"
	classname="java.util.List" ignore="true" />
<c:forEach var="js" items="${scripts}">
	<script type="text/javascript" src="<c:url value="${js}" />"></script>
</c:forEach>
<!-- 3rd party 자바스크립트 종료 -->
<script type="text/javascript" src="<c:url value="/js/app.js" />"></script>
<script type="text/javascript">
    var menus = eval('${menus}');

	function dialog(message, callback) {
		$('.qms-dialog-message').text(message);
		$('.qms-dialog').dialog({
			autoOpen : false,
			modal : true,
			buttons : [ {
				text : '<s:message code="button.accept" />',
				click : function() {
					$(this).dialog("close");
					callback();
				}
			}, {
				text : '<s:message code="button.cancel" />',
				click : function() {
					$(this).dialog("close");
				}
			} ],
			closeText : '<s:message code="dialog.close" />',
			title : '<s:message code="dialog.confirm" />'
		});
		$('.ui-dialog-titlebar').css('font-size', '.7em');
		$('.ui-dialog-buttonset').css('font-size', '.7em');
		$('.qms-dialog').dialog('open');
	}

	function infomation(message) {
		$('#qms-dialog-message').text(message);
		$('#qms-infomation').dialog({
			autoOpen : false,
			modal : true,
			buttons : [ {
				text : '<s:message code="button.accept" />',
				click : function() {
					$(this).dialog("close");
				}
			} ],
			closeText : '<s:message code="dialog.close" />',
			title : '<s:message code="dialog.confirm" />'
		});
		$('.ui-dialog-titlebar').css('font-size', '.7em');
		$('.ui-dialog-buttonset').css('font-size', '.7em');
		$('#qms-infomation').dialog('open');
	}

	function parseUrl(url) {
		var root = '<c:url value="/" />'.slice(0, -1);

		return root + url;
	}
	//연계콤보 설정
	function setChildSelectBox(pObject, cObject, parentCode, defalutMsg) {
		if (pObject.val() == "") {
			cObject.children().remove();
			cObject.append($('<option>').text(defalutMsg).attr('value', ""));
			return;
		}
		var url = '/rest/slm/getLinkCode';
		var data = {
			parentId : parentCode,
			filterCode : pObject.val()
		};
		$.post(url, data, function(data, textStatus, XMLHttpRequest) {
			cObject.children().remove();
			if (defalutMsg != undefined && defalutMsg != ""
					&& defalutMsg != null) {
				cObject
						.append($('<option>').text(defalutMsg)
								.attr('value', ""));
			}
			$.each(data, function(i, value) {
				cObject.append($('<option>').text(value.name).attr('value',
						value.id));
			});
		});
	};
</script>
</head>
<body>
	<div class="qms-pane-container">
		<div class="qms-pane-header">
			<tiles:insertAttribute name="header" />
		</div>
		<div class="qms-pane-sidebar">
			<tiles:insertAttribute name="sidebar" />
		</div>
		<div class="qms-pane-content">
			<div class="qms-pane-msg" style="z-index:99;">
				<c:if test="${not empty message.information}">
					<div class="qms-notify ui-widget">
						<div class="ui-state-highlight ui-corner-all"
							style="margin-top: 20px; padding: 0 .7em; font-size: .8em;">
							<p>
								<span class="ui-icon ui-icon-info"
									style="float: left; margin-right: .3em;"></span>
								${message.information}
							</p>
						</div>
					</div>
				</c:if>
				<c:if test="${not empty message.alert}">
					<div class="qms-notify ui-widget">
						<div class="ui-state-error ui-corner-all"
							style="margin-top: 20px; padding: 0 .7em; font-size: .8em;">
							<p>
								<span class="ui-icon ui-icon-alert"
									style="float: left; margin-right: .3em;"></span>
								${message.alert}
							</p>
						</div>
					</div>
				</c:if>
			</div>
			<tiles:insertAttribute name="content" />

		</div>
		<div class="qms-pane-footer">
			<tiles:insertAttribute name="footer" />
		</div>
		<div class="qms-dialog">
			<p class="qms-dialog-content">
				<span class="qms-dialog-icon ui-icon ui-icon-alert"></span> <span
					class="qms-dialog-message"></span>
			</p>
		</div>
		<div id="qms-infomation">
			<p class="qms-dialog-content">
				<span class="qms-dialog-icon ui-icon ui-icon-info"></span> <span
					id="qms-dialog-message"></span>
			</p>
		</div>
	</div>
</body>
</html>