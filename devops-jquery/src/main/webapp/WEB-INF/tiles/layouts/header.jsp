<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<div class="qms-header-logo"></div>
<div class="qms-header-padding">
	<div class="qms-header-userinfo">
		<sec:authorize access="!isAuthenticated()">
			<button id="btn-login-page">로그인</button>
		</sec:authorize>
		<sec:authorize access="isAuthenticated()">
			<sec:authentication var="principal" property="principal" />
			<div class="qms-header-hello">
			<i class="fa fa-user fa-lg"></i> ${principal.name}(${principal.username})님
			</div>
			<div class="qms-header-userinfo">
			<c:url value="/logout" var="action" />
			<form:form id="logout" action="${action}">
				<button id="btn-logout">로그아웃</button>
			</form:form>
			</div>
		</sec:authorize>
	</div>
</div>
<div class="qms-header-menu ui-widget-content">
	<div class="pure-menu pure-menu-open pure-menu-horizontal">
		<ul>
		</ul>
	</div>
</div>