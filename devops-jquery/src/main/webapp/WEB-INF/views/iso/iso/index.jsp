<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">업무절차 및 규정</span>    
    </div>
</div>
<div class="qms-content-main">
	<a href="http://165.243.192.44/ISO/qp/qupl_menu.htm" target="about:blank">표준프로세스 보기(팝업)</a>
	<br/><br/>
	<a href="http://165.243.192.44/QC/qc01/qcmenu.htm" target="about:blank">시스템 개발방법론 보기(팝업)</a>
</div>
<br />
