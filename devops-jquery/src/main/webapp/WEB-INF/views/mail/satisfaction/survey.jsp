<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>만족도 설문조사</title>

<link rel="shortcut icon" href="<c:url value = "/img/favicon.ico" />" type="image/x-icon">
<link rel="stylesheet" href="<c:url value = "/webjars/jquery-ui-themes/1.10.3/redmond/jquery-ui.css" />">
<link rel="stylesheet" href="<c:url value = "/webjars/jquery-ui-themes/1.10.3/redmond/jquery.ui.theme.css" />">
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css" />
<link rel="stylesheet" href="<c:url value = "/webjars/font-awesome/4.3.0/css/font-awesome.min.css" />">
<link rel="stylesheet" href="<c:url value ="/webjars/jqgrid/4.6.0/css/ui.jqgrid.css" />">
<link rel="stylesheet" href="<c:url value ="/css/jquery-ui-timepicker-addon.css" />">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/demos/style.css">
  
<style>
body {
	font-family: 'Nanum Gothic Coding';
	margin-top: 60px;
	margin-left: 10px;
	margin-right: 10px;
}

.ui-jqgrid .ui-jqgrid-view {
	font-family: 'Malgun Gothic', sans-serif;
	font-size: 0.8em;
}

.ui-jqgrid .ui-jqgrid-htable th div {
	font-family: 'Malgun Gothic', sans-serif;
	height: 30px;
}

.ui-jqgrid tr.jqgrow td {
	font-family: 'Malgun Gothic', sans-serif;
	height: 30px;
}

.ui-jqgrid .ui-pg-table td {
	font-family: 'Malgun Gothic', sans-serif;
}

.qms-content-buttonset {
	clear: both;
    float: right;
    font-size: 0.9em;
    margin-top: 2px;
    font-family: 'Nanum Gothic Coding';
}

.tableQ1 {
	border:none;
	font-family: 'Nanum Gothic Coding';
	width:100%;
	
}

.tdQ1 {
	border:1px gray solid;
	font-family: 'Nanum Gothic Coding';
	font-size:0.8em;
	width:60%;
	height:20px;
}
.tableH {
	text-align:center;
	background-color: #D9E5FF;
}
.tdR1 {
	border:1px gray solid;
	font-size:0.8em;
	text-align:center;
	width:5%;
}
.inputQ1{
	border:none;
	border-right:0px;
	border-top:0px;
	boder-left:0px;
	boder-bottom:0px;
	width:90%;
}

.tdQ2 {
	border:none;
	font-family: 'Nanum Gothic Coding';
	font-size:0.8em;
	width:90%;
	height:20px;
}
.tdR2 {
	border:1px gray solid;
	font-size:0.8em;
	text-align:center;
	width:13%;
	height:20px;
}
.tdR3 {
	border:none;
	font-size:0.8em;
	text-align:center;
	width:100%;
	height:20px;
}
.textareaR3 {
	width:99%;
}




</style>
</head>

<body>
	<div>
		<p align="center"
			style="font-family: 'Malgun Gothic ', sans-serif; font-size: x-large; font-weight: bold;">만족도 설문조사</p>
	</div>
	<div class="qms-content-main">
	 

		<form:form id="frm-project" modelAttribute="projectVO" action='edit' method="POST"
			cssClass="pure-form pure-form-aligned">
			<fieldset>
				<legend>프로젝트 정보</legend>
				<div class="pure-control-group">
					<div class="pure-u-1-4">
						<form:label path="code">프로젝트 코드</form:label>
						<form:input path="code" class="pure-u-1-3" readOnly="readOnly" />
					</div>
					<div class="pure-u-2-5">
						<form:label path="name">프로젝트 명</form:label>
						<form:input path="name" class="pure-u-7-12" readOnly="readOnly" />
					</div>
					<div class="pure-u-1-4">
						<form:label path="qa.name">품질 관리자</form:label>
						<form:input path="qa.name" class="pure-u-1-3" readOnly="readOnly" />
						
					</div>
				</div>
			</fieldset>
			<fieldset>			
				<legend>설문 문항</legend>
				<div class="pure-g">
					<div class="pure-u-1-2">
					</div>
					<div class="pure-u-1-2">
						<div>
							<button class="pure-button qms-content-buttonset" id="btn-edit">저장</button>
						</div>
					</div>
				</div>
				<div id="tabs" >
					<ul>
					
					</ul>
				</div>

			</fieldset>
		<input type="hidden" id="rowdata" name="rowdata" />
		<input type="hidden" id="username" name="username" value="${memberVO.id}" />
		
		</form:form>
		
	</div>
	
	<script src="<c:url value="/webjars/jquery-ui/1.10.3/ui/minified/i18n/jquery.ui.datepicker-ko.min.js" />"></script>
	<script src="<c:url value="/webjars/tinymce-jquery/4.0.16/tinymce.min.js" />"></script>
	<script src="<c:url value="/webjars/jqgrid/4.4.5/js/i18n/grid.locale-kr.js" />"></script>
	<script src="<c:url value="/webjars/jqgrid/4.4.5/js/minified/jquery.jqGrid.min.js" />"></script>
	<script src="<c:url value="/webjars/json2/20110223/json2.min.js" />"></script>
	<script src="<c:url value="/webjars/jquery-maskedinput/1.3.1/jquery.maskedinput.min.js" />"></script>
	<script src="<c:url value="/js/jquery-ui-timepicker-addon.js" />"></script>
	<script src="<c:url value="/js/jquery-ui-timepicker-ko.js" />"></script>
	<script src="<c:url value="/js/jquery.jqGrid.custom.js" />"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


	<script type="text/javascript">
		

		$(function() {
		    
		    
		  //저장버튼
			$("#btn-edit").button({
				icons : {
					primary : 'ui-icon-pencil'
				}
			}).click(function(event) {
				if (checkValidation()){
					//alert("입력을 전부 하시기 바랍니다.");
					return false;
				}
				var frm = $('#frm-project');
				var data = JSON.stringify(frm.serializeArray());
				$('#rowdata').val(data);
				frm.submit();
				//post('/mail/satisfaction/survey/edit', data);
				

			});

		  	//탭 타이틀 설정
			<c:forEach var="code" items="${surveyTypeVO}">
				$('#tabs ul').append('<li><a href="#${code.id}">${code.name}</a></li>');
				$('#tabs').append('<div id="${code.id}">' +						 
				                       	'<div><font size="3em">1. 다음은 ' + '${code.name}' + '에 관한 설문입니다.</font>'+
				                       		'<table id="table_'+'${code.code}'+'T210" class="tableQ1">'+
				                       			'<tr><td class="tdQ1 tableH">문항</td>' +
				                       			    '<td class="tdR1 tableH">매우<br/>만족</td>'+
				                       				'<td class="tdR1 tableH">만족</td>'+
				                       				'<td class="tdR1 tableH">약간<br/>만족</td>'+
				                       				'<td class="tdR1 tableH">그저<br/>그렇다</td>'+
				                       				'<td class="tdR1 tableH">약간<br/>불만족</td>'+
				                       				'<td class="tdR1 tableH">불만족</td>'+
				                       				'<td class="tdR1 tableH">매우<br/>불만족</td>'+
				                       				'<td class="tableH"></td>'+
				                       				'<td class="tdR1  tableH">모름<br/>(N/A)</td>'+
				                       			'</tr>' +
				                       		'</table>' +
				                       	'</div><br/>'+
				                       	'<div>' +
				                       		'<table id="table_'+'${code.code}'+'T220" class="tableQ1"></table>'+
			                       		'</div><br/>'+
			                       		'<div>' +
			                       			'<table id="table_'+'${code.code}'+'T230" class="tableQ1"></table>' +
		                       			'</div>'+				                       
				                  '</div>');
			</c:forEach>
			
			var i = 1;
			<c:forEach var="question" items="${questionListVO}">
				var tableId = '#table_'+'${question.surveyType.code}'+'${question.questionType.code}';
				var questionId = '#result'+'$^'+'${question.surveyType.id}'+'$^' +'${question.questionType.id}'+'$^' +'${question.questionNumber}';
				
				//1번유형 문항
				if("T210" == "${question.questionType.code}"){
					$(tableId)				
					.append('<tr>'+
	                    '<td class="tdQ1"><label id="" class="inputQ1" readOnly=true>1-${question.questionNumber}. ${question.surveyQuestion}</label></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=7 ></td>'+ 
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=6 ></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=5 ></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=4 ></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=3 ></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=2 ></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=1 ></td>'+
	                    '<td class=""></td>'+
	                    '<td class="tdR1"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=0></td>'+
	                    
	                  '</tr>');
				}
				
				//2번문항유형
				if("T220" == "${question.questionType.code}"){					
					$(tableId)				
					.append('<tr>'+
	                    		'<td class="tdQ2" colspan="9"><label id="" class="inputQ1" readOnly=true>2-${question.questionNumber}. ${question.surveyQuestion}</label></td>'+	                    
	                  		'</tr>'+
	                  		'<tr>'+
	                  			'<td class="tdR2 tableH"><label>매우만족</label></td>'+
	                  			'<td class="tdR2 tableH"><label>만족</label></td>'+
	                  			'<td class="tdR2 tableH"><label>약간만족</label></td>'+
	                  			'<td class="tdR2 tableH"><label>그저그렇다</label></td>'+
	                  			'<td class="tdR2 tableH"><label>약간불만족</label></td>'+
	                  			'<td class="tdR2 tableH"><label>불만족</label></td>'+
	                  			'<td class="tdR2 tableH"><label>매우불만족</label></td>'+
	                  			'<td class="tableH"><label></label></td>'+
	                  			'<td class="tdR2 tableH"><label>모름(N/A)</label></td>'+
                  			'</tr>' +
	                  		'<tr>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=7 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=6 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=5 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=4 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=3 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=2 ></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=1 ></td>'+
	                  			'<td></td>'+
	                  			'<td class="tdR2"><input type="radio" id="'+questionId+'" name="'+questionId+'" value=0 ></td>'+
	                  		'</tr>'
					);
				}
				
				//3번문항유형
				if("T230" == "${question.questionType.code}"){					
					$(tableId)				
					.append('<tr>'+
	                    		'<td class="tdQ2"><label id="" class="inputQ1" readOnly=true>3-${question.questionNumber}. ${question.surveyQuestion}</label></td>'+	                    
	                  		'</tr>'+
	                  		'<tr>'+
	                  			'<td class="tdR3">'+
	                  				'<textarea id="'+questionId+'" name="'+questionId+'" class="textareaR3" rows="5" />' +	                  				
								'</td>'+
                  			'</tr>'
					);
				}
			
			</c:forEach>
			$( "#tabs" ).tabs();
			
			
			function checkValidation(){
				
				var check = false;
				var radios = [];
				var lastNameInput = 1;
				var lastNameTextarea = 1;
				$('input[type="radio"]').each(function() {
			        radios[$(this).attr('name')] = true;
			    });
				
				for (name in radios) {
			        var radio_buttons = $("input[name='" + name + "']");
			        radio_buttons.parent().parent().removeAttr("style");
			        if (radio_buttons.filter(':checked').length == 0) {
			        	radio_buttons.parent().parent().css('background-color', '#FFC6C6');
			        	lastNameInput = name;
			        	check = true;
			        } 
			        else {
			            var val = radio_buttons.val();
			        }
			    }
				
				$('textarea[name*="result"]').each(function(index,e){
					$(e).removeAttr("style");
					if ( $(e).val() == "" ){
						$(e).css('background-color', '#FFC6C6');
						lastNameTextarea = $(e).attr('name');
						check = true;
					}
				});
				
				if (check){
					var eInput = $("[name='" + lastNameInput + "']");					
					var eTextarea = $("[name='" + lastNameTextarea + "']");
					var tabIndexInput = eInput.parent().parent().parent().parent().parent().parent().index();
					var tabIndexTextarea = eTextarea.parent().parent().parent().parent().parent().parent().index();
					var tabIndex = 1;
					
					tabIndexInput > tabIndexTextarea ? tabIndex=tabIndexInput : tabIndex=tabIndexTextarea;
					
			        $("#tabs").tabs( "option","active", tabIndex-1 );			        
				}
				return check;
			}
			
			
			
			
		  });
		
		
	</script>
</body>
</html>