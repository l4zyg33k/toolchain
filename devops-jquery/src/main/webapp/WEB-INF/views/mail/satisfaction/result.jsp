<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>만족도 설문조사</title>

<link rel="shortcut icon" href="<c:url value = "/img/favicon.ico" />" type="image/x-icon">
<link rel="stylesheet" href="<c:url value = "/webjars/jquery-ui-themes/1.11.2/redmond/jquery-ui.css" />">
<link rel="stylesheet" href="<c:url value = "/webjars/jquery-ui-themes/1.11.2/redmond/jquery.ui.theme.css" />">
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css" />
<link rel="stylesheet" href="<c:url value = "/webjars/font-awesome/4.3.0/css/font-awesome.min.css" />">
<link rel="stylesheet" href="<c:url value ="/webjars/jqgrid/4.4.5/css/ui.jqgrid.css" />">
<link rel="stylesheet" href="<c:url value ="/css/jquery-ui-timepicker-addon.css" />">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/demos/style.css">
  
<style>
body {
	font-family: 'Nanum Gothic Coding';
	margin-top: 60px;
	margin-left: 10px;
	margin-right: 10px;
}

.ui-jqgrid .ui-jqgrid-view {
	font-family: 'Malgun Gothic', sans-serif;
	font-size: 0.8em;
}

.ui-jqgrid .ui-jqgrid-htable th div {
	font-family: 'Malgun Gothic', sans-serif;
	height: 30px;
}

.ui-jqgrid tr.jqgrow td {
	font-family: 'Malgun Gothic', sans-serif;
	height: 30px;
}

.ui-jqgrid .ui-pg-table td {
	font-family: 'Malgun Gothic', sans-serif;
}

.qms-content-buttonset {
	clear: both;
    float: right;
    font-size: 0.9em;
    margin-top: 2px;
    font-family: 'Nanum Gothic Coding';
}

.tableQ1 {
	border:none;
	font-family: 'Nanum Gothic Coding';
	width:100%;
	
}

.tdQ1 {
	border:1px gray solid;
	font-family: 'Nanum Gothic Coding';
	font-size:0.8em;
	width:60%;
	height:20px;
}
.tableH {
	text-align:center;
	background-color: #D9E5FF;
}
.tdR1 {
	border:1px gray solid;
	font-size:0.8em;
	text-align:center;
	width:5%;
}
.inputQ1{
	border:none;
	border-right:0px;
	border-top:0px;
	boder-left:0px;
	boder-bottom:0px;
	width:90%;
}

.tdQ2 {
	border:none;
	font-family: 'Nanum Gothic Coding';
	font-size:0.8em;
	width:90%;
	height:20px;
}
.tdR2 {
	border:1px gray solid;
	font-size:0.8em;
	text-align:center;
	width:13%;
	height:20px;
}
.tdR3 {
	border:none;
	font-size:0.8em;
	text-align:center;
	width:100%;
	height:20px;
}

</style>
</head>

<body>
	<div>
		<p align="center"
			style="font-family: 'Malgun Gothic ', sans-serif; font-size: x-large; font-weight: bold;">${messageVO}</p>
	</div>
	<div class="qms-content-main">
	 

		
		
	</div>
	
	<script src="<c:url value="/webjars/jquery-ui/1.10.3/ui/minified/i18n/jquery.ui.datepicker-ko.min.js" />"></script>
	<script src="<c:url value="/webjars/tinymce-jquery/4.0.16/tinymce.min.js" />"></script>
	<script src="<c:url value="/webjars/jqgrid/4.4.5/js/i18n/grid.locale-kr.js" />"></script>
	<script src="<c:url value="/webjars/jqgrid/4.4.5/js/minified/jquery.jqGrid.min.js" />"></script>
	<script src="<c:url value="/webjars/json2/20110223/json2.min.js" />"></script>
	<script src="<c:url value="/webjars/jquery-maskedinput/1.3.1/jquery.maskedinput.min.js" />"></script>
	<script src="<c:url value="/js/jquery-ui-timepicker-addon.js" />"></script>
	<script src="<c:url value="/js/jquery-ui-timepicker-ko.js" />"></script>
	<script src="<c:url value="/js/jquery.jqGrid.custom.js" />"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


	<script type="text/javascript">		
		
	</script>
</body>
</html>