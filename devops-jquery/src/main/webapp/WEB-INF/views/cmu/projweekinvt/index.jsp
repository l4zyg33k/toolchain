<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		initDateTimePicker();
		initSelect();
	});

	//버튼 처리
	function initButtons() {
		//조회
		// navGrid의 refresh를 클릭하여, serializeGridData 이벤트에 의해 postData가 설정 되도록 합니다. 
		setSearchButton('#btn-search', false, '#refresh_jqg-table');

		//보기
		setViewButton('#btn-view', eval('${!navGrid.view}'), '#view_jqg-table');

		//엑셀다운
		$('#btn-excel').button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function() {
			var i, record;
			var jsonData = "[";
			var excelGridData = $('#jqg-table').jqGrid('getRowData');
			for (row in excelGridData) {
				i = 1;
				record = excelGridData[row];
				for (cell in record) {
					if (i == 2) {
						jsonData += '{"프로젝트 코드":"' + record[cell] + '"';
					}
					if (i == 3) {
						jsonData += ',"프로젝트이름":"' + record[cell] + '"';
					}
					if (i == 4) {
						jsonData += ',"제목":"' + record[cell] + '"';
					}
					if (i == 5) {
						jsonData += ',"보고 일자":"' + record[cell] + '"';
					}
					if (i == 6) {
						jsonData += ',"작성자":"' + record[cell] + '"},';
					}
					i++;
				}
			}
			jsonData = jsonData.substr(0, jsonData.length - 1) + "]";

			//$('#excelForm #data').val(JSON.stringify($('#jqg-table').getRowData()));
			$('#excelForm #data').val(jsonData);
			$('#excelForm').submit();
		});

		$('.qms-content-buttonset').css('display', 'block');
	}

	//date picker, timepicker 세팅
	function initDateTimePicker() {
		$('#fromReportDate').datepicker();
		$('#fromReportDate').mask('9999-99-99');
		$('#toReportDate').datepicker();
		$('#toReportDate').mask('9999-99-99');
	}

	//주간보고목록 그리드 조회
	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/cmu/projweekinvt" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '프로젝트 코드',
				name : 'project.code',
				align : 'center',
				width : 100,
				fixed : true
			}, {
				label : '프로젝트이름',
				name : 'project.name',
				align : 'center',
				width : 100,
				fixed : true
			}, {
				label : '제목',
				name : 'title'
			}, {
				label : '보고 일자',
				name : 'reportDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '작성자',
				name : 'reportName',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				name : 'editable',
				hidden : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'project.code',
			sortorder : 'desc',
			caption : '주간보고 목록',
			serializeGridData : function(postData) {
				postData.team = $('#team').val();
				postData.fromReportDate = $('#fromReportDate').val();
				postData.toReportDate = $('#toReportDate').val();
				return postData;
			},
			loadComplete : function() {
				//데이터가 존재하는 경우
				if ($(this).jqGrid('getGridParam', 'records') > 0) {
					$('#btn-view').button('option', 'disabled', false);
					$('#btn-excel').button('option', 'disabled', false);
				} else {
					//데이터가 없는 경우
					$('#btn-view').button('option', 'disabled', true);
					$('#btn-excel').button('option', 'disabled', true);
				}
				// 데이터가 존재하는 경우, 해당 rowId를 선택합니다.
				setSelectionJqGrid('${rowId}', this);
			}
		});
	}

	//그리드 버튼 처리 
	function initNavGrid() {
		$('#jqg-table').jqGrid('navGrid', '#jqg-pager', {
			edit : false,
			add : false,
			del : false,
			view : true
		}, {}, {}, {}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});
	}

	//상세보기
	function beforeInitDataView(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		if (eval('${navGrid.view}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var fromReportDate = $('#fromReportDate').val();
			var toReportDate = $('#toReportDate').val();
			
			var url = '<s:url value="/cmu/projweekinvt/" />' + rowId
					+ '/view?page=' + page + '&rowNum=' + rowNum + '&rowId='
					+ rowId + '&fromReportDate=' + fromReportDate + '&toReportDate=' + toReportDate;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('조회 할 수 없습니다.');
			return false;
		}
	}

	function initSelect() {
		$('#team').change(function(event) {
			$('#refresh_jqg-table').click();
		});
		$('#fromReportDate').change(function(event) {
			$('#refresh_jqg-table').click();
		});
		$('#toReportDate').change(function(event) {
			$('#refresh_jqg-table').click();
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">주간보고 현황</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form id="query-form" class="pure-form pure-form-stacked">
				<div class="pure-g">
					<div class="pure-u-1-2">
						<label for="team">소속 팀</label>
						<form:select id="team" path="team" items="${teams}" />
					</div>
					<div class="pure-u-1-4">
						<label for="fromReportDate">검색 시작 일자</label> <input
							id="fromReportDate" value="${fromReportDate}"
							placeholder="YYYY-MM-DD" size="10" />
					</div>
					<div class="pure-u-1-4">
						<label for="toReportDate">검색 종료 일자</label> <input
							id="toReportDate" value="${toReportDate}"
							placeholder="YYYY-MM-DD" size="10" />
					</div>
				</div>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<a id="btn-view">보기</a> <a id="btn-excel">엑셀저장</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />
<form id="excelForm" name="excelForm"
	action="<s:url value="/excelDownload" />" method="post">
	<input type="hidden" name="data" id="data" /> <input type="hidden"
		name="filename" id="filename" value="주간보고현황.xlsx" /> <input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>