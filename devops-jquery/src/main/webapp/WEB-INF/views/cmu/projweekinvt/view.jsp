<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initAttachmentsJqGrid();
		initJqGrid();
		initButtons();
		initCKEditor();
		initForm();
	});

	function initJqGrid() {
		//이슈정보
		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/cmu/isuemng/projweekinvt" />',
			postData : {
				page : '1',
				rowNum : '3',
				rowId : '',
				project : '${projWeekInvtFrmVO.project}'
			},
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '이슈 등록일',
				name : 'occurrenceDate',
				key : true,
				align : 'center',
				search : false,
				width : '50'
			}, {
				label : '이슈 제목',
				name : 'title',
				search : false,
				width : '150'
			}, {
				label : '상태',
				name : 'status.name',
				search : false,
				width : '50'
			}, {
				label : '상태 점검일',
				name : 'planResolveDate',
				align : 'center',
				width : '50'
			} ],
			page : 1,
			rowNum : 3,
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '이슈정보'
		});

		//리스크 정보
		$("#jqg-table1").jqGrid({
			url : '<s:url value="/rest/cmu/riskmng/projweekinvt" />',
			postData : {
				page : '1',
				rowNum : '3',
				rowId : '',
				project : '${projWeekInvtFrmVO.project}'
			},
			colModel : [ {
				name : 'id', //id
				key : true,
				hidden : true
			//칼럼 hide
			}, {
				label : '리스크 발생일',
				name : 'occurDate',
				key : true,
				align : 'center',
				search : false,
				width : 100
			}, {
				label : '리스크 제목',
				name : 'title',
				search : false,
				width : 300
			}, {
				label : '리스크 상태',
				name : 'status.name',
				search : false,
				width : 100
			}, {
				label : '상태 점검일',
				name : '', //상태점검일 - 미정
				align : 'center',
				width : 100
			} ],
			page : 1,
			rowNum : 3,
			pager : '#jqg-pager1',
			sortname : 'id',
			sortorder : 'desc',
			height : "100%",
			caption : '리스크 정보'
		});

		//인스펙션 정보
		$("#jqg-table2").jqGrid({
			url : '<s:url value="/rest/pms/projinsp/projweekinvt" />',
			postData : {
				page : "1",
				rowNum : "3",
				rowId : "",
				project : "${projWeekInvtFrmVO.project}"
			},
			datatype : 'json',
			mtype : 'POST',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			//칼럼 hide
			}, {
				label : '수행일',
				name : 'inspectionDate',
				key : true,
				align : 'center',
				search : false,
				width : 100
			}, {
				label : '단계',
				name : 'productPhase.name',
				search : false,
				width : 50
			}, {
				label : '인스펙션 산출물',
				name : 'productName',
				search : false,
				width : 200
			}, {
				label : '큰 결함',
				name : 'majorDefectCount',
				align : 'center',
				width : 100
			}, {
				label : '작은 결함',
				name : 'minorDefectCount',
				align : 'center',
				width : 100
			}, {
				label : '등록자',
				name : 'producer.name',
				align : 'center',
				width : 100
			} ],
			page : 1,
			rowNum : 3,
			pager : '#jqg-pager2',
			sortname : 'id',
			sortorder : 'desc',
			height : "100%",
			caption : '인스펙션 정보'
		});

		//품질점검
		$("#jqg-table3").jqGrid(
				{
					//datatype : 'local',
					url : '<s:url value="/rest/qam/qualchec/detailsList" />',
					postData : {
						page : "1",
						rowNum : "2",
						rowId : "",
						projectCode : "${projWeekInvtFrmVO.project}"
					},
					datatype : 'json',
					mtype : 'POST',					
					colNames : [ '#', '점검일', '점검유형', '점검단계', '점검내용', '필수', '권고', 'IL명', '점검자' ],
					colModel : [ {
						name : 'id', //id
						key : true,
						hidden : true
					//칼럼 hide
					}, {
						label : '점검일',
						name : 'inspectedDate', 
						key : true,
						align : 'center',
						search : false,
						width : 100
					}, {
						label : '점검 유형',
						name : 'type', 
						search : false,
						width : 100
					}, {
						label : '점검 단계',
						name : 'stage',
						search : false,
						width : 100
					}, {
						label : '점검 내용',
						name : 'title', 
						align : 'center',
						width : 150
					}, {
						label : '필수 수',
						name : 'requiredCount', 
						align : 'center',
						edittype : 'text',
						width : 25
					}, {
						label : '권고 수',
						name : 'opinionCount',
						align : 'center',
						edittype : 'text',
						width : 25
					}, {
						label : 'IL 이름',
						name : 'iLname',
						align : 'center',
						width : 100
					}, {
						label : '점검자',
						name : 'inspector', 
						align : 'center',
						width : 100
					} ],
					page : 1,
					rowNum : 3,
					pager : '#jqg-pager3',
					sortname : 'id',
					sortorder : 'desc',
					height : "100%",		
					caption : '품질 점검 정보'   
				});
	};

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${projWeekInvtFrmVO.getAttachmentsUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '생성자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '생성일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			} ],
			pager : '#jqg-atmt-pager',
			page : 1,
			rowNum : 3,
			caption : '첨부 파일 목록'
		});
	}

	function initButtons() {
		var fromReportDate = "${fromReportDate}";
		var toReportDate = "${toReportDate}";
		setCancleFormButton('#btn-list', false,
				'<s:url value="${projWeekInvtFrmVO.getPrevUrl()}" />' + '&fromReportDate=' + fromReportDate + '&toReportDate=' + toReportDate);
		$('.qms-content-buttonset').css('display', 'block');
	};

	function initCKEditor() {
		$('textarea').attr('disabled', 'disabled');
		$('textarea').ckeditor({
			toolbar : [ {
				name : 'tools',
				items : [ 'Maximize', '-', 'About' ]
			} ]
		});
	}

	function initForm() {
		$('#frm-isuemng :input').attr('readonly', 'readonly');
		$('#frm-isuemng :input').css({
			'background' : 'white',
			'color' : 'black'
		});
	}
</script>

<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">주간업무 현황</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-list">목록</button>
			</div>
		</div>
	</div>
	<form:form id="frm-isuemng" modelAttribute="projWeekInvtFrmVO"
		enctype="multipart/form-data" cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="projectName">프로젝트 명</form:label>
					<form:input path="projectName" cssClass="pure-input-2-3"
						readonly="true" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="projectStartDate">프로젝트 시작일</form:label>
					<form:input path="projectStartDate" cssClass="pure-input-2-3"
						readonly="true" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="projectFinishDate">프로젝트 종료일</form:label>
					<form:input path="projectFinishDate" cssClass="pure-input-2-3"
						readonly="true" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>주간업무</legend>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:label path="title">보고서 제목</form:label>
					<form:input path="title" cssClass="pure-input-1" readonly="true" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="reportDate">보고 일자</form:label>
					<form:input path="reportDate" cssClass="pure-input-2-3"
						readonly="true" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="reporterName">작성자</form:label>
					<form:input path="reporterName" cssClass="pure-input-2-3"
						readonly="true" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="phase">진행 단계</form:label>
					<form:input path="phase" cssClass="pure-input-2-3" readonly="true" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>금주 실적</legend>
			<form:textarea path="thisWeek" cssClass="pure-input-1" />
		</fieldset>
		<fieldset>
			<legend>차주 계획</legend>
			<form:textarea path="nextWeek" cssClass="pure-input-1" />
		</fieldset>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
	</form:form>
	<%-- 첨부 파일 --%>
	<table id="jqg-atmt-table"></table>
	<div id="jqg-atmt-pager"></div>
	<br />
	<%-- 이슈정보 --%>
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	<br />
	<%-- 리스크정보 --%>
	<table id="jqg-table1"></table>
	<div id="jqg-pager1"></div>
	<br />
	<%-- 인스펙션정보 --%>
	<table id="jqg-table2"></table>
	<div id="jqg-pager2"></div>
	<br />
	<%-- 품질점검정보 --%>
	<table id="jqg-table3"></table>
	<div id="jqg-pager3"></div>
</div>
<br />