<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initJqGrid();
		initAttachmentsJqGrid();
		initButtons();
		initCKEditor();
		initForm();
	});

	function initButtons() {
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${isueMngFrmVO.getPrevUrl()}" />');
		$('.qms-content-buttonset').css('display', 'block');
	};

	function initCKEditor() {
		$('textarea').attr('disabled', 'disabled');
		$('textarea').ckeditor({
			toolbar : [ {
				name : 'tools',
				items : [ 'Maximize', '-', 'About' ]
			} ]
		});
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${isueMngFrmVO.getDetailsUrl()}" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '변경일자',
				name : 'changedDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '상태',
				name : 'status',
				formatter : 'select',
				editoptions : {
					value : '${status}'
				},
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '변경내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '변경 목록',
			autoencode : false
		});
	}

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${isueMngFrmVO.getAttachmentsUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '생성자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '생성일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			} ],
			pager : '#jqg-atmt-pager',
			caption : '첨부 목록'
		});
	}

	function initForm() {
		$('#frm-isuemng :input').attr('readonly', 'readonly');
		$('#frm-isuemng :input').css({
			'background' : 'white',
			'color' : 'black'
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">이슈 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<form:form id="frm-isuemng" modelAttribute="isueMngFrmVO"
		enctype="multipart/form-data" cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>이슈 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" />
				</div>
				<div class="pure-u-3-4">
					<form:label path="projectName">프로젝트 이름</form:label>
					<form:input path="projectName" class="pure-input-1" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:label path="title">이슈 제목</form:label>
					<form:input path="title" cssClass="pure-input-1" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="projectIlName">담당자</form:label>
					<form:input path="projectIlName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="occurrenceDate">발생일자</form:label>
					<form:input path="occurrenceDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="planResolveDate">해결 예정일</form:label>
					<form:input path="planResolveDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="categoryName">범주</form:label>
					<form:input path="categoryName" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="statusName">상태</form:label>
					<form:input path="statusName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="importanceName">중요성</form:label>
					<form:input path="importanceName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="urgencyName">긴급성</form:label>
					<form:input path="urgencyName" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>내용</legend>				
			<div class="pure-g">
				<div class="pure-u-1">
					<form:textarea path="content" cssClass="pure-input-1" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>영향</legend>		
			<div class="pure-g">
				<div class="pure-u-1">
					<form:textarea path="impact" cssClass="pure-input-1" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>처리내용</legend>						
			<div class="pure-g">
				<div class="pure-u-1">
					<form:textarea path="actionPlan" cssClass="pure-input-1" />
				</div>
			</div>
		</fieldset>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="rowData" />
	</form:form>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	<br />
	<table id="jqg-atmt-table"></table>
	<div id="jqg-atmt-pager"></div>
</div>
<br />