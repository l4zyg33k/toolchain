<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		setViewButton('#btn-view', eval('${!navGrid.view}'), '#view_jqg-table');
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/cmu/isuemng" />',
			editurl : '<s:url value="/rest/cmu/isuemng/edit" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '프로젝트<br />코드',
				name : 'project.code',
				align : 'center',
				width : 70,
				fixed : true
			}, {
				label : '프로젝트 명',
				name : 'project.name'
			}, {
				label : '이슈 제목',
				name : 'title'
			}, {
				label : '담당자',
				name : 'project.il.name',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '발생일',
				name : 'occurrenceDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '범주',
				name : 'category.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '상태',
				name : 'status.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '중요도',
				name : 'importance.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '긴급성',
				name : 'urgency.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '해결<br />예정일',
				name : 'planResolveDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				name : 'editable',
				hidden : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'occurrenceDate',
			sortorder : 'desc',
			caption : '이슈  목록',
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					$('#view_jqg-table').click();
				}
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});
	}

	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '<s:url value="/cmu/isuemng/" />' + rowId + '/form?page='
					+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataAdd(formId, scope) {
		if (eval('${navGrid.add}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow') || 0;
			var url = '<s:url value="/cmu/isuemng/form?page=" />' + page
					+ '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('등록 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataDel(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataView(formId, scope) {
		if (eval('${navGrid.view}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow');
			var url = '<s:url value="/cmu/isuemng/" />' + rowId + '/view?page='
					+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('조회 할 수 없습니다.');
			return false;
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">이슈 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-view">보기</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />