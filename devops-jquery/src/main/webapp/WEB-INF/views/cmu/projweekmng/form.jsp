<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initAutoComplete();
		initDateTimePicker();
		initCKEditor();
		if (eval('${not projWeekMngFrmVO.isNew()}')) {
			initFileUpload();
			initAttachmentsJqGrid();
		}
	});

	function initButtons() {
		setSaveFormButton('#btn-save', false, '#frm-projweekmng');
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projWeekMngFrmVO.getPrevUrl()}" />');
	};

	function initAutoComplete() {
		applyAccountAutoComplete("#reporter");
	};

	function initDateTimePicker() {
		$('#reportDate').datepicker();
		$('#reportDate').mask('9999-99-99');
	}

	function initCKEditor() {
		$('textarea').ckeditor();
	}

	function initFileUpload() {
		$('#ajaxform').ajaxForm({
			success : function(responseText, statusText) {
				$('#jqg-atmt-table').jqGrid().trigger("reloadGrid");
				$('#ajaxform').clearForm();
			}
		});
	}

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${projWeekMngFrmVO.getAttachmentsUrl()}" />';
		var editurl = '<s:url value="${projWeekMngFrmVO.getAttachmentsEditUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			editurl : editurl,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '생성자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '생성일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '삭제',
				align : 'center',
				width : 40,
				fixed : true,
				formatter : 'actions',
				formatoptions : {
					editbutton : false,
					delbutton : true
				}
			} ],
			pager : '#jqg-atmt-pager',
			caption : '첨부 목록'
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">주간보고 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${projWeekMngFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-projweekmng" action="${action}"
		method="${projWeekMngFrmVO.getMethod()}"
		modelAttribute="projWeekMngFrmVO"
		cssClass="pure-form pure-form-stacked"
		enctype="multipart/form-data">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<div class="pure-g">
						<div class="pure-u-2-3">
							<form:label path="project" required="true">프로젝트</form:label>
							<c:choose>
								<c:when test="${projWeekMngFrmVO.isNew()}">
									<form:select path="project" items="${projects}"
										class="pure-input-1" />
									<form:errors path="project" cssClass="ui-state-error-text" />
								</c:when>
								<c:otherwise>
									<form:hidden path="project" />
									<input
										value="[ ${projWeekMngFrmVO.project} ] ${projWeekMngFrmVO.projectName}"
										readonly="readonly" class="pure-input-1" />
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="pure-u-1-4">
					<form:label path="projectStartDate">프로젝트 시작일</form:label>
					<form:input path="projectStartDate" readonly="true" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="projectFinishDate">프로젝트 종료일</form:label>
					<form:input path="projectFinishDate" readonly="true" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>주간 업무</legend>
			<div class="pure-g">
				<div class="pure-u-2-3">
					<form:label path="title" required="true">보고서 제목</form:label>
					<form:input path="title" cssClass="pure-input-1"
						placeholder="보고서 제목을 입력하세요." />
					<form:errors path="title" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="reportDate" required="true">보고 일자</form:label>
					<form:input path="reportDate" placeholder="보고 일자를 입력하세요." />
					<form:errors path="reportDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="reporter" />
					<form:label path="reporterName" required="true">작성자</form:label>
					<form:input path="reporterName" placeholder="작성자를 입력하세요." />
					<form:errors path="reporterName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="phase">진행 단계</form:label>
					<form:select path="phase" items="${phases}" />
					<form:errors path="phase" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>금주 실적</legend>
			<form:textarea path="thisWeek" cssClass="pure-input-1" />
			<form:errors path="thisWeek" cssClass="ui-state-error-text" />
		</fieldset>
		<fieldset>
			<legend>차주 실적</legend>
			<form:textarea path="nextWeek" cssClass="pure-input-1" />
			<form:errors path="nextWeek" cssClass="ui-state-error-text" />
		</fieldset>
		<c:if test="${projWeekMngFrmVO.isNew()}">
			<fieldset>
				<legend>첨부 파일</legend>
				<div class="pure-g">
					<div class="pure-u-1-2">
						<input name="files[0]" type="file" />
					</div>
					<div class="pure-u-1-2">
						<input name="files[1]" type="file" />
					</div>
				</div>
				<div class="pure-g">
					<div class="pure-u-1-2">
						<input name="files[2]" type="file" />
					</div>
					<div class="pure-u-1-2">
						<input name="files[3]" type="file" />
					</div>
				</div>
				<div class="pure-u-1">
					<input name="files[4]" type="file" />
				</div>
			</fieldset>
		</c:if>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
	</form:form>
	<c:if test="${not projWeekMngFrmVO.isNew()}">
		<br />
		<table id="jqg-atmt-table"></table>
		<div id="jqg-atmt-pager"></div>
		<br />
		<div class="pure-g">
			<div class="pure-u-1">
				<s:url value="${projWeekMngFrmVO.getAttachmentsUploadUrl()}"
					var="fileupload" />
				<form:form id="ajaxform" action="${fileupload}" method="POST"
					enctype="multipart/form-data">
					<input type="file" name="files[]" multiple>
					<input type="submit" value="업로드" />
				</form:form>
			</div>
		</div>
	</c:if>
</div>
<br />