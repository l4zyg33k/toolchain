<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initAutoComplete();
		initDateTimePicker();
		initCKEditor();
		if (eval('${riskMngFrmVO.isNew()}')) {
			initClentArrayButtons();
			initClentArrayJqGrid();
			initClentArrayNavGrid();
		} else {
			initRemoteButtons();
			initRemoteJqGrid();
			initRemoteNavGrid();
			initFileUpload();
			initAttachmentsJqGrid();
		}
		initButtons();
		initMaskedInputs();
	});

	function initButtons() {
		setSaveFormButtonFn('#btn-save', false, function() {
			if (eval('${riskMngFrmVO.isNew()}')) {
				$('#jqg-table_ilsave').click();
				var rowData = JSON.stringify($("#jqg-table").getRowData());
				$('#rowData').val(rowData);
			}
			$('#frm-riskmng').submit();
		});
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${riskMngFrmVO.getPrevUrl()}" />');

		$('.qms-content-buttonset').css('display', 'block');
	};

	function initAutoComplete() {
		applyProjectAutoComplete('#project');
	};

	function initDateTimePicker() {
		$('input[name$="Date"]').datepicker();
		$('input[name$="Date"]').mask('9999-99-99');
	}

	function initCKEditor() {
		$('textarea').ckeditor();
	}

	function initClentArrayButtons() {
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveButton('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	}

	function initClentArrayJqGrid() {
		$('#jqg-table').jqGrid({
			datastr : '${fn:replace(riskMngFrmVO.rowData, "\\n", "\\\\n")}',
			datatype : 'jsonstring',
			editurl : 'clientArray',
			colModel : [ {
				label : '변경일자',
				name : 'changedDate',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true
				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '상태',
				name : 'status',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${status}'
				},
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '변경내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '변경 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('#jqg-table textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});
	}

	function initClentArrayNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}

	function initRemoteButtons() {
		setDelButton('#btn-del', false, '#del_jqg-table');
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveButton('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	}

	function initRemoteJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${riskMngFrmVO.getDetailsUrl()}" />',
			editurl : '<s:url value="${riskMngFrmVO.getDetailsEditUrl()}" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '변경일자',
				name : 'changedDate',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true
				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '상태',
				name : 'status',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${status}'
				},
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '변경내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '변경 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('#jqg-table textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});

		$("#jqg-table").bind('jqGridInlineSuccessSaveRow', function() {
			$(this).jqGrid().trigger("reloadGrid");
		});
	}

	function initRemoteNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}

	function initFileUpload() {
		$('#ajaxform').ajaxForm({
			success : function(responseText, statusText) {
				$('#jqg-atmt-table').jqGrid().trigger("reloadGrid");
				$('#ajaxform').clearForm();
			}
		});
	}

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${riskMngFrmVO.getAttachmentsUrl()}" />';
		var editurl = '<s:url value="${riskMngFrmVO.getAttachmentsEditUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			editurl : editurl,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '생성자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '생성일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '삭제',
				align : 'center',
				width : 40,
				fixed : true,
				formatter : 'actions',
				formatoptions : {
					editbutton : false,
					delbutton : true
				}
			} ],
			pager : '#jqg-atmt-pager',
			caption : '첨부 목록'
		});
	}

	function initMaskedInputs() {
		$('#occurRate').mask('9?99');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">리스크 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${riskMngFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-riskmng" action="${action}"
		method="${riskMngFrmVO.getMethod()}" modelAttribute="riskMngFrmVO"
		enctype="multipart/form-data" cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>리스크 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-3-4">
					<form:label path="projectName" required="true">프로젝트 이름</form:label>
					<form:input path="projectName" placeholder="이름를 입력하세요."
						class="pure-input-1" readonly="${not riskMngFrmVO.isNew()}" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-2-3">
					<form:label path="title" required="true">리스크 제목</form:label>
					<form:input path="title" cssClass="pure-input-1"
						placeholder="제목을 입력하세요." />
					<form:errors path="title" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="occurDate" required="true">발생일</form:label>
					<form:input path="occurDate" placeholder="발생일" />
					<form:errors path="occurDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="projectIlName">담당자</form:label>
					<form:input path="projectIlName" readonly="true" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-5">
					<form:label path="category" required="true">범주 </form:label>
					<form:select path="category" items="${categories}" />
					<form:errors path="category" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-5">
					<form:label path="status" required="true">상태</form:label>
					<form:select path="status" items="${statuses}" />
					<form:errors path="status" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-5">
					<form:label path="inspect" required="true">영향도</form:label>
					<form:select path="inspect" items="${inspectes}" />
					<form:errors path="inspect" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-5">
					<form:label path="approchPlan" required="true">접근방안</form:label>
					<form:select path="approchPlan" items="${approchPlans}" />
					<form:errors path="approchPlan" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-5">
					<form:label path="occurRate">발생확률(%)</form:label>
					<form:input path="occurRate" />
					<form:errors path="occurRate" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>내용</legend>
			<form:textarea path="content" cssClass="pure-input-1" />
			<form:errors path="content" cssClass="ui-state-error-text" />
		</fieldset>
		<fieldset>
			<legend>영향</legend>
			<form:textarea path="impact" cssClass="pure-input-1" />
			<form:errors path="impact" cssClass="ui-state-error-text" />
		</fieldset>
		<fieldset>
			<legend>완화계획</legend>
			<form:textarea path="relaxPlan" cssClass="pure-input-1" />
			<form:errors path="relaxPlan" cssClass="ui-state-error-text" />
		</fieldset>
		<fieldset>
			<legend>비상대처방안</legend>
			<form:textarea path="emergencyPlan" cssClass="pure-input-1" />
			<form:errors path="emergencyPlan" cssClass="ui-state-error-text" />
		</fieldset>
		<c:if test="${riskMngFrmVO.isNew()}">
			<fieldset>
				<legend>첨부 파일</legend>
				<div class="pure-g">
					<div class="pure-u-1-2">
						<input name="files[0]" type="file" />
					</div>
					<div class="pure-u-1-2">
						<input name="files[1]" type="file" />
					</div>
				</div>
				<div class="pure-g">
					<div class="pure-u-1-2">
						<input name="files[2]" type="file" />
					</div>
					<div class="pure-u-1-2">
						<input name="files[3]" type="file" />
					</div>
				</div>
				<div class="pure-u-1">
					<input name="files[4]" type="file" />
				</div>
			</fieldset>
		</c:if>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="rowData" />
	</form:form>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<c:choose>
					<c:when test="${riskMngFrmVO.isNew()}">
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:when>
					<c:otherwise>
						<button id="btn-del">삭제</button>
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	<c:if test="${not riskMngFrmVO.isNew()}">
		<br />
		<table id="jqg-atmt-table"></table>
		<div id="jqg-atmt-pager"></div>
		<br />
		<div class="pure-g">
			<div class="pure-u-1">
				<s:url value="${riskMngFrmVO.getAttachmentsUploadUrl()}"
					var="fileupload"></s:url>
				<form:form id="ajaxform" action="${fileupload}" method="POST"
					enctype="multipart/form-data">
					<input type="file" name="files[]" multiple>
					<input type="submit" value="업로드" />
				</form:form>
			</div>
		</div>
	</c:if>
</div>
<br />