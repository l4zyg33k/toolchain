<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		initDateTimePicker();
	});

	function initButtons() {
		setSearchButtonFn('#btn-search', false, function(event) {
			$('#jqg-table').setGridParam({
				postData : {
					startDate : $('#startDate').val(),
					endDate : $('#endDate').val()
				}
			}).trigger('reloadGrid');
		});
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');

		$('.qms-content-buttonset').css('display', 'block');
	}

	//jqGrid 처리
	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/cmu/coworkmng" />',
			editurl : '<s:url value="/rest/cmu/coworkmng/edit" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '조직',
				name : 'organization.name',
				align : 'center',
				width : '7%'
			}, {
				label : '이름',
				name : 'name',
				align : 'center',
				width : '10%'
			}, {
				label : '직급',
				name : 'position.name',
				align : 'center',
				width : '6%'
			}, {
				label : '업체명',
				name : 'companyName',
				align : 'left',
				width : '17%'
			}, {
				label : '파트명',
				name : 'part.name',
				align : 'left',
				width : '15%'
			}, {
				label : '프로젝트명',
				name : 'project.name',
				align : 'left',
				width : '20%'
			}, {
				label : '투입일',
				name : 'startDate',
				align : 'center',
				width : '8%'
			}, {
				label : '철수일',
				name : 'endDate',
				align : 'center',
				width : '8%'
			}, {
				label : '상태',
				name : 'status.name',
				align : 'center',
				width : '9%'
			}, {
				name : 'editable',
				hidden : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'account.name',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '협력인원현황관리',
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					$('#view_jqg-table').click();
				}
			}
		});
	}

	//버튼별 처리 
	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {});
	}

	//수정
	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '<s:url value="/cmu/coworkmng/" />' + rowId
					+ '/form?page=' + page + '&rowNum=' + rowNum + '&rowId='
					+ rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}

	//등록
	function beforeInitDataAdd(formId, scope) {
		if (eval('${navGrid.add}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
			var url = '<s:url value="/cmu/coworkmng/form?page=" />' + page
					+ '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('등록 할 수 없습니다.');
			return false;
		}
	}

	//삭제
	function beforeInitDataDel(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}

	//date picker, timepicker 세팅
	function initDateTimePicker() {
		$('#startDate').datepicker();
		$('#startDate').mask('9999-99-99');
		$('#endDate').datepicker();
		$('#endDate').mask('9999-99-99');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">협력업체 인원 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form id="query-form" class="pure-form">
				<input id="startDate" type="text" class="pure-input-rounded"
					placeholder="투입일자" size="9"> ~ <input id="endDate"
					type="text" class="pure-input-rounded" placeholder="철수일자" size="9">
				<a id="btn-search">조회</a>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />