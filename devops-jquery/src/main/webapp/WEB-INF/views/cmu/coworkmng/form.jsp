<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initAutoComplete();
		initDateTimePicker();
		initMaskedInputs();
		setPartAndProjectType();

		//조직에 따라 입력 항목 설정
		$('#organization').change(function() {
			setPartAndProjectType();
		});

	});

	function initButtons() {
		setSaveFormButton('#btn-save', false, '#frm-cowork');
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${coWorkMngFrmVO.getPrevUrl()}" />');
	};

	//자동완성
	function initAutoComplete() {
		applyProjectAutoComplete('#project');

		$('#accountName').autocomplete({
			autoFocus : true,
			source : parseUrl('/rest/adm/acntmng/all'),
			select : function(event, ui) {
				$("#accountName").val(ui.item.name);
				$("#account").val(ui.item.username);
				$("#email").val(ui.item.email);
				return false;
			},
			change : function(event, ui) {
				if (ui.item == null) {
					$("#accountName").val('');
					$("#account").val('');
					$("#email").val('');
				}
			}
		}).data('ui-autocomplete')._renderItem = function(ul, item) {
			return $('<li>').append(
					'<a>' + item.name + ' (' + item.team + ')</a>')
					.appendTo(ul);
		};
	};

	//data picker, time picker 설정
	function initDateTimePicker() {
		$('input[name$="Date"]').datepicker();
		$('input[name$="Date"]').mask('9999-99-99');
	}

	//파트, 프로젝트 타입설정
	function setPartAndProjectType() {
		if ($("#organization :selected").text() == "프로젝트") {
			$("#part").attr("disabled", true);
			$("#project").attr("disabled", false);
			$("#projectName").attr("disabled", false);
			$("#part").val("");
		} else if ($("#organization :selected").text() == "운영") {
			$("#part").attr("disabled", false);
			$("#project").attr("disabled", true);
			$("#projectName").attr("disabled", true);
			$("#projectName").val("");
			$("#project").val("");
		}
	}

	function initMaskedInputs() {
		$('#phone').mask('999999999?99');
		$('#mobile').mask('9999999999?9');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">협력업체 인원 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${coWorkMngFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-cowork" action="${action}"
		method="${coWorkMngFrmVO.getMethod()}" modelAttribute="coWorkMngFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>외부 협력직원 기본정보 등록</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="organization" required="true">조직</form:label>
					<form:select path="organization" items="${organization}" />
					<form:errors path="organization" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="part" required="true">파트</form:label>
					<form:select path="part" items="${part}" />
					<form:errors path="part" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="projectName" required="true">프로젝트 이름</form:label>
					<form:input path="projectName" placeholder="이름를 입력하세요."
						class="pure-input-1" readonly="${not coWorkMngFrmVO.isNew()}" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">\
				<div class="pure-u-1-4">				
				<c:choose>
				    <c:when test="${coWorkMngFrmVO.isNew()}">
	                    <form:hidden path="account" />
	                    <form:label path="accountName" required="true">성명</form:label>
                        <form:input path="accountName" placeholder="이름을 입력하세요." />
                        <form:errors path="accountName" cssClass="ui-state-error-text" />                   	                    
				    </c:when>
				    <c:otherwise>
                        <form:hidden path="account" />
                        <form:input path="name" placeholder="이름을 입력하세요." />
                        <form:errors path="name" cssClass="ui-state-error-text" />                                           				    
				    </c:otherwise>
				</c:choose>
				</div>
				<div class="pure-u-1-4">
					<form:label path="position">직급</form:label>
					<form:select path="position" items="${position}" />
					<form:errors path="position" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="purpose">투입목적</form:label>
					<form:input path="purpose" class="pure-u-5-6"
						placeholder="투입목적을 입력하세요." />
					<form:errors path="purpose" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="phone">전화번호</form:label>
					<form:input path="phone" placeholder="전화번호를 입력하세요." maxlength='11'
						style='ime-mode:disabled;' />
					<form:errors path="phone" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="mobile">핸드폰</form:label>
					<form:input path="mobile" placeholder="핸드폰번호를 입력하세요."
						maxlength='11' style='ime-mode:disabled;' />
					<form:errors path="mobile" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="startDate" required="true">투입기간(시작)</form:label>
					<form:input path="startDate" placeholder="날짜를 입력하세요." />
					<form:errors path="startDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="endDate" required="true">투입기간(종료)</form:label>
					<form:input path="endDate" placeholder="날짜를 입력하세요." />
					<form:errors path="endDate" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form:label path="email">이메일</form:label>
					<form:input path="email" class="pure-u-5-6" readonly="readonly" />
					<form:errors path="email" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="companyName">회사명</form:label>
					<form:input path="companyName" class="pure-u-5-6"
						placeholder="회사명을 입력하세요." />
					<form:errors path="companyName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-3-4">
					<form:label path="address">주소</form:label>
					<form:input path="address" class="pure-u-11-12"
						placeholder="주소를 입력하세요." />
					<form:errors path="address" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="status" required="true">상태</form:label>
					<form:select path="status" items="${status}" />
					<form:errors path="status" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="indoor" required="true">상주여부</form:label>
					<form:select path="indoor" items="${indoor}" />
					<form:errors path="indoor" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="computer" required="true">PC제공</form:label>
					<form:select path="computer" items="${computer}" />
					<form:errors path="computer" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>보안관련정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="clientMemorandumStartDate">GS리테일 보안각서<br />징구일자(투입)</form:label>
					<form:input path="clientMemorandumStartDate"
						placeholder="날짜를 입력하세요." />
					<form:errors path="clientMemorandumStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="clientMemorandumEndDate">GS리테일 보안각서<br />징구일자(철수)</form:label>
					<form:input path="clientMemorandumEndDate" placeholder="날짜를 입력하세요." />
					<form:errors path="clientMemorandumEndDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="checklistStartDate">보안체크리스트<br />징구일자(투입)</form:label>
					<form:input path="checklistStartDate" placeholder="날짜를 입력하세요." />
					<form:errors path="checklistStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="checklistEndDate">보안체크리스트<br />징구일자(철수)</form:label>
					<form:input path="checklistEndDate" placeholder="날짜를 입력하세요." />
					<form:errors path="checklistEndDate" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="memorandum">LG CNS 보안각서<br />적용여부</form:label>
					<form:select path="memorandum">
						<form:option value="">선택</form:option>
						<form:options items="${memorandum}" />
					</form:select>
					<form:errors path="" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="memorandumStartDate">LG CNS 보안각서<br />징구일자(투입)</form:label>
					<form:input path="memorandumStartDate" placeholder="날짜를 입력하세요." />
					<form:errors path="memorandumStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="memorandumEndDate">LG CNS 보안각서<br />징구일자(철수)</form:label>
					<form:input path="memorandumEndDate" placeholder="날짜를 입력하세요." />
					<form:errors path="memorandumEndDate"
						cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="givedDate">출입카드 제공일자</form:label>
					<form:input path="givedDate" placeholder="날짜를 입력하세요." />
					<form:errors path="givedDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="callbackDate">출입카드 회수일자</form:label>
					<form:input path="callbackDate" placeholder="날짜를 입력하세요." />
					<form:errors path="callbackDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="returnedDate">출입카드 총무팀반환일자</form:label>
					<form:input path="returnedDate" placeholder="날짜를 입력하세요." />
					<form:errors path="returnedDate" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="orientationDate">보안교육일자</form:label>
					<form:input path="orientationDate" placeholder="날짜를 입력하세요." />
					<form:errors path="orientationDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="installedDate">보안Tool 설치일자</form:label>
					<form:input path="installedDate" placeholder="날짜를 입력하세요." />
					<form:errors path="installedDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="notes">비고</form:label>
					<form:input path="notes" class="pure-u-5-6"
						placeholder="비고를 입력하세요." />
					<form:errors path="notes" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<!-- TODO : 임으로 입력하게 추후 성명 공통이 되면 hidden 처리 할것   -->
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
	</form:form>
</div>
<br />