<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');

		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/cmu/acntmng" />',
			editurl : '<s:url value="/rest/cmu/acntmng/edit" />',
			colModel : [ {
				label : '아이디',
				name : 'username',
				align : 'center',
				key : true,
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				},
				align : 'center'
			}, {
				label : '이름',
				name : 'name',
				align : 'center',
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				},
				align : 'center'
			}, {
				label : '비밀번호(등록, 수정)',
				name : 'newPassword',
				hidden : true,
				editable : true,
				editrules : {
					edithidden : true,
					password : true
				},
				align : 'center'
			}, {
				label : '직급',
				name : 'position',
				index : 'position.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${positions}'
				},
				align : 'center'
			}, {
				label : '이메일',
				name : 'email',
				editable : true,
				editrules : {
					email : true
				},
				align : 'center'
			}, {
				name : 'editable',
				hidden : true
			} ],
			pager : '#jqg-pager',
			sortname : 'name',
			sortorder : 'asc',
			caption : '협력업체 계정 목록',
			loadComplete : function() {
				if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var id = $(this).jqGrid('getDataIDs')[0];
					$(this).setSelection(id);
				}
			},
			ondblClickRow : function(id) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					infomation('수정 할 수 없습니다.');
				}
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i> <span
			class="qms-content-title-text">협력업체 계정 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />