<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		$('#btn-login').button({
			icons : {
				primary : 'ui-icon-key'
			}
		});
	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-key fa-lg"></i><span class="qms-content-title-text">로그인</span>
	</div>
</div>
<div class="qms-content-main">
	<br />
	<c:url value="/login" var="action" />
	<form:form action='${action}' method="POST"
		class="pure-form pure-form-aligned">
		<fieldset>
			<div class="pure-control-group">
				<label for="username">아이디</label> <input id="username"
					name="username" type="text" />
			</div>
			<div class="pure-control-group">
				<label for="password">비밀번호</label> <input id="password"
					name="password" type="password" />
			</div>
			<div class="pure-controls">
				<input id="btn-login" type="submit" value="로그인" />
			</div>
		</fieldset>
	</form:form>
</div>