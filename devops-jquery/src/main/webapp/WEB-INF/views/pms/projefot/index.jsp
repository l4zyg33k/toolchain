<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		initSelect();
	});

	function initButtons() {
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/pms/projefot" />',
			editurl : '<s:url value="/rest/pms/projefot/edit" />',
			colModel : [ {
				name : 'id',
				hidden : true,
				key : true
			}, {
				label : '기간',
				name : 'week',
				align : 'center',
				width : 300,
				sortable : false
			}, {
				label : '공수 계획<br/>(M/H)',
				name : 'manHours',
				align : 'center',
				sortable : false
			}, {
				label : '착수',
				name : 'beginning',
				align : 'center',
				sortable : false
			}, {
				label : '분석',
				name : 'analysis',
				align : 'center',
				sortable : false
			}, {
				label : '설계',
				name : 'design',
				align : 'center',
				sortable : false
			}, {
				label : '개발',
				name : 'development',
				align : 'center',
				sortable : false
			}, {
				label : '테스트',
				name : 'test',
				align : 'center',
				sortable : false
			}, {
				label : '이행',
				name : 'execution',
				align : 'center',
				sortable : false
			}, {
				label : '교육',
				name : 'education',
				align : 'center',
				sortable : false
			}, {
				label : '기타<br/>(휴가)',
				name : 'etc',
				align : 'center',
				sortable : false
			}, {
				label : '실적 합계<br/>(M/H)',
				name : 'total',
				align : 'center',
				sortable : false
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '기간별 프로젝트 공수',
			serializeGridData : function(postData) {
				postData.project = $('#project').val();
				return postData;
			},
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				$('#edit_jqg-table').click();
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			del : false,
			view : false,
			search : false
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {}, {}, {}, {});
	}

	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		var url = '<s:url value="/pms/projefot/" />' + rowId + '/form?page='
				+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
		$(location).attr('href', url);
		return false;
	}

	function initSelect() {
		$('#project').change(function() {
			$('#refresh_jqg-table').click();
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 공수 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form pure-form-stacked">
				<form:label path="project">프로젝트</form:label>
				<form:select id="project" path="project" items="${projects}" />
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-edit">수정</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />