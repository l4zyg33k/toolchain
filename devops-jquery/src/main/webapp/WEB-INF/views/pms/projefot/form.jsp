<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		initSelect();
	});

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projEfotFrmVO.getPrevUrl()}" />');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/pms/projefot/result" />',
			editurl : '<s:url value="/rest/pms/projefot/result/edit" />',
			colModel : [ {
				name : 'id',
				hidden : true,
				key : true
			}, {
				label : '작업일자',
				name : 'workDate',
				align : 'center',
				width : 300,
				editable : true,
				editrules : {
					required : true,
					date : true
				},
				editoptions : {
					dataInit : function(elem) {
						$(elem).datepicker()
					}
				},
				sortable : false
			}, {
				label : '착수',
				name : 'beginning',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '분석',
				name : 'analysis',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '설계',
				name : 'design',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '개발',
				name : 'development',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '테스트',
				name : 'test',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '이행',
				name : 'execution',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '교육',
				name : 'education',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			}, {
				label : '기타<br/>(휴가)',
				name : 'etc',
				align : 'center',
				editable : true,
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).mask('9?9')
					}
				},
				sortable : false
			} ],
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '일자별 프로젝트 공수',
			serializeGridData : function(postData) {
				postData.plan = $('#plan').val();
				postData.type = $('#type').val();
				return postData;
			},
			ondblClickRow : function(rowId) {
				$('#edit_jqg-table').click();
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : false,
			search : false
		}, {
			beforeShowForm : function(formid) {
				$('#workDate').prop('disabled', true);
				$('#workDate', formid).attr('readonly', 'readonly');
			},
			serializeEditData : function(postData) {
				postData.plan = $('#plan').val();
				postData.type = $('#type').val();
				return postData;
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			beforeShowForm : function(formid) {
				$('#workDate').prop('disabled', false);
				$('#workDate', formid).removeAttr('readonly');
			},
			serializeEditData : function(postData) {
				postData.plan = $('#plan').val();
				postData.type = $('#type').val();
				return postData;
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			serializeDelData : function(postData) {
				postData.plan = $('#plan').val();
				postData.type = $('#type').val();
				return postData;
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {}, {});
	}

	function initSelect() {
		$('#plan').change(function() {
			$('#refresh_jqg-table').click();
		});
		$('#type').change(function() {
			$('#refresh_jqg-table').click();
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 공수 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form pure-form-stacked">
						<form:label path="projEfotFrmVO.plan">수행 기간</form:label>
						<form:select id="plan" path="projEfotFrmVO.plan"
							items="${projEfotFrmVO.plans}" />
					</form>
				</div>
				<div class="pure-u-1-2">
					<form class="pure-form pure-form-stacked">
						<form:label path="type">작업 유형</form:label>
						<form:select id="type" path="type" items="${types}" />
					</form>
				</div>
			</div>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />