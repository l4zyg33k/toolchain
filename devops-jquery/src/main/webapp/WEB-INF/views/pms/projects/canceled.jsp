<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
    $(function() {
	    $("#office").change(
		    	function() {
	            	$("#jqg-table").setGridParam({
	                	postData: {
	                    	query: $("#office").val()
	                	} 
	            	}).trigger("reloadGrid");
		    	}); 
		$("#btn-view").button({
			icons : {
				primary : 'ui-icon-search'
			}
		});		    
        $("#jqg-table").jqGrid({
            url: '<s:url value="/rest/pms/projects/canceled" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['사업부', '프로젝트\n코드','프로젝트명','PM','IL','패널시작일','패널완료일','패널예정일','패널시간'],
            colModel: [{
                    name: 'office.name',
                    align: 'center',
					width : 40
                }, {
                    name: 'code',
                    key: true,
                    align: 'center',
					width : 30	
                }, {
                    name: 'name',
					width : 130	
                }, {
                    name: 'pmName',
                    align: 'center',
					width : 30		
                }, {
                    name: 'il.name',
                    align: 'center',
					width : 30		
                }, {
                    name: 'panelStartDate',
                    align: 'center',
					width : 40		                    
                }, {
                    name: 'panelFinishDate',
                    align: 'center',
					width : 40		
                }, {
                    name: 'panelActualDate',
                    align: 'center',
					width : 40		
                }, {
                    name: 'panelActualTime',
                    align: 'center',
					formatter:"date",
					formatoptions: { srcformat: 'H:i:s', newformat: 'H:i' },
					width : 30					
                }],
            pager: '#jqg-pager',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'code',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '프로젝트 취소 현황',
			loadComplete : function() {
				$("#btn-view").button("option", "disabled", true);
			},
			onSelectRow : function(id) {
				var gubun = "Canceled";
				$('#btn-view').button('option', 'disabled', false);
				$('#btn-view').prop('href',	'<c:url value="/pms/projects/" />' + id + '/view/' + gubun);
			},
            ondblClickRow: function(id) {
				var gubun = "Canceled";		            	
                var url = parseUrl('/pms/projects/' + id + '/view/' + gubun);
                $(location).attr('href', url);
            }   			
        });
        
        $("#jqg-table").jqGrid('navGrid','#jqg-pager', navigator_options, {}, {}, {}, search_options);        
    });
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 취소 현황</span>    
    </div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
				<select id="office">
						<option value="">--- 전체 ---</option>
					<c:forEach var="item" items="${offices}">
						<option value="<c:out value="${item.code}"/>"><c:out value="${item.name}"/></option>
					</c:forEach>
				</select>
            </form>
        </div>	
        <div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-view" >보기</a>			
			</div>
		</div>         
	</div>
        <br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>	
</div>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">프로젝트 관리</span>
	<span id="sidebar-menu">프로젝트 취소 현황</span>
</div>
 -->
<br />