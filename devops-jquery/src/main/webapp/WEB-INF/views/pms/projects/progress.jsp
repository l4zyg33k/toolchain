<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
    $(function() {
	    $("#office").change(
		    	function() {
	            	$("#jqg-table").setGridParam({
	                	postData: {
	                    	query: $("#office").val()
	                	} 
	            	}).trigger("reloadGrid");
		    	});    	
		$("#btn-view").button({
			icons : {
				primary : 'ui-icon-search'
			}
		});    	
        $("#jqg-table").jqGrid({
        	
            url: '<s:url value="/rest/pms/projects/progress" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['사업부', '프로젝트<br>코드','프로젝트명','PM','IL','프로젝트<br>시작일','프로젝트<br>F/U종료일','단계','ST1','ST2','ST3','ST4','ST5'],
            colModel: [{
                    name: 'project.office.name',
                    jsonmap: 'projectWeeklyReport.project.office.name',
                    align: 'left',
					width : 50
                }, {
                    name: 'project.code',
                    jsonmap: 'projectWeeklyReport.project.code',
                    key: true,
                    align: 'center',
					width : 45	
                }, {
                    name: 'project.name',
                    jsonmap: 'projectWeeklyReport.project.name',
                    align: 'left',
                    width : 80
                }, {
                    name: 'project.pmName',
                    jsonmap: 'projectWeeklyReport.project.pmName',
                    align: 'center',
					width : 30
                }, {
                    name: 'project.il.name',
                    jsonmap: 'projectWeeklyReport.project.il.name',
                    align: 'center',
					width : 30
                }, {
                    name: 'project.startDate',
                    jsonmap: 'projectWeeklyReport.project.startDate',
                    align: 'center',
					width : 45                    
                }, {
                    name: 'project.followUpFinishDate',
                    jsonmap: 'projectWeeklyReport.project.startDate',
                    align: 'center',
					width : 45
                }, {
                    name: 'phase.name',
                    jsonmap: 'projectWeeklyReport.phase.name',
                    align: 'center',
					width : 45                     
                },{	
                	name: 'openingDate',
                	jsonmap: 'committees',
                    align: 'center',
					width : 45,
                    sortable: false,
                    formatter: function (cellvalue, options, rowObject) {
                    	if (cellvalue != null && cellvalue !="") {
	                    	var code = cellvalue[0].id;
	                    	var url  = "<c:url value='/pms/committees/"+code+"/view'/>"            	
	                     	return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">' + cellvalue[0].openingDate + '</font></a>';
                    	} else {
                    		return '';
                    	}
                    }
                },{	
                	name: 'openingDate',
                	jsonmap: 'committees',
                    align: 'center',
					width : 45,
                    sortable: false,
                    formatter: function (cellvalue) {
                    	if (cellvalue.length > 1) {
                    		var code = cellvalue[1].id;
                        	var url  = "<c:url value='/pms/committees/"+code+"/view'/>"            	
                         	return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">' + cellvalue[1].openingDate + '</font></a>';
                    	}else {
                            return "";
                    	}
                    }
                },{	
                	name: 'openingDate',
                	jsonmap: 'committees',
                    align: 'center',
					width : 45,
                    sortable: false,
                    formatter: function (cellvalue) {
                    	if (cellvalue.length > 2) {
                    		var code = cellvalue[2].id;
                        	var url  = "<c:url value='/pms/committees/"+code+"/view'/>"            	
                         	return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">' + cellvalue[2].openingDate + '</font></a>';
                    	}else {
                            return "";
                    	}
                    }
                },{	
                	name: 'openingDate',
                	jsonmap: 'committees',
                    align: 'center',
					width : 45,
                    sortable: false,
                    formatter: function (cellvalue) {
                    	if (cellvalue.length > 3) {
                    		var code = cellvalue[3].id;
                        	var url  = "<c:url value='/pms/committees/"+code+"/view'/>"            	
                         	return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">' + cellvalue[3].openingDate + '</font></a>';
                    	}else {
                            return "";
                    	}
                    }
                },{	
                	name: 'openingDate',
                	jsonmap: 'committees',
                    align: 'center',
					width : 45,
                    sortable: false,
                    formatter: function (cellvalue) {
                    	if (cellvalue.length > 4) {
                    		var code = cellvalue[4].id;
                        	var url  = "<c:url value='/pms/committees/"+code+"/view'/>"            	
                         	return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">' + cellvalue[4].openingDate + '</font></a>';
                    	}else {
                            return "";
                    	}
                    }
                }]
                ,
            pager: '#jqg-pager',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'code',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '프로젝트 진행 현황',
			loadComplete : function() {
				$("#btn-view").button("option", "disabled", true);
			},
			onSelectRow : function(id) {
				var gubun = "Progress";
				$('#btn-view').button('option', 'disabled', false);
				$('#btn-view').prop('href',	'<c:url value="/pms/projects/" />' + id + '/view/' + gubun);
			},
            ondblClickRow: function(id) {
				var gubun = "Progress";		            	
                var url = parseUrl('/pms/projects/' + id + '/view/' + gubun);
                $(location).attr('href', url);
            } 
        });
        
        $("#jqg-table").jqGrid('navGrid','#jqg-pager', navigator_options, {}, {}, {}, search_options);        
    });
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 진행 현황</span>    
    </div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
				<select id="office">
						<option value="">--- 전체 ---</option>
					<c:forEach var="item" items="${offices}">
						<option value="<c:out value="${item.code}"/>"><c:out value="${item.name}"/></option>
					</c:forEach>
				</select>
            </form>
        </div>	
        <div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-view" >보기</a>			
			</div>
		</div>        
	</div>
        <br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>	
</div>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">프로젝트 관리</span>
	<span id="sidebar-menu">프로젝트 진행 현황</span>
</div>
 -->
<br />