<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setViewButton('#btn-view', false, '#view_jqg-table');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/pms/projpanl" />',
			colModel : [ {
				label : '사업부',
				name : 'office.name',
				align : 'center',
				width : 110,
				fixed : true
			}, {
				label : '프로젝트<br />코드',
				name : 'code',
				key : true,
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '프로젝트 명칭',
				name : 'name',
				width : 240,
				fixed : true
			}, {
				label : 'PM 이름',
				name : 'pmName',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : 'IL 이름',
				name : 'il.name',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '준비 시작',
				name : 'panelStartDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '준비 완료',
				name : 'panelFinishDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '회의 일자',
				name : 'panelActualDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '회의 시각',
				name : 'panelActualTime',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '회의 장소',
				name : 'location.name',
				align : 'center',
				width : 100,
				fixed : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'code',
			sortorder : 'desc',
			caption : '패널회의 준비 목록',
			shrinkToFit : false,
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				$('#view_jqg-table').click();
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			edit : false,
			add : false,
			del : false,
			view : true
		}, {}, {}, {}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});
	}

	function beforeInitDataView(formId, scope) {
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var url = '<s:url value="/pms/projpanl/" />' + rowId + '/view?page='
				+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
		$(location).attr('href', url);
		return false;
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">패널회의 준비 현황</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-view">보기</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />