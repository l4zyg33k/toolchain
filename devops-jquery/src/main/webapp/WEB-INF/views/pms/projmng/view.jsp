<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initForm();
		initCKEditor();
		initFileUpload();
		initAttachmentsJqGrid();		
	});

	function initButtons() {
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projMngFrmVO.getPrevUrl()}" />');
	};

	function initForm() {
		$('#frm-project :input').attr('readonly', 'readonly');
		$('#frm-project :input').css({
			'background' : 'white',
			'color' : 'black'
		});
	}

	function initCKEditor() {
		$('#recommandation').attr('disabled', 'disabled');
		$('#recommandation').ckeditor({
			toolbar : [ {
				name : 'tools',
				items : [ 'Maximize', '-', 'About' ]
			} ]
		});
	}
	
	function initFileUpload() {
		$('#ajaxform').ajaxForm({
			success : function(responseText, statusText) {
				$('#jqg-atmt-table').jqGrid().trigger("reloadGrid");
				$('#ajaxform').clearForm();
			}
		});
	}

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${projMngFrmVO.getAttachmentsUrl()}" />';
		var editurl = '<s:url value="${projMngFrmVO.getAttachmentsEditUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			editurl : editurl,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '등록자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '등록일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '삭제',
				align : 'center',
				width : 40,
				fixed : true,
				formatter : 'actions',
				formatoptions : {
					editbutton : false,
					delbutton : true
				}
			} ],
			pager : '#jqg-atmt-pager',
			caption : '첨부 목록'	
		});
		
		$("#jqg-atmt-table").jqGrid('navGrid', '#jqg-atmt-pager', {
			add : false, edit : false, search : false, del : false
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}			
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}			
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		});		
	}	
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<form:form id="frm-project" modelAttribute="projMngFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>프로젝트 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="code">프로젝트 코드</form:label>
					<form:input path="code" />
				</div>
				<div class="pure-u-3-4">
					<form:label path="name">프로젝트 명</form:label>
					<form:input path="name" cssClass="pure-input-2-3" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="officeName">추진 사업부</form:label>
					<form:input path="officeName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="blDepartment">추진 부서</form:label>
					<form:input path="blDepartment" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="pmName">프로젝트 관리자</form:label>
					<form:input path="pmName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="blName">비지니스 리더</form:label>
					<form:input path="blName" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="typeName">프로젝트 유형</form:label>
					<form:input path="typeName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="ilName">정보기술 리더</form:label>
					<form:input path="ilName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="opName">운영 담당자</form:label>
					<form:input path="opName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="qaName">품질 담당자</form:label>
					<form:input path="qaName" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>패널회의</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="panelStartDate">준비 시작일자</form:label>
					<form:input path="panelStartDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelFinishDate">준비 종료일자</form:label>
					<form:input path="panelFinishDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelActualDate">회의일자</form:label>
					<form:input path="panelActualDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelActualTime">회의시각</form:label>
					<form:input path="panelActualTime" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="locationName">회의장소</form:label>
					<form:input path="locationName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="passName">통과여부</form:label>
					<form:input path="passName" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>권고사항</legend>			
			<div class="pure-g">
				<div class="pure-u-1">
					<form:textarea path="recommandation" cssClass="pure-input-1" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>프로젝트 기간</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="startDate">시작일자</form:label>
					<form:input path="startDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="finishDate">종료일자</form:label>
					<form:input path="finishDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualStartDate">실제 시작일자</form:label>
					<form:input path="actualStartDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualFinishDate">실제 종료일자</form:label>
					<form:input path="actualFinishDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpStartDate">인수인계 시작일자</form:label>
					<form:input path="followUpStartDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpFinishDate">인수인계 종료일자</form:label>
					<form:input path="followUpFinishDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpActualStartDate">실제 인수인계 시작일자</form:label>
					<form:input path="followUpActualStartDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpActualFinishDate">실제 인수인계 종료일자</form:label>
					<form:input path="followUpActualFinishDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openActualDate">오픈일자</form:label>
					<form:input path="openActualDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openActualDate">오픈일자</form:label>
					<form:input path="openActualDate" />
				</div>					
			</div>
		</fieldset>
		<fieldset>
			<legend>프로젝트 투입 계획</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="pdEffort">총 M/M</form:label>
					<form:input path="pdEffort" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingDay">1달 근무기준 일수</form:label>
					<form:input path="workingDay" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingHour">1일 근무기준 시간</form:label>
					<form:input path="workingHour" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingEffort">총 M/H</form:label>
					<form:input path="workingEffort" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>종료보고 현황</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="closeReportDate">종료보고 일자</form:label>
					<form:input path="closeReportDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="closeReportTypeName">종료보고 형식</form:label>
					<form:input path="closeReportTypeName" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>개발진행 현황</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="openCheckDate">오픈점검 일자</form:label>
					<form:input path="openCheckDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openReportDate">운영보고 일자</form:label>
					<form:input path="openReportDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="phaseName">패널 단계</form:label>
					<form:input path="phaseName" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="watching">현황판 표시</form:label>
					<form:checkbox path="watching" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="privacy">개인정보 취급</form:label>
					<form:checkbox path="privacy" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="critical">핵심 업무</form:label>
					<form:checkbox path="critical" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="statusName">진행 단계</form:label>
					<form:input path="statusName" />
				</div>									
			</div>
		</fieldset>
	</form:form>
	<br />
	<table id="jqg-atmt-table"></table>
	<div id="jqg-atmt-pager"></div>
	<br />
	<div class="pure-g">
		<div class="pure-u-1">
			<s:url value="${projMngFrmVO.getAttachmentsUploadUrl()}"
				var="fileupload"></s:url>
			<form:form id="ajaxform" action="${fileupload}" method="POST"
				enctype="multipart/form-data">
				<input type="file" name="files[]" multiple>
				<input type="submit" value="업로드" />
			</form:form>
		</div>
	</div>	
</div>
<br />