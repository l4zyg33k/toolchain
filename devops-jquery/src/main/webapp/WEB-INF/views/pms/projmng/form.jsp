<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initAutoComplete();
		initDateTimePicker();
		initCKEditor();
		initInputFocus();
		initNumericInputs();
		initFileUpload();
		initAttachmentsJqGrid();
	});

	function initButtons() {
		setSaveFormButton('#btn-save', false, '#frm-project');
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projMngFrmVO.getPrevUrl()}" />');
	};

	function initAutoComplete() {
		applyAccountAutoComplete("#il");
		applyAccountAutoComplete("#op");
		applyAccountAutoComplete("#qa");
	};

	function initDateTimePicker() {
		$('input[name$="Date"]').datepicker();
		$('input[name$="Date"]').mask('9999-99-99');

		$('input[name$="Time"]').timepicker();
	}

	function initCKEditor() {
		$('textarea#recommandation').ckeditor();
	}

	function initInputFocus() {
		$('#pdEffort').focusout(setWorkingEffort);
		$('#workingDay').focusout(setWorkingEffort);
		$('#workingHour').focusout(setWorkingEffort);
	}

	function setWorkingEffort() {
		$('#workingEffort').val(calcEffort());
	};

	function calcEffort() {
		var result = Math.round($('#pdEffort').val() * $('#workingDay').val()
				* $('#workingHour').val() * 100) / 100;
		return isNaN(result) ? 0 : result;
	};

	function initNumericInputs() {
		$('#pdEffort').numeric();
		$('#workingHour').numeric();
		$('#workingDay').numeric();
	}
	
	function initFileUpload() {
		$('#ajaxform').ajaxForm({
			success : function(responseText, statusText) {
				$('#jqg-atmt-table').jqGrid().trigger("reloadGrid");
				$('#ajaxform').clearForm();
			}
		});
	}

	function initAttachmentsJqGrid() {
		var url = '<s:url value="${projMngFrmVO.getAttachmentsUrl()}" />';
		var editurl = '<s:url value="${projMngFrmVO.getAttachmentsEditUrl()}" />';
		$('#jqg-atmt-table').jqGrid({
			url : url,
			editurl : editurl,
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '파일 이름',
				name : 'fileName',
				formatter : 'showlink',
				formatoptions : {
					baseLinkUrl : url
				}
			}, {
				label : '파일 크기',
				name : 'fileSize',
				formatter : 'number',
				formatoptions : {
					decimalPlaces : 0
				},
				align : 'right',
				width : 90,
				fixed : true
			}, {
				label : '생성자',
				name : 'createdBy.name',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '생성일',
				name : 'createdDate',
				align : 'center',
				width : 90,
				fixed : true
			}, {
				label : '삭제',
				align : 'center',
				width : 40,
				fixed : true,
				formatter : 'actions',
				formatoptions : {
					editbutton : false,
					delbutton : true
				}
			} ],
			pager : '#jqg-atmt-pager',
			caption : '첨부 목록'
		});
	}	
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${projMngFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-project" action="${action}"
		method="${projMngFrmVO.getMethod()}" modelAttribute="projMngFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>프로젝트 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="code">프로젝트 코드</form:label>
					<form:input path="code" readonly="true" />
					<form:errors path="code" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-3-4">
					<form:label path="name" required="true">프로젝트 명</form:label>
					<form:input path="name" cssClass="pure-input-2-3" />
					<form:errors path="name" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="office" required="true">추진 사업부</form:label>
					<c:choose>
						<c:when test="${projMngFrmVO.isNew()}">
							<form:select path="office" items="${offices}" />
							<form:errors path="office" cssClass="ui-state-error-text" />
						</c:when>
						<c:otherwise>
							<form:hidden path="office" />
							<form:input path="officeName" readonly="true" />
						</c:otherwise>
					</c:choose>
				</div>
				<div class="pure-u-1-4">
					<form:label path="blDepartment" required="true">추진 부서</form:label>
					<form:input path="blDepartment" placeholder="부서를 입력하세요." />
					<form:errors path="blDepartment" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="pmName" required="true">프로젝트 관리자</form:label>
					<form:input path="pmName" placeholder="이름을 입력하세요." />
					<form:errors path="pmName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="blName" required="true">비지니스 리더</form:label>
					<form:input path="blName" placeholder="이름을 입력하세요." />
					<form:errors path="blName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="type" required="true">프로젝트 유형</form:label>
					<form:select path="type" items="${types}" />
					<form:errors path="type" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="il" />
					<form:label path="ilName" required="true">정보기술 리더</form:label>
					<form:input path="ilName" placeholder="이름을 입력하세요." />
					<form:errors path="ilName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="op" />
					<form:label path="opName" required="true">운영 담당자</form:label>
					<form:input path="opName" placeholder="이름을 입력하세요." />
					<form:errors path="opName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="qa" />
					<form:label path="qaName" required="true">품질 담당자</form:label>
					<form:input path="qaName" placeholder="이름을 입력하세요." />
					<form:errors path="qaName" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>패널회의</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="panelStartDate">준비 시작일자</form:label>
					<form:input path="panelStartDate" placeholder="일자를 선택하세요." />
					<form:errors path="panelStartDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelFinishDate">준비 종료일자</form:label>
					<form:input path="panelFinishDate" placeholder="일자를 선택하세요." />
					<form:errors path="panelFinishDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelActualDate">회의일자</form:label>
					<form:input path="panelActualDate" placeholder="일자를 선택하세요." />
					<form:errors path="panelActualDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="panelActualTime">회의시각</form:label>
					<form:input path="panelActualTime" placeholder="시간을 선택하세요." />
					<form:errors path="panelActualTime" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="location">회의장소</form:label>
					<form:select path="location" items="${locations}" />
					<form:errors path="location" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="pass">통과여부</form:label>
					<form:select path="pass" items="${passes}" />
					<form:errors path="pass" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>권고사항</legend>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:textarea path="recommandation" cssClass="pure-input-1" />
					<form:errors path="recommandation" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>프로젝트 기간</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="startDate">시작일자</form:label>
					<form:input path="startDate" placeholder="일자를 선택하세요." />
					<form:errors path="startDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="finishDate">종료일자</form:label>
					<form:input path="finishDate" placeholder="일자를 선택하세요." />
					<form:errors path="finishDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualStartDate">실제 시작일자</form:label>
					<form:input path="actualStartDate" placeholder="일자를 선택하세요." />
					<form:errors path="actualStartDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualFinishDate">실제 종료일자</form:label>
					<form:input path="actualFinishDate" placeholder="일자를 선택하세요." />
					<form:errors path="actualFinishDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpStartDate">인수인계 시작일자</form:label>
					<form:input path="followUpStartDate" placeholder="일자를 선택하세요." />
					<form:errors path="followUpStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpFinishDate">인수인계 종료일자</form:label>
					<form:input path="followUpFinishDate" placeholder="일자를 선택하세요." />
					<form:errors path="followUpFinishDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpActualStartDate">실제 인수인계 시작일자</form:label>
					<form:input path="followUpActualStartDate" placeholder="일자를 선택하세요." />
					<form:errors path="followUpActualStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="followUpActualFinishDate">실제 인수인계 종료일자</form:label>
					<form:input path="followUpActualFinishDate"
						placeholder="일자를 선택하세요." />
					<form:errors path="followUpActualFinishDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openActualDate">오픈일자</form:label>
					<form:input path="openActualDate" placeholder="일자를 선택하세요." />
					<form:errors path="followUpActualStartDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openActualDate">오픈일자</form:label>
					<form:input path="openActualDate"
						placeholder="일자를 선택하세요." />
					<form:errors path="followUpActualFinishDate"
						cssClass="ui-state-error-text" />
				</div>				
			</div>
		</fieldset>
		<fieldset>
			<legend>프로젝트 투입 계획</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="pdEffort">총 M/M</form:label>
					<form:input path="pdEffort" />
					<form:errors path="pdEffort" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingDay">1달 근무기준 일수</form:label>
					<form:input path="workingDay" />
					<form:errors path="workingDay" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingHour">1일 근무기준 시간</form:label>
					<form:input path="workingHour" />
					<form:errors path="workingHour" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="workingEffort">총 M/H</form:label>
					<form:input path="workingEffort" readonly="true" />
					<form:errors path="workingEffort" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>종료보고 현황</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="closeReportDate">종료보고 일자</form:label>
					<form:input path="closeReportDate" placeholder="일자를 선택하세요." />
					<form:errors path="closeReportDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="closeReportType">종료보고 형식</form:label>
					<form:select path="closeReportType" items="${closeReportTypes}" />
					<form:errors path="closeReportType" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>개발진행 현황</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="openCheckDate">오픈점검 일자</form:label>
					<form:input path="openCheckDate" placeholder="일자를 선택하세요." />
					<form:errors path="openCheckDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openReportDate">운영보고 일자</form:label>
					<form:input path="openReportDate" placeholder="일자를 선택하세요." />
					<form:errors path="openReportDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="phase">패널 단계</form:label>
					<form:select path="phase" items="${phases}" />
					<form:errors path="phase" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="watching">현황판 표시</form:label>
					<form:checkbox path="watching" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="privacy">개인정보 취급</form:label>
					<form:checkbox path="privacy" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="critical">핵심 업무</form:label>
					<form:checkbox path="critical" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="status">진행 단계</form:label>
					<form:select path="status" items="${statuses}" />
					<form:errors path="status" cssClass="ui-state-error-text" />
				</div>							
			</div>
		</fieldset>
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
	</form:form>
	<c:if test="${not projMngFrmVO.isNew()}">
		<br />
		<table id="jqg-atmt-table"></table>
		<div id="jqg-atmt-pager"></div>
		<br />
		<div class="pure-g">
			<div class="pure-u-1">
				<s:url value="${projMngFrmVO.getAttachmentsUploadUrl()}"
					var="fileupload"></s:url>
				<form:form id="ajaxform" action="${fileupload}" method="POST"
					enctype="multipart/form-data">
					<input type="file" name="files[]" multiple>
					<input type="submit" value="업로드" />
				</form:form>
			</div>
		</div>
	</c:if>			
</div>
<br />