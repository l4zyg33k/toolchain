<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/pms/takingovers" var="urlformselect" />
<link rel="stylesheet"
	href="<c:url value = "/plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css" />" />
<script type="text/javascript"
	src="http://bp.yahooapis.com/2.4.21/browserplus-min.js"></script>
<script type="text/javascript"
	src="<c:url value="/plupload/js/plupload.full.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/plupload/js/i18n/ko.js"/>"></script>
<script type="text/javascript">
	$(function() {

		$('#btn-upload')
				.button({ icons : { primary : 'ui-icon-arrowthickstop-1-n' } })
				.click(function(event) {
					$('#frm-takingOver')
							.submit();
				});

		$('#btn-back')
				.button({ icons : { primary : 'ui-icon-arrowreturnthick-1-w' } });

		$('#project\\.name')
				.autocomplete({ autoFocus : true, source : function(request, response) {
					request['search.search'] = true;
					request['search.field'] = 'name';
					request['search.value'] = request['term'];
					request['search.oper'] = 'cn';

					$
							.post('<s:url value="/rest/pms/projects" />', request, response);
				}, focus : function(event, ui) {
					$('#project\\.code').val(ui.item.code);
					$('#project\\.name').val(ui.item.name);
					return false;
				}, select : function(event, ui) {
					$('#project\\.code').val(ui.item.code);
					$('#project\\.name').val(ui.item.name);
					return false;
				} }).data('ui-autocomplete')._renderItem = function(ul, item) {
			return $('<li>')
					.append('<a>' + item.code + ', ' + item.name + '</a>')
					.appendTo(ul);
		};

		$("#uploader")
				.plupload({
				// General settings
				runtimes : 'html5,html4,flash,silverlight,browserplus,gears', url : '<c:url value="/pms/takingovers/upload"/>', max_file_size : '10mb', chunk_size : '1mb', unique_names : true,
				// Resize images on clientside if we can
				resize : { width : 320, height : 240, quality : 90 },
				// Specify what files to browse for
				filters : [ { title : "Image files", extensions : "jpg,jpeg,gif,png,tiff,bmp,psd" }, { title : "Zip files", extensions : "zip" }, { title : "Document files", extensions : "xls,xlsx,doc,docx,ppt,pptx,txt,hwp" }, { title : "Source files", extensions : "java,jsp,pc,c,h,asp,aspx,cs,php,phpx,ph,html,js,css" } ],
				// Flash settings
				flash_swf_url : '<c:url value="/plupload/js/plupload.flash.swf"/>',
				// Silverlight settings
				silverlight_xap_url : '<c:url value="/plupload/js/plupload.silverlight.xap"/>',
				multipart_params : {
					"project" : $('#project\\.code').val(),
					"system": $('#system').val()
				}
				});

		$(".plupload_start").detach();

		// Client side form validation
		$('#frm-takingOver')
				.submit(function(e) {

					var uploader = $('#uploader').plupload('getUploader');
					
					uploader.settings.multipart_params = {"project" : $('#project\\.code').val(),
							"system": $('#system').val()};

					var files_count = uploader.files.length;
					
					if (files_count<1) {
						//
					} else {
						uploader.bind('StateChanged', function() {
							if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
								uploader.settings.url += "?project=" + $('#project\\.code').val() + "&system" + $('#system').val();
								$('#frm-takingOver')[0].submit();
							}
						});
					}
					
					for(var i=0;i<files_count;i++) {
						uploader.start();
						i++;
					}
					
					return false;
				});
	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i>&nbsp;<span>운영문서등록</span>
	</div>
</div>
<div class="qms-content-buttonset">
	<c:if test="${buttons.btn_save}">
		<a id="btn-save">임시저장</a>
	</c:if>
	<a id="btn-upload">업로드</a>
	<a id="btn-back" href="<c:url value='/pms/takingovers' />">목록으로</a>
</div>
<div class="qms-content-main">
	<c:choose>
		<c:when test="${not empty id}">
			<!-- 임시저장 -->
			<c:set value="PUT" var="method" />
		</c:when>
		<c:otherwise>
			<!-- 등록 -->
			<s:url value="/pms/takingovers" var="urlsave" />
			<c:set value="POST" var="method" />
		</c:otherwise>
	</c:choose>

	<form:form id="frm-takingOver" action="${urlsave}" method="${method}"
		commandName="takingOverVO" cssClass="pure-form pure-form-stacked"
		enctype="multipart/form-data">
		<fieldset>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="project.name">프로젝트</form:label>
					<form:input path="project.name" placeholder="이름를 입력하세요."
						cssClass="pure-input-2-3" readonly="${not empty id}" />
					<form:hidden path="project.code" />
					<form:errors path="project.name" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="system.division">사업부</form:label>
					<c:choose>
						<c:when test="${not empty id}">
							<form:input path="system.division.name" readonly="true" />
							<form:hidden path="system.division.id" />
						</c:when>
						<c:otherwise>
							<form:select path="system.division" items="${divisions}"
								itemLabel="name" itemValue="id" />
						</c:otherwise>
					</c:choose>
				</div>
				<div class="pure-u-1-3">
					<form:label path="system">시스템</form:label>
					<c:choose>
						<c:when test="${not empty id}">
							<form:input path="system.name" readonly="true" />
							<form:hidden path="system.id" />
						</c:when>
						<c:otherwise>
							<form:select path="system" items="${systems}" itemLabel="name"
								itemValue="id" />
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<br/>
			<div id="uploader">
				<p>Your browser doesn't have Flash, Silverlight, Gears,
					BrowserPlus or HTML5 support.</p>
			</div>
		</fieldset>
	</form:form>
</div>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">프로젝트 관리</span> <span id="sidebar-menu">운영문서등록</span>
</div>
 -->
<br />