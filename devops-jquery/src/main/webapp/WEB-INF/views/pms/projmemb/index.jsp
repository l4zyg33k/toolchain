<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		initJqGrid2();
		initDateTimePicker();
		initDialog();
		initSelect();
	});

	//버튼 처리
	function initButtons() {
		//수정 세팅
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');

		//삭제버튼 세팅
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');

		//팀원추가 버튼 클릭시
		setSaveButtonFn('#btn-save', false, function(event) {
			$.post('<s:url value="/rest/pms/projmemb/save" />', {
				'project' : $('#project').val(),
				'members' : $('#sel_userNames').val(),
				'startDate' : $('#sel_startDate').val(),
				'endDate' : $('#sel_endDate').val(),
				'role' : $('#sel_role').val()
			}, function(data) {
				if (data.success) {
					$('#refresh_jqg-table').click();
					$('#dialog-form').dialog('close');
					$('#dialog-form-msg').text('');
				} else {
					$('#dialog-form-msg').text(data.message);
				}
			});
		});

		//선택사용자추가 클릭
		setSaveButtonFn('#btn-addSel', false, function(event) {
			if ($('#project').val() == '') {
				infomation('프로젝트를 조회하세요.');
				return;
			}

			var storedSel = $('#jqg-table2')
					.jqGrid('getGridParam', 'selarrrow');

			if (storedSel.length < 1) {
				infomation('사용자를 선택하세요. (다중 선택 가능)');
				return;
			}

			$('#sel_projectCode').val($('#project').val());
			$('#sel_userNames').val(storedSel);
			$('#dialog-form').dialog('open');
		});

		//선택사용자 팝업창 닫기
		setCancelButtonFn('#btn-close', false, function(event) {
			$('#dialog-form').dialog('close');
		});
	}

	//프로젝트 팀원 조회
	function initJqGrid() {
		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/pms/projmemb" />',
			editurl : '<s:url value="/rest/pms/projmemb/edit" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '프로젝트<br />코드',
				name : 'project.code',
				align : 'center',
				width : '9%',
				editable : true,
				edittype : 'text',
				search : false,
				editoptions : {
					readonly : true
				}
			}, {
				label : '프로젝트 명칭',
				name : 'project.name',
				align : 'left',
				width : '28%',
				editable : true,
				edittype : 'text',
				search : false,
				editoptions : {
					readonly : true
				}
			}, {
				label : '아이디',
				name : 'member.username',
				align : 'left',
				width : '19%',
				editable : true,
				edittype : 'text',
				editoptions : {
					readonly : true
				}
			}, {
				label : '이름',
				name : 'member.name',
				align : 'left',
				width : '10%',
				editable : true,
				edittype : 'text',
				editoptions : {
					readonly : true
				}
			}, {
				label : '역활',
				name : 'role',
				align : 'center',
				width : '14%',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${roles}'
				}
			}, {
				label : '투입일',
				name : 'startDate',
				align : 'center',
				width : '10%',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					date : true

				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				}
			}, {
				label : '철수일',
				name : 'endDate',
				align : 'center',
				width : '10%',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					date : true
				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				}
			}, {
				name : 'editable',
				hidden : true
			} ],
			pager : '#jqg-pager',
			rowNum : 10,
			sortname : 'project.code',
			sortorder : 'desc',
			caption : '프로젝트 팀원',
			serializeGridData : function(postData) {
				postData.project = $('#project').val();
				return postData;
			},
			loadComplete : function() {
				$('#refresh_jqg-table2').click();
			}
		});
	}

	function initNavGrid() {
		//네비그리드 세팅
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			edit : true,
			add : false,
			del : true,
			view : false
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {}, {});
	}

	//삭제
	function beforeInitDataDel(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}

	//수정
	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			return true;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}

	function initJqGrid2() {
		$('#jqg-table2').jqGrid({
			url : '<s:url value="/rest/pms/projmemb/accounts" />',
			colModel : [ {
				label : '아이디',
				name : 'username',
				align : 'center',
				key : true,
				editable : true,
				edittype : 'text',
				searchoptions : {
					sopt : [ 'eq', 'cn' ]
				}
			}, {
				label : '이름',
				name : 'name',
				align : 'center',
				editable : true,
				edittype : 'text',
				searchoptions : {
					sopt : [ 'eq', 'cn' ]
				}
			}, {
				label : '직급',
				name : 'position',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${positions}'
				},
				align : 'center'
			}, {
				label : '소속팀',
				name : 'team',
				align : 'center',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${teams}'
				}
			}, {
				label : '이메일',
				name : 'email',
				editable : true,
				edittype : 'text',
				searchoptions : {
					sopt : [ 'eq', 'cn' ]
				}
			} ],
			pager : '#jqg-pager2',
			rowNum : 10,
			rownumbers : true,
			sortname : 'name',
			sortorder : 'asc',
			caption : '사용자 목록',
			multiselect : true,
			serializeGridData : function(postData) {
				postData.project = $('#project').val();
				return postData;
			},
			loadComplete : function() {
			},
			onSelectRow : function(id) {
			}
		});

		$("#jqg-table2").jqGrid('navGrid', '#jqg-pager2', {
			edit : false,
			add : false,
			del : false,
			view : false
		}, {}, {}, {}, {}, {});
	}

	function initDateTimePicker() {
		//선택사용자 팝업창 date picker 세팅
		$("#sel_startDate").datepicker();
		$("#sel_startDate").mask('9999-99-99');
		$("#sel_endDate").datepicker();
		$("#sel_endDate").mask('9999-99-99');
	}

	function initDialog() {
		//사용자 선택추가의 투입기간 팝업등록
		$("#dialog-form").dialog({
			autoOpen : false,
			width : 360,
			modal : true
		});
	}

	function initSelect() {
		$('#project').change(function(event) {
			$('#refresh_jqg-table').click();
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 팀원관리</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form pure-form-stacked">
				<div class="pure-g">
					<div class="pure-u-2-3">
						<label for="project">프로젝트</label>${proj}
						<form:select id="project" path="project" items="${projects}" />
					</div>
				</div>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />
<br />
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-addSel">선택 사용자 추가</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table2"></table>
	<div id="jqg-pager2"></div>
</div>
<div id="dialog-form" class="qms-dialog" title="참여 일정">
	<form id="frm-addSel" class="pure-form pure-form-aligned">
		<fieldset>
			<div class="pure-control-group">
				<label for="sel_startDate">투입일</label> <input type="text"
					name="sel_startDate" id="sel_startDate" style="font-size: 0.7em;">
			</div>
			<div class="pure-control-group">
				<label for="sel_endDate">철수일</label> <input type="text"
					name="sel_endDate" id="sel_endDate" style="font-size: 0.7em;">
			</div>
			<div class="pure-control-group">
				<label for="sel_role">역활</label>
				<form:select id="sel_role" path="dlgRole" items="${dlgRoles}"
					style="font-size: 0.7em;" />
			</div>
			<div class="pure-control-group">
				<span id="dialog-form-msg" class="ui-state-error-text"></span>
			</div>
			<div class="pure-controls">
				<a id="btn-save">저장</a> <a id="btn-close">닫기</a>
			</div>
			<input type="hidden" id="sel_projectCode" name="sel_projectCode">
			<input type="hidden" id="sel_userNames" name="sel_userNames">
		</fieldset>
	</form>
</div>

<br />