<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initAutoComplete();
		initDateTimePicker();
		if (eval('${steeCommFrmVO.isNew()}')) {
			initClentArrayButtons();
			initClentArrayJqGrid();
			initClentArrayNavGrid();
		} else {
			initRemoteButtons();
			initRemoteJqGrid();
			initRemoteNavGrid();
		}
		initButtons();
	});

	function initButtons() {
		setSaveFormButtonFn('#btn-save', false, function() {
			if (eval('${steeCommFrmVO.isNew()}')) {
				$('#jqg-table_ilsave').click();
				var rowData = JSON.stringify($("#jqg-table").getRowData());
				$('#rowData').val(rowData);
			}
			$('#frm-steecomm').submit();
		});
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${steeCommFrmVO.getPrevUrl()}" />');

		$('.qms-content-buttonset').css('display', 'block');
	};

	function initAutoComplete() {
		applyProjectAutoComplete('#project');
	};

	function initDateTimePicker() {
		$('input[name$="Date"]').datepicker();
		$('input[name$="Date"]').mask('9999-99-99');

		$('input[name$="Time"]').timepicker();
	}

	function initClentArrayButtons() {
		$("#btn-iladd").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function() {
			$('#jqg-table_iladd').click();
		});

		$("#btn-iledit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(function() {
			$('#jqg-table_iledit').click();
		});

		$("#btn-ilsave").button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function() {
			$('#jqg-table_ilsave').click();
		});

		$("#btn-ilcancel").button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		}).click(function() {
			$('#jqg-table_ilcancel').click();
		});
	}

	function initClentArrayJqGrid() {
		$('#jqg-table').jqGrid({
			datastr : '${fn:replace(steeCommFrmVO.rowData, "\\n", "\\\\n")}',
			datatype : 'jsonstring',
			editurl : 'clientArray',
			colModel : [ {
				label : '안건',
				name : 'issue',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '회의 결과',
				name : 'outcome',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '결과 검토',
				name : 'review',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '안건 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});

	}

	function initClentArrayNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}

	function initRemoteButtons() {
		$("#btn-del").button({
			icons : {
				primary : 'ui-icon-trash'
			}
		}).click(function() {
			$('#del_jqg-table').click();
		});

		$("#btn-iladd").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function() {
			$('#jqg-table_iladd').click();
		});

		$("#btn-iledit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(function() {
			$('#jqg-table_iledit').click();
		});

		$("#btn-ilsave").button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function() {
			$('#jqg-table_ilsave').click();
		});

		$("#btn-ilcancel").button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		}).click(function() {
			$('#jqg-table_ilcancel').click();
		});
	}

	function initRemoteJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${steeCommFrmVO.getAgendasUrl()}" />',
			editurl : '<s:url value="${steeCommFrmVO.getAgendasEditUrl()}" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '안건',
				name : 'issue',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '회의 결과',
				name : 'outcome',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '결과 검토',
				name : 'review',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '안건 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});

		$("#jqg-table").bind('jqGridInlineSuccessSaveRow', function() {
			$(this).jqGrid().trigger("reloadGrid");
		});
	}

	function initRemoteNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">스티어링 커미티 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${steeCommFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-steecomm" action="${action}"
		method="${steeCommFrmVO.getMethod()}" modelAttribute="steeCommFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>스티어링 커미티 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="projectName" required="true">프로젝트 이름</form:label>
					<form:input path="projectName" placeholder="이름를 입력하세요."
						class="pure-input-1" readonly="${not steeCommFrmVO.isNew()}" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="location" required="true">회의장소</form:label>
					<form:select path="location" items="${locations}" />
					<form:errors path="location" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="title" required="true">제목</form:label>
					<form:input path="title" placeholder="이름를 입력하세요."
						class="pure-input-1" />
					<form:errors path="title" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="openingDate" required="true">회의일</form:label>
					<form:input path="openingDate" placeholder="일자를 선택하세요." />
					<form:errors path="openingDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualOpeningDate">실제 회의일</form:label>
					<form:input path="actualOpeningDate" placeholder="일자를 선택하세요." />
					<form:errors path="actualOpeningDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openingTime">회의시각</form:label>
					<form:input path="openingTime" placeholder="시간을 선택하세요." />
					<form:errors path="openingTime" cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="rowData" />
	</form:form>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<c:choose>
					<c:when test="${steeCommFrmVO.isNew()}">
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:when>
					<c:otherwise>
						<button id="btn-del">삭제</button>
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />