<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initForm();
	});

	function initButtons() {
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${steeCommFrmVO.getPrevUrl()}" />');

		$('.qms-content-buttonset').css('display', 'block');
	};

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${steeCommFrmVO.getAgendasUrl()}" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '안건',
				name : 'issue',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '회의 결과',
				name : 'outcome',
				editable : true,
				edittype : 'textarea'
			}, {
				label : '결과 검토',
				name : 'review',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '안건 목록',
			autoencode : false
		});
	}

	function initForm() {
		$('#frm-steecomm :input').attr('readonly', 'readonly');
		$('#frm-steecomm :input').css({
			'background' : 'white',
			'color' : 'black'
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">스티어링 커미티 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<form:form id="frm-steecomm" modelAttribute="steeCommFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>스티어링 커미티 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="projectName">프로젝트 이름</form:label>
					<form:input path="projectName" class="pure-input-1" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="locationName">회의장소</form:label>
					<form:input path="locationName" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="title">제목</form:label>
					<form:input path="title" class="pure-input-1" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="openingDate">회의일</form:label>
					<form:input path="openingDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="actualOpeningDate">실제 회의일</form:label>
					<form:input path="actualOpeningDate" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="openingTime">회의시각</form:label>
					<form:input path="openingTime" />
				</div>
			</div>
		</fieldset>
	</form:form>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />