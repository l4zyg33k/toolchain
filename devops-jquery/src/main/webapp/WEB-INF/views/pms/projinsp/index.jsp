<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		setViewButton('#btn-view', eval('${!navGrid.view}'), '#view_jqg-table');
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/pms/projinsp" />',
			editurl : '<s:url value="/rest/pms/projinsp/edit" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '프로젝트<br />코드',
				name : 'project.code',
				align : 'center',
				width : 70,
				fixed : true
			}, {
				label : '프로젝트 명',
				name : 'project.name'
			}, {
				label : '산출물 명칭',
				name : 'productName'
			}, {
				label : '산출물<br />타입',
				name : 'productType.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '검토<br />단계',
				name : 'productPhase.name',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '산출물<br /> 크기',
				name : 'productSize',
				align : 'center',
				width : 50,
				fixed : true
			}, {
				label : '검토일시',
				name : 'inspectionDate',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				name : 'editable',
				hidden : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'inspectionDate',
			sortorder : 'desc',
			caption : '프로젝트 인스펙션 목록',
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					$('#view_jqg-table').click();
				}
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});
	}

	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '<s:url value="/pms/projinsp/" />' + rowId
					+ '/form?page=' + page + '&rowNum=' + rowNum + '&rowId='
					+ rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataAdd(formId, scope) {
		if (eval('${navGrid.add}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow') || 0;
			var url = '<s:url value="/pms/projinsp/form?page=" />' + page
					+ '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('등록 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataDel(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataView(formId, scope) {
		if (eval('${navGrid.view}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow');
			var url = '<s:url value="/pms/projinsp/" />' + rowId
					+ '/view?page=' + page + '&rowNum=' + rowNum + '&rowId='
					+ rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('조회 할 수 없습니다.');
			return false;
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 인스펙션 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-view">보기</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />