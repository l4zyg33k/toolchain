<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initAutoComplete();
		initDateTimePicker();
		if (eval('${projInspFrmVO.isNew()}')) {
			initClentArrayButtons();
			initClentArrayJqGrid();
			initClentArrayNavGrid();
		} else {
			initRemoteButtons();
			initRemoteJqGrid();
			initRemoteNavGrid();
		}
		initButtons();
		initColPropDefectType();
		initProductPhases();
		initMaskedInputs();
	});

	function initButtons() {
		setSaveFormButtonFn('#btn-save', false, function() {
			if (eval('${projInspFrmVO.isNew()}')) {
				$('#jqg-table_ilsave').click();
				var rowData = JSON.stringify($("#jqg-table").getRowData());
				$('#rowData').val(rowData);
			}
			$('#frm-projinsp').submit();
		});
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projInspFrmVO.getPrevUrl()}" />');

		$('.qms-content-buttonset').css('display', 'block');
	};

	function initAutoComplete() {
		applyProjectAutoComplete('#project');
		applyAccountAutoComplete("#producer");
		applyAccountAutoComplete("#moderator");
	};

	function initDateTimePicker() {
		$('input[name$="Date"]').datepicker();
		$('input[name$="Date"]').mask('9999-99-99');
	}

	function initClentArrayButtons() {
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveButton('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	}

	function initClentArrayJqGrid() {
		$('#jqg-table').jqGrid({
			datastr : '${fn:replace(projInspFrmVO.rowData, "\\n", "\\\\n")}',
			datatype : 'jsonstring',
			editurl : 'clientArray',
			colModel : [ {
				label : '결함<br />구분',
				name : 'defectGrade',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${defectGrades}'
				},
				align : 'center',
				width : 60,
				fixed : true
			}, {
				label : '결함 유형',
				name : 'defectType',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {},
				align : 'center',
				width : 120,
				fixed : true
			}, {
				label : '결함 내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '결함 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('#productPhase').unbind('change');
			$('#productPhase').attr('readonly', true);
			$('#productPhase :selected').each(function() {
				$(this).parent().data("default", this);
			});
			$('#productPhase').change(function(e) {
				$($(this).data('default')).prop('selected', true);
			});
			$('textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});
	}

	function initClentArrayNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}

	function initRemoteButtons() {
		setDelButton('#btn-del', false, '#del_jqg-table');
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveButton('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	}

	function initRemoteJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${projInspFrmVO.getDetailsUrl()}" />',
			editurl : '<s:url value="${projInspFrmVO.getDetailsEditUrl()}" />',
			colModel : [ {
				label : '결함<br />구분',
				name : 'defectGrade',
				index : 'defectGrade.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${defectGrades}'
				},
				align : 'center',
				width : 60,
				fixed : true
			}, {
				label : '결함 유형',
				name : 'defectType',
				index : 'defectType.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {},
				align : 'center',
				width : 120,
				fixed : true
			}, {
				label : '결함 내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '안건 목록',
			autoencode : false
		});

		$("#jqg-table").bind('jqGridInlineEditRow', function() {
			$('textarea').ckeditor({
				toolbar : [ {
					name : 'basicstyles',
					items : [ 'Bold', 'Italic' ]
				}, {
					name : 'paragraph',
					items : [ 'NumberedList', 'BulletedList' ]
				}, {
					name : 'tools',
					items : [ 'Maximize', '-', 'About' ]
				} ]
			});
		});

		$("#jqg-table").bind('jqGridInlineSuccessSaveRow', function() {
			$(this).jqGrid().trigger("reloadGrid");
		});
	}

	function initRemoteNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}

	function initColPropDefectType() {
		var id = $('#productPhase').val();
		var url = '<s:url value="/rest/pms/projinsp/" />' + id + '/defecttypes';
		$.post(url, function(data) {
			$('#jqg-table').jqGrid('setColProp', 'defectType', {
				editoptions : {
					value : data
				}
			});
			$('#jqg-table').jqGrid().trigger("reloadGrid");
		});
	}

	function initProductPhases() {
		$('#productPhase').change(function() {
			initColPropDefectType();
		});
	}

	function initMaskedInputs() {
		$('#inspectionDate').mask('9999-99-99');
		$('#productSize').mask('9?99');
		$('#inspectors').mask('9?99');
		$('#inspectionPercent').mask('9?99');
		$('#inspectionMeetingTime').mask('9?99');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 인스펙션 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<s:url value="${projInspFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-projinsp" action="${action}"
		method="${projInspFrmVO.getMethod()}" modelAttribute="projInspFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>프로젝트 인스펙션 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="projectName" required="true">프로젝트 이름</form:label>
					<form:input path="projectName" placeholder="이름를 입력하세요."
						class="pure-input-2-3" readonly="${not projInspFrmVO.isNew()}" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="producer" />
					<form:label path="producerName" required="true">작성자</form:label>
					<form:input path="producerName" placeholder="작성자를 입력하세요." />
					<form:errors path="producerName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="inspectionDate" required="true">검토 일자</form:label>
					<form:input path="inspectionDate" placeholder="일자를 선택하세요."
						cssClass="pure-input-2-3" />
					<form:errors path="inspectionDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="inspectors">검토자 수</form:label>
					<form:input path="inspectors" placeholder="인원 수를 입력하세요."
						cssClass="pure-input-2-3" />
					<form:errors path="inspectors" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-3">
					<form:hidden path="moderator" />
					<form:label path="moderatorName" required="true">진행자</form:label>
					<form:input path="moderatorName" placeholder="진행자를 입력하세요."
						cssClass="pure-input-2-3" />
					<form:errors path="moderatorName" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-2-3">
					<form:label path="productName" required="true">산출물 명칭</form:label>
					<form:input path="productName" cssClass="pure-input-2-3"
						placeholder="산출물 명칭을 입력하세요." />
					<form:errors path="productName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="productType">산출물 구분</form:label>
					<form:select path="productType" items="${productTypes}" />
					<form:errors path="productType" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="productPhase">검토 단계</form:label>
					<c:choose>
						<c:when test="${projInspFrmVO.isNew()}">
							<form:select path="productPhase" items="${productPhases}" />
							<form:errors path="productPhase" cssClass="ui-state-error-text" />
						</c:when>
						<c:otherwise>
							<form:hidden path="productPhase" />
							<form:input path="productPhaseName" readonly="true" />
						</c:otherwise>
					</c:choose>

				</div>
				<div class="pure-u-1-3">
					<form:label path="productSize">산출물 크기(WW)</form:label>
					<form:input path="productSize" cssClass="pure-input-2-3"
						placeholder="산출물 크기(WW)를 입력하세요." />
					<form:errors path="productSize" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="inspectionPercent">검토 분량(%)</form:label>
					<form:input path="inspectionPercent" cssClass="pure-input-2-3"
						placeholder="검토 분량(%)을 입력하세요." />
					<form:errors path="inspectionPercent"
						cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="inspectionMeetingTime">미팅 시간</form:label>
					<form:input path="inspectionMeetingTime"
						placeholder="미팅 시간을 입력하세요." />
					<form:errors path="inspectionMeetingTime"
						cssClass="ui-state-error-text" />
				</div>
			</div>
		</fieldset>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="rowData" />
	</form:form>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-iladd">등록</button>
				<button id="btn-iledit">수정</button>
				<button id="btn-ilsave">저장</button>
				<button id="btn-ilcancel">취소</button>
				<c:if test="${not projInspFrmVO.isNew()}">
					<button id="btn-del">삭제</button>
				</c:if>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />