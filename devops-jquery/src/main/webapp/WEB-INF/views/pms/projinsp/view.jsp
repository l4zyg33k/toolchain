<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initJqGrid();
		initColPropDefectType();
		initButtons();
		initForm();
	});

	function initButtons() {
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projInspFrmVO.getPrevUrl()}" />');
		$('.qms-content-buttonset').css('display', 'block');
	};

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="${projInspFrmVO.getDetailsUrl()}" />',
			colModel : [ {
				label : '결함<br />구분',
				name : 'defectGrade',
				index : 'defectGrade.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${defectGrades}'
				},
				align : 'center',
				width : 60,
				fixed : true
			}, {
				label : '결함 유형',
				name : 'defectType',
				index : 'defectType.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {},
				align : 'center',
				width : 120,
				fixed : true
			}, {
				label : '결함 내용',
				name : 'content',
				editable : true,
				edittype : 'textarea'
			} ],
			pager : '#jqg-pager',
			caption : '안건 목록',
			autoencode : false
		});
	}

	function initColPropDefectType() {
		var id = $('#productPhase').val();
		var url = '<s:url value="/rest/pms/projinsp/" />' + id + '/defecttypes';
		$.post(url, function(data) {
			$('#jqg-table').jqGrid('setColProp', 'defectType', {
				editoptions : {
					value : data
				}
			});
			$('#jqg-table').jqGrid().trigger("reloadGrid");
		});
	}

	function initForm() {
		$('#frm-projinsp :input').attr('readonly', 'readonly');
		$('#frm-projinsp :input').css({
			'background' : 'white',
			'color' : 'black'
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 인스펙션 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<form:form id="frm-projinsp" modelAttribute="projInspFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>프로젝트 인스펙션 개요</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="projectName">프로젝트 이름</form:label>
					<form:input path="projectName" class="pure-input-2-3" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="producerName">작성자</form:label>
					<form:input path="producerName" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="inspectionDate">검토 일자</form:label>
					<form:input path="inspectionDate" cssClass="pure-input-2-3" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="inspectors">검토자 수</form:label>
					<form:input path="inspectors" cssClass="pure-input-2-3" />
				</div>
				<div class="pure-u-1-3">
					<form:hidden path="moderator" />
					<form:label path="moderatorName">진행자</form:label>
					<form:input path="moderatorName" cssClass="pure-input-2-3" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-2-3">
					<form:label path="productName">산출물 명칭</form:label>
					<form:input path="productName" cssClass="pure-input-2-3" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="productTypeName">산출물 구분</form:label>
					<form:input path="productTypeName" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:hidden path="productPhase" />
					<form:label path="productPhaseName">검토 단계</form:label>
					<form:input path="productPhaseName" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="productSize">산출물 크기(WW)</form:label>
					<form:input path="productSize" cssClass="pure-input-2-3" />
				</div>
				<div class="pure-u-1-3">
					<form:label path="inspectionPercent">검토 분량(%)</form:label>
					<form:input path="inspectionPercent" cssClass="pure-input-2-3" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-3">
					<form:label path="inspectionMeetingTime">미팅 시간</form:label>
					<form:input path="inspectionMeetingTime" />
				</div>
			</div>
		</fieldset>
		<form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
	</form:form>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />