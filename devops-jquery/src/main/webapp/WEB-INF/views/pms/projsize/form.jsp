<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
    $(function() {
    	//프로젝트코드 자동생성
    	initAutoComplete();
    	if (eval('${projSizeFrmVO.isNew()}')) {
    		//console.log("등록화면");
			initClentArrayButtons();
			initClentArrayJqGrid();
			initClentArrayNavGrid();
		} else {
			//console.log("수정화면");
			initRemoteButtons();
			initRemoteJqGrid();
			initRemoteNavGrid();
		}
		initButtons();
    });
    
	function initAutoComplete() {
		applyProjectTypeAutoComplete('#project');
	}    
    
	function initButtons() {
		setSaveFormButtonFn('#btn-save', false, function() {
			if (eval('${projSizeFrmVO.isNew()}')) {
				$('#jqg-table_ilsave').click();
				var rowData = JSON.stringify($("#jqg-table").getRowData());
				$('#rowData').val(rowData);
				$('#frm-projsize').submit();
			}else{
				$('#jqg-table_ilsave').click();
				$('#frm-projsize').submit();
			}
		});
		
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${projSizeFrmVO.getPrevUrl()}" />');

		$('.qms-content-buttonset').css('display', 'block');
	};
	
	//주간보고목록 그리드 조회
	function initRemoteJqGrid() {
		$("#jqg-table").jqGrid({
            url : '<s:url value="${projSizeFrmVO.getDetailsUrl()}" />',
			editurl : '<s:url value="${projSizeFrmVO.getDetailsEditUrl()}" />',
            colNames: ['id', '보고일자', '신규<br/>입력','신규<br/>조회','신규<br/>배치','신규<br/>보고서', '수정<br/>입력','수정<br/>조회','수정<br/>배치','수정<br/>보고서','총계','환산','projectSize'],
            colModel: [{
                name: 'id',
                key: true,
                hidden: true //칼럼 hide
            }, {
                name: 'reportDate',
                key: true,
                align: 'center',
                width: '20%',
                editable : true,
                edittype : 'text',
                editrules : { required : true },
                editoptions : { dataInit : function(e) {$(e).datepicker();}}
            }, {
                name: 'newInput',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newSearch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newBatch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newReport',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editInput',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editSearch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editBatch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editReport',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'totSum',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%',
                editoptions:{disabled:true}
            }, {
                name: 'wwCal',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%',
                editoptions:{disabled:true}
            },{
                name: 'projectSize',
                hidden: true 
            }],            
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '프로젝트 size 목록',    
			autoencode : false,
			serializeGridData : function(postData) {
				postData.projectTypeCode = $('#projectTypeCode').val();
				return postData;
			},
        });	
	}
	
	function initClentArrayJqGrid() {
		$('#jqg-table').jqGrid({
			datatype : 'jsonstring',
			editurl : 'clientArray',
			colNames: ['id', '보고일자', '신규<br/>입력','신규<br/>조회','신규<br/>배치','신규<br/>보고서', '수정<br/>입력','수정<br/>조회','수정<br/>배치','수정<br/>보고서','총계','환산'],
			colModel: [{
                name: 'id',
                key: true,
                hidden: true //칼럼 hide
            }, {
                name: 'reportDate',
                key: true,
                align: 'center',
                width: '20%',
                editable : true,
                edittype : 'text',
                editrules : { required : true },
                editoptions : { dataInit : function(e) {$(e).datepicker();}}
            }, {
                name: 'newInput',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newSearch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newBatch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'newReport',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editInput',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editSearch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editBatch',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'editReport',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%'
            }, {
                name: 'totSum',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%',
                editoptions:{disabled:true}
            }, {
                name: 'wwCal',
                align: 'right',
                search: false,
                editable : true,
                edittype : 'text',
                width: '8%',
                editoptions:{disabled:true}
            }],            
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '프로젝트 size 목록',
			autoencode : false
		});	
	}
	
	function initClentArrayNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false,
			del : false,
			search : false,
			refresh : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}
	
	function initRemoteNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			edit : false
		});

		$("#jqg-table").jqGrid('inlineNav', '#jqg-pager');
	}	
  	
	function initRemoteButtons() {
		setDelButton('#btn-del', false, '#del_jqg-table');
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveBefore('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	}
	
	function initClentArrayButtons() {
		setAddButton('#btn-iladd', false, '#jqg-table_iladd');
		setEditButton('#btn-iledit', false, '#jqg-table_iledit');
		setSaveBefore('#btn-ilsave', false, '#jqg-table_ilsave');
		setCancelButton('#btn-ilcancel', false, '#jqg-table_ilcancel');
	} 
	
	function setSaveBefore(selector, disabled, el) {
		$(selector).button({
			icons : {
				primary : 'ui-icon-disk'
			},
			disabled : disabled
		}).click(function() {
			var rowData = JSON.stringify($("#jqg-table").getRowData());
			$('#rowData').val(rowData);
			$(el).click();
			$('#jqg-table').setGridParam({}).trigger('reloadGrid');
		});		
	}
	
	
  
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 size 등록(수정)</span>    
    </div>
</div>
<div class="qms-content-main">  
    <div class="pure-g">
        <div class="pure-u-1-2"></div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
				<button id="btn-save">저장</button>
				<button id="btn-cancel">취소</button>
            </div>       
        </div>        
    </div>
 	<s:url value="${projSizeFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-projsize" action="${action}" method="${projSizeFrmVO.getMethod()}" modelAttribute="projSizeFrmVO"
		cssClass="pure-form pure-form-stacked">   
        <fieldset>
        	<legend>프로젝트 Size 정보</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="project">프로젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />               
                </div>
                <div class="pure-u-1-2">
                    <form:label path="projectName" required="true">프로젝트 이름</form:label>
					<form:input path="projectName" placeholder="프로젝트 이름를 입력하세요."
						class="pure-input-2-3" readonly="${not projSizeFrmVO.isNew()}" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:hidden path="projectTypeCode"/>
                    <form:label path="projectTypeName">프로젝트유형</form:label>
                    <form:input path="projectTypeName" readonly="true" />
                    <form:errors path="projectTypeName" cssClass="ui-state-error-text" />
                </div>
            </div>
            <div class="pure-g">
            	<div class="pure-u-1-4">
                    <form:label path="threshold">(±)Threshold(%)</form:label>
                    <form:input path="threshold" numberonly="true" maxlength="3" placeholder="threshold"/>
                    <form:errors path="threshold" cssClass="ui-state-error-text" />                    
                </div>
                <div class="pure-u-1-4">
                    <form:label path="contingency">Contingency(%)</form:label>
                    <form:input path="contingency" numberonly="true" maxlength="3" placeholder="contingency" />                    
                    <form:errors path="contingency" cssClass="ui-state-error-text" />                    
                </div>
                <div class="pure-u-1-4">
                    <form:label path="phase">단계</form:label> 
                    <form:select path="phase" items="${phases}" />
                    <form:errors path="phase" cssClass="ui-state-error-text" />                    
                </div>
            </div>
        </fieldset>

        <form:hidden path="id" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="rowData" />
		    
        <fieldset>
            <legend>프로젝트 Size 상세정보</legend>
        </fieldset>
    </form:form>
	<div class="pure-g">
        <div class="pure-u-1-2">
        </div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
                <c:choose>
					<c:when test="${projSizeFrmVO.isNew()}">
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:when>
					<c:otherwise>
						<button id="btn-del">삭제</button>
						<button id="btn-iladd">등록</button>
						<button id="btn-iledit">수정</button>
						<button id="btn-ilsave">저장</button>
						<button id="btn-ilcancel">취소</button>
					</c:otherwise>
				</c:choose>
            </div>
        </div>
    </div>
    <br/>
    <div class="pure-g">
        <div class="pure-u-1">
            <table id="jqg-table"></table>
			<div id="jqg-pager"></div>
        </div>
    </div>
</div>
<br />