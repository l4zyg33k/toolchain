<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
    $(function() {      
		//조회 - navGrid의 refresh를 클릭하여, serializeGridData 이벤트에 의해 postData가 설정 되도록 합니다. 
		setSearchButton('#btn-search', false, '#refresh_jqg-table');
        //등록
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		//수정
        setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		//삭제
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
        //버튼처리
        $("#btn-add").button("option", "disabled", !eval('${navGrid.add}'));
        setButton(true, true);
        //그리드 조회
        initJqGrid();
        //네비그리드 세팅
        initNavGrid();
    });
    
    //버튼 처리
    function setButton(edit, del){
        $("#btn-edit").button("option", "disabled", edit);
        $("#btn-del").button("option", "disabled", del);
    }    
    
	//주간보고목록 그리드 조회
	function initJqGrid() {
		$("#jqg-table").jqGrid({
            url: '<s:url value="/rest/pms/projsize" />',
            editurl : '<s:url value="/rest/pms/projsize/edit" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['id', '프로젝트코드', '프로젝트이름','단계','THRESHOLD<br/>(%)','CONTINGENCY<br/>(%)','il'],
            colModel: [{
                    name: 'id',
                    key: true,
                    hidden: true //칼럼 hide
                }, {
                    name: 'project.name',
                    align: 'center',
                    search: false,
                    width : '20%'
                }, {
                    name: 'project.code',
                    align: 'left'
                }, {
                    name: 'phase.name',
                    align: 'center',
                    width : '25%'
                }, {
                    name: 'threshold',
                    align: 'center',
                    width : '15%'
                }, {
                    name: 'contingency',
                    align: 'center',
                    width : '15%'
                }, {
    				name : 'editable',
    				hidden : true
    			}],
    		page : '${page}',
    		rowNum : '${rowNum}',
    		pager : '#jqg-pager',
            sortname: 'id',
            sortorder: 'desc',
            height: "100%",
            caption: '프로젝트 size 목록',
            serializeGridData : function(postData) {
				postData.project = $('#project').val();
				return postData;
			},
            loadComplete: function() {
            	//데이터가 존재하는 경우
            	if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var rowId = '${rowId}';
					if (rowId) {
						$(this).setSelection(rowId);
					} else {
						rowId = $(this).jqGrid('getDataIDs')[0];
						$(this).setSelection(rowId);
					}
					setButton(false, false);
            	} else {
            		//데이터가 없는 경우
            		setButton(true, true);
				}
            },
			ondblClickRow : function(rowId) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					$('#view_jqg-table').click();
				}
			}
        });		
	}
    
	//그리드 버튼 처리 
	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {});
	}
	
	//수정
	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '<s:url value="/pms/projsize/" />' + rowId
					+ '/form?page=' + page + '&rowNum=' + rowNum + '&rowId='
					+ rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}
	
	//등록
	function beforeInitDataAdd(formId, scope) {
		if (eval('${navGrid.add}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
			if(rowId == ""){
				rowId = "0";
			} 
			var url = '<s:url value="/pms/projsize/form?page=" />' + page + '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('등록 할 수 없습니다.');
			return false;
		}
	}

	//삭제
	function beforeInitDataDel(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}
	
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 size 관리</span>    
    </div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
                <input id="project" type="text" class="pure-input-rounded" placeholder="프로젝트명 또는 프로젝트코드" size="30">
                <a id="btn-search">조회</a>
            </form>
        </div>	
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
			    <button id="btn-add">등록</button>
			    <button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
    <div id="jqg-pager"></div>
</div>
<br />