<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initJqGrid();
		initNavGrid();
		initSelect();
		initDatepicker();
		refreshProjects();
	});

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			datatype : 'local',
			url : '<s:url value="/rest/pms/teamefot" />',
			colModel : [ {
				label : '일자',
				name : 'workDate',
				align : 'center',
				width : 300,
				key : true
			}, {
				label : '착수',
				name : 'beginning',
				align : 'center',
				sortable : false
			}, {
				label : '분석',
				name : 'analysis',
				align : 'center',
				sortable : false
			}, {
				label : '설계',
				name : 'design',
				align : 'center',
				sortable : false
			}, {
				label : '개발',
				name : 'development',
				align : 'center',
				sortable : false
			}, {
				label : '테스트',
				name : 'test',
				align : 'center',
				sortable : false
			}, {
				label : '이행',
				name : 'execution',
				align : 'center',
				sortable : false
			}, {
				label : '교육',
				name : 'education',
				align : 'center',
				sortable : false
			}, {
				label : '기타<br/>(휴가)',
				name : 'etc',
				align : 'center',
				sortable : false
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'workDate',
			sortorder : 'asc',
			caption : '팀원 일별 공수',
			serializeGridData : function(postData) {
				postData.project = $('#project').val();
				postData.member = $('#member').val();
				postData.fromDate = $('#fromDate').val();
				postData.toDate = $('#toDate').val();
				return postData;
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			add : false,
			del : false,
			view : false,
			search : false
		}, {}, {}, {}, {}, {});
	}

	function initSelect() {
		$('#year').change(function() {
			refreshProjects();
		});
		$('#project').change(function() {
			refreshMembers();
		});
		$('#member').change(function() {
			$('#refresh_jqg-table').click();
		});
	}

	function initDatepicker() {
		$('#fromDate').datepicker();
		$('#fromDate').mask('9999-99-99');
		$('#fromDate').change(function() {
			$('#refresh_jqg-table').click();
		});
		$('#toDate').datepicker();
		$('#toDate').mask('9999-99-99');
		$('#toDate').change(function() {
			$('#refresh_jqg-table').click();
		});
	}

	function refreshProjects() {
		$.post('<s:url value="/rest/pms/teamefot/projects" />', {
			year : $('#year').val()
		}, function(data) {
			$('#project').empty();
			for (var i = 0; i < data.length; i++) {
				$('#project').append(
						"<option value='" + data[i].key + "'>" + data[i].value
								+ "</option>");
			}
			refreshMembers();
		});
	}

	function refreshMembers() {
		$.post('<s:url value="/rest/pms/teamefot/members" />', {
			project : $('#project').val()
		}, function(data) {
			$('#member').empty();
			for (var i = 0; i < data.length; i++) {
				$('#member').append(
						"<option value='" + data[i].key + "'>" + data[i].value
								+ "</option>");
			}
			$('#jqg-table').jqGrid('setGridParam', {
				datatype : 'json'
			});
			$('#refresh_jqg-table').click();
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">팀원 일별 공수</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<form class="pure-form pure-form-stacked">
			<div class="pure-u-1-8">
				<form:label path="year">수행 년도</form:label>
				<form:select id="year" path="year" items="${years}" />
			</div>
			<div class="pure-u-1-2">
				<label for="project">수행 프로젝트</label> <select id="project"></select>
			</div>
			<div class="pure-u-1-8">
				<label for="member">수행 인원</label> <select id="member"></select>
			</div>
			<div class="pure-u-1-8">
				<label for="fromDate">조회 시작일</label> <input id="fromDate"
					value="${fromDate}" class="pure-input-2-3" />
			</div>
			<div class="pure-u-1-8">
				<label for="toDate">조회 종료일</label> <input id="toDate"
					value="${toDate}" class="pure-input-2-3" />
			</div>
		</form>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />