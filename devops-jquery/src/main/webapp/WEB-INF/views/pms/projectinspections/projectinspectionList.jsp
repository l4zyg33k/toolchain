<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication var="username" property="principal.username" />
</sec:authorize>

<script type="text/javascript">
	//권한확인
	var adminYn = true;
	<sec:authorize ifNotGranted="ROLE_IL, ROLE_ADMIN">
		adminYn = false;
	</sec:authorize>
	
    $(function() {
        //검색버튼 파일 돋보기 모양 아이콘 주기
        $("#btn-search").button({
            icons: {
                primary: 'ui-icon-search'
            }
        }).click(function(event) {
        	/*
        	var url = '<s:url value="/pms/projectinspections/result" />';
			var data = {
				afterDate : $("#afterDate").val(),
				beforeDate : $("#beforeDate").val()
			};
			$.post(url, data, function(data,
					textStatus, XMLHttpRequest) {
			});
			*/
			$.ajax({
                type: 'POST',
                url: '<s:url value="/pms/projectinspections/result"/>',
                data : {
                	afterDate:$("#afterDate").val(),
                	beforeDate:$("#beforeDate").val()
                	
                },
                success : function(data) {
                	$("#totalSum").val(data.totalSum);
                	$("#majorCountSum").val(data.majorCountSum);
                	$("#minorCountSum").val(data.minorCountSum);
                	$("#inspectorsAvg").val(Math.round(data.inspectorsAvg*100)/100+" 명");
                	$("#inspectionMeetingTimeAvg").val(Math.round(data.inspectionMeetingTimeAvg*100)/100+" 분");
                	$("#productSizeAvg").val(Math.round(data.productSizeAvg*100)/100+" WW");
                	$("#inspectionPercentAvg").val(Math.round(data.inspectionPercentAvg*100)/100+" %");
                	$("#progressSize").val(data.progressSize+" WW");
                	$("#progressSpeed").val(data.progressSpeed+" WW");
                	$("#defectDensity").val(data.defectDensity);
                	$("#effect").val(data.effect);
                	
                	//그리드 조회
                	$("#jqg-table").jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
                	
                }
            });
        });
       

        $("#jqg-table").jqGrid({
            url: '<s:url value="/rest/pms/projectinspections/result"/>',
            datatype: 'local',
            mtype: 'POST',
            colNames: ['프로젝트 명칭', '단계', '대상<br>건수', 'Major<br>결함수',
                       'Minor<br>결함수', '참가<br>인원수','수행<br>시간','수행<br>분량' ,'산출물<br>크기',
                       '수행<br>크기','수행<br>속도','결함<br>밀도', '효율성'],
            colModel: [{
                    name: 'project.name',
                    align: 'left',
                    width : 500
                }, {
                    name: 'productPhase.name',
                    align: 'center'
                }, {
                    name: 'totalSum',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'majorCountSum',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'minorCountSum',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'inspectorsAvg',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'inspectionMeetingTimeAvg',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'productSizeAvg',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'inspectionPercentAvg',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'progressSize',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'progressSpeed',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'defectDensity',
                    align: 'center',
                    sortable:false
                }, {
                    name: 'effect',
                    align: 'center',
                    sortable:false
                }],
            pager: '#jqg-pager',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'project.id desc, ProjectInspection.productPhase.id',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '프로젝트 Inspection 목록',
            beforeRequest: function() {
				$("#jqg-table").setGridParam({
					postData : {
						afterDate : $("#afterDate").val(),
						beforeDate : $("#beforeDate").val()
					}
				});
            },
            loadComplete: function() {
            	
            },
            onSelectRow: function(id) {
            	
            }
        });

        $("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {}, {}, {}, search_options);
        
        //masking
        $('#afterDate').mask('9999-99-99');	//기간
        $('#beforeDate').mask('9999-99-99');	//기간
        
        //picker처리
        $('#afterDate').datepicker();		//기간
        $('#beforeDate').datepicker();			//기간
        
    });
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 Inspection 관리</span>
    </div>
</div>
<div class="qms-content-main">
    <div class="pure-g">
        <div class="pure-u-2-3">
            <form id="query-form" class="pure-form" action='<s:url value="/pms/projectinspections/result"/>' method="POST">
                <input id="afterDate" name="afterDate" type="text" class="pure-input-rounded" placeholder="조회시작일" size="9">
                
                <input id="beforeDate" name="beforeDate" type="text" class="pure-input-rounded" placeholder="조회종료일" size="9">
                <a id="btn-search" style="font-size: 0.8em">조회</a>
            </form>
        </div>
    </div>
    <br/>
    <form:form id="frm-form" action='/pms/projectinspections/result' method="post"
               modelAttribute="projectInspectionListVO"
               cssClass="pure-form pure-form-stacked">
        <fieldset>
            <legend>프로젝트 Inspection 결과정보</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="totalSum">대상건수</form:label>
                    <form:input path="totalSum" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="totalSum" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="majorCountSum">Major DF 수</form:label>
                    <form:input path="majorCountSum" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="majorCountSum" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="minorCountSum">Minor DF 수</form:label>
                    <form:input path="minorCountSum" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="minorCountSum" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="inspectorsAvg">참가인원 평균</form:label>
                    <form:input path="inspectorsAvg" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="inspectorsAvg" cssClass="ui-state-error-text" />
                </div>
            </div>
            
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="inspectionMeetingTimeAvg">수행시간 평균</form:label>
                    <form:input path="inspectionMeetingTimeAvg" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="inspectionMeetingTimeAvg" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="productSizeAvg">산출물크기 평균</form:label>
                    <form:input path="productSizeAvg" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="productSizeAvg" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="inspectionPercentAvg">수행분량 평균</form:label>
                    <form:input path="inspectionPercentAvg" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="inspectionPercentAvg" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="progressSize">수행크기</form:label>
                    <form:input path="progressSize" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="progressSize" cssClass="ui-state-error-text" />
                </div>
            </div>
            
            
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="progressSpeed">수행속도</form:label>
                    <form:input path="progressSpeed" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="progressSpeed" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="defectDensity">결함밀도</form:label>
                    <form:input path="defectDensity" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="defectDensity" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="effect">효율성</form:label>
                    <form:input path="effect" cssClass="pure-input-2-3" readonly="true" />
                    <form:errors path="effect" cssClass="ui-state-error-text" />
                </div>
            </div>
            
            </fieldset>
		</form:form>
    <br />
    <table id="jqg-table"></table>
    <div id="jqg-pager"></div>
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">프로젝트 관리</span>
    <span id="sidebar-menu">프로젝트 Inspection 조회</span>
</div>
 -->
<br />