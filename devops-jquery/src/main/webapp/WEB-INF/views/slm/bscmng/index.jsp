<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<s:url value="/rest/slm/bsc/chained/types" var="chained_types" />
<s:url value="/rest/slm/bsc/chained/types?demand=" var="chained_types_params" />
<s:url value="/rest/slm/bsc/lvlmng" var="main_grid_url" />
<s:url value="/rest/slm/bsc/lvlmng/edit" var="main_grid_edit_url" />
<s:url value="/rest/slm/bsc/argmng" var="sub_grid_url" />
<s:url value="/rest/slm/bsc/argmng/edit" var="sub_grid_edit_url" />

<script type="text/javascript">
	$(function() {
		initOptions();
        initButtons();
        initJqGridMain();
        initNavGridMain();
        initJqGridSub();
        initNavGridSub();
    });
	
	function initOptions() {
		$('#type_').remoteChained({
		    parents : '#demand',
		    url : '${chained_types}'
		}).change(function() {
			$("#jqg-main-table").trigger("reloadGrid");
			$("#jqg-sub-table").trigger("reloadGrid");
		});
		
		$('#year').change(function() {
			$("#jqg-main-table").trigger("reloadGrid");
			$("#jqg-sub-table").trigger("reloadGrid");
		});		
		
		$('#demand').change(function() {
			$("#jqg-main-table").trigger("reloadGrid");
			$("#jqg-sub-table").trigger("reloadGrid");
		});	
		
		$('#job_').change(function() {
			$("#jqg-main-table").trigger("reloadGrid");
			$("#jqg-sub-table").trigger("reloadGrid");
		});
	}

	function initButtons() {
		setAddButton('#btn-main-add', eval('${!navGrid.add}'),
				'#add_jqg-main-table');
		setEditButton('#btn-main-edit', eval('${!navGrid.edit}'),
				'#edit_jqg-main-table');
		setDelButton('#btn-main-del', eval('${!navGrid.del}'),
				'#del_jqg-main-table');

		setAddButton('#btn-sub-add', eval('${!navGrid.add}'),
				'#add_jqg-sub-table');
		setEditButton('#btn-sub-edit', eval('${!navGrid.edit}'),
				'#edit_jqg-sub-table');
		setDelButton('#btn-sub-del', eval('${!navGrid.del}'),
				'#del_jqg-sub-table');
	}

	function initJqGridMain() {

		$("#jqg-main-table").jqGrid({
			url : '${main_grid_url}',
			editurl : '${main_grid_edit_url}',
			datatype : 'json',
			mtype : 'POST',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				name : 'year',
				label : '기준<br />년도',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${yearsVals}',
					dataInit : function(e) {
						$(e).val($('#year').val());
					}
				},
				formoptions : {
					label : '기준년도'
				}
			}, {
				name : 'function',
				label : '요구<br />기능',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${demandsVals}',
					dataInit : function(e) {
						$(e).val($('#demand').val());
					}
				},
				formoptions : {
					label : '요구기능'
				}
			}, {
				name : 'job',
				label : '작업<br />유형',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${jobsVals}'
				},
				formoptions : {
					label : '작업유형'
				}
			}, {
				name : 'type',
				label : '기능<br />유형',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				formoptions : {
					label : '기능유형'
				}
			}, {
				name : 'ftrMin',
				label : 'MIN',
				align : 'right',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '0'
				},
				formoptions : {
					label : 'RET/FTR MIN 값'
				}
			}, {
				name : 'ftrMinOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${mores}'
				},
				formoptions : {
					label : 'RET/FTR MIN 연산'
				}
			}, {
				name : 'ftrMax',
				label : 'MAX',
				align : 'right',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					integer : true
				},
				editoptions : {
					defaultValue : '0'
				},
				formoptions : {
					label : 'RET/FTR MAX 값'
				}
			}, {
				name : 'ftrMaxOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${lesses}'
				},
				formoptions : {
					label : 'RET/FTR MAX 연산'
				}
			}, {
				name : 'detMin',
				label : 'MIN',
				align : 'right',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					integer : true
				},
				editoptions : {
					defaultValue : '0'
				},
				formoptions : {
					label : 'DET MIN 값'
				}
			}, {
				name : 'detMinOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${mores}'
				},
				formoptions : {
					label : 'DET MIN 연산'
				}
			}, {
				name : 'detMax',
				label : 'MAX',
				align : 'right',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					integer : true
				},
				editoptions : {
					defaultValue : '0'
				},
				formoptions : {
					label : 'DET MAX 값'
				}
			}, {
				name : 'detMaxOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${lesses}'
				},
				formoptions : {
					label : 'DET MAX 연산'
				}
			}, {
				name : 'level',
				label : '난이도',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${levelsVals}'
				}
			}, {
				name : 'fp',
				label : '기능<br />점수',
				align : 'center',
				editable : true,
				formatter : 'number',
				edittype : 'text',
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : '기능점수'
				}
			} ],
			pager : '#jqg-main-pager',
			page : '${page}',
			rowNum : '${rowNum}',
			sortname : 'job asc, type',
			sortorder : 'asc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '레벨관리',
			serializeGridData : function(postData) {
				postData.year = $('#year').val();
				postData.demand = $('#demand').val();
				postData.job = $('#job_').val();
				postData.type = $('#type_').val();
				return postData;
			},
			loadComplete : function() {
				var id = $('#demand').val();

				if (id == 0) {
					id = $('#demand option:eq(1)').val();
				}

				var url = '${chained_types_params}' + id;
				$.post(url, function(data) {
					$('#jqg-main-table').jqGrid('setColProp', 'type', {
						editoptions : {
							value : data
						}
					});
				});

				return false;
			}
		});

		$("#jqg-main-table").jqGrid("setGroupHeaders", {
			useColSpanStyle : true,
			groupHeaders : [ {
				startColumnName : "ftrMin",
				numberOfColumns : 4,
				titleText : "RET/FTR"
			}, {
				startColumnName : "detMin",
				numberOfColumns : 4,
				titleText : "DET"
			} ]
		});
	}

	function initNavGridMain() {
		$("#jqg-main-table").jqGrid('navGrid', '#jqg-main-pager', {
			view : '${navGrid.view}',
			search : false
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#function', formId).attr('disabled', 'disabled');
				$('#job', formId).attr('disabled', 'disabled');
				$('#type', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#function', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		});
	}


	function initJqGridSub() {

		$("#jqg-sub-table").jqGrid({
			url : '${sub_grid_url}',
			editurl : '${sub_grid_edit_url}',
			datatype : 'json',
			mtype : 'POST',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				name : 'year',
				label : '기준<br />년도',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${yearsVals}',
					dataInit : function(e) {
						$(e).val($('#year').val());
					}
				},
				formoptions : {
					label : '기준년도'
				}
			}, {
				name : 'function',
				label : '요구<br />기능',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${demandsVals}',
					dataInit : function(e) {
						$(e).val($('#demand').val());
					}
				},
				formoptions : {
					label : '요구기능'
				}
			}, {
				name : 'job',
				label : '작업<br />유형',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${jobsVals}'
				},
				formoptions : {
					label : '작업유형'
				}
			}, {
				name : 'type',
				label : '기능<br />유형',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				formoptions : {
					label : '기능유형'
				}
			}, {
				name : 'ftrMin',
				label : 'MIN',
				align : 'right',
				formatter : 'number',
				editable : true,
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : 'RET/FTR 변경률 MIN 값'
				}
			}, {
				name : 'ftrMinOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${mores}'
				},
				formoptions : {
					label : 'RET/FTR 변경률 MIN 연산'
				}
			}, {
				name : 'ftrMax',
				label : 'MAX',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : 'RET/FTR 변경률 MAX 값'
				}
			}, {
				name : 'ftrMaxOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${lesses}'
				},
				formoptions : {
					label : 'RET/FTR 변경률 MAX 연산'
				}
			}, {
				name : 'detMin',
				label : 'MIN',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : 'DET 변경률 MIN 값'
				}
			}, {
				name : 'detMinOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${mores}'
				},
				formoptions : {
					label : 'DET 변경률 MIN 연산'
				}
			}, {
				name : 'detMax',
				label : 'MAX',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : 'DET 변경률 MAX 값'
				}
			}, {
				name : 'detMaxOp',
				label : '연산',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${lesses}'
				},
				formoptions : {
					label : 'DET 변경률 MAX 연산'
				}
			}, {
				name : 'adjArg',
				label : '조정<br />인자',
				align : 'center',
				editable : true,
				formatter : 'number',
				edittype : 'text',
				editrules : {
					required : true,
					number : true
				},
				editoptions : {
					defaultValue : '0.00'
				},
				formoptions : {
					label : '조정인자'
				}
			} ],
			pager : '#jqg-sub-pager',
			page : '${page}',
			rowNum : '${rowNum}',
			sortname : 'job asc, type',
			sortorder : 'asc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '조정인자관리',
			serializeGridData : function(postData) {
				postData.year = $('#year').val();
				postData.demand = $('#demand').val();
				postData.job = $('#job_').val();
				postData.type = $('#type_').val();
				return postData;
			},
			loadComplete : function() {
				var id = $('#demand').val();

				if (id == 0) {
					id = $('#demand option:eq(1)').val();
				}

				var url = '${chained_types_params}' + id;
				$.post(url, function(data) {
					$('#jqg-sub-table').jqGrid('setColProp', 'type', {
						editoptions : {
							value : data
						}
					});
				});

				return false;
			}
		});

		$("#jqg-sub-table").jqGrid("setGroupHeaders", {
			useColSpanStyle : true,
			groupHeaders : [ {
				startColumnName : "ftrMin",
				numberOfColumns : 4,
				titleText : "RET/FTR 변경률"
			}, {
				startColumnName : "detMin",
				numberOfColumns : 4,
				titleText : "DET 변경률"
			} ]
		});
	}

	function initNavGridSub() {
		$("#jqg-sub-table").jqGrid('navGrid', '#jqg-sub-pager', {
			view : '${navGrid.view}',
			search : false
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#function', formId).attr('disabled', 'disabled');
				$('#job', formId).attr('disabled', 'disabled');
				$('#type', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#function', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">기본 정보 관리</span>
	</div>
</div>

<div class="qms-content-main">
    <br />
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form class="pure-form">
				<form:select id="year" name="year" path="year" items="${years}" />
				<form:select id="demand" name="demand" path="demand" items="${demands}" />
				<form:select id="job_" name="job" path="job" items="${jobs}" />
				<form:select id="type_" name="type" path="type" items="${types}" />
			</form>
		</div>
		<div class="pure-u-1-3">
		</div>
	</div>
	<br />
	<div class="pure-g">
		<div class="pure-u-2-3"></div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-main-add">등록</a>
				<a id="btn-main-edit">수정</a>
				<a id="btn-main-del">삭제</a>			
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-main-table"></table>
	<div id="jqg-main-pager"></div>
	<br /> 
	<br /> 
	<br />
	<div class="pure-g">
		<div class="pure-u-2-3"></div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-sub-add">등록</a>
				<a id="btn-sub-edit">수정</a>
				<a id="btn-sub-del">삭제</a>			
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-sub-table"></table>
	<div id="jqg-sub-pager"></div>
</div>