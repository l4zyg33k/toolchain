<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/slm/elemrangmng" var="jqg_url" />
<s:url value="/rest/slm/elemrangmng/edit" var="jqg_edit_url" />
<script type="text/javascript">
	$(function() {
		initOptions();
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initOptions() {
		$('#year').change(function() {
			$("#jqg-table").trigger("reloadGrid");
		});
		$('#component').change(function() {
			$("#jqg-table").trigger("reloadGrid");
		});		
	}

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '${jqg_url}',
			editurl : '${jqg_edit_url}',
			colModel : [ {
				name : 'year',
				label : '기준년도',
				index : 'id.year',
				align : 'center',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : $('#year').val(),
					dataInit : function(e) {
						$(e).val($('#year').val());
					}
				}	
			}, {
				name : 'component',
				label : 'FP 유형',
				index : 'id.component',
				align : 'center',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : $('#component').val(),
					dataInit : function(e) {
						$(e).val($('#component').val());
					}
				}			
			}, {
				name : 'element',
				label : 'FP 요소',
				index : 'id.element',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editoptions : {
					value : 'RET:RET;FTR:FTR;DET:DET'
				}			
			}, {
				name : 'lessThan',
				label : '하위(미만)',
				align : 'center',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			}, {
				name : 'greaterThan',
				label : '상위(초과)',
				align : 'center',
				formatter : 'integer',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'id.element',
			sortorder : 'asc',
			caption : 'FP 요소별 구간 목록',
			serializeGridData : function(postData) {
				postData.year = $('#year').val();
				postData.component = $('#component').val();
				return postData;
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			search : false
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#component', formId).attr('disabled', 'disabled');
				$('#element', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#component', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			},
			closeAfterAdd : true
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			},
			serializeDelData : function(postData) {
				var rowData = $('#jqg-table').getRowData(postData.id);
				postData.year = rowData.year;
				postData.component = rowData.component;
				postData.element = rowData.element;
				return postData;
			}
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">FP 요소별 구간 관리</span>
	</div>
</div>

<div class="qms-content-main">
	<br />
	<div class="pure-g">
		<div class="pure-u-1-8">
			<form:form modelAttribute="elemRangWebVO" class="pure-form pure-form-stacked">
				<form:label path="year">기준년도</form:label>
				<form:select path="year" items="${years}" />
			</form:form>	
		</div>
		<div class="pure-u-1-8">
			<form:form modelAttribute="elemRangWebVO" class="pure-form pure-form-stacked">
				<form:label path="component">FP 유형</form:label>
				<form:select path="component" items="${components}" />
			</form:form>		
		</div>
	</div>
	<br />
	<div class="pure-g">
		<div class="pure-u-2-3"></div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>