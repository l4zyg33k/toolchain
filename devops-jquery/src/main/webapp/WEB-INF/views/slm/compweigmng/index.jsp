<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/slm/compweigmng" var="jqg_url" />
<s:url value="/rest/slm/compweigmng/edit" var="jqg_edit_url" />
<script type="text/javascript">
	$(function() {
		initOptions();
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initOptions() {
		$('#year').change(function() {
			$("#jqg-table").trigger("reloadGrid");
		});
	}

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '${jqg_url}',
			editurl : '${jqg_edit_url}',
			colModel : [ {
				name : 'year',
				label : '기준년도',
				index : 'id.year',
				align : 'center',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : $('#year').val(),
					dataInit : function(e) {
						$(e).val($('#year').val());
					}
				}
			}, {
				name : 'complexity',
				label : '복잡도',
				index : 'id.complexity',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : 'LOW:하;AVGERAGE:중;HIGH:상'
				}
			}, {
				name : 'ei',
				label : '외부입력(EI)',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			}, {
				name : 'eo',
				label : '외부출력(EO)',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			}, {
				name : 'eq',
				label : '외부쿼리(EQ)',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			}, {
				name : 'ilf',
				label : '내부로직파일<br />(ILF)',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			}, {
				name : 'eif',
				label : '외부연계파일<br />(EIF)',
				align : 'right',
				formatter : 'number',
				editable : true,
				edittype : 'text',
				editoptions : {
					defaultValue : '1'
				}
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'ei asc, eo',
			sortorder : 'asc',
			caption : 'FP 유형별 복잡도 목록',
			serializeGridData : function(postData) {
				postData.year = $('#year').val();
				return postData;
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			search : false
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
				$('#complexity', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			}
		}, {
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#year', formId).attr('disabled', 'disabled');
			},
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			},
			closeAfterAdd : true
		}, {
			afterSubmit : function(response, postData) {
				return jqResult(response, postData);
			},
			serializeDelData : function(postData) {
				var rowData = $('#jqg-table').getRowData(postData.id);
				postData.year = rowData.year;
				postData.type = rowData.type;
				return postData;
			}
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">FP 복잡도별 가중치 관리</span>
	</div>
</div>

<div class="qms-content-main">
	<br />
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form:form modelAttribute="compWeigMngWebVO" class="pure-form pure-form-stacked">
				<form:label path="year">기준년도</form:label>
				<form:select path="year" items="${years}" />
			</form:form>
		</div>
		<div class="pure-u-1-3"></div>
	</div>
	<br />
	<div class="pure-g">
		<div class="pure-u-2-3"></div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>