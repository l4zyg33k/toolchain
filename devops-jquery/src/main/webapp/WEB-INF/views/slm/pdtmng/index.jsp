<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButton();
		
		//기준년도
		$('#standardYear').change(function() {
			gridReload($("#jqg-table"));
		});

		//csr 파트
		$('#csrPart').change(function() {
			//재조회
			gridReload($("#jqg-table"));
		});
		
		//시스템 특성
		$('#systemType').change(function() {
			gridReload($("#jqg-table"));
		});
		
		//기본버튼 disable처리
		$("#btn-add").button("option", "disabled", true);
		$("#btn-edit").button("option", "disabled", true);
		$("#btn-del").button("option", "disabled", true);
		
		initJqGrid();
	});
	
    //버튼 처리
    function initButton(){
		//등록버튼
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		//수정버튼
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		//삭제버튼
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');  	
    }
    
	//그리드 조회
	function initJqGrid() {
		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/slm/productivity" />',
			editurl : '<s:url value="/rest/slm/productivity/edit"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ 'id', '년도', '시스템파트', '시스템유형','생산성'],
			colModel : [
					{
						name : 'id',
						key : true,
						hidden: true
					},{
						name : 'standardYear.name',
						align : 'center',
						editable : true,
						edittype : 'select',
						editrules : {required : true},
						editoptions : {	value : '${standardYearGrid}'}
					},{
						name : 'csrPart.name',
						align : 'center',
						editable : true,
						edittype : 'select',
						editrules : {required : true},
						editoptions : {	value : '${csrPartGrid}'}
					},{
						name : 'systemType.name',
						align : 'center',
						editable : true,
						edittype : 'select',
						editrules : {required : true},
						editoptions : {	value : '${systemTypeGrid}'}
					},{
						name : 'productivity',
						align : 'right',
						editable : true,
						editrules : {required : true},
						edittype : 'text'
					}],
			pager : '#jqg-pager',
			rowNum : 10,
			sortname : 'id',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '생산성관리',
			loadComplete : function() {
				setButton(false, true, true);//add, edit, del
			},
			onSelectRow : function(id) {
				setButton(false, false, false);//add, edit, del
			}
		});
		
		$("#jqg-table").jqGrid('navGrid','#jqg-pager'
			,{/* options */
				add:true,edit:true,view:false,del:true,search:true,refresh:true
			 } 													
			,{/* Edit options */
				recreateForm : true,
				closeAfterEdit : true
			 } 		
			,{/* Add options */
				recreateForm : true,
				closeAfterAdd : true
			}
			,{/* Delete options */
				recreateForm : true
			} 							
			,{/* Search options */
			}
			,{/* view parameters*/
			} 
	   	);		
	}

	//버튼제어
    function setButton(add, edit, del){
    	$("#btn-add").button("option", "disabled", add);
		$("#btn-edit").button("option", "disabled", edit);
		$("#btn-del").button("option", "disabled", del);
    }
	
	//그리드 조회
    function gridReload(obj){
		//그리드 초기화
		obj.jqGrid("clearGridData");
		//그리드 재조회
		obj.setGridParam({
			postData : {
				standardYear : $("#standardYear").val(),
				csrPart : $("#csrPart").val(),
				systemType : $("#systemType").val()
			}
		}).trigger("reloadGrid");
	};	
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">생산성 관리</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form class="pure-form">
				<select id="standardYear">
					<option value="">- 기준년도 -</option>
					<c:forEach var="item" items="${standardYearCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
				<select id="csrPart">
					<option value="">- CSR 파트 -</option>
					<c:forEach var="item" items="${csrPartCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
				<select id="systemType">
					<option value="">- 시스템 특성 -</option>
					<c:forEach var="item" items="${systemTypeCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</form>
		</div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a>
				<a id="btn-edit">수정</a>
				<a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br/>	
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
