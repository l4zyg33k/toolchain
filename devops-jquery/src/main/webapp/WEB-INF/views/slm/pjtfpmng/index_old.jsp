<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
    $(function() {
        $("#btn-search").button({
            icons: {
                primary: 'ui-icon-search'
            }
        }).click(function(event) {
            $("#jqg-table").setGridParam({
                postData: {
                    query: $("#query").val()
                } 
            }).trigger("reloadGrid");
        });
        
        $("#btn-add").button({
            icons: {
                primary: 'ui-icon-plus'
            }
        }).click(function(event) {
			var id = $('#jqg-table').jqGrid('getGridParam', 'selrow');
            var url = parseUrl('/slm/projectfpresult/'+$('#standardYear').val()+'/new');
            get(url);
        });
        
        $("#btn-edit").button({
            icons: {
                primary: 'ui-icon-pencil'
            }
        }).click(function(event) {
			var id = $('#jqg-table').jqGrid('getGridParam', 'selrow');
			var url = parseUrl('/slm/projectfpresult/'+ id +'/edit');
            get(url);
        });
        $("#btn-del").button({
            icons: {
                primary: 'ui-icon-trash'
            }
        }).click(function(event) {
            dialog('삭제 할까요?', function() {
                var id = $('#jqg-table').jqGrid('getGridParam', 'selrow');
                var url = parseUrl('/slm/projectfpresult/' + id);
                post(url, 'DELETE');
            });
        });
        
        $('#standardYear').change(function (){
        	$("#jqg-table").setGridParam({
                postData: {
                	standardYear: $("#standardYear").val()
                } 
            }).trigger("reloadGrid");
        });
        
        $("#jqg-table").jqGrid({
            url: '<s:url value="/rest/slm/projectfpresult" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['ID','기준년도','프로젝트코드', '프로젝트명', '프로그램명', '처리자','시스템유형','FP결과','예상MH'],
            colModel: [{
                    name: 'id',
                    align: 'center',
                    search: false,
                    key: true,
                    hidden:true
                }, {
                    name: 'standardYear.name',
                    align: 'center',
                    search: false
                }, {
                	name: 'project.code',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'project.name',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'programName',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'operator.name',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'systemType.name',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'finalFP',
                    key: true,
                    align: 'center',
                    search: false
                }, {
                	name: 'expectMH',
                    key: true,
                    align: 'center',
                    search: false
                }],
            pager: '#jqg-pager',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'id',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '프로젝트 목록',
            loadComplete : function() {
            	setBtnDisable(false, true, true);
			},
            beforeRequest: function() {
            	$("#jqg-table").setGridParam({
                    postData: {
                    	standardYear: $("#standardYear").val()
                    } 
                });
            },
            onSelectRow: function(id) {
            	//add, edit, del
            	setBtnDisable(false, false, false);
            }
        });
        $("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {}, {}, {}, search_options);
        
        //버튼disable제어
        function setBtnDisable( add, edit, del){
        	$("#btn-add").button("option", "disabled", add);
        	$("#btn-edit").button("option", "disabled", edit);
        	$("#btn-del").button("option", "disabled", del);
        }
        
        
    	//엔터키 이벤트 제어
    	$('#query-form').bind("keypress", function(e) {
            if (e.keyCode === 13) {               
              e.preventDefault();
              return false;
            }
          });
    	
    	//버튼 초기화
    	setBtnDisable(false, true, true);
    	
        
    });
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 FP</span>
    </div>
</div>
<div class="qms-content-main">
    <div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
                <select id="standardYear">
					<c:forEach var="item" items="${standardYearList}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
            </form>
        </div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">                
                <a id="btn-add">등록</a>
                <a id="btn-edit">수정</a>
                <a id="btn-del">삭제</a>
            </div>            
        </div>        
    </div>
    <br />
    <table id="jqg-table"></table>
    <div id="jqg-pager"></div>
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">기능점수</span> 
	<span id="sidebar-menu">프로젝트 FP</span>
</div>
 -->
<br />