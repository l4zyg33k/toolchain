<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<s:url value="/rest/slm/pjtfpmng/pjtfp" var="jqg_url" />
<s:url value="/rest/slm/pjtfpmng/pjtfp/edit" var="jqg_edit_url" />
<s:url value="/pms/projmng/" var="base_url" />
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		setViewButton('#btn-view', eval('${!navGrid.view}'), '#view_jqg-table');
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '${jqg_url}',
			editurl : '${jqg_edit_url}',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				name : 'year',
				label : '기준<br />년도',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${yearsVals}'
				},
				formoptions : {
					label : '기준년도'
				}
			}, {
				name : 'project',
				label : '프로젝트<br />코드',
				align : 'center',
				editable : true,
				editrules : {
					edithidden : true
				},
				formoptions : {
					label : '프로젝트 코드'
				}
			}, {
				name : 'projectName',
				label : '프로젝트<br />명칭',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true
				},
				formoptions : {
					label : '프로젝트 명칭'
				}
			}, {
				name : 'part',
				label : '산정자<br />파트',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${partVals}'
				},
				formoptions : {
					label : '산정자 파트'
				}
			}, {
				label : '산정자<br />아이디',
				name : 'op',
				hidden : true,
				editable : true,
				editrules : {
					edithidden : true
				},
				formoptions : {
					label : '산정자 아이디'
				}
			}, {
				label : '산정자',
				name : 'opName',
				align : 'center',
				editable : true,
				edittype : 'text',
				editrules : {
					required : true	
				},
				formoptions : {
					label : '산정자 이름'
				}
			}, {
				name : 'system',
				label : '시스템<br />특성',
				align : 'center',
				formatter : 'select',
				editable : true,
				edittype : 'select',
				editrules : {
					required : true
				},
				editoptions : {
					value : '${systemVals}'
				},
				formoptions : {
					label : '시스템 특성'
				}
			}, {
				name : 'fp',
				label : '최종<br />기능<br />점수',
				align : 'center',
				formatter : 'number',
				editable : false
			}, {
				name : 'mh',
				label : '예상<br />토입<br />MH',
				align : 'center',
				formatter : 'number',
				editable : false
			}, {
				name : 'editable',
				hidden : true
			} ],
			page : '${page}',
			rowNum : '${rowNum}',
			pager : '#jqg-pager',
			sortname : 'id',
			sortorder : 'desc',
			caption : '프로젝트 기능점수 목록',
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);
			},
			ondblClickRow : function(rowId) {
				var editable = $(this).jqGrid('getRowData', rowId).editable;
				if (eval('${navGrid.edit}') && eval(editable)) {
					$('#edit_jqg-table').click();
				} else {
					$('#view_jqg-table').click();
				}
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			zIndex : 100,
			recreateForm : true,			
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			},
			beforeShowForm : function(formId) {
				$('#project', formId).attr('disabled', 'disabled');
				$('#op', formId).attr('disabled', 'disabled');				
				applyProjectAutoComplete('#project');
				applyAccountAutoComplete('#op');
			}			
		}, {
			zIndex : 100,
			recreateForm : true,
			beforeShowForm : function(formId) {
				$('#project', formId).attr('disabled', 'disabled');
				$('#op', formId).attr('disabled', 'disabled');				
				applyProjectAutoComplete('#project');
				applyAccountAutoComplete('#op');
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});
	}

	function beforeInitDataEdit(formId, scope) {
		/*
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.edit}') && eval(editable)) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '${base_url}' + rowId + '/form?page=' + page + '&rowNum='
					+ rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
		*/
		return true;
	}

	function beforeInitDataDel(formId, scope) {
		/*
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		if (eval('${navGrid.del}') && eval(editable)) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
		*/
		return true;
	}

	function beforeInitDataView(formId, scope) {
		if (eval('${navGrid.view}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow');
			var url = '${base_url}' + rowId + '/view?page=' + page + '&rowNum='
					+ rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('조회 할 수 없습니다.');
			return false;
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 기능점수 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
				<button id="btn-view">프로그램·기능 상세</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />