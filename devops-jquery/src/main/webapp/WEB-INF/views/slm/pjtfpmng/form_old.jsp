<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--
VO의 id 값 유무로 등록인지 수정인지 판단 한다.
--%>
<c:set value="${empty projectFpResultVO.id}" var="isNew" />

<script type="text/javascript">
    <%--
    등록인 경우 jqGrid는 local 데이타를 소스로 한다.
    유효성 에러로 다시 포워딩 된 경우 이전에 입력된 jqGrid 값을 가지도록 한다.
    --%>
    
    
    $(function() {
    	
        /*
         * 등록 버튼
         */
        $('#btn-add1').button({
            icons: {
                primary: 'ui-icon-plus'
            }
        }).click(function(event) {
            $("#jqg-table1").jqGrid('editGridRow', 'new', {
                reloadAfterSubmit: true,
                recreateForm : true,
                closeAfterAdd : true
                
            });
        });
        /*
         * 수정 버튼
         */
        $('#btn-edit1').button({
            icons: {
                primary: 'ui-icon-pencil'
            }
        }).click(function(event) {
            $("#jqg-table1").jqGrid('editGridRow', $('#jqg-table2').jqGrid('getGridParam', 'selrow'), {
            	reloadAfterSubmit: true,
                recreateForm : true,
                closeAfterEdit : true
            });
        });
        /*
         * 삭제 버튼
         */
        $("#btn-del1").button({
            icons: {
                primary: 'ui-icon-trash'
            }
        }).click(function(event) {
            $("#jqg-table1").jqGrid('delGridRow', $('#jqg-table2').jqGrid('getGridParam', 'selrow'), {
                reloadAfterSubmit: true,
                recreateForm : true
            });
        });

        /*
         * 등록 버튼
         */
        $('#btn-add2').button({
            icons: {
                primary: 'ui-icon-plus'
            }
        }).click(function(event) {
            $("#jqg-table2").jqGrid('editGridRow', 'new', {
                reloadAfterSubmit: true,
                recreateForm : true,
                closeAfterAdd : true
                
            });
        });
        /*
         * 수정 버튼
         */
        $('#btn-edit2').button({
            icons: {
                primary: 'ui-icon-pencil'
            }
        }).click(function(event) {
            $("#jqg-table2").jqGrid('editGridRow', $('#jqg-table2').jqGrid('getGridParam', 'selrow'), {
            	reloadAfterSubmit: true,
                recreateForm : true,
                closeAfterEdit : true
            });
        });
        /*
         * 삭제 버튼
         */
        $("#btn-del2").button({
            icons: {
                primary: 'ui-icon-trash'
            }
        }).click(function(event) {
            $("#jqg-table2").jqGrid('delGridRow', $('#jqg-table2').jqGrid('getGridParam', 'selrow'), {
                reloadAfterSubmit: true,
                recreateForm : true
            });
        });
        
        /*
         * 저장 버튼
         */
        $('#btn-save').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        }).click(function(event) {
            $('#frm-master').submit();
        });
        /*
         * 취소 버튼
         */
        $('#btn-cancel').button({
            icons: {
                primary: 'ui-icon-cancel'
            }
        });

        //그리드 조건에 따라 변경처리
        $('#demandFunctionCode').change(function(){
        	setGridCombo();//그리드 콤보설정
        	setGridColHide();//그리드 컬럼 hide처리
        	gridReload($("#jqg-table2"));
        });
        $('#jobDivisionCode').change(function(){
        	setGridColHide();//그리드 컬럼 hide처리
        	gridReload($("#jqg-table2"));
        })
        
        
        //defalut설정
        setGridCombo();
        //setGridColHide();
    
    	//프로젝트 자동완성
		$('#project\\.name').autocomplete({
            autoFocus: true,
            mustMatch: true,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'name';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/pms/projects" />', request, response);
            },
            select: function(event, ui) {
            	$('#project\\.code').val(ui.item.code);
                $('#project\\.name').val(ui.item.name);
                return false;
            },
            change: function (event, ui) {
                /* mustmatch: */
                if (!ui.item) {
                	$('#project\\.code').val('');
                    $('#project\\.name').val('');
                }
            }
        }).data('ui-autocomplete')._renderItem = function(ul, item) {
			return $('<li>').append('<a>' + item.code + ', ' + item.name + '</a>').appendTo(ul);
        };

      //프로젝트 자동완성
		$('#project\\.code').autocomplete({
            autoFocus: true,
            mustMatch: true,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'code';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/pms/projects" />', request, response);
            },
            select: function(event, ui) {
            	$('#project\\.code').val(ui.item.code);
                $('#project\\.name').val(ui.item.name);
                return false;
            },
            change: function (event, ui) {
                /* mustmatch: */
                if (!ui.item) {
                	$('#project\\.code').val('');
                    $('#project\\.name').val('');
                }
            }
        }).data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li>').append('<a>' + item.code + ', ' + item.name + '</a>').appendTo(ul);
        };
        

        $('#operator\\.name').autocomplete({
            autoFocus: true,
            mustMatch: true,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'name';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/adm/accounts" />', request, response);
            },
            select: function(event, ui) {
                $('#operator\\.username').val(ui.item.username);
                $('#operator\\.name').val(ui.item.name);
                return false;
            },
            change: function (event, ui) {
                /* mustmatch: */
                if (!ui.item) {
                	$('#operator\\.username').val('');
                    $('#operator\\.name').val('');
                }
            }
        }).data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li>').append('<a>' + item.name + ', ' + item.team.name + '</a>').appendTo(ul);
        };
        
        $("#jqg-table1").jqGrid({
            url: '<s:url value="/rest/slm/programfpresult/${projectFpResultVO.id}"/>',
            editurl: '<s:url value="/rest/slm/programfpresult/${projectFpResultVO.id}/edit"/>',            
            mtype: 'POST',
            datatype: '${isNew ? "local" : "json"}',
            colNames: ['id', '프로그램명', '프로그램FP','예상MH'],
            colModel: [{
                    name: 'id',
                    hidden: true,
                    key: true
                    
                }, {
                    name: 'programName',
                    align : 'center',
                    editable: true,
                    edittype: 'text', 
                    editrules: {required: true}
                }, {
                    name: 'programFP',
                    align : 'center',
                	formatter :'number',
                	formatoptions : {decimalPlaces: 2}
                }, {
                    name: 'programExpectMH',
                    align : 'center',
                    edittype: 'text',
                    formatter :'number',
                    formatoptions : {decimalPlaces: 2}
                }],
            pager: '#jqg-pager1',
            rowNum: 10,
            rowList: [10, 30, 50],
            sortname: 'id',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            autowidth:true,
            height: "100%",
            caption: '프로그램',
            beforeRequest: function() {
            },
            loadComplete : function() {
            	setBtnDisable1(false, true, true);//grid1 : add, edit, del
            	setBtnDisable2(true, true, true);//grid2 : add, edit, del
			},
			onSelectRow : function(id) {
				setBtnDisable2(false, true, true);//add, edit, del
				$("#jqg-table2").setGridParam({ datatype: 'json' });
				gridReload($("#jqg-table2"));
			}
        });
        $("#jqg-table1").jqGrid('navGrid', '#jqg-pager1', {add: false, edit: false, del: false, search: false, refresh: false}, {}, {}, {}, {});

        
        $("#jqg-table2").jqGrid({
                        
            mtype: 'POST',
            datatype: 'local',
            colNames: ['id', '요구기능', '작업유형','테이블 및 기능명','기능유형','변경RET/FTR수','변경DET수',
                       '작업후<br/>RET/FTR수','작업전<br/>RET/FTR수','변경RET/FTR<br/>(추가)',
                       '변경RET/FTR<br/>(수정)','변경RET/FTR<br/>(삭제)', '작업전<br/>DET수','작업후<br/>DET수','변경DET<br/>(추가)',
                       '변경DET<br/>(수정)','변경DET<br/>(삭제)','RET/FTR<br/>변경률','DET<br/>변경률',
                       '난이도','기능<br/>점수','조정<br/>인자','최종<br/>FP' ],
            colModel: [{
                    name: 'id',
                    hidden: true,
                    key: true
                    
                }, {
                    name: 'demandFunction.name',
                    align : 'center',
                    editable: true,
                    search: false,
                    hidden: true,
                    edittype: 'select', 
                    editrules: {required: true},
                    editoptions : {	value : '${demandFunctionGrid}',
						dataInit : function(e) {
							$(e).val($('#demandFunctionCode').val());
						},
						disabled:true
                    
					}
                }, {
                    name: 'jobDivision.name',
                    align : 'center',
                    width :60,
                    editable: true,                    
                    hidden: true,
                    edittype: 'select',                    
                    editrules: {required: true},                        
                    editoptions : {	value : '${jobDivisionGrid}',
						dataInit : function(e) {
							$(e).val($('#jobDivisionCode').val());
						},
						disabled:true
					}                                                
                }, {
                    name: 'functionName',
                    align : 'left',
                    editable: true,
                    edittype: 'text',                    
                    editrules: {required: true}
                }, {
                    name: 'functionDivision.name',
                    align : 'center',
                    editable: true,
                    edittype: 'select',                    
                    editrules: {required: true},                        
                    editoptions : {	value : ''}
                }, {
                    name: 'changeFtrCount',
                    align : 'right',
                    width :60,
                    editable: true,
                    hidden: true,
                    edittype: 'text',
                    editoptions:{
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'changeDetCount',
                    align : 'right',
                    width :60,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'afterFtrCount',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'beforeFtrCount',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'addChangeFtr',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'editChangeFtr',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'delChangeFtr',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'afterDetCount',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'beforeDetCount',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'addChangeDet',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'editChangeDet',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'delChangeDet',
                    align : 'right',
                    width :80,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{
                    	dataInit: function (e) { $(e).mask("9?999"); },
                    	dataEvents:[{
                    		type: 'change',
                    		fn: function(e){
                    			getFpMarksDataNew();
                    		}
                    	}]
                    }
                }, {
                    name: 'rateChangeFtr',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{disabled:true},
                    formatter:setRateChangeFtr
                }, {
                    name: 'rateChangeDet',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{disabled:true},
                    formatter:setRateChangeDet
                }, {
                    name: 'level.name',
                    align : 'center',
                    width :60,
                    editable: true,
                    edittype: 'select',                    
                    editrules: {required: true},                        
                    editoptions : {	value : '${levelGrid}',
                    				disabled:true}
                }, {
                    name: 'fpMark',
                    align : 'right',
                    width :60,
                    editable: true,
                    edittype: 'text',
                    editrules: {required: true},
                    editoptions:{disabled:true}
                }, {
                    name: 'adjustArgument',
                    align : 'right',
                    width :60,
                    hidden: true,
                    editable: true,
                    edittype: 'text',
                    editoptions:{disabled:true}
                }, {
                    name: 'adjustFpMark',
                    align : 'right',
                    width :60,
                    hidden: false,
                    editable: true,
                    edittype: 'text',
                    editoptions:{disabled:true}
                }],
            pager: '#jqg-pager2',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'id',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            autowidth:true,
            height: "100%",
            caption: '기능상세',
            beforeRequest: function() {
				$("#jqg-table2").setGridParam({
					postData : {
						demandFunction : $("#demandFunctionCode").val(),
						jobDivision : $("#jobDivisionCode").val()
					}
				});
            },
            loadComplete : function() {
            	setBtnDisable2(false, true, true);//add, edit, del
            	<c:choose>
                <c:when test="${isNew}">
                </c:when>
                <c:otherwise>
            	$.ajax({
					type:'GET',
					url:'<s:url value="/slm/projectfpresult/get/${projectFpResultVO.id}"/>',
					success : function(data){
						$('#finalFP').val(data.finalFP);
						$('#expectMH').val(data.expectMH);
						}
					});
            	</c:otherwise>
                </c:choose>
			},
			onSelectRow : function(id) {
				setBtnDisable2(false, false, false);//add, edit, del
			}
        });
        $("#jqg-table2").jqGrid('navGrid', '#jqg-pager2', {add: false, edit: false, del: false, search: false, refresh: false}, {}, {}, {}, {});

        
        //그리드에 기능유형콤보 설정
        function setGridCombo(){
        	$.post(
        		'<s:url value="/rest/slm/getLinkCodeGrid"/>',
        		{parentId: "SLM30" , filterCode: $('#demandFunctionCode').val() },
        		function( data ){
        			$("#jqg-table2").setColProp('functionDivision.name', { 
                        editoptions: {
                            value : data
                        }
                    });
        		}
        	);
        };
        
        
        //그리드 재조회
        function gridReload(obj){
        	obj.jqGrid("clearGridData");
			//그리드 재조회
			obj.setGridParam({
				url: '<s:url value="/rest/slm/programfpResultDetail/'+$('#jqg-table1').jqGrid('getGridParam', 'selrow')+'"/>',
	            editurl: '<s:url value="/rest/slm/programfpResultDetail/'+$('#jqg-table1').jqGrid('getGridParam', 'selrow')+'/edit"/>',
				postData : {
					demandFunction : $("#demandFunctionCode").val(),
					jobDivision : $("#jobDivisionCode").val()
				}
			}).trigger("reloadGrid");
        	
        }
        
        //그리드 컬럼 변경
       	function setGridColHide(){
        	var demandFunction = $('#demandFunctionCode option:selected').text();
        	var jobDivision = $('#jobDivisionCode option:selected').text();
       		
       		var cm = $("#jqg-table2").jqGrid("getGridParam", "colModel");
   			
   			var hideCols = new Array();
   			for (var i = 0  ; i < cm.length ; i++){
   				hideCols[i] = (cm[i]['name']);
   			}
   			
   				
   			var showCols = null;
        	//데이터, 신규
        	if (demandFunction == "DATA" ){
        		if(jobDivision == "신규"){
        			showCols = ['functionName','functionDivision.name',
        	        			'changeDetCount','level.name','fpMark','adjustFpMark'];
		            //필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: false}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        			
        			
        		}else if(jobDivision == "수정"){
        			showCols = ['functionName','functionDivision.name',
        			            'beforeDetCount','afterDetCount','addChangeDet','editChangeDet',
        			            'delChangeDet','rateChangeDet','adjustArgument','level.name','fpMark','adjustFpMark']
        			//필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: true}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        		}else if(jobDivision == "삭제"){
        			showCols = ['functionName','functionDivision.name',
        			            'changeDetCount','level.name','fpMark','adjustFpMark']
        			//필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: false}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        		}
        	}else if ( demandFunction == "TRANSACTION" ){
        		if(jobDivision == "신규"){
        			showCols = ['functionName','functionDivision.name',
        			            'changeFtrCount', 'changeDetCount','level.name','fpMark','adjustFpMark']
        			//필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: false}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        		}else if(jobDivision == "수정"){
        			showCols = ['functionName','functionDivision.name',
        			            'beforeFtrCount','beforeDetCount','afterFtrCount','afterDetCount',
        			            'addChangeFtr','editChangeFtr','delChangeFtr',
        			            'addChangeDet','editChangeDet','delChangeDet',
        			            'rateChangeFtr','rateChangeDet','adjustArgument','level.name','fpMark','adjustFpMark']
        			//필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: true}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        		}else if(jobDivision == "삭제"){
        			showCols = ['functionName','functionDivision.name',
        			            'changeFtrCount','changeDetCount','level.name','fpMark','adjustFpMark']
        			//필수입력설정
        			$("#jqg-table2").jqGrid('setColProp', 'adjustArgument', {editrules: {required: false}});
        			$("#jqg-table2").jqGrid('setColProp', 'adjustFpMark', {editrules: {required: true}});
        		}
        	}
			
			var gw = $("#jqg-table2").jqGrid('getGridParam','width');
			
        	jQuery("#jqg-table2").jqGrid('hideCol',hideCols);
        	jQuery("#jqg-table2").jqGrid('showCol',showCols);
        	
        	//그리드 길이조정
        	$("#jqg-table2").jqGrid('setGridWidth',gw,true);

        }
        
        
        function getFpMarksDataNew(){

        	//요구기능, 작업유형, 기능유형
        	var dfText = $('#demandFunctionCode option:selected').text();
        	var jdText = $('#jobDivisionCode option:selected').text();
        	var fdText = $('#functionDivision\\.name option:selected').text();

        	//난이도 기능점수 계산
        	if (dfText == "DATA" && jdText == "신규" ){
       			//DATA일경우 변경ftr은 1로 고정
       			var cFtr = 1;
               	var cDet = $("#changeDetCount").val();
               	//난이도 및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cDet==""){
               		$("#fpMark").val("");
                }else{
                	$("#adjustFpMark").val($("#fpMark").val());
                }
        	}
        	
        	//난이도 기능점수 계산
        	if (dfText == "TRANSACTION" && jdText == "신규" ){
       			//DATA일경우 변경ftr은 1로 고정
       			var cFtr = $("#changeFtrCount").val();
               	var cDet = $("#changeDetCount").val();
               	//난이도 및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cFtr=="" || cDet==""){
               		$("#fpMark").val("");
                }else{
                	$("#adjustFpMark").val($("#fpMark").val());
                }
        	}
        	
        	//조정인자, 조정후 FP계산
        	if (dfText == "DATA" && jdText == "수정"){
        		//난이도, FP처리
           		var cFtr = 1;
               	var cDet = $("#afterDetCount").val();
               	//난이도및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cDet==""){
               		$("#fpMark").val("");
                }

   				//조정인자, 조정후 FP처리
   				var ftrChangeRate = "";
				var beforeDetCount = Number( $('#beforeDetCount').val());
				var changeDetCount = Number($('#addChangeDet').val()) + Number($('#editChangeDet').val()) + Number($('#delChangeDet').val());
				if (beforeDetCount == 0 || changeDetCount == 0){
					$('#rateChangeDet').val("");
					$("#adjustArgument").val("");
       				$("#adjustFpMark").val("");
				}else{
					var detChangeRate = Math.round(Number(changeDetCount/beforeDetCount)*1000)/1000;
					$('#rateChangeDet').val(detChangeRate);
					setAdjustFp(ftrChangeRate, detChangeRate, dfText, jdText, fdText);
				}
        	}

        	//조정인자, 조정후 FP계산
        	if (dfText == "TRANSACTION" && jdText == "수정"){
        		//난이도, FP처리
           		var cFtr = $("#afterFtrCount").val();
               	var cDet = $("#afterDetCount").val();
               	//난이도및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cFtr=="" || cDet==""){
               		$("#fpMark").val("");
                }

   				//조정인자, 조정후 FP처리
   				var beforeFtrCount = Number( $('#beforeFtrCount').val());
				var changeFtrCount = Number($('#addChangeFtr').val()) + Number($('#editChangeFtr').val()) + Number($('#delChangeFtr').val());
				var beforeDetCount = Number( $('#beforeDetCount').val());
				var changeDetCount = Number($('#addChangeDet').val()) + Number($('#editChangeDet').val()) + Number($('#delChangeDet').val());
				
				if (beforeFtrCount == 0 || changeFtrCount == 0){
					$('#rateChangeFtr').val("");
				}else{
					var ftrChangeRate = Math.round(Number(changeFtrCount/beforeFtrCount)*1000)/1000;
					$('#rateChangeFtr').val(ftrChangeRate);
				}
				if (beforeDetCount == 0 || changeDetCount == 0){
					$('#rateChangeDet').val("");
					
				}else{
					var detChangeRate = Math.round(Number(changeDetCount/beforeDetCount)*1000)/1000;
					$('#rateChangeDet').val(detChangeRate);
				}

				if( $('#rateChangeFtr').val() != "" && $('#rateChangeDet').val() != "" ){
					setAdjustFp(ftrChangeRate, detChangeRate, dfText, jdText, fdText);
				}else{
					$("#adjustArgument").val("");
       				$("#adjustFpMark").val("");
				}
        	}

        	//난이도 기능점수 계산
        	if (dfText == "DATA" && jdText == "삭제" ){
       			//DATA일경우 변경ftr은 1로 고정
       			var cFtr = 1;
               	var cDet = $("#changeDetCount").val();
               	//난이도 및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cDet==""){
               		$("#fpMark").val("");
                }
               	setAdjustFp("", "", dfText, jdText, fdText);
        	}

        	//난이도 기능점수 계산
        	if (dfText == "TRANSACTION" && jdText == "삭제" ){
       			//DATA일경우 변경ftr은 1로 고정
       			var cFtr = $("#changeFtrCount").val();
               	var cDet = $("#changeDetCount").val();
               	//난이도 및 기능점수 설정
               	setFp(cFtr, cDet, dfText, jdText, fdText);
               	if (cFtr=="" || cDet==""){
               		$("#fpMark").val("");
                }
               	setAdjustFp("", "", dfText, jdText, fdText);
        	}
        	
        }
      	//비교연산
    	function between(value, min, minOper, max, maxOper){
        	
    		var minCheck = false;
    		var maxCheck = false;
    		//최소값 확인
    		if (min == ""){
    			minCheck = true;
    		}else {
        		if (minOper="이상"){
    				Number(value) >= Number(min) ? minCheck = true : minCheck = false;
        		}else{
        			Number(value) > Number(min) ? minCheck = true : minCheck = false;
				}
    		}
    		
    		//최대값 확인
    		if (max == ""){
    			maxCheck = true;
    		}else {
    			if (minOper="이하"){
    				Number(value) <= Number(max) ? maxCheck = true : maxCheck = false;
    			}else{
    				Number(value) < Number(max) ? maxCheck = true : maxCheck = false;
        		}	
    		}
    		
    		return minCheck&&maxCheck;
    	}

      	//ftr병경률 계산 formatter
    	function setRateChangeFtr( cellvalue, options, rowObject ){
    		var beforeFtrCount = Number(rowObject.beforeFtrCount);
			var changeFtrCount = Number(rowObject.addChangeFtr)+Number(rowObject.editChangeFtr)+Number(rowObject.delChangeFtr);
			if (beforeFtrCount == 0){return "";}
			return Math.round(changeFtrCount/beforeFtrCount*1000)/1000;
    	}
    	//det병경률 계산 formatter
    	function setRateChangeDet( cellvalue, options, rowObject ){
    		var beforeDetCount = Number(rowObject.beforeDetCount);
			var changeDetCount = Number(rowObject.addChangeDet)+Number(rowObject.editChangeDet)+Number(rowObject.delChangeDet);
			if (beforeDetCount == 0){return "";}
			return Math.round(changeDetCount/beforeDetCount*1000)/1000;
    	}

    	//버튼disable제어
        function setBtnDisable1( add, edit, del){
    		if ( eval('${isNew}') == true ){
    			$("#btn-add1").button("option", "disabled", true);
            	$("#btn-edit1").button("option", "disabled", true);
            	$("#btn-del1").button("option", "disabled", true);
    		}else{    			
        		$("#btn-add1").button("option", "disabled", add);
        		$("#btn-edit1").button("option", "disabled", edit);
        		$("#btn-del1").button("option", "disabled", del);
    		}
        }
        
    	//버튼disable제어
        function setBtnDisable2( add, edit, del){
        	$("#btn-add2").button("option", "disabled", add);
    		$("#btn-edit2").button("option", "disabled", edit);
    		$("#btn-del2").button("option", "disabled", del);
        }

        //난이도, 기능점수 처리
        function setFp(cFtr, cDet, dfText, jdText, fdText){
        	//난이도및 기능점수 리스트에서 
        	//반복문을 돌리면서 난이도 및 기능점수 조회 및 설정
        	<c:forEach var="item" items="${levelList}">
        		if ("${item.demandFunction.name}" == dfText && "${item.jobDivision.name}" == jdText && "${item.functionDivision.name}" == fdText){
        			
        			if ( between(cFtr,"${item.ftrMin}","${item.ftrMinOperator.name}","${item.ftrMax}","${item.ftrMaxOperator.name}" ) && 
           				 between(cDet,"${item.detMin}","${item.detMinOperator.name}","${item.detMax}","${item.detMaxOperator.name}" ) ){
        				$("#fpMark").val("${item.functionPoint}");
        				$("#level\\.name").val("${item.level.id}");
        			}
        			
        		}
			</c:forEach>
		}

		//조정인자 및 조정후FP계산
		function setAdjustFp(ftrChangeRate, detChangeRate, dfText, jdText, fdText){
			<c:forEach var="item" items="${adjustArgumentList}">
			if ("${item.demandFunction.name}" == dfText && "${item.jobDivision.name}" == jdText && "${item.functionDivision.name}" == fdText){
				
				if (between(ftrChangeRate,"${item.ftrChangeRateMin}","${item.ftrChangeRateMinOperator.name}","${item.ftrChangeRateMax}","${item.ftrChangeRateMaxOperator.name}" ) && 
        			between(detChangeRate,"${item.detChangeRateMin}","${item.detChangeRateMinOperator.name}","${item.detChangeRateMax}","${item.detChangeRateMaxOperator.name}" ) ){
        				$("#adjustArgument").val("${item.adjustArgument}");
        				$("#adjustFpMark").val(    Math.round(Number("${item.adjustArgument}") * $("#fpMark").val()*100)/100  );
				}
			}
			</c:forEach>
		}
        setBtnDisable1(true, true, true);
        setBtnDisable2(true, true, true);
        
    });
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">프로젝트 FP</span>
    </div>
</div>
<div class="qms-content-main" id="main-div">
    <c:choose>
        <c:when test="${isNew}">
            <s:url value="/slm/projectfpResult/edit" var="action" />
        </c:when>
        <c:otherwise>
            <s:url value="/slm/projectfpResult/edit" var="action" />
        </c:otherwise>
    </c:choose>
    <div class="pure-g">
        <div class="pure-u-1-2">
        </div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
                <a id="btn-save">저장</a>
                <a id="btn-cancel" href="<c:url value='/slm/projectfp/projectfpresultList' />" >취소</a>
            </div>
        </div>
    </div>
    <form:form id="frm-master" action="${action}" method="${isNew ? 'POST' : 'PUT'}"
               modelAttribute="projectFpResultVO"
               cssClass="pure-form pure-form-stacked">
        <fieldset>
            <legend>프로젝트 FP 정보</legend>
            <div class="pure-g">
                <form:hidden path="id" />
                <div class="pure-u-1-4">
                	<form:label path="standardYear.name">기준년도</form:label>
                	<form:input path="standardYear.name" readOnly="true"/>
                    <form:errors path="standardYear.name" cssClass="ui-state-error-text" />
                </div>
                
                <div class="pure-u-1-4">
                    <form:label path="project.code">프로젝트코드</form:label>
                    <form:input path="project.code" placeholder="코드를 입력하세요." cssClass="pure-input-2-3"
                                readonly="${isNew ? 'false' : 'true'}" />
                    <form:errors path="project.code" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="project.name">프로젝트명</form:label>
                    <form:input path="project.name" placeholder="명을 입력하세요."
                                cssClass="pure-input-2-3"
                                readonly="${isNew ? 'false' : 'true'}" />
                    <form:errors path="project.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="operator.name">처리자</form:label>
                    <form:input path="operator.name" placeholder="처리자를 입력하세요"
                                cssClass="pure-input-2-3" />
                    <form:errors path="operator.name" cssClass="ui-state-error-text" />
                </div>
                
            </div>
            <div class="pure-g">
            	<div class="pure-u-1-4">
                    <form:label path="csrPart">파트</form:label>
                    <form:select path="csrPart" items="${csrSystemCode}"
                                 itemLabel="name" itemValue="id" />
                    <form:errors path="csrPart" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="systemType">시스템특성</form:label>
                    <form:select path="systemType" items="${systemTypeCode}"
                                 itemLabel="name" itemValue="id" />
                    <form:errors path="systemType" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="finalFP">FP점수</form:label>
                    <form:input path="finalFP" cssClass="pure-input-2-3" readonly="true"/>
                    <form:errors path="finalFP" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="expectMH">예상투입MH</form:label>
                    <form:input path="expectMH" cssClass="pure-input-2-3" readonly="true"/>
                    <form:errors path="expectMH" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <legend>프로그램 및 상세정보</legend>
        <form:hidden path="id" />
        <form:hidden path="standardYear.id" />
        <form:hidden path="standardYear.code" />
        <form:hidden path="operator.username" />
		<form:hidden path="createdBy" />
        <form:hidden path="createdDate" />
    </form:form>	
	<div class="pure-g">
		<div class="pure-u-2-3">
		</div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-add1">등록</a>
				<a id="btn-edit1">수정</a>
				<a id="btn-del1">삭제</a>
			</div>
		</div>
	</div>
	<table id="jqg-table1"></table>
	<div id="jqg-pager1"></div>
	
	<br/>
	
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form class="pure-form">
				<select id="demandFunctionCode">
					<c:forEach var="item" items="${demandFunctionCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
				<select id="jobDivisionCode">
					<c:forEach var="item" items="${jobDivisionCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</form>
		</div>
	</div>
	<div class="pure-g">
		<div class="pure-u-2-3">
		</div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<a id="btn-add2">등록</a>
				<a id="btn-edit2">수정</a>
				<a id="btn-del2">삭제</a>
			</div>
		</div>
	</div>	
	
	<table id="jqg-table2"></table>
	<div id="jqg-pager2"></div>
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">기능점수</span> 
	<span id="sidebar-menu">프로젝트 FP</span>
</div>
 -->
<br />