<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa  fa-warning fa-lg"></i><span class="qms-content-title-text">오류가 발생 하였습니다.</span>
    </div>
</div>
<div class="qms-content-main">
    <div class="pure-g">
    	<div class="pure-u-1">${error.message}</div>    	
    </div>
    <br />
</div>
<br />