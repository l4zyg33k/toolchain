<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/dms/docmng" var="urlformselect" />
<script type="text/javascript">
	$(function() {
		$('#btn-download')
				.button({
					icons : {
						primary : 'fa fa-download'
					}
				})
				.attr(
						'href',
						'<c:url value="/download?path=${docMngFrmVO.filePath}&fileName=${docMngFrmVO.fileName}"/>');

		$('#btn-rejected')
				.button({
					icons : {
						primary : 'fa fa-ban'
					}
				})
				.click(
						function(event) {
							location.href = parseUrl('/dms/docmng/revisions/${rejectedRevisionId}/view?${docMngFrmVO.getPageUrl()}');
						});
		$('#btn-checkined')
				.button({
					icons : {
						primary : 'fa fa-lock'
					}
				})
				.click(
						function(event) {
							location.href = parseUrl('/dms/docmng/${docMngFrmVO.id}/view/?${docMngFrmVO.getPageUrl()}');
						});
		$('#btn-checkout')
				.button({
					icons : {
						primary : 'fa fa-unlock'
					}
				})
				.click(
						function(event) {
							location.href = parseUrl('/dms/docmng/${docMngFrmVO.id}/checkout/?${docMngFrmVO.getPageUrl()}');
						});
		$('#btn-cancel')
				.button({
					icons : {
						primary : 'fa fa-undo'
					}
				})
				.click(
						function() {
							dialog(
									"체크아웃 취소 하시겠습니까?",
									function(event) {
										$('#frm-document')
												.attr('action',
														"<c:url value="/dms/docmng/revisions/${docMngFrmVO.documentRevision}/revert"/>")
												.removeAttr('enctype').submit();
									});
						});
		$('#btn-checkin').button({
			icons : {
				primary : 'fa fa-lock'
			}
		}).click(
				function(event) {
					$('#frm-document').attr('action',
							"<c:url value="/dms/docmng/checkin"/>").submit();
				});
		$('#btn-approve').button({
			icons : {
				primary : 'fa fa-gavel'
			}
		}).click(
				function() {
					dialog("승인 하시겠습니까?", function(event) {
						$('#frm-document').attr('action',
								"<c:url value="/dms/docmng/approve"/>")
								.submit();
					});
				});
		$('#btn-reject').button({
			icons : {
				primary : 'fa fa-ban'
			}
		}).click(
				function() {
					dialog("반송하시겠습니까?",
							function(event) {
								$('#frm-document').attr('action',
										"<c:url value="/dms/docmng/reject"/>")
										.submit();
							});
				});
		$('#btn-delete')
				.button({
					icons : {
						primary : 'fa fa-trash'
					}
				})
				.click(
						function() {
							dialog(
									"삭제요청을 승인하시겠습니까?",
									function(event) {
										$('#frm-document')
												.attr('action',
														"<c:url value="/dms/docmng/revisions/${docMngFrmVO.documentRevision}/delete"/>")
												.removeAttr('enctype').submit();
									});
						});
		$('#btn-deleterequest')
				.button({
					icons : {
						primary : 'fa fa-trash'
					}
				})
				.click(
						function() {
							infomation("변경내용에 삭제요청 사유를 기입하시고 삭제요청 버튼을 다시 클릭하세요.");
							$('#contents')
									.removeAttr('readOnly')
									.val(
											$('#contents').text()
													+ '\n--------------------------------------------------------------------------------\n삭제사유: ')
									.focus();
							$(this)
									.unbind("click")
									.click(
											function() {
												dialog(
														"삭제요청하시겠습니까?",
														function(event) {
															$('#frm-document')
																	.attr(
																			'action',
																			"<c:url value="/dms/docmng/deleteRequest"/>")
																	.submit();
														});
											});
						});
		$('#btn-create').button({
			icons : {
				primary : 'fa fa-plus'
			}
		}).click(
				function(event) {
					$('#frm-document').attr('action',
							"<c:url value="/dms/docmng"/>").submit();
				});
		$('#btn-save').button({
			icons : {
				primary : 'fa fa-floppy-o'
			}
		}).click(
				function(event) {
					$('#frm-document').attr('action',
							"<c:url value="/dms/docmng/save"/>").submit();
				});
		$('#btn-back').button({
			icons : {
				primary : 'fa fa-list'
			}
		});

		$('#division')
				.change(
						function() {
							$
									.post(
											'${urlformselect}/systems/',
											{
												"_search" : true,
												"searchField" : "division.id",
												"searchString" : this.value,
												"searchOper" : "eq"
											},
											function(data) {
												var len = data.length;
												var systemId;
												$('#system').empty();
												for (var i = 0; i < len; i++) {
													if (i == 0) {
														systemId = data[i].id;
													}
													$('#system')
															.append(
																	"<option value='" + data[i].id + "'>"
																			+ data[i].name
																			+ "</option>");
												}
												if (systemId != null) {
													$
															.post(
																	'${urlformselect}/documentcategories/',
																	{
																		"_search" : true,
																		"searchField" : "system.id",
																		"searchString" : systemId,
																		"searchOper" : "eq"
																	},
																	function(
																			data) {
																		var len = data.length;
																		$(
																				'#documentCategory')
																				.empty();
																		if (len == null
																				|| len == 0) {
																			$(
																					'#documentCategory')
																					.append(
																							"<option value=''>선택</option>");
																		}
																		for (var i = 0; i < len; i++) {
																			if (i == 0) {
																				$(
																						'#documentCategory')
																						.append(
																								"<option value=''>전체</option>");
																			}
																			$(
																					'#documentCategory')
																					.append(
																							"<option value='" + data[i].id + "'>"
																									+ data[i].name
																									+ "</option>");
																		}
																		isSavable(len);
																	});
												} else { // if systemId is null then
													$('#system').empty();
													$('#system')
															.append(
																	"<option value=''>선택</option>");
													$('#documentCategory')
															.empty();
													$('#documentCategory')
															.append(
																	"<option value=''>선택</option>");
												}
											}, 'json');
						});
		$('#system').change(
				function() {
					$.post('${urlformselect}/documentcategories/', {
						"_search" : true,
						"searchField" : "system.id",
						"searchString" : this.value,
						"searchOper" : "eq"
					}, function(data) {
						var len = data.length;
						$('#documentCategory').empty();
						if (len == null || len == 0) {
							$('#documentCategory').append(
									"<option value=''>선택</option>");
						}
						for (var i = 0; i < len; i++) {
							if (i == 0) {
								$('#documentCategory').append(
										"<option value=''>전체</option>");
							}
							$('#documentCategory').append(
									"<option value='" + data[i].id + "'>"
											+ data[i].name + "</option>");
						}
						isSavable(len);
					});
				});

		$('#documentCategory')
				.change(
						function() {
							if (this.value != null && this.value != "") {
								$
										.post(
												'${urlformselect}/getmaxseq/'
														+ this.value,
												function(data) {
													$('#seq')
															.val(
																	('000' + data)
																			.substr(('000' + data).length - 4));
												});
							} else {
								$('#seq').val('');
							}
						});
		$('#seq').val(
				('000' + $('#seq').val())
						.substr(('000' + $('#seq').val()).length - 4));
		$('#seq').mask('9999');

	});

	function isSavable(len) {
		$('#btn-create').button({
			disabled : len < 1
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i>&nbsp;<span>운영문서</span>
	</div>
</div>
<div class="qms-content-buttonset">
	<c:if
		test="${not empty docMngFrmVO.code and not empty docMngFrmVO.fileName and empty exception.message}">
		<a id="btn-download">다운로드</a>
	</c:if>
	<c:if test="${buttons.btn_rejected}">
		<a id="btn-rejected">반송내용보기</a>
	</c:if>
	<c:if test="${buttons.btn_checkined}">
		<a id="btn-checkined">최종버전보기</a>
	</c:if>
	<c:if test="${buttons.btn_checkout}">
		<a id="btn-checkout">체크아웃</a>
	</c:if>
	<c:if test="${buttons.btn_cancel}">
		<a id="btn-cancel">체크아웃 취소</a>
	</c:if>
	<c:if test="${buttons.btn_checkin}">
		<a id="btn-checkin">체크인</a>
	</c:if>
	<c:if test="${buttons.btn_approve}">
		<a id="btn-approve">승인</a>
	</c:if>
	<c:if test="${buttons.btn_save}">
		<a id="btn-save">임시저장</a>
	</c:if>
	<c:if test="${buttons.btn_create}">
		<a id="btn-create">등록</a>
	</c:if>
	<c:if test="${buttons.btn_reject}">
		<a id="btn-reject">반송</a>
	</c:if>
	<c:if test="${buttons.btn_delete}">
		<a id="btn-delete">삭제</a>
	</c:if>
	<c:if test="${buttons.btn_deleterequest}">
		<a id="btn-deleterequest">삭제요청</a>
	</c:if>
	<a id="btn-back" href="<c:url value='${docMngFrmVO.getPrevUrl()}' />">목록으로</a>
</div>
<div class="qms-content-main">
	<form:form id="frm-document" action="${docMngFrmVO.getActionUrl()}"
		method="${docMngFrmVO.getMethod()}" modelAttribute="docMngFrmVO"
		cssClass="pure-form pure-form-stacked" enctype="multipart/form-data">
		<fieldset>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="divisionName">사업부</form:label>
					<c:choose>
						<c:when test="${not empty docMngFrmVO.statusValue}">
							<form:input path="divisionName" readonly="true"
								cssClass="pure-u-7-8" />
							<form:hidden path="division" />
						</c:when>
						<c:otherwise>
							<form:select path="division" items="${docMngCtlVO.divisions}"
								itemLabel="name" itemValue="id" cssClass="pure-u-7-8" />
						</c:otherwise>
					</c:choose>
					<form:errors path="division" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="systemName">시스템</form:label>
					<c:choose>
						<c:when test="${not empty docMngFrmVO.statusValue}">
							<form:input path="systemName" readonly="true"
								cssClass="pure-u-7-8" />
							<form:hidden path="system" />
						</c:when>
						<c:otherwise>
							<form:select path="system" items="${docMngCtlVO.systems}"
								itemLabel="name" itemValue="id" cssClass="pure-u-7-8" />
						</c:otherwise>
					</c:choose>
					<form:errors path="system" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="documentCategoryName">카테고리</form:label>
					<c:choose>
						<c:when test="${not empty docMngFrmVO.statusValue}">
							<form:input path="documentCategoryName" readonly="true"
								cssClass="pure-u-7-8" />
							<input type="hidden" id="documentCategory"
								name="documentCategory" value="${docMngFrmVO.documentCategory}" />
						</c:when>
						<c:otherwise>
							<form:select path="documentCategory"
								items="${docMngCtlVO.categories}" itemLabel="name"
								itemValue="id" cssClass="pure-u-7-8" required="true" />
						</c:otherwise>
					</c:choose>
					<form:errors path="documentCategory" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="code">문서코드</form:label>
					<form:input path="code" readonly="true"
						title="문서코드는 선택하신 사업부+시스템+카테고리+일련번호에 따라 자동생성됩니다."
						cssClass="pure-u-7-8" />
					<form:errors path="code" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-1">
					<form:label path="name">문서명</form:label>
					<form:input path="name" required="true"
						readonly="${not empty docMngFrmVO.statusValue}"
						placeholder="문서명을 입력해 주십시오." cssClass="pure-u-1-2" />
					<form:errors path="name" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="seq">일련번호</form:label>
					<form:input path="seq" required="true"
						readonly="${not empty docMngFrmVO.status}" maxlength="4"
						title="0부터시작하는 4자리입력<br/>예) 0001 또는 0101<br/><br/>문서종류 내에서 자동채번되나 확인 후 등록바랍니다."
						placeholder="0000" cssClass="pure-u-7-8" />
					<form:errors path="seq" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="version">문서버전</form:label>
					<form:input path="version" readonly="true" cssClass="pure-u-7-8" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="statusName">문서상태</form:label>
					<form:input path="statusName" readonly="true" cssClass="pure-u-7-8" />
					<form:hidden path="status" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="documentRevision" />
					<form:label path="occasion">변경근거</form:label>
					<form:input path="occasion" required="true"
						readonly="${docMngFrmVO.statusValue != 'CO' && not empty docMngFrmVO.id}"
						value="${empty docMngFrmVO.code ? '최초등록': ''}"
						title="CSR번호 또는 프로젝트코드" cssClass="pure-u-7-8" />
					<form:errors path="occasion" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:label path="contents">변경내용</form:label>
					<form:textarea path="contents" class="pure-u-1"
						readonly="${docMngFrmVO.statusValue != 'CO' && not empty docMngFrmVO}" />
					<form:errors path="contents" cssClass="ui-state-error-text" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="sysopName">운영자</form:label>
					<form:input path="sysopName" readonly="true" cssClass="pure-u-7-8" />
					<form:hidden path="sysop" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="approbatorName">검토자</form:label>
					<form:input path="approbatorName" readonly="true"
						cssClass="pure-u-7-8" />
					<form:hidden path="approbator" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="approvedDate">검토일자</form:label>
					<form:input path="approvedDate" readonly="true"
						cssClass="pure-u-7-8" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:label path="comment">검토의견</form:label>
					<form:textarea path="comment" class="pure-u-1"
						readonly="${docMngFrmVO.statusValue!='EX' && docMngFrmVO.statusValue!='DE'}" />
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1">
					<form:label path="fileDescription">파일설명</form:label>
					<form:textarea path="fileDescription" class="pure-u-1"
						readonly="${docMngFrmVO.statusValue != 'CO' && not empty docMngFrmVO.id}"
						placeholder="파일설명을 입력해 주십시오." required="true" />
					<form:errors path="fileDescription" cssClass="ui-state-error-text" />
				</div>
			</div>
			<c:if test="${not empty docMngFrmVO.document}">
				<div class="pure-g">
					<div class="pure-u-1-1">
						<form:label path="fileName" class="pure-u-1-3">파일</form:label>
						<a class="pure-u-2-3"
							href='<c:url value="/download?path=${docMngFrmVO.filePath}&fileName=${docMngFrmVO.fileName}"/>'
							title="다운로드하기">${docMngFrmVO.fileName}</a>
					</div>
				</div>
			</c:if>
			<c:if
				test="${docMngFrmVO.statusValue=='CO' || empty docMngFrmVO.document}">
				<div class="pure-g">
					<div class="pure-u-1">
						<form:label path="file">문서파일</form:label>
						<form:input path="file" class="pure-u-1" type="file" />
						<form:errors path="file" cssClass="ui-state-error-text" />
					</div>
				</div>
			</c:if>
		</fieldset>
		<form:hidden path="document" />
		<form:hidden path="page" />
		<form:hidden path="rowNum" />
		<form:hidden path="rowId" />
		<form:hidden path="ddlDivisionId" />
		<form:hidden path="ddlSystemId" />
		<form:hidden path="ddlCategoryId" />
		<form:hidden path="statusValue" />
		<form:hidden path="keyword" />
	</form:form>
</div>
<br />