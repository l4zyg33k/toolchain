<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/dms/docmng" var="urlformselect" />
<script type="text/javascript">
  $(function() {
    initButtons();
    initJqGrid();
    initNavGrid();
  });

  function initButtons() {
    <c:if test="${not empty docMngCtlVO.systems}">
    setAddButton('#btn-add', false, '#add_jqg-table');
    setEditButton('#btn-edit', false, '#edit_jqg-table');
    </c:if>
    setViewButton('#btn-view', false, '#view_jqg-table');

    $("#toggle").buttonset();
    $('.qms-content-buttonset').css('display', 'block');

    $("#btn-excel").button({
      icons: {
        primary: 'ui-icon-disk'
      }
    }).click(function() {
      $('#excelForm #data').val(JSON.stringify($("#jqg-table").getRowData()));
      $('#excelForm').submit();
    });
    $("#btn-search").button({
      icons: {
        primary: 'fa fa-filter'
      }
    }).click(function() {
      $("#jqg-table").setGridParam({
        postData: {
          "_search": true,
          "searchField": 'document.name',
          "searchString": $("#docMngCtlVO\\.keyword").val(),
          "searchOper": 'cn',
          divisionId: $('#docMngCtlVO\\.division').val(),
          systemId: $('#docMngCtlVO\\.system').val(),
          categoryId: $('#docMngCtlVO\\.category').val(),
          filters: getSchStatus()
        },
        page: 1
      }).trigger("reloadGrid");
    });

    $('#docMngCtlVO\\.division').change(
            function() {
              $.post('${urlformselect}/systems/', {
                "_search": true,
                "searchField": "division.id",
                "searchString": this.value,
                "searchOper": "eq"
              }, function(data) {
                var len = data.length;
                var systemId;
                $('#docMngCtlVO\\.system').empty();
                for (var i = 0; i < len; i++) {
                  if (i == 0) {
                    systemId = data[i].id;
                    $('#docMngCtlVO\\.system').append(
                            "<option value=''>전체</option>");
                  }
                  $('#docMngCtlVO\\.system').append(
                          "<option value='" + data[i].id + "'>" + data[i].name
                                  + "</option>");
                }
                $('#docMngCtlVO\\.category').empty();
                $('#docMngCtlVO\\.category').append(
                        "<option value=''>전체</option>");
              }, 'json');
              $("#jqg-table").setGridParam({
                postData: {
                  divisionId: $('#docMngCtlVO\\.division').val(),
              	systemId : '',
                categoryId: ''
                },
                page: 1
              }).trigger("reloadGrid");
            });
    $('#docMngCtlVO\\.system').change(
            function() {
              $.post('${urlformselect}/documentcategories/', {
                "_search": true,
                "searchField": "system.id",
                "searchString": (this.value == "" || this.value == null) ? -1
                        : this.value,
                "searchOper": "eq"
              }, function(data) {
                var len = data.length;
                $('#docMngCtlVO\\.category').empty();

                if (len == null || len == 0) {
                  $('#docMngCtlVO\\.category').append(
                          "<option value=''>전체</option>");
                }

                for (var i = 0; i < len; i++) {
                  if (i == 0) {
                    $('#docMngCtlVO\\.category').append(
                            "<option value=''>전체</option>");
                  }
                  $('#docMngCtlVO\\.category').append(
                          "<option value='" + data[i].id + "'>" + data[i].name
                                  + "</option>");
                }
                $("#jqg-table").setGridParam({
                  postData: {
                    divisionId: $('#docMngCtlVO\\.division').val(),
                    systemId: $('#docMngCtlVO\\.system').val(),
                    categoryId: $('#docMngCtlVO\\.category').val()
                  },
                  page: 1
                }).trigger("reloadGrid");
              });
            });
    $('#docMngCtlVO\\.category').change(function(event) {
      $("#jqg-table").setGridParam({
        postData: {
          systemId: $('#docMngCtlVO\\.system').val(),
          categoryId: $('#docMngCtlVO\\.category').val()
        },
        page: 1
      }).trigger("reloadGrid");
    });

    <c:if test="${not empty docMngCtlVO.systems}">
    $('#docMngCtlVO\\.keyword').autocomplete({
      autoFocus: true,
      minLength: 2,
      source: function(request, response) {
        request['divisionId'] = $('#docMngCtlVO\\.division').val();
        request['systemId'] = $('#docMngCtlVO\\.system').val();
        request['categoryId'] = $('#docMngCtlVO\\.category').val();
        request['_search'] = true;
        request['searchField'] = 'name';
        request['searchString'] = request['term'];
        request['searchOper'] = 'cn';

        $.post('<s:url value="/rest/dms/docmng/search" />', request, response);
      },
      select: function(event, ui) {
        $('#docMngCtlVO\\.keyword').val(ui.item.name);
        return false;
      }
    }).data('ui-autocomplete')._renderItem = function(ul, item) {
      return $('<li>').append('<a>' + item.code + ', ' + item.name + '</a>')
              .appendTo(ul);
    };
    </c:if>
  }

  function initJqGrid() {

    var mainGridPrefix = 's_';
    $("#jqg-table")
            .jqGrid(
                    {
                      url: '<s:url value="/rest/dms/docmng" />',
                      postData: {
                        "_search": true,
                        "searchField": 'document.name',
                        "searchString": $("#docMngCtlVO\\.keyword").val(),
                        "searchOper": 'cn',
                        divisionId: $('#docMngCtlVO\\.division').val(),
                        systemId: $('#docMngCtlVO\\.system').val(),
                        categoryId: $('#docMngCtlVO\\.category').val()
                      },
                      datatype: 'json',
                      mtype: 'POST',
                      colNames: ['id', '문서코드', '사업부', '시스템', '문서종류', '문서명',
                          '버전', '상태', '개정자', '검토자', '최종검토일'],
                      colModel: [{
                        name: 'id',
                        index: 'document.id',
                        key: true,
                        hidden: true
                      }, {
                        name: 'code',
                        index: 'document.code',
                        align: 'center',
                        width: 50,
                        sortable: false
                      }, {
                        name: 'divisionName',
                        index: 'document.category.system.division.name',
                        align: 'left',
                        width: 60,
                        search: false,
                        sortable: false
                      }, {
                        name: 'systemName',
                        index: 'document.category.system.name',
                        align: 'left',
                        width: 60,
                        search: false,
                        sortable: false
                      }, {
                        name: 'categoryName',
                        index: 'document.category.name',
                        align: 'left',
                        width: 80,
                        sortable: false
                      }, {
                        name: 'name',
                        index: 'document.name',
                        sortable: false
                      }, {
                        name: 'version',
                        align: 'center',
                        width: 30,
                        search: false,
                        sortable: false
                      }, {
                        name: 'statusName',
                        align: 'center',
                        width: 50,
                        search: false,
                        sortable: false
                      }, {
                        name: 'sysop',
                        align: 'center',
                        width: 50,
                        search: false,
                        sortable: false
                      }, {
                        name: 'approvator',
                        align: 'center',
                        width: 50,
                        search: false,
                        sortable: false
                      }, {
                        name: 'approvedDate',
                        align: 'center',
                        search: false,
                        width: 60,
                        sortable: false
                      }],
                      pager: '#jqg-pager',
                      page: '${page}',
                      rowNum: '${rowNum}',
                      sortname: 'code',
                      sortorder: 'asc',
                      viewrecords: true,
                      gridview: true,
                      autoencode: true,
                      height: '100%',
                      caption: '운영문서 목록',
                      subGrid: true,
                      beforeProcessing: function(data) {
                        var rows = data.content, l = rows.length, i, item, subgrids = {};
                        for (i = 0; i < l; i++) {
                          item = rows[i];
                          if (item.revisions) {
                            subgrids[item.id] = item.revisions;
                          }
                        }
                        data.userdata = subgrids;
                      },
                      subGridRowExpanded: function(subgridDivId, rowId) {
                        var $subgrid = $("<table id='" + subgridDivId + "_t'></table>"), pureRowId = $.jgrid
                                .stripPref(mainGridPrefix, rowId), subgrids = $(
                                this).jqGrid("getGridParam", "userData");

                        $subgrid.appendTo("#" + $.jgrid.jqID(subgridDivId));
                        $subgrid
                                .jqGrid({
                                  datatype: "local",
                                  data: subgrids[pureRowId],
                                  colNames: ['버전', '문서상태', '개정자', '변경근거',
                                      '변경내역', '결재일자', '파일'],
                                  colModel: [
                                      {
                                        name: 'version',
                                        width: 40,
                                        align: 'center'
                                      },
                                      {
                                        name: 'status.name',
                                        width: 50,
                                        align: 'center'
                                      },
                                      {
                                        name: 'sysop.name',
                                        width: 60,
                                        align: 'center'
                                      },
                                      {
                                        name: 'occasion',
                                        width: 80,
                                        align: 'center'
                                      },
                                      {
                                        name: 'contents'
                                      },
                                      {
                                        name: 'approvedDate',
                                        width: 50,
                                        align: 'center'
                                      },
                                      {
                                        name: 'fileName',
                                        width: 30,
                                        align: 'center',
                                        formatter: function(cellValue, options,
                                                rowObject) {
                                          if (cellValue != null) {
                                            return '<a href="<c:url value="/download?"/>path='
                                                    + rowObject.filePath
                                                    + '&fileName='
                                                    + cellValue
                                                    + '" id="fileLink" class="fa fa-paperclip" style="text-decoration: none" title="내려받기 '
                                                    + cellValue + '"></a>';
                                          } else {
                                            return '';
                                          }
                                          ;
                                        }
                                      }],
                                  sortname: "version, id",
                                  sortorder: "desc, desc",
                                  height: "100%",
                                  rowNum: 10000,
                                  autoencode: true,
                                  autowidth: true,
                                  jsonReader: {
                                    repeatitems: false,
                                    id: "id"
                                  },
                                  gridview: true,
                                  idPrefix: rowId + "_",
                                  onSelectRow: function(id) {
                                    $('#btn-edit').button('option', 'disabled',
                                            false);
                                    $('tr#' + rowId).click();
                                  }
                                });
                        $('#gview_' + subgridDivId + '_t > .ui-jqgrid-titlebar')
                                .hide();
                        $(
                                '#gview_' + subgridDivId
                                        + '_t > .ui-jqgrid .ui-state-highlight')
                                .css({
                                  "background-color": "blue"
                                });
                      },
                      loadComplete: function() {
                        $('#btn-edit').button("option", "disabled", true);
                      },
                      onSelectRow: function(id) {
                        $('#btn-edit').button('option', 'disabled', false);
                      },
                      searchoptions: {
                        searchOperators: true,
                        sopt: ["gt", "eq"]
                      },
                      ondblClickRow: function(id) {
                        $('#view_jqg-table').click();
                      }
                    });
  }

  function initNavGrid() {
    $("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
      view: true,
      edit: false,
      del: false
    }, {}, {
      beforeInitData: function(formId) {
        return beforeInitDataAdd(formId, this);
      }
    }, {
      beforeInitData: function(formId) {
        return beforeInitDataDel(formId, this);
      }
    }, {
      multipleSearch: true
    }, {
      beforeInitData: function(formId) {
        return beforeInitDataView(formId, this);
      }
    });
  }

  function beforeInitDataEdit(formId, scope) {
    var rowId = $(scope).jqGrid('getGridParam', 'selrow');
    var page = $(scope).jqGrid('getGridParam', 'page');
    var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
    var sp = new SearchPanel();
    sp.refresh();
    var url = '<s:url value="/dms/docmng/" />' + rowId + '/form?page=' + page
            + '&rowNum=' + rowNum + '&rowId=' + rowId + sp.toString();
    $(location).attr('href', url);
    return false;
  }

  function beforeInitDataAdd(formId, scope) {
    var page = $(scope).jqGrid('getGridParam', 'page');
    var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
    var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
    var sp = new SearchPanel();
    sp.refresh();
    var url = '<s:url value="/dms/docmng/form?page=" />' + page + '&rowNum='
            + rowNum + '&rowId=' + rowId + sp.toString();
    $(location).attr('href', url);
    return false;
  }

  function beforeInitDataDel(formId, scope) {
    return true;
  }

  function beforeInitDataView(formId, scope) {
    var page = $(scope).jqGrid('getGridParam', 'page');
    var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
    var rowId = $(
            '#jqg-table_' + $(scope).jqGrid('getGridParam', 'selrow') + '_t')
            .jqGrid('getGridParam', 'selrow');

    var sp = new SearchPanel();
    sp.refresh();
    var url = "";

    if (rowId == null || rowId == '') {
      rowId = '_' + $(scope).jqGrid('getGridParam', 'selrow');
      url = '<s:url value="/dms/docmng/" />' + rowId.split("_")[1]
              + '/view?page=' + page + '&rowNum=' + rowNum + '&rowId='
              + rowId.split("_")[1] + sp.toString();
    } else {
      url = '<s:url value="/dms/docmng/revisions/" />' + rowId.split("_")[1]
              + '/view?page=' + page + '&rowNum=' + rowNum + '&rowId='
              + rowId.split("_")[1] + sp.toString();
    }

    $(location).attr('href', url);
    return false;
  }

  function SearchPanel(division, system, category, keyword) {
    var division = division;
    var system = system;
    var category = category;
    var keyword = keyword;
    this.toString = function() {
      return "&ddlDivisionId=" + this.division + "&ddlSystemId=" + this.system
              + "&ddlCategoryId=" + this.category + "&keyword=" + this.keyword
    };
    this.getDivision = function() {
      return this.division
    };
    this.getSystem = function() {
      return this.system
    };
    this.getCategory = function() {
      return this.category
    };
    this.getKeyword = function() {
      return this.keyword
    };
    this.refresh = function() {
      this.division = $('#docMngCtlVO\\.division').val();
      this.system = $('#docMngCtlVO\\.system').val();
      this.category = $('#docMngCtlVO\\.category').val();
      this.keyword = $('#docMngCtlVO\\.keyword').val();
    };
  }

  function getSchStatus() {
    var arrRule = new Array();
    if ($('input:checkbox[id="checkined"]').is(":checked")) {
      arrRule.push({
        field: 'status.value',
        op: 'eq',
        data: 'CI'
      });
    }
    if ($('input:checkbox[id="checkouted"]').is(":checked")) {
      arrRule.push({
        field: 'status.value',
        op: 'eq',
        data: 'CO'
      });
    }
    if ($('input:checkbox[id="review"]').is(":checked")) {
      arrRule.push({
        field: 'status.value',
        op: 'eq',
        data: 'EX'
      });
    }
    if ($('input:checkbox[id="rejected"]').is(":checked")) {
      arrRule.push({
        field: 'status.value',
        op: 'eq',
        data: 'RE'
      });
    }
    if ($('input:checkbox[id="deleting"]').is(":checked")) {
      arrRule.push({
        field: 'status.value',
        op: 'eq',
        data: 'DE'
      });
    }
    return JSON.stringify({
      groupOp: 'OR',
      rules: arrRule
    });
  }
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i>&nbsp;<span>운영문서</span>
	</div>
</div>
<div class="qms-content-buttonset">
	<c:if test="${not empty docMngCtlVO.systems}">
		<a id="btn-add">등록</a>
		<a id="btn-edit">상세보기</a>
		<!-- <a id="btn-excel">엑셀저장</a>  -->
	</c:if>
</div>
<div class="qms-content-buttonset"></div>
<div class="qms-content-main">
	<form class="pure-form pure-form-stacked qms-content-searchpanel"
		id="search-panel" onsubmit="$('#btn-search').click();return false;">
		<div class="pure-g">
			<div class="pure-u-1-6">
				<label for="docMngCtlVO.division">사업부</label> <select
					id="docMngCtlVO.division" style="width: 140px">
					<c:forEach var="item" items="${docMngCtlVO.divisions}">
						<option value="<c:out value="${item.id}"/>"
							<c:if test="${item.id eq docMngCtlVO.ddlDivisionId}">selected</c:if>><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</div>
			<div class="pure-u-1-6">
				<label for="docMngCtlVO.system">시스템</label> <select
					id="docMngCtlVO.system" style="width: 140px">
					<c:forEach var="item" items="${docMngCtlVO.systems}">
						<option value="<c:out value="${item.id}"/>"
							<c:if test="${item.id eq docMngCtlVO.ddlSystemId}">selected</c:if>><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</div>
			<div class="pure-u-1-6">
				<label for="docMngCtlVO.category">문서종류</label> <select
					id="docMngCtlVO.category" style="width: 140px">
					<c:forEach var="item" items="${docMngCtlVO.categories}">
						<option value="<c:out value="${item.id}"/>"
							<c:if test="${item.id eq docMngCtlVO.ddlCategoryId}">selected</c:if>><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</div>
			<div class="pure-u-1-6">
				<label>문서상태</label>
				<div id="toggle" class="pure-g" style="padding: 0 0 0 0">
					<div class="pure-u-1-5">
						<input type="checkbox" id="checkined" value="CI"><label
							for="checkined" class="pure-u-1" title="체크인"><i
							class="fa fa-lock"></i></label>
					</div>
					<div class="pure-u-1-5">
						<input type="checkbox" id="checkouted" value="CO"><label
							for="checkouted" class="pure-u-1" title="체크아웃 "><i
							class="fa fa-unlock"></i></label>
					</div>
					<div class="pure-u-1-5">
						<input type="checkbox" id="review" value="EX"><label
							for="review" class="pure-u-1" title="검토중"><i
							class="fa fa-ellipsis-h"></i></label>
					</div>
					<div class="pure-u-1-5">
						<input type="checkbox" id="rejected" value="RE"><label
							for="rejected" class="pure-u-1" title="반송"><i
							class="fa fa-ban"></i></label>
					</div>
					<div class="pure-u-1-5">
						<input type="checkbox" id="deleting" value="DE"><label
							for="deleting" class="pure-u-1" title="삭제요청"><i
							class="fa fa-trash"></i></label>
					</div>
				</div>
			</div>
			<c:if test="${not empty docMngCtlVO.systems}">
				<div class="pure-u-1-6">
					<label for="docMngCtlVO.keyword">통합검색</label> <input type="text"
						id="docMngCtlVO.keyword" name="docMngCtlVO.keyword" class="pure-input-7-8"
						placeholder="문서명,변경근거,사람" title="문서명, 변경근거, 개정자, 검토자에서 검색어를 조회합니다.<br/>하위버전에 해당하는 결과가 있을 경우 버전전체를 보여줍니다." value="${docMngCtlVO.keyword}" />
				</div>
				<div class="pure-u-1-6">
					<label for="btn-search">&nbsp;</label> <a id="btn-search">찾기</a>
				</div>
			</c:if>
		</div>
	</form>
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	<br>
	<div style="width: 540px">
		<i class="fa fa-info-circle fa-1x"></i> <div style="float: right"><span class="qms-content-info">목록의
			버전과 상태에서는 반송상태는 무시하며, 반송만 있는 경우에만 반송으로 표시합니다.<br />문서상태 아이콘: <i
			class="fa fa-lock"></i> 체크인 <i class="fa fa-unlock"></i> 체크아웃 <i
			class="fa fa-ellipsis-h"></i> 검토중 <i class="fa fa-ban"></i> 반송 <i
			class="fa fa-trash"></i> 삭제요청
		</span></div>
	</div>
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">운영문서 관리</span>
    <span id="sidebar-menu">운영문서</span>
</div>
 -->
<br />
<form id="excelForm" name="excelForm"
	action="<s:url value="/excelDownload" />" method="post">
	<input type="hidden" name="data" id="data" /> <input type="hidden"
		name="filename" id="filename" value="운영문서.xlsx" />
	<!-- <input type="hidden" name="version" id="version" value="xlsx"/> -->
</form>