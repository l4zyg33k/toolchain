<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/dms/catmng" var="urlformselect" />
<script type="text/javascript">
	var rowdata = '${rowdata}';

	$(function() {
		$('#btn-save').button({
			icons : {
				primary : 'ui-icon-disk'
			},
			disabled : true
		}).click(function(event) {
			var data = $('#jqg-table').jqGrid('getGridParam', 'selarrrow');
			var dataArr = new Array();
			for (var i = 0; i < data.length; i++) {
				dataArr[i] = $("#jqg-table").getRowData(data[i]);
			}

			$('#rowdata').val(JSON.stringify(dataArr));

			$('#frm-documentcategory').submit();
		});

		$('#btn-cancel').button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		}).click(
            function(event) {
              location.href = parseUrl('/dms/catmng');
            });

		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/dms/catmng" />',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '#', '문서종류코드', '문서종류명' ],
			colModel : [ {
				name : 'id',
				key : true,
				align : 'center',
				hidden : true
			}, {
				name : 'code',
				width : 100
			}, {
				name : 'name'
			} ],
			pager : '#jqg-pager',
			page : '${page}',
			rowNum : '${rowNum}',
			sortname : 'code',
			sortorder : 'asc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			width : "300px",
			height : "100%",
			multiselect : true,
			caption : '문서종류 일괄 생성'
		});

		//$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {view:false, add:false, edit:false, del:false, search:false, refresh:false});

		$('#fromSystem\\.division, #toSystem\\.division').change(function() {
			systems(this.value, this.id);
		});

		$('#fromSystem').change(function() {
			$("#jqg-table").setGridParam({
				postData : {
					divisionId : $("#"+this.id+"\\.division").val(),
					systemId : $("#"+this.id).val()
				}
			}).trigger("reloadGrid");
			isCopyable();
		});
		
		$('#toSystem').change(function() {isCopyable()});

		systems($('#fromSystem\\.division').val(),"fromSystem.division");
		systems($('#toSystem\\.division').val(),"toSystem.division");
	});

	function systems(divisionId, elem) {
		$.post('<s:url value="/rest/dms/docmng/systems" />', {
			"_search" : true,
			"searchField" : "division.id",
			"searchString" : divisionId,
			"searchOper" : "eq"
		}, function(data) {
			var len = data.length;
			var systemId;
			$('#'+elem.split(".")[0]).empty();
			if (len == 0) {
				$('#'+elem.split(".")[0]).append("<option value=''> --- 없음 --- </option>");
			}
			for (var i = 0; i < len; i++) {
				if (i == 0) {
					systemId = data[i].id;
				}
				$('#'+elem.split(".")[0]).append(
						"<option value='" + data[i].id + "'>" + data[i].name
								+ "</option>");
			}

			if (elem == "fromSystem.division") {
				$("#jqg-table").setGridParam({
					postData : {
						divisionId : divisionId,
						systemId : data[0].id
					}
				}).trigger("reloadGrid");
			}
		}, 'json');
		isCopyable();
	}
	
	function isCopyable() {
		$('#btn-save').button({disabled : $('#fromSystem').val() == $('#toSystem').val()});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">문서종류 일괄생성</span>
	</div>
</div>
<div class="qms-content-main">
	<s:url value="/dms/catmng/batch" var="action" />
	<c:set value="POST" var="method" />
	<form:form id="frm-documentcategory" action="${action}"
		method="${method}" modelAttribute="catMngBatchVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<div class="pure-g">
				<div class="pure-u-1-5">
					<form:label path="fromSystem.division">원본 사업부</form:label>
					<form:select path="fromSystem.division" items="${divisions}"
						itemLabel="name" itemValue="id" cssStyle="width:95%">
					</form:select>
				</div>
				<div class="pure-u-1-5">
					<form:label path="fromSystem">원본 시스템</form:label>
					<form:select path="fromSystem" itemLabel="name" itemValue="id" cssStyle="width:95%" />
				</div>
				<div class="pure-u-1-5" align="center">=&gt;<br/>=&gt;<br/>=&gt;</div>
				<div class="pure-u-1-5">
					<form:label path="toSystem.division">대상 사업부</form:label>
					<form:select path="toSystem.division" items="${divisions}"
						itemLabel="name" itemValue="id" cssStyle="width:95%">
					</form:select>
				</div>
				<div class="pure-u-1-5">
					<form:label path="toSystem">대상 시스템</form:label>
					<form:select path="toSystem" itemLabel="name" itemValue="id" cssStyle="width:95%" />
				</div>
			</div>
			<input type="hidden" id="rowdata" name="rowdata" />
		</fieldset>
	</form:form>
	<div class="pure-g">
		<div class="pure-u-5-6" align="left">
			<label>대상시스템으로 복사하실 문서종류를 선택 후 복사 버튼을 누르십시오. 대상과 원본이 다를 경우에만 복사 가능합니다.</label>
		</div>
		<div class="pure-u-1-6">
			<div class="qms-content-buttonset">
				<button id="btn-save">복사</button>
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
</div>
<br />