<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<s:url value="/rest/dms/catmng" var="urlformselect" />
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
		
		$('#btn-batch').button({
			icons : {
				primary : 'ui-icon-plus'
			},
			disabled : eval('${!navGrid.add}')
		}).click(function() {
			var page = $('#jqg-table').jqGrid('getGridParam', 'page');
			var rowNum = $('#jqg-table').jqGrid('getGridParam', 'rowNum');
			var rowId = $('#jqg-table').jqGrid('getGridParam', 'selrow') || '';
			var url = '<s:url value="/dms/catmng/batch?page=" />' + page
					+ '&rowNum=' + rowNum + '&rowId=' + rowId;
					$(location).attr('href', url);
		});

		$('#division').change(
				function() {
					$.post('<s:url value="/rest/dms/docmng/systems" />', {
						"_search" : true,
						"searchField" : "division.id",
						"searchString" : this.value,
						"searchOper" : "eq"
					}, function(data) {
						var len = data.length;
						var systemId;
						$('#system').empty();
						$('#system').append(
								"<option value=''>--- 전체 ---</option>");
						for (var i = 0; i < len; i++) {
							if (i == 0) {
								systemId = data[i].id;
							}
							$('#system').append(
									"<option value='" + data[i].id + "'>"
											+ data[i].name + "</option>");
						}
						$("#jqg-table").setGridParam({
							postData : {
								divisionId : $("#division").val(),
								systemId : $("#system").val()
							}
						}).trigger("reloadGrid");
					}, 'json');
				});

		$('#system').change(function() {
			$("#jqg-table").setGridParam({
				postData : {
					divisionId : $("#division").val(),
					systemId : $("#system").val()
				}
			}).trigger("reloadGrid");
		});
	});

	function initButtons() {
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');

		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {

		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/dms/catmng" />',
			editurl : '<s:url value="/rest/dms/catmng/edit" />',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '#', '사업부코드', ' 사업부명', '시스템코드', '시스템명',
			/*'문서저장경로',*/'문서종류코드', '문서종류명' ],
			colModel : [ {
				name : 'id',
				key : true,
				align : 'center',
				hidden : true
			}, {
				name : 'divisionValue',
				align : 'center',
				width : 50
			}, {
				name : 'divisionName',
				align : 'left',
				width : 120
			}, {
				name : 'systemCode',
				//key: true,
				align : 'center',
				width : 50
			}, {
				name : 'systemName'
			}/*, {
												name : 'repository', //문서저장경로
												width : 100
											}*/, {
				name : 'code',
				width : 70
			}, {
				name : 'name',
				width : 100
			} ],
			pager : '#jqg-pager',
			page : '${page}',
			rowNum : '${rowNum}',
			//rowList: [20, 40, 60],
			sortname : 'id',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '문서종류 목록',
			loadComplete : function() {
				$("#btn-edit").button("option", "disabled", true);
				$("#btn-del").button("option", "disabled", true);
			},
			onSelectRow : function(id) {
				$('#btn-edit').button('option', 'disabled', false);
				//$('#btn-edit').prop('href', '<c:url value="/dms/catmng/" />' + id + '/edit');
				$('#btn-del').button('option', 'disabled', false);
			},
			ondblClickRow : function(id) {
				$('#edit_jqg-table').click();
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			view : '${navGrid.view}'
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {});
	}

	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		if (eval('${navGrid.edit}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var url = '<s:url value="/dms/catmng/" />' + rowId + '/form?page='
					+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('수정 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataAdd(formId, scope) {
		debugger;
		if (eval('${navGrid.add}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
			var url = '<s:url value="/dms/catmng/form?page=" />' + page
					+ '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('등록 할 수 없습니다.');
			return false;
		}
	}

	function beforeInitDataDel(formId, scope) {
		if (eval('${navGrid.del}')) {
			return true;
		} else {
			infomation('삭제 할 수 없습니다.');
			return false;
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">문서종류 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form id="query-form" class="pure-form">
				<label for="division">사업부</label> <select id="division">
					<option value="">--- 선택 ---</option>
					<c:forEach var="item" items="${divisions}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>&nbsp;&nbsp; <label for="system">시스템</label> <select id="system">
					<option value="">--- 전체 ---</option>
					<c:forEach var="item" items="${systems}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select>
			</form>
		</div>
		<div class="pure-u-1-3">
			<div class="qms-content-buttonset">
				<button id="btn-batch">일괄등록</button>
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />
