<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<s:url value="/rest/dms/catmng" var="urlformselect" />
<script type="text/javascript">
	$(function() {
		$('#btn-save').button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function(event) {
			$('#frm-documentcategory').submit();
		});
		$('#btn-cancel').button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		});

		$('#system\\.division').change(
				function() {systems(this.value)});

		systems($('#system\\.division').val());
	});
	
	function systems(divisionId) {
		$.post('<s:url value="/rest/dms/docmng/systems" />', {
			"_search" : true,
			"searchField" : "division.id",
			"searchString" : divisionId,
			"searchOper" : "eq"
		}, function(data) {
			var len = data.length;
			var systemId;
			$('#system').empty();
			if (len == 0) {
				$('#system').append(
						"<option value=''> --- 없음 --- </option>");
			}
			for (var i = 0; i < len; i++) {
				if (i == 0) {
					systemId = data[i].id;
				}
				$('#system').append(
						"<option value='" + data[i].id + "'>" + data[i].name
								+ "</option>");
			}
		}, 'json');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">문서종류 등록(수정)</span>
	</div>
</div>
<div class="qms-content-main">
	<c:choose>
		<c:when test="${not empty catMngFrmVO.id}">
			<s:url value="/dms/catmng/${catMngFrmVO.id}" var="action" />
			<c:set value="PUT" var="method" />
		</c:when>
		<c:otherwise>
			<s:url value="/dms/catmng" var="action" />
			<c:set value="POST" var="method" />
		</c:otherwise>
	</c:choose>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-save">저장</a> <a id="btn-cancel"
					href="<c:url value='${catMngFrmVO.getPrevUrl()}' />">취소</a>
			</div>
		</div>
	</div>
	<form:form id="frm-documentcategory" action="${action}"
		method="${method}" modelAttribute="catMngFrmVO"
		cssClass="pure-form pure-form-aligned">
		<fieldset>
			<div class="pure-control-group">
				<form:label path="system.division">사업부</form:label>
				<c:choose>
					<c:when test="${not empty catMngFrmVO.id}">
						<form:input path="system.division.name" readonly="true" />
						<form:hidden path="system.division.id" />
					</c:when>
					<c:otherwise>
						<form:select path="system.division.id" items="${divisions}"
							itemLabel="name" itemValue="id">
						</form:select>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="pure-control-group">
				<form:label path="system">시스템</form:label>
				<c:choose>
					<c:when test="${not empty catMngFrmVO.id}">
						<form:input path="system.name" readonly="true" />
						<form:hidden path="system.id" />
					</c:when>
					<c:otherwise>
						<form:select path="system" itemLabel="name" itemValue="id" />
					</c:otherwise>
				</c:choose>
			</div>
			<div class="pure-control-group">
				<form:label path="code">문서종류코드</form:label>
				<form:input path="code" maxlength="2" />
				<form:errors path="code" cssClass="ui-state-error-text" />
			</div>
			<div class="pure-control-group">
				<form:label path="name">문서종류명</form:label>
				<form:input path="name" />
				<form:errors path="name" cssClass="ui-state-error-text" />
			</div>
			<form:hidden path="id" />
			<form:hidden path="repository" />
			<form:hidden path="page" />
			<form:hidden path="rowNum" />
			<form:hidden path="rowId" />
			<div class="pure-controls">
				<form:errors path="*" cssClass="ui-state-error-text" />
			</div>
		</fieldset>
	</form:form>
</div>
<br />