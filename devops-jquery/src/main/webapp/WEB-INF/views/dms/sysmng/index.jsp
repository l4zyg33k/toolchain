<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
$(function() {
	initButtons();
	initJqGrid();
	initNavGrid();
	
	$("#division").change(function() {
		$("#jqg-table").setGridParam({
			postData : {
				query : $("#division").val()
			}
		}).trigger("reloadGrid");
	});
});

function initButtons() {
	setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
	setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
	setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');

	$('.qms-content-buttonset').css('display', 'block');
}

function initJqGrid() {
		
        $("#jqg-table").jqGrid({
            url: '<s:url value="/rest/dms/sysmng" />',
			editurl : '<s:url value="/rest/dms/sysmng/edit" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['#','사업부코드', ' 사업부명', '시스템코드', '시스템명', '사용여부', '최종수정자', '최종수정일시'],
            colModel: [{
                    name: 'id',
                    key: true,
                    align: 'center',
                    hidden: true
            	}, {
	                name: 'divisionValue',
	                align: 'center',
                    width: 60	                
                }, {
                    name: 'divisionName',
                    align: 'left',
                    width: 100
                }, {
                    name: 'code',
                    //key: true,
                    align: 'center',
                    width: 60
                }, {
                    name: 'name',
                    width: 150
                }, {
                    name: 'enabled',
                    width: 50,
                    align: 'center',
                    formatter: function(cellValue, options,
                            rowObject) {
                      if (cellValue != null) {
                        return Boolean(cellValue) ? 'Y' : 'N';
                      } else {
                        return 'N';
                      };
                    }
                }, {
                    name: 'lastModifiedBy.name',
                    align: 'center',
                    width: 70
                }, {
                    name: 'lastModifiedDate',
                    align: 'center',
                    width: 70                   
                }],
            pager: '#jqg-pager',
			page : '${page}',
			rowNum : '${rowNum}',
            //rowList: [20, 40, 60],
            sortname: 'id',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '시스템 목록',
            loadComplete: function() {
                $("#btn-edit").button("option", "disabled", true);
                $("#btn-del").button("option", "disabled", true);
            },
            onSelectRow: function(id) {
                $('#btn-edit').button('option', 'disabled', false);
                //$('#btn-edit').prop('href', '<c:url value="/dms/sysmng/" />' + id + '/edit');
                $('#btn-del').button('option', 'disabled', false);
            },
            ondblClickRow: function(id) {
            	$('#edit_jqg-table').click();
            }
        });
    }

function initNavGrid() {
	$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
		view : '${navGrid.view}'
	}, {
		beforeInitData : function(formId) {
			return beforeInitDataEdit(formId, this);
		}
	}, {
		beforeInitData : function(formId) {
			return beforeInitDataAdd(formId, this);
		}
	}, {
		beforeInitData : function(formId) {
			return beforeInitDataDel(formId, this);
		}
	}, {}, {});
}

function beforeInitDataEdit(formId, scope) {
	var rowId = $(scope).jqGrid('getGridParam', 'selrow');
	if (eval('${navGrid.edit}')) {
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		var url = '<s:url value="/dms/sysmng/" />' + rowId + '/form?page='
				+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
		$(location).attr('href', url);
		return false;
	} else {
		infomation('수정 할 수 없습니다.');
		return false;
	} 
}

function beforeInitDataAdd(formId, scope) {
	if (eval('${navGrid.add}')) {
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
		var url = '<s:url value="/dms/sysmng/form?page=" />' + page
				+ '&rowNum=' + rowNum + '&rowId=' + rowId;
		$(location).attr('href', url);
		return false;
	} else {
		infomation('등록 할 수 없습니다.');
		return false;
	}
}

function beforeInitDataDel(formId, scope) {
	if (eval('${navGrid.del}')) {
		return true;
	} else {
		infomation('삭제 할 수 없습니다.');
		return false;
	}
}

</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">시스템 관리</span>    
    </div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
				<select id="division">
					<option value="">--- 전체 ---</option>
					<c:forEach var="item" items="${divisions}">
						<option value="<c:out value="${item.id}"/>"><c:out value="${item.name}"/></option>
					</c:forEach>
				</select>
            </form>
        </div>	
        <div class="pure-u-1-2">
			<div class="qms-content-buttonset">
			    <a id="btn-add">등록</a>
			    <a id="btn-edit">수정</a>
			    <a id="btn-del">삭제</a>
			</div>
		</div>		        
	</div>
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>	
</div>
<br />
