<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript"
	src="<c:url value="/js/tag-it.js" />" charset="utf-8"></script>
<link href="<c:url value="/css/jquery.tagit.css" />"
	rel="stylesheet" type="text/css">
<script type="text/javascript">

    $(function() {
    	
        $('#btn-save').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        }).click(function(event) {
        	$('#sysops').val(function(){var a=$('#sysopsNames').val().match(/\([^\)]+\)/g); return a!=null ? a.join(", ").replace(/[\(\)]/g,""): "";});
        	$('#approbators').val(function(){var a=$('#approbatorsNames').val().match(/\([^\)]+\)/g); return a!=null ? a.join(", ").replace(/[\(\)]/g,""): "";});
        	$('#auditors').val(function(){var a=$('#auditorsNames').val().match(/\([^\)]+\)/g); return a!=null ? a.join(", ").replace(/[\(\)]/g,""): "";});
        	
            $('#frm-system').submit();
        });
        $('#btn-cancel').button({
            icons: {
                primary: 'ui-icon-cancel'
            }
        });

		$.dataSource = new Array();
		$.isLoaded = false;
		applyAccountTagit("#sysopsNames");
		applyAccountTagit("#approbatorsNames");
		applyAccountTagit("#auditorsNames");
		$.isLoaded = true;
		$("ul.tagit").css("width","440px").css("font-size","0.7em").css("font-weight","none");
		
    });

</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">시스템 등록(수정)</span>
	</div>
</div>
<div class="qms-content-main">
	<c:choose>
		<c:when test="${!sysMngFrmVO.isNew()}">
			<s:url value="/dms/sysmng/${sysMngFrmVO.id}" var="action" />
			<c:set value="PUT" var="method" />
		</c:when>
		<c:otherwise>
			<s:url value="/dms/sysmng" var="action" />
			<c:set value="POST" var="method" />
		</c:otherwise>
	</c:choose>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-save">저장</a> <a id="btn-cancel"
					href="<c:url value='${sysMngFrmVO.getPrevUrl()}' />">취소</a>
			</div>
		</div>
	</div>
	<form:form id="frm-system" action="${action}" method="${method}"
		modelAttribute="sysMngFrmVO" cssClass="pure-form pure-form-aligned">
		<fieldset>
			<div class="pure-control-group">
				<form:label path="division">사업부</form:label>
					<c:choose>
						<c:when test="${!sysMngFrmVO.isNew()}">
							<form:input path="division.name" readonly="true" />
							<form:hidden path="division" />
						</c:when>
						<c:otherwise>
							<form:select path="division" items="${divisions}" itemLabel="name"
								itemValue="id" />
						</c:otherwise>
					</c:choose>
			</div>
			<div class="pure-control-group">
				<form:label path="code">시스템코드</form:label>
				<form:input path="code" maxlength="2"
					readonly="${!sysMngFrmVO.isNew()}" />
				<form:errors path="code" cssClass="ui-state-error-text" />
			</div>
			<div class="pure-control-group">
				<form:label path="name">시스템명</form:label>
				<form:input path="name" />
				<form:errors path="name" cssClass="ui-state-error-text" />
			</div>
			<div class="pure-control-group">
				<form:label path="enabled">사용여부</form:label>
				<form:checkbox path="enabled" />
			</div>
			<div class="pure-control-group">
				<label>운영자</label>
				<div style="position: relative; left: 144px; top: -30px">
					<c:forEach items="${sysMngFrmVO.sysops}" var="sysop" varStatus="stat"><c:set var="sysopsNames"
							value="${stat.first ? '' : sysopsNames.concat(',')}${sysop.name}(${sysop.username})/${sysop.team.name}" /></c:forEach>
					<input type="hidden" id="sysops" name="sysops" />
					<input type="hidden" id="sysopsNames" name="sysopsNames" value="${sysopsNames}" />
				</div>
			</div>
			<div class="pure-control-group">
				<label>승인자</label>
				<div style="position: relative; left: 144px; top: -36px">
					<c:forEach items="${sysMngFrmVO.approbators}" var="approbator" varStatus="stat"><c:set var="approbatorsNames"
							value="${stat.first ? '' : approbatorsNames.concat(',')}${approbator.name}(${approbator.username})/${approbator.team.name}" /></c:forEach>
					<input type="hidden" id="approbators" name="approbators" />
					<input type="hidden" id="approbatorsNames" name="approbatorsNames" value="${approbatorsNames}" />
				</div>
			</div>
			<div class="pure-control-group">
				<label>감사자</label>
				<div style="position: relative; left: 144px; top: -36px">
					<c:forEach items="${sysMngFrmVO.auditors}" var="auditor" varStatus="stat"><c:set var="auditorsNames"
							value="${stat.first ? '' : auditorsNames.concat(',')}${auditor.name}(${auditor.username})/${auditor.team.name}" /></c:forEach>
					<input type="hidden" id="auditors" name="auditors" />
					<input type="hidden" id="auditorsNames" name="auditorsNames" value="${auditorsNames}" />
				</div>
			</div>
			<form:hidden path="id" />
			<form:hidden path="page" />
			<form:hidden path="rowNum" />
			<form:hidden path="rowId" />
			<div class="pure-controls">
				<form:errors path="*" cssClass="ui-state-error-text" />
			</div>
		</fieldset>
	</form:form>
</div>
<br />