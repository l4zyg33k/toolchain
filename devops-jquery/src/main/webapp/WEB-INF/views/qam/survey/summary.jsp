<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
	$('#btn-cancel').button({
           icons: {
               primary: 'ui-icon-cancel'
           }
       }).click(function(event) {
           get('<s:url value="/qam/project/quality" />');
       });
		
	$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					
					jQuery("#jqg-table").editGridRow(
							jQuery('#jqg-table').jqGrid('getGridParam'
									,'selrow'), {
								recreateForm : true,
								closeAfterEdit : true,
								closeAfterAdd : true,
								//폼로드시 히든처리
								beforeShowForm: function(form){
									$('#tr_satisfactionSurveySummary\\.id', form).hide();
								}
							});
				});


		$("#btn-get").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(
			function(event) {
				var url = "<c:url value='/qam/survey/summary/${projectVO.code}/get'/>";
				get(url);
		});
	
		$("#btn-edit2").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					jQuery("#jqg-table2").editGridRow(
							jQuery('#jqg-table2').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterEdit : true,
								closeAfterAdd : true,
								width:700,
								height:250
							});
				});
		
		
		
		
		//기본버튼 disable처리		
		$("#btn-edit").button("option", "disabled", true);
		$("#btn-edit2").button("option", "disabled", true);

		
		$("#jqg-table")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/summary/${projectVO.code}/list" />',
							editurl : '<s:url value="/rest/qam/survey/summary/${projectVO.code}/edit" />',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'I1D', '조사 대상', '조사 예정일', '실제 조사일',
									'대상자', '응답수', '만족도'],
							colModel : [ {
								name : 'satisfactionSurveySummary.id',
								align : 'center',
								search : false,
								editable : true,
								edittype : 'text',
								hidden : true
								
							}, {
								name : 'userType.name',
								align : 'center',
								search : false,
								editable : true,
								edittype : 'select',
								width : '140',
								editrules : {
									required : true
								},
								editoptions : {
									disabled : true,
									value : '${userTypeCode}'
								}
							}, {
								name : 'satisfactionSurveySummary.surveyStartDate',
								align : 'center',
								search : false,
								editable : true,
								edittype : 'text',
								editoptions : {
									dataInit : function(e) {
										$(e).datepicker();
										$(e).mask('9999-99-99');
									}
								}
							}, {
								name : 'satisfactionSurveySummary.surveyRealDate',
								align : 'center',
								search : false,
								editable : true,
								edittype : 'text',
								editoptions : {
									dataInit : function(e) {
										$(e).datepicker();
										$(e).mask('9999-99-99');
									}
								}
							}, {
								name : 'satisfactionSurveySummary.targetCount',
								align : 'right',
								search : false,
								editable : true,
								edittype : 'text',
								editoptions : {
									dataInit : function(e) {
										$(e).mask("9?99");
									}
								}

							}, {
								name : 'satisfactionSurveySummary.answerCount',
								align : 'right',
								search : false,
								editable : true,
								edittype : 'text',
								width : '80',
								editoptions : {
									dataInit : function(e) {
										$(e).mask("9?99");
									}
								}
							}, {
								name : 'satisfactionSurveySummary.satisfaction',
								align : 'right',
								search : false,
								editable : true,
								edittype : 'text',
								editoptions : {
									dataInit : function(e) {
										$(e).mask("9?.99");
									}
								}
							} ],
							//pager : '#jqg-pager',
							rowNum : 100,
							rowList : [ 100, 200, 300 ],
							sortname : 'userType.code',
							sortorder : 'asc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '만족도 점수',
							loadComplete : function() {
								$("#btn-edit").button("option", "disabled",true);
							},
							onSelectRow : function(id) {
								$("#btn-edit").button("option", "disabled",false);

							},
							gridComplete : function() {
								//종합만족도 계산
								var cnt = 0;
								var sum = 0;
								var rowIds = $('#jqg-table').jqGrid(
										'getDataIDs');
								for (var i = 0, len = rowIds.length; i < len; i++) {

									var tCnt = Number($('#jqg-table').getCell(rowIds[i], "satisfactionSurveySummary.answerCount"));
									var point = Number($('#jqg-table').getCell(rowIds[i], "satisfactionSurveySummary.satisfaction"));
									cnt += tCnt;
									sum += tCnt * point;
								}
								if (cnt == 0) {
									$('#summaryPoint').val(0);
								} else {
									$('#summaryPoint').val(
											Math.round(sum / cnt * 100) / 100);
								}
							}

						});

		$("#jqg-table2")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/summary/${projectVO.code}/list" />',
							editurl : '<s:url value="/rest/qam/survey/summary/${projectVO.code}/edit2" />',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'ID', '조사 대상', '의견' ],
							colModel : [
									{
										name : 'satisfactionSurveySummary.id',
										search : false,
										editable : true,
										edittype : 'text',
										hidden : true
									},
									{
										name : 'userType.name',
										align : 'center',
										search : false,
										editable : true,
										edittype : 'select',
										editrules : {
											required : true
										},
										width : '30',
										editoptions : {
											disabled : true,
											value : '${userTypeCode}'
										}
									},
									{
										name : 'satisfactionSurveySummary.comment',
										align : 'left',
										search : false,
										editable : true,
										edittype : 'textarea',
										editoptions : {
											rows : 10,
											cols : 100,
											dataInit : function(e) {
												$(e).val(
														$(e).val().replace(
																/\<br\>/gi,
																'\r\n'));
											}

										},
										formatter : function(e) {

											if (e != null) {
												return e.replace(/\r?\n/g,
														'<br>')
											} else
												return "";

										}

									} ],
							//pager : '#jqg-pager',
							rowNum : 10,
							rowList : [ 20, 40, 60 ],
							sortname : 'userType.code',
							sortorder : 'asc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '만족도 점수',
							loadComplete : function() {
								$("#btn-edit2").button("option", "disabled",
										true);
							},
							onSelectRow : function(id) {
								$("#btn-edit2").button("option", "disabled",
										false);
							}
						});

	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">만족도 평가등록</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
        </div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
                <a id="btn-cancel">취소</a>
            </div>
        </div>
    </div>
	<form:form id="frm-project" modelAttribute="projectVO"
		cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-control-group">
				<div class="pure-u-3-8">
					<form:label path="code">프로젝트 코드</form:label>
					<form:input path="code" class="pure-u-1-3" readOnly="readOnly" />
				</div>
				<div class="pure-u-3-5">
					<form:label path="name">프로젝트 명</form:label>
					<form:input path="name" class="pure-u-7-12" readOnly="readOnly" />
				</div>
			</div>			
		</fieldset>
		<fieldset>
			<legend>만족도 평가</legend>
			<div class="pure-u-1">
				<form id="query-form" class="pure-form">
					<div class="qms-grid-top-notify">
						<label>종합만족도</label> <input id="summaryPoint" type="text"
							class="pure-input-rounded" readOnly>
					</div>
				</form>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-edit">수정</a>
						<a id="btn-get">설문결과 가져오기</a>
					</div>
				</div>
			</div>
		</fieldset>
		<table id="jqg-table"></table>
		<br/>
		<div class="pure-g">
			<div class="pure-u-1-2">
				<form class="pure-form"></form>
			</div>
			<div class="pure-u-1-2">
				<div class="qms-content-buttonset">
					<a id="btn-edit2">수정</a>
				</div>
			</div>
		</div>		
		<br/>
		<table id="jqg-table2"></table>				
	</form:form>

</div>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">품질현황 관리</span>
	<span id="sidebar-menu">프로젝트 품질관리</span>
</div>
 -->
<br />