<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		$("#btn-add").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function(event) {
			jQuery("#jqg-table").editGridRow("new", {
				recreateForm : true,
				closeAfterAdd : true
			});

		});
		$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					jQuery("#jqg-table").editGridRow(
							jQuery('#jqg-table').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterAdd : true,
								beforeShowForm: function(form){
									$('#userType\\.name', form).attr("disabled:true");
								}
							});
					//reloadGrid();

				});

		;

		$("#btn-del")
				.button({
					icons : {
						primary : 'ui-icon-trash'
					}
				})
				.click(
						function(event) {
							dialog(
									'삭제 할까요?',
									function() {
										var rowId = $('#jqg-table').jqGrid(
												'getGridParam', 'selrow');
										var url = '<s:url value="/rest/qam/survey/member/${projectVO.code}/edit" />';
										var data = {
											oper : 'del',
											id : rowId
										};
										$.post(url, data, function(data,textStatus, XMLHttpRequest) {
											reloadGrid();
											
										});
									});
						});
		
		//메일발송
		$("#btn-mail")
		.button({
			icons : {
				primary : 'ui-icon-mail-closed'
			}
		})
		.click(
				function(event) {
					dialog(
							'메일발송을 하시겠습니까?',
							function() {
								//var data = $('#jqg-table').jqGrid('getGridParam','userData');
								var data = $('#jqg-table').getRowData();
								//var sendMail = new Object();
								var sendMail = new Array();
								$.each(data,function(i){
									if ( data[i].gridCheckBox == 'Yes' ){
										sendMail.push(data[i].id);
									}
								});
								var postData = JSON.stringify(sendMail);
								var url = '<s:url value="/qam/survey/member/send/${projectVO.code}"/>';
								var data = {
									mailList : postData
								};
								
								$.post(url, data, function(data,textStatus, XMLHttpRequest) {
									reloadGrid();
								});
								
								
							});
				});

		//기본버튼 disable처리
		//$("#btn-add").button("option", "disabled", true);
		$("#btn-edit").button("option", "disabled", true);
		$("#btn-del").button("option", "disabled", true);

		$("#jqg-table")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/member/${projectVO.code}" />',
							editurl : '<s:url value="/rest/qam/survey/member/${projectVO.code}/edit" />',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'ID','메일전송' ,'사용자유형','이름', 'ID','이메일','부서', '직급','메일발송일' ],
							colModel : [ {
								name : 'id',
								align : 'center',
								search : false,
								hidden : true,
								key : true
							}, {
								name : 'gridCheckBox',
								align : 'center',
								width : 60,
								formatter: "checkbox",
								formatoptions: { disabled: false}
							}, {
								name : 'userType.name',
								align : 'center',
								width : 100,
								editable : true,
								edittype : 'select',
								editrules : {
									required : true
								},
								editoptions : {
									value : '${userTypeCode}'
								}
							}, {
								name : 'member.name',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editoptions : {
									dataInit : function(e) {
										$(e).autocomplete({
											source : function(request,response) {
												request['search.search'] = true;
												request['search.field'] = 'name';
												request['search.value'] = request['term'];
												request['search.oper'] = 'cn';

												$.post('<s:url value="/rest/adm/accounts" />',request,response);
											},
											minLength : 1,											
											open : function() {
												$(e).autocomplete("widget").css('z-index',1000);
												return false;
											},
											select : function(event,ui) {
												$("#member\\.name").val(ui.item.name);
												$("#member\\.username").val(ui.item.username);
												$("#member\\.email").val(ui.item.email);
												$("#member\\.team\\.name").val(ui.item.team.name);
												$("#member\\.position\\.name").val(ui.item.position.name);
												return false;
											},
								            change: function(event, ui) {
								            	if (ui.item == null) {
								            		$("#member\\.name").val('');
													$("#member\\.username").val('');
													$("#member\\.email").val('');
													$("#member\\.team\\.name").val('');
													$("#member\\.position\\.name").val('');
								               	}    
								            }
										}).data('ui-autocomplete')._renderItem = function(ul, item) {
											return $('<li>').append('<a>'+ item.name+ ', '+ item.team.name+ '</a>').appendTo(ul);
										};
									}
								}
							}, {
								name : 'member.username',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editoptions : {
									disabled : true
								}
							}, {
								name : 'member.email',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editoptions : {
									disabled : true
								}

							}, {								
								name : 'member.team.name',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editoptions : {
									disabled : true
								}
							}, {								
								name : 'member.position.name',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editoptions : {
									disabled : true
								}
							}, {								
								name : 'sendDate',
								align : 'left',
								width : 100,
								edittype : 'text'
							} ],
							pager : '#jqg-pager',
							rowNum : 10,
							rowList : [ 20, 40, 60 ],
							sortname : 'id',
							sortorder : 'desc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '프로젝트 목록',
							//multiselect: true,
							loadComplete : function() {
								$("#btn-edit").button("option", "disabled",true);
								$("#btn-del").button("option", "disabled", true);
								
								//계획대비 진척률 계산
								getProgressPerPlan();
							},
							onSelectRow : function(id) {
								$("#btn-edit").button("option", "disabled",false);
								$("#btn-del").button("option", "disabled",false);
							}
						});

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {},
				{}, {}, search_options);
		
		//계획대비 진척률 계산		
		function getProgressPerPlan(){
			$.ajax({
                type: 'GET',
                url: '<s:url value="/rest/qam/progress/evaluation/plan/${projectVO.code}"/>',
                success : function(data) {
                	//결과값 매핑
                	$("#progressPerPlan").val(data+"%");
                }
            });	
		}
		
		function reloadGrid(){
			$('#jqg-table').trigger("reloadGrid");
		}
	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 품질관리</span>
	</div>
</div>

<div class="qms-content-main">
	<form:form id="frm-project" modelAttribute="projectVO"
		cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>고객만족도 대상자 관리</legend>
			<div class="pure-control-group">
				<div class="pure-u-3-8">
					<form:label path="code">프로젝트 코드</form:label>
					<form:input path="code" class="pure-u-1-3" readOnly="readOnly" />
				</div>
				<div class="pure-u-3-5">
					<form:label path="name">프로젝트 명</form:label>
					<form:input path="name" class="pure-u-7-12" readOnly="readOnly" />
				</div>
			</div>
			<div class="pure-control-group">
				<div class="pure-u-3-8">
					<form:label path="qa.name">품질 관리자</form:label>
					<form:input path="qa.name" class="pure-u-1-3" readOnly="readOnly" />
				</div>
				<div class="pure-u-3-5">
					<form:label path="qa.name">프로젝트 관계자</form:label>
					<form:input path="qa.name" class="pure-u-7-12" readOnly="readOnly" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-add">등록</a>
						<a id="btn-edit">수정</a>
						<a id="btn-del">삭제</a>
						<a id="btn-mail">메일발송</a>
					</div>
				</div>
			</div>
		</fieldset>		
		<table id="jqg-table"></table>
		<div id="jqg-pager"></div>
	</form:form>

</div>
<br />