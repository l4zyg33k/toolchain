<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		//검색버튼 파일 돋보기 모양 아이콘 주기
		$("#btn-search").button({
			icons : {
				primary : 'ui-icon-search'
			}
		}).click(function(event) {
			//그리드 재조회
			reloadGrid();
		});
		
		$("#btn-excel")
		.button({
			icons : {
				primary : 'ui-icon-disk'
			}/**/
		}).click(function() {
			var jsonData = '[{"name":"사업부(객관식)","data":"'+JSON.stringify($("#jqg-table").getRowData()).replace(/\"/g,"\\\"")+'"},{"name":"사업부(주관식)","data":"'+JSON.stringify($("#jqg-table2").getRowData()).replace(/\"/g,"\\\"")+'"},{"name":"현업(객관식)","data":"'+JSON.stringify($("#jqg-table3").getRowData()).replace(/\"/g,"\\\"")+'"},{"name":"현업(주관식)","data":"'+JSON.stringify($("#jqg-table4").getRowData()).replace(/\"/g,"\\\"")+'"},{"name":"시스템(객관식)","data":"'+JSON.stringify($("#jqg-table5").getRowData()).replace(/\"/g,"\\\"")+'"},{"name":"시스템(주관식)","data":"'+JSON.stringify($("#jqg-table6").getRowData()).replace(/\"/g,"\\\"")+'"}]';$('#excelForm #data').val(jsonData);$('#excelForm').submit();
		});
		
        $("#btn-search").button({
            icons: {
                primary: 'ui-icon-search'
            }
        });
		
		$("#btn-expired")
		.button({
			icons : {
				primary : 'ui-icon-trash'
			}
		})
		.click(
				function(event) {
					var url = '<s:url value="/pms/satisfactionExpired/'+ $('#projectCode').val() +'"/>';
					alert(url);
					$.ajax({
		                type: 'GET',
		                url: url,
		                success : function(data) {
		                	alert("설문이 종료 되었습니다.");
		                },
		                error:function(e){  
		                    alert(e.responseText);  
		                }
		            });
				});
		

		$('#projectCode').autocomplete(
				{
					autoFocus : true,
					source : function(request, response) {
						request['search.search'] = true;
						request['search.field'] = 'code';
						request['search.value'] = request['term'];
						request['search.oper'] = 'cn';

						$.post('<s:url value="/rest/pms/projects" />', request,
								response);
					},
					select : function(event, ui) {
						$('#projectCode').val(ui.item.code);
						$('#projectName').val(ui.item.name);
						//그리드 재조회
						reloadGrid();
						//버튼 활성화
						//$("#btn-excel").button("option", "disabled", false);
						$("#btn-expired").button("option", "disabled", false);
						return false;
					}
				}).data('ui-autocomplete')._renderItem = function(ul, item) {
			$('#projectName').val("");
			return $('<li>').append(
					'<a>' + item.code + ', ' + item.name + '</a>').appendTo(ul);
		};

		// 프로젝트 자동완성
		$('#projectName').autocomplete(
				{
					autoFocus : true,
					source : function(request, response) {
						request['search.search'] = true;
						request['search.field'] = 'name';
						request['search.value'] = request['term'];
						request['search.oper'] = 'cn';

						$.post('<s:url value="/rest/pms/projects" />', request,
								response);
					},
					select : function(event, ui) {
						$('#projectCode').val(ui.item.code);
						$('#projectName').val(ui.item.name);
						//그리드 재조회
						reloadGrid();
						//버튼 활성화
						//$("#btn-excel").button("option", "disabled", false);
						$("#btn-expired").button("option", "disabled", false);
						return false;
					}
				}).data('ui-autocomplete')._renderItem = function(ul, item) {
			$('#projectCode').val("");
			return $('<li>').append(
					'<a>' + item.code + ', ' + item.name + '</a>').appendTo(ul);
		};

		//버튼 초기설정(프로젝트 선택시 활성화)
		//$("#btn-excel").button("option", "disabled", true);
		$("#btn-expired").button("option", "disabled", true);
		
		/* 사업부 담장자 탭 시작*/
		//객관식 설문
		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/multipleChoice/T3 "/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','응답속도','요구사항','출력양식','자료정확','보안체계','일정준수','개발시스템.<br/>종합',
			             '교육자료','메뉴얼','UI','호환성','시스템활용지원.<br>종합'],
			colModel : [{
						name : 'project.name',
						width: '500'
					}, {
						name : 'username'
					}, {
						name : 't301',
						align : 'right'
					}, {
						name : 't302',
						align : 'right'
					}, {
						name : 't303',
						align : 'right'
					}, {
						name : 't304',
						align : 'right'
					}, {
						name : 't305',
						align : 'right'
					}, {
						name : 't306',
						align : 'right'
					}, {
						name : 't307',
						align : 'right'
					}, {
						name : 't308',
						align : 'right'
					}, {
						name : 't309',
						align : 'right'
					}, {
						name : 't310',
						align : 'right'
					}, {
						name : 't311',
						align : 'right'
					}, {
						name : 't312',
						align : 'right'
					} ],
			pager : '#jqg-pager',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록',
			loadComplete : function() {
				return false;
			},
			onSelectRow : function(id) {
				return false;
			}
		});

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {},{}, {}, search_options);
		
		
		//주관식 설문
		$("#jqg-table2").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/shortAnswer/T3"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','답변내용' ],
			colModel : [{
						name : 'project.name',
						width: '70'
					}, {
						name : 'username',
						width: '20'
					}, {
						name : 'contentsResult',
						align : 'left'
					} ],
			pager : '#jqg-pager2',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록'
		});

		$("#jqg-table2").jqGrid('navGrid', '#jqg-pager2', navigator_options, {},{}, {}, search_options);
		/* 사업부 담당자 탭 종료 */
		
		
		/* 현업 담당자 탭 시작*/
		//객관식 설문
		$("#jqg-table3").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/multipleChoice/T4 "/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','응답속도','요구사항','자료정확','Sys.안정','사용편의','Sys.개선','개발시스템.종합' ],
			colModel : [{
						name : 'project.name',
						width: '500'
					}, {
						name : 'username'
					}, {
						name : 't401',
						align : 'right'
					}, {
						name : 't402',
						align : 'right'
					}, {
						name : 't403',
						align : 'right'
					}, {
						name : 't404',
						align : 'right'
					}, {
						name : 't405',
						align : 'right'
					}, {
						name : 't406',
						align : 'right'
					}, {
						name : 't407',
						align : 'right'
					} ],
			pager : '#jqg-pager3',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록'
		});

		$("#jqg-table3").jqGrid('navGrid', '#jqg-pager3', navigator_options, {},{}, {}, search_options);
		
		
		//주관식 설문
		$("#jqg-table4").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/shortAnswer/T4"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','답변내용' ],
			colModel : [{
						name : 'project.name',
						width: '70'
					}, {
						name : 'username',
						width: '20'
					}, {
						name : 'contentsResult',
						align : 'left'
					} ],
			pager : '#jqg-pager4',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록'
		});

		$("#jqg-table4").jqGrid('navGrid', '#jqg-pager4', navigator_options, {},{}, {}, search_options);
		/* 현업 담당자 탭 종료 */
		
		
		/* 시스템 운영자 탭 시작*/
		//객관식 설문
		$("#jqg-table5").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/multipleChoice/T5 "/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','문서<br>완성','문서<br>이해','문서<br>일치','문서<br>실질','개발문서.<br/>종합',
			             'Sys.<br>구성','Sys.<br/>확장','보안<br>체계','Sys.<br>성능','오류<br>빈도','시스템구조및성능<br/>종합',
			             '운영<br>편의','수정<br>용이','표준<br>준수','Sys.<br>연동','User<br>관리','배치<br>편리','시스템운영편리성.<br/>종합'],
			colModel : [{
						name : 'project.name',
						width: '500'
					}, {
						name : 'username'
					}, {
						name : 't501',
						align : 'right'
					}, {
						name : 't502',
						align : 'right'
					}, {
						name : 't503',
						align : 'right'
					}, {
						name : 't504',
						align : 'right'
					}, {
						name : 't505',
						align : 'right'
					}, {
						name : 't506',
						align : 'right'
					}, {
						name : 't507',
						align : 'right'
					}, {
						name : 't508',
						align : 'right'
					}, {
						name : 't509',
						align : 'right'
					}, {
						name : 't510',
						align : 'right'
					}, {
						name : 't511',
						align : 'right'
					}, {
						name : 't512',
						align : 'right'
					}, {
						name : 't513',
						align : 'right'
					}, {
						name : 't514',
						align : 'right'
					}, {
						name : 't515',
						align : 'right'
					}, {
						name : 't516',
						align : 'right'
					}, {
						name : 't517',
						align : 'right'
					}, {
						name : 't518',
						align : 'right'
					} ],
			pager : '#jqg-pager5',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록'
		});

		$("#jqg-table5").jqGrid('navGrid', '#jqg-pager5', navigator_options, {},{}, {}, search_options);
		
		
		//주관식 설문
		$("#jqg-table6").jqGrid({
			url : '<s:url value="/rest/mail/satisfaction/survey/shortAnswer/T5"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명','응답자','답변내용' ],
			colModel : [{
						name : 'project.name',
						width: '70'
					}, {
						name : 'username',
						width: '20'
					}, {
						name : 'contentsResult',
						align : 'left'
					} ],
			pager : '#jqg-pager6',
			rowNum : 20,
			rowList : [ 20, 40, 60 ],
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록'
		});

		$("#jqg-table6").jqGrid('navGrid', '#jqg-pager6', navigator_options, {},{}, {}, search_options);
		/* 현업 담당자 탭 종료 */

		function reloadGrid() {
			//사업부 담당자
			$("#jqg-table").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
			
			$("#jqg-table2").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
			
			//현업 사용자
			$("#jqg-table3").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
			
			$("#jqg-table4").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
			
			//시스템 운영자
			$("#jqg-table5").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
			
			$("#jqg-table6").setGridParam({
				postData : {
					projectCode : $("#projectCode").val()
				}
			}).trigger("reloadGrid");
		}
		
		$( "#tabs" ).tabs();
		
		$("#tabs").tabs({
			 	
				beforeActivate: function (event, ui) {
                	reloadGrid();
            	}
			});
		
	});
</script>
<style>
.ui-jqgrid .ui-jqgrid-htable th div {
    overflow: hidden;
    position: relative;
    height: 30px;
    vertical-align:middle !important;
    padding-top: 10px !important;
    
}
</style>

<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 설문결과 조회</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form">
				<input id="projectCode" name="projectCode" type="text"
					class="pure-input-rounded" placeholder="프로젝트 코드"> <input id="projectName"
					name="projectName" type="text" class="pure-input-rounded" placeholder="프로젝트 명">
				<a id="btn-search" style="font-size: 0.8em">조회</a>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-excel">엑셀저장</a>
				<a id="btn-expired">설문종료</a>
			</div>
		</div>
	</div>
	<br />
</div>
<div id="tabs" >
		<ul>
			<li><a href="#T2">사업부 담당자</a></li>
			<li><a href="#T3">현업 사용자</a></li>
			<li><a href="#T4">시스템 운영자</a></li>
		</ul>
		<div id="T2" class="pure-u-11-12">
			<table id="jqg-table"></table>
			<div id="jqg-pager"></div>
			<br/><br/>
			<table id="jqg-table2"></table>
			<div id="jqg-pager2"></div>
		</div>
		<div id="T3" class="pure-u-11-12">
			<table id="jqg-table3"></table>
			<div id="jqg-pager3"></div>
			<br/><br/>
			<table id="jqg-table4"></table>
			<div id="jqg-pager4"></div>
		</div>
		<div id="T4" class="pure-u-11-12">
			<table id="jqg-table5"></table>
			<div id="jqg-pager5"></div>
			<br/><br/>
			<table id="jqg-table6"></table>
			<div id="jqg-pager6"></div>
		</div>
</div>
<br/>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">품질현황 관리</span>
    <span id="sidebar-menu">만족도설문결과조회</span>
</div>
<br />
 -->
<form id="excelForm" name="excelForm" action="<s:url value="/multiExcelDownload" />" method="post">
    <input type="hidden" name="data" id="data"/>
    <input type="hidden" name="filename" id="filename" value="만족도설문조사결과.xlsx"/>
    <!-- <input type="hidden" name="version" id="version" value="xlsx"/> -->
</form>