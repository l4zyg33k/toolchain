<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		//검색버튼 파일 돋보기 모양 아이콘 주기
		$("#btn-add").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function(event) {
			jQuery("#jqg-table").editGridRow("new", {
				recreateForm : true,
				closeAfterAdd : true,
				closeAfterEdit : true,
				beforeShowForm: function(form){
					//입력창 컬럼 히든 처리
					setGridColHidden(form);
				}
			});

		});
		
		/* 수정버튼 처리 */
		$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					jQuery("#jqg-table").editGridRow(
							jQuery('#jqg-table').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterAdd : true,								
								closeAfterEdit : true,
								beforeShowForm: function(form){	
									//입력창 컬럼 히든 처리
									setGridColHidden(form);
								}
							});

				});
		

		/* 삭제버튼 처리 */
		$("#btn-del")
				.button({
					icons : {
						primary : 'ui-icon-trash'
					}
				})
				.click(
						function(event) {
							dialog(
									'삭제 할까요?',
									function() {
										var rowId = $('#jqg-table').jqGrid(
												'getGridParam', 'selrow');
										var url = '<s:url value="/rest/qam/projectQualities/SatisfactionSurveyQuestion/edit" />';
										var data = {
											oper : 'del',
											id : rowId
										};
										$.post(url, data, function(data,
												textStatus, XMLHttpRequest) {
											$('#jqg-table').trigger(
													"reloadGrid");
										});
									});
						});

		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid1", true,true,true);

		//대상자유형에따른 설문유형설정		
		function setChildSelectBox2(pObject, cObject, defalutMsg) {
			var id = pObject.val();
			$.ajax({
				type : 'GET',
				url : '<s:url value="/rest/qam/getchildcode/'+id + '"/>',
				success : function(json) {
					cObject.children().remove();
					if (defalutMsg != undefined && defalutMsg != ""
							&& defalutMsg != null) {
						cObject.append($('<option>').text(defalutMsg).attr(
								'value', null));
					}
					$.each(json, function(i, value) {
						cObject.append($('<option>').text(value.name).attr(
								'value', value.id));
					});
				}
			});
		}

		//사용자유형콤보 변경시 설문유형설정
		$('#userType').change(function() {
			setChildSelectBox2($('#userType'), $('#surveyType'), "---선택---");
		});

		//설문유형변경시 조회 처리
		$('#surveyType').change(function() {
			//그리드 초기화
			$("#jqg-table").jqGrid("clearGridData");
			$("#jqg-table2").jqGrid("clearGridData");
			$("#jqg-table3").jqGrid("clearGridData");
			
			//1번유형 그리드 초기화
			$("#jqg-table").setGridParam({
				postData : {
					userType : $("#userType").val(),
					surveyType : $("#surveyType").val()
				}
			}).trigger("reloadGrid");
			
			//2번유형 그리드 초기화
			$("#jqg-table2").setGridParam({
				postData : {
					userType : $("#userType").val(),
					surveyType : $("#surveyType").val()
				}
			}).trigger("reloadGrid");
			
			//3번유형 그리드 초기화
			$("#jqg-table3").setGridParam({
				postData : {
					userType : $("#userType").val(),
					surveyType : $("#surveyType").val()
				}
			}).trigger("reloadGrid");
		});

		$("#jqg-table")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/question/T210"/>',
							editurl : '<s:url value="/rest/qam/survey/question/edit"/>',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호',
									'문항', '키워드' ],
							colModel : [ {
								name : 'id',
								hidden : true,
								key : true
							}, {
								name : 'questionType.name',
								align : 'center',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val("T210");
									},									
									readonly : true
								}
							}, {
								name : 'userType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#userType").val());
									},
									readonly : true
								}
							}, {
								name : 'surveyType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#surveyType").val());
									},
									editrules: {required: true},
									readonly : true
								}
							}, {
								name : 'questionNumber',
								align : 'center',
								width : 100,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).mask('9');
									}
								}
							}, {
								name : 'surveyQuestion',
								align : 'left',
								width : 400,
								editable : true,
								edittype : 'text',
								editrules: {required: true}
							}, {
								name : 'keyWord',
								align : 'left',
								width : 100,
								editable : true,
								edittype : 'text',
								editrules: {required: true}
							} ],
							pager : '#jqg-pager',
							rowNum : 10,
							rowList : [ 10, 20, 30 ],
							sortname : 'questionNumber',
							sortorder : 'asc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '1번 문항',
							loadComplete : function() {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid1", false,true,true);
								return false;
							},
							onSelectRow : function(id) {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid1", false,false,false);
								return false;
							}
						});

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {},
				{}, {}, search_options);
		
		//--2번문항 관리
		
		$("#btn-add2").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function(event) {
			jQuery("#jqg-table2").editGridRow("new", {
				recreateForm : true,
				closeAfterAdd : true,				
				beforeShowForm: function(form){
					//입력창 컬럼 히든 처리
					setGridColHidden(form);
				}
			});

		});
		$("#btn-edit2").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					jQuery("#jqg-table2").editGridRow(
							jQuery('#jqg-table2').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterAdd : true,								
								beforeShowForm: function(form){
									//입력창 컬럼 히든 처리
									setGridColHidden(form);
								}
							});

				});

		;

		$("#btn-del2")
				.button({
					icons : {
						primary : 'ui-icon-trash'
					}
				})
				.click(
						function(event) {
							dialog(
									'삭제 할까요?',
									function() {
										var rowId = $('#jqg-table2').jqGrid(
												'getGridParam', 'selrow');
										var url = '<s:url value="/rest/qam/survey/question/edit" />';
										var data = {
											oper : 'del',
											id : rowId
										};
										$.post(url, data, function(data,
												textStatus, XMLHttpRequest) {
											$('#jqg-table2').trigger(
													"reloadGrid");
										});
									});
						});

		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid2", true,true,true);

		$("#jqg-table2")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/question/T220"/>',
							editurl : '<s:url value="/rest/qam/survey/question/edit"/>',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호',
									'문항', '키워드' ],
							colModel : [ {
								name : 'id',
								hidden : true,
								key : true
							}, {
								name : 'questionType.name',
								align : 'center',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val("T220");
									},									
									readonly : true
								}
							}, {
								name : 'userType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#userType").val());
									},
									readonly : true
								}
							}, {
								name : 'surveyType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#surveyType").val());
									},
									editrules: {required: true},
									readonly : true
								}
							}, {
								name : 'questionNumber',
								align : 'center',
								width : 100,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).mask('9');
									}
								}
							}, {
								name : 'surveyQuestion',
								align : 'left',
								width : 400,
								editable : true,
								edittype : 'text',
								editrules: {required: true}
							}, {
								name : 'keyWord',
								align : 'left',
								width : 100,
								editable : true,
								//hidden : true,
								edittype : 'text'
							} ],
							pager : '#jqg-pager2',
							rowNum : 10,
							rowList : [ 10, 20, 30 ],
							sortname : 'questionNumber',
							sortorder : 'asc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '2번 문항',
							loadComplete : function() {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid2", false,true,true);
								return false;
							},
							onSelectRow : function(id) {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid2", false,false,false);
								return false;
							}
						});

		$("#jqg-table2").jqGrid('navGrid', '#jqg-pager2', navigator_options, {},
				{}, {}, search_options);
		
		
		
	/* 3번문항 관리 */
		$("#btn-add3").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function(event) {
			jQuery("#jqg-table3").editGridRow("new", {
				recreateForm : true,
				closeAfterAdd : true,
				//폼로드시 그리드 컬럼 히든처리
				beforeShowForm: function(form){
					$('#tr_questionType\\.name', form).hide();
					$('#tr_userType\\.id', form).hide();
					$('#tr_surveyType\\.id', form).hide();
				}
			});

		});
		$("#btn-edit3").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
				function(event) {
					jQuery("#jqg-table3").editGridRow(
							jQuery('#jqg-table3').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterAdd : true,
								//폼로드시 히든처리
								beforeShowForm: function(form){
									$('#tr_questionType\\.name', form).hide();
									$('#tr_userType\\.id', form).hide();
									$('#tr_surveyType\\.id', form).hide();
								}
							});

				});

		;

		$("#btn-del3")
				.button({
					icons : {
						primary : 'ui-icon-trash'
					}
				})
				.click(
						function(event) {
							dialog(
									'삭제 할까요?',
									function() {
										var rowId = $('#jqg-table3').jqGrid(
												'getGridParam', 'selrow');
										var url = '<s:url value="/rest/qam/survey/question/edit" />';
										var data = {
											oper : 'del',
											id : rowId
										};
										$.post(url, data, function(data,
												textStatus, XMLHttpRequest) {
											$('#jqg-table3').trigger(
													"reloadGrid");
										});
									});
						});

		
		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid3", true,true,true);
		
		$("#jqg-table3")
				.jqGrid(
						{
							url : '<s:url value="/rest/qam/survey/question/T230"/>',
							editurl : '<s:url value="/rest/qam/survey/question/edit"/>',
							datatype : 'json',
							mtype : 'POST',
							colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호',
									'문항', '키워드' ],
							colModel : [ {
								name : 'id',
								hidden : true,
								key : true
							}, {
								name : 'questionType.name',
								align : 'center',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val("T230");
									},									
									readonly : true
								}
							}, {
								name : 'userType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#userType").val());
									},
									readonly : true
								}
							}, {
								name : 'surveyType.id',
								align : 'left',
								hidden : true,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).val($("#surveyType").val());
									},
									editrules: {required: true},
									readonly : true
								}
							}, {
								name : 'questionNumber',
								align : 'center',
								width : 100,
								editable : true,
								edittype : 'text',
								editrules: {required: true},
								editoptions : {
									dataInit : function(e) {
										$(e).mask('9');
									}
								}
							}, {
								name : 'surveyQuestion',
								align : 'left',
								width : 400,
								editable : true,
								edittype : 'text',
								editrules: {required: true}
							}, {
								name : 'keyWord',
								align : 'left',
								width : 100,
								editable : true,
								hidden : true,
								edittype : 'text'
							} ],
							pager : '#jqg-pager3',
							rowNum : 10,
							rowList : [ 10, 20, 30 ],
							sortname : 'questionNumber',
							sortorder : 'asc',
							viewrecords : true,
							gridview : true,
							autoencode : true,
							height : "100%",
							caption : '3번 문항',
							loadComplete : function() {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid3", false,true,true);
								return false;
							},
							onSelectRow : function(id) {
								//버튼 disabled 처리(grid, add, edit, del)
								setButtonDisable("grid3", false,false,false);
								return false;
							}
						});

		$("#jqg-table3").jqGrid('navGrid', '#jqg-pager3', navigator_options, {},
				{}, {}, search_options);
		
		/* 그리드 컬럼 히든 처리*/		
		function setGridColHidden(gridObject){
			$('#tr_questionType\\.name', gridObject).hide();
			$('#tr_userType\\.id', gridObject).hide();
			$('#tr_surveyType\\.id', gridObject).hide();
		}
		
		/* 그리드에 따른 버튼 disabled 처리*/
		function setButtonDisable(buttonIndex,add,edit,del){
			if (buttonIndex == "grid1"){
				$("#btn-add").button("option", "disabled",add);
				$("#btn-edit").button("option", "disabled",edit);
				$("#btn-del").button("option", "disabled", del);
			}else if (buttonIndex == "grid2"){
				$("#btn-add2").button("option", "disabled",add);
				$("#btn-edit2").button("option", "disabled",edit);
				$("#btn-del2").button("option", "disabled", del);
			}else if (buttonIndex == "grid3"){
				$("#btn-add3").button("option", "disabled",add);
				$("#btn-edit3").button("option", "disabled",edit);
				$("#btn-del3").button("option", "disabled", del);
			}
		}

	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">만족도설문문항관리</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form">
				<select id="userType">
					<option value="">--- 선택 ---</option>
					<c:forEach var="item" items="${userTypeCode}">
						<option value="<c:out value="${item.id}"/>"><c:out
								value="${item.name}" /></option>
					</c:forEach>
				</select> <select id="surveyType">
					<option value="">--- 선택 ---</option>
				</select>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	
	<!-- 2번문항시작 -->
	<br/>
	<div class="pure-g">
		<div class="pure-u-1-2">
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add2">등록</a> <a id="btn-edit2">수정</a> <a id="btn-del2">삭제</a>
			</div>
		</div>
	</div>
	<br/>
	<table id="jqg-table2"></table>
	<div id="jqg-pager2"></div>
	<!-- 2번문항 종료-->
	
	<!-- 3번문항시작 -->
	<br/>
	<div class="pure-g">
		<div class="pure-u-1-2">
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add3">등록</a> <a id="btn-edit3">수정</a> <a id="btn-del3">삭제</a>
			</div>
		</div>
	</div>
	<br/>
	<table id="jqg-table3"></table>
	<div id="jqg-pager3"></div>
	<!-- 3번문항 종료-->
</div>
<br/>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">품질현황 관리</span> <span id="sidebar-menu">만족도설문문항관리</span>
</div>
 -->