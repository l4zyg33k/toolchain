<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
    $(function() {
    	$('#btn-cancel').button({
            icons: {
                primary: 'ui-icon-cancel'
            }
        }).click(function(event) {
            get('<s:url value="/qam/project/quality" />');
        });
    	
    	 $("#btn-add").button({
             icons: {
                 primary: 'ui-icon-plus'
             }
    	 }).click(function(event) {
     	        var code = $('#qualityProjectCode').val();
     	        var url = "<c:url value='/qam/quality/check/"+code+"/new'/>";
    	        get(url);
         });
    	 //수정버튼에 가위 모양 아이콘 주기
         $("#btn-edit").button({
             icons: {
                 primary: 'ui-icon-pencil'
             }
         });
    	 
    	  $("#btn-del").button({
              icons: {
                  primary: 'ui-icon-trash'
              }
          }).click(function(event) {
        	  dialog('삭제 할까요?', function() {
  				var rowId = $('#jqg-table').jqGrid('getGridParam', 'selrow');
  				var url = '<s:url value="/rest/qam/quality/check/edit" />';
  				var data = { oper: 'del', id: rowId};
  				$.post(url, data, function(data, textStatus, XMLHttpRequest) {
  		           	 					$('#jqg-table').trigger("reloadGrid");
  		        				});
  			});
          });
        $("#jqg-table").jqGrid({
            url: '<s:url value="/rest/qam/quality/check/${projectVO.code}" />',
            editurl : '<s:url value="/rest/qam/quality/check/edit" />',
            datatype: 'json',
            mtype: 'POST',
            colNames: ['ID','프로젝트코드','프로젝트명', '품질점검제목', '점검일','유형','단계','점검자'],
            colModel: [{
					name : 'id',
					align : 'center',
					search : false,
					hidden : false,
					key : true
			    }, {
                    name: 'project.code',
                    key: true,
                    align: 'center'
                }, {
                    name: 'project.name',       
                    search: false
                    //formatoptions:{baseLinkUrl:'projectDetail'}
                }, {
                    name: 'qualityInspectionTitle',
                    align: 'center',
                    search: false
                }, {
                    name: 'qualityInspectionDate',
                    align: 'center',
                    search: false
                }, {
                    name: 'qualityInspectionType.name',
                    align: 'center',
                    search: false
                }, {
                    name: 'qualityInspectionStage.name',
                    align: 'center',
                    search: false
                }, {
                    name: 'qualityInspecter.name',
                    align: 'center',
                    search: false
                }],
            pager: '#jqg-pager',
            rowNum: 20,
            rowList: [20, 40, 60],
            sortname: 'project.code',
            sortorder: 'desc',
            viewrecords: true,
            gridview: true,
            autoencode: true,
            height: "100%",
            caption: '프로젝트 품질점검 목록',
            
            loadComplete: function() {
            	//기본버튼 disable처리 그리드 선택시 enable처리
            	$("#btn-edit").button("option", "disabled", true);
            	$("#btn-del").button("option", "disabled", true);
            },
            onSelectRow: function(id) {
            	$("#btn-edit").button("option", "disabled", false);
            	$("#btn-del").button("option", "disabled", false);
            	$('#btn-edit').prop('href', '<c:url value="/pms/qualityCheckResult/" />' + id + '/edit');
            	
            }
            
        });

        $("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {}, {}, {}, search_options);
        //그리드 링크&색상지정
        
      /*   function reloadGrid() {
        	alert();
			$("#jqg-table").setGridParam({
				postData : {
					query : $("#qualityProjectCode").val()
				}
			}).trigger("reloadGrid");
		}
        
        reloadGrid(); */
    });
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">품질점검 결과</span>
	</div>
</div>

<div class="qms-content-main">
<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
            </form>
        </div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add" >등록</a>
				<a id="btn-edit">수정</a>
				<a id="btn-del" >삭제</a>
				<a id="btn-cancel" >취소</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>

    <form:form class="pure-form" modelAttribute="projectVO">
        <form:hidden id="qualityProjectCode" name="qualityProjectCode" path="code" />
    </form:form>   
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">품질현황 관리</span>
    <span id="sidebar-menu">프로젝트 품질관리</span>
</div>
 -->
<br />