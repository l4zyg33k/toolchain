<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--
VO의 id 값 유무로 등록인지 수정인지 판단 한다.
--%>
<c:set value="${empty qualityCheckResultVO.id}" var="isNew" />
<script type="text/javascript">
<c:if test="${isNew}">
var rowdata = '${rowdata}';
</c:if>
	$(function() {
		 /*
         * 등록 버튼
         */
        $('#btn-add').button({
            icons: {
                primary: 'ui-icon-plus'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('editGridRow', 'new', {
                <%--
                등록인 경우 전송 후 리로드를 하지 않도록 하여 입력 값을 보존한다
                --%>
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true,
                closeAfterAdd : true,
                closeAfterEdit : true,
                width:600,
				height:300
                
            });
        });
        /*
         * 수정 버튼
         */
        $('#btn-edit').button({
            icons: {
                primary: 'ui-icon-pencil'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('editGridRow', $('#jqg-table').jqGrid('getGridParam', 'selrow'), {
                <%--
                등록인 경우 전송 후 리로드를 하지 않도록 하여 입력 값을 보존한다.
                --%>
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true,
                closeAfterEdit : true,
                width:600,
				height:300
            });
        });
        /*
         * 삭제 버튼
         */
        $("#btn-del").button({
            icons: {
                primary: 'ui-icon-trash'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('delGridRow', $('#jqg-table').jqGrid('getGridParam', 'selrow'), {
                <%--
                등록인 경우 전송 후 리로드를 하지 않도록 하여 입력 값을 보존한다.
                --%>
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true
            });
        });
		/*
		 * 저장 버튼
		 */
		$('#btn-save').button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function(event) {
			if ($('#project\\.code').val().length === 0) {
                $('#project\\.code').val(null);
            }

            if ($('#project\\.name').val().length === 0) {
                $('#project\\.name').val(null);
            }            
            <%--
            등록인 경우 jqGrid의 행 데이타를 JSON으로 변경하여 전송 하도록 한다.
            --%>
            <c:if test="${isNew}">
            $('#rowdata').val(JSON.stringify($("#jqg-table").getRowData()));
            </c:if>
			$('#frm-qualityCheckResult').submit();
		});
		/*
		 * 취소 버튼
		 */
		$('#btn-cancel').button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		}).click(function(event) {
			 var code = $('#qualityProjectCode').val();
			 var url = "<c:url value='/qam/quality/check/"+code+"'/>";
 	        get(url);
		});
		
		$('#project\\.code').autocomplete({
            autoFocus: true,
            minLength: 1,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'code';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/pms/projects" />', request, response);
            },
            select: function(event, ui) {
                $('#project\\.code').val(ui.item.code);
                $('#project\\.name').val(ui.item.name);
                $('#project\\.il\\.name').val(ui.item.il.name);
                return false;
            }
	}).data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li>').append('<a>' + item.code + ' ' + item.name + '</a>').appendTo(ul);
        };
        
        $('#project\\.name').autocomplete({
            autoFocus: true,
            minLength: 1,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'name';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/pms/projects" />', request, response);
            },
            select: function(event, ui) {
                $('#project\\.code').val(ui.item.code);
                $('#project\\.name').val(ui.item.name);
                $('#project\\.il\\.name').val(ui.item.il.name);
                return false;
            },
            change: function (event, ui) {
                if (!ui.item) {
                	$('#project\\.code').val('');
                    $('#project\\.name').val('');
                    $('#project\\.il\\.name').val('');
                }
            }
	}).data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li>').append('<a>' + item.code + ' ' + item.name + '</a>').appendTo(ul);
        };
        
        
        $('#qualityInspecter\\.name').autocomplete({
            autoFocus: true,
            source: function(request, response) {
                request['search.search'] = true;
                request['search.field'] = 'name';
                request['search.value'] = request['term'];
                request['search.oper'] = 'cn';

                $.post('<s:url value="/rest/adm/accounts" />', request, response);
            },
            select: function(event, ui) {
                $('#qualityInspecter\\.username').val(ui.item.username);
                $('#qualityInspecter\\.name').val(ui.item.name);
                return false;
            },
            change: function (event, ui) {
                if (!ui.item) {
                	$('#qualityInspecter\\.username').val('');
                    $('#qualityInspecter\\.name').val('');
                }
            }
        }).data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li>').append('<a>' + item.name + ', ' + item.team.name + '</a>').appendTo(ul);
        };
        $('#qualityInspectionDate').datepicker();
        $('#qualityInspectionDate').mask('9999-99-99');
        
        
        $("#jqg-table").jqGrid({
            <c:choose>
                <%--
                등록인 경우 데이타 소스를 local로 하고 더미 URL로 행 데이타를 전송한다.
                --%>
                <c:when test="${isNew}">
                data: rowdata,
                editurl: '<s:url value="/rest/dummy"/>',
                </c:when>
                <c:otherwise>
                url: '<s:url value="/rest/qam/quaility/check/${qualityCheckResultVO.id}/details"/>',                
                editurl: '<s:url value="/rest/qam/quaility/check/${qualityCheckResultVO.id}/details/edit"/>',            
                mtype: 'POST',
                </c:otherwise>
            </c:choose>
                datatype: '${isNew ? "local" : "json"}',
                colNames: ['#', '결과 유형', '부적합 내용', '시정조치계획/결과','권고일','합의일','완료일'],
                colModel: [{
                        name: 'id',
                        hidden: true,
                        key: true
                        
                    }, {
                        name: 'qualityResultType.name',
                        <c:if test="${isNew}">
                        formatter: 'select',
                        </c:if>
                        editable: true,
                        edittype: 'select', 
                        editrules: {required: true},
                        editoptions: {value: ''}
                    }, {
                        name: 'qualityResultContent',
                        width: 300,
                        editable: true,
                        edittype: 'textarea',
                        editoptions : {
							rows : 3,
							cols : 70,
							dataInit : function(e) {
								$(e).val(
										$(e).val().replace(/\<br\>/gi,'\r\n'));
								}

						},
						formatter : function(e) {

							if (e != null) {
								return e.replace(/\r?\n/g,'<br>')
							} else
								return "";

						},
                        editrules: {required: true}
                    }, {
                        name: 'correctiveAction',
                        width: 300,
                        editable: true,
                        edittype: 'textarea',
                        editoptions : {
							rows : 3,
							cols : 70,
							dataInit : function(e) {
								$(e).val(
										$(e).val().replace(/\<br\>/gi,'\r\n'));
								}

						},
						formatter : function(e) {

							if (e != null) {
								return e.replace(/\r?\n/g,'<br>')
							} else
								return "";

						}
                    }, {
                    	 name: 'actionSuggestDate',
                    	 align : 'center',
 						editable : true,
 						edittype : 'text',
 						editrules : {
 							required : true
 						},
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						},
                         editrules: {required: true}
                    }, {
                        name: 'actionAgreementDate',
                        align : 'center',
 						editable : true,
 						edittype : 'text',
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						}
                    }, {
                        name: 'actionCompleteDate',
                        align : 'center',
 						editable : true,
 						edittype : 'text',
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						}
                    }],
                <%--
                등록인 경우 페이저를 표시하지 않는다.
                --%>
                <c:if test="${not isNew}">
                pager: '#jqg-pager',
                </c:if>
                rowNum: 20,
                rowList: [20, 40, 60],
                sortname: 'id',
                sortorder: 'desc',
                viewrecords: true,
                gridview: true,
                autoencode: true,
                height: "100%",
                caption: '결함 목록',
                loadComplete: function() {
                	<c:choose>
                    <c:when test="${isNew}">
                    </c:when>
                    <c:otherwise>
                	$.ajax({
                        type: 'GET',
                        url: '<s:url value="/qam/quaility/check/${qualityCheckResultVO.id}"/>',
                        success : function(data) {
                        	$("#requiredCount").val(data.requiredCount);
                        	$("#opinionCount").val(data.opinionCount);
                        }
                    });            	
                    </c:otherwise>
                	</c:choose>
                },
                onSelectRow: function(id) {
                },
                beforeRequest: function() {
                    $.ajax({
                        type: 'GET',
                        url: '<s:url value="/rest/pms/quality/check/types"/>',
                        success : function(data) {
                            $("#jqg-table").setColProp('qualityResultType.name', { 
                                editoptions: {
                                    value : data
                                }
                            });  
                        }
                    });  
                }                
            });

            <%--
            등록인 경우 페이저를 표시하지 않는다.
            --%>
            <c:if test="${not isNew}">
            $("#jqg-table").jqGrid('navGrid', '#jqg-pager', {add: false, edit: false, del: false, search: false, refresh: false}, {}, {}, {}, {});
            </c:if>  
            
            jQuery("#jqg-table").trigger("reloadGrid");
        
        
        
	});
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">품질점검결과등록</span>
    </div>
</div>
<div class="qms-content-main">
    <c:choose>
        <c:when test="${isNew}">
            <s:url value="/pms/quality/check" var="action" />
        </c:when>
        <c:otherwise>
            <s:url value="/pms/quality/check/${qualityCheckResultVO.id}"
                   var="action" />
        </c:otherwise>
    </c:choose>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-save">저장</a> <a id="btn-cancel">취소</a>
			</div>
		</div>
	</div>

	<form:form id="frm-qualityCheckResult" action="${action}"
		method="${isNew ? 'POST' : 'PUT'}"
		modelAttribute="qualityCheckResultVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>품질점검결과</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project.code">프젝트 코드</form:label>
					<form:input path="project.code" placeholder="코드를 입력하세요."
						readonly="true" />
					<form:errors path="project.code" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="project.name">프로젝트 이름</form:label>
					<form:input path="project.name" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="project.name" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="project.il.name">IL명 </form:label>
					<form:input path="project.il.name" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="project.il.name" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="qualityInspecter.name">검토자 </form:label>
					<form:input path="qualityInspecter.name" placeholder="이름를 입력하세요."
						readonly="" />
					<form:errors path="qualityInspecter.name"
						cssClass="ui-state-error-text" />
				</div>
			</div>

			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="qualityInspectionDate">검토일자</form:label>
					<form:input path="qualityInspectionDate" placeholder="검토일자를 입력하세요."
						readonly="" />
					<form:errors path="qualityInspectionDate"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="qualityInspectionTitle">제목</form:label>
					<form:input path="qualityInspectionTitle" placeholder="제목를 입력하세요."
						readonly="" />
					<form:errors path="qualityInspectionTitle"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="qualityInspectionStage">단계</form:label>
					<form:select path="qualityInspectionStage" items="${phases}"
						itemLabel="name" itemValue="id" />
					<form:errors path="qualityInspectionStage"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="qualityInspectionType">검토종류</form:label>
					<form:select path="qualityInspectionType"
						items="${qualityInspectionTypes}" itemLabel="name" itemValue="id" />
					<form:errors path="qualityInspectionType"
						cssClass="ui-state-error-text" />
				</div>
			</div>

			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="qualifingSpendTime">소요시간</form:label>
					<form:input path="qualifingSpendTime" placeholder="코드를 입력하세요."
						readonly="" />
					<form:errors path="qualifingSpendTime"
						cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="requiredCount">필수 건수</form:label>
					<form:input path="requiredCount" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="requiredCount" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="opinionCount">권고 건수 </form:label>
					<form:input path="opinionCount" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="opinionCount" cssClass="ui-state-error-text" />
				</div>
			</div>
			<form:hidden path="id" />
			<form:hidden path="createdBy" />
			<form:hidden path="createdDate" />
			<form:hidden path="qualityInspecter.username" />
			<%--
            등록인 경우 jqGrid의 행 값을 가지로록 rowdata 요소를 가진다.
            --%>
			<c:if test="${isNew}">
				<input type="hidden" id="rowdata" name="rowdata" />
			</c:if>
		</fieldset>
	</form:form>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<div class="pure-g">
		<div class="pure-u-1">
			<table id="jqg-table"></table>
			<%--
            등록인 경우 페이저를 표시하지 않는다.
            --%>
			<c:if test="${not isNew}">
				<div id="jqg-pager"></div>
			</c:if>
		</div>
	</div>
	<form:form class="pure-form" modelAttribute="qualityCheckResultVO">
        <form:hidden id="qualityProjectCode" name="qualityProjectCode" path="project.code"/>
    </form:form>   
</div>
<!-- 
<div class="qms-content-hidden">
    <span id="header-menu">품질현황 관리</span>
    <span id="sidebar-menu">프로젝트 품질관리</span>
</div>
 -->
<br />