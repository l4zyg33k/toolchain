<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButton();
		initJqGrid();
	});
	
    //버튼 처리
    function initButton(){
		//취소버튼
		setCancleFormButton('#btn-cancel', false, '<s:url value="${survEvalFrmVO.getPrevUrl()}" />');
		//수정
		$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
			function(event) {
				jQuery("#jqg-table").editGridRow(
					jQuery('#jqg-table').jqGrid('getGridParam','selrow'), {
						recreateForm : true,
						closeAfterEdit : true,
						closeAfterAdd : true
					});
			}
		);
		//설문결과 가져오기
		$("#btn-get").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(
			function(event) {
				var page = "${survEvalFrmVO.page}";
				var rowNum = "${survEvalFrmVO.rowNum}";
				var rowId = "${survEvalFrmVO.rowId}";
	        	var code = "${survEvalFrmVO.code}";		        
		        var url = '<s:url value="/qam/surveval/" />' + code + '/get?page='
						   + page + '&rowNum=' + rowNum + '&rowId=' + rowId;
		        $(location).attr('href', url);
			    return false;
		});
		//수정
		$("#btn-edit2").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
			function(event) {
				jQuery("#jqg-table2").editGridRow(
						jQuery('#jqg-table2').jqGrid('getGridParam', 'selrow'), {
							recreateForm : true,
							closeAfterEdit : true,
							closeAfterAdd : true,
							width:600,
							height:230
						});
			}
		);
    }
    
	//산출물 측정결과  그리드 조회
	function initJqGrid() {	
		$("#jqg-table")
		.jqGrid(
			{
				url : '<s:url value="/rest/qam/surveval" />',
				editurl : '<s:url value="/rest/qam/surveval/edit" />',
				datatype : 'json',
				mtype : 'POST',
				colNames : [ 'ID', '조사 대상', '조사 예정일', '실제 조사일', '대상자', '응답수', '만족도', '의견'],
				colModel : [ {
					name : 'id',
					editable : true,
					edittype : 'text',
					hidden : true
					
				}, {
					name : 'userType',
					align : 'center',
					editable : true,
					formatter : 'select',
					edittype : 'select',
					width : '140',
					editrules : {
						required : true
					},
					editoptions : {
						disabled : true,
						value : '${recipients}'
					}
				}, {
					name : 'planDate',
					align : 'center',
					editable : true,
					edittype : 'text',
					editoptions : {
						dataInit : function(e) {
							$(e).datepicker();
							$(e).mask('9999-99-99');
						}
					}
				}, {
					name : 'actualDate',
					align : 'center',
					editable : true,
					edittype : 'text',
					editoptions : {
						dataInit : function(e) {
							$(e).datepicker();
							$(e).mask('9999-99-99');
						}
					}
				}, {
					name : 'recipientCnt',
					align : 'right',
					editable : true,
					edittype : 'text',
					editoptions : {
						dataInit : function(e) {
							$(e).mask("9?99");
						}
					}

				}, {
					name : 'answerCnt',
					align : 'right',
					editable : true,
					edittype : 'text',
					width : '80',
					editoptions : {
						dataInit : function(e) {
							$(e).mask("9?99");
						}
					}
				}, {
					name : 'point',
					align : 'right',
					editable : true,
					edittype : 'text',
					editoptions : {
						dataInit : function(e) {
							$(e).mask("9?.99");
						}
					}
				},{
					name : 'comment',
					editable : true,
					edittype : 'textarea',
					hidden : true,
					editoptions : {
						dataInit : function(e) {
							$(e).val($(e).val().replace(/\<br\>/gi, '\r\n'));
						}
					}
				}],
				rowNum : 10,
				sortname : 'userType',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				autoencode : true,
				height : "100%",
				caption : '만족도 점수',
				serializeGridData : function(postData) {
					postData.projectCode = $('#code').val();
					return postData;
				},
				loadComplete : function() {
					$("#btn-edit").button("option", "disabled",true);
				},
				onSelectRow : function(id) {
					$("#btn-edit").button("option", "disabled",false);
				},
				gridComplete : function() {
					//종합만족도 계산
					var cnt = 0;
					var sum = 0;
					var rowIds = $('#jqg-table').jqGrid('getDataIDs');
					for (var i = 0, len = rowIds.length; i < len; i++) {
						var tCnt = Number($('#jqg-table').getCell(rowIds[i], "answerCnt"));
						var point = Number($('#jqg-table').getCell(rowIds[i], "point"));
						cnt += tCnt;
						sum += tCnt * point;
					}
					if (cnt == 0) {
						$('#summaryPoint').val(0);
					} else {
						$('#summaryPoint').val(Math.round(sum / cnt * 100) / 100);
					}
				}
			});
		
		$("#jqg-table2")
			.jqGrid(
			{
				url : '<s:url value="/rest/qam/surveval" />',
				editurl : '<s:url value="/rest/qam/surveval/edit" />',
				datatype : 'json',
				mtype : 'POST',
				colNames : [ 'ID', '조사 대상', '조사 예정일', '실제 조사일', '대상자', '응답수', '만족도', '의견'],
				colModel : [
				{
					name : 'id',
					editable : true,
					edittype : 'text',
					hidden : true
				},
				{
					name : 'userType',
					align : 'center',
					editable : true,
					formatter : 'select',
					edittype : 'select',
					editrules : {
						required : true
					},
					width : '30',
					editoptions : {
						disabled : true,
						value : '${recipients}'
					}
				},
				{
					name : 'planDate',
					editable : true,
					edittype : 'text',
					hidden : true
				}, {
					name : 'actualDate',
					editable : true,
					edittype : 'text',
					hidden : true
				}, {
					name : 'recipientCnt',
					editable : true,
					edittype : 'text',
					hidden : true
				}, {
					name : 'answerCnt',
					editable : true,
					edittype : 'text',
					hidden : true
				}, {
					name : 'point',
					editable : true,
					edittype : 'text',
					hidden : true
				},
				{
					name : 'comment',
					align : 'left',
					editable : true,
					edittype : 'textarea',
					editoptions : {
						rows : 10,
						cols : 80,
						dataInit : function(e) {
							$(e).val($(e).val().replace(/\<br\>/gi, '\r\n'));
						}
					},
					formatter : function(e) {
						if (e != null) {
							return e.replace(/\r?\n/g, '<br>')
						} else {
							return "";
						}
					}
				} ],
				rowNum : 10,
				sortname : 'userType',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				autoencode : true,
				height : "100%",
				caption : '만족도 점수',
				serializeGridData : function(postData) {
					postData.projectCode = $('#code').val();
					return postData;
				},
				loadComplete : function() {
					$("#btn-edit2").button("option", "disabled", true);
				},
				onSelectRow : function(id) {
					$("#btn-edit2").button("option", "disabled", false);
				}
			});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">만족도 평가등록</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
        <div class="pure-u-1-2">
        </div>
        <div class="pure-u-1-2">
            <div class="qms-content-buttonset">
                <a id="btn-cancel">취소</a>
            </div>
        </div>
    </div>
	<form:form id="frm-project" modelAttribute="survEvalFrmVO" cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form:label path="code" class="pure-u-1-5">프로젝트 코드</form:label>
					<form:input path="code" class="pure-u-4-6" readOnly="readOnly" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="name" class="pure-u-1-6">프로젝트 명</form:label>
					<form:input path="name" class="pure-u-3-5" readOnly="readOnly" />
				</div>
			</div>			
		</fieldset>
		<fieldset>
			<legend>만족도 평가</legend>
			<div class="pure-u-1">
				<form id="query-form" class="pure-form">
					<div class="qms-grid-top-notify">
						<label>종합만족도</label> <input id="summaryPoint" type="text"
							class="pure-input-rounded" readOnly>
					</div>
				</form>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-edit">수정</a>
						<a id="btn-get">설문결과 가져오기</a>
					</div>
				</div>
			</div>
		</fieldset>
		<table id="jqg-table"></table>
		<br/>
		<div class="pure-g">
			<div class="pure-u-1-2">
				<form class="pure-form"></form>
			</div>
			<div class="pure-u-1-2">
				<div class="qms-content-buttonset">
					<a id="btn-edit2">수정</a>
				</div>
			</div>
		</div>		
		<br/>
		<table id="jqg-table2"></table>				
	</form:form>

</div>
<br />