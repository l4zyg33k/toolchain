<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		initButton();
		initJqGrid();
		initNavGrid();	
	});	
	
    //버튼 처리
    function initButton(){
		$("#btn-add").button({
			icons : {
				primary : 'ui-icon-plus'
			}
		}).click(function(event) {
			jQuery("#jqg-table").editGridRow("new", {
				recreateForm : true,
				closeAfterAdd : true
			});
		});
		
		$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			}
		}).click(
			function(event) {
				jQuery("#jqg-table").editGridRow(
					jQuery('#jqg-table').jqGrid('getGridParam', 'selrow'), {
						recreateForm : true,
						closeAfterEdit : true
					});
			});

		//삭제처리
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
		
		//메일발송
		$("#btn-mail")
		.button({
			icons : {
				primary : 'ui-icon-mail-closed'
			}
		})
		.click(
				function(event) {
					dialog(
							'메일발송을 하시겠습니까?',
							function() {
								var data = $('#jqg-table').getRowData();
								var sendMail = new Array();
								$.each(data,function(i){
									if ( data[i].gridCheckBox == 'Yes' ){
										sendMail.push(data[i].id);
									}
								});
								var postData = JSON.stringify(sendMail);
								var url = '<s:url value="/rest/qam/custeval/send/${custEvalFrmVO.project.code}"/>';
								var data = {
									mailList : postData
								};
								$.post(url, data, function(data,textStatus, XMLHttpRequest) {
									$('#jqg-table').trigger("reloadGrid");
								});
							});
				});

		//기본버튼 disable처리
		$("#btn-edit").button("option", "disabled", true);
		$("#btn-del").button("option", "disabled", true);    	
    }
    
	//그리드 조회
	function initJqGrid() {	
		$("#jqg-table")
		.jqGrid(
				{
					url : '<s:url value="/rest/qam/custeval" />',
					editurl : '<s:url value="/rest/qam/custeval/edit" />',
					datatype : 'json',
					mtype : 'POST',
					//datatype : 'local',
					colNames : [ 'ID','메일전송' ,'사용자유형','이름', 'ID','이메일','부서', '직급','메일발송일', '프로젝트코드' ],
					colModel : [ {
						name : 'id',
						align : 'center',
						search : false,
						hidden : true,
						key : true
					}, {
						name : 'gridCheckBox',
						align : 'center',
						width : 60,
						formatter: "checkbox",
						formatoptions: { disabled: false}
					}, {
						name : 'userType',
						align : 'center',
						width : 100,
						editable : true,
						formatter : 'select',
						edittype : 'select',
						editrules : {
							required : true
						},
						editoptions : {
							value : '${recipients}'
						}
					}, {
						name : 'recipientName',
						align : 'left',
						width : 100,
						editable : true,
						edittype : 'text',
						editoptions : {
							dataInit : function(e) {
								$(e).autocomplete({
									autoFocus : true,
									source : parseUrl('/rest/adm/acntmng/all'),
									select : function(event, ui) {
										$("#recipientName").val(ui.item.name);
										$("#recipient").val(ui.item.username);
										$("#recipientEmail").val(ui.item.email);
										$("#recipientTeamName").val(ui.item.team);
										$("#recipientPositionName").val(ui.item.position);
										return false;
									},
									minLength : 1,											
									open : function() {
										$(e).autocomplete("widget").css('z-index',1000);
										return false;
									},
									change : function(event, ui) {
										if (ui.item == null) {
											$("#recipientName").val('');
											$("#recipient").val('');
											$("#recipientEmail").val('');
											$("#recipientTeamName").val('');
											$("#recipientPositionName").val('');
										}
									}
								}).data('ui-autocomplete')._renderItem = function(ul, item) {
									return $('<li>').append('<a>' + item.name + ' (' + item.team + ')</a>').appendTo(ul);
								};
							}
						}
					}, {
						name : 'recipient',
						align : 'left',
						width : 90,
						editable : true,
						edittype : 'text',
						editoptions : {
							disabled : true
						}
					}, {
						name : 'recipientEmail',
						align : 'left',
						width : 130,
						editable : true,
						edittype : 'text',
						editoptions : {
							disabled : true
						}

					}, {								
						name : 'recipientTeamName',
						align : 'left',
						width : 100,
						editable : true,
						edittype : 'text',
						editoptions : {
							disabled : true
						}
					}, {								
						name : 'recipientPositionName',
						align : 'left',
						width : 100,
						editable : true,
						edittype : 'text',
						editoptions : {
							disabled : true
						}
					}, {								
						name : 'sentDate',
						align : 'left',
						width : 80,
						edittype : 'text',
						search : false,
						editable : false
					}, {
						name : 'project',
						align : 'center',
						search : false,
						hidden : true,
						editable : true,
						edittype : 'text',
						editoptions : {
							dataInit : function(e) {
								$(e).val($('#project\\.code').val());
							},
							readonly : true
						}
					}],
					serializeGridData : function(postData) {
						postData.project = $('#project\\.code').val();
						return postData;
					},
					pager : '#jqg-pager',
					rowNum : 10,
					sortname : 'id',
					sortorder : 'desc',
					height : "100%",
					caption : '프로젝트 목록',
					loadComplete : function() {
						$("#btn-edit").button("option", "disabled",true);
						$("#btn-del").button("option", "disabled", true);
					},
					onSelectRow : function(id) {
						$("#btn-edit").button("option", "disabled",false);
						$("#btn-del").button("option", "disabled",false);
					}
				});		
	}
	
	//그리드 버튼 처리 
	function initNavGrid() {		
		//$("#jqg-table").jqGrid('navGrid', '#jqg-pager', navigator_options, {}, {}, {}, search_options);
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
				edit : true,
				add : true,
				del : true,
				view : false
			}, {
			}, {
			}, {
			}, {}, {});
	}	
</script>	
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 품질관리</span>
	</div>
</div>

<div class="qms-content-main">
	<form:form id="frm-project" modelAttribute="custEvalFrmVO" cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>고객만족도 대상자 관리</legend>
			<div class="pure-control-group">
				<div class="pure-u-3-8">
					<form:label path="project.code">프로젝트 코드</form:label>
					<form:input path="project.code" class="pure-u-1-3" readOnly="readOnly" />
				</div>
				<div class="pure-u-3-5">
					<form:label path="project.name">프로젝트 명</form:label>
					<form:input path="project.name" class="pure-u-7-12" readOnly="readOnly" />
				</div>
			</div>
			<div class="pure-control-group">
				<div class="pure-u-3-8">
					<form:label path="project.qa.name">품질 관리자</form:label>
					<form:input path="project.qa.name" class="pure-u-1-3" readOnly="readOnly" />
				</div>
				<div class="pure-u-3-5">
					<form:label path="project.qa.name">프로젝트 관계자</form:label>
					<form:input path="project.qa.name" class="pure-u-7-12" readOnly="readOnly" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-add">등록</a>
						<a id="btn-edit">수정</a>
						<a id="btn-del">삭제</a>
						<a id="btn-mail">메일발송</a>
					</div>
				</div>
			</div>
		</fieldset>		
		<table id="jqg-table"></table>
		<div id="jqg-pager"></div>
	</form:form>

</div>
<br />