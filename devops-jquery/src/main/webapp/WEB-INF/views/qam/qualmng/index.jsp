<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<sec:authorize ifAnyGranted="ROLE_ADMIN, ROLE_QA" var="isAuthorized" />
<script type="text/javascript">
	$(function() {
		//버튼 처리
		initButton();

		//그리드 조회
		initJqGrid();

		//네비그리드 세팅
		initNavGrid();

		//엔터키 이벤트 막기
		$('#projectName').bind("keypress", function(e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				return false;
			}
		});
	});

	//버튼 처리
	function initButton() {
		//조회
		setSearchButton('#btn-search', false, '#refresh_jqg-table');

		//진척도 평가등록
		$("#btn-edit1").button({
			icons : {
				primary : '${icon}'
			}
		})
				.click(
						function(event) {
							var page = $('#jqg-table').jqGrid('getGridParam',
									'page');
							var rowNum = $('#jqg-table').jqGrid('getGridParam',
									'rowNum');
							var rowId = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var code = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var url = '<s:url value="/qam/progeval/" />' + code
									+ '/form?page=' + page + '&rowNum='
									+ rowNum + '&rowId=' + rowId;
							$(location).attr('href', url);
							return false;
						});

		//산출물 평가등록
		$("#btn-edit2").button({
			icons : {
				primary : '${icon}'
			}
		})
				.click(
						function(event) {
							var page = $('#jqg-table').jqGrid('getGridParam',
									'page');
							var rowNum = $('#jqg-table').jqGrid('getGridParam',
									'rowNum');
							var rowId = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var code = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var url = '<s:url value="/qam/docueval/" />' + code
									+ '/form?page=' + page + '&rowNum='
									+ rowNum + '&rowId=' + rowId;
							$(location).attr('href', url);
							return false;
						});

		//만족도 평가등록
		$("#btn-edit3").button({
			icons : {
				primary : '${icon}'
			}
		})
				.click(
						function(event) {
							var page = $('#jqg-table').jqGrid('getGridParam',
									'page');
							var rowNum = $('#jqg-table').jqGrid('getGridParam',
									'rowNum');
							var rowId = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var code = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var url = '<s:url value="/qam/surveval/" />' + code
									+ '/form?page=' + page + '&rowNum='
									+ rowNum + '&rowId=' + rowId;
							$(location).attr('href', url);
							return false;
						});

		//품질점검 결과등록
		$("#btn-edit4").button({
			icons : {
				primary : '${icon}'
			}
		})
				.click(
						function(event) {
							var page = $('#jqg-table').jqGrid('getGridParam',
									'page');
							var rowNum = $('#jqg-table').jqGrid('getGridParam',
									'rowNum');
							var rowId = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var code = $('#jqg-table').jqGrid('getGridParam',
									'selrow');
							var url = '<s:url value="/qam/qualchec/" />' + code
									+ '/index?page=' + page + '&rowNum='
									+ rowNum + '&rowId=' + rowId;
							$(location).attr('href', url);
							return false;
						});
	}

	//버튼세팅
	function setButton(edit1, edit2, edit3, edit4) {
		$('#btn-edit1').button('option', 'disabled', edit1);
		$('#btn-edit2').button('option', 'disabled', edit2);
		$('#btn-edit3').button('option', 'disabled', edit3);
		$('#btn-edit4').button('option', 'disabled', edit4);
	}

	//고객만족도
	function mailFormatter(cellvalue, options, rowObject) {
		var page = $('#jqg-table').jqGrid('getGridParam', 'page');
		var rowNum = $('#jqg-table').jqGrid('getGridParam', 'rowNum');
		var rowId = $('#jqg-table').jqGrid('getGridParam', 'selrow');
		var url = "<c:url value='/qam/custeval/"+cellvalue+"'/>";
		return '<a href="'+url+'" style="text-decoration:none" ><font color="blue">'
				+ '메일' + '</font></a>';
	}

	//품질관리 목록 그리드 조회
	function initJqGrid() {
		var mailhidden = true;
		<c:if test="${isAuthorized}">
		mailhidden = false;
		</c:if>

		//console.log("mailhidden : " + mailhidden);

		$("#jqg-table").jqGrid(
				{
					url : '<s:url value="/rest/qam/qualmng" />',
					datatype : 'json',
					mtype : 'POST',
					//datatype : 'local',
					colNames : [ '프로젝트코드', '프로젝트명', '시작일', 'F/U종료일', '착수',
							'분석', '설계', '코드', '테스트', '이행', '진척도', '산출물', '만족도',
							'만족도설문' ],
					colModel : [ {
						name : 'projectCode',
						key : true,
						width : 50,
						align : 'center'
					}, {
						name : 'projectName',
						width : 150
					}, {
						name : 'projectStartDate',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'projectFollowUpFinishDate',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG0',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG1',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG2',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG3',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG4',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressG5',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'progressRate',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'productPoint',
						align : 'center',
						width : 60,
						search : false,
						formatter : roundFormat2
					}, {
						name : 'satisfactionPoint',
						align : 'center',
						width : 60,
						search : false
					}, {
						name : 'projectCode',
						align : 'center',
						width : 60,
						search : false,
						hidden : mailhidden,
						formatter : mailFormatter
					} ],
					page : '${page}',
					rowNum : '${rowNum}',
					pager : '#jqg-pager',
					sortname : 'code',
					sortorder : 'desc',
					height : "100%",
					caption : '프로젝트 목록',
					serializeGridData : function(postData) {
						postData.projectName = $('#projectName').val();
						return postData;
					},
					onSelectRow : function(id) {
						setButton(false, false, false, false);
					},
					loadComplete : function() {
						//데이터가 존재하는 경우
						if ($(this).jqGrid('getGridParam', 'records') > 0) {
							var rowId = '${rowId}';
							if (!rowId) {
								rowId = $(this).jqGrid('getDataIDs')[0];
							}
							$(this).jqGrid('setSelection', rowId);
							setButton(false, false, false, false);
						} else {
							setButton(true, true, true, true);
						}
					},
					ondblClickRow : function() {
						if (eval('${navGrid.view}')) {
							$('#view_jqg-table').click();
						}
					}
				});
	}

	//그리드 버튼 처리 
	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			edit : false,
			add : false,
			del : false,
			view : true
		}, {}, {}, {}, {}, {
			beforeInitData : function(formId) {
				return beforeInitDataView(formId, this);
			}
		});

		//헤더 colspan, rowspan 처리
		$("#jqg-table").jqGrid("setGroupHeaders", {
			useColSpanStyle : true,
			groupHeaders : [ {
				//시작위치(컬럼명)
				startColumnName : "project.startDate",
				//병합개수
				numberOfColumns : 2,
				//title
				titleText : "프로젝트기간"
			}, {
				//시작위치(컬럼명)
				startColumnName : "progressG0",
				//병합개수
				numberOfColumns : 6,
				//title
				titleText : "진척도 측정"
			} ]
		});
	}

	function beforeInitDataView(formId, scope) {
		if (eval('${navGrid.view}')) {
			var page = $(scope).jqGrid('getGridParam', 'page');
			var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
			var rowId = $(scope).jqGrid('getGridParam', 'selrow');
			var url = '<s:url value="/qam/qualmng/" />' + rowId + '/view?page='
					+ page + '&rowNum=' + rowNum + '&rowId=' + rowId;
			$(location).attr('href', url);
			return false;
		} else {
			infomation('조회 할 수 없습니다.');
			return false;
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 품질관리</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-3">
			<form class="pure-form">
				<input id="projectName" name="projectName" type="text"
					class="pure-input-rounded" placeholder="프로젝트 코드, 명" size="30">
				<a id="btn-search">조회</a>
			</form>
		</div>
		<div class="pure-u-2-3">
			<div class="qms-content-buttonset">
				<a id="btn-edit1">진척도 평가 ${label}</a> <a id="btn-edit2">산출물 평가
					${label}</a> <a id="btn-edit3">만족도 평가 ${label}</a> <a id="btn-edit4">품질점검
					결과 ${label}</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />