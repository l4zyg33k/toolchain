<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<sec:authorize ifAnyGranted="ROLE_ADMIN, ROLE_QA" var="isAuthorized" />
<c:choose>
	<c:when test="${isAuthorized}">
		<c:set value="등록" var="label" />
	</c:when>
	<c:otherwise>
		<c:set value="조회" var="label" />
	</c:otherwise>
</c:choose>
<script type="text/javascript">
	$(function() {
		initButton();
		initJqGrid();
		initNavGrid();
	});

	//버튼 처리
	function initButton() {
		//취소버튼
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${progEvalFrmVO.getPrevUrl()}" />');

		//수정
		setEditButton('#btn-edit', '${not isAuthorized}', '#edit_jqg-table');
	}

	//진척도 평가 그리드 조회
	function initJqGrid() {
		$("#jqg-table").jqGrid({
			url : '<s:url value="/rest/qam/progeval" />',
			editurl : '<s:url value="/rest/qam/progeval/edit" />',
			colModel : [ {
				name : 'id',
				align : 'center',
				hidden : true,
				key : true
			}, {
				label : '프로젝트단계',
				name : 'step',
				align : 'center',
				width : '80',
				formatter : 'select',
                editable : true,				
				edittype : 'select',
				editoptions : {
					disabled : true,
					value : '${steps}'
				}
			}, {
				label : '측정 예정일',
				name : 'planDate',
				align : 'center',
				editable : true,
				width : '100',
				editrules : {
					required : true,
					date : true
				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				}
			}, {
				label : '실제 측정일',
				name : 'actualDate',
				align : 'center',
				editable : true,
				width : '100',
				editrules : {
					date : true
				},
				editoptions : {
					dataInit : function(e) {
						$(e).datepicker();
						$(e).mask('9999-99-99');
					}
				}
			}, {
				label : '계획 진척률',
				name : 'planRate',
				align : 'right',
				editable : true,
				width : '100',
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).numeric()
					}
				}
			}, {
				label : '실제 진척률',
				name : 'actualRate',
				align : 'right',
				editable : true,
				width : '100',
				editrules : {
					required : true,
					number : true,
					minValue : 0
				},
				editoptions : {
					defaultValue : '0',
					dataInit : function(elem) {
						$(elem).numeric()
					}
				}
			}, {
				label : '사유',
				name : 'reason',
				align : 'left',
				editable : true,
				width : '600',
				edittype : 'textarea',
				editoptions : {
					rows : "3",
					cols : "25"
				}
			} ],
			page : 1,
			rowNum : 10,
			pager : '#jqg-pager',
			sortname : 'step',
			sortorder : 'asc',
			height : "100%",
			caption : '진척도',
			serializeGridData : function(postData) {
				postData.projectCode = $('#code').val();
				return postData;
			},
			loadComplete : function() {
				setSelectionJqGrid('${rowId}', this);

				//계획대비 진척률 계산
				getProgressPerPlan();
			}
		});
	}

	//그리드 버튼 처리 
	function initNavGrid() {
		var editButton = false;
		<c:if test="${isAuthorized}">
		editButton = true;
		</c:if>

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			edit : editButton,
			add : false,
			del : false,
			view : false,
			search : false
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {}, {}, {}, {});
	}

	//수정
	function beforeInitDataEdit(formId, scope) {
		jQuery("#jqg-table").editGridRow(
				jQuery('#jqg-table').jqGrid('getGridParam', 'selrow'), {
					recreateForm : true,
					closeAfterEdit : true,
					closeAfterAdd : true
				});
		return true;
	}

	//계획대비 진척률 계산		
	function getProgressPerPlan() {
		$
				.ajax({
					type : 'GET',
					url : '<s:url value="/rest/qam/progeval/plan/${progEvalFrmVO.code}"/>',
					success : function(data) {
						//결과값 매핑
						$("#progressPerPlan").val(data);
					}
				});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">진척도 평가 결과 ${label}</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<button id="btn-cancel">취소</button>
			</div>
		</div>
	</div>
	<form:form id="frm-project" modelAttribute="progEvalFrmVO"
		cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form:label path="code" class="pure-u-1-5">프로젝트 코드</form:label>
					<form:input path="code" class="pure-u-4-6" readOnly="readOnly" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="name" class="pure-u-1-6">프로젝트 명</form:label>
					<form:input path="name" class="pure-u-3-5" readOnly="readOnly" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>진척도 평가</legend>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-edit">수정</a>
					</div>
				</div>
			</div>
		</fieldset>

		<div class="pure-u-1">
			<form id="query-form" class="pure-form">
				<div class="qms-grid-top-notify">
					<form:label path="progressPerPlan">계획대비 진척률</form:label>
					<form:input path="progressPerPlan" class="pure-input-rounded"
						readOnly="readOnly" />
				</div>
			</form>
		</div>
		<table id="jqg-table"></table>
		<div id="jqg-pager"></div>
	</form:form>
</div>
<br />