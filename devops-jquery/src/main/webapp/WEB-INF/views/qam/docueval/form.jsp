<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<sec:authorize ifAnyGranted="ROLE_ADMIN, ROLE_QA" var="isAuthorized" />
<c:choose>
	<c:when test="${isAuthorized}">
		<c:set value="등록" var="label" />
	</c:when>
	<c:otherwise>
		<c:set value="조회" var="label" />
	</c:otherwise>
</c:choose>
<script type="text/javascript">
	$(function() {
		initButton();
		initJqGrid();
		initNavGrid();
	});

	//버튼 처리
	function initButton() {
		//취소버튼
		setCancleFormButton('#btn-cancel', false,
				'<s:url value="${docuEvalFrmVO.getPrevUrl()}" />');

		//등록
		$("#btn-add").button({
			icons : {
				primary : 'ui-icon-plus'
			},
			disabled : '${not isAuthorized}'
		}).click(function(event) {
			$("#jqg-table").jqGrid('editGridRow', 'new', {
				recreateForm : true,
				closeAfterAdd : true,
				closeAfterEdit : true
			});
		});

		//수정
		$("#btn-edit").button({
			icons : {
				primary : 'ui-icon-pencil'
			},
			disabled : '${not isAuthorized}'
		}).click(
				function(event) {
					jQuery("#jqg-table").editGridRow(
							jQuery('#jqg-table').jqGrid('getGridParam',
									'selrow'), {
								recreateForm : true,
								closeAfterEdit : true,
								closeAfterAdd : true
							});
				});

		//삭제
		setDelButton('#btn-del', '${not isAuthorized}', '#del_jqg-table');
	}

	//산출물 측정결과  그리드 조회
	function initJqGrid() {
		$("#jqg-table").jqGrid(
				{
					url : '<s:url value="/rest/qam/docueval" />',
					editurl : '<s:url value="/rest/qam/docueval/edit" />',
					datatype : 'json',
					mtype : 'POST',
					//datatype : 'local',
					colNames : [ '일련번호', '프로젝트코드', '문서명', '가중치', '평점' ],
					colModel : [ {
						name : 'id',
						align : 'center',
						search : false,
						key : true,
						readonly : true,
						hidden : true,
						width : '100'
					}, {
						name : 'projectCode',
						align : 'center',
						search : false,
						hidden : true,
						editable : true,
						edittype : 'text',
						editoptions : {
							dataInit : function(e) {
								$(e).val($('#code').val());
							},
							readonly : true
						}
					}, {
						name : 'name',
						align : 'left',
						search : false,
						editable : true,
						editrules : {
							required : true
						},
						edittype : 'text'
					}, {
						name : 'weight',
						align : 'right',
						search : false,
						editable : true,
						editrules : {
							required : true,
							number : true
						},
						width : '100',
						editoptions : {
							dataInit : function(e) {
								$(e).mask('9?99');
							}
						},
						edittype : 'text'
					}, {
						name : 'point',
						align : 'right',
						search : false,
						editable : true,
						width : '100',
						editrules : {
							required : true,
							number : true
						},
						editoptions : {
							dataInit : function(e) {
								$(e).mask('9?99');
							}
						},
						edittype : 'text'
					} ],
					pager : '#jqg-pager',
					rowNum : 10,
					sortname : 'id',
					sortorder : 'asc',
					height : "100%",
					caption : '산출물 측정 결과',
					serializeGridData : function(postData) {
						postData.projectCode = $('#code').val();
						return postData;
					},
					loadComplete : function() {
						setSelectionJqGrid('${rowId}', this);
					},
					gridComplete : function() {
						//종합만족도 계산
						var cnt = 0;
						var sum = 0;
						var rowIds = $('#jqg-table').jqGrid('getDataIDs');
						for (var i = 0, len = rowIds.length; i < len; i++) {
							var tCnt = Number($('#jqg-table').getCell(
									rowIds[i], "weight"));
							var point = Number($('#jqg-table').getCell(
									rowIds[i], "point"));
							cnt += tCnt;
							sum += tCnt * point;
						}
						if (cnt == 0) {
							$('#summaryPoint').val(0);
						} else {
							$('#summaryPoint').val(
									Math.round(sum / cnt * 100) / 100);
						}
					}
				});
	}

	//그리드 버튼 처리 
	function initNavGrid() {
		var editButton = false;
		<c:if test="${isAuthorized}">
		editButton = true;
		</c:if>

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
			edit : editButton,
			add : editButton,
			del : editButton,
			view : false,
			search : false
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataEdit(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataAdd(formId, this);
			}
		}, {
			beforeInitData : function(formId) {
				return beforeInitDataDel(formId, this);
			}
		}, {}, {});
	}

	//수정
	function beforeInitDataEdit(formId, scope) {
		jQuery("#jqg-table").editGridRow(
				jQuery('#jqg-table').jqGrid('getGridParam', 'selrow'), {
					recreateForm : true,
					closeAfterEdit : true,
					closeAfterAdd : true
				});
		return true;
	}

	//등록
	function beforeInitDataAdd(formId, scope) {
		$("#jqg-table").jqGrid('editGridRow', 'new', {
			recreateForm : true,
			closeAfterAdd : true,
			closeAfterEdit : true
		});
		return true;
	}

	//삭제
	function beforeInitDataDel(formId, scope) {
		return true;
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">산출물 측정결과 ${label}</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-cancel">취소</a>
			</div>
		</div>
	</div>
	<form:form id="frm-project" modelAttribute="docuEvalFrmVO"
		cssClass="pure-form pure-form-aligned">
		<fieldset>
			<legend>프로젝트 정보</legend>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form:label path="code" class="pure-u-1-5">프로젝트 코드</form:label>
					<form:input path="code" class="pure-u-4-6" readOnly="readOnly" />
				</div>
				<div class="pure-u-1-2">
					<form:label path="name" class="pure-u-1-6">프로젝트 명</form:label>
					<form:input path="name" class="pure-u-3-5" readOnly="readOnly" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>산출물 평가</legend>
			<div class="pure-u-1">
				<form id="query-form" class="pure-form">
					<div class="qms-grid-top-notify">
						<label>종합평점</label> <input id="summaryPoint" type="text"
							class="pure-input-rounded" readOnly>
					</div>
				</form>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<form class="pure-form"></form>
				</div>
				<div class="pure-u-1-2">
					<div class="qms-content-buttonset">
						<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
					</div>
				</div>
			</div>
		</fieldset>

		<table id="jqg-table"></table>
		<div id="jqg-pager"></div>
	</form:form>

</div>
<br />