<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<script type="text/javascript">
	$(function() {
		//버튼 처리
		initButton();
		//그리드 조회
	    initJqGrid1();
	    initJqGrid2();
	    initJqGrid3();
	    
		//사용자유형콤보 변경시 설문유형설정
		$('#userType').change(function() {
			setChildSelectBox2($('#userType'), $('#surveyType'));
		});
		
		//설문유형변경시 조회 처리
		$('#surveyType').change(function() {
			//그리드 초기화
			$("#jqg-table").jqGrid("clearGridData");
			$("#jqg-table2").jqGrid("clearGridData");
			$("#jqg-table3").jqGrid("clearGridData");
			
			//1번유형 그리드 초기화
			$("#jqg-table").setGridParam({
			}).trigger("reloadGrid");
			
			//2번유형 그리드 초기화
			$("#jqg-table2").setGridParam({
			}).trigger("reloadGrid");
			
			//3번유형 그리드 초기화
			$("#jqg-table3").setGridParam({
			}).trigger("reloadGrid");
		});		
	});
	
    //버튼 처리
    function initButton(){
    	//1번문항 관리버튼
    	setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
    	setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
    	setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');

		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid1", true, true, true);  
		
		//2번문항 관리버튼
		setAddButton('#btn-add2', eval('${!navGrid.add}'), '#add_jqg-table2');
    	setEditButton('#btn-edit2', eval('${!navGrid.edit}'), '#edit_jqg-table2');
    	setDelButton('#btn-del2', eval('${!navGrid.del}'), '#del_jqg-table2');

		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid2", true, true, true);	
		
		//3번문항 관리버튼
		setAddButton('#btn-add3', eval('${!navGrid.add}'), '#add_jqg-table3');
    	setEditButton('#btn-edit3', eval('${!navGrid.edit}'), '#edit_jqg-table3');
    	setDelButton('#btn-del3', eval('${!navGrid.del}'), '#del_jqg-table3');
			
		//버튼 disabled 처리(grid, add, edit, del)
		setButtonDisable("grid3", true,true,true);		
    }
    
	//그리드 조회
	function initJqGrid1() {	
		
		$("#jqg-table")
		.jqGrid(
			{
				url : '<s:url value="/rest/qam/survques"/>',
				editurl : '<s:url value="/rest/qam/survques/edit"/>',
				datatype : 'json',
				mtype : 'POST',
				//datatype : 'local',
				colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호', '문항', '키워드' ],
				colModel : [ {
					name : 'id',
					hidden : true,
					key : true
				}, {
					name : 'questionType',
					align : 'center',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).val("T210");
						},									
						readonly : true
					}
				}, {
					name : 'userType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var uType = $("#userType").val();
							if(0 == uType){
								uType = "";
							}
							$(e).val(uType);
						},
						readonly : true
					}
				}, {
					name : 'surveyType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var sType = $("#surveyType").val();
							if(0 == sType){
								sType = "";
							}
							$(e).val(sType);
						},
						editrules: {required: true},
						readonly : true
					}
				}, {
					name : 'no',
					align : 'center',
					width : 90,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).mask('9');
						}
					}
				}, {
					name : 'question',
					align : 'left',
					width : 400,
					editable : true,
					edittype : 'text',
					editrules: {required: true}
				}, {
					name : 'keyword',
					align : 'left',
					width : 110,
					editable : true,
					edittype : 'text',
					editrules: {required: true}
				} ],
				pager : '#jqg-pager',
				rowNum : 10,
				sortname : 'no',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				autoencode : true,
				height : "100%",
				caption : '1번 문항',
				serializeGridData : function(postData) {
					postData.code = "T210";
					postData.userType = $("#userType").val();
					postData.surveyType = $("#surveyType").val();
					return postData;
				},
				loadComplete : function() {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid1", false,true,true);
					return false;
				},
				onSelectRow : function(id) {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid1", false,false,false);
					return false;
				}
			});	
		
		$("#jqg-table").jqGrid('navGrid','#jqg-pager'
			,{/* options */
				add:true,edit:true,view:false,del:true,search:true,refresh:true
			 } 													
			,{/* Edit options */
				recreateForm : true,
				closeAfterEdit : true
			 } 		
			,{/* Add options */
				recreateForm : true,
				closeAfterAdd : true
			}
			,{/* Delete options */
				recreateForm : true
			} 							
			,{/* Search options */
				closeOnEscape:true,
				onSearch:function(){									
				}
			}
			,{/* view parameters*/
			} 
	   	);
	}
	
	//그리드 조회
	function initJqGrid2() {
		$("#jqg-table2")
		.jqGrid(
			{
				url : '<s:url value="/rest/qam/survques"/>',
				editurl : '<s:url value="/rest/qam/survques/edit"/>',
				datatype : 'json',
				mtype : 'POST',
				//datatype : 'local',
				colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호', '문항', '키워드' ],
				colModel : [ {
					name : 'id',
					hidden : true,
					key : true
				}, {
					name : 'questionType',
					align : 'center',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).val("T220");
						},									
						readonly : true
					}
				}, {
					name : 'userType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var uType = $("#userType").val();
							if(0 == uType){
								uType = "";
							}
							$(e).val(uType);
						},
						readonly : true
					}
				}, {
					name : 'surveyType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var sType = $("#surveyType").val();
							if(0 == sType){
								sType = "";
							}
							$(e).val(sType);
						},
						editrules: {required: true},
						readonly : true
					}
				}, {
					name : 'no',
					align : 'center',
					width : 90,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).mask('9');
						}
					}
				}, {
					name : 'question',
					align : 'left',
					width : 400,
					editable : true,
					edittype : 'text',
					editrules: {required: true}
				}, {
					name : 'keyword',
					align : 'left',
					width : 110,
					editable : true,
					edittype : 'text'
				} ],
				pager : '#jqg-pager2',
				rowNum : 10,
				sortname : 'no',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				autoencode : true,
				height : "100%",
				caption : '2번 문항',
				serializeGridData : function(postData) {
					postData.code = "T220";
					postData.userType = $("#userType").val();
					postData.surveyType = $("#surveyType").val();
					return postData;
				},
				loadComplete : function() {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid2", false,true,true);
					return false;
				},
				onSelectRow : function(id) {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid2", false,false,false);
					return false;
				}
			});
		
		$("#jqg-table2").jqGrid('navGrid','#jqg-pager2'
				,{/* options */
					add:true,edit:true,view:false,del:true,search:true,refresh:true
				 } 													
				,{/* Edit options */
					recreateForm : true,
					closeAfterEdit : true
				 } 		
				,{/* Add options */
					recreateForm : true,
					closeAfterAdd : true
				}
				,{/* Delete options */
					recreateForm : true
				} 							
				,{/* Search options */
					closeOnEscape:true,
					onSearch:function(){									
					}
				}
				,{/* view parameters*/
				} 
		   	);		
	}
	
	//그리드 조회
	function initJqGrid3() {
		$("#jqg-table3")
		.jqGrid(
			{
				url : '<s:url value="/rest/qam/survques"/>',
				editurl : '<s:url value="/rest/qam/survques/edit"/>',
				datatype : 'json',
				mtype : 'POST',
				//datatype : 'local',
				colNames : [ 'id', '문항유형', '사용자유형', '설문유형', '문항번호', '문항', '키워드' ],
				colModel : [ {
					name : 'id',
					hidden : true,
					key : true
				}, {
					name : 'questionType',
					align : 'center',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).val("T230");
						},									
						readonly : true
					}
				}, {
					name : 'userType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var uType = $("#userType").val();
							if(0 == uType){
								uType = "";
							}
							$(e).val(uType);
						},
						readonly : true
					}
				}, {
					name : 'surveyType',
					align : 'left',
					hidden : true,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							var sType = $("#surveyType").val();
							if(0 == sType){
								sType = "";
							}
							$(e).val(sType);
						},
						editrules: {required: true},
						readonly : true
					}
				}, {
					name : 'no',
					align : 'center',
					width : 90,
					editable : true,
					edittype : 'text',
					editrules: {required: true},
					editoptions : {
						dataInit : function(e) {
							$(e).mask('9');
						}
					}
				}, {
					name : 'question',
					align : 'left',
					width : 510,
					editable : true,
					edittype : 'text',
					editrules: {required: true}
				}, {
					name : 'keyword',
					align : 'left',
					editable : true,
					hidden : true,
					edittype : 'text'
				} ],
				pager : '#jqg-pager3',
				rowNum : 10,
				sortname : 'no',
				sortorder : 'asc',
				viewrecords : true,
				gridview : true,
				autoencode : true,
				height : "100%",
				caption : '3번 문항',
				serializeGridData : function(postData) {
					postData.code = "T230";
					postData.userType = $("#userType").val();
					postData.surveyType = $("#surveyType").val();
					return postData;
				},
				loadComplete : function() {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid3", false,true,true);
					return false;
				},
				onSelectRow : function(id) {
					//버튼 disabled 처리(grid, add, edit, del)
					setButtonDisable("grid3", false,false,false);
					return false;
				}
			});	

		$("#jqg-table3").jqGrid('navGrid','#jqg-pager3'
				,{/* options */
					add:true,edit:true,view:false,del:true,search:true,refresh:true
				 } 													
				,{/* Edit options */
					recreateForm : true,
					closeAfterEdit : true
				 } 		
				,{/* Add options */
					recreateForm : true,
					closeAfterAdd : true
				}
				,{/* Delete options */
					recreateForm : true
				} 							
				,{/* Search options */
					closeOnEscape:true,
					onSearch:function(){									
					}
				}
				,{/* view parameters*/
				} 
		   	);		
	}
	
	/* 그리드 컬럼 히든 처리*/		
	function setGridColHidden(gridObject){
		$('#tr_questionType', gridObject).hide();
		$('#tr_userType', gridObject).hide();
		$('#tr_surveyType', gridObject).hide();
	}
	
	/* 그리드에 따른 버튼 disabled 처리*/
	function setButtonDisable(buttonIndex,add,edit,del){
		if (buttonIndex == "grid1"){
			$("#btn-add").button("option", "disabled",add);
			$("#btn-edit").button("option", "disabled",edit);
			$("#btn-del").button("option", "disabled", del);
		}else if (buttonIndex == "grid2"){
			$("#btn-add2").button("option", "disabled",add);
			$("#btn-edit2").button("option", "disabled",edit);
			$("#btn-del2").button("option", "disabled", del);
		}else if (buttonIndex == "grid3"){
			$("#btn-add3").button("option", "disabled",add);
			$("#btn-edit3").button("option", "disabled",edit);
			$("#btn-del3").button("option", "disabled", del);
		}
	}
	
	//대상자유형에따른 설문유형설정		
	function setChildSelectBox2(pObject, cObject) {
		var id = pObject.val();
		if(id==275){ //사업부
			cObject.find("option").remove();
			<c:forEach var="option" items="${divisions}">
				cObject.append($('<option>').text('${option.value}').attr('value', '${option.key}'));
			</c:forEach>
		}else if(id==276){ //현업사용자
			cObject.find("option").remove();
			<c:forEach var="option" items="${actualWork}">
				cObject.append($('<option>').text('${option.value}').attr('value', '${option.key}'));
			</c:forEach>
		}else if(id==277){ //운영자
			cObject.find("option").remove();
			<c:forEach var="option" items="${operator}">
				cObject.append($('<option>').text('${option.value}').attr('value', '${option.key}'));
			</c:forEach>
		}
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">만족도설문문항관리</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form">
				<form:select id="userType" path="userType" items="${userType}" />
				<form:select id="surveyType" path="surveyType" items="${surveyType}" />
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
	
	<!-- 2번문항시작 -->
	<br/>
	<div class="pure-g">
		<div class="pure-u-1-2">
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add2">등록</a> <a id="btn-edit2">수정</a> <a id="btn-del2">삭제</a>
			</div>
		</div>
	</div>
	<br/>
	<table id="jqg-table2"></table>
	<div id="jqg-pager2"></div>
	<!-- 2번문항 종료-->
	
	<!-- 3번문항시작 -->
	<br/>
	<div class="pure-g">
		<div class="pure-u-1-2">
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add3">등록</a> <a id="btn-edit3">수정</a> <a id="btn-del3">삭제</a>
			</div>
		</div>
	</div>
	<br/>
	<table id="jqg-table3"></table>
	<div id="jqg-pager3"></div>
	<!-- 3번문항 종료-->
</div>
<br/>