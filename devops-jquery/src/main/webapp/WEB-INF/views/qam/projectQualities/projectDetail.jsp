<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		tinymce.init({
			selector : 'textarea',
			readonly : 1
		});
		$(':input').attr("readonly","true");
		$('#recommandation').attr("readonly","true");
		/*
		 * 수정 버튼
		 */
		$("#btn-cancle").button({
		   icons: {
		       primary: 'ui-icon-arrowreturn-1-w'
		   }
		}).click(function(event) {
            var url = "<s:url value = '${returnUrl}' />";
            get(url);
        });

        $('#panelActualTime').mask('99:99');
		$('#startDate').mask('9999-99-99');
		$('#finishDate').mask('9999-99-99');
		$('#actualStartDate').mask('9999-99-99');
		$('#actualFinishDate').mask('9999-99-99');
		$('#followUpStartDate').mask('9999-99-99');
		$('#followUpFinishDate').mask('9999-99-99');
		$('#followUpActualStartDate').mask('9999-99-99');
		$('#followUpActualFinishDate').mask('9999-99-99');
		$('#closeReportDate').mask('9999-99-99');
		$('#openCheckDate').mask('9999-99-99');
		$('#openReportDate').mask('9999-99-99');

	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 상세</span>
	</div>
</div>
<div class="qms-content-main">
	<s:url value="/pms/projects" var="action" />
	<c:set value="POST" var="method" />
	<form:form id="frm-project" action="${action}" method="${method}"
		modelAttribute="projectVO" cssClass="pure-form pure-form-stacked">
		<fieldset>
            <legend>프로젝트 개요</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="code">코드</form:label>
                    <form:input path="code" readonly="true" />
                    <form:errors path="code" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-3-4">
                    <form:label path="name">이름</form:label>
                    <form:input path="name" cssClass="pure-input-2-3" />
                    <form:errors path="name" cssClass="ui-state-error-text" />
                </div>
            </div>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="office">추진 사업부</form:label>
                        <form:hidden path="office.id" />
						<form:input path="office.name" readonly="true" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="blDepartment">추진 부서</form:label>
                    <form:input path="blDepartment" />
                    <form:errors path="blDepartment" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="pmName">프로젝트 관리자</form:label>
                    <form:input path="pmName" placeholder="이름을 입력하세요." />
                    <form:errors path="pmName" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="blName">비지니스 리더</form:label>
                    <form:input path="blName" placeholder="이름을 입력하세요." />
                    <form:errors path="blName" cssClass="ui-state-error-text" />
                </div>
            </div>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="type.name">프로젝트 유형</form:label>
                    <form:input path="type.name" />
                    <form:errors path="type.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:hidden path="il.username" />
                    <form:label path="il.name">정보기술 리더</form:label>
                    <form:input path="il.name" placeholder="이름을 입력하세요." />
                    <form:errors path="il.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:hidden path="op.username" />
                    <form:label path="op.name">운영 담당자</form:label>
                    <form:input path="op.name" placeholder="이름을 입력하세요." />
                    <form:errors path="op.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:hidden path="qa.username" />
                    <form:label path="qa.name">품질 담당자</form:label>
                    <form:input path="qa.name" placeholder="이름을 입력하세요." />
                    <form:errors path="qa.name" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>패널회의</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="panelStartDate">준비 시작일자</form:label>
                    <form:input path="panelStartDate" placeholder="일자를 선택하세요." />
                    <form:errors path="panelStartDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="panelFinishDate">준비 종료일자</form:label>
                    <form:input path="panelFinishDate" placeholder="일자를 선택하세요." />
                    <form:errors path="panelFinishDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="panelActualDate">회의일자</form:label>
                    <form:input path="panelActualDate" placeholder="일자를 선택하세요." />
                    <form:errors path="panelActualDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="panelActualTime">회의시각</form:label>
                    <form:input path="panelActualTime" placeholder="시간을 선택하세요." />
                    <form:errors path="panelActualTime" cssClass="ui-state-error-text" />
                </div>
            </div>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="location.name">회의장소</form:label>
                    <form:input path="location.name" />
                    <form:errors path="location.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="pass.name">통과여부</form:label>
                    <form:input path="pass.name"/>
                    <form:errors path="pass.name" cssClass="ui-state-error-text" />
                </div>
            </div>
            <div class="pure-g">
                <div class="pure-u-1">
                    <form:label path="recommandation">권고사항</form:label>
                    <form:textarea path="recommandation" readonly="true" cssClass="pure-input-1" />
                    <form:errors path="recommandation" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>프로젝트 기간</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="startDate">시작일자</form:label>
                    <form:input path="startDate" placeholder="일자를 선택하세요." />
                    <form:errors path="startDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="finishDate">종료일자</form:label>
                    <form:input path="finishDate" placeholder="일자를 선택하세요." />
                    <form:errors path="finishDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="actualStartDate">실제 시작일자</form:label>
                    <form:input path="actualStartDate" placeholder="일자를 선택하세요." />
                    <form:errors path="actualStartDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="actualFinishDate">실제 종료일자</form:label>
                    <form:input path="actualFinishDate" placeholder="일자를 선택하세요." />
                    <form:errors path="actualFinishDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="followUpStartDate">인수인계 시작일자</form:label>
                    <form:input path="followUpStartDate" placeholder="일자를 선택하세요." />
                    <form:errors path="followUpStartDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="followUpFinishDate">인수인계 종료일자</form:label>
                    <form:input path="followUpFinishDate" placeholder="일자를 선택하세요." />
                    <form:errors path="followUpFinishDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="followUpActualStartDate">실제 인수인계 시작일자</form:label>
                    <form:input path="followUpActualStartDate" placeholder="일자를 선택하세요." />
                    <form:errors path="followUpActualStartDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="followUpActualFinishDate">실제 인수인계 종료일자</form:label>
                    <form:input path="followUpActualFinishDate" placeholder="일자를 선택하세요." />
                    <form:errors path="followUpActualFinishDate" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>프로젝트 투입 계획</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="pdEffort">총 M/M</form:label>
                    <form:input path="pdEffort" />
                    <form:errors path="pdEffort" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="workingDay">1달 근무기준 일수</form:label>
                    <form:input path="workingDay" />
                    <form:errors path="workingDay" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="workingHour">1일 근무기준 시간</form:label>
                    <form:input path="workingHour" />
                    <form:errors path="workingHour" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="workingEffort">총 M/H</form:label>
                    <form:input path="workingEffort" />
                    <form:errors path="workingEffort" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>종료보고 현황</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="closeReportDate">종료보고 일자</form:label>
                    <form:input path="closeReportDate" placeholder="일자를 선택하세요." />
                    <form:errors path="closeReportDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="closeReportType.name">종료보고 형식</form:label>
                    <form:input path="closeReportType.name"/>
                    <form:errors path="closeReportType.name" cssClass="ui-state-error-text" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>개발진행 현황</legend>
            <div class="pure-g">
                <div class="pure-u-1-4">
                    <form:label path="openCheckDate">오픈점검 일자</form:label>
                    <form:input path="openCheckDate" placeholder="일자를 선택하세요." />
                    <form:errors path="openCheckDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="openReportDate">운영보고 일자</form:label>
                    <form:input path="openReportDate" placeholder="일자를 선택하세요." />
                    <form:errors path="openReportDate" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="phase.name">진행단계</form:label>
                    <form:input path="phase.name"/>
                    <form:errors path="phase.name" cssClass="ui-state-error-text" />
                </div>
                <div class="pure-u-1-4">
                    <form:label path="watching">현황판 표시</form:label>
                    <form:checkbox path="watching" />
                </div>
            </div>
        </fieldset>
	</form:form>
	<br/>
	<div class="pure-g">
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-cancle">닫기</a>
			</div>
		</div>
		<div class="pure-u-1-2">
			
		</div>
	</div>
</div>
<!-- 
<div class="qms-content-hidden">
	<span id="header-menu">품질현황 관리</span>
	<span id="sidebar-menu">프로젝트 품질관리</span>
</div>
 -->
<br />