<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		//버튼 처리
		initButton();
		
		//자동완성
		initAutoComplete();
		
		//그리드 조회
	    initJqGrid1();
	    initJqGrid2();
	    initJqGrid3();
	    initJqGrid4();
	    initJqGrid5();
	    initJqGrid6();
		
		$("#btn-expired").button("option", "disabled", true);
		
		$( "#tabs" ).tabs();
	});
	
    //버튼 처리
    function initButton(){
		//조회버튼
		$("#btn-search").button({
			icons : {
				primary : 'ui-icon-search'
			}
		}).click(function(event) {
			//그리드 재조회
			reloadGrid();
		}); 
		
		//엑셀저장
		$("#btn-excel")
		.button({
			icons : {
				primary : 'ui-icon-disk'
			}/**/
		}).click(function() {
			var i, record;
			var jsonData1 = "[";
			var excelGridData1 = $('#jqg-table').jqGrid('getRowData');
			if(excelGridData1.length > 0){
				for (row in excelGridData1){
					i = 1;
					record = excelGridData1[row];
					for (cell in record)
					{
						if(i==1){
							jsonData1 += '{"프로젝트명":"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData1 += '{"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData1 += ',"응답속도":"' + record[cell] +'"';	
						}
						if(i==4){
							jsonData1 += ',"요구사항":"' + record[cell] +'"';	
						}
						if(i==5){
							jsonData1 += ',"출력양식":"' + record[cell] +'"';	
						}
						if(i==6){
							jsonData1 += ',"자료정확":"' + record[cell] +'"';	
						}
						if(i==7){
							jsonData1 += ',"보안체계":"' + record[cell] +'"';		
						}
						if(i==8){
							jsonData1 += ',"일정준수":"' + record[cell] +'"';		
						}
						if(i==9){
							jsonData1 += ',"개발시스템.종합":"' + record[cell] +'"';		
						}
						if(i==10){
							jsonData1 += ',"교육자료":"' + record[cell] +'"';	
						}
						if(i==11){
							jsonData1 += ',"메뉴얼":"' + record[cell] +'"';
						}
						if(i==12){
							jsonData1 += ',"UI":"' + record[cell] + '"';	
						}
						if(i==13){
							jsonData1 += ',"호환성":"' + record[cell] +'"';	
						}
						if(i==14){
							jsonData1 += ',"시스템활용지원.종합":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData1 = jsonData1.substr(0, jsonData1.length-1)  + "]";
			}else{
				jsonData1 = '[{"프로젝트명":"","응답자":"","응답속도":"","요구사항":"","출력양식":'+
					        '"","자료정확":"","보안체계":"","일정준수":"","개발시스템.종합":"","교육자료":'+
					        '"","메뉴얼":"","UI":"","호환성":"","시스템활용지원.종합":""}]';
			}
			
			var jsonData2 = "[";
			var excelGridData2 = $('#jqg-table2').jqGrid('getRowData');
			if(excelGridData2.length > 0){
				for (row in excelGridData2){
					i = 1;
					record = excelGridData2[row];
					for (cell in record)
					{
						if(i==1){
							jsonData2 += '{"프로젝트명:"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData2 += ',"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData2 += ',"답변내용":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData2 = jsonData2.substr(0, jsonData2.length-1)  + "]";
			}else{
				jsonData2 = '[{"프로젝트명":"","응답자":"","답변내용":""}]';
			}
			
			var jsonData3 = "[";
			var excelGridData3 = $('#jqg-table3').jqGrid('getRowData');
			if(excelGridData3.length > 0){
				for (row in excelGridData3){
					i = 1;
					record = excelGridData3[row];
					for (cell in record)
					{
						if(i==1){
							jsonData3 += '{"프로젝트명":"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData3 += '{"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData3 += ',"응답속도":"' + record[cell] +'"';	
						}
						if(i==4){
							jsonData3 += ',"요구사항":"' + record[cell] +'"';	
						}
						if(i==5){
							jsonData3 += ',"자료정확":"' + record[cell] +'"';	
						}
						if(i==6){
							jsonData3 += ',"Sys.안정":"' + record[cell] +'"';	
						}
						if(i==7){
							jsonData3 += ',"사용편의":"' + record[cell] +'"';		
						}
						if(i==8){
							jsonData3 += ',"Sys.개선":"' + record[cell] +'"';		
						}
						if(i==9){
							jsonData3 += ',"개발시스템.종합":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData3 = jsonData3.substr(0, jsonData3.length-1)  + "]";
			}else{
				jsonData3 = '[{"프로젝트명":"","응답자":"","응답속도":"","요구사항":"","자료정확":'+
					        '"","Sys.안정":"","사용편의":"","Sys.개선":"","개발시스템.종합":""}]';
			}
			
			var jsonData4 = "[";
			var excelGridData4 = $('#jqg-table4').jqGrid('getRowData');
			if(excelGridData4.length > 0){
				for (row in excelGridData4){
					i = 1;
					record = excelGridData4[row];
					for (cell in record)
					{
						if(i==1){
							jsonData4 += '{"프로젝트명:"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData4 += ',"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData4 += ',"답변내용":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData4 = jsonData4.substr(0, jsonData4.length-1)  + "]";
			}else{
				jsonData4 = '[{"프로젝트명":"","응답자":"","답변내용":""}]';
			}

			var jsonData5 = "[";
			var excelGridData5 = $('#jqg-table5').jqGrid('getRowData');
			if(excelGridData5.length > 0){
				for (row in excelGridData5){
					i = 1;
					record = excelGridData5[row];
					for (cell in record)
					{
						if(i==1){
							jsonData5 += '{"프로젝트명":"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData5 += '{"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData5 += ',"문서완성":"' + record[cell] +'"';	
						}
						if(i==4){
							jsonData5 += ',"문서이해":"' + record[cell] +'"';	
						}
						if(i==5){
							jsonData5 += ',"문서일치":"' + record[cell] +'"';	
						}
						if(i==6){
							jsonData5 += ',"문서실질":"' + record[cell] +'"';	
						}
						if(i==7){
							jsonData5 += ',"개발문서.종합":"' + record[cell] +'"';		
						}
						if(i==8){
							jsonData5 += ',"Sys.구성":"' + record[cell] +'"';		
						}
						if(i==9){
							jsonData5 += ',"Sys.확장":"' + record[cell] +'"';		
						}
						if(i==10){
							jsonData5 += ',"보안체계":"' + record[cell] +'"';	
						}
						if(i==11){
							jsonData5 += ',"Sys.성능":"' + record[cell] +'"';
						}
						if(i==12){
							jsonData5 += ',"오류빈도":"' + record[cell] + '"';	
						}
						if(i==13){
							jsonData5 += ',"시스템구조및성능종합":"' + record[cell] +'"';	
						}
						if(i==14){
							jsonData5 += ',"운영편의":"' + record[cell] +'"';		
						}
						if(i==15){
							jsonData5 += ',"수정용이":"' + record[cell] +'"';		
						}
						if(i==16){
							jsonData5 += ',"표준준수":"' + record[cell] +'"';	
						}
						if(i==17){
							jsonData5 += ',"Sys.연동":"' + record[cell] +'"';
						}
						if(i==18){
							jsonData5 += ',"User관리":"' + record[cell] + '"';	
						}
						if(i==19){
							jsonData5 += ',"배치편리":"' + record[cell] +'"';	
						}
						if(i==20){
							jsonData5 += ',"시스템운영편리성.종합":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData5 = jsonData5.substr(0, jsonData5.length-1)  + "]";
			}else{
				jsonData5 = '[{"프로젝트명":"","응답자":"","문서완성":"","문서이해":"","문서일치":'+
					        '"","문서실질":"","개발문서.종합":"","Sys.구성":"","Sys.확장":"","보안체계":'+
					        '"","Sys.성능":"","오류빈도":"","시스템구조및성능종합":"","운영편의":"","수정용이":'+
					        '"","표준준수":"","Sys.연동":"","User관리":"","배치편리":"","시스템운영편리성.종합":""}]';
			}	             
			
			var jsonData6 = "[";
			var excelGridData6 = $('#jqg-table6').jqGrid('getRowData');
			if(excelGridData6.length > 0){
				for (row in excelGridData6){
					i = 1;
					record = excelGridData6[row];
					for (cell in record)
					{
						if(i==1){
							jsonData6 += '{"프로젝트명:"' + record[cell] +'"';	
						}
						if(i==2){
							jsonData6 += ',"응답자":"' + record[cell] +'"';	
						}
						if(i==3){
							jsonData6 += ',"답변내용":"' + record[cell] +'"},';	
						}
						i++;
					}
				}
				jsonData6 = jsonData6.substr(0, jsonData6.length-1)  + "]";
			}else{
				jsonData6 = '[{"프로젝트명":"","응답자":"","답변내용":""}]';
			}
			
			var jsonData = '[{"name":"사업부(객관식)","data":"'+ jsonData1.replace(/\"/g,"\\\"") + 
				           '"},{"name":"사업부(주관식)","data":"'+ jsonData2.replace(/\"/g,"\\\"") + 
				           '"},{"name":"현업(객관식)","data":"'+ jsonData3.replace(/\"/g,"\\\"") + 
				           '"},{"name":"현업(주관식)","data":"'+ jsonData4.replace(/\"/g,"\\\"") + 
				           '"},{"name":"시스템(객관식)","data":"'+ jsonData5.replace(/\"/g,"\\\"") + 
				           '"},{"name":"시스템(주관식)","data":"'+ jsonData6.replace(/\"/g,"\\\"") + 
				           '"}]';
			$('#excelForm #data').val(jsonData);
			$('#excelForm').submit();
		});
		
		//설문종료
		$("#btn-expired")
		.button({
			icons : {
				primary : 'ui-icon-trash'
			}
		})
		.click(
			function(event) {
				if($('#project').val()==""){
					return;
				} 

			$.post('<s:url value="/rest/qam/survresu/expire"/>', {
						projectCode : $('#project').val()
					}, function(data) {
						if (data.success) {
							alert("설문이 종료 되었습니다.");
							return;
						} else {
							alert(data.message);
							return;
						}
					});
		});
	}

	//자동완성
	function initAutoComplete() {
		$('#projectName').autocomplete({
			autoFocus : true,
			source : parseUrl('/rest/pms/projmng'),
			select : function(event, ui) {
				$('#project').val(ui.item.code);
				$('#projectName').val(ui.item.name);
				//그리드 재조회
				reloadGrid();
				//버튼 활성화
				$("#btn-expired").button("option", "disabled", false);
				return false;
			},
			change : function(event, ui) {
				if (ui.item == null) {
					$('#project').val(null);
					$('#projectName').val(null);
				}
			}
		}).data('ui-autocomplete')._renderItem = function(ul, item) {
			return $('<li>').append(
					'<a><b>' + item.code + '</b> ' + item.name + '</a>')
					.appendTo(ul);
		};
	}

	//그리드 조회 : 사업부 담장자 탭 - 객관식 설문
	function initJqGrid1() {
		$("#jqg-table").jqGrid(
				{
					url : '<s:url value="/rest/qam/survresu/multipleChoice"/>',
					datatype : 'json',
					mtype : 'POST',
					colNames : [ '프로젝트명', '응답자', '응답속도', '요구사항', '출력양식',
							'자료정확', '보안체계', '일정준수', '개발시스템.<br/>종합', '교육자료',
							'메뉴얼', 'UI', '호환성', '시스템활용지원.<br>종합' ],
					colModel : [ {
						name : 'project.name',
						width : '500'
					}, {
						name : 'username'
					}, {
						name : 't301',
						align : 'right',
						search : false
					}, {
						name : 't302',
						align : 'right',
						search : false
					}, {
						name : 't303',
						align : 'right',
						search : false
					}, {
						name : 't304',
						align : 'right',
						search : false
					}, {
						name : 't305',
						align : 'right',
						search : false
					}, {
						name : 't306',
						align : 'right',
						search : false
					}, {
						name : 't307',
						align : 'right',
						search : false
					}, {
						name : 't308',
						align : 'right',
						search : false
					}, {
						name : 't309',
						align : 'right',
						search : false
					}, {
						name : 't310',
						align : 'right',
						search : false
					}, {
						name : 't311',
						align : 'right',
						search : false
					}, {
						name : 't312',
						align : 'right',
						search : false
					} ],
					page : '${page}',
		       		rowNum : 20,
		       		pager : '#jqg-pager',
					sortname : 'project.code',
					sortorder : 'desc',
					height : "100%",
					caption : '프로젝트 공수 목록',
					serializeGridData : function(postData) {
						postData.userType = "T3";
						postData.projectCode = $("#project").val();
						return postData;
					},
					loadComplete : function() {
						return false;
					},
					onSelectRow : function(id) {
						return false;
					}
				});

		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 조회 : 사업부 담장자 탭 - 주관식 설문
	function initJqGrid2() {
		$("#jqg-table2").jqGrid({
			url : '<s:url value="/rest/qam/survresu/shortAnswer"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명', '응답자', '답변내용' ],
			colModel : [ {
				name : 'project.name',
				width : '70'
			}, {
				name : 'username',
				width : '20'
			}, {
				name : 'content',
				align : 'left'
			} ],
			page : '${page}',
			pager : '#jqg-pager2',
			rowNum : 20,
			sortname : 'project.code',
			sortorder : 'desc',
			height : "100%",
			caption : '프로젝트 공수 목록',
			serializeGridData : function(postData) {
				postData.userType = "T3";
				postData.projectCode = $("#project").val();
				return postData;
			}
		});

		$("#jqg-table2").jqGrid('navGrid', '#jqg-pager2', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 조회 : 현업 담당자 탭 - 객관식 설문
	function initJqGrid3() {
		$("#jqg-table3").jqGrid(
				{
					url : '<s:url value="/rest/qam/survresu/multipleChoice"/>',
					datatype : 'json',
					mtype : 'POST',
					colNames : [ '프로젝트명', '응답자', '응답속도', '요구사항', '자료정확',
							'Sys.안정', '사용편의', 'Sys.개선', '개발시스템.종합' ],
					colModel : [ {
						name : 'project.name',
						width : '500'
					}, {
						name : 'username'
					}, {
						name : 't401',
						align : 'right',
						search : false
					}, {
						name : 't402',
						align : 'right',
						search : false
					}, {
						name : 't403',
						align : 'right',
						search : false
					}, {
						name : 't404',
						align : 'right',
						search : false
					}, {
						name : 't405',
						align : 'right',
						search : false
					}, {
						name : 't406',
						align : 'right',
						search : false
					}, {
						name : 't407',
						align : 'right',
						search : false
					} ],
					page : '${page}',
					pager : '#jqg-pager3',
					rowNum : 20,
					sortname : 'project.code',
					sortorder : 'desc',
					height : "100%",
					caption : '프로젝트 공수 목록',
					serializeGridData : function(postData) {
						postData.userType = "T4";
						postData.projectCode = $("#project").val();
						return postData;
					}
				});

		$("#jqg-table3").jqGrid('navGrid', '#jqg-pager3', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 조회 : 현업 담당자 탭 - 주관식 설문
	function initJqGrid4() {
		$("#jqg-table4").jqGrid({
			url : '<s:url value="/rest/qam/survresu/shortAnswer"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명', '응답자', '답변내용' ],
			colModel : [ {
				name : 'project.name',
				width : '70'
			}, {
				name : 'username',
				width : '20'
			}, {
				name : 'content',
				align : 'left'
			} ],
			page : '${page}',
			pager : '#jqg-pager4',
			rowNum : 20,
			sortname : 'project.code',
			sortorder : 'desc',
			height : "100%",
			caption : '프로젝트 공수 목록',
			serializeGridData : function(postData) {
				postData.userType = "T4";
				postData.projectCode = $("#project").val();
				return postData;
			}
		});

		$("#jqg-table4").jqGrid('navGrid', '#jqg-pager4', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 조회 : 시스템 운영자 탭 - 객관식 설문
	function initJqGrid5() {
		$("#jqg-table5").jqGrid(
				{
					url : '<s:url value="/rest/qam/survresu/multipleChoice"/>',
					datatype : 'json',
					mtype : 'POST',
					//datatype : 'local',
					colNames : [ '프로젝트명', '응답자', '문서<br>완성', '문서<br>이해',
							'문서<br>일치', '문서<br>실질', '개발문서.<br/>종합',
							'Sys.<br>구성', 'Sys.<br/>확장', '보안<br>체계',
							'Sys.<br>성능', '오류<br>빈도', '시스템구조및성능<br/>종합',
							'운영<br>편의', '수정<br>용이', '표준<br>준수', 'Sys.<br>연동',
							'User<br>관리', '배치<br>편리', '시스템운영편리성.<br/>종합' ],
					colModel : [ {
						name : 'project.name',
						width : '500'
					}, {
						name : 'username'
					}, {
						name : 't501',
						align : 'right',
						search : false
					}, {
						name : 't502',
						align : 'right',
						search : false
					}, {
						name : 't503',
						align : 'right',
						search : false
					}, {
						name : 't504',
						align : 'right',
						search : false
					}, {
						name : 't505',
						align : 'right',
						search : false
					}, {
						name : 't506',
						align : 'right',
						search : false
					}, {
						name : 't507',
						align : 'right',
						search : false
					}, {
						name : 't508',
						align : 'right',
						search : false
					}, {
						name : 't509',
						align : 'right',
						search : false
					}, {
						name : 't510',
						align : 'right',
						search : false
					}, {
						name : 't511',
						align : 'right',
						search : false
					}, {
						name : 't512',
						align : 'right',
						search : false
					}, {
						name : 't513',
						align : 'right',
						search : false
					}, {
						name : 't514',
						align : 'right',
						search : false
					}, {
						name : 't515',
						align : 'right',
						search : false
					}, {
						name : 't516',
						align : 'right',
						search : false
					}, {
						name : 't517',
						align : 'right',
						search : false
					}, {
						name : 't518',
						align : 'right',
						search : false
					} ],
					page : '${page}',
					pager : '#jqg-pager5',
					rowNum : 20,
					sortname : 'project.code',
					sortorder : 'desc',
					height : "100%",
					caption : '프로젝트 공수 목록',
					serializeGridData : function(postData) {
						postData.userType = "T5";
						postData.projectCode = $("#project").val();
						return postData;
					}
				});

		$("#jqg-table5").jqGrid('navGrid', '#jqg-pager5', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 조회 : 시스템 운영자 탭 - 주관식 설문
	function initJqGrid6() {
		$("#jqg-table6").jqGrid({
			url : '<s:url value="/rest/qam/survresu/shortAnswer"/>',
			datatype : 'json',
			mtype : 'POST',
			colNames : [ '프로젝트명', '응답자', '답변내용' ],
			colModel : [ {
				name : 'project.name',
				width : '70'
			}, {
				name : 'username',
				width : '20'
			}, {
				name : 'content',
				align : 'left'
			} ],
			pager : '#jqg-pager6',
			rowNum : 20,
			sortname : 'project.code',
			sortorder : 'desc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : "100%",
			caption : '프로젝트 공수 목록',
			serializeGridData : function(postData) {
				postData.userType = "T5";
				postData.projectCode = $("#project").val();
				return postData;
			}
		});

		$("#jqg-table6").jqGrid('navGrid', '#jqg-pager6', {/* options */
			add : false,
			edit : false,
			view : false,
			del : false,
			search : true,
			refresh : true
		}, {}, {}, {}, {}, {});
	}

	//그리드 재조회
	function reloadGrid() {
		//사업부 담당자
		$("#jqg-table").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");

		$("#jqg-table2").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");

		//현업 사용자
		$("#jqg-table3").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");

		$("#jqg-table4").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");

		//시스템 운영자
		$("#jqg-table5").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");

		$("#jqg-table6").setGridParam({
			postData : {
				projectCode : $("#projectCode").val()
			}
		}).trigger("reloadGrid");
	}
</script>
<style>
.ui-jqgrid .ui-jqgrid-htable th div {
	overflow: hidden;
	position: relative;
	height: 30px;
	vertical-align: middle !important;
	padding-top: 10px !important;
}
</style>

<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">프로젝트 설문결과 조회</span>
	</div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2">
			<form class="pure-form">
				<input id="project" name="project" type="text"
					class="pure-input-rounded" readonly="true"> <input
					id="projectName" name="projectName" type="text"
					class="pure-input-rounded" placeholder="프로젝트 명"> <a
					id="btn-search" style="font-size: 0.8em">조회</a>
			</form>
		</div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-excel">엑셀저장</a> <a id="btn-expired">설문종료</a>
			</div>
		</div>
	</div>
	<br />
</div>
<div id="tabs">
	<ul style="font-size: 0.7em;">
		<li><a href="#T2">사업부 담당자</a></li>
		<li><a href="#T3">현업 사용자</a></li>
		<li><a href="#T4">시스템 운영자</a></li>
	</ul>
	<div id="T2" style="padding: 10px">
		<table id="jqg-table"></table>
		<div id="jqg-pager"></div>
		<br /> <br />
		<table id="jqg-table2"></table>
		<div id="jqg-pager2"></div>
	</div>
	<div id="T3" style="padding: 10px">
		<table id="jqg-table3"></table>
		<div id="jqg-pager3"></div>
		<br /> <br />
		<table id="jqg-table4"></table>
		<div id="jqg-pager4"></div>
	</div>
	<div id="T4" style="padding: 10px">
		<table id="jqg-table5"></table>
		<div id="jqg-pager5"></div>
		<br /> <br />
		<table id="jqg-table6"></table>
		<div id="jqg-pager6"></div>
	</div>
</div>
<br />
<form id="excelForm" name="excelForm"
	action="<s:url value="/multiExcelDownload" />" method="post">
	<input type="hidden" name="data" id="data" /> <input type="hidden"
		name="filename" id="filename" value="만족도설문조사결과.xlsx" /> <input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
