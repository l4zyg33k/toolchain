<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set value="${empty qualChecFrmVO.id}" var="isNew" />
<script type="text/javascript">
	$(function() {
		//버튼 처리
		initButton();
		
		//자동완성
		initAutoComplete();
		
		//그리드 조회
	    initJqGrid();
	});
	
	//자동완성
	function initAutoComplete() {
		applyAccountAutoComplete("#inspector");
	};	
	
    //버튼 처리
    function initButton(){
        //등록처리
    	$('#btn-add').button({
            icons: {
                primary: 'ui-icon-plus'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('editGridRow', 'new', {
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true,
                closeAfterAdd : true,
                width:600,
				height:260
            });
        });
        //수정처리
        $('#btn-edit').button({
            icons: {
                primary: 'ui-icon-pencil'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('editGridRow', $('#jqg-table').jqGrid('getGridParam', 'selrow'), {
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true,
                closeAfterEdit : true,
                width:600,
				height:260
            });
        });
        //삭제처리
        $("#btn-del").button({
            icons: {
                primary: 'ui-icon-trash'
            }
        }).click(function(event) {
            $("#jqg-table").jqGrid('delGridRow', $('#jqg-table').jqGrid('getGridParam', 'selrow'), {
                <c:if test="${isNew}">
                reloadAfterSubmit: false,
                </c:if>
                recreateForm : true
            });
        });
        //저장
		$('#btn-save').button({
			icons : {
				primary : 'ui-icon-disk'
			}
		}).click(function(event) {           
            <c:if test="${isNew}">
            $('#rowdata').val(JSON.stringify($("#jqg-table").getRowData()));
            </c:if>
			$('#frm-qualityCheckResult').submit();
		}); 
        //취소
		$('#btn-cancel').button({
			icons : {
				primary : 'ui-icon-cancel'
			}
		}).click(function(event) {
 	        var code = $('#project').val();
			var page = $('#page').val();
			var rowNum = $('#rowNum').val();
			var rowId = $('#rowId').val();	
			var mainPage = $('#mainPage').val();
			var mainRowNum = $('#mainRowNum').val();
	        var url = '<s:url value="/qam/qualchec/" />' + code + '/list?page='
					   + page + '&rowNum=' + rowNum + '&rowId=' + rowId + '&mainPage=' + mainPage + '&mainRowNum=' + mainRowNum;
	        $(location).attr('href', url);
	        return false;
		});  
        
        //검토일자
		$('#inspectedDate').datepicker();
        $('#inspectedDate').mask('9999-99-99');
    }
    
	//그리드 조회
	function initJqGrid() {	
		$("#jqg-table").jqGrid({
            <c:choose>
                <c:when test="${isNew}">
                data: rowdata,
                editurl: '<s:url value="/rest/dummy"/>',
                </c:when>
                <c:otherwise> 
                url : '<s:url value="${qualChecFrmVO.getDetailsUrl()}" />',
				editurl : '<s:url value="${qualChecFrmVO.getDetailsEditUrl()}" />',
                mtype: 'POST',
                </c:otherwise>
            </c:choose>
                datatype: '${isNew ? "local" : "json"}',
                colNames: ['#', '결과 유형', '부적합 내용', '시정조치계획/결과','권고일','합의일','완료일'],
                colModel: [{
                        name: 'id',
                        hidden: true,
                        key: true
                        
                    }, {
                        name: 'result',
                        formatter: 'select',
                        editable: true,
                        edittype: 'select', 
                        editrules: {required: true},
                        editoptions: {
                        	value : '${resultTypes}'
                        }
                    }, {
                        name: 'content',
                        width: 300,
                        editable: true,
                        edittype: 'textarea',
                        editoptions : {
							rows : 3,
							cols : 70,
							dataInit : function(e) {
								$(e).val(
										$(e).val().replace(/\<br\>/gi,'\r\n'));
								}

						},
						formatter : function(e) {
							if (e != null) {
								return e.replace(/\r?\n/g,'<br>')
							} else {
								return "";
							}
						},
                        editrules: {required: true}
                    }, {
                        name: 'action',
                        width: 300,
                        editable: true,
                        edittype: 'textarea',
                        editoptions : {
							rows : 3,
							cols : 70,
							dataInit : function(e) {
								$(e).val(
										$(e).val().replace(/\<br\>/gi,'\r\n'));
								}

						},
						formatter : function(e) {
							if (e != null) {
								return e.replace(/\r?\n/g,'<br>')
							} else {
								return "";
							}
						}
                    }, {
                    	 name: 'recommendedDate',
                    	 align : 'center',
 						editable : true,
 						edittype : 'text',
 						editrules : {
 							required : true
 						},
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						},
                         editrules: {required: true}
                    }, {
                        name: 'appointedDate',
                        align : 'center',
 						editable : true,
 						edittype : 'text',
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						}
                    }, {
                        name: 'completedDate',
                        align : 'center',
 						editable : true,
 						edittype : 'text',
 						editoptions : {
 							dataInit : function(e) {
 								$(e).datepicker();
 								$(e).mask('9999-99-99');
 							}
 						}
                    }],
                <c:if test="${not isNew}">
                pager: '#jqg-pager',
                </c:if>
                rowNum : 20,
                sortname: 'id',
                sortorder: 'desc',
                viewrecords: true,
                gridview: true,
                autoencode: true,
                height: "100%",
                caption: '결함 목록',
                loadComplete: function() {
                	<c:choose>
                    <c:when test="${isNew}"></c:when>
                    <c:otherwise>
                    	var recnt = 0; //필수
						var opcnt = 0; //권고
                    	var rowIds = $('#jqg-table').jqGrid('getDataIDs');
						for (var i = 0, len = rowIds.length; i < len; i++) {
							var result = $('#jqg-table').getCell(rowIds[i], "result");
							if(result == 93){
								recnt++;
							}else{
								opcnt++;
							}
						}
						$('#requiredCount').val(recnt);
						$('#opinionCount').val(opcnt);
                    </c:otherwise>
                	</c:choose>
                }  
            });		
	}
    
</script>
<div class="qms-content-header">
    <div class="qms-content-title">
        <i class="fa fa-chevron-circle-right fa-lg"></i><span class="qms-content-title-text">품질점검결과등록</span>
    </div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-save">저장</a> <a id="btn-cancel">취소</a>
			</div>
		</div>
	</div>
	<s:url value="${qualChecFrmVO.getActionUrl()}" var="action" />
	<form:form id="frm-qualityCheckResult" action="${action}" method="${qualChecFrmVO.getMethod()}" modelAttribute="qualChecFrmVO"
		cssClass="pure-form pure-form-stacked">
		<fieldset>
			<legend>품질점검결과</legend>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="project">프젝트 코드</form:label>
					<form:input path="project" readonly="true" />
					<form:errors path="project" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="projectName">프로젝트 이름</form:label>
					<form:input path="projectName" readonly="true" />
					<form:errors path="projectName" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="il" />
					<form:label path="iLname">IL명 </form:label>
					<form:input path="iLname" readonly="true" />
					<form:errors path="iLname" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:hidden path="inspector" />
                    <form:label path="inspectorName" required="true">검토자</form:label>
                    <form:input path="inspectorName"  placeholder="이름을 입력하세요." />
                    <form:errors path="inspectorName" cssClass="ui-state-error-text" />
				</div>				
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="inspectedDate">검토일자</form:label>
					<form:input path="inspectedDate" placeholder="검토일자를 입력하세요." readonly="" />
					<form:errors path="inspectedDate" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="title">제목</form:label>
					<form:input path="title" placeholder="제목를 입력하세요." readonly="" />
					<form:errors path="title" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="stage">단계</form:label> 
                    <form:select path="stage" items="${status}" />
                    <form:errors path="stage" cssClass="ui-state-error-text" />    
				</div>
				<div class="pure-u-1-4">
					<form:label path="type">검토종류</form:label>
					<form:select path="type" items="${kinds}" />
                    <form:errors path="type" cssClass="ui-state-error-text" />  
				</div>
			</div>
			<div class="pure-g">
				<div class="pure-u-1-4">
					<form:label path="hours">소요시간</form:label>
					<form:input path="hours" placeholder="소요시간을 입력하세요." readonly="" />
					<form:errors path="hours" cssClass="ui-state-error-text" />
				</div>	
				<div class="pure-u-1-4">
					<form:label path="requiredCount">필수 건수</form:label>
					<form:input path="requiredCount" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="requiredCount" cssClass="ui-state-error-text" />
				</div>
				<div class="pure-u-1-4">
					<form:label path="opinionCount">권고 건수 </form:label>
					<form:input path="opinionCount" placeholder="이름를 입력하세요."
						readonly="true" />
					<form:errors path="opinionCount" cssClass="ui-state-error-text" />
				</div>
			</div>
			<form:hidden path="id" />
			<form:hidden path="page" />
			<form:hidden path="rowNum" />
			<form:hidden path="rowId" />
			<form:hidden path="mainPage" />
			<form:hidden path="mainRowNum" />
			<c:if test="${isNew}">
				<input type="hidden" id="rowdata" name="rowdata" />
			</c:if>
		</fieldset>
	</form:form>
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add">등록</a> <a id="btn-edit">수정</a> <a id="btn-del">삭제</a>
			</div>
		</div>
	</div>
	<br />
	<div class="pure-g">
		<div class="pure-u-1">
			<table id="jqg-table"></table>
			<c:if test="${not isNew}">
				<div id="jqg-pager"></div>
			</c:if>
		</div>
	</div> 
</div>
<br />