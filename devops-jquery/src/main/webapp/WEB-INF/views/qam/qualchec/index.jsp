<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() { 		
		//버튼 처리
		initButton();
		
		//그리드 조회
	    initJqGrid();
	    
		//네비그리드 세팅
	    initNavGrid();
	});
	
    //버튼 처리
    function initButton(){
        //등록
		setAddButton('#btn-add', eval('${!navGrid.add}'), '#add_jqg-table');
		//수정
        setEditButton('#btn-edit', eval('${!navGrid.edit}'), '#edit_jqg-table');
		//삭제
		setDelButton('#btn-del', eval('${!navGrid.del}'), '#del_jqg-table');
		
		//취소버튼
		$('#code').val("${code}");
		setCancleFormButton('#btn-cancel', false, '<s:url value="${previewMainUrl}" />');		
    }
    
	//그리드 조회
	function initJqGrid() {	
		$("#jqg-table").jqGrid({
            url: '<s:url value="/rest/qam/qualchec" />',
            editurl : '<s:url value="/rest/qam/qualchec/edit" />',
            datatype: 'json',
            mtype: 'POST',
            //datatype : 'local',
            colNames: ['ID','프로젝트코드','프로젝트명', '품질점검제목', '점검일','유형','단계','점검자'],
            colModel: [{
					name : 'id',
					align : 'center',
					search : false,
					hidden : false,
					key : true
			    }, {
                    name: 'projectCode',
                    key: true,
                    align: 'center'
                }, {
                    name: 'projectName',       
                    search: false
                }, {
                    name: 'title',
                    align: 'center',
                    search: false
                }, {
                    name: 'inspectedDate',
                    align: 'center',
                    search: false
                }, {
                    name: 'type',
                    align: 'center',
                    search: false
                }, {
                    name: 'stage',
                    align: 'center',
                    search: false
                }, {
                    name: 'inspector',
                    align: 'center',
                    search: false
                }],
       		page : '${page}',
       		rowNum : '${rowNum}',
       		pager : '#jqg-pager',
            sortname: 'id',
            sortorder: 'desc',
            height: "100%",
            caption: '프로젝트 품질점검 목록',
            serializeGridData : function(postData) {
				postData.projectCode = $('#code').val();
				return postData;
			},
            loadComplete: function() {
            	//데이터가 존재하는 경우
            	if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var rowId = '${rowId}';
					if (rowId) {
						$(this).setSelection(rowId);
					} else {
						rowId = $(this).jqGrid('getDataIDs')[0];
						$(this).setSelection(rowId);
					}
					$("#btn-edit").button("option", "disabled", false);
	            	$("#btn-del").button("option", "disabled", false);
            	}else{
            		//데이터가 없는 경우
            		$("#btn-edit").button("option", "disabled", true);
                	$("#btn-del").button("option", "disabled", true);
            	}
            }
        });		
	}
	
	//그리드 버튼 처리 
	function initNavGrid() {		
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {
				edit : true,
				add : true,
				del : true,
				view : false,
				search : false
			}, {
				beforeInitData : function(formId) {
					return beforeInitDataEdit(formId, this);
				}
			}, {
				beforeInitData : function(formId) {
					return beforeInitDataAdd(formId, this);
				}
			}, {
				beforeInitData : function(formId) {
					return beforeInitDataDel(formId, this);
				}
			}, {}, {});
	}
	
	//수정
	function beforeInitDataEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var editable = $(scope).jqGrid('getRowData', rowId).editable;
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		
		var mainPage = "${mainPage}";
		var mainRowNum = "${mainRowNum}";
		var code = $('#code').val();
		var url = '<s:url value="/qam/qualchec/" />' + code + '/edit?page='
		+ page + '&rowNum=' + rowNum + '&rowId=' + rowId + '&code=' + code+ '&mainPage=' + mainPage + '&mainRowNum=' + mainRowNum;
		$(location).attr('href', url);
		
		return false;
	}
	
	//등록
	function beforeInitDataAdd(formId, scope) {
		var page = $(scope).jqGrid('getGridParam', 'page');
		var rowNum = $(scope).jqGrid('getGridParam', 'rowNum');
		var rowId = $(scope).jqGrid('getGridParam', 'selrow') || '';
		if(rowId == ""){
			rowId = "0";
		} 
		
		var mainPage = "${mainPage}";
		var mainRowNum = "${mainRowNum}";
		var code = $('#code').val();
		var url = '<s:url value="/qam/qualchec/" />' + code + '/form?page='
		+ page + '&rowNum=' + rowNum + '&rowId=' + rowId + '&code=' + code+ '&mainPage=' + mainPage + '&mainRowNum=' + mainRowNum;
		
		$(location).attr('href', url);
		return false;
	}	

	//삭제
	function beforeInitDataDel(formId, scope) {
		return true;
	}	
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">품질점검 결과</span>
	</div>
</div>

<div class="qms-content-main">
<div class="pure-g">
        <div class="pure-u-1-2">
            <form id="query-form" class="pure-form">
				<input type="hidden" name="code" id="code" /> 
            </form>
        </div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset">
				<a id="btn-add" >등록</a>
				<a id="btn-edit">수정</a>
				<a id="btn-del" >삭제</a>
				<a id="btn-cancel" >취소</a>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>  
</div>
<br />