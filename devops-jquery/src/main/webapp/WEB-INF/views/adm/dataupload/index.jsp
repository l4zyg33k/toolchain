<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet"
      href="<c:url value = "/css/jquery.handsontable.full.css" />" />
<script type="text/javascript"
src="<c:url value="/js/jquery.handsontable.full.js" />"></script>
<script type="text/javascript">
	$(function() {
		$("#btn-post").button({ icons : { primary : 'ui-icon-disk' } })
		.click(function(event) {
					if ($("#file").val() == "") {
						infomation("저장하실 파일을 지정하신 후 저장버튼을 눌러주세요.");
						return;
					}
					dialog('Database에 일괄저장 됩니다. 계속하시겠습니까?', function() {
					$("#frm-dateupload").submit();
					});
				});
		/*
		  var data = [["", "Maserati", "Mazda", "Mercedes", "Mini", "Mitsubishi"],
		              ["2009", 0, 2941, 4303, 354, 5814],
		              ["2010", 5, 2905, 2867, 412, 5284],
		              ["2011", 4, 2517, 4822, 552, 6127],
		              ["2012", 2, 2422, 5399, 776, 4151]];
		            
		            $('#example').handsontable({
		              data: data,
		              minSpareRows: 1,
		              colHeaders: true,
		              contextMenu: true
		            });
		            
		            function bindDumpButton() {
		                $('body').on('click', 'button[name=dump]', function () {
		                  var dump = $(this).data('dump');
		                  var $container = $(dump);
		                  console.log('data of ' + dump, $container.handsontable('getData'));
		                });
		              }
		            bindDumpButton();
		*/
	});
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">데이터 업로드</span>
	</div>
</div>

<div class="qms-content-buttonset">
	
	<div style="width:200px"></div>
</div>

<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-2-3">
			<form id="frm-dateupload"
				action="<s:url value="/adm/dataupload"/>" method="POST"
				class="pure-form pure-form-stacked"
				enctype="multipart/form-data">
				<div class="pure-g">
					<div class="pure-u-1">
						<label for="entity" class="pure-u-1-3">유형선택</label>
						<select name="entity" id="entity" class="pure-u-2-3">
							<option value="Account">사용자</option>
						</select>
					</div>
				</div>
				<div class="pure-g">
					<div class="pure-u-1">
						<label for="file" class="pure-u-1-3">엑셀파일</label>
						<input type="file" name="file" id="file" class="pure-u-2-3" type="file" />
					</div>
				</div>
			</form>
		</div>
	</div>
	<br/>
	<a id="btn-post">저장</a>
	<br/>
	<c:if test="${not empty errorMsg}">
	<div>
	<br/>
	선택하신 엑셀파일의 형식이 유형과 일치하지 않습니다. 데이터는 저장되지 않았습니다.
	<br>
	오류사유:<br>
	${errorMsg}
	</div>
	</c:if>
</div>
<br />