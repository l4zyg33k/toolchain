<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');

		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/adm/rolemng" />',
			editurl : '<s:url value="/rest/adm/rolemng/edit" />',
			colModel : [ {
				label : '권한',
				name : 'authority',
				align : 'center',
				key : true,
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				}
			}, {
				label : '이름',
				name : 'name',
				align : 'center',
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				}
			} ],
			pager : '#jqg-pager',
			sortname : 'authority',
			sortorder : 'asc',
			caption : '권한 목록',
			loadComplete : function() {
				if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var id = $(this).jqGrid('getDataIDs')[0];
					$(this).setSelection(id);
				}
			},
			ondblClickRow : function(id) {
				$(this).jqGrid('editGridRow', id);
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager');
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">권한</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />