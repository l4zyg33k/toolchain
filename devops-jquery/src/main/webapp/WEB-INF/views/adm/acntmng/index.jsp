<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initButtons();
		initJqGrid();
		initNavGrid();
	});

	function initButtons() {
		setAddButton('#btn-add', false, '#add_jqg-table');
		setEditButton('#btn-edit', false, '#edit_jqg-table');
		setDelButton('#btn-del', false, '#del_jqg-table');

		$('.qms-content-buttonset').css('display', 'block');
	}

	function initJqGrid() {
		$('#jqg-table').jqGrid({
			url : '<s:url value="/rest/adm/acntmng" />',
			editurl : '<s:url value="/rest/adm/acntmng/edit" />',
			colModel : [ {
				label : '아이디',
				name : 'username',
				align : 'center',
				key : true,
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				},
				align : 'center'
			}, {
				label : '이름',
				name : 'name',
				align : 'center',
				editable : true,
				editrules : {
					required : true
				},
				formoptions : {
					elmsuffix : '<span style="color:red">*</span>'
				},
				align : 'center'
			}, {
				label : '비밀번호(변경)',
				name : 'newPassword',
				hidden : true,
				editable : true,
				editrules : {
					edithidden : true,
					password : true
				},
				align : 'center'
			}, {
				label : '직급',
				name : 'position',
				index : 'position.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${positions}'
				},
				align : 'center'
			}, {
				label : '소속팀',
				name : 'team',
				index : 'team.name',
				editable : true,
				formatter : 'select',
				edittype : 'select',
				editoptions : {
					value : '${teams}'
				},
				align : 'center'
			}, {
				label : '이메일',
				name : 'email',
				editable : true,
				editrules : {
					email : true
				},
				align : 'center'
			}, {
				label : '권한',
				name : 'authorities',
				hidden : true,
				editable : true,
				edittype : 'select',
				editrules : {
					edithidden : true
				},
				editoptions : {
					value : '${authorities}',
					multiple : true
				}
			} ],
			pager : '#jqg-pager',
			sortname : 'name',
			sortorder : 'asc',
			caption : '사용자 목록',
			loadComplete : function() {
				if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var id = $(this).jqGrid('getDataIDs')[0];
					$(this).setSelection(id);
				}
			},
			ondblClickRow : function(id) {
				$('#edit_jqg-table').click();
			}
		});
	}

	function initNavGrid() {
		$("#jqg-table").jqGrid('navGrid', '#jqg-pager', {}, {
			afterShowForm : function(formId) {
				afterShowFormEdit(formId, this);
			}
		});
	}

	function afterShowFormEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var url = '<s:url value="/rest/adm/acntmng/" />' + rowId
				+ '/authorities';
		$.post(url, function(data) {
			$("#authorities", formId).val(data);
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i> <span
			class="qms-content-title-text">사용자 관리</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-add">등록</button>
				<button id="btn-edit">수정</button>
				<button id="btn-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-table"></table>
	<div id="jqg-pager"></div>
</div>
<br />