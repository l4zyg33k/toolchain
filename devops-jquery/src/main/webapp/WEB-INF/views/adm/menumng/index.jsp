<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<script type="text/javascript">
	$(function() {
		initMasterButtons();
		initDetailButtons();
		initMasterJqGrid();
		initDetailJqGrid();
		initMasterNavGrid();
		initDetailNavGrid();
		initButtons();
	});

	function initButtons() {
		$('.qms-content-buttonset').css('display', 'block');
	}

	function initMasterButtons() {
		setAddButton('#btn-master-add', false, '#add_jqg-master-table');
		setEditButton('#btn-master-edit', false, '#edit_jqg-master-table');
		setDelButton('#btn-master-del', false, '#del_jqg-master-table');
	}

	function initDetailButtons() {
		setAddButton('#btn-detail-add', false, '#add_jqg-detail-table');
		setEditButton('#btn-detail-edit', false, '#edit_jqg-detail-table');
		setDelButton('#btn-detail-del', false, '#del_jqg-detail-table');
	}

	function initMasterJqGrid() {
		$('#jqg-master-table').jqGrid({
			url : '<s:url value="/rest/adm/menumng" />',
			editurl : '<s:url value="/rest/adm/menumng/edit" />',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '메뉴 코드',
				name : 'code',
				editable : true,
				editrules : {
					required : true
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '메뉴 이름',
				name : 'name',
				editable : true,
				editrules : {
					required : true
				}
			}, {
				label : '경로',
				name : 'href',
				editable : true
			}, {
				label : '표시 순서',
				name : 'rank',
				align : 'center',
				editable : true,
				editrules : {
					integer : true
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '사용여부',
				name : 'enabled',
				align : 'center',
				formatter : 'checkbox',
				editable : true,
				edittype : 'checkbox',
				editoptions : {
					value : 'true:false',
					defaultValue : 'true'
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '등록자',
				name : 'createdBy.name',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '등록일',
				name : 'createdDate',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '수정자',
				name : 'lastModifiedBy.name',
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '수정일',
				name : 'lastModifiedDate',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '권한',
				name : 'authorities',
				hidden : true,
				editable : true,
				edittype : 'select',
				editrules : {
					edithidden : true
				},
				editoptions : {
					value : '${authorities}',
					multiple : true
				}
			} ],
			pager : '#jqg-master-pager',
			sortname : 'rank',
			sortorder : 'asc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : '100%',
			caption : '상위 메뉴',
			loadComplete : function() {
				if ($(this).jqGrid('getGridParam', 'records') > 0) {
					var id = $(this).jqGrid('getDataIDs')[0];
					$(this).setSelection(id);
				}
			},
			onSelectRow : function(id) {
				reloadChildren(id);
			},
			ondblClickRow : function(id) {
				$('#edit_jqg-master-table').click();
			}
		});
	}

	function initDetailJqGrid() {
		$('#jqg-detail-table').jqGrid({
			datatype : 'local',
			colModel : [ {
				name : 'id',
				key : true,
				hidden : true
			}, {
				label : '메뉴 코드',
				name : 'code',
				editable : true,
				editrules : {
					required : true
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '메뉴 이름',
				name : 'name',
				editable : true,
				editrules : {
					required : true
				}
			}, {
				label : '경로',
				name : 'href',
				editable : true
			}, {
				label : '표시 순서',
				name : 'rank',
				align : 'center',
				editable : true,
				editrules : {
					integer : true
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '사용여부',
				name : 'enabled',
				align : 'center',
				formatter : 'checkbox',
				editable : true,
				edittype : 'checkbox',
				editoptions : {
					value : 'true:false',
					defaultValue : 'true'
				},
				align : 'center',
				width : 80,
				fixed : true
			}, {
				label : '등록자',
				name : 'createdBy.name',
				align : 'center',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '등록일',
				name : 'createdDate',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '수정자',
				name : 'lastModifiedBy.name',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '수정일',
				name : 'lastModifiedDate',
				align : 'center',
				width : 80,
				fixed : true,
				search : false
			}, {
				label : '권한',
				name : 'authorities',
				hidden : true,
				editable : true,
				edittype : 'select',
				editrules : {
					edithidden : true
				},
				editoptions : {
					value : '${authorities}',
					multiple : true
				}
			} ],
			pager : '#jqg-detail-pager',
			sortname : 'rank',
			sortorder : 'asc',
			viewrecords : true,
			gridview : true,
			autoencode : true,
			height : '100%',
			caption : '하위 메뉴',
			ondblClickRow : function(id) {
				$('#edit_jqg-detail-table').click();
			}
		});
	}

	function initMasterNavGrid() {
		$("#jqg-master-table").jqGrid('navGrid', '#jqg-master-pager', {}, {
			afterShowForm : function(formId) {
				afterShowFormEdit(formId, this);
			}
		});
	}

	function initDetailNavGrid() {
		$("#jqg-detail-table").jqGrid('navGrid', '#jqg-detail-pager', {}, {
			afterShowForm : function(formId) {
				afterShowFormEdit(formId, this);
			}
		});
	}

	function reloadChildren(id) {
		var restUrl = '<s:url value="/rest/adm/menumng/" />';
		var url = restUrl + id + '/children';
		var editurl = restUrl + id + '/children/edit';
		var param = {
			datatype : 'json',
			url : url,
			editurl : editurl
		};
		$('#jqg-detail-table').jqGrid('setGridParam', param).trigger(
				"reloadGrid");
	}

	function afterShowFormEdit(formId, scope) {
		var rowId = $(scope).jqGrid('getGridParam', 'selrow');
		var url = '<s:url value="/rest/adm/menumng/" />' + rowId
				+ '/authorities';
		$.post(url, function(data) {
			$("#authorities", formId).val(data);
		});
	}
</script>
<div class="qms-content-header">
	<div class="qms-content-title">
		<i class="fa fa-chevron-circle-right fa-lg"></i><span
			class="qms-content-title-text">메뉴</span>
	</div>
</div>
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-master-add">등록</button>
				<button id="btn-master-edit">수정</button>
				<button id="btn-master-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-master-table"></table>
	<div id="jqg-master-pager"></div>
</div>
<br />
<div class="qms-content-main">
	<div class="pure-g">
		<div class="pure-u-1-2"></div>
		<div class="pure-u-1-2">
			<div class="qms-content-buttonset" style="display: none;">
				<button id="btn-detail-add">등록</button>
				<button id="btn-detail-edit">수정</button>
				<button id="btn-detail-del">삭제</button>
			</div>
		</div>
	</div>
	<br />
	<table id="jqg-detail-table"></table>
	<div id="jqg-detail-pager"></div>
</div>
<br />