<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="_csrf" content="${_csrf.token}" />
        <meta name="_csrf_header" content="${_csrf.headerName}" />        
		
        <title>개발진행 현황판</title>

        <link rel="stylesheet"
              href="<c:url value = "/webjars/jquery-ui-themes/1.11.4/redmond/jquery-ui.css" />" >
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css" />
        <link rel="stylesheet" href="<c:url value = "/webjars/font-awesome/4.3.0/css/font-awesome.min.css" />" >        
        <link rel="stylesheet" href="<c:url value ="/webjars/jqgrid/4.6.0/css/ui.jqgrid.css" />">		        
        <style>
            body {
                margin-top: 60px;
                margin-left: 10px;
                margin-right: 10px;
            }
            
            .ui-jqgrid .ui-jqgrid-view {
                font-family: 'Malgun Gothic', sans-serif;
                font-size: 0.8em;
            }
            
            .ui-jqgrid .ui-jqgrid-htable th div {       
                font-family: 'Malgun Gothic', sans-serif;
                height: 30px;
            }
            
            .ui-jqgrid tr.jqgrow td {
                font-family: 'Malgun Gothic', sans-serif;
                height: 30px;
            }
            
            .ui-jqgrid .ui-pg-table td {            
                font-family: 'Malgun Gothic', sans-serif;
            }
        </style>
    </head>

    <body>
        <c:choose>
            <c:when test="${code eq 'p'}">
                <c:set var="p_title" value="PD팀" />
                <c:url var="p_url" value="/dashboard/projects/p" />
            </c:when>
            <c:otherwise>
                <c:set var="p_title" value="개발파트" />
                <c:url var="p_url" value="/dashboard/projects/d" />
            </c:otherwise>
        </c:choose>        
        <div>
            <p align="center" style="font-family: 'Malgun Gothic ', sans-serif; font-size: xx-large; font-weight: bold;">${p_title} 개발 진행 현황</p>
            <table id="jqg-table"></table>
            <div id="jqg-pager"></div>
        </div>

        <script src="<c:url value ="/webjars/jquery/1.11.4/jquery.min.js" />"></script>
        <script src="<c:url value ="/webjars/jqgrid/4.6.0/js/i18n/grid.locale-kr.js" />"></script>
        <script src="<c:url value ="/webjars/jqgrid/4.6.0/js/minified/jquery.jqGrid.min.js" />"></script>
        <script src="<c:url value ="/js/jquery.jqGrid.custom.js" />"></script>
        <script type="text/javascript">
        $(function() {            
            $("#jqg-table").jqGrid({
                url: '${p_url}',
                datatype: 'json',
                mtype: 'POST',
                colNames: ['사업부', '부서', '프로젝트명', '비지니스리더', '정보기술리더', '진행단계', '시작일', '종료일', '패널회의일', '종료보고일', '운영보고일'],
                colModel: [{
                        name: 'office.name',
                        width: '100px',
                        align: 'center'
                    }, {
                        name: 'blDepartment',
                        width: '70px',
                        align: 'center'
                    }, {
                        name: 'name'
                    }, {
                        name: 'blName',
                        width: '70px',
                        align: 'center'
                    }, {
                        name: 'il.name',
                        width: '70px',
                        align: 'center'
                    }, {
                        name: 'phase.name',
                        width: '70px',
                        align: 'center'
                    }, {
                        name: 'startDate',
                        width: '70px',
                        align: 'center',
                        formatter : 'date',
                        formatoptions: { 'srcformat' : 'Y-m-d H:i:s', 'newformat' : 'Y-m-d' }
                    }, {
                        name: 'finishDate',
                        width: '70px',
                        align: 'center',
                        formatter : 'date',
                        formatoptions: { 'srcformat' : 'Y-m-d H:i:s', 'newformat' : 'Y-m-d' }                        
                    }, {
                        name: 'panelActualDate',
                        width: '70px',
                        align: 'center',
                        formatter : 'date',
                        formatoptions: { 'srcformat' : 'Y-m-d H:i:s', 'newformat' : 'Y-m-d' }                        
                    }, {
                        name: 'closeReportDate',
                        width: '70px',
                        align: 'center',
                        formatter : 'date',
                        formatoptions: { 'srcformat' : 'Y-m-d H:i:s', 'newformat' : 'Y-m-d' }                        
                    }, {
                        name: 'openReportDate',
                        width: '70px',
                        align: 'center',
                        formatter : 'date',
                        formatoptions: { 'srcformat' : 'Y-m-d H:i:s', 'newformat' : 'Y-m-d' }                        
                    }],
                pager: '#jqg-pager',
                rowNum: 20,
                rowList: [20, 40, 60],
                sortname: 'finishDate',
                sortorder: 'asc',
                viewrecords: true,
                gridview: true,
                autoencode: true,
                height: '100%',
                caption: '${p_title} 개발 목록'
            });
        });
        
        var timer = setInterval(function () {
            $("#jqg-table").trigger('reloadGrid', [{current: true}]);
        }, 1000 * 60 * 5);        
        </script>
    </body>
</html>