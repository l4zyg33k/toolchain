package com.gsitm.devops.cmu.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.gsitm.devops.Application;
import com.gsitm.devops.cmu.service.biz.CoWorkMngBiz;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
@Transactional
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
public class CoWorkMngBizImplTest {

	@Autowired
	private CoWorkMngBiz biz;

	@Test
	public void testCreate() {
	}

	@Test
	public void testDelete() {
	}

	@Test
	public void testFindAllPageableSearchableUserDetails() {
	}

	@Test
	public void testFindAllPageableUserDetails() {
	}

	@Test
	@DatabaseSetup(value = "coworkmng/insert.xml")
	public void testFindOne() {
		assertNull(biz.findOne(1L));
	}

	@Test
	public void testUpdate() {
	}
}
